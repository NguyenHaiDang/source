﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Lib.Service;

namespace ZoneAddress_Plugin
{
    public class ZoneAddress : IPlugin
    {
        IOrganizationService service = null;
        IPluginExecutionContext context = null;
        public void Execute(IServiceProvider serviceProvider)
        {
            context = serviceProvider.GetService(typeof(IPluginExecutionContext)) as IPluginExecutionContext;
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            service = factory.CreateOrganizationService(context.UserId);
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            if (context.Depth > 1) return;
            if (context.MessageName == "Create")
            {
                Entity target = context.InputParameters["Target"] as Entity;
                Entity zoneaddress = service.Retrieve(target.LogicalName, target.Id, new ColumnSet(true));
                if (zoneaddress.HasValue("bsd_address"))
                {
                    EntityReference RFzone = (EntityReference)zoneaddress["bsd_zone"];
                    Entity newaddress = new Entity("bsd_address", ((EntityReference)zoneaddress["bsd_address"]).Id);
                    newaddress["bsd_zone"] = new EntityReference(RFzone.LogicalName, RFzone.Id);
                    service.Update(newaddress);
                }
            }
            else if (context.MessageName == "Update")
            {
                Entity target = context.InputParameters["Target"] as Entity;
                Entity zoneaddress = service.Retrieve(target.LogicalName, target.Id, new ColumnSet(true));
                if (zoneaddress.HasValue("bsd_zone"))
                {
                    Entity PreImage = (Entity)context.PreEntityImages["PreImage"];
                    EntityReference RFzone = (EntityReference)zoneaddress["bsd_zone"];
                    if (PreImage.HasValue("bsd_address"))
                    {
                        EntityCollection etcaddressolder = RetrieveTwoCondition("bsd_address", "bsd_addressid", ((EntityReference)PreImage["bsd_address"]).Id, "bsd_zone", RFzone.Id);
                        foreach (var addressolder in etcaddressolder.Entities)
                        {
                            Entity newaddress = new Entity(addressolder.LogicalName, addressolder.Id);
                            newaddress["bsd_zone"] = null;
                            service.Update(newaddress);
                        }
                    }
                    if (zoneaddress.HasValue("bsd_address"))
                    {
                        Entity newaddress = new Entity("bsd_address", ((EntityReference)zoneaddress["bsd_address"]).Id);
                        newaddress["bsd_zone"] = new EntityReference(RFzone.LogicalName, RFzone.Id);
                        service.Update(newaddress);
                    }
                }
            }
        }
        public EntityCollection RetrieveTwoCondition(string localname, string attribute, object value, string attribute1, object value1)
        {
            QueryExpression q = new QueryExpression(localname);
            q.ColumnSet = new ColumnSet(true);
            FilterExpression filter = new FilterExpression();
            filter.AddCondition(new ConditionExpression(attribute, ConditionOperator.Equal, value));
            filter.AddCondition(new ConditionExpression(attribute1, ConditionOperator.Equal, value1));
            q.Criteria = filter;
            return service.RetrieveMultiple(q);
        }
    }
}