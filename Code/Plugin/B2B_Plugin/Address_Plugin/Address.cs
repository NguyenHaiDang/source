﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Lib.Service;

namespace Address
{
    public class Address : IPlugin
    {
        IOrganizationService service = null;
        IPluginExecutionContext context = null;
        public void Execute(IServiceProvider serviceProvider)
        {
            context = serviceProvider.GetService(typeof(IPluginExecutionContext)) as IPluginExecutionContext;
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            service = factory.CreateOrganizationService(context.UserId);
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            if (context.Depth > 1) return;
            if (context.MessageName == "Create")
            {
                //throw new Exception("ok");
                Entity target = context.InputParameters["Target"] as Entity;
                Entity address = service.Retrieve(target.LogicalName, target.Id, new ColumnSet(true));
                if (address.HasValue("bsd_zone"))
                {
                    EntityReference RFzone = (EntityReference)address["bsd_zone"];
                    Entity newzoneaddress = new Entity("bsd_zoneaddress");
                    newzoneaddress["bsd_zone"] = new EntityReference(RFzone.LogicalName, RFzone.Id);
                    newzoneaddress["bsd_address"] = new EntityReference(address.LogicalName, address.Id);
                    service.Create(newzoneaddress);
                }
            }
            else if (context.MessageName == "Update")
            {
                Entity target = context.InputParameters["Target"] as Entity;
                Entity address = service.Retrieve(target.LogicalName, target.Id, new ColumnSet(true));
                Entity PreImage = (Entity)context.PreEntityImages["PreImage"];
                if (PreImage.HasValue("bsd_zone"))
                {
                    EntityCollection etczoneaddressolder = RetrieveTwoCondition("bsd_zoneaddress", "bsd_zone", ((EntityReference)PreImage["bsd_zone"]).Id, "bsd_address", target.Id);
                    foreach (var zoneaddressolder in etczoneaddressolder.Entities)
                    {
                        service.Delete(zoneaddressolder.LogicalName, zoneaddressolder.Id);
                    }
                }
                // tao moi
                if (address.HasValue("bsd_zone"))
                {
                    EntityReference RFzone = (EntityReference)address["bsd_zone"];
                    Entity newzoneaddress = new Entity("bsd_zoneaddress");
                    newzoneaddress["bsd_zone"] = new EntityReference(RFzone.LogicalName, RFzone.Id);
                    newzoneaddress["bsd_address"] = new EntityReference(address.LogicalName, address.Id);
                    service.Create(newzoneaddress);
                }
            }
            
        }
        public EntityCollection RetrieveTwoCondition(string localname, string attribute, object value, string attribute1, object value1)
        {
            QueryExpression q = new QueryExpression(localname);
            q.ColumnSet = new ColumnSet(true);
            FilterExpression filter = new FilterExpression();
            filter.AddCondition(new ConditionExpression(attribute, ConditionOperator.Equal, value));
            filter.AddCondition(new ConditionExpression(attribute1, ConditionOperator.Equal, value1));
            q.Criteria = filter;
            return service.RetrieveMultiple(q);
        }
    }
}