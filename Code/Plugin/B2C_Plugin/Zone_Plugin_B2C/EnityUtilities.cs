﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
namespace Lib.Service
{
    public static class EnityUtilities
    {
        public static bool HasValue(this Entity entity, string attr)
        {
            return entity.Contains(attr) && entity[attr] != null;
        }

        public static List<string> GetAllAttr(this Entity entity)
        {
            List<string> list = new List<string>();
            foreach (var i in entity.Attributes)
            {
                list.Add(i.Key);
            }
            return list;
        }

    }
}
