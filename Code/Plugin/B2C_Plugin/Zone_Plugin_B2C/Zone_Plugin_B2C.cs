﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Lib.Service;

namespace Zone_Plugin_B2C
{
    public class Zone_Plugin_B2C : IPlugin
    {
        IOrganizationService service = null;
        IPluginExecutionContext context = null;
        public void Execute(IServiceProvider serviceProvider)
        {
            context = serviceProvider.GetService(typeof(IPluginExecutionContext)) as IPluginExecutionContext;
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            service = factory.CreateOrganizationService(context.UserId);
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            if (context.Depth > 1) return;
            if (context.MessageName == "Create")
            {
                Entity target = context.InputParameters["Target"] as Entity;
                Entity zone = service.Retrieve(target.LogicalName, target.Id, new ColumnSet(true));
                EntityCollection etcaddress = null;
                if (zone.HasValue("bsd_district"))
                {
                    Guid districtid = ((EntityReference)zone["bsd_district"]).Id;
                    etcaddress = RetrieveFetchXML_Address("bsd_district", "bsd_district", districtid);
                }
                else
                {
                    Guid provinceid = ((EntityReference)zone["bsd_province"]).Id;
                    etcaddress = RetrieveFetchXML_Address("bsd_province", "bsd_province", provinceid);
                }
                if (etcaddress.Entities.Any())
                {
                    foreach (var address in etcaddress.Entities)
                    {
                        Entity newzoneaddress = new Entity("bsd_zoneaddress");
                        newzoneaddress["bsd_zone"] = new EntityReference(zone.LogicalName, zone.Id);
                        newzoneaddress["bsd_address"] = new EntityReference(address.LogicalName, address.Id);
                        newzoneaddress["bsd_name"] = address["bsd_name"];
                        service.Create(newzoneaddress);
                    }
                }
            }
            else if (context.MessageName == "Update")
            {
                Entity target = context.InputParameters["Target"] as Entity;
                Entity zone = service.Retrieve(target.LogicalName, target.Id, new ColumnSet(true));


                // lay tat ca zone address cua thang zon nay.
                QueryExpression q = new QueryExpression("bsd_zoneaddress");
                FilterExpression f = new FilterExpression(LogicalOperator.And);
                f.AddCondition(new ConditionExpression("bsd_zone", ConditionOperator.Equal, zone.Id));
                q.Criteria = f;

                EntityCollection list_zone_address = service.RetrieveMultiple(q);

                //throw new Exception(list_zone_address.Entities.Count.ToString());


                foreach (var item in list_zone_address.Entities)
                {
                    service.Delete(item.LogicalName, item.Id);
                }


                EntityCollection etcaddress = null;
                if (zone.HasValue("bsd_district"))
                {
                    Guid districtid = ((EntityReference)zone["bsd_district"]).Id;
                    etcaddress = RetrieveFetchXML_Address("bsd_district", "bsd_district", districtid);
                }
                else
                {
                    Guid provinceid = ((EntityReference)zone["bsd_province"]).Id;
                    etcaddress = RetrieveFetchXML_Address("bsd_province", "bsd_province", provinceid);
                }
                if (etcaddress.Entities.Any())
                {
                    foreach (var address in etcaddress.Entities)
                    {
                        Entity newzoneaddress = new Entity("bsd_zoneaddress");
                        newzoneaddress["bsd_zone"] = new EntityReference(zone.LogicalName, zone.Id);
                        newzoneaddress["bsd_address"] = new EntityReference(address.LogicalName, address.Id);
                        newzoneaddress["bsd_name"] = address["bsd_name"];
                        service.Create(newzoneaddress);
                    }
                }
            }
        }

        public EntityCollection RetrieveFetchXML_Address(string attribute, string uitype, Guid condition)
        {
            string fetchxml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
                                  <entity name='bsd_address'>
                                    <attribute name='bsd_name' />
                                    <attribute name='bsd_addressid' />
                                    <order attribute='bsd_name' descending='false' />
                                    <link-entity name='bsd_zoneaddress' from='bsd_address' to='bsd_addressid' alias='aa' link-type='outer'>
                                           <attribute name='bsd_zoneaddressid' />
                                      </link-entity>
                                      <filter type='and'>
                                      <condition attribute='" + attribute + "' operator='eq' uitype='" + uitype + "'  value='" + condition + @"' />
                                            <condition attribute='statecode' operator='eq' value='0' />
                                            <condition entityname='aa' attribute='bsd_zoneaddressid' operator='null' uitype='bsd_zoneaddress'  />
                                    </filter>
                                  </entity>
                                </fetch>";
            return service.RetrieveMultiple(new FetchExpression(fetchxml));
        }
    }
}
