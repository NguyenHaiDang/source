//Mr:Diệm
//Note: Load defaut
function Onload() {
    setDisabled("bsd_region", false);
    setNull("bsd_region");
    load_region();
    load_area();
    loadcountry();
}
//Mr:Diệm
//Note: Lookup region follow Province
function LK_Region() {
    console.log(getControl("bsd_region").getDefaultView());
    //{4871A8DD-C3B5-4EFC-B91B-9B9CF9E88306}
    var country = getValue("bsd_country");
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    Xrm.Page.getControl("bsd_region").removePreSearch(presearch_region);
    if (country != null) {
        setDisabled("bsd_region", false);
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_region">',
                        '<attribute name="bsd_regionid"/>',
                        '<attribute name="bsd_name"/>',
                        '<attribute name="createdon"/>',
                        '<order attribute="bsd_name" descending="false"/>',
                        '<filter type="and">',
                        '<condition attribute="bsd_country" operator="eq" uitype="bsd_country" value="' + country[0].id + '"/>',
                          '<condition attribute="statecode" operator="eq"  value="0"/>',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join();
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_regionid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='bsd_regionid'>  " +
                       "<cell name='bsd_name'   " + "width='200' />  " +
                       "<cell name='createdon'    " + "width='100' />  " +
                       "</row>   " +
                    "</grid>";
        getControl("bsd_region").addCustomView("{4871A8DD-C3B5-4EFC-B91B-9B9CF9E88306}", "bsd_region", "Vùng miền", fetchxml, layoutXml, true);
        set_value(["bsd_region", "bsd_area"], null);
        clear_area();
    }
    else {      
        set_value(["bsd_region", "bsd_area"], null);       
        clear_area();
        clear_region();
    }
}
//Author:Mr.Phong
//Description:filter area
function filter_area() {
    var region = Xrm.Page.getAttribute("bsd_region").getValue();
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    var xml = [];
    if (region != null) {
        var viewId = "{32F4BE54-EB7D-4FDA-A5CC-CF7D17572AC2}";
        var entityName = "bsd_areab2c";
        var viewDisplayName = "test";
        fetch(xml, "bsd_areab2c", ["bsd_areab2cid", "bsd_name", "bsd_name"], "bsd_name", false, null
        , ["bsd_region", "statecode"], ["eq", "eq"], [0, region[0].id, 1, "0"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_areab2cid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_areab2cid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_area").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        Xrm.Page.getAttribute("bsd_area").setValue(null);
    }
    else {
        Xrm.Page.getAttribute("bsd_area").setValue(null);
        clear_area()
    }
}
//author:Mr.Phong
//description:load region
function load_region() {
    var country = Xrm.Page.getAttribute("bsd_country").getValue();
    Xrm.Page.getControl("bsd_region").removePreSearch(presearch_region);
    var xml = [];  
        if (country != null) {
            var viewId = "{4871A8DD-C3B5-4EFC-B91B-9B9CF9E88306}";
            var entityName = "bsd_region";
            var viewDisplayName = "test";
            fetch(xml, "bsd_countryregion", ["bsd_regionid", "bsd_name", "createdon"], "bsd_name", false, null
           , ["bsd_country", "statecode"], ["eq", "eq"], [0, country[0].id, 1, "0"]);
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_countryregionid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_countryregionid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_region").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        if (country == null) {
            Xrm.Page.getAttribute("bsd_region").setValue(null);
            clear_region();
        }   
}
//author:Mr.Phong
//description:load area
function load_area() {
    var region = Xrm.Page.getAttribute("bsd_region").getValue();
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    var xml = [];
        if (region != null) {
            var viewId = "{32F4BE54-EB7D-4FDA-A5CC-CF7D17572AC2}";
            var entityName = "bsd_areab2c";
            var viewDisplayName = "test";
            fetch(xml, "bsd_areab2c", ["bsd_areab2cid", "bsd_name", "bsd_name"], "bsd_name", false, null
         , ["bsd_region", "statecode"], ["eq", "eq"], [0, region[0].id, 1, "0"]);
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_areab2cid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_areab2cid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_area").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        if (region == null) {
            Xrm.Page.getAttribute("bsd_area").setValue(null);
            clear_area();
        } 
}
//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    set_disableProvince();
    load_area();
    load_region();
}
//Author:Mr.Đăng
//Description: Disable Province
function set_disableProvince() {
    if (formType() == 2) {
        setDisabled("bsd_id", true);
    }
    else {
        setDisabled("bsd_id", false);
    }
}
//Author:Mr.Đăng
//Description:Check ProvinceID
function Check_ProvinceID() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var id = getValue("bsd_id");
    if (id != null) {
        if (mikExp.test(id)) {
            setNotification("bsd_id", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_province">',
                            '<attribute name="bsd_provinceid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_id" operator="eq" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join('');
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_id", "Mã tỉnh/thành phố đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_id");
                setValue("bsd_id", id.toUpperCase());
            }
        }
    }
}
function clear_area() {
    Xrm.Page.getControl("bsd_area").addPreSearch(presearch_area);
}
function presearch_area() {
    Xrm.Page.getControl("bsd_area").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_region() {
    Xrm.Page.getControl("bsd_region").addPreSearch(presearch_region);
}
function presearch_region() {
    Xrm.Page.getControl("bsd_region").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//author:Mr.Phong
//description: Kiểm tra không cho phép nhập kí tự đặc biệt
function Check_id() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var name = getValue("bsd_name");
    if (name != null) {
        if (mikExp.test(name)) {
            setNotification("bsd_name", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_name");
        }
    }
}
//author:Mr.Phong
//description:load country
function loadcountry() {
    var countryFilter = "<filter type='and'><condition attribute='statecode' operator='eq' value='0'/></filter>";
    Xrm.Page.getControl("bsd_country").addCustomFilter(countryFilter, "bsd_country");
}