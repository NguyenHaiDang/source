//Author:Mr.Đăng
//Description:set filed name
function set_valuefilename() {
    var id = getId();
    var account = getValue("bsd_account");
    var pricegroup = getValue("bsd_pricegroup");
    if (account != null && pricegroup != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_pricegroupdetail">',
                        '<attribute name="bsd_pricegroupdetailid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_pricegroup" operator="eq" uitype="bsd_pricegroups" value="' + pricegroup[0].id + '" />',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + account[0].id + '" />',
                        '<condition attribute="bsd_pricegroupdetailid" operator="ne" value="' + id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setNotification("bsd_account", "Đã tồn tại, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_account");
            setValue("bsd_name", account[0].name);
        }
    }
    else {
        setNull("bsd_name");
    }
}