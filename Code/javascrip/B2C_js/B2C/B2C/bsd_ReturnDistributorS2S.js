/**
Mr:Diệm
Note:Load defaut và khai báo biến.  
**/
var distributor = null;
var outlet = null;
function Onload() {
    debugger;
    Load_Distributor_Change();
    LK_LoadReason(false);
    if (formType() == 2) {
        LK_Distributor_Change(false);
        LK_WarehouseDistributor_Change(false);
        LK_Outlet_Change(false);
    } else {
        clear_outlet();
        clear_warehourse();
        clear_warehousedmsaddress();
        clear_addressoutlet();
        clear_employee();
    }
}
/*
Mr:Diệm
Note: Tìm kiếm warehouse và outlet khi distributor thay đổi.
*/
function LK_Distributor_Change(reset) {
    debugger;
    if (reset != false) setNull(["bsd_outlet", "bsd_addressoutlet", "bsd_employee", "bsd_warehourse", "bsd_warehousedmsaddress"]);
    distributor = getValue("bsd_distributor");
    if (distributor != null) {
        Load_WarehouseDistributor_change();
        Load_OutLetDistributor_Change();
    }
    else if (reset != false) {
        clear_outlet();
        clear_warehourse();
        clear_warehousedmsaddress();
        clear_addressoutlet();
        clear_employee();
    }
}
/*
   Mr:Diệm
   Note:Load account is Distributor.
*/
function Load_Distributor_Change() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '  <entity name="account">',
                    '    <attribute name="name" />',
                    '    <attribute name="accountid" />',
                    '    <order attribute="name" descending="false" />',
                    '    <filter type="and">',
                    '      <condition attribute="bsd_accounttype" operator="eq" value="100000000" />',
                    '    </filter>',
                    '  </entity>',
                    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='accountid'>  " +
                     "<cell name='name'   " + "width='200' />  " +
                     "</row>   " +
                  "</grid>";
    getControl("bsd_distributor").removePreSearch(presearch_outlet);
    getControl("bsd_distributor").addCustomView(getControl("bsd_distributor").getDefaultView(), "account", "Distributor", fetchxml, layoutXml, true);
}

/*
Mr:Diệm
Note: Nếu distribtor có giá trị, sẽ load danh sách outlet.
*/
function Load_OutLetDistributor_Change() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '  <entity name="contact">',
                    '    <attribute name="fullname" />',
                    '    <attribute name="telephone1" />',
                    '    <attribute name="contactid" />',
                    '    <order attribute="fullname" descending="false" />',
                    '    <filter type="and">',
                    '      <condition attribute="parentcustomerid" operator="eq" uitype="account" value="' + distributor[0].id + '" />',
                    '    </filter>',
                    '  </entity>',
                    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='contactid'>  " +
                     "<cell name='fullname'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>";
    getControl("bsd_outlet").removePreSearch(presearch_outlet);
    getControl("bsd_outlet").addCustomView(getControl("bsd_outlet").getDefaultView(), "contact", "OutLet", fetchxml, layoutXml, true);
}
/*
Mr:Diệm
Note: Nếu distribtor có giá trị, sẽ load danh sách warehouse.
*/
function Load_WarehouseDistributor_change() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '  <entity name="bsd_warehousedms">',
                    '    <attribute name="bsd_warehousedmsid" />',
                    '    <attribute name="bsd_name" />',
                    '    <attribute name="createdon" />',
                    '    <order attribute="bsd_name" descending="false" />',
                    '    <filter type="and">',
                    '      <condition attribute="bsd_account" operator="eq" uitype="account" value="' + distributor[0].id + '" />',
                    '    </filter>',
                    '  </entity>',
                    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_warehousedmsid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='bsd_warehousedmsid'>  " +
                  "<cell name='bsd_name'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl("bsd_warehourse").removePreSearch(presearch_warehourse);
    getControl("bsd_warehourse").addCustomView(getControl("bsd_warehourse").getDefaultView(), "bsd_warehousedms", "Warehouse", fetchxml, layoutXml, true);

}
/*
Mr:Diệm
Note: Nếu warehouse có giá trị, sẽ load danh sách Address của warehouse đó.
*/
function LK_WarehouseDistributor_Change(reset) {
    if (reset != false) setNull(["bsd_warehousedmsaddress"]);
    var warehouseDistributor = getValue("bsd_warehourse");
    if (warehouseDistributor != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                        '  <entity name="bsd_address">',
                        '    <attribute name="bsd_name" />',
                        '    <attribute name="createdon" />',
                        '    <attribute name="bsd_purpose" />',
                        '    <attribute name="bsd_account" />',
                        '    <attribute name="bsd_addressid" />',
                        '    <order attribute="bsd_name" descending="false" />',
                        '    <link-entity name="bsd_warehousedms" from="bsd_addresswarehouse" to="bsd_addressid" alias="ac">',
                        '      <filter type="and">',
                        '        <condition attribute="bsd_warehousedmsid" operator="eq" uitype="bsd_warehousedms" value="'+warehouseDistributor[0].id+'" />',
                        '      </filter>',
                        '    </link-entity>',
                        '  </entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            if (rs.length > 0) {
                var first = rs[0];
                if (getValue("bsd_warehousedmsaddress") == null) {
                    setValue("bsd_warehousedmsaddress", [{
                        id: first.Id,
                        name: first.attributes.bsd_name.value,
                        entityType: first.logicalName
                    }]
                    );
                }
            }
        }, function (er) { });
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>";
        getControl("bsd_warehousedmsaddress").removePreSearch(presearch_warehousedmsaddress);
        getControl("bsd_warehousedmsaddress").addCustomView(getControl("bsd_warehousedmsaddress").getDefaultView(), "bsd_address", "Warehouse Address", fetchxml, layoutXml, true);
    } else if (reset != false) {
        clear_warehousedmsaddress();
    }
}
/*
Mr:Diệm
Note: Nếu outlet có giá trị, sẽ load danh sách Address và Employee của outlet đó.
*/
function LK_Outlet_Change(reset) {
    if (reset != false) setNull(["bsd_addressoutlet", "bsd_employee"]);
    outlet = getValue("bsd_outlet");
    if (outlet != null) {
        Load_AddressOutlet_Change();
        Load_EmployeeOutLet_Change();
    }
    else if (reset != false) {
        clear_addressoutlet();
        clear_employee();
    }
}

/*
Mr:Diệm
Note: Nếu outlet có giá trị, sẽ load danh sách Address.
*/
function Load_AddressOutlet_Change() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                    '  <entity name="bsd_address">',
                    '    <attribute name="bsd_name" />',
                    '    <attribute name="createdon" />',
                    '    <attribute name="bsd_purpose" />',
                    '    <attribute name="bsd_account" />',
                    '    <attribute name="bsd_addressid" />',
                    '    <order attribute="bsd_name" descending="false" />',
                    '    <link-entity name="contact" from="bsd_address" to="bsd_addressid" alias="ab">',
                    '      <filter type="and">',
                    '        <condition attribute="contactid" operator="eq" uitype="contact" value="' + outlet[0].id + '" />',
                    '      </filter>',
                    '    </link-entity>',
                    '  </entity>',
                    '</fetch>'].join("");
    CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
        if (rs.length > 0) {
            var first = rs[0];
            if (getValue("bsd_addressoutlet") == null) {
                setValue("bsd_addressoutlet", [{
                    id: first.Id,
                    name: first.attributes.bsd_name.value,
                    entityType: first.logicalName
                }]
                );
            }
        }
    }, function (er) { });
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
               "<row name='result'  " + "id='bsd_addressid'>  " +
               "<cell name='bsd_name'   " + "width='200' />  " +
               "<cell name='createdon'    " + "width='100' />  " +
               "</row>   " +
            "</grid>";
    getControl("bsd_addressoutlet").removePreSearch(presearch_addressoutlet);
    getControl("bsd_addressoutlet").addCustomView(getControl("bsd_addressoutlet").getDefaultView(), "bsd_address", "OutLet Address", fetchxml, layoutXml, true);

}

/*
Mr:Diệm
Note: Nếu outlet có giá trị, sẽ load danh sách Employee.
*/
function Load_EmployeeOutLet_Change() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                    '  <entity name="bsd_employee">',
                    '    <attribute name="bsd_name" />',
                    '    <attribute name="createdon" />',
                    '    <attribute name="bsd_account" />',
                    '    <attribute name="bsd_employeeid" />',
                    '    <order attribute="bsd_name" descending="false" />',
                    '    <link-entity name="contact" from="bsd_employee" to="bsd_employeeid" alias="ae">',
                    '      <filter type="and">',
                    '        <condition attribute="contactid" operator="eq" uitype="contact" value="' + outlet[0].id + '" />',
                    '      </filter>',
                    '    </link-entity>',
                    '  </entity>',
                    '</fetch>'].join("");
    CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
        if (rs.length > 0) {
            var first = rs[0];
            console.log(first);
            if (getValue("bsd_employee") == null) {
                setValue("bsd_employee", [{
                    id: first.Id,
                    name: first.attributes.bsd_name.value,
                    entityType: first.logicalName
                }]
                );
            }
        }
    }, function (er) { });
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_employeeid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='bsd_employeeid'>  " +
                  "<cell name='bsd_name'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl("bsd_employee").removePreSearch(presearch_employee);
    getControl("bsd_employee").addCustomView(getControl("bsd_employee").getDefaultView(), "bsd_employee", "OutLet Employee", fetchxml, layoutXml, true);
}

//MR:DIỆM
//NOTE: HÀM CLEAN, SETNULL KHI CHƯA CHỌN ĐIỀU KIỆN
function clear_outlet() {
    getControl("bsd_outlet").addPreSearch(presearch_outlet);
}
function presearch_outlet() {
    getControl("bsd_outlet").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_warehourse() {
    getControl("bsd_warehourse").addPreSearch(presearch_warehourse);
}
function presearch_warehourse() {
    getControl("bsd_warehourse").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_warehousedmsaddress() {
    getControl("bsd_warehousedmsaddress").addPreSearch(presearch_warehousedmsaddress);
}
function presearch_warehousedmsaddress() {
    getControl("bsd_warehousedmsaddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_addressoutlet() {
    getControl("bsd_addressoutlet").addPreSearch(presearch_addressoutlet);
}
function presearch_addressoutlet() {
    getControl("bsd_addressoutlet").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_employee() {
    getControl("bsd_employee").addPreSearch(presearch_employee);
}
function presearch_employee() {
    getControl("bsd_employee").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

//Author:Mr.Đăng
//Description: Load Reason value 861450001 vs 861450002
function LK_LoadReason(reset) {
    debugger;
    var type = getValue("bsd_type");
    var reason = getValue("bsd_reason");
    if (reset != false) setNull("bsd_reason");
    getControl("bsd_reason").removePreSearch(presearch_Reason);
    if (type == 861450000) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_reason">',
                        '<attribute name="bsd_reasonid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_type" operator="eq" value="861450001" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_reason", "bsd_reason", "bsd_name", "Reason", fetchxml);
    }
    else if (type == 861450001) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_reason">',
                        '<attribute name="bsd_reasonid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_type" operator="eq" value="861450002" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_reason", "bsd_reason", "bsd_name", "Reason", fetchxml);
    }
    else if (reset != false)
    {
        clear_Reason();
    }
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}
function clear_Reason() {
    getControl("bsd_reason").addPreSearch(presearch_Reason);
}
function presearch_Reason() {
    getControl("bsd_reason").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}