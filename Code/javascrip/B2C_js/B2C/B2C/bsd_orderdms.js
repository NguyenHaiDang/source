//Mr:Diêm 
//Note":Fix 
var setnulladdress = true;
var setnullcunsomer = true; 
function EditAuto() {
    Load_PriceList_changeOutlet();
    Load_Account();
    debugger;
    if (formType() == 2) { 
        get_khachhangtheoNPP(false);
        setDisabled(["bsd_distributor","bsd_pricelist"],true)
    } else {
        setDefaultcurrency();
        clear_cusomer();
        clear_employee();
        clear_shiptoaddress();
        clear_WarehouseDistributor();
        Load_FromDate_Auto();
        //clear_distributoraddress();
    }
}
/*
    Mr: Diệm
    Note: set date defaut cho fromdate.
*/
function Load_FromDate_Auto() {
    var date = new Date();
    setValue("bsd_fromdatedelivery", date);
}
//Diệm
//Load Account is distributor.
function Load_Account() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '  <entity name="account">',
                    '    <attribute name="name" />',
                    '    <attribute name="primarycontactid" />',
                    '    <attribute name="telephone1" />',
                    '    <attribute name="accountid" />',
                    '    <order attribute="name" descending="false" />',
                    '    <filter type="and">',
                    '      <condition attribute="bsd_accounttype" operator="eq" value="100000000" />',
                    '    </filter>',
                    '  </entity>',
                    '</fetch>'].join("");
    LookUp_After_Load("bsd_distributor", "account", "name", "Account", fetchxml);
}

//Diệm
// Load price list theo outlet.
function Load_PriceList_changeOutlet(){
	var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
					'  <entity name="pricelevel">',
					'    <attribute name="name" />',
					'    <attribute name="transactioncurrencyid" />',
					'    <attribute name="enddate" />',
					'    <attribute name="begindate" />',
					'    <attribute name="statecode" />',
					'    <attribute name="pricelevelid" />',
					'    <order attribute="name" descending="false" />',
					'    <filter type="and">',
					'      <condition attribute="bsd_type" operator="eq" value="861450002" />',
					'    </filter>',
					'  </entity>',
					'</fetch>'].join("");
	 var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='pricelevelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='pricelevelid'>  " +
                                 "<cell name='name'   " + "width='200' />  " +
                                 "</row>   " +
                              "</grid>   ";
        getControl("bsd_pricelist").addCustomView(getControl("bsd_pricelist").getDefaultView(), "pricelevel", "Price List", fetchxml, layoutXml, true);
}



//Mr:Diệm
//Note: Change customer theo distributor.
function get_khachhangtheoNPP(reset) {
    customer_change(reset)
    if (reset != false) setNull(["bsd_cusomer", "bsd_shiptoaddress", "bsd_employee"]);
    getControl("bsd_cusomer").removePreSearch(presearch_cusomer);
    var distributor = getValue("bsd_distributor");
    if (distributor != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="contact">',
                        '    <attribute name="fullname" />',
                        '    <attribute name="telephone1" />',
                        '    <attribute name="contactid" />',
                        '    <order attribute="fullname" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="parentcustomerid" operator="eq" uitype="account" value="' + distributor[0].id + '" />',
                        '      <condition attribute="bsd_contacttype" operator="eq" value="861450001" />',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join("");

        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='bsd_address'   " + "width='200' />  " +
                                 "</row>   " +
                              "</grid>   ";
        
        getControl("bsd_cusomer").addCustomView(getControl("bsd_cusomer").getDefaultView(), "contact", "Consumer", fetchxml, layoutXml, true);
    }
    else if (reset != false) {
        clear_cusomer();
        clear_employee();
        clear_shiptoaddress();
    }
}

//Mr:Diệm
// Note: load change ship to address follow potential customer   
function customer_change(reset) {
    LoadDistributersAddress(reset)
    debugger;
    if (reset != false && setnulladdress) {
        setNull(["bsd_shiptoaddress", "bsd_employee"]);
    }
    var customer = getValue("bsd_cusomer");
    if (customer != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="contact">',
                        '    <attribute name="fullname" />',
                        '    <attribute name="contactid" />',
                        '    <attribute name="bsd_employee" />',
                        '    <attribute name="bsd_address" />',
                        '    <order attribute="fullname" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="contactid" operator="eq" uitype="contact" value="' + customer[0].id + '" />',

                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join("");

        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            //console.log(rs);
            var first = rs[0];
            if (getValue("bsd_shiptoaddress") == null) {
                if (first.attributes.bsd_address.guid != null && first.attributes.bsd_address.guid != "") {
                    setValue("bsd_shiptoaddress", [{
                        id: first.attributes.bsd_address.guid,
                        name: first.attributes.bsd_address.name,
                        entityType: first.attributes.bsd_address.logicalName
                    }]);
                }
            }

            if (first.attributes.bsd_employee != null) {
                if (getValue("bsd_employee") == null) {
                    setValue("bsd_employee", [{
                        id: first.attributes.bsd_employee.guid,
                        name: first.attributes.bsd_employee.name,
                        entityType: first.attributes.bsd_employee.logicalName
                    }]);
                    clearNotification("bsd_employee");
                }
            } else {
                clear_employee();
                setNotification("bsd_employee", "Khách hàng này không có nhân viên.");
            }
        }
    }

    else if (reset != false) {
        clear_shiptoaddress();
        clear_employee();
    }
}

function clear_shiptoaddress() {
    getControl("bsd_shiptoaddress").addPreSearch(presearch_shiptoaddress);
}
function presearch_shiptoaddress() {
    getControl("bsd_shiptoaddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_cusomer() {
    getControl("bsd_cusomer").addPreSearch(presearch_cusomer);
}
function presearch_cusomer() {
    getControl("bsd_cusomer").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_employee() {
    getControl("bsd_employee").addPreSearch(presearch_employee);
}
function presearch_employee() {
    getControl("bsd_employee").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function onsave() {
    setnulladdress = false;
    setnullcunsomer = false;
}
//Set Defaut Currency
function setDefaultcurrency() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '  <entity name="transactioncurrency">',
                   '    <attribute name="transactioncurrencyid" />',
                   '    <attribute name="currencyname" />',
                   '    <attribute name="isocurrencycode" />',
                   '    <attribute name="currencysymbol" />',
                   '    <attribute name="exchangerate" />',
                   '    <attribute name="currencyprecision" />',
                   '    <order attribute="currencyname" descending="false" />',
                   '    <filter type="and">',
                   '      <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="{B4544CAB-0195-E611-80CC-000C294C7A2D}" />',
                   '    </filter>',
                   '  </entity>',
                   '</fetch>'].join("");
    CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
        //console.log(rs);
        if (rs.length > 0) {
            if (getValue("bsd_currency") == null) {
                setValue("bsd_currency", [{
                    id: rs[0].Id,
                    name: rs[0].attributes.currencyname.value,
                    entityType: "transactioncurrency"
                }]);
            }
        }
    }, function (er) {
        console.log(er.message);
    });
}
//Author:Mr.Đăng
//Description: Load Address Distributers
function LoadDistributersAddress(result) {
    LK_LoadWarehouseDistributor(result)
    if (result != false) setNull("bsd_distributoraddress");
    getControl("bsd_distributoraddress").removePreSearch(presearch_distributoraddress);
    var distributersaddress = getValue("bsd_distributor");
    if (distributersaddress != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + distributersaddress[0].id + '" />',
                        '<condition attribute="bsd_purpose" operator="eq" value="861450000" />',
                        '<condition attribute="statecode" operator="eq" value="0" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            //console.log(rs);
            if (rs.length > 0) {
                var first = rs[0];
                if (getValue("bsd_distributoraddress") == null) {
                    setValue("bsd_distributoraddress", [{
                        id: first.Id,
                        name: first.attributes.bsd_name.value,
                        entityType: first.logicalName
                    }]
                   );
                }
            }

        }, function (er) { });
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_addressid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>";
       
        getControl("bsd_distributoraddress").addCustomView(getControl("bsd_distributoraddress").getDefaultView(), "bsd_address", "Address", fetchxml, layoutXml, true);
    }
    else if (result != false) {
        clear_distributoraddress();
    }
}
function clear_distributoraddress() {
    getControl("bsd_distributoraddress").addPreSearch(presearch_distributoraddress);
}
function presearch_distributoraddress() {
    getControl("bsd_distributoraddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.Đăng
//Description: Load Warehouse theo Account
function LK_LoadWarehouseDistributor(reset) {
    if (reset != false) setNull("bsd_warehouse");
    var distributor = getValue("bsd_distributor");
    getControl("bsd_warehouse").removePreSearch(presearch_WarehouseDistributor);
    if (distributor != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_warehousedms">',
                        '<attribute name="bsd_warehousedmsid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + distributor[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml)
        LookUp_After_Load("bsd_warehouse", "bsd_warehousedms", "bsd_name", "Warehouse", fetchxml);
    }
    else if (reset != false) clear_WarehouseDistributor();
}
function clear_WarehouseDistributor() {
    getControl("bsd_distributoraddress").addPreSearch(presearch_WarehouseDistributor);
}
function presearch_WarehouseDistributor() {
    getControl("bsd_distributoraddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.Đăng
//Description: Load Warehouse theo Account

//auhtor:Mr.Phong
//description:get customer of distributor
/*function get_khachhangtheoNPP()
{
    var distributor = Xrm.Page.getAttribute("bsd_distributor").getValue();
    var xml = [];
    if (distributor != null)
    {
        var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
        var entityName = "contact";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='contact'>");
        xml.push("<attribute name='fullname' />");
        xml.push("<attribute name='telephone1'/>");
        xml.push("<attribute name='contactid'/>");
        xml.push("<order attribute='fullname' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='"+distributor[0].id+"' />");
        xml.push("<condition attribute='jobtitle' operator='null' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='contactid'>  " +
                           "<cell name='fullname'    " + "width='200' />  " +
                            "<cell name='telephone1'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_cusomer").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    }
}*/
//author:Mr.Phong
//description:get employee from customer
/*function get_employeetheocustomer()
{
    var customer = Xrm.Page.getAttribute("bsd_cusomer").getValue();
    var fieldemployee = Xrm.Page.getAttribute("bsd_employee");
    var xml = [];
    if (customer != null) {      
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='contact'>");
        xml.push("<attribute name='fullname' />");
        xml.push("<attribute name='telephone1'/>");
        xml.push("<attribute name='contactid'/>");
        xml.push("<attribute name='bsd_employee'/>");
        xml.push("<order attribute='fullname' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='contactid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0 && rs[0].attributes.bsd_employee!=null) {             
                    fieldemployee.setValue([{
                        id: rs[0].attributes.bsd_employee.guid,
                        name: rs[0].attributes.bsd_employee.name,
                        entityType: 'bsd_employee'
                    }]);               
            }
        },
              function (er) {
                  console.log(er.message)
              });
    }
}*/