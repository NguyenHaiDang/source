function AutoLoad() {
    if (formType() == 2) {
    } else {
        Load_Product();
    }

}
var orderDMSid = null;

//Author:Mr.Đăng
//Description: LK Product theo OrderDMS
function Load_Product() {
    var requestreturnitem = getValue("bsd_requestreturnitem");
    var product = getValue("bsd_product");
    if (requestreturnitem != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="bsd_requestreturnitem">',
                        '    <attribute name="bsd_requestreturnitemid" />',
                        '    <attribute name="bsd_orderdms" />',
                         '   <order attribute="bsd_orderdms" descending="false" />',
                         '   <filter type="and">',
                        '      <condition attribute="bsd_requestreturnitemid" operator="eq" uitype="bsd_requestreturnitem" value="' + requestreturnitem[0].id + '" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            orderDMSid = rs[0].attributes.bsd_orderdms.guid;
        }
        // mr. Đăng
        // lay id order tu return itiem
        if (orderDMSid != null) {
            var fetchxmlorderproduct = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
              '  <entity name="product">',
              '    <attribute name="name" />',
               '   <attribute name="productnumber" />',
               '   <attribute name="description" />',
              '    <attribute name="statecode" />',
               '   <attribute name="productstructure" />',
               '   <attribute name="productid" />',
              '    <order attribute="productnumber" descending="false" />',
               '   <link-entity name="bsd_orderproductdms" from="bsd_productid" to="productid" alias="ac">',
               '     <filter type="and">',
               '       <condition attribute="bsd_orderdms" operator="eq" uitype="bsd_orderdms" value="' + orderDMSid + '" />',
                '      <condition attribute="statecode" operator="eq" value="0" />',
                '    </filter>',
                 ' </link-entity>',
                '</entity>',
             ' </fetch>'].join("");
            LookUp_After_Load("bsd_product", "product", "name", "Product", fetchxmlorderproduct);
        }
    }
    else if (requestreturnitem == null) {
        setNull("bsd_product");
    }
}
function LK_NameProduct() {
    var product = getValue("bsd_product");
    if (product != null && orderDMSid != null) {
        setValue("bsd_name", product[0].name);
        console.log(product);
        if (product != null && orderDMSid != null) {
            var fetchxmlorderproduct = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                        '  <entity name="bsd_orderproductdms">',
                                        '    <attribute name="bsd_orderproductdmsid" />',
                                        '    <attribute name="bsd_uomid" />',
                                        '   <attribute name="bsd_quantitynet" />',
                                        '    <order attribute="bsd_uomid" descending="false" />',
                                        '    <filter type="and">',
                                        '      <condition attribute="bsd_orderdms" operator="eq" uitype="bsd_orderdms" value="'+orderDMSid+'" />',
                                        '      <condition attribute="bsd_productid" operator="eq" uitype="product" value="'+product[0].id+'" />',
                                        '    </filter>',
                                        '  </entity>',
                                        '</fetch>'].join("");
            CrmFetchKit.Fetch(fetchxmlorderproduct, false).then(function (rs) {
                console.log(rs);
                if (rs.length > 0) {
                    if (getValue("bsd_unit") == null) {
                        setValue("bsd_unit", [{
                            id: rs[0].attributes.bsd_uomid.guid,
                            name: rs[0].attributes.bsd_uomid.name,
                            entityType: "uom"
                        }]);
                    }
                    if (getValue("bsd_quantityorder") == null) {
                        setValue("bsd_quantityorder", rs[0].attributes.bsd_quantitynet.value);
                    }
                }
            }, function (er) {
                console.log(er.message);
            });
        }
        LK_CheckQuantity();
    }
    else {
        setNull(["bsd_name", "bsd_unit", "bsd_quantityorder"]);
    }
}
//Author:Mr.Đăng
//Description: Check Quantity
function LK_CheckQuantity() {
    var product = getValue("bsd_product");
    if (product != null && orderDMSid != null) {
        console.log(product);
        if (product != null && orderDMSid != null) {
            var fetchxmlorderproduct = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                        '<entity name="bsd_orderproductdms">',
                                        '<attribute name="bsd_orderproductdmsid" />',
                                        '<attribute name="bsd_quantity" />',
                                        '<order attribute="bsd_quantity" descending="false" />',
                                        '<filter type="and">',
                                        '<condition attribute="bsd_orderdms" operator="eq" uitype="bsd_orderdms" value="'+ orderDMSid +'" />',
                                        '<condition attribute="bsd_productid" operator="eq" uitype="product" value="'+ product[0].id +'" />',
                                        '</filter>',
                                        '</entity>',
                                        '</fetch>'].join("");
            CrmFetchKit.Fetch(fetchxmlorderproduct, false).then(function (rs) {
                console.log(rs);        
                if (rs.length > 0) {
                    var quantityRI = getValue("bsd_quantity");
                    var quantity = rs[0].attributes.bsd_quantity.value;
                    if (quantityRI != null && quantityRI > quantity)
                        setNotification("bsd_quantity", "Vui lòng nhập Quantity nhỏ hơn hoặc bằng Quantity OrderDMS");
                    else
                        clearNotification("bsd_quantity");
                }
            }, function (er) {
                console.log(er.message);
            });
        }
    }
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}