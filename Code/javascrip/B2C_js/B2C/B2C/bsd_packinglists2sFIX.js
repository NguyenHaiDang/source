//Mr:Diệm
//Note: load defaut.
var employee = null;
function AutoLoad() {
    debugger;
    LK_LoadReason();
    if (formType() == 2) {
        Lk_Account_Distributor_Change();
        LK_Distributor_Change(false);
        LK_Employee_Change(false);
        set_hideconsumer();
    } else {
        Lk_Account_Distributor_Change();
        clear_consumer();
        clear_warehouse();
        clear_address();
        clear_employee();
        set_hideconsumer();
    }
}
//Mr:Diệm
//Note: load defaut Account is distribitor.
function Lk_Account_Distributor_Change() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name"/>',
                    '<attribute name="primarycontactid"/>',
                    '<attribute name="telephone1"/>',
                    '<attribute name="accountid"/>',
                    '<order attribute="name" descending="false"/>',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="100000000"/>',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                          "<row name='result'  " + "id='accountid'>  " +
                          "<cell name='name'   " + "width='200' />  " +
                          "<cell name='telephone1'    " + "width='100' />  " +
                          "</row>   " +
                       "</grid>   ";
    getControl("bsd_distributor").addCustomView("{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}", "account", "Distributor", fetchxml, layoutXml, true);
}
//Mr: Diệm
function LK_Distributor_Change(reset) {
    debugger;
    if (reset != false) setNull(["bsd_warehousefrom", "bsd_address", "bsd_employee", "bsd_contact"]);
    var distributer = getValue("bsd_distributor");
    if (distributer != null) {
        //Note: change warehouse follow distributor
        var fetchxml1 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_warehousedms">',
                            '<attribute name="bsd_warehousedmsid"/>',
                            '<attribute name="bsd_name"/>',
                            '<attribute name="createdon"/>',
                            '<order attribute="bsd_name" descending="false"/>',
                            '<filter type="and">',
                            '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + distributer[0].id + '"/>',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
        var layoutxml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_warehousedmsid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_warehousedmsid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "</row>   " +
                   "</grid>";
        getControl("bsd_warehousefrom").removePreSearch(presearch_warehouse);
        getControl("bsd_warehousefrom").addCustomView("{4871A8DD-C3B5-4EFC-B91B-9B9CF9E88306}", "bsd_warehousedms", "Warehouse Form", fetchxml1, layoutxml1, true);
        
        // Note: Change address follow distributor 
        if (getValue("bsd_address") == null) {
            var fetchxml2 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_address">',
                            '<attribute name="bsd_name"/>',
                            '<attribute name="createdon"/>',
                            '<attribute name="bsd_purpose"/>',
                            '<attribute name="bsd_account"/>',
                            '<attribute name="bsd_addressid"/>',
                            '<order attribute="bsd_name" descending="false"/>',
                            '<filter type="and">',
                            '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + distributer[0].id + '"/>',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            //Note:Set defaut address
            CrmFetchKit.Fetch(fetchxml2, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    console.log(first);
                    setValue("bsd_address", [{
                        id: first.Id,
                        name: first.attributes.bsd_name.value,
                        entityType: first.logicalName
                    }]
                    );
                }
            }, function (er) { });
        }
        var layoutxml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "</row>   " +
                   "</grid>";
        getControl("bsd_address").removePreSearch(presearch_address);
        getControl("bsd_address").addCustomView("{A2D479C5-53E3-4C69-ADDD-802327E67A0D}", "bsd_address", "Address", fetchxml2, layoutxml2, true);

        //Note:Change employee follow distributor
        console.log(getControl("bsd_employee").getDefaultView());
        var fetchxml3 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                         '<entity name="bsd_employee">',
                         '<attribute name="bsd_name"/>',
                         '<attribute name="createdon"/>',
                         '<attribute name="bsd_account"/>',
                         '<attribute name="bsd_employeeid"/>',
                         '<order attribute="bsd_name" descending="false"/>',
                         '<filter type="and">',
                         '<condition attribute="bsd_account" operator="eq" uitype="account" value="'+distributer[0].id+'"/>',
                         '</filter>',
                         '</entity>',
                         '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml3, false).then(function (rs3) {
            if (rs3.length > 0) {
                var first = rs3[0];
                employee = first.Id;
            }
        }, function (er) { });
        var layoutxml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_employeeid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_employeeid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "</row>   " +
                  "</grid>";
        getControl("bsd_employee").removePreSearch(presearch_employee);
        getControl("bsd_employee").addCustomView("{FF82D3E5-2040-4798-AABE-9ECB298228E9}", "bsd_employee", "Employee", fetchxml3, layoutxml3, true);
    }
    else if (reset != false) {
        clear_consumer();
        clear_warehouse();
        clear_address();
        clear_employee();
    }
}
//Mr:Diệm
function LK_Employee_Change(reset) {
    debugger
    //Note: Change concumer follow employee
    if (reset != false) setNull(["bsd_contact"]);
    if (employee != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="contact">',
                        '    <attribute name="fullname" />',
                        '    <attribute name="telephone1" />',
                        '    <attribute name="contactid" />',
                        '    <order attribute="fullname" descending="false" />',
                        '    <link-entity name="bsd_employee" from="bsd_employeeid" to="bsd_employee" alias="ac">',
                        '      <filter type="and">',
                        '        <condition attribute="bsd_employeeid" operator="eq" uitype="bsd_employee" value="'+employee+'" />',
                        '        <condition attribute="bsd_salegroup" operator="eq" value="861450000" />',
                        '      </filter>',
                        '    </link-entity>',
                        '  </entity>',
                        '</fetch>'].join("");
        var layoutxml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='contactid'>  " +
                       "<cell name='fullname'   " + "width='200' />  " +
                       "</row>   " +
                    "</grid>";
        getControl("bsd_contact").removePreSearch(presearch_consumer);
        getControl("bsd_contact").addCustomView("{4871A8DD-C3B5-4EFC-B91B-9B9CF9E88306}", "contact", "contact", fetchxml, layoutxml, true);
    } else if (reset != false) {
        clear_consumer();
    }
}

//Note: Function set condition defaut clean you load formtype = 1
function clear_consumer() {
    getControl("bsd_contact").addPreSearch(presearch_consumer);
}
function presearch_consumer() {
    getControl("bsd_contact").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_warehouse() {
    getControl("bsd_warehousefrom").addPreSearch(presearch_warehouse);
}
function presearch_warehouse() {
    getControl("bsd_warehousefrom").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_address() {
    getControl("bsd_address").addPreSearch(presearch_address);
}
function presearch_address() {
    getControl("bsd_address").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_employee() {
    getControl("bsd_employee").addPreSearch(presearch_employee);
}
function presearch_employee() {
    getControl("bsd_employee").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//author:Mr.Phong
//description:hide consumer theo packing type
function set_hideconsumer()
{
    var packingtype = Xrm.Page.getAttribute("bsd_packinglisttype").getValue();
    if (packingtype == "861450000")
    {
        Xrm.Page.ui.controls.get("bsd_contact").setVisible(false);
    }
   else{
        Xrm.Page.ui.controls.get("bsd_contact").setVisible(true);
    }
}
//Author:Mr.Đăng
//Description: Load Reason value 861450003
function LK_LoadReason() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="bsd_reason">',
                    '<attribute name="bsd_reasonid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="createdon" />',
                    '<order attribute="bsd_name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_type" operator="eq" value="861450003" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    LookUp_After_Load("bsd_reason", "bsd_reason", "bsd_name", "Reason", fetchxml);
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}