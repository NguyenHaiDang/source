﻿//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    LK_LoadReason();
}
//Author:Mr.Đăng
//Description: Load Reason value 861450004
function LK_LoadReason() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="bsd_reason">',
                    '<attribute name="bsd_reasonid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="createdon" />',
                    '<order attribute="bsd_name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_type" operator="eq" value="861450004" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    LookUp_After_Load("bsd_reason", "bsd_reason", "bsd_name", "Reason", fetchxml);
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}