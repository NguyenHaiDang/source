function init() {
    check_type();
    if (formType() == 1) {
        setValueDefault();
        load_bankdefault(true);
        load_unitdefault(true);
        load_currencydefault(true);
        customeraccount_change(true);
        check_enable_shipping();
        setTimeout(function () { Xrm.Page.getControl("bsd_potentialcustomer").setFocus(); }, 1000);
    } else {

        load_congno(false);
        load_bankdefault(false);
        load_currencydefault(false);
        load_unitdefault(false);
        customeraccount_change(false);

        if (getValue("bsd_status") != 861450000 || getValue("bsd_duyet")) DisabledForm();
        load_paymentterm_date(false);

        //hide_checkbox();

        check_enable_shipping(false);
        porteroption_change(false);
        check_create_deliveryplan();
        var type = getValue("bsd_type");
        if (type == 861450001 || type == 861450002 || type == 861450003) {
            load_congno(true);
            setDisabled(["bsd_potentialcustomer", "bsd_addresscustomeraccount", "bsd_invoicenameaccount", "bsd_addressinvoiceaccount", "bsd_warehousefrom", "bsd_shiptoaccount", "bsd_shiptoaddress", "bsd_fromdate", "bsd_todate"], true);
            setDisabled(["bsd_warehouseaddress", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_unitshipping", "bsd_unitdefault"], true);
            setDisabled(["bsd_requestporter", "bsd_transportation", "bsd_shippingdeliverymethod", "bsd_truckload", "bsd_porter", "bsd_priceofporter", "bsd_shippingporter"], true);
            setDisabled(["bsd_customerpo", "bsd_paymentterm", "bsd_paymentmethod", "bsd_porteroption", "bsd_pricepotter"], true);
        }


    }
    hide_button();
    check_show_duyet();
    Load_MaNhaPhanPhoi();
    visible();
    check_customerpo();
    Load_UseAccountId();
}

//Diệm: 14/3/2016: an suborderid theo user.
var accountid = null;
function Load_UseAccountId() {
    if (AccountUserID().length > 0) {
        if (AccountUserID()[0].attributes.bsd_accounttype.value == 100000000) {
            if (getValue("bsd_duyet") == true) {
                //   setVisible("bsd_customerpo", true);
            }
            setDisabled("bsd_duyet", true);
            setVisible(["bsd_so", "bsd_sodate", "bsd_customerpo", "bsd_creditlimit", "bsd_customerdebt"
                , "bsd_requestedshipdate", "bsd_requestedreceiptdate", "bsd_confirmedreceiptdate"], false);
            setVisible(["bsd_suborderidnpp"], true);
            setRequired(["bsd_requestedshipdate", "bsd_requestedreceiptdate"], "none");
        }
    }
}

// Ẩn hien so chung tu khi là nha pan phối và là khách hàng B2C
function Load_MaNhaPhanPhoi() {
    var customer = getValue("bsd_potentialcustomer");
    if (customer != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="account">',
                        '    <attribute name="name" />',
                        '    <attribute name="primarycontactid" />',
                        '    <attribute name="telephone1" />',
                        '    <attribute name="accountid" />',
                        '    <attribute name="bsd_accounttype" />',
                        '    <order attribute="name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                        '      <condition attribute="bsd_accounttype" operator="not-null" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_accounttype.value == 100000000) {
                    setVisible(["bsd_so", "bsd_customerpo"], false);
                    setVisible("bsd_suborderidnpp", true);
                } else {
                    setVisible(["bsd_so"], true);
                    setVisible("bsd_suborderidnpp", false);
                }
            }
            else {
                setVisible("bsd_so", true);
                setVisible("bsd_suborderidnpp", false);
            }
        });
    } else {
        setVisible("bsd_so", true);
        setVisible("bsd_suborderidnpp", false);
    }
}
//Diệm set hiện S.O
function visible() {
    debugger;
    if (getValue("bsd_duyet") == true) {
        setVisible(["bsd_so", "bsd_customerpo", "bsd_sodate"], true);
        setVisible("bsd_suborderidnpp", false);
        setDisabled("bsd_sodate", false);
    }
}
//end Diệm

//huy
//nếu quotation type  = 861450000 hoặc nếu từ order mà customer po = null => customer po trong sub sẽ hiển thị, không khóa... để người dùng nhập
function check_customerpo() {
    var quoteid = getValue("bsd_quote");
    var orderid = getValue("bsd_order");
    if (quoteid != null) {
        console.log(quoteid);
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="quote">',
                   '     <attribute name="name" />',
                   '     <attribute name="customerid" />',
                   '     <attribute name="statecode" />',
                   '     <attribute name="totalamount" />',
                   '     <attribute name="quoteid" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_quotationtype" />',
                   '     <order attribute="name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="quotenumber" operator="eq" value="' + quoteid[0].name + '" />',
                   '       <condition attribute="bsd_quotationtype" operator="eq" value="861450000" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                setDisabled("bsd_customerpo", false);
            }
        });
    } else if (orderid != null) {//sub tạo từ order, nếu PO null thì hiện ra cho user nhập
        if (getValue("bsd_customerpo") != null) {
            setDisabled("bsd_customerpo", true);
        }
        else {
            setDisabled("bsd_customerpo", false);
        }
        setVisible("bsd_customerpo", true);
    }
}

function check_show_duyet() {
    var status = getValue("bsd_status");
    if (status == 861450000 && getValue("bsd_duyet") == false && formType() == 2) {
        setDisabled("bsd_duyet", false);
    } else {
        setDisabled("bsd_duyet", true);
    }
}

function check_create_deliveryplan() {
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="bsd_deliveryplan">',
        '<attribute name="bsd_deliveryplanid" />',
        '<attribute name="bsd_name" />',
        '<attribute name="createdon" />',
        '<order attribute="bsd_name" descending="false" />',
        '<filter type="and">',
          '<condition attribute="bsd_suborder" operator="eq" uitype="bsd_suborder" value="' + getId() + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            DisabledForm();
        }
    });
}

function setValueDefault() {
    setValue("bsd_type", 861450000);
    setValue("bsd_detailamount", 0);
    setValue("bsd_totaltax", 0)
    setValue("bsd_totalamount", 0)
    setValue("bsd_totalcurrencyexchange", 0)
    setValue("bsd_date", getCurrentDateTime());
}

function check_type() {
    var type = getValue("bsd_type");
    if (type == 861450001 || type == 861450003) {
        setVisible("bsd_quote", true);
        Xrm.Page.getControl("orderdelierystatusSubgrid").setVisible(false);
        filter_quotedeliverystatus_grid();
    } else if (type == 861450002) {
        setVisible("bsd_order", true);
        Xrm.Page.getControl("quotedelierystatusSubgrid").setVisible(false);
        filter_orderdeliverystatus_grid();
    } else {
        Xrm.Page.getControl("orderdelierystatusSubgrid").setVisible(false);
        Xrm.Page.getControl("quotedelierystatusSubgrid").setVisible(false);
    }
}

function load_warehouse_address(reset) {
    clearNotification("bsd_warehouseaddress");

    if (reset != false) setNull("bsd_warehouseaddress");

    var warehouse = getValue("bsd_warehousefrom");
    if (warehouse != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_warehouseentity">',
                        '<attribute name="bsd_warehouseentityid" />',
                        '<attribute name="bsd_address" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_warehouseentityid" operator="eq" uitype="bsd_warehouseentity" value="' + warehouse[0].id + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && reset != false) {
                var first = rs[0];
                if (first.getValue("bsd_address") != null) {
                    setValue("bsd_warehouseaddress", [{
                        id: first.attributes.bsd_address.guid,
                        name: first.attributes.bsd_address.name,
                        entityType: first.attributes.bsd_address.logicalName
                    }]);
                } else {
                    setNotification("bsd_warehouseaddress", "You must provide a value for Warehouse Address!");
                }
                
            }
        });
    }
    check_enable_shipping(reset);
}

function receipcustomer_change(reset) {
    if (reset != false) setNull(["bsd_shiptoaddress", "bsd_contactshiptoaccount"]);
    getControl("bsd_shiptoaddress").removePreSearch(presearch_shiptoaddress);
    load_contact_shiptoaccount(reset);
    var shiptoaccount = getValue("bsd_shiptoaccount");
    if (shiptoaccount != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_address">',
                   '    <attribute name="bsd_name" />',
                   '     <attribute name="bsd_account" />',
                   '     <attribute name="bsd_addressid" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_account" operator="eq" uitype="account" value="' + shiptoaccount[0].id + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                            "<row name='result'  " + "id='bsd_addressid'>  " +
                                            "<cell name='bsd_name'   " + "width='200' />  " +
                                            "</row>" +
                                         "</grid>";
        if (reset != false) {
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                setDisabled("bsd_shiptoaddress", false);
                if (rs.length > 0) {

                    setValue("bsd_shiptoaddress", [{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
            });
        }
        getControl("bsd_shiptoaddress").addCustomView(getDefaultView("bsd_shiptoaddress"), "bsd_address", "address", xml, layoutXml, true);
    }
    else if (reset != false) {
        //clear_shiptoaddress();
        setDisabled("bsd_shiptoaddress", true);

    }
}

function load_shiptoaddress(reset) {

}

function customeraccount_change(reset) {
    if (reset != false) setNull(["bsd_customercode", "bsd_taxregistration", "bsd_telephone"]);
    var customeraccount = getValue("bsd_potentialcustomer");
    if (customeraccount != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="account">',
                   '     <attribute name="name" />',
                   '     <attribute name="primarycontactid" />',
                   '     <attribute name="telephone1" />',
                   '     <attribute name="accountid" />',
                   '     <attribute name="accountnumber" />',
                   '     <attribute name="bsd_taxregistration" />',
                   '     <order attribute="name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="accountid" operator="eq" uitype="account" value="' + customeraccount[0].id + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");

        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && reset != false) {
                setValue("bsd_customercode", rs[0].getValue("accountnumber"));
                setValue("bsd_taxregistration", rs[0].getValue("bsd_taxregistration"));
                setValue("bsd_telephone", rs[0].getValue("telephone1"));
                setValue("bsd_shiptoaccount", [{
                    id: rs[0].Id,
                    name: rs[0].attributes.name.value,
                    entityType: rs[0].logicalName
                }]);
                if (getValue("bsd_invoicenameaccount") == null && reset != false) {
                    setValue("bsd_invoicenameaccount", [{
                        id: rs[0].Id,
                        name: rs[0].attributes.name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
                if (getValue("bsd_bank") == null) {
                    load_bankdefault(reset);
                }
            }
        });
        
    }
    alert(getValue("bsd_shiptoaccount")+getValue("bsd_invoicenameaccount"))

    load_customer_address(reset);
    load_contact(reset);
    load_currency(reset);
    load_pricelist(reset);
    load_paymentterm_date(reset);
    load_paymentmethod(reset);
    load_duedate(reset);
    receipcustomer_change(reset);
    invoiceaccount_change(reset);
    check_currency(reset);
    bank_change(reset);
    load_congno(reset);
}

function load_customer_address(reset) {
    if (reset != false) setNull("bsd_addresscustomeraccount");
    var customeraccount = getValue("bsd_potentialcustomer");
    if (customeraccount != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_address">',
                   '    <attribute name="bsd_name" />',
                   '     <attribute name="bsd_purpose" />',
                   '     <attribute name="bsd_account" />',
                   '     <attribute name="bsd_addressid" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_account" operator="eq" uitype="account" value="' + customeraccount[0].id + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='bsd_addressid'>  " +
                  "<cell name='bsd_name'   " + "width='200' />  " +
                  "</row>   " +
               "</grid>";
        getControl("bsd_addresscustomeraccount").addCustomView(getDefaultView("bsd_addresscustomeraccount"), "bsd_address", "Address", xml, layoutXml, true);
        if (reset != false) {
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    setValue("bsd_addresscustomeraccount", [{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
            });
        }
    } else if (reset != false) {
        clear_lookup("bsd_addresscustomeraccount", "bsd_address");
    }
}

function load_contact(reset) {
    if (reset != false) setNull("bsd_contact");
    var customeraccount = getValue("bsd_potentialcustomer");
    getControl("bsd_contact").removePreSearch(presearch_contact);
    if (customeraccount != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="contact">',
                   '     <attribute name="fullname" />',
                   '     <attribute name="telephone1" />',
                   '     <attribute name="contactid" />',
                   '     <order attribute="fullname" descending="false" />',
                  '      <filter type="and">',
                  '     <condition attribute="bsd_contacttype" operator="eq" value="861450000" />',
                  '        <condition attribute="parentcustomerid" operator="eq" uitype="account" value="' + customeraccount[0].id + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='contactd'>  " +
                  "<cell name='bsd_name'   " + "width='200' />  " +
                  "</row>   " +
               "</grid>";
        getControl("bsd_contact").addCustomView(getControl("bsd_contact").getDefaultView(), "contact", "contact", xml, layoutXml, true);
        if (reset != false) {
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                setDisabled("bsd_contact", false);
                if (rs.length > 0) {
                    setValue("bsd_contact", [{
                        id: rs[0].Id,
                        name: rs[0].attributes.fullname.value,
                        entityType: rs[0].logicalName
                    }])
                }
            });
        }

    }
    else if (reset != false) {
        //clear_contact();
        setDisabled("bsd_contact", true);
    }
}

function getCurrentDateTime() {
    var d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
}

function currency_change(reset) {

    check_currency(reset);
    load_pricelist(reset);
}

//huy
function paymentterm_change(reset) {

    if (reset != false) setNull("bsd_datept");

    var paymentterm = getValue("bsd_paymentterm");
    if (paymentterm != null && reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_paymentterm">',
                   '     <attribute name="bsd_paymenttermid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_date" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_termofpayment" operator="eq" value="' + paymentterm[0].name + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join('');
        CrmFetchKit.Fetch(xml).then(function (rs) {
            var first = rs[0];

            setValue("bsd_datept", first.attributes.bsd_date.value);
            load_duedate(reset);
        });
    }
    //load_duedate(reset);
}


//lấy pricelist khi customer change, from date, to date
function load_pricelist(reset) {
    debugger;
    clearNotification("bsd_pricelist");
    var customer = getValue("bsd_potentialcustomer");
    var fromdate = getFullDay("bsd_fromdate");
    var todate = getFullDay("bsd_todate");
    var currency = getValue("transactioncurrencyid");
    if (reset != false) setNull("bsd_pricelist");
    var first;
    if (fromdate == null || todate == null) {
        var fromdate = getCurrentDateTime().getFullYear() + "-" + (getCurrentDateTime().getMonth() + 1) + "-" + getCurrentDateTime().getDate();
        var todate = getCurrentDateTime().getFullYear() + "-" + (getCurrentDateTime().getMonth() + 1) + "-" + getCurrentDateTime().getDate();
    }
    if (customer != null && currency != null && reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" count="1"  mapping="logical" distinct="true">',
                           '    <entity name="pricelevel">',
                           '        <all-attributes />',
                           '        <order attribute="begindate" descending="false" />',
                           '    <filter type="and">',
                           '        <condition attribute="begindate" operator="on-or-before" value="' + fromdate + '" />',
                           '        <condition attribute="enddate" operator="on-or-after" value="' + todate + '" />',
                           '        <condition attribute="bsd_account" operator="eq" uitype="account" value="' + customer[0].id + '"/>',
                           '        <condition attribute="statecode" operator="eq" value="0" />',
                           '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                           '    </filter>',
                           '    </entity>',
                           '</fetch>'].join('');
        CrmFetchKit.Fetch(xml, false).then(function (rsaccount) {
            if (rsaccount.length > 0) /*Nếu dữ liệu Account*/ {
                first = rsaccount[0];
                if (getValue("bsd_pricelist") == null) {
                    setValue("bsd_pricelist",
                         [{
                             id: first.Id,
                             name: first.attributes.name.value,
                             entityType: first.logicalName
                         }]);
                }
            }
            else /*Price group*/ {

                var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                                    '   <entity name="pricelevel">',
                                    '        <all-attributes />',
                                    '     <order attribute="begindate" descending="false" />',
                                    '     <filter type="and">',
                                    '        <condition attribute="begindate" operator="on-or-before" value="' + fromdate + '" />',
                                    '        <condition attribute="enddate" operator="on-or-after" value="' + todate + '" />',
                                    '        <condition attribute="statecode" operator="eq" value="0" />',
                                    '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                                    '     </filter>',
                                    '     <link-entity name="bsd_pricegroups" from="bsd_pricegroupsid" to="bsd_pricegroups" alias="ai">',
                                    '       <link-entity name="account" from="bsd_pricegroups" to="bsd_pricegroupsid" alias="aj">',
                                    '         <filter type="and">',
                                    '           <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                                    '         </filter>',
                                    '       </link-entity>',
                                    '     </link-entity>',
                                    '    </entity>',
                                    '</fetch>'].join('');
                CrmFetchKit.Fetch(xml, false).then(function (rspricegroup) {
                    if (rspricegroup.length > 0) {

                        first = rspricegroup[0];
                        if (getValue("bsd_pricelist") == null) {
                            setValue("bsd_pricelist",
                                 [{
                                     id: first.Id,
                                     name: first.attributes.name.value,
                                     entityType: first.logicalName
                                 }]);
                        }
                    }
                    else /*All*/ {
                        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                         ' <entity name="account">',
                                         '   <attribute name="accountid" />',
                                         '   <attribute name="bsd_accounttype" />',
                                         '   <order attribute="bsd_accounttype" descending="false" />',
                                         '   <filter type="and">',
                                         '     <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                                         '     <condition attribute="statecode" operator="eq" value="0" />',
                                         '   </filter>',
                                         ' </entity>',
                                        '</fetch>'].join('');
                        
                        CrmFetchKit.Fetch(xml, false).then(function (rsaccount) {
                            var typeaccount = rsaccount[0].attributes.bsd_accounttype.value;
                            var fetchxml = "";
                            if (typeaccount == 861450000 || typeaccount == 861450001) {
                                fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                          '    <entity name="pricelevel">',
                                          '        <all-attributes />',
                                          '      <order attribute="begindate" descending="false" />',
                                          '      <filter type="and">',
                                          '        <condition attribute="statecode" operator="eq" value="0" />',
                                          '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                                          '        <condition attribute="begindate" operator="on-or-before" value="' + fromdate + '" />',
                                          '        <condition attribute="enddate" operator="on-or-after" value="' + todate + '" />',
                                          '        <condition attribute="bsd_typeprice" operator="eq" value="2" />',
                                          '        <condition attribute="bsd_type" operator="eq" value="861450000" />',
                                          '     </filter>',
                                          '   </entity>',
                                          '</fetch>'].join('');
                            }
                            else if (typeaccount == 100000000) {
                                fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                          '    <entity name="pricelevel">',
                                          '        <all-attributes />',
                                          '      <order attribute="begindate" descending="false" />',
                                          '      <filter type="and">',
                                          '        <condition attribute="statecode" operator="eq" value="0" />',
                                          '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                                          '        <condition attribute="begindate" operator="on-or-before" value="' + fromdate + '" />',
                                          '        <condition attribute="enddate" operator="on-or-after" value="' + todate + '" />',
                                          '        <condition attribute="bsd_typeprice" operator="eq" value="2" />',
                                          '        <condition attribute="bsd_type" operator="eq" value="861450001" />',
                                          '     </filter>',
                                          '   </entity>',
                                          '</fetch>'].join('');

                            } else {
                                fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                          '    <entity name="pricelevel">',
                                          '        <all-attributes />',
                                          '      <order attribute="begindate" descending="false" />',
                                          '      <filter type="and">',
                                          '        <condition attribute="statecode" operator="eq" value="0" />',
                                          '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                                          '        <condition attribute="begindate" operator="on-or-before" value="' + fromdate + '" />',
                                          '        <condition attribute="enddate" operator="on-or-after" value="' + todate + '" />',
                                          '        <condition attribute="bsd_typeprice" operator="eq" value="2" />',
                                          '     </filter>',
                                          '   </entity>',
                                          '</fetch>'].join('');
                            }
                            CrmFetchKit.Fetch(fetchxml, false).then(function (rsAll) {
                                if (rsAll.length > 0) {
                                    first = rsAll[0];
                                    if (getValue("bsd_pricelist") == null) {
                                        setValue("bsd_pricelist",
                                             [{
                                                 id: first.Id,
                                                 name: first.attributes.name.value,
                                                 entityType: first.logicalName
                                             }]);
                                    }
                                } else {
                                    setNotification("bsd_pricelist", "Account không có price list hoặc price list đã hết hạn");
                                }
                            });
                        });
                        
                        
                    }
                });
                
            }
        });
    }
    else if (reset != false) {
        clear_priceleve();
    }
}

//lấy pricelist type all
function getPriceList_all(fromdate, todate, currency) {
    clearNotification("bsd_pricelist");
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                           '   <entity name="pricelevel">',
                                           '     <attribute name="name" />',
                                           '     <attribute name="transactioncurrencyid" />',
                                           '     <attribute name="enddate" />',
                                           '     <attribute name="begindate" />',
                                           '     <attribute name="statecode" />',
                                           '     <attribute name="pricelevelid" />',
                                           '     <order attribute="begindate" descending="true" />',
                                           '     <filter type="and">',
                                           '       <condition attribute="bsd_pricelisttype" operator="eq" value="100000000" />',
                                           '       <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency + '" />',
                                           '       <condition attribute="begindate" operator="on-or-before" value="' + fromdate + '" />',
                                           '       <condition attribute="enddate" operator="on-or-after" value="' + todate + '" />',
                                           '       <condition attribute="statecode" operator="eq" value="0" />',
                                           '     </filter>',
                                           '   </entity>',
                                           ' </fetch>'].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            setValue("bsd_pricelist", [{
                id: rs[0].Id,
                name: rs[0].attributes.name.value,
                entityType: rs[0].logicalName
            }]);
        } else {
            setNotification("bsd_pricelist", "Account không có Price List, hoặc Price List đã hết hạn.");
        }
    });
}

function invoiceaccount_change(reset) {
    if (reset != false) setNull(["bsd_invoiceaccount", "bsd_addressinvoiceaccount", "bsd_contactinvoiceaccount"]);

    var invoiceaccount = getValue("bsd_invoicenameaccount");
    if (invoiceaccount != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="account">',
                   '     <attribute name="name" />',
                   '     <attribute name="primarycontactid" />',
                   '     <attribute name="telephone1" />',
                   '     <attribute name="accountid" />',
                   '     <attribute name="accountnumber" />',
                   '     <attribute name="bsd_taxregistration" />',
                   '     <order attribute="name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="accountid" operator="eq" uitype="account" value="' + invoiceaccount[0].id + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && reset != false) {

                setValue("bsd_invoiceaccount", rs[0].getValue("accountnumber"));
            }
        });
    }
    load_invoiceaddress(reset);
    load_contact_invoice(reset);
}

function load_invoiceaddress(reset) {
    var invoiceaccount = getValue("bsd_invoicenameaccount");
    getControl("bsd_addressinvoiceaccount").removePreSearch(presearch_addressinvoiceaccount);
    if (invoiceaccount != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                          '    <entity name="bsd_address">',
                          '      <attribute name="bsd_name" />',
                          '      <attribute name="bsd_purpose" />',
                          '      <attribute name="bsd_account" />',
                          '      <attribute name="bsd_addressid" />',
                          '      <order attribute="bsd_name" descending="false" />',
                          '      <filter type="and">',
                          '        <condition attribute="bsd_account" operator="eq" uitype="account" value="' + invoiceaccount[0].id + '" />',
                          '      </filter>',
                          '    </entity>',
                          '  </fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>";
        getControl("bsd_addressinvoiceaccount").addCustomView(getControl("bsd_addressinvoiceaccount").getDefaultView(), "bsd_address", "Address", xml, layoutXml, true);
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            setDisabled("bsd_addressinvoiceaccount", false);
            if (rs.length > 0 && reset != false) {

                setValue("bsd_addressinvoiceaccount", [{
                    id: rs[0].Id,
                    name: rs[0].attributes.bsd_name.value,
                    entityType: rs[0].logicalName
                }])
            }
        });

    } else if (reset != false) {
        //clear_addressinvoiceaccount();
        setDisabled("bsd_addressinvoiceaccount", true);
    }
}

function load_paymentterm_date(reset) {
    if (reset != false) setNull(["bsd_paymentterm", "bsd_datept"]);

    var customeraccount = getValue("bsd_potentialcustomer");
    if (customeraccount != null && reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="account">',
                   '     <attribute name="name" />',
                   '     <attribute name="primarycontactid" />',
                   '     <attribute name="telephone1" />',
                   '     <attribute name="accountid" />',
                   '     <attribute name="bsd_paymentterm" />',
                   '     <order attribute="name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="accountid" operator="eq" uitype="account" value="' + customeraccount[0].id + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");

        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                setValue("bsd_paymentterm", [{
                    id: rs[0].attributes.bsd_paymentterm.guid,
                    name: rs[0].attributes.bsd_paymentterm.name,
                    entityType: rs[0].attributes.bsd_paymentterm.logicalName
                }]);

                var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                           '   <entity name="bsd_paymentterm">',
                           '     <attribute name="bsd_paymenttermid" />',
                           '     <attribute name="bsd_name" />',
                           '     <attribute name="createdon" />',
                           '     <attribute name="bsd_date" />',
                           '     <order attribute="bsd_name" descending="false" />',
                           '     <filter type="and">',
                           '       <condition attribute="bsd_termofpayment" operator="eq" value="' + rs[0].attributes.bsd_paymentterm.name + '" />',
                           '     </filter>',
                           '   </entity>',
                           ' </fetch>'].join("");
                CrmFetchKit.Fetch(xml, false).then(function (rs) {
                    if (rs.length > 0) {
                        setValue("bsd_datept", rs[0].getValue("bsd_date"));
                    }
                });
                
            }
        });
        

    }
    //load_duedate(reset);
}

function load_paymentmethod(reset) {
    if (reset != false) setNull("bsd_paymentmethod");

    var customeraccount = getValue("bsd_potentialcustomer");
    if (customeraccount != null && reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="account">',
                   '     <attribute name="name" />',
                   '     <attribute name="primarycontactid" />',
                   '     <attribute name="telephone1" />',
                   '     <attribute name="accountid" />',
                   '     <attribute name="bsd_paymentmethod" />',
                   '     <order attribute="name" descending="false" />',
                   '     <filter type="and">',
                  '        <condition attribute="accountid" operator="eq" uitype="account" value="' + customeraccount[0].id + '" />',
                  '      </filter>',
                  '    </entity>',
                  '  </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                setValue("bsd_paymentmethod", [{
                    id: rs[0].attributes.bsd_paymentmethod.guid,
                    name: rs[0].attributes.bsd_paymentmethod.name,
                    entityType: rs[0].attributes.bsd_paymentmethod.logicalName
                }]);
            }
        });
    }
}

function date_change(reset) {
    load_duedate(reset);
}

//huy -8h20h00  14/3/2017
function load_unitdefault(reset) {

    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="uom">',
        '<attribute name="name" />',
        '<filter type="and">',
          '<condition attribute="uomscheduleid" operator="eq" uitype="uomschedule" value="{A8C3859A-0095-E611-80CC-000C294C7A2D}" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' object='1' jump='uomid' select='1' icon='0' preview='0'>  " +
                     "<row name='result'  " + "id='uomid'><cell name='name'   " + "width='200' /> </row></grid>";
    getControl("bsd_unitdefault").addCustomView(getDefaultView("bsd_unitdefault"), "uom", "uom", xml, layoutXml, true);

    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs[0].getValue("bsd_unitdefault") != null) {
                setValue("bsd_unitdefault", [{
                    id: rs[0].attributes.bsd_unitdefault.guid,
                    name: rs[0].attributes.bsd_unitdefault.name,
                    entityType: rs[0].attributes.bsd_unitdefault.logicalName
                }]);
            }
        });

    }
}

//huy
function load_currency(reset) {
    var account = getValue("bsd_potentialcustomer");
    if (account != null && reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="account">',
                   '     <attribute name="name" />',
                   '     <attribute name="primarycontactid" />',
                   '     <attribute name="telephone1" />',
                   '     <attribute name="accountid" />',
                   '     <attribute name="transactioncurrencyid" />',
                   '     <order attribute="name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="accountid" operator="eq" uitype="account" value="' + account[0].id + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].getValue("transactioncurrencyid") != null) {
                    setValue("transactioncurrencyid", [{
                        id: rs[0].attributes.transactioncurrencyid.guid,
                        name: rs[0].attributes.transactioncurrencyid.name,
                        entityType: rs[0].attributes.transactioncurrencyid.logicalName
                    }]);
                    check_currency(reset);
                }
            }
        });
    }
}

//huy
function load_currencydefault(reset) {
    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs[0].getValue("bsd_currencydefault") != null) {
                setValue("bsd_currencydefault", [{
                    id: rs[0].attributes.bsd_currencydefault.guid,
                    name: rs[0].attributes.bsd_currencydefault.name,
                    entityType: rs[0].attributes.bsd_currencydefault.logicalName
                }]);
                check_currency(reset);
            }
        });

    }
}

//huy
function load_bankdefault(reset) {
    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            setValue("bsd_bank", [{
                id: rs[0].attributes.bsd_bankdefault.guid,
                name: rs[0].attributes.bsd_bankdefault.name,
                entityType: rs[0].attributes.bsd_bankdefault.logicalName,
            }]);
        });
    }

}

//huy
function load_shippingpricelist_ton(reset) {
    if (reset != false) {

        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("bsd_potentialcustomer");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var effective_from = getValue("bsd_fromdate");
        var effective_to = getValue("bsd_todate");

        var date_form = getFullDay("bsd_fromdate");
        var date_to = getFullDay("bsd_todate");
        var data = null;
        if (request_porter == true) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                       '   <entity name="bsd_shippingpricelist">',
                       '     <attribute name="bsd_shippingpricelistid" />',
                       '     <attribute name="bsd_name" />',
                       '     <attribute name="createdon" />',
                       '     <attribute name="bsd_priceunitporter" />',
                       '     <attribute name="bsd_priceofton" />',
                       '     <attribute name="bsd_deliverymethod" />',
                       '     <order attribute="bsd_priceunitporter" descending="true" />',
                       '     <order attribute="bsd_priceofton" descending="true" />',
                       '     <filter type="and">',
                       '       <condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                       '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                       '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                       '       <condition attribute="statecode" operator="eq" value="0" />',
                       '       <condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                       '     </filter>',
                       '     <link-entity name="bsd_zone" from="bsd_zoneid" to="bsd_zone" alias="ao">',
                       '       <link-entity name="bsd_zoneaddress" from="bsd_zone" to="bsd_zoneid" alias="ap">',
                       '         <filter type="and">',
                       '           <condition attribute="bsd_address" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
                       '         </filter>',
                       '       </link-entity>',
                       '     </link-entity>',
                       '   </entity>',
                       ' </fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    if (first.getValue("bsd_priceunitporter") != null) {
                        console.log(first);
                        //shipping price list có price unit porter
                        data = {
                            pricelist_id: first.getValue("bsd_shippingpricelistid"),
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_priceunitporter.value,
                            porter: true
                        };
                    } else {
                        data = {
                            pricelist_id: first.getValue("bsd_shippingpricelistid"),
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_priceofton.value,
                            porter: false
                        };
                    }
                }
            });
           
        } else if (request_porter == false) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                       '   <entity name="bsd_shippingpricelist">',
                       '     <attribute name="bsd_shippingpricelistid" />',
                       '     <attribute name="bsd_name" />',
                       '     <attribute name="createdon" />',
                       '     <attribute name="bsd_priceunitporter" />',
                       '     <attribute name="bsd_priceofton" />',
                       '     <attribute name="bsd_deliverymethod" />',
                       '     <order attribute="bsd_priceofton" descending="true" />',
                       '     <filter type="and">',
                       '       <condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                       '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                       '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                       '       <condition attribute="statecode" operator="eq" value="0" />',
                       '       <condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                       '     </filter>',
                       '     <link-entity name="bsd_zone" from="bsd_zoneid" to="bsd_zone" alias="ao">',
                       '       <link-entity name="bsd_zoneaddress" from="bsd_zone" to="bsd_zoneid" alias="ap">',
                       '         <filter type="and">',
                       '           <condition attribute="bsd_address" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
                       '         </filter>',
                       '       </link-entity>',
                       '     </link-entity>',
                       '   </entity>',
                       ' </fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    if (first.getValue("bsd_priceofton") != null) {
                        data = {
                            pricelist_id: first.Id,
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_priceofton.value,
                            porter: false
                        };
                    }
                }
            });
            

        }

        if (data != null) {
            set_shipping_pricelist(data);
            clearNotification("bsd_shippingpricelistname");
        } else {
            set_shipping_pricelist(null);
        }

    } else {
        porteroption_change(reset);
    }
}

//huy
function truckload_change(reset) {
    // load shipping theo trip
    if (reset != false) {
        setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
    }

    var truckload = getValue("bsd_truckload");
    if (truckload != null && reset != false) {
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("bsd_potentialcustomer");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var effective_from = getValue("bsd_fromdate");
        var effective_to = getValue("bsd_todate");

        var date_form = getFullDay("bsd_fromdate");
        var date_to = getFullDay("bsd_todate");
        var data = null;

        if (request_porter == true) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                   '   <entity name="bsd_shippingpricelist">',
                   '     <attribute name="bsd_shippingpricelistid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_pricetripporter" />',
                   '     <attribute name="bsd_priceoftrip" />',
                   '     <attribute name="bsd_deliverymethod" />',
                   '     <order attribute="bsd_pricetripporter" descending="true" />',
                   '     <order attribute="bsd_priceoftrip" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                    '      <condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                    '    </filter>',
                    '    <link-entity name="bsd_zone" from="bsd_zoneid" to="bsd_zone" alias="ao">',
                    '      <link-entity name="bsd_zoneaddress" from="bsd_zone" to="bsd_zoneid" alias="ap">',
                    '        <filter type="and">',
                    '          <condition attribute="bsd_address" operator="eq" value="' + shiptoaddress[0].id + '" />',
                    '        </filter>',
                    '      </link-entity>',
                    '    </link-entity>',
                    '  </entity>',
                    '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    if (first.getValue("bsd_pricetripporter") != null) {
                        console.log(first);
                        //shipping price list có price unit porter
                        data = {
                            pricelist_id: first.getValue("bsd_shippingpricelistid"),
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_pricetripporter.value,
                            porter: true
                        };
                    } else {
                        data = {
                            pricelist_id: first.getValue("bsd_shippingpricelistid"),
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_priceoftrip.value,
                            porter: false
                        };
                    }
                }
            });
           
        } else if (request_porter == false) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                   '   <entity name="bsd_shippingpricelist">',
                   '     <attribute name="bsd_shippingpricelistid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_pricetripporter" />',
                   '     <attribute name="bsd_priceoftrip" />',
                   '     <attribute name="bsd_deliverymethod" />',
                   '     <order attribute="bsd_priceoftrip" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                    '      <condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                    '    </filter>',
                    '    <link-entity name="bsd_zone" from="bsd_zoneid" to="bsd_zone" alias="ao">',
                    '      <link-entity name="bsd_zoneaddress" from="bsd_zone" to="bsd_zoneid" alias="ap">',
                    '        <filter type="and">',
                    '          <condition attribute="bsd_address" operator="eq" value="' + shiptoaddress[0].id + '" />',
                    '        </filter>',
                    '      </link-entity>',
                    '    </link-entity>',
                    '  </entity>',
                    '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    if (first.getValue("bsd_priceoftrip") != null) {
                        data = {
                            pricelist_id: first.Id,
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_priceoftrip.value,
                            porter: false
                        };
                    }
                }
            });
           
        }
        if (data != null) {
            set_shipping_pricelist(data);
            clearNotification("bsd_shippingpricelistname");
        } else {
            set_shipping_pricelist(null);
        }
    } else {
        porteroption_change(reset);
    }
}

//huy
function load_truckload(reset) {
    var shiptoaddress = getValue("bsd_shiptoaddress");
    var warehouse_address = getValue("bsd_warehouseaddress");

    if (shiptoaddress != null && warehouse_address != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                   '   <entity name="bsd_truckload">',
                   '     <attribute name="bsd_truckloadid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '     <link-entity name="bsd_shippingpricelist" from="bsd_truckload" to="bsd_truckloadid" alias="bv">',
                   '       <filter type="and">',
                   '         <condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                   '         <condition attribute="bsd_addressfrom" operator="eq"  uitype="bsd_address" value="' + warehouse_address[0].id + '" />',
                   '       </filter>',
                   '       <link-entity name="bsd_zone" from="bsd_zoneid" to="bsd_zone" alias="bw">',
                   '         <link-entity name="bsd_zoneaddress" from="bsd_zone" to="bsd_zoneid" alias="bx">',
                   '           <filter type="and">',
                   '             <condition attribute="bsd_address" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
                   '           </filter>',
                   '         </link-entity>',
                   '       </link-entity>',
                   '     </link-entity>',
                   '   </entity>',
                   ' </fetch>'].join("");
        var layoutXml = "<grid name='resultset' object='1' jump='bsd_truckloadid' select='1' icon='0' preview='0'>  " +
                        "<row name='result' id='bsd_truckloadid'> <cell name='bsd_name'   " + "width='200' /></row></grid>";
        getControl("bsd_truckload").addCustomView(getDefaultView("bsd_truckload"), "bsd_truckload", "bsd_truckload", xml, layoutXml, true);
    }
}

function load_duedate(reset) {
    if (reset != false) setNull("bsd_duedate");
    var date = getValue("bsd_date");
    var customeraccount = getValue("bsd_potentialcustomer");
    if (customeraccount != null && reset != false && date != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="account">',
                   '     <attribute name="name" />',
                   '     <attribute name="primarycontactid" />',
                   '     <attribute name="telephone1" />',
                   '     <attribute name="accountid" />',
                   '     <attribute name="bsd_paymentday" />',
                   '     <order attribute="name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="accountid" operator="eq" uitype="account" value="' + customeraccount[0].id + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");

        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                console.log(rs[0]);

                var date_paymentterm = getValue("bsd_datept")
                var paymentday = rs[0].getValue("bsd_paymentday");
                var duedate = new Date();
                date = new Date(date);
                if (paymentday != null) {
                    if ((date.addDays(date_paymentterm)).getDate() > paymentday) {
                        duedate = ((date.addDays(date_paymentterm)).getMonth() + 2) + "/1/" + (date.addDays(date_paymentterm)).getFullYear();
                        duedate = (new Date(duedate)).addDays(paymentday - 1);
                    } else {
                        duedate = ((date.addDays(date_paymentterm)).getMonth() + 1) + "/1/" + (date.addDays(date_paymentterm)).getFullYear();
                        duedate = (new Date(duedate)).addDays(paymentday - 1);
                    }
                }
                else {
                    duedate = (new Date(date)).addDays(date_paymentterm);
                }


                //nếu là ngày thứ 7, chủ nhật thì chuyển sang ngày thứ 2
                if ((new Date(duedate)).getDay() == 6) {//saturday
                    duedate = (new Date(duedate)).addDays(2);
                } else if ((new Date(duedate)).getDay() == 0) {//sunday
                    duedate = (new Date(duedate)).addDays(1);
                }
                setValue("bsd_duedate", new Date(duedate));
            }
        });
        
    }
}


function check_enable_shipping(reset) {
    // call when customername or warehouse or address change.
    var customername = getValue("bsd_potentialcustomer");
    var warehousefrom = getValue("bsd_warehousefrom");
    var warehouseaddress = getValue("bsd_warehouseaddress");
    var shiptoaddress = getValue("bsd_shiptoaddress");
    var effectivefrom = getValue("bsd_fromdate");
    var effectiveto = getValue("bsd_todate");
    if (customername != null && warehousefrom != null && shiptoaddress != null && warehouseaddress != null && effectivefrom != null && effectiveto != null) {
        setDisabled("bsd_transportation", false);
    } else {
        setValue("bsd_transportation", false);
        setDisabled("bsd_transportation", true);
    }
    shipping_change(reset);
}

function shipping_deliverymethod_change(reset) {

    if (reset != false) setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"]);
    var method;
    if (getValue("bsd_shippingdeliverymethod") == null) {
        setValue("bsd_shippingdeliverymethod", 861450000);
        method = 861450000;
    } else {
        method = getValue("bsd_shippingdeliverymethod");
    }

    if (method == 861450000) { // ton
        setVisible("bsd_unitshipping", true);
        setRequired("bsd_unitshipping", "required");
        if (getValue("bsd_unitshipping") == null) {
            setValue("bsd_unitshipping", [{
                id: "{7E3545D7-0AA0-E611-93F6-000C29EBC66A}",
                name: "Tấn",
                entityType: "uom"
            }]);
        }


        setVisible("bsd_truckload", false);
        setNull("bsd_truckload");
        setRequired("bsd_truckload", "none");

        load_shippingpricelist_ton(reset);

    } else if (method == 861450001) { // trip

        setVisible("bsd_unitshipping", false);
        setNull("bsd_unitshipping");
        setRequired("bsd_unitshipping", "none");

        setVisible("bsd_truckload", true);
        setRequired("bsd_truckload", "required");

        if (reset != false) {
            setNull("bsd_truckload");
        }
        load_truckload(reset);
        truckload_change(reset);
    }
}

function set_shipping_pricelist(data) {
    if (data != null) {
        setValue("bsd_shippingpricelistname", [{
            id: data.pricelist_id,
            name: data.pricelist_name,
            entityType: "bsd_shippingpricelist"
        }]);
        setValue("bsd_priceoftransportationn", data.price);
        if (data.porter == true) {
            setValue("bsd_shippingporter", true);
            setValue("bsd_porter", true);
        } else {
            setValue("bsd_shippingporter", false);
        }
        if (getValue("bsd_shippingpricelistname") == null) {
            setTimeout(function () {
                set_shipping_pricelist(data);
            }, 50);
        } else {
            porteroption_change(true);
        }
    } else {
        setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
        porteroption_change(true);
    }
}

function shipping_pricelist_change(reset) {
    // code
    if (reset != false) setNull("bsd_priceoftransportationn");
    var pricelist = getValue("bsd_shippingpricelistname");
    if (pricelist != null) {

    } else if (reset != false) {

    }
    shipping_price_change();
}

function shipping_price_change() {
    // validate price
    var price = getValue("bsd_priceoftransportationn");
    if (price == null) {
        //setNotification("bsd_priceoftransportationn", "You must provide a value for Price");
    } else if (price <= 0) {
        setNotification("bsd_priceoftransportationn", "Enter a value from 0");
    } else {
        clearNotification("bsd_priceoftransportationn");
    }
}

function shipping_change(reset) {
    clearNotification("bsd_shippingpricelistname");
    if (reset != false) {
        setNull(["bsd_truckload", "bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
    }

    var bsd_transportation = getValue("bsd_transportation");
    if (bsd_transportation) { // có ship
        setRequired(["bsd_shiptoaccount", "bsd_shiptoaddress"], "required");
        setVisible(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], true);
        setRequired(["bsd_shippingdeliverymethod", "bsd_shippingpricelistname", "bsd_priceoftransportationn"], "required");
        shipping_deliverymethod_change(reset);
    } else {
        setRequired(["bsd_shiptoaccount", "bsd_shiptoaddress"], "none");
        setVisible(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], false);
        setRequired(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], "none");

        if (reset != false) {
            setValue("bsd_shippingporter", false);
            porteroption_change(reset);
        }
    }
}

function porteroption_change(reset) {
    //if (reset != false) setNull(["bsd_priceofporter", "bsd_pricepotter"]);
    var request_porter = getValue("bsd_requestporter");
    var porteroption = getValue("bsd_porteroption");
    var shipping_porter = getValue("bsd_shippingporter");
    if (request_porter == false) {
        // Không yêu cầu !
        setDisabled(["bsd_porteroption"], true);

        setVisible(["bsd_priceofporter", "bsd_pricepotter"], false);
        setRequired(["bsd_priceofporter", "bsd_pricepotter"], "none");
        if (reset != false) {
            setValue("bsd_porteroption", false);
            setValue("bsd_porter", 861450001);
            setNull(["bsd_priceofporter", "bsd_pricepotter"]);
        }

    } else if (request_porter == true) { // Có yêu cầu
        if (shipping_porter == true) { // Giá đã gồm Porter
            setDisabled(["bsd_porteroption"], true);
            setVisible(["bsd_priceofporter", "bsd_pricepotter"], false);
            setRequired(["bsd_priceofporter", "bsd_pricepotter"], "none");
            if (reset != false) {
                setValue("bsd_porteroption", true);
                setValue("bsd_porter", 861450000);
                setNull(["bsd_priceofporter", "bsd_pricepotter"]);
            }
        } else { // Không có giá  porter
            setDisabled(["bsd_porteroption"], true);
            setVisible(["bsd_priceofporter", "bsd_pricepotter"], true);
            setRequired(["bsd_priceofporter", "bsd_pricepotter"], "required");
            if (reset != false) {
                setValue("bsd_porteroption", true);
                setValue("bsd_porter", 861450000);
            }
            load_porter();
            porterprice_change(reset);
        }
    }
}

function porterprice_change(reset) {
    if (reset != false) {
        var bsd_priceofporter = getValue("bsd_priceofporter");
        if (bsd_priceofporter != null) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_porter">',
                '<attribute name="bsd_porterid" />',
                '<attribute name="bsd_price" />',
                '<order attribute="bsd_price" descending="false" />',
                '<filter type="and">',
                  '<condition attribute="bsd_porterid" operator="eq" uitype="bsd_porter" value="' + bsd_priceofporter[0].id + '" />',
                  '<condition attribute="bsd_price" operator="not-null" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    setValue("bsd_pricepotter", first.getValue("bsd_price"));
                }
            });
        } else {
            setNull("bsd_pricepotter");
        }
    }
}

function requestporter_change(reset) {
    shipping_change(reset);
}

function load_porter() {
    var effective_to = getValue("bsd_todate");
    var effective_from = getValue("bsd_fromdate");
    getControl("bsd_priceofporter").removePreSearch(presearch_priceofporter);
    if (effective_from != null && effective_to != null) {
        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();

        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_porter">',
                   '     <attribute name="bsd_porterid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_effectiveto" />',
                   '     <attribute name="bsd_effectivefrom" />',
                   '     <attribute name="bsd_unit" />',
                   '     <attribute name="bsd_price" />',
                   '     <order attribute="bsd_effectiveto" descending="true" />',
                   '     <order attribute="createdon" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");

        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_porterid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                           "<row name='result'  " + "id='bsd_porterid'>  " +
                                           "<cell name='bsd_name'   " + "width='200' />  " +
                                           "</row>" +
                                        "</grid>";
        getControl("bsd_priceofporter").addCustomView(getDefaultView("bsd_priceofporter"), "bsd_porter", "bsd_porter", xml, layoutXml, true);
        var priceofporter = getValue("bsd_priceofporter");
        if (priceofporter != null) {
            var xml2 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_porter">',
                   '     <attribute name="bsd_porterid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_effectiveto" />',
                   '     <attribute name="bsd_effectivefrom" />',
                   '     <attribute name="bsd_unit" />',
                   '     <attribute name="bsd_price" />',
                   '     <order attribute="bsd_effectiveto" descending="true" />',
                   '     <order attribute="createdon" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="bsd_porterid" operator="eq" uitype="bsd_porter" value="' + priceofporter[0].id + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join('');
            CrmFetchKit.Fetch(xml2, false).then(function (rs) {
                if (rs.length > 0) {

                } else {

                    setNull(["bsd_priceofporter", "bsd_pricepotter"]);
                }
            });
        }
    } else {
        clear_priceofporter();
        setNull(["bsd_priceofporter", "bsd_pricepotter"]);
    }
}

function check_date() {
    check_enable_shipping(true);
    check_fromdate_todate();
    load_pricelist();
}

function check_fromdate_todate() {
    clearNotification("bsd_fromdate");
    var from = getValue("bsd_fromdate");
    var to = getValue("bsd_todate");
    if (from != null && to != null) {
        if (from > to) {
            setNotification("bsd_fromdate", "The From Date cannot occur before the To Date");
        }
    }
    check_requestshipdate();
}

function check_requestshipdate() {
    clearNotification("bsd_requestedshipdate");
    var requestshipdate = getValue("bsd_requestedshipdate");
    var fromdate = getValue("bsd_fromdate");
    if (fromdate != null && requestshipdate != null) {
        if (requestshipdate < fromdate) {
            setNotification("bsd_requestedshipdate", "The request ship date cannot occur before the From date");
        }
    }
}

function load_sale_tax_group() {
    var customer = getValue("bsd_potentialcustomer");
    if (customer != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="account">',
           ' <attribute name="name" />',
           ' <attribute name="bsd_saletaxgroup" />',
           ' <filter type="and">',
            '  <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
           ' </filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && rs[0].getValue("bsd_saletaxgroup") != null) {
                setValue("bsd_saletaxgroup", [{
                    id: rs[0].getValue("bsd_saletaxgroup"),
                    name: rs[0].attributes.bsd_saletaxgroup.name,
                    entityType: rs[0].attributes.bsd_saletaxgroup.logicalName
                }]);
            }
        });
        
    } else {
        setNull("bsd_saletaxgroup");
    }
}
//end code update

function load_exchangerate(reset) {
}


function bank_change(reset) {
    if (reset != false) setNull("bsd_exchangerate");
    getControl("bsd_exchangerate").removePreSearch(presearch_exchangerate);

    var bank = getValue("bsd_bank");
    if (bank != null && getValue("transactioncurrencyid") != null) {
        var fromcurrencyid = getValue("transactioncurrencyid")[0].id;
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var date = date.getDate();
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
        '  <entity name="bsd_exchangerate">',
        '    <attribute name="bsd_exchangerateid" />',
        '    <attribute name="bsd_name" />',
        '    <attribute name="bsd_date" />',
        '    <order attribute="bsd_date" descending="true" />',
        '    <filter type="and">',
        '      <condition attribute="bsd_currencyfrom" operator="eq" uitype="transactioncurrency" value="' + fromcurrencyid + '" />',
        '      <condition attribute="bsd_currencyto" operator="eq" uiname="Đồng" uitype="transactioncurrency" value="{B4544CAB-0195-E611-80CC-000C294C7A2D}" />',
        '      <condition attribute="bsd_bankaccount" operator="eq" uitype="bsd_bankgroup" value="' + bank[0].id + '" />',
        '      <condition attribute="bsd_date" operator="on-or-before" value="' + year + '-' + month + '-' + date + '" />',
        '    </filter>',
        '  </entity>',
        '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_exchangerateid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                            "<row name='result'  " + "id='bsd_exchangerateid'>  " +
                                            "<cell name='bsd_name' width='200' />  " +
                                            "<cell name='bsd_date' width='150' />  " +
                                            "</row>   " +
                                         "</grid>   ";
        getControl("bsd_exchangerate").addCustomView(getDefaultView("bsd_exchangerate"), "bsd_exchangerate", "bsd_exchangerate", xml, layoutXml, true);

        if (reset != false) {
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    setValue("bsd_exchangerate", [{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: "bsd_exchangerate"
                    }])
                    exchangerate_change(reset);
                }
            });
           
        }
    } else if (reset != false) {
        clear_exchangerate();
    }
    //exchangerate_change(reset);
}

function exchangerate_change(reset) {

    if (reset != false) setValue("bsd_exchangeratevalue", 1);
    var exchangerate = getValue("bsd_exchangerate");
    if (exchangerate != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
        '  <entity name="bsd_exchangerate">',
        '    <attribute name="bsd_exchangerateid" />',
        '    <attribute name="bsd_exchangerate" />',
        '    <filter type="and">',
        '      <condition attribute="bsd_exchangerateid" operator="eq" uitype="bsd_exchangerate" value="' + exchangerate[0].id + '" />',
        '    </filter>',
        '  </entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {

                setValue("bsd_exchangeratevalue", rs[0].attributes.bsd_exchangerate.value);
            }
        });
    }
}

function filter_orderdeliverystatus_grid() {
    var objSubGrid = window.parent.document.getElementById("orderdelierystatusSubgrid");
    if (objSubGrid != null && objSubGrid.control != null) {
        var xml = [
           '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
             '<entity name="bsd_orderdeliverystatus">',
               '<attribute name="bsd_orderdeliverystatusid" />',
               '<attribute name="bsd_name" />',
               '<attribute name="bsd_productid" />',
               '<attribute name="createdon" />',
               '<attribute name="statecode" />',
               '<attribute name="bsd_shippedquantity" />',
               '<attribute name="bsd_remainingquantity" />',
               '<attribute name="bsd_product" />',
               '<attribute name="bsd_orderquantity" />',
               '<attribute name="bsd_order" />',
               '<order attribute="bsd_name" descending="false" />',
               '<filter type="and">',
                 '<condition attribute="bsd_order" operator="eq" uitype="salesorder" value="' + getValue("bsd_order")[0].id + '" />',
               '</filter>',
             '</entity>',
           '</fetch>'];
        objSubGrid.control.SetParameter("fetchXML", xml);
        objSubGrid.control.Refresh();
    } else {
        setTimeout('filter_orderdeliverystatus_grid()', 1000);
    }
}

function filter_quotedeliverystatus_grid() {
    var objSubGrid = window.parent.document.getElementById("quotedelierystatusSubgrid");
    if (objSubGrid != null && objSubGrid.control != null) {
        var xml = [
               '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
             '<entity name="bsd_orderdeliverystatus">',
               '<attribute name="bsd_orderdeliverystatusid" />',
               '<attribute name="bsd_name" />',
               '<attribute name="bsd_productid" />',
               '<attribute name="createdon" />',
               '<attribute name="statecode" />',
               '<attribute name="bsd_shippedquantity" />',
               '<attribute name="bsd_remainingquantity" />',
               '<attribute name="bsd_product" />',
               '<attribute name="bsd_orderquantity" />',
               '<attribute name="bsd_quote" />',
               '<order attribute="bsd_name" descending="false" />',
               '<filter type="and">',
                 '<condition attribute="bsd_quote" operator="eq" uitype="salesorder" value="' + getValue("bsd_quote")[0].id + '" />',
               '</filter>',
             '</entity>',
           '</fetch>'];
        objSubGrid.control.SetParameter("fetchXML", xml);
        objSubGrid.control.Refresh();
    } else {
        setTimeout('filter_quotedeliverystatus_grid()', 1000);
    }
}

function check_currency(reset) {
    var fromcurrency = getValue("transactioncurrencyid");
    var currency_default = getValue("bsd_currencydefault");
    if (fromcurrency != null && currency_default != null) {
        var currency_default_id = currency_default[0].id.toLowerCase().replace("}", "").replace("{", "");
        var account_currency_id = fromcurrency[0].id.toLowerCase().replace("}", "").replace("{", "");
        if (currency_default_id == account_currency_id) {
            if (reset != false) {
                setValue("bsd_exchangeratevalue", 1);
            }
            setVisible(["bsd_bank", "bsd_exchangerate", "bsd_exchangeratevalue"], false);
        }
        else {

            setVisible(["bsd_bank", "bsd_exchangerate", "bsd_exchangeratevalue"], true);
            load_bankdefault(reset);
            bank_change(reset);
        }
    }

}

function load_contact_invoice(reset) {
    var invoice_account = getValue("bsd_invoicenameaccount");
    getControl("bsd_contactinvoiceaccount").removePreSearch(presearch_contactinvoice);
    if (invoice_account != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="contact">',
                        '<attribute name="fullname" />',
                        '<attribute name="telephone1" />',
                        '<attribute name="contactid" />',
                        '<order attribute="fullname" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_contacttype" operator="eq" value="861450000" />',
                          '<condition attribute="parentcustomerid" operator="eq" uitype="account" value="' + invoice_account[0].id + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                            "<row name='result'  " + "id='contactid'>  " +
                                            "<cell name='fullname'   " + "width='200' />  " +
                                            "</row>" +
                                         "</grid>";
        getControl("bsd_contactinvoiceaccount").addCustomView(getDefaultView("bsd_contactinvoiceaccount"), "contact", "contact", xml, layoutXml, true);
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            setDisabled("bsd_contactinvoiceaccount", false);
            if (rs.length > 0 && reset!=false) {

                setValue("bsd_contactinvoiceaccount", [{
                    id: rs[0].Id,
                    name: rs[0].attributes.fullname.value,
                    entityType: rs[0].logicalName
                }])
            }
        });
    } else if (reset != false) {
        //clear_contactinvoice();
        setDisabled("bsd_contactinvoiceaccount", true);
    }
}

function load_contact_shiptoaccount(reset) {
    var bsd_shiptoaccount = getValue("bsd_shiptoaccount");
    getControl("bsd_contactshiptoaccount").removePreSearch(presearch_contactshiptoaccount);
    if (bsd_shiptoaccount != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="contact">',
        '<attribute name="fullname" />',
        '<attribute name="telephone1" />',
        '<attribute name="contactid" />',
        '<order attribute="fullname" descending="false" />',
        '<filter type="and">',
        '   <condition attribute="bsd_contacttype" operator="eq" value="861450000" />',
          '<condition attribute="parentcustomerid" operator="eq" uitype="account" value="' + bsd_shiptoaccount[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                            "<row name='result'  " + "id='contactid'>  " +
                                            "<cell name='fullname'   " + "width='200' />  " +
                                            "</row>" +
                                         "</grid>";
        getControl("bsd_contactshiptoaccount").addCustomView(getDefaultView("bsd_contactshiptoaccount"), "contact", "contact", xml, layoutXml, true);
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            setDisabled("bsd_contactshiptoaccount", false);
            if (rs.length > 0 && reset!=false) {

                setValue("bsd_contactshiptoaccount", [{
                    id: rs[0].Id,
                    name: rs[0].attributes.fullname.value,
                    entityType: rs[0].logicalName
                }])
            }
        });

    } else if (reset != false) {
        //clear_contactshiptoaccount();
        setDisabled("bsd_contactshiptoaccount", true);
    }
}

function load_creditlimit(reset) {
    if (reset != false) setNull("bsd_creditlimit");
    var customeraccount = getValue("bsd_potentialcustomer");
    if (customeraccount != null && reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="account">',
                   '     <attribute name="name" />',
                   '     <attribute name="accountid" />',
                   '     <attribute name="creditlimit" />',
                   '     <order attribute="name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="accountid" operator="eq" uitype="account" value="' + customeraccount[0].id + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                setValue("bsd_creditlimit", rs[0].getValue("creditlimit"));
            }
        });
    } else if (reset != false) {
        setNull("bsd_creditlimit");
    }
}

function load_congno(reset) {
    debugger;
    load_creditlimit(reset);
    var account = getValue("bsd_potentialcustomer");
    if (account != null && reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_customerdebt">',
                '<attribute name="bsd_customerdebtid" />',
                '<attribute name="bsd_outstandingdebtoverdue" />',
                '<attribute name="bsd_outstandingdebt" />',
                '<attribute name="bsd_cretditlimit" />',
                '<order attribute="createdon" descending="true" />',
                '<filter type="and">',
                  '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + account[0].id + '" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                var customerdebt = rs[0];
                //if (customerdebt.getValue("bsd_cretditlimit") != null) {
                //    setValue("bsd_creditlimit", customerdebt.getValue("bsd_cretditlimit"));
                //}
                var duyet = getValue("bsd_duyet");
                if (duyet == false) {
                    var suborder_amount = getValue("bsd_totalamount");
                    var outstandingdebtoverdue = 0;
                    for (var i = 0; i < rs.length; i++) {
                        outstandingdebtoverdue += rs[i].getValue("bsd_outstandingdebtoverdue");
                    }
                    setVisible(["bsd_olddebt", "bsd_newdebt"], true);
                    setVisible(["bsd_customerdebt"], false);
                    setValue("bsd_olddebt", outstandingdebtoverdue);
                    setValue("bsd_newdebt", outstandingdebtoverdue + suborder_amount);
                    setValue("bsd_customerdebt", outstandingdebtoverdue + suborder_amount);
                } else {
                    setVisible(["bsd_olddebt", "bsd_newdebt"], false);
                    setVisible(["bsd_customerdebt"], true);
                    if (AccountUserID().length > 0) /*nếu là nhà pahn phối*/ {
                        if (AccountUserID()[0].attributes.bsd_accounttype.value == 100000000) {
                            setVisible(["bsd_customerdebt"], false);
                        }
                    }
                }
            } else {
                setValue("bsd_olddebt", 0);
                setValue("bsd_newdebt", 0);
                setValue("bsd_customerdebt", 0);
            }
        });
    }
}

function duyet() {
    var duyet = getValue("bsd_duyet");
    var requestedshipdate = getValue("bsd_requestedshipdate");
    var requestedreceiptdate = getValue("bsd_requestedreceiptdate");
    if (duyet == true && requestedreceiptdate != null && requestedshipdate != null) {
        window.top.$ui.Confirm("Confirm", "Are you sure ?", function (e) {
            ExecuteAction(getId(), Xrm.Page.data.entity.getEntityName(), "bsd_duyetcongno", null, function (result) {
                if (result != null && result.status != null) {
                    if (result.status == "success") {
                       // setDisabled(["bsd_duyet"], true);
                        setValue("bsd_duyet", true);
                        setValue("bsd_sodate", new Date());
                        setVisible(["bsd_customerdebt", "bsd_customerpo", "bsd_sodate","bsd_so"], true);
                        setVisible(["bsd_olddebt", "bsd_newdebt", "bsd_suborderidnpp"], false);

                        Xrm.Page.ui.setFormNotification("Suborder đã duyệt thành công. ", "INFO", "1");
                        setTimeout(function () {
                            Xrm.Page.ui.clearFormNotification('1');
                        }, 10000);
                        DisabledForm();
                        setDisabled("bsd_sodate", false);

                    } else if (result.status == "error") {
                        m_alert(result.data);
                        setValue("bsd_duyet", false);
                        setDisabled("bsd_duyet", false);
                    } else {
                        m_alert(result.data);
                        setValue("bsd_duyet", false);
                        setDisabled("bsd_duyet", false);
                    }
                }
            });
        }, function () {
            setValue("bsd_duyet", false);
        });
    }
    else {
        m_alert("Cannot approve this suborder. Please add requested ship date and requested receipt date.");
        setValue("bsd_duyet", false);
    }
}

function BtnDeliveryPlanClick() {
    Xrm.Page.data.save().then(function () {
        var id = Xrm.Page.data.entity.getId();
        var xml = [
            '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_deliveryplan">',
                '<attribute name="bsd_deliveryplanid" />',
                '<filter type="and">',
                  '<condition attribute="bsd_suborder" operator="eq" uitype="bsd_suborder" value="' + id + '" />',
                '</filter>',
              '</entity>',
            '</fetch>'
        ].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Utility.openEntityForm("bsd_deliveryplan", rs[0].getValue("bsd_deliveryplanid"));
            } else {
                var check = getValue("bsd_duyet");
                if (check) {
                    if (confirm("Are you sure to create the Delivery Plan ?")) {
                        ExecuteAction(id, Xrm.Page.data.entity.getEntityName(), "bsd_CreateDeliveryPlan", null, function (result) {
                            if (result != null && result.status != null) {
                                if (result.status == "success") {
                                    Xrm.Utility.openEntityForm("bsd_deliveryplan", result.data.ReturnId.value);
                                } else if (result.status == "error") {
                                    m_alert(result.data);
                                } else {
                                    m_alert(result.data);
                                }
                            }
                        });
                    }
                } else {
                    m_alert("Duyệt Sub Order trước khi tạo kế hoạch giao hàng ");
                }
            }
        });
    });
}
/*Phong le tạm thời đóng lại*/
//function BtnDeliveryPlanEnableRule() {
//    debugger;
//    var id = Xrm.Page.data.entity.getId();

//    var rnames = Xrm.Page.context.getUserRoles();

//    var statecode = Xrm.Page.getAttribute("statuscode").getValue();
//    var stagename = Xrm.Page.data.process.getActiveStage().getId();
//    //stagename = Xrm.Page.data.process.getActiveStage().getId();
//    var xmlaccount = [];
//    if (id != " ") {
//        if (rnames.length > 1) {
//            for (var i = 0; i < rnames.length; i++) {
//                if (rnames[i] == "376bea81-de94-e611-80cc-000c294c7a2d") {
//                    return true;
//                }
//            }
//        }
//        else {
//            if (stagename == "6cb47dc1-208b-263d-de6b-62d681a4b6e6" || rnames == "376bea81-de94-e611-80cc-000c294c7a2d")
//                return true;
//            else
//                return false;
//        }
//    }
//    if (id == " ") {
//        return false;
//    }
//}
//end 
function BtnCancelSubOrder() {
    var duyet = getValue("bsd_duyet");
    if (duyet == false) {
        if (confirm("Are you sure to cancel suborder ?")) {
            ExecuteAction(getId(), Xrm.Page.data.entity.getEntityName(), "bsd_CancelSubOrder", null, function (result) {
                if (result != null && result.status != null) {
                    if (result.status == "success") {
                        Xrm.Utility.openEntityForm(Xrm.Page.data.entity.getEntityName(), id);
                    } else if (result.status == "error") {
                        alert(result.data);
                    } else {
                        alert(result.data);
                    }
                }
            });
        }
    } else {
        alert("Đã duyệt rồi");
    }

}
function BtnCancelSubOrderEnableRule() {
    var duyet = getValue("bsd_duyet");
    if (duyet == false && getValue("bsd_status") != 861450003) {
        return true;
    } else {
        return false;
    }
}

function BtnApprove(Selected) {
    alert("ok");
    alert("Chon : " + Selected.length)
}
function BtnApproveEnableRule() {
    return true;
}

function BtnAddNewSubOrderEnableRule() {
    //var type = getValue("bsd_type");
    //if (type == 861450000 && getValue("bsd_duyet") == false && getValue("bsd_status") == 861450000) {
    //    return true;
    //} else {
    //    return false;
    //}
    return true;
}

// nút xóa sản phẩm trên trên subgrid suborder product
function BtnDeleteSubOrderProduct() {
    //var result = false;
    //var type = getValue("bsd_type");
    //if (type == 861450001) {
    //    alert("SubOrder tạo từ quote không thể xóa");
    //} else if (type == 861450002) {
    //    alert("SubOrder tạo từ order không thể xóa");
    //} else {
    //    var duyet = getValue("bsd_duyet");
    //    if (duyet == true) {
    //        alert("SubOrder đã duyệt không thể xóa");
    //    } else {
    //        result = true;
    //    }
    //}
    //return result;
    return true;
}

function request_receipt_date_change() {
    var date = getValue("bsd_requestedreceiptdate");
    if (date != null) {
        var timeship = getValue("bsd_timeship");
        if (getValue("bsd_timeship") == null) {
            timeship = 0;
        }
        date.setDate(date.getDate() - timeship);
        setValue("bsd_requestedshipdate", date);
    }
    check_fromdate_requestshipdate();
}

function check_fromdate_requestshipdate() {
    clearNotification("bsd_requestedshipdate");
    var from = getValue("bsd_fromdate");
    var request_shipdate = getValue("bsd_requestedshipdate");
    if (from != null && request_shipdate != null) {
        if (request_shipdate - from < 0) {
            setNotification("bsd_requestedshipdate", "The request ship date cannot occur before the From date");
        }
    }
}

function request_ship_date_change() {
    var date = getValue("bsd_requestedshipdate");
    if (date != null) {
        var timeship = getValue("bsd_timeship");
        if (getValue("bsd_timeship") == null) {
            timeship = 0;
        }
        date.setDate(date.getDate() + timeship);
        setValue("bsd_requestedreceiptdate", date);
    }
    check_fromdate_requestshipdate();
}

function preventAutoSave(econtext) {
    var eventArgs = econtext.getEventArgs();
    if (eventArgs.getSaveMode() == 70 || eventArgs.getSaveMode() == 2) {
        eventArgs.preventDefault();
    }
    var bsd_transportation = getValue("bsd_transportation");
    if (bsd_transportation == true) {
        var pricelist = getValue("bsd_shippingpricelistname");
        if (pricelist == null) {
            setNotification("bsd_shippingpricelistname", "You must provide a value for Price List !");
        }
    }
    if (getValue("bsd_pricelist") == null) {
        alert("This account has no pricelist. Please create an pricelist for this account");
        eventArgs.preventDefault();
    }
    if (getValue("bsd_exchangeratevalue") == null) {
        alert("Chưa có tỉ giá!");
        eventArgs.preventDefault();
    }
}
//author:Mr.Phong
function hide_button() {
    try {
        window.parent.document.getElementById("stageBackActionContainer").style.display = "none";
    } catch (e) {

    }
    try {
        window.parent.document.getElementById("stageBackActionContainer").style.visibility = "hidden";
    } catch (e) {

    }
    try {
        window.parent.document.getElementById("stageAdvanceActionContainer").style.display = "none";
    } catch (e) {

    }
    try {
        window.parent.document.getElementById("stageAdvanceActionContainer").style.visibility = "hidden";
    } catch (e) {

    }
    try {
        window.parent.document.getElementById("stageSetActiveActionContainer").style.display = "none";
    } catch (e) {

    }
    try {
        window.parent.document.getElementById("stageSetActiveActionContainer").style.visibility = "hidden";
    } catch (e) {

    }

}
//Author:Mr.Đăng
//Description: Load Date PaymentTerm
function Load_DatepaymentTerm() {
    var paymentterm = getValue("bsd_paymentterm");
    if (paymentterm != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_paymentterm">',
                        '<attribute name="bsd_paymenttermid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_date" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_paymenttermid" operator="eq" uiname="10D" value="' + paymentterm[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            if (rs.length > 0) {
                setValue("bsd_datept", rs[0].attributes.bsd_date.value);
            } else {
                setValue("bsd_datept", null);
            }
        });
        
    }
}
//Author:Mr.Đăng
function Duedate_change() {
    var date = getValue("bsd_date");
    var duedate = getValue("bsd_duedate");
    if (date != null && duedate == null) {
        var datept = getValue("bsd_datept");
        if (datept != null) {
            date.setDate(date.getDate() + datept);
            setValue("bsd_duedate", date);
        }
    }
}
//Author:Mr.Phong
//Description:hide checkbox
function hide_checkbox() {
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var stagename = Xrm.Page.data.process.getActiveStage().getName();
    if (rnames = "Phòng Kế Toán" && stagename == "Kế Toán Duyệt") {
        Xrm.Page.ui.controls.get("bsd_congno").setVisible(true);
    }
    else {
        Xrm.Page.ui.controls.get("bsd_congno").setVisible(false);
    }
}
function GetRoleName(roleIds) {
    var serverUrl = location.protocol + "//" + location.host + "/" + Xrm.Page.context.getOrgUniqueName();
    var odataSelect = serverUrl + "/XRMServices/2011/OrganizationData.svc" + "/" + "RoleSet?$select=Name";
    var cdn = "";
    if (roleIds != null && roleIds.length > 0) {
        for (var i = 0; i < roleIds.length; i++) {
            if (i == roleIds.length - 1)
                cdn += "RoleId eq guid'" + roleIds[i] + "'";
            else
                cdn += "RoleId eq guid'" + roleIds[i] + "' or ";
        }
        if (cdn.length > 0)
            cdn = "&$filter=" + cdn;
        odataSelect += cdn;
        var roleName = [];
        $.ajax(
            {
                type: "GET",
                async: false,
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                url: odataSelect,
                beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
                success: function (data, textStatus, XmlHttpRequest) {
                    if (data.d != null && data.d.results != null) {
                        var len = data.d.results.length;
                        for (var k = 0; k < len; k++)
                            roleName.push(data.d.results[k].Name);
                    }
                },
                error: function (XmlHttpRequest, textStatus, errorThrown) { alert('OData Select Failed: ' + textStatus + errorThrown + odataSelect); }
            }
        );
    }

    return roleName;
}

function clear_exchangerate() {
    getControl("bsd_exchangerate").addPreSearch(presearch_exchangerate);
}
function presearch_exchangerate() {
    getControl("bsd_exchangerate").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_shiptoaddress() {
    getControl("bsd_shiptoaddress").addPreSearch(presearch_shiptoaddress);
}
function presearch_shiptoaddress() {
    getControl("bsd_shiptoaddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_contact() {
    getControl("bsd_contact").addPreSearch(presearch_contact);
}
function presearch_contact() {
    getControl("bsd_contact").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_addresscustomeraccount() {
    getControl("bsd_addresscustomeraccount").addPreSearch(presearch_addresscustomeraccount);
}
function presearch_addresscustomeraccount() {
    getControl("bsd_addresscustomeraccount").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_contactinvoice() {
    getControl("bsd_contactinvoiceaccount").addPreSearch(presearch_contactinvoice);
}
function presearch_contactinvoice() {
    getControl("bsd_contactinvoiceaccount").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_addressinvoiceaccount() {
    getControl("bsd_addressinvoiceaccount").addPreSearch(presearch_addressinvoiceaccount);
}
function presearch_addressinvoiceaccount() {
    getControl("bsd_addressinvoiceaccount").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_priceofporter() {
    getControl("bsd_priceofporter").addPreSearch(presearch_priceofporter);
}
function presearch_priceofporter() {
    getControl("bsd_priceofporter").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_contactshiptoaccount() {
    getControl("bsd_contactshiptoaccount").addPreSearch(presearch_contactshiptoaccount);
}
function presearch_contactshiptoaccount() {
    getControl("bsd_contactshiptoaccount").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_paymentterm() {
    getControl("bsd_paymentterm").addPreSearch(presearch_paymentterm);
}
function presearch_paymentterm() {
    getControl("bsd_paymentterm").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_paymentmethod() {
    getControl("bsd_paymentmethod").addPreSearch(presearch_paymentmethod);
}
function presearch_paymentmethod() {
    getControl("bsd_paymentmethod").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_priceleve() {
    getControl("bsd_pricelist").addPreSearch(presearch_priceleve);
}
function presearch_priceleve() {
    getControl("bsd_pricelist").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function BtnDeliveryPlanEnableRule() {
    debugger;
    var statecode = Xrm.Page.getAttribute("statecode").getValue();
    if (AccountUserID().length > 0) {
        if (AccountUserID()[0].attributes.bsd_accounttype.value == 100000000) {
            return false;
        }
        else {
            return true;
        }
    } else {
        return true;
    }
}