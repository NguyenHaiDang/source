function AutoLoad() {
    debugger;
    LK_LoadAccountDistributor();
    set_DisabledCountingjournal();
    if (formType() == 2) {
        LK_LoadWarehouse(false);
    } else {
        clear_Warehouse();
        clear_Product();
    }
}
//Author:Mr.Đăng
//Description: Load Warehouse theo Account
function LK_LoadAccountDistributor() {
    debugger;
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="primarycontactid" />',
                    '<attribute name="telephone1" />',
                    '<attribute name="accountid" />',
                    '<order attribute="name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="100000000" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    LookUp_After_Load("bsd_distributor", "account", "name", "Distributor", fetchxml);
    //LK_LoadWarehouse(reset);
}
//Author:Mr.Đăng
//Description: Load Warehouse theo Distributor Account 
function LK_LoadWarehouse(reset) {
    debugger;
    var distributor = getValue("bsd_distributor");
    getControl("bsd_warehousedms").removePreSearch(presearch_Warehouse);
    if (distributor != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_warehousedms">',
                        '<attribute name="bsd_warehousedmsid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + distributor[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            console.log(rs);
            if (rs.length > 0) {
                var first = rs[0];
                if (getValue("bsd_warehousedms") == null) {
                    setValue("bsd_warehousedms", [{
                        id: first.Id,
                        name: first.attributes.bsd_name.value,
                        entityType: first.logicalName
                    }]
                   );
                    LK_LoadProductDistributor(false);
                }
            }
        }, function (er) { });
        LookUp_After_Load("bsd_warehousedms", "bsd_warehousedms", "bsd_name", "Warehouse DMS", fetchxml);
    }
    else if (reset != false) {
        setNull(["bsd_warehousedms", "bsd_product", "bsd_netquatity", "bsd_quantity", "bsd_quantitydifference"]);
        clear_Warehouse();
    }
}
function clear_Warehouse() {
    getControl("bsd_warehousedms").addPreSearch(presearch_Warehouse);
}
function presearch_Warehouse() {
    getControl("bsd_warehousedms").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.Đăng
//Description: Load Warehouse theo Account
function LK_LoadProductDistributor(reset) {
    debugger;
    var warehouse = getValue("bsd_warehousedms");
    getControl("bsd_product").removePreSearch(presearch_Product);
    if (warehouse != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                        '<entity name="product">',
                        '<attribute name="name" />',
                        '<attribute name="productnumber" />',
                        '<attribute name="statecode" />',
                        '<attribute name="productid" />',
                        '<attribute name="productstructure" />',
                        '<order attribute="productnumber" descending="false" />',
                        '<link-entity name="bsd_warehourseproductdms" from="bsd_product" to="productid" alias="ac">',
                        '<filter type="and">',
                        '<condition attribute="bsd_warehouses" operator="eq" uitype="bsd_warehousedms" value="' + warehouse[0].id + '" />',
                        '</filter>',
                        '</link-entity>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_product", "product", "name", "Product", fetchxml);
    }
    else if (reset != false) {
        setNull(["bsd_product", "bsd_netquatity", "bsd_quantity", "bsd_quantitydifference"]);
        clear_Product();
    }
}
function clear_Product() {
    getControl("bsd_product").addPreSearch(presearch_Product);
}
function presearch_Product() {
    getControl("bsd_product").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.Đăng
//Description: Load Quantity
function LK_LoadQuantityProduct() {
    debugger;
    var product = getValue("bsd_product");
    var warehouse = getValue("bsd_warehousedms");
    if (product != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_warehourseproductdms">',
                        '<attribute name="bsd_warehourseproductdmsid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<attribute name="bsd_quantity" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_product" operator="eq" uitype="product" value="' + product[0].id + '" />',
                        '<condition attribute="bsd_warehouses" operator="eq" uitype="bsd_warehousedms" value="' + warehouse[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            if (rs.length > 0) {
                setValue("bsd_netquatity", rs[0].attributes.bsd_quantity.value);
            } else {
                setNull(["bsd_netquatity", "bsd_quantity", "bsd_quantitydifference"]);
            }
        });
    }
    else {
        setNull(["bsd_netquatity", "bsd_quantity", "bsd_quantitydifference"]);
    }
}
//Author:Mr.Diệm
//Description: LookUp_After_Load
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}
//Author:Mr.Đăng
//Description: Load Quantity
function QuantityDifference() {
    debugger;
    var netquantity = 0;
    if (getValue("bsd_netquatity") != null) {
        netquantity = getValue("bsd_netquatity");
    }
    var quantity = 0;
    if (getValue("bsd_quantity") != null) {
        quantity = getValue("bsd_quantity");
    }
    var quantitydifference = 0;
    quantitydifference = netquantity - quantity;
    setValue("bsd_quantitydifference", quantitydifference);
}
// 29.03.2017 Đăng NH: Disable field Notification
function set_DisabledCountingjournal() {
    var countingjournal = getValue("bsd_countingjournal");
    if (countingjournal != null) setDisabled("bsd_countingjournal", true);
}