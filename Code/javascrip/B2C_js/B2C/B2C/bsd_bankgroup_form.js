//Author:Mr.Phong
//Description:count text length
function counttextlength()
{
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var number = getValue("bsd_routingnumber");
    var id = getId();
    if (number != null)
    {
        if (mikExp.test(number)) {
            Xrm.Page.getControl("bsd_routingnumber").setNotification("Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else if (!mikExp.test(number))
        {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_bankgroup">',
                            '<attribute name="bsd_bankgroupid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_routingnumber" operator="eq" value="' + number + '" />',
                            '<condition attribute="bsd_bankgroupid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_routingnumber", "Mã số ngân hàng đã tồn tại, vui lòng kiểm tra lại.");
            }
            else {
                clearNotification("bsd_routingnumber");
            }
        }
    }
}