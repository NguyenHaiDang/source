function getFullDay(day) {
    var fullday = null;
    var createdatefrom = getValue(day);
    if (createdatefrom != null) {
        var year = createdatefrom.getFullYear();
        var date = createdatefrom.getDate();

        var laymonth = (createdatefrom.getMonth() + 1);
        var month;
        if (laymonth > 0 && laymonth <= 9) {
            month = "0" + laymonth;
        } else {
            month = laymonth;
        }
        fullday = year + "-" + month + "-" + date;
    }
    return fullday;
}

function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}

//Function báo lỗi. 
function m_Error(message,id) {
    Xrm.Page.ui.setFormNotification(message, "ERROR", id);
}

function c_Error(id) {
    Xrm.Page.ui.clearFormNotification(id);
}