// 29.03.2017 Đăng NH: Autoload
function AutoLoad() {
    set_DisabledDeliverytruck();// 29.03.2017 Đăng NH: Disable field Notification
    if (formType() == 1) {
    } else {
    }
}
function onload() {
    var xml = [];
    var entityName = "bsd_deliverytruck";
    var viewDisplayName = "test";
    fetch(xml, "bsd_deliverytruck", ["bsd_deliverytruckid", "bsd_name", "createdon"], "bsd_name", false, null
           , ["bsd_companytype"], ["eq"], [0, "861450000", 1]);
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_deliverytruckid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                          "<row name='result'  " + "id='bsd_deliverytruckid'>  " +
                          "<cell name='bsd_name'   " + "width='200' />  " +
                          "<cell name='createdon'    " + "width='100' />  " +
                          "</row>   " +
                       "</grid>   ";
    Xrm.Page.getControl("bsd_deliverytruck").addCustomView(getDefaultView("bsd_deliverytruck"), entityName, viewDisplayName, xml.join(""), layoutXml, true);
}
/*
//Author:Mr.Đăng
//Description:Check Driver ID & UpperCase
function Check_DriverID() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var codedriver = getValue("bsd_codedriver");
    var id = getId();
    if (codedriver != null) {
        if (mikExp.test(codedriver)) {
            setNotification("bsd_code", "Can't enter special character.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_driver">',
                            '<attribute name="bsd_driverid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_codedriver" operator="eq" value="' + codedriver + '" />',
                            '<condition attribute="bsd_driverid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
                if (rs.length > 0) {
                    setNotification("bsd_codedriver", "The driver already exists.")
                } else {
                    clearNotification("bsd_codedriver");
                    setValue("bsd_codedriver", codedriver.toUpperCase());
                }
            });
        }
    }
}
*/
// 29.03.2017 Đăng NH: Disable field MCP
function set_DisabledDeliverytruck() {
    var deliverytruck = getValue("bsd_deliverytruck");
    if (deliverytruck != null) setDisabled("bsd_deliverytruck", true);
}