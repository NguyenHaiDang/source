
// staff: Mr.Diệm
//Note: set disabled for warehousefrom when distributor is null
function Onload() {
    debugger;
    if (formType() == 2) {
        Load_PackingList();
        Lk_Distributor(false);
        LK_Consumer_Change(false);
    }
    else {
        Load_PackingList();
        Lk_Distributor(false);
        clear_warehousefrom();
    }
}

//Load Packinglists2s lay packinglisttype.

function Load_PackingList() {
    var packinglists2s = getValue("bsd_packinglist");
    if (packinglists2s != null) {
        var fetchxml1 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                          '<entity name="bsd_packinglists2s">',
                          '  <attribute name="bsd_packinglists2sid" />',
                          '  <attribute name="bsd_packinglisttype" />',
                          '  <order attribute="bsd_name" descending="false" />',
                          '  <filter type="and">',
                          '    <condition attribute="bsd_packinglists2sid" operator="eq" uitype="bsd_packinglists2s" value="' + packinglists2s[0].id + '" />',
                          '  </filter>',
                          '</entity>',
                        '</fetch>'].join("");
        
        CrmFetchKit.Fetch(fetchxml1, false).then(function (rs1) {
            if (rs1.length > 0) {
                var first = rs1[0];
                if (first.attributes.bsd_packinglisttype != undefined) {
                    var packinglisttype = first.attributes.bsd_packinglisttype.value;
                    if (packinglisttype == "861450000") {
                        setVisible(["bsd_consumer", "bsd_addresstoship"], false);
                    }
                }
            }
        }, errorHandler);
    }
}

// staff: Mr.Diệm
function Lk_Distributor(reset) {
    debugger;
    //console.log(getControl("bsd_warehousefrom").getDefaultView
    if (reset != false) setNull(["bsd_warehousefrom", "bsd_consumer", "bsd_addresstoship"]);
    var packinglists2s = getValue("bsd_packinglist");
    var Distributor = getValue("bsd_distributor");
    if (Distributor != null) {
        // Note: search resurt Warehouse_From with condition distributor.
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_warehousedms">',
                        '<attribute name="bsd_warehousedmsid"/>',
                        '<attribute name="bsd_name"/>',
                        '<attribute name="createdon"/>',
                        '<order attribute="bsd_name" descending="false"/>',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + Distributor[0].id + '"/>',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        //  set defaut
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {

            if (rs.length > 0) {
                var first = rs[0];
                setValue("bsd_warehousefrom", [{
                    id: first.Id,
                    name: first.attributes.bsd_name.value,
                    entityType: first.logicalName
                }]
                );
            }
        }, errorHandler);

        //load resurt.
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_warehousedmsid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_warehousedmsid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "</row>   " +
                        "</grid>";
        getControl("bsd_warehousefrom").removePreSearch(presearch_warehousefrom);
        getControl("bsd_warehousefrom").addCustomView("{1335C685-F794-49D9-827D-012D0D81D9C6}", "bsd_warehousedms", "Warehouse From", fetchxml, layoutXml, true);

        // Note: search resurt comsumer with condition distributor.


        var fetchxml2 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                          ' <entity name="contact">',
                           '  <attribute name="fullname" />',
                           ' <attribute name="contactid" />',
                           '  <order attribute="fullname" descending="false" />',
                           '  <filter type="and">',
                           '    <condition attribute="parentcustomerid" operator="eq"  uitype="account" value="' + Distributor[0].id + '" />',
                           '  </filter>',
                           '</entity>',
                         '</fetch>'].join("");

        //CrmFetchKit.Fetch(fetchxml2, false).then(function (rs2) {
        //     if (rs2.length > 0) {
        //         var first = rs2[0];
        //         setValue("bsd_consumer", [{
        //             id: first.Id,
        //             name: first.attributes.fullname.value,
        //             entityType: first.logicalName
        //         }]
        //         );
        //     }
        // }, errorHandler);
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                 "<row name='result'  " + "id='contactid'>  " +
                 "<cell name='fullname'   " + "width='200' />  " +
                 "</row>   " +
            "</grid>";
        getControl("bsd_consumer").removePreSearch(presearch_consumer);
        getControl("bsd_consumer").addCustomView(getControl("bsd_consumer").getDefaultView(), "contact", "Contacts", fetchxml2, layoutXml2, true);
    }
    else if (reset != false) {
        clear_warehousefrom();
        clear_consumer();
        clear_address();
    }
}

//Mr:Diệm
//Note:Load address to ship.
function LK_Consumer_Change(reset) {
    if (reset != false) setNull("bsd_addresstoship");
    var consumer = getValue("bsd_consumer");
    if (consumer != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                        '  <entity name="bsd_address">',
                        '    <attribute name="bsd_name" />',
                        '    <attribute name="createdon" />',
                        '    <attribute name="bsd_purpose" />',
                        '    <attribute name="bsd_account" />',
                        '    <attribute name="bsd_addressid" />',
                        '    <order attribute="bsd_name" descending="false" />',
                        '    <link-entity name="contact" from="bsd_address" to="bsd_addressid" alias="ab">',
                        '      <filter type="and">',
                        '        <condition attribute="contactid" operator="eq" uitype="contact" value="'+consumer[0].id+'" />',
                        '      </filter>',
                        '    </link-entity>',
                        '  </entity>',
                        '</fetch>'].join("");
         CrmFetchKit.Fetch(fetchxml, false).then(function (rs2) {
             if (rs2.length > 0) {
                 console.log(rs2);
                 var first = rs2[0];
                 setValue("bsd_addresstoship", [{
                     id: first.Id,
                     name: first.attributes.bsd_name.value,
                     entityType: first.logicalName
                 }]
                 );
             }
         }, errorHandler);
         var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='bsd_addressid'>  " +
                  "<cell name='bsd_name'   " + "width='200' />  " +
                  "</row>   " +
             "</grid>";
         getControl("bsd_addresstoship").removePreSearch(presearch_address);
         getControl("bsd_addresstoship").addCustomView(getControl("bsd_addresstoship").getDefaultView(), "bsd_address", "Address", fetchxml, layoutXml2, true);

    }
    else if (reset != false) {
        clear_address();
    }
}

// staff: Mr.Diệm
// Note:
function errorHandler(err) {
    console.log(err);
}
//staff Mr:Diệm
//Note: load productsp where ia packing list
function filter_grid() {
    var idpackinglist = getValue("bsd_packinglist");
    //var objSubGrid = document.getElementById("bsd_scheduledelivery");
    if (idpackinglist != null) {
        var xml = [
            '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_scheduledelivery">',
                '<attribute name="bsd_scheduledeliveryid" />',
                '<attribute name="bsd_name" />',
                '<attribute name="createdon" />',
                '<order attribute="bsd_name" descending="false" />',
                '<filter type="and">',
                  '<condition attribute="bsd_orderid" operator="eq" uitype="salesorder" value="' + idpackinglist[0].id + '" />',
                '</filter>',
              '</entity>',
            '</fetch>'
        ];
        objSubGrid.control.SetParameter("fetchXML", xml);
        objSubGrid.control.Refresh();
    } else {
        setTimeout('filter_grid()', 1000);
    }
}

function clear_warehousefrom() {
    getControl("bsd_warehousefrom").addPreSearch(presearch_warehousefrom);
}
function presearch_warehousefrom() {
    getControl("bsd_warehousefrom").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_consumer() {
    getControl("bsd_consumer").addPreSearch(presearch_consumer);
}
function presearch_consumer() {
    getControl("bsd_consumer").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_address() {
    getControl("bsd_addresstoship").addPreSearch(presearch_address);
}
function presearch_address() {
    getControl("bsd_addresstoship").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}