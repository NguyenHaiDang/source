//huy
function init() {
    load_unit();
    if (formType() == 1) {
        load_default_unit();
    } else if (formType() == 2) {

    }
}
function load_default_unit() {
    setDisabled("bsd_unit", true)
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
	'<entity name="uom">',
    '	<attribute name="name" />',
    '    <link-entity name="bsd_configdefault" from="bsd_porterunitdefault" to="uomid" alias="aa" />',
	'</entity>',
	'</fetch>'].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            setValue("bsd_unit", [{
                id: rs[0].Id,
                name: rs[0].attributes.name.value,
                entityType: rs[0].logicalName
            }]);
        }
    });
}
function effective_change() {
    clearNotification("bsd_effectiveto");

    var effective_from = getValue("bsd_effectivefrom");
    var effective_to = getValue("bsd_effectiveto");
    if (effective_to != null && effective_from != null) {
        if (effective_from > effective_to) {
            setNotification("bsd_effectiveto", "Effective To is not smaller than Effective To!");
        }
    }
}

function load_unit() {
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="uom">',
        '<attribute name="name" />',
        '<filter type="and">',
          '<condition attribute="uomscheduleid" operator="eq" uitype="uomschedule" value="{1DB70548-0A95-E611-80CC-000C294C7A2D}" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='uomid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                       "<row name='result'  " + "id='uomid'>  " +
                                       "<cell name='name'   " + "width='200' />  " +
                                       "</row>" +
                                    "</grid>";
    getControl("bsd_unit").addCustomView(getDefaultView("bsd_unit"), "uom", "uom", xml, layoutXml, true);
}

function price_change() {
    clearNotification("bsd_price");
    if (getValue("bsd_price") < 0) {
        setNotification("bsd_price", "Price is not smaller than 0!");
    }
}

//end code huy
//author:Mr.Phong
function set_presentprice() {
    var porter = Xrm.Page.getAttribute("bsd_potertype").getValue();
    if (porter == "861450001")//Porter type :No
    {
        Xrm.Page.getAttribute("bsd_price").setValue(0);
        Xrm.Page.getControl("bsd_price").setDisabled(true);

    }
    if (porter == "861450000")//Porter type :Yes
    {
        Xrm.Page.getControl("bsd_price").setDisabled(false);
        Xrm.Page.getAttribute("bsd_price").setValue(null);
    }
}
//28.03.2017 Đăng NH: Bắt trùng mã porter
function check_Code() {
    debugger;
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var code = getValue("bsd_code");
    var id = getId();
    if (code != null) {
        if (mikExp.test(code)) {
            setNotification("bsd_code", "Can't enter special character.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_porter">',
                            '<attribute name="bsd_porterid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_code" operator="eq" value="' + code + '" />',
                            '<condition attribute="bsd_porterid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
                if (rs.length > 0) {
                    setNotification("bsd_code", "The porter code already exists.")
                } else {
                    clearNotification("bsd_code");
                    setValue("bsd_code", code.toUpperCase());
                }
            });
        }
    }
}