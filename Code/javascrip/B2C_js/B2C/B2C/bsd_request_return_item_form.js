﻿function AutoLoad() {
    LK_Reason();
    set_Status();
    if (formType() == 2) {
        LK_Order_OutletandDate(false);
    } else {
        clear_LK_Order_OutletandDate();
    }
}
//Author:Mr.Đăng
//Description: LK Order theo Outlet và Date
function LK_Order_OutletandDate(reset) {
    if (reset != false) setNull(["bsd_orderdms"]);
    getControl("bsd_orderdms").removePreSearch(presearch_LK_Order_OutletandDate);
    var outlet = getValue("bsd_outlet");
    var orderdate = getValue("bsd_orderdate");
    if (outlet != null && orderdate !=null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_orderdms">',
                        '<attribute name="bsd_orderdmsid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_cusomer" operator="eq" uitype="contact" value="' + outlet[0].id + '" />',
                        '<condition attribute="createdon" operator="on" value="' + getFullDay("bsd_orderdate") + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_orderdms", "bsd_orderdms", "bsd_name", "OrderDMS", fetchxml);
    }
    else if (reset != false) {
        clear_LK_Order_OutletandDate();
    }
}
function clear_LK_Order_OutletandDate() {
    getControl("bsd_orderdms").addPreSearch(presearch_LK_Order_OutletandDate);
}
function presearch_LK_Order_OutletandDate() {
    getControl("bsd_orderdms").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.Đăng
//Description: LK Reason theo Type
function LK_Reason() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="bsd_reason">',
                    '<attribute name="bsd_reasonid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="createdon" />',
                    '<order attribute="bsd_name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_type" operator="eq" value="861450001" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    LookUp_After_Load("bsd_reason", "bsd_reason", "bsd_name", "Reason", fetchxml);
}
function set_Status() {
    var status = getValue("bsd_status");
    if (status == "861450001") {
        setDisabled(["bsd_outlet", "bsd_orderdate", "bsd_orderdms", "bsd_employee", "bsd_reason", "bsd_status", "bsd_description"], true);
    }
    if (status == "861450002") {
        setDisabled(["bsd_outlet", "bsd_orderdate", "bsd_orderdms", "bsd_employee", "bsd_reason", "bsd_status", "bsd_description"], true);
    }
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}
function getFullDay(day) {
    var fullday = null;
    var createdatefrom = getValue(day);
    if (createdatefrom != null) {
        var year = createdatefrom.getFullYear();
        var date = createdatefrom.getDate();

        var laymonth = (createdatefrom.getMonth() + 1);
        var month;
        if (laymonth > 0 && laymonth <= 9) {
            month = "0" + laymonth;
        } else {
            month = laymonth;
        }
        fullday = year + "-" + month + "-" + date;
    }
    return fullday;
}