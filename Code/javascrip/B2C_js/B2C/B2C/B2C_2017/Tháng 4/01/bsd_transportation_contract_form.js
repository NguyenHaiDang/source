﻿// 31.03.2017 Đăng : Autoload
function Autoload() {
    load_SellerAccount();
    load_BuyerAccount();
    if (formType() == 2) {
        //set_DisableWarehouse(false);
    } else {
        clear_SellerAddress();
        clear_BuyerAddress();
        clear_BankAccount();
        clear_SellerContact();
        clear_BankGroup();
        clear_Representative();
    }
}
// 31.03.2017 Đăng : Check Contract Number
function check_ContractNumber() {
    debugger;
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var code = getValue("bsd_code");
    var id = getId();
    if (code != null) {
        if (mikExp.test(code)) {
            setNotification("bsd_code", "Invalid Contract Number.");
        }
        else {
            setValue("bsd_code", code.toUpperCase());
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_transportationcontract">',
                            '<attribute name="bsd_transportationcontractid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_code" operator="eq" value="' + code + '" />',
                            '<condition attribute="bsd_transportationcontractid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
                if (rs.length > 0) {
                    setNotification("bsd_code", "Contract number already exist.");
                }
                else {
                    clearNotification("bsd_code");
                }
            });
        }
    }
}
// 31.03.2017 Đăng : Check DO Price Percentage Change
function check_PricePercent() {
    var mikExp = /[^0-9]/;
    var dopricepercentage = getValue("bsd_dopricepercentagechange");
    if (dopricepercentage != null) {
        if (mikExp.test(dopricepercentage)) {
            setNotification("bsd_dopricepercentagechange", "Invalid DO Price Percentage.");
        }
        else {
            clearNotification("bsd_dopricepercentagechange");
        }
    }
}
// 01.04.2017 Đăng: Check daystartcontract & dayendcontract
function check_Datenew() {
    debugger;
    var startdate = getValue("bsd_contractstartdate");
    var enddate = getValue("bsd_contractenddate");
    if (startdate != null && enddate != null && enddate < startdate) {
        setNotification("bsd_contractenddate", "The Day Start Contract cannot occur after Day End Contract");
    }
    else {
        clearNotification("bsd_contractenddate");
    }
}
// 31.03.2017 Đăng : Load Seller Account
function load_SellerAccount() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="accountid" />',
                    '<order attribute="name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="861450002" />', // Shipper
                    '<condition attribute="statecode" operator="eq" value="0" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    LookUp_After_Load("bsd_seller", "account", "name", "Seller", fetchxml);
}
// 30.03.2017 Mr.Đăng : Load Seller Address
function load_SellerAddress() {
    debugger;
    var seller = getValue("bsd_seller");
    getControl("bsd_selleraddress").removePreSearch(presearch_SellerAddress);
    if (seller != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_account" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_purpose_tmpvalue" operator="like" value="%Business%" />',
                        '<condition attribute="statecode" operator="eq" value="0" />',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + seller[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_selleraddress", "bsd_address", "bsd_name", "Address", fetchxml);
    }
    else {
        setNull("bsd_selleraddress");
        clear_SellerAddress();
    }
    load_TaxRegistration();
}
// 30.03.2017 Mr.Đăng : Load Tax Registration
function load_TaxRegistration() {
    debugger;
    var seller = getValue("bsd_seller");
    if (seller != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="account">',
                        '<attribute name="name" />',
                        '<attribute name="accountid" />',
                        '<attribute name="bsd_taxregistration" />',
                        '<attribute name="accountnumber" />',
                        '<attribute name="bsd_paymentterm" />',
                        '<attribute name="bsd_paymentmethod" />',
                        '<order attribute="name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="accountid" operator="eq" uitype="account" value="' + seller[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            console.log(rs);
            if (rs.length > 0) {
                if (getValue("bsd_sellertaxregistration") == null) setValue("bsd_sellertaxregistration", rs[0].attributes.bsd_taxregistration.value);
                if (getValue("bsd_accountnumberseller") == null) setValue("bsd_accountnumberseller", rs[0].attributes.accountnumber.value);
                if (getValue("bsd_termofpayment") == null) {
                    setValue("bsd_termofpayment", [{
                        id: rs[0].attributes.bsd_paymentterm.guid,
                        name: rs[0].attributes.bsd_paymentterm.name,
                        entityType: rs[0].attributes.bsd_paymentterm.logicalName
                    }]
                   );
                }
                if (getValue("bsd_methodsofpayment") == null) {
                    setValue("bsd_methodsofpayment", [{
                        id: rs[0].attributes.bsd_paymentmethod.guid,
                        name: rs[0].attributes.bsd_paymentmethod.name,
                        entityType: rs[0].attributes.bsd_paymentmethod.logicalName
                    }]
                   );
                }
            }
        });
    }
    else {
        setNull(["bsd_sellertaxregistration", "bsd_accountnumberseller", "bsd_termofpayment", "bsd_methodsofpayment"]);
    }
    load_SellerContact();
}
// 30.03.2017 Mr.Đăng : Load Seller Address
function load_SellerContact() {
    debugger;
    var seller = getValue("bsd_seller");
    getControl("bsd_sellercontact").removePreSearch(presearch_SellerContact);
    getControl("bsd_sellerrepresentative").removePreSearch(presearch_Representative);
    if (seller != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="contact">',
                        '<attribute name="fullname" />',
                        '<attribute name="contactid" />',
                        '<order attribute="fullname" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="parentcustomerid" operator="eq" uitype="account" value="' + seller[0].id + '" />',
                        '<condition attribute="statecode" operator="eq" value="0" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_sellerrepresentative", "contact", "fullname", "Representative", fetchxml);
        LookUp_After_Load("bsd_sellercontact", "contact", "fullname", "Contact", fetchxml);
    }
    else {
        setNull(["bsd_sellercontact", "bsd_sellerrepresentative", "bsd_sellerposition"]);
        clear_SellerContact();
        clear_Representative();
    }
    load_BankAccount();
}
// 30.03.2017 Mr.Đăng : Load Bank Account
function load_BankAccount() {
    debugger;
    var seller = getValue("bsd_seller");
    getControl("bsd_sellerbankaccount").removePreSearch(presearch_BankAccount);
    if (seller != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_bankaccount">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_bankaccountid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + seller[0].id + '" />',
                        '<condition attribute="statecode" operator="eq" value="0" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_sellerbankaccount", "bsd_bankaccount", "bsd_name", "Bank Account", fetchxml);
    }
    else {
        setNull("bsd_sellerbankaccount");
        clear_BankAccount();
    }
    load_BankAccountInf();
}
// 30.03.2017 Mr.Đăng : Load Tax Registration
function load_BankAccountInf() {
    debugger;
    var sellerbankaccount = getValue("bsd_sellerbankaccount");
    getControl("bsd_sellerbankaccount").removePreSearch(presearch_BankGroup);
    if (sellerbankaccount != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_bankaccount">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_brand" />',
                        '<attribute name="bsd_bankaccountid" />',
                        '<attribute name="bsd_bankgroup" />',
                        '<attribute name="bsd_bankaccount" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_bankaccountid" operator="eq" uitype="bsd_bankaccount" value="' + sellerbankaccount[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            console.log(rs);
            if (rs.length > 0) {
                if (getValue("bsd_sellerbrand") == null) setValue("bsd_sellerbrand", rs[0].attributes.bsd_brand.value);
                if (getValue("bsd_sellerbankgroup") == null) {
                    setValue("bsd_sellerbankgroup", [{
                        id: rs[0].attributes.bsd_bankgroup.guid,
                        name: rs[0].attributes.bsd_bankgroup.name,
                        entityType: rs[0].attributes.bsd_bankgroup.logicalName
                    }]
                   );
                }
                if (getValue("bsd_sellerbankaccountnumber") == null) setValue("bsd_sellerbankaccountnumber", rs[0].attributes.bsd_bankaccount.value);
            }

        }, function (er) { });
    }
    else {
        setNull(["bsd_sellerbrand", "bsd_sellerbankgroup", "bsd_sellerbankaccountnumber"]);
        clear_BankGroup();
    }
}
// 30.03.2017 Mr.Đăng : Load Tax Registration
function load_Position() {
    debugger;
    var sellerrepresentative = getValue("bsd_sellerrepresentative");
    if (sellerrepresentative != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="contact">',
                        '<attribute name="fullname" />',
                        '<attribute name="contactid" />',
                        '<attribute name="jobtitle" />',
                        '<order attribute="fullname" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="contactid" operator="eq" uitype="contact" value="' + sellerrepresentative[0].id + '" />',
                        '<condition attribute="statecode" operator="eq" value="0" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            console.log(rs);
            if (rs.length > 0) {
                if (getValue("bsd_sellerposition") == null) setValue("bsd_sellerposition", rs[0].attributes.jobtitle.value);
            }
        });
    }
    else {
        setNull("bsd_sellerposition");
    }
}
/////////////////////////// Buyer
// 31.03.2017 Đăng : Load Buyer Account
function load_BuyerAccount() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="accountid" />',
                    '<order attribute="name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="861450005" />', // BHS
                    '<condition attribute="statecode" operator="eq" value="0" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    LookUp_After_Load("bsd_buyer", "account", "name", "Buyer", fetchxml);
}
// 30.03.2017 Mr.Đăng : Load Buyer Address
function load_BuyerAddress() {
    debugger;
    var buyer = getValue("bsd_buyer");
    getControl("bsd_buyeraddress").removePreSearch(presearch_BuyerAddress);
    if (buyer != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_account" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_purpose_tmpvalue" operator="like" value="%Business%" />',
                        '<condition attribute="statecode" operator="eq" value="0" />',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + buyer[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_buyeraddress", "bsd_address", "bsd_name", "Buyer Address", fetchxml);
    }
    else {
        setNull("bsd_buyeraddress");
        clear_BuyerAddress();
    }
    load_AccountNumberBuyer();
}
// 30.03.2017 Mr.Đăng : Load Tax Registration
function load_AccountNumberBuyer() {
    debugger;
    var buyer = getValue("bsd_buyer");
    if (buyer != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="account">',
                        '<attribute name="name" />',
                        '<attribute name="accountid" />',
                        '<attribute name="bsd_taxregistration" />',
                        '<attribute name="accountnumber" />',
                        '<order attribute="name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="accountid" operator="eq" uitype="account" value="' + buyer[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            console.log(rs);
            if (rs.length > 0) {
                if (getValue("bsd_accountnumberbuyer") == null) setValue("bsd_accountnumberbuyer", rs[0].attributes.accountnumber.value);
            }
        });
    }
    else {
        setNull("bsd_accountnumberbuyer");
    }
}
function clear_SellerAddress() {
    getControl("bsd_selleraddress").addPreSearch(presearch_SellerAddress);
}
function presearch_SellerAddress() {
    getControl("bsd_selleraddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_BankAccount() {
    getControl("bsd_sellerbankaccount").addPreSearch(presearch_BankAccount);
}
function presearch_BankAccount() {
    getControl("bsd_sellerbankaccount").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_BankGroup() {
    getControl("bsd_sellerbankgroup").addPreSearch(presearch_BankGroup);
}
function presearch_BankGroup() {
    getControl("bsd_sellerbankgroup").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_SellerContact() {
    getControl("bsd_sellercontact").addPreSearch(presearch_SellerContact);
}
function presearch_SellerContact() {
    getControl("bsd_sellercontact").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Representative() {
    getControl("bsd_sellerrepresentative").addPreSearch(presearch_Representative);
}
function presearch_Representative() {
    getControl("bsd_sellerrepresentative").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_BuyerAddress() {
    getControl("bsd_buyeraddress").addPreSearch(presearch_BuyerAddress);
}
function presearch_BuyerAddress() {
    getControl("bsd_buyeraddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}