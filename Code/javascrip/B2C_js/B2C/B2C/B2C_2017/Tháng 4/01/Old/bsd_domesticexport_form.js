//Author:Mr.phong
//Desciption:filter segment follow channel
function filtersegmentfollowchannel() {
    var channel = Xrm.Page.getAttribute("bsd_channel").getValue();
    Xrm.Page.getControl("bsd_segment").removePreSearch(presearch_segment);
    Xrm.Page.getControl("bsd_type").removePreSearch(presearch_type);
    Xrm.Page.getControl("bsd_tenrutngansieuthi").removePreSearch(presearch_tenrutngansieuthi);
    var xml = [];
    var xml1 = [];
    if (channel != null) {
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='bsd_accountgroupchannel'>");
        xml1.push("<attribute name='bsd_accountgroupchannelid'/>");
        xml1.push("<attribute name='bsd_name' />")
        xml1.push("<attribute name='createdon' />");
        xml1.push("<attribute name='statecode' />");
        xml1.push("<attribute name='bsd_domesticexport' />");
        xml1.push("<attribute name='bsd_channel' />");
        xml1.push("<order attribute='bsd_name' descending='false' />");
        xml1.push("<filter type='and' >");
        xml1.push("<condition attribute='bsd_accountgroupchannelid' operator='eq' uitype='bsd_accountgroupchannel' value='" + channel[0].id + "' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
            if (rs1.length > 0) {
                var viewId = "{79D7AF1E-6AAB-45BC-8045-8786A2E2792D}";
                var entityName = "bsd_segmentchanel";
                var viewDisplayName = "test";
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml.push("<entity name='bsd_segmentchanel'>");
                xml.push("<attribute name='bsd_segmentchanelid'/>");
                xml.push("<attribute name='bsd_name' />")
                xml.push("<attribute name='createdon' />");
                xml.push("<attribute name='statecode' />");
                xml.push("<attribute name='bsd_segment' />");
                xml.push("<attribute name='bsd_channel' />");
                xml.push("<order attribute='bsd_name' descending='false' />");
                xml.push("<filter type='and' >");
                xml.push("<condition attribute='bsd_channel' operator='eq' value='" + rs1[0].attributes.bsd_channel.guid + "' />");
                xml.push("<condition attribute='statecode' operator='eq' value='0' />");
                xml.push("</filter>");
                xml.push("</entity>");
                xml.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_channelsegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_channelsegmentid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                Xrm.Page.getControl("bsd_segment").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
                Xrm.Page.getAttribute("bsd_segment").setValue(null);
            }
        }, function (er) {
            console.log(er.message)
        });
        Xrm.Page.getAttribute("bsd_type").setValue(null);
        Xrm.Page.getAttribute("bsd_tenrutngansieuthi").setValue(null);
        clear_type();
        clear_tenrutngansieuthi();
    }
    else {
        clear_segment();
        clear_tenrutngansieuthi();
        clear_type();
        Xrm.Page.getAttribute("bsd_segmentt").setValue(null);
        Xrm.Page.getAttribute("bsd_typee").setValue(null);
        Xrm.Page.getAttribute("bsd_tenrutngansieuthi").setValue(null);
    }
}
//Author:Mr.phong
//Desciption:filter type follow segment
function filtertypefollowsegment() {
    var segment = Xrm.Page.getAttribute("bsd_segment").getValue();
    Xrm.Page.getControl("bsd_type").removePreSearch(presearch_type);
    Xrm.Page.getControl("bsd_tenrutngansieuthi").removePreSearch(presearch_tenrutngansieuthi);
    var xml = [];
    var xml1 = [];
    if (segment != null) {
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='bsd_channelsegment'>");
        xml1.push("<attribute name='bsd_name' />")
        xml1.push("<attribute name='createdon' />");
        xml1.push("<attribute name='statecode' />");
        xml1.push("<attribute name='bsd_segment' />");
        xml1.push("<attribute name='bsd_channel' />");
        xml1.push("<attribute name='bsd_channelsegmentid' />");
        xml1.push("<order attribute='bsd_name' descending='false' />");
        xml1.push("<filter type='and' >");
        xml1.push("<condition attribute='bsd_channelsegmentid' operator='eq' uitype='bsd_channelsegment' value='" + segment[0].id + "' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
            if (rs1.length > 0) {
                var viewId = "{F32CC20D-C159-4E6B-8455-F73E2E25FE30}";
                var entityName = "bsd_typesegment";
                var viewDisplayName = "test";
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml.push("<entity name='bsd_typesegment'>");
                xml.push("<attribute name='bsd_typesegmentid'/>");
                xml.push("<attribute name='bsd_name' />")
                xml.push("<attribute name='createdon' />");
                xml.push("<attribute name='bsd_type' />");
                xml.push("<attribute name='statecode' />");
                xml.push("<attribute name='bsd_segment' />");
                xml.push("<order attribute='bsd_name' descending='false' />");
                xml.push("<filter type='and' >");
                xml.push("<condition attribute='bsd_segment' operator='eq' value='" + rs1[0].attributes.bsd_segment.guid + "' />");
                xml.push("<condition attribute='statecode' operator='eq' value='0' />");
                xml.push("</filter>");
                xml.push("</entity>");
                xml.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_typesegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_typesegmentid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                Xrm.Page.getControl("bsd_type").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            }
        }, function (er) {
            console.log(er.message)
        });
        Xrm.Page.getAttribute("bsd_type").setValue(null);
        Xrm.Page.getAttribute("bsd_tenrutngansieuthi").setValue(null);
        clear_tenrutngansieuthi();
    }
    if (segment == null) {
        Xrm.Page.getAttribute("bsd_type").setValue(null);
        Xrm.Page.getAttribute("bsd_tenrutngansieuthi").setValue(null);
        clear_tenrutngansieuthi();
        clear_type();
    }
}
//Author:Mr.phong
//Desciption:filter tenrutngansieuthi follow type
function filtertenrutngansieuthifollowtype() {
    var type = Xrm.Page.getAttribute("bsd_type").getValue();
    Xrm.Page.getControl("bsd_tenrutngansieuthi").removePreSearch(presearch_tenrutngansieuthi);
    var xml = [];
    var xml1 = [];
    if (type != null) {
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='bsd_typesegment'>");
        xml1.push("<attribute name='bsd_name' />")
        xml1.push("<attribute name='createdon' />");
        xml1.push("<attribute name='statecode' />");
        xml1.push("<attribute name='bsd_segment' />");
        xml1.push("<attribute name='bsd_type' />");
        xml1.push("<attribute name='bsd_typesegmentid' />");
        xml1.push("<order attribute='bsd_name' descending='false' />");
        xml1.push("<filter type='and' >");
        xml1.push("<condition attribute='bsd_typesegmentid' operator='eq' uitype='bsd_channelsegment' value='" + type[0].id + "' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
            if (rs1.length > 0) {
                var viewId = "{F32CC20D-C159-4E6B-8455-F73E2E25FE30}";
                var entityName = "bsd_typetrnst";
                var viewDisplayName = "test";
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml.push("<entity name='bsd_typetrnst'>");
                xml.push("<attribute name='bsd_typetrnstid'/>");
                xml.push("<attribute name='bsd_name' />")
                xml.push("<attribute name='createdon' />");
                xml.push("<attribute name='bsd_type' />");
                xml.push("<attribute name='statecode' />");
                xml.push("<attribute name='bsd_tenrutngansieuthi' />");
                xml.push("<order attribute='bsd_name' descending='false' />");
                xml.push("<filter type='and' >");
                xml.push("<condition attribute='bsd_type' operator='eq' value='" + rs1[0].attributes.bsd_type.guid + "' />");
                xml.push("<condition attribute='statecode' operator='eq' value='0' />");
                xml.push("</filter>");
                xml.push("</entity>");
                xml.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_typetrnst'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_typetrnst'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                Xrm.Page.getControl("bsd_tenrutngansieuthi").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            }
        }, function (er) {
            console.log(er.message)
        });
        Xrm.Page.getAttribute("bsd_tenrutngansieuthi").setValue(null);
    }
    else {
        Xrm.Page.getAttribute("bsd_tenrutngansieuthi").setValue(null);
        clear_tenrutngansieuthi();
    }
}

function clear_channel() {
    Xrm.Page.getControl("bsd_channel").addPreSearch(presearch_channel);
}
function presearch_channel() {
    Xrm.Page.getControl("bsd_channel").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_segment() {
    Xrm.Page.getControl("bsd_segment").addPreSearch(presearch_segment);
}
function presearch_segment() {
    Xrm.Page.getControl("bsd_segment").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_type() {
    Xrm.Page.getControl("bsd_type").addPreSearch(presearch_type);
}
function presearch_type() {
    Xrm.Page.getControl("bsd_type").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_tenrutngansieuthi() {
    Xrm.Page.getControl("bsd_tenrutngansieuthi").addPreSearch(presearch_tenrutngansieuthi);
}
function presearch_tenrutngansieuthi() {
    Xrm.Page.getControl("bsd_tenrutngansieuthi").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.Phong
//Description:filter channel follow account group
function filterchannelfollowaccountgroup() {
    var domesticexport = Xrm.Page.getAttribute("bsd_domesticexport").getValue();
    Xrm.Page.getControl("bsd_channel").removePreSearch(presearch_channel);
    Xrm.Page.getControl("bsd_segment").removePreSearch(presearch_segment);
    Xrm.Page.getControl("bsd_type").removePreSearch(presearch_type);
    Xrm.Page.getControl("bsd_tenrutngansieuthi").removePreSearch(presearch_tenrutngansieuthi);
    var xml = [];
    if (domesticexport != null) {
        var viewId = "{6CEC5866-1379-4FC1-BCE6-69E198ECE5F7}";
        var entityName = "bsd_accountgroupchannel";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_accountgroupchannel'>");
        xml.push("<attribute name='statecode'/>");
        xml.push("<attribute name='bsd_channel'/>");
        xml.push("<attribute name='bsd_domesticexport'/>");
        xml.push("<attribute name='bsd_name' />");
        xml.push("<attribute name='bsd_accountgroupchannelid' />");
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_domesticexport' operator='eq' value='" + domesticexport[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_accountgroupchannelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_accountgroupchannelid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_channel").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        Xrm.Page.getAttribute("bsd_channel").setValue(null);
        clear_segment();
        clear_type();
        clear_tenrutngansieuthi();
        Xrm.Page.getAttribute("bsd_tenrutngansieuthi").setValue(null);
        Xrm.Page.getAttribute("bsd_segment").setValue(null);
        Xrm.Page.getAttribute("bsd_type").setValue(null);
    }
    else {
        clear_channel();
        clear_type();
        clear_tenrutngansieuthi();
        clear_segment();
        Xrm.Page.getAttribute("bsd_channel").setValue(null);
        Xrm.Page.getAttribute("bsd_tenrutngansieuthi").setValue(null);
        Xrm.Page.getAttribute("bsd_segment").setValue(null);
        Xrm.Page.getAttribute("bsd_type").setValue(null);

    }
}
function domesticonload() {
    var channel = Xrm.Page.getAttribute("bsd_channel").getValue();
    var segment = Xrm.Page.getAttribute("bsd_segment").getValue();
    var domesticexport = Xrm.Page.getAttribute("bsd_domesticexport").getValue();
    var type = Xrm.Page.getAttribute("bsd_type").getValue();
    Xrm.Page.getControl("bsd_channel").removePreSearch(presearch_channel);
    Xrm.Page.getControl("bsd_segment").removePreSearch(presearch_segment);
    Xrm.Page.getControl("bsd_type").removePreSearch(presearch_type);
    Xrm.Page.getControl("bsd_tenrutngansieuthi").removePreSearch(presearch_tenrutngansieuthi);
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    if (channel != null) {
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='bsd_accountgroupchannel'>");
        xml1.push("<attribute name='bsd_accountgroupchannelid'/>");
        xml1.push("<attribute name='bsd_name' />")
        xml1.push("<attribute name='createdon' />");
        xml1.push("<attribute name='statecode' />");
        xml1.push("<attribute name='bsd_domesticexport' />");
        xml1.push("<attribute name='bsd_channel' />");
        xml1.push("<order attribute='bsd_name' descending='false' />");
        xml1.push("<filter type='and' >");
        xml1.push("<condition attribute='bsd_accountgroupchannelid' operator='eq' uitype='bsd_accountgroupchannel' value='" + channel[0].id + "' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
            if (rs1.length > 0) {
                var viewId = "{79D7AF1E-6AAB-45BC-8045-8786A2E2792D}";
                var entityName = "bsd_segmentchanel";
                var viewDisplayName = "test";
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml.push("<entity name='bsd_segmentchanel'>");
                xml.push("<attribute name='bsd_segmentchanelid'/>");
                xml.push("<attribute name='bsd_name' />")
                xml.push("<attribute name='createdon' />");
                xml.push("<attribute name='statecode' />");
                xml.push("<attribute name='bsd_segment' />");
                xml.push("<attribute name='bsd_channel' />");
                xml.push("<order attribute='bsd_name' descending='false' />");
                xml.push("<filter type='and' >");
                xml.push("<condition attribute='bsd_channel' operator='eq' value='" + rs1[0].attributes.bsd_channel.guid + "' />");
                xml.push("<condition attribute='statecode' operator='eq' value='0' />");
                xml.push("</filter>");
                xml.push("</entity>");
                xml.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_channelsegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_channelsegmentid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                Xrm.Page.getControl("bsd_segment").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
                Xrm.Page.getAttribute("bsd_segment").setValue(null);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    else if (channel == null) {
        clear_segment();
    }
    if (domesticexport != null) {
        var viewId2 = "{6CEC5866-1379-4FC1-BCE6-69E198ECE5F7}";
        var entityName2 = "bsd_accountgroupchannel";
        var viewDisplayName2 = "test";
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml2.push("<entity name='bsd_accountgroupchannel'>");
        xml2.push("<attribute name='statecode'/>");
        xml2.push("<attribute name='bsd_channel'/>");
        xml2.push("<attribute name='bsd_domesticexport'/>");
        xml2.push("<attribute name='bsd_name' />");
        xml2.push("<attribute name='bsd_accountgroupchannelid' />");
        xml2.push("<attribute name='createdon' />");
        xml2.push("<order attribute='bsd_name' descending='false' />");
        xml2.push("<filter type='and' >");
        xml2.push("<condition attribute='bsd_domesticexport' operator='eq' value='" + domesticexport[0].id + "' />");
        xml2.push("</filter>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_accountgroupchannelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_accountgroupchannelid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_channel").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
    }
    else if (domesticexport == null) {
        clear_channel();
    }
    if (segment != null) {
        xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml3.push("<entity name='bsd_channelsegment'>");
        xml3.push("<attribute name='bsd_name' />")
        xml3.push("<attribute name='createdon' />");
        xml3.push("<attribute name='statecode' />");
        xml3.push("<attribute name='bsd_segment' />");
        xml3.push("<attribute name='bsd_channel' />");
        xml3.push("<attribute name='bsd_channelsegmentid' />");
        xml3.push("<order attribute='bsd_name' descending='false' />");
        xml3.push("<filter type='and' >");
        xml3.push("<condition attribute='bsd_channelsegmentid' operator='eq' uitype='bsd_channelsegment' value='" + segment[0].id + "' />");
        xml3.push("</filter>");
        xml3.push("</entity>");
        xml3.push("</fetch>");
        CrmFetchKit.Fetch(xml3.join(""), false).then(function (rs3) {
            if (rs3.length > 0) {
                var viewId4 = "{F32CC20D-C159-4E6B-8455-F73E2E25FE30}";
                var entityName4 = "bsd_typesegment";
                var viewDisplayName4 = "test";
                xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml4.push("<entity name='bsd_typesegment'>");
                xml4.push("<attribute name='bsd_typesegmentid'/>");
                xml4.push("<attribute name='bsd_name' />")
                xml4.push("<attribute name='createdon' />");
                xml4.push("<attribute name='bsd_type' />");
                xml4.push("<attribute name='statecode' />");
                xml4.push("<attribute name='bsd_segment' />");
                xml4.push("<order attribute='bsd_name' descending='false' />");
                xml4.push("<filter type='and' >");
                xml4.push("<condition attribute='bsd_segment' operator='eq' value='" + rs3[0].attributes.bsd_segment.guid + "' />");
                xml4.push("<condition attribute='statecode' operator='eq' value='0' />");
                xml4.push("</filter>");
                xml4.push("</entity>");
                xml4.push("</fetch>");
                var layoutXml4 = "<grid name='resultset' " + "object='1' " + "jump='bsd_typesegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_typesegmentid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                Xrm.Page.getControl("bsd_type").addCustomView(viewId4, entityName4, viewDisplayName4, xml4.join(""), layoutXml4, true);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    else if (segment == null) {
        clear_type();
    }
    if (type!=null) {
        xml5.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml5.push("<entity name='bsd_typesegment'>");
        xml5.push("<attribute name='bsd_name' />")
        xml5.push("<attribute name='createdon' />");
        xml5.push("<attribute name='statecode' />");
        xml5.push("<attribute name='bsd_segment' />");
        xml5.push("<attribute name='bsd_type' />");
        xml5.push("<attribute name='bsd_typesegmentid' />");
        xml5.push("<order attribute='bsd_name' descending='false' />");
        xml5.push("<filter type='and' >");
        xml5.push("<condition attribute='bsd_typesegmentid' operator='eq' uitype='bsd_channelsegment' value='" + type[0].id + "' />");
        xml5.push("</filter>");
        xml5.push("</entity>");
        xml5.push("</fetch>");
        CrmFetchKit.Fetch(xml5.join(""), false).then(function (rs5) {
            if (rs5.length > 0) {
                var viewId6 = "{F32CC20D-C159-4E6B-8455-F73E2E25FE30}";
                var entityName6 = "bsd_typetrnst";
                var viewDisplayName6 = "test";
                xml6.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml6.push("<entity name='bsd_typetrnst'>");
                xml6.push("<attribute name='bsd_typetrnstid'/>");
                xml6.push("<attribute name='bsd_name' />")
                xml6.push("<attribute name='createdon' />");
                xml6.push("<attribute name='bsd_type' />");
                xml6.push("<attribute name='statecode' />");
                xml6.push("<attribute name='bsd_tenrutngansieuthi' />");
                xml6.push("<order attribute='bsd_name' descending='false' />");
                xml6.push("<filter type='and' >");
                xml6.push("<condition attribute='bsd_type' operator='eq' value='" + rs5[0].attributes.bsd_type.guid + "' />");
                xml6.push("<condition attribute='statecode' operator='eq' value='0' />");
                xml6.push("</filter>");
                xml6.push("</entity>");
                xml6.push("</fetch>");
                var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='bsd_typetrnst'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_typetrnst'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                Xrm.Page.getControl("bsd_tenrutngansieuthi").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    else if (type==null) {
        clear_tenrutngansieuthi();
    }
}
//Author:Mr.Phong
//Description:set value filed name
function set_filename()
{
    var domesticexport = Xrm.Page.getAttribute("bsd_domesticexport").getValue();
    if (domesticexport != null)
    {
        Xrm.Page.getAttribute("bsd_name").setValue(domesticexport[0].name);
    }
    
}
function set_disablefieldaccount()
{
    var customer = getValue("bsd_customer");
    if (customer == null)
    {
        setDisabled("bsd_customer", false)
    }
    else {
        setDisabled("bsd_customer", true)
    }
}