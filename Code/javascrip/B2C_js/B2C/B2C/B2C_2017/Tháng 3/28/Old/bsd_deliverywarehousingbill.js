/*
    Fix: 
    - 18/02/2017: ẩn hiện subrib packing list hoac order dms.
    - 22/2/2017: load packinglist. 
    - 27/2/2017: thêm type good output, load các field theo type.
*/


//Staff: Mr.Diệm
//Note: set disabled goodinput and goodoutput
function AutoLoad() {
    debugger;
    setValue("bsd_transationtype", 861450000);
    checkexitField();
    Lk_Account_Distributor_Change();
    if (formType() == 1) {
        debugger;
        Lk_DistributorChange(false);
        LoadreturnDistributor();
    }
    else {
        Lk_DistributorChange(false);
        groupoutputHideAnhShowEntity();
        if (getValue("bsd_statusorderdms") || getValue("bsd_statuswarehousingbill")) /*Xac nhan nhan hang hoac da chuyen kho khóa tat ca field.*/ {
            DisabledForm();
        }
    }
}
/*Diệm*/
function LK_Load_PackingListS2S() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '  <entity name="bsd_packinglists2s">',
                    '    <attribute name="bsd_packinglists2sid" />',
                    '    <attribute name="bsd_name" />',
                    '    <attribute name="createdon" />',
                    '    <order attribute="bsd_name" descending="false" />',
                    '    <filter type="and">',
                    '      <condition attribute="bsd_status" operator="eq" value="861450003" />',
                    '      <condition attribute="statecode" operator="eq" value="0" />',
                    '      <condition attribute="bsd_statusdeliverybills2s" operator="eq" value="0" />',
                    '    </filter>',
                    '  </entity>',
                    '</fetch>'].join();
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_packinglists2sid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_packinglists2sid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
    getControl("bsd_packinglists2s").addCustomView(getControl("bsd_packinglists2s").getDefaultView(), "bsd_packinglists2s", "PackingListS2S", fetchxml, layoutXml, true);
}


/*Diệm*/
function Lk_Account_Distributor_Change() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name"/>',
                    '<attribute name="primarycontactid"/>',
                    '<attribute name="telephone1"/>',
                    '<attribute name="accountid"/>',
                    '<order attribute="name" descending="false"/>',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="100000000"/>',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                          "<row name='result'  " + "id='accountid'>  " +
                          "<cell name='name'   " + "width='200' />  " +
                          "<cell name='telephone1'    " + "width='100' />  " +
                          "</row>   " +
                       "</grid>   ";
    getControl("bsd_account").addCustomView("{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}", "account", "Distributor", fetchxml, layoutXml, true);
}
/*
    Diệm
    Ẩn hiện packing list và orderDms
    Ngày: 17/02/2017
*/

function groupoutputHideAnhShowEntity() {
    var goodoutput = getValue("bsd_goodoutputform");
    console.log(goodoutput);
    if (goodoutput != null) {
        var fetchgoodoutputxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                        '    <entity name="bsd_goodoutputform">',
                                        '      <attribute name="bsd_goodoutputformid" />',
                                        '      <attribute name="bsd_type" />',
                                        '      <order attribute="bsd_type" descending="false" />',
                                        '      <filter type="and">',
                                        '        <condition attribute="bsd_goodoutputformid" operator="eq" uitype="bsd_goodoutputform" value="' + goodoutput[0].id + '" />',
                                        '      </filter>',
                                        '    </entity>',
                                        '  </fetch>'].join('');
        CrmFetchKit.Fetch(fetchgoodoutputxml, false).then(function (rsgoodoutput) {
            if (rsgoodoutput.length > 0) {
                if (rsgoodoutput[0].attributes.bsd_type.value == 861450000) /*orderdms*/ {
                    Xrm.Page.ui.tabs.get("{ad9497a2-6149-457e-9771-8886e8cc8592}").sections.get("{ad9497a2-6149-457e-9771-8886e8cc8592}_section_2").setVisible(true);// ngoài là general cuối là section
                    Xrm.Page.ui.tabs.get("{ad9497a2-6149-457e-9771-8886e8cc8592}").sections.get("{ad9497a2-6149-457e-9771-8886e8cc8592}_section_3").setVisible(false);// ngoài là general cuối là section
                } else /*packinglists2s*/ {
                    Xrm.Page.ui.tabs.get("{ad9497a2-6149-457e-9771-8886e8cc8592}").sections.get("{ad9497a2-6149-457e-9771-8886e8cc8592}_section_3").setVisible(true);// ngoài là general cuối là section
                    Xrm.Page.ui.tabs.get("{ad9497a2-6149-457e-9771-8886e8cc8592}").sections.get("{ad9497a2-6149-457e-9771-8886e8cc8592}_section_2").setVisible(false);// ngoài là general cuối là section
                }
            }
        });
    }
}

//Staff: Mr.Diệm
//Note: Check field goodinput or goodoutput is exits to visible
function checkexitField() {
    debugger;
    var idtransstiontype = getValue("bsd_transationtype");
    if (idtransstiontype != null) {
        var goodoutputform = getValue("bsd_goodoutputform");
        if (goodoutputform != null) {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '  <entity name="bsd_goodoutputform">',
                            '    <attribute name="bsd_goodoutputformid" />',
                            '    <attribute name="bsd_typechange" />',
                            '    <attribute name="bsd_type" />',
                            '    <order attribute="bsd_typechange" descending="false" />',
                            '    <filter type="and">',
                            '      <condition attribute="bsd_goodoutputformid" operator="eq" uitype="bsd_goodoutputform" value="' + goodoutputform[0].id + '" />',
                            '      <condition attribute="statecode" operator="eq" value="0" />',
                            '    </filter>',
                            '  </entity>',
                            '</fetch>'].join();

            CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
                if (rs.length > 0) {
                    if (rs[0].attributes.bsd_type.value == 861450000) /*order DMS*/ {
                        setVisible(["bsd_packinglists2s", "bsd_returndistributors2s", "bsd_statuswarehousingbill"], false);
                        setNull(["bsd_packinglists2s", "bsd_returndistributors2s"]);
                        if (getValue("bsd_statuswarehousingbill") != null) {
                            setNull("bsd_statuswarehousingbill");
                        }
                        setRequired(["bsd_packinglists2s", "bsd_returndistributors2s"], "none");
                        setVisible(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_distributoraddress", "bsd_statusorderdms", "bsd_account", "bsd_warehouse"], true);
                        setRequired(["bsd_order", "bsd_distributoraddress", "bsd_account", "bsd_warehouse"], "required");
                        if (getValue("bsd_statusorderdms") == null) {
                            setValue("bsd_statusorderdms", false);
                        }


                    } else if (rs[0].attributes.bsd_type.value == 861450001)/*packinglists2s*/ {
                        setVisible(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_distributoraddress", "bsd_returndistributors2s", "bsd_statusorderdms", "bsd_statuswarehousingbill"], false);
                        setNull(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_distributoraddress", "bsd_returndistributors2s"]);
                        if (getValue("bsd_statusorderdms") != null && getValue("bsd_statuswarehousingbill") != null) {
                            setNull(["bsd_statusorderdms", "bsd_statuswarehousingbill"]);
                        }
                        setRequired(["bsd_order", "bsd_distributoraddress", "bsd_returndistributors2s"], "none");
                        setRequired(["bsd_packinglists2s", "bsd_account", "bsd_warehouse"], "required");
                        setVisible(["bsd_packinglists2s", "bsd_account", "bsd_warehouse"], true);
                    } else if (rs[0].attributes.bsd_type.value == 861450002) /*return  distributor*/ {
                        setVisible(["bsd_returndistributors2s", "bsd_account", "bsd_warehouse"], true);
                        setRequired(["bsd_returndistributors2s", "bsd_account", "bsd_warehouse"], "required");
                        setNull(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_distributoraddress", "bsd_packinglists2s"]);
                        if (getValue("bsd_statusorderdms") != null && getValue("bsd_statuswarehousingbill") != null) {
                            setNull(["bsd_statusorderdms", "bsd_statuswarehousingbill"]);
                        }
                        setVisible(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_distributoraddress", "bsd_packinglists2s", "bsd_statusorderdms", "bsd_statuswarehousingbill"], false);
                        setRequired(["bsd_order", "bsd_distributoraddress", "bsd_packinglists2s"], "none");

                    } else if (rs[0].attributes.bsd_type.value == 861450003)/*delivery bill s2s*/ {
                        setVisible(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_packinglists2s", "bsd_distributoraddress", "bsd_returndistributors2s", "bsd_statusorderdms"], false);
                        setNull(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_packinglists2s", "bsd_distributoraddress", "bsd_returndistributors2s"]);
                        if (getValue("bsd_statusorderdms") != null) {
                            setNull("bsd_statusorderdms");
                        }
                        setRequired(["bsd_packinglists2s", "bsd_order", "bsd_distributoraddress", "bsd_returndistributors2s", "bsd_returndistributors2s"], "none");
                        setVisible(["bsd_account", "bsd_warehouse", "bsd_statuswarehousingbill"], true);
                        setRequired(["bsd_account", "bsd_warehouse"], "required");
                        if (getValue("bsd_statuswarehousingbill") == null) {
                            setValue("bsd_statuswarehousingbill", false);
                        }
                    } else if (rs[0].attributes.bsd_type.value == 861450004) /*none*/ {
                        setVisible(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_packinglists2s", "bsd_distributoraddress", "bsd_account", "bsd_warehouse", "bsd_statuswarehousingbill", "bsd_statusorderdms"], false);
                        setNull(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_packinglists2s", "bsd_distributoraddress", "bsd_account", "bsd_warehouse", "bsd_statusorderdms", "bsd_statuswarehousingbill"]);
                        setRequired(["bsd_packinglists2s", "bsd_order", "bsd_distributoraddress", "bsd_account", "bsd_warehouse"], "none");
                    }
                }
            });
        } else {
            setVisible(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_packinglists2s", "bsd_distributoraddress", "bsd_account", "bsd_warehouse", "bsd_statusorderdms", "bsd_statuswarehousingbill"], false);
            setNull(["bsd_order", "bsd_customer", "bsd_shiptoaddress", "bsd_packinglists2s", "bsd_distributoraddress", "bsd_account", "bsd_warehouse", "bsd_statusorderdms", "bsd_statuswarehousingbill"]);
            setRequired(["bsd_packinglists2s", "bsd_order", "bsd_distributoraddress", "bsd_account", "bsd_warehouse"], "none");
        }
        groupoutputHideAnhShowEntity();
    }
}
/*
    Diệm
    load return distributor.
*/
function LoadreturnDistributor() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '  <entity name="bsd_returndistributors2s">',
                    '    <attribute name="bsd_returndistributors2sid" />',
                    '    <attribute name="bsd_code" />',
                    '    <attribute name="createdon" />',
                    '    <order attribute="bsd_name" descending="false" />',
                    '    <filter type="and">',
                    '      <condition attribute="statecode" operator="eq" value="0" />',
                    '      <condition attribute="bsd_statusdeliverybills2s" operator="eq" value="0" />',
                    '      <condition attribute="bsd_statusreturndistributor" operator="eq" value="861450001" />',
                    '    </filter>',
                    '  </entity>',
                    '</fetch>'].join('');
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_returndistributors2sid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_returndistributors2sid'>  " +
                     "<cell name='bsd_code'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>";
    getControl("bsd_returndistributors2s").addCustomView(getControl("bsd_returndistributors2s").getDefaultView(), "bsd_returndistributors2s", "Return Distributor", fetchxml, layoutXml, true);
}

//Mr:Diệm
//Note:Load warehouse follow distributor
function Lk_DistributorChange(reset) {
    debugger;
    if (reset != false) setNull(["bsd_warehouse"]);
    getControl("bsd_warehouse").removePreSearch(presearch_warehouse);
    var distributor = getValue("bsd_account");
    if (distributor != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_warehousedms">',
                        '<attribute name="bsd_warehousedmsid"/>',
                        '<attribute name="bsd_name"/>',
                        '<attribute name="bsd_typewarehousedms"/>',
                        '<attribute name="createdon"/>',
                        '<order attribute="bsd_name" descending="false"/>',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + distributor[0].id + '"/>',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            if (rs.length > 0) {
                for (var i = 0; i < rs.length; i++) {
                    var first = rs[i];
                    if (setWarehouseDMSDefaut(first)) {
                        if (getValue("bsd_warehouse") == null) {
                            setValue("bsd_warehouse", [{
                                id: first.Id,
                                name: first.attributes.bsd_name.value,
                                entityType: first.logicalName
                            }]
                            );
                        }
                    }
                }

            }
        });
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_warehousedmsid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_warehousedmsid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "</row>   " +
                        "</grid>";

        getControl("bsd_warehouse").addCustomView(getControl("bsd_warehouse").getDefaultView(), "bsd_warehousedms", "WareHouseDMS", fetchxml, layoutXml, true);

    }

    else if (reset != false) {
        clear_warehouse();
    }

}

function setWarehouseDMSDefaut(first) /*Kiểm tra typewarehouse là distributor*/ {
    if (first.attributes.bsd_typewarehousedms.value == 861450000)// nếu có tra về true
        return true;
    return false;
}

//Set Function
function clear_warehouse() {
    getControl("bsd_warehouse").addPreSearch(presearch_warehouse);
}
function presearch_warehouse() {
    getControl("bsd_warehouse").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
/*
    Diệm
    khóa tất cả field khi save 
*/
function SaveAuto() {
    DisabledForm();
}

/*
    Diệm
    Ngày 22/2/2017: xuất hóa đơn trước khi cập nhập quantity net.
 */
function BtncreateInvoiceDMS() {
    var id = Xrm.Page.data.entity.getId();
    if (id != null && id != "") {
        var fetchdeliverybills2s = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                    '  <entity name="bsd_delivery_warehousingbill">',
                                    '    <attribute name="bsd_delivery_warehousingbillid" />',
                                    '    <attribute name="bsd_order" />',
                                    '    <order attribute="bsd_order" descending="false" />',
                                    '    <filter type="and">',
                                    '      <condition attribute="bsd_delivery_warehousingbillid" operator="eq" uitype="bsd_delivery_warehousingbill" value="' + id + '" />',
                                    '    </filter>',
                                    '  </entity>',
                                    '</fetch>'].join();
        CrmFetchKit.Fetch(fetchdeliverybills2s, false).then(function (rsDLB) {
            var orderid = rsDLB[0].attributes.bsd_order.guid;
            if (orderid != null && orderid != '') {
                var fetchxmlorderdms = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                        '  <entity name="bsd_orderdms">',
                                        '    <attribute name="bsd_orderdmsid" />',
                                        '    <attribute name="bsd_statusinvoicedms" />',
                                        '    <attribute name="bsd_status" />',
                                        '    <order attribute="bsd_statusinvoicedms" descending="false" />',
                                        '    <filter type="and">',
                                        '      <condition attribute="bsd_orderdmsid" operator="eq" uiname="uyy" uitype="bsd_orderdms" value="' + orderid + '" />',
                                        '    </filter>',
                                        '  </entity>',
                                        '</fetch>'].join('');

                CrmFetchKit.Fetch(fetchxmlorderdms, false).then(function (rsorderdms) {
                    console.log(rsorderdms);
                    if (!rsorderdms[0].attributes.bsd_statusinvoicedms.value) {
                        window.top.$ui.Confirm("Confirm", "Do you want to create a DMS sales bill ?", function (e) {
                            ExecuteAction(
                            id
                            , "bsd_delivery_warehousingbill"
                            , "bsd_Action_createInvoiceDMS"/*Name Action trong process*/
                            , null//[{ name: 'ReturnId', type: 'string', value: null }]
                            , function (result) {
                                //debugger;

                                if (result != null && result.status != null) {
                                    if (result.status == "error")
                                        window.top.$ui.Dialog("Lỗi", result.data);
                                    else if (result.status == "success") {
                                        window.parent.Xrm.Utility.openEntityForm("bsd_invoice", result.data.ReturnId.value, null, { openInNewWindow: false });
                                    }
                                    else {
                                        console.log(JSON.stringify(result));
                                    }
                                }
                            });
                        }, null);
                    }
                    else {
                        m_alert("This bill has been invoiced.");
                    }
                });
            }
        });
    }
}
