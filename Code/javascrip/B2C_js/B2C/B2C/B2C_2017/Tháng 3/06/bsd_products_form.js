﻿//Author:Mr.Đăng
//Description:Check ProductID & UpperCase
function Check_ProductID() {
    var productid = getValue("productnumber");
    var id = getId();
    if (productid != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="product">',
                        '<attribute name="name" />',
                        '<attribute name="productid" />',
                        '<attribute name="productnumber" />',
                        '<order attribute="name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="productnumber" operator="eq" value="' + productid + '" />',
                        '<condition attribute="productid" operator="ne" value="' + id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setNotification("productnumber", "Mã đã tồn tại, vui lòng kiểm tra lại.");
        } else {
            clearNotification("productnumber");
            setValue("productnumber", productid.toUpperCase());
        }
    }
}