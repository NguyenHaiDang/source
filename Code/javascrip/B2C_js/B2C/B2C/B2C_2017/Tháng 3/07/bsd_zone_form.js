﻿//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    if (formType() == 2) {
        LK_Country_Change(false);
    } else {
        clear_region();
        clear_area();
        clear_province();
        clear_district();
    }
}
function LK_Country_Change(result) {
    debugger;
    if (result != false) setNull(["bsd_region", "bsd_area", "bsd_province", "bsd_district"]);
    LK_Region_Change(result);
    getControl("bsd_region").removePreSearch(presearch_region);
    var country = getValue("bsd_country");
    if (country != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_region">',
                        '<attribute name="bsd_regionid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_country" operator="eq" uitype="bsd_country" value="' + country[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_region", "bsd_region", "bsd_name", "Region", fetchxml);
    }
    else if (result != false) {
        clear_region();
    }
}
function LK_Region_Change(result) {
    debugger;
    if (result != false) setNull(["bsd_area", "bsd_province", "bsd_district"]);
    LK_Area_Change(result);
    getControl("bsd_area").removePreSearch(presearch_area);
    var region = getValue("bsd_region");
    if (region != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_areab2c">',
                        '<attribute name="bsd_areab2cid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_region" operator="eq" uiname="Miền nam" uitype="bsd_region" value="' + region[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_area", "bsd_areab2c", "bsd_name", "Area", fetchxml);
    }
    else if (result != false) {
        clear_area();
    }
}
function LK_Area_Change(result) {
    debugger;
    if (result != false) setNull(["bsd_province", "bsd_district"]);
    LK_Province_Change(result);
    getControl("bsd_province").removePreSearch(presearch_province);
    var area = getValue("bsd_area");
    if (area != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_province">',
                        '<attribute name="bsd_provinceid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_area" operator="eq" uitype="bsd_areab2b" value="' + area[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_province", "bsd_province", "bsd_name", "Province", fetchxml);
    }
    else if (result != false) {
        clear_province();
    }
}
function LK_Province_Change(result) {
    debugger;
    getControl("bsd_district").removePreSearch(presearch_district);
    if (result != false) setNull("bsd_district");
    var province = getValue("bsd_province");
    if (province != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_district">',
                        '<attribute name="bsd_districtid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_province" operator="eq" uitype="bsd_province" value="' + province[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_district", "bsd_district", "bsd_name", "District", fetchxml);
    }
    else if (result != false) {
        clear_district();
    }
}
function clear_district() {
    getControl("bsd_district").addPreSearch(presearch_district);
}
function presearch_district() {
    getControl("bsd_district").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_province() {
    getControl("bsd_province").addPreSearch(presearch_province);
}
function presearch_province() {
    getControl("bsd_province").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_area() {
    getControl("bsd_area").addPreSearch(presearch_area);
}
function presearch_area() {
    getControl("bsd_area").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_region() {
    getControl("bsd_region").addPreSearch(presearch_region);
}
function presearch_region() {
    getControl("bsd_region").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}