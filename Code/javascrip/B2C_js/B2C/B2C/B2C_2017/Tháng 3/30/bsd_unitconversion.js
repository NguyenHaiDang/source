function init() {
    setDisabled_Product();
    if (formType() == 1) {
        if (getValue("bsd_product") != null) {
            product_change();
        }
    } else if (formType() == 2) {
        setDisabled(["bsd_product", "bsd_tounit"], true);
    }
}

function product_change(reset) {
    var product = getValue("bsd_product");
    clearNotification("bsd_fromunit");
    if (product != null) {

        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_unitconversions">',
                        '<attribute name="bsd_unitconversionsid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<attribute name="bsd_fromunit" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_product" operator="eq" uitype="product" value="' + product[0].id + '" />',
                        '</filter>',
                        '<link-entity name="product" from="productid" to="bsd_product" alias="ab">',
                          '<attribute name="productnumber" />',
                        '</link-entity>',
                      '</entity>',
                    '</fetch>'].join("");


        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            var xml2 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="product">',
                         '   <attribute name="name" />',
                          '  <attribute name="productnumber" />',
                          '  <attribute name="defaultuomid" />',
                          '  <filter type="and">',
                          '    <condition attribute="productid" operator="eq" uitype="product" value="' + product[0].id + '" />',
                          '  </filter>',
                          '</entity>',
                        '</fetch>'].join("");

            CrmFetchKit.Fetch(xml2, false).then(function (rs) {

                if (rs.length > 0) {
                    console.log(rs[0]);
                    setValue("bsd_name", rs[0].getValue("productnumber"));
                    setValue("bsd_fromunit", [{
                        id: rs[0].getValue("defaultuomid"),
                        name: rs[0].attributes.defaultuomid.name,
                        entityType: "uom"
                    }]);

                }
            });
        });


        tounit_change();
    } else {
        setNull(["bsd_name", "bsd_fromunit"]);
    }

}

function fromunit_change(reset) {
    check_fromunit_tounit();
}

function tounit_change(reset) {
    clearNotification("bsd_tounit");

    var tounit = getValue("bsd_tounit");
    var product = getValue("bsd_product");
    var fromunit = getValue("bsd_fromunit");
    if (tounit != null && product != null && fromunit != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
         ' <entity name="bsd_unitconversions">',
           ' <attribute name="bsd_unitconversionsid" />',
           ' <filter type="and">',
           '   <condition attribute="bsd_fromunit" operator="eq" uitype="uom" value="' + fromunit[0].id + '" />',
            '  <condition attribute="bsd_product" operator="eq" uitype="product" value="' + product[0].id + '" />',
           '   <condition attribute="bsd_tounit" operator="eq" uitype="uom" value="' + tounit[0].id + '" />',
          '  </filter>',
         ' </entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                setNotification("bsd_tounit", "Đã chọn rồi!");
            }
        });
    }
    check_fromunit_tounit();
}

function factor_change() {
    var factor = getValue("bsd_factor");
    clearNotification("bsd_factor");
    if (factor <= 0) {
        setNotification("bsd_factor", "Factor must not less than or equal 0");
    }
}

function check_fromunit_tounit() {
    clearNotification("bsd_tounit");

    var fromunit = getValue("bsd_fromunit");
    var tounit = getValue("bsd_tounit");
    if (fromunit != null && tounit != null) {
        var fromunit_id = fromunit[0].id.toLowerCase().replace("}", "").replace("{", "");
        var tounit_id = tounit[0].id.toLowerCase().replace("}", "").replace("{", "");
        if (fromunit_id == tounit_id) {
            setNotification("bsd_tounit", "To Unit must be different From Unit");
        }
    }
}

function check_fromunit() {
    if (getValue("bsd_fromunit") == null) {
        setNotification("bsd_fromunit", "You must provide a value for From Unit");
    } else {
        clearNotification("bsd_fromunit");
    }
}

function preventAutoSave(econtext) {
    check_fromunit();
}
// 30.03.2017 Đăng: setDisabled Product
function setDisabled_Product() {
    var product = getValue("bsd_product");
    if (product != null) {
        setDisabled("bsd_product", true);
    }
}