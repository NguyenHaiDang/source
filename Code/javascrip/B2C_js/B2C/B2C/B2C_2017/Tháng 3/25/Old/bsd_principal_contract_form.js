//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    //var numberprocess = getValue("bsd_numberprocess");
    //var numberprocessctq = getValue("bsd_numberprocessctq");
    //if (numberprocess == null && numberprocessctq == null) {
    //    setValue("bsd_numberprocessctq", false);
    //    setValue("bsd_numberprocess", false);
    //}

    //set_DisableWarehouse();
    if (formType() == 2) {
        set_DisableWarehouse(false);
        khachhang_change(false);
        set_defaultaddress();
        get_allbankaccountfromsitebonload();
        filter_bankaccounta();
        setvalue_sectionBHSonload();
    } else {
        setValueDefault();
        clear_AddressDeliveryShipping();
        clear_addressaccount();
    }
    setvalue_sectionBHS();
    //set_hidebutton();
    //changestatuswhenstagefinalonload();
    //switchProcess();
    //switchProcessctq();
    filter_customerhaveprincipalcontract();
    setdisableallfield();

}
function set_hidebutton() {
    window.parent.document.getElementById("stageBackActionContainer").style.display = "none";
    window.parent.document.getElementById("stageBackActionContainer").style.visibility = "hidden";
    window.parent.document.getElementById("stageAdvanceActionContainer").style.display = "none";
    window.parent.document.getElementById("stageAdvanceActionContainer").style.visibility = "hidden";
    window.parent.document.getElementById("stageSetActiveActionContainer").style.display = "none";
    window.parent.document.getElementById("stageSetActiveActionContainer").style.visibility = "hidden";
}
//Description:load thong tin cua khach hang
function set_defaultaddress() {
    var customer = getValue("bsd_account");
    var xmladdress_CB = [];
    var xmladdress_C = [];
    var xmlprimary_C = [];
    var xmlre_C = [];
    getControl("bsd_address").removePreSearch(presearch_addressaccount);
    getControl("bsd_nguoidaidienb").removePreSearch(presearch_contactaccount);
    if (customer != null) {
        //load dia chi cua khach hang     
        fetch(xmladdress_CB, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
           ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, customer[0].id]);
        CrmFetchKit.Fetch(xmladdress_CB.join(""), true).then(function (rsaddress_CB) {
            if (rsaddress_CB.length > 0) {
                LookUp_After_Load("bsd_address", "bsd_address", "bsd_name", "Customer Address", xmladdress_CB.join(""));
            }
            else {
                fetch(xmladdress_C, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                LookUp_After_Load("bsd_address", "bsd_address", "bsd_name", "Customer Address", xmladdress_C.join(""));
            }
        });
        //load nguoi dai dien cu khach hang
        xmlprimary_C.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xmlprimary_C.push("<entity name='account'>");
        xmlprimary_C.push("<attribute name='name' />");
        xmlprimary_C.push("<attribute name='primarycontactid'/>");
        xmlprimary_C.push("<attribute name='telephone1'/>");
        xmlprimary_C.push("<attribute name='accountid'/>");
        xmlprimary_C.push("<order attribute='name' descending='false'/> ");
        xmlprimary_C.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
        xmlprimary_C.push("<attribute name='jobtitle'/>");
        xmlprimary_C.push("<filter type='and'>");
        xmlprimary_C.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "'/>");
        xmlprimary_C.push("</filter>");
        xmlprimary_C.push("</link-entity>");
        xmlprimary_C.push("</entity>");
        xmlprimary_C.push("</fetch>");
        CrmFetchKit.Fetch(xmlprimary_C.join(""), true).then(function (rsprimary_C) {
            if (rsprimary_C.length > 0) {
                fetch(xmlre_C, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                 ["parentcustomerid", "statecode"], ["eq", "eq"], [0, customer[0].id, 1, "0"]);
                LookUp_After_Load("bsd_nguoidaidienb", "contact", "fullname", "Customer Contact", xmlre_C.join(""));
            }
            else {
                fetch(xmlre_C, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                ["parentcustomerid", "statecode"], ["eq", "eq"], [0, customer[0].id, 1, "0"]);
                LookUp_After_Load("bsd_nguoidaidienb", "contact", "fullname", "Customer Contact", xmlre_C.join(""));
            }
        });
    }
    else {
        clear_addressaccount();
        clear_contactaccount();
    }
}
//Description:lay thong tin tu nguoi dai dien ben b
function get_thongtinchucvucustomer() {
    var present = getValue("bsd_nguoidaidienb");
    var xmljob_C = [];
    if (present != null) {
        fetch(xmljob_C, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
               ["contactid"], ["eq"], [0, present[0].id, 1]);
        CrmFetchKit.Fetch(xmljob_C.join(""), true).then(function (rsjob_C) {
            if (rsjob_C.length > 0) {
                if (rsjob_C[0].attributes.jobtitle != null) {
                    setValue("bsd_jobtitle", rsjob_C[0].attributes.jobtitle.value);
                }
            }
        });
    }
    if (present == null) {
        setNull("bsd_jobtitle");
    }
}
//Description:filter bank account theo ben A
function filter_bankaccounta() {
    var trader = getValue("bsd_renter");
    var xmlbankaccount_T = [];
    getControl("bsd_bankaccounta").removePreSearch(presearch_LoadBankAccountTrader);
    if (trader != null) {
        fetch(xmlbankaccount_T, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand"
                            , "bsd_bankaccount", "bsd_account", "bsd_bankgroup"]
                            , ["bsd_name"], false, null,
                            ["bsd_account", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
        LookUp_After_Load("bsd_bankaccounta", "bsd_bankaccount", "bsd_name", "Bank Account Trader", xmlbankaccount_T.join(""));
    }
    else {
        setNull(["bsd_bankaccounta", "bsd_bankgroupa", "bsd_bankaccountnumbera", "bsd_branda"]);
        clear_LoadBankAccountTrader();
    }
}
//Description:filter bank account theo ben b
function get_allbankaccountfromsiteboncreate() {
    if (formType() == 1) {
        var bankaccount = getAttribute("bsd_bankaccountb");
        var param = Xrm.Page.context.getQueryStringParameters();
        var param1 = param["regarding_Id"];
        var bankgroup = Xrm.Page.getAttribute("bsd_bankgroupb");
        var bankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumberb");
        var brand = Xrm.Page.getAttribute("bsd_brandb");
        if (param1 != null) {
            var source = param["regarding_Id"].toString();
            var xml = [];
            try {
                if (source != null) {
                    var viewId = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
                    var entityName = "bsd_bankaccount";
                    var viewDisplayName = "test";
                    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                    xml.push("<entity name='bsd_bankaccount'>");
                    xml.push("<attribute name='bsd_name'/>");
                    xml.push("<attribute name='createdon'/>");
                    xml.push("<attribute name='bsd_brand'/>");
                    xml.push("<attribute name='bsd_bankaccount'/>");
                    xml.push("<attribute name='bsd_account'/>");
                    xml.push("<attribute name='bsd_bankaccountid'/>");
                    xml.push("<attribute name='bsd_bankgroup'/>");
                    xml.push("<order attribute='bsd_name' descending='true' />");
                    xml.push("<filter type='and'>");
                    xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='" + source + "' />");
                    xml.push("<condition attribute='statecode' operator='eq'  value='0' />");
                    xml.push("</filter>");
                    xml.push("</entity>");
                    xml.push("</fetch>");
                    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
       "<row name='result'  " + "id='bsd_bankaccountid'>  " +
       "<cell name='bsd_name'   " + "width='200' />  " +
       "<cell name='createdon'    " + "width='100' />  " +
       "<cell name='bsd_brand'    " + "width='100' />  " +
       "<cell name='bsd_bankaccount'    " + "width='100' />  " +
       "<cell name='bsd_account'    " + "width='100' />  " +
       "</row>   " +
       "</grid>   ";

                    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                        if (rs.length > 0) {
                            if (rs[0].attributes.bsd_name != null) {
                                bankaccount.setValue([{
                                    id: rs[0].Id,
                                    name: rs[0].attributes.bsd_name.value,
                                    entityType: rs[0].logicalName
                                }]);
                            }
                            if (rs[0].attributes.bsd_bankgroup != null) {
                                bankgroup.setValue([{
                                    id: rs[0].attributes.bsd_bankgroup.guid,
                                    name: rs[0].attributes.bsd_bankgroup.name,
                                    entityType: rs[0].attributes.bsd_bankgroup.logicalName
                                }]);
                            }
                            if (rs[0].attributes.bsd_bankaccount != null) {
                                bankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                            }
                            if (rs[0].attributes.bsd_brand != null) {
                                brand.setValue(rs[0].attributes.bsd_brand.value);
                            }
                            Xrm.Page.getControl("bsd_bankaccountb").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
                        }
                        if (rs.length == 0) {
                            bankaccount.setValue(null);
                            bankgroup.setValue(null);
                            bankaccountnumber.setValue(null);
                            brand.setValue(null);
                            Xrm.Page.getControl("bsd_bankaccountb").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
                        }
                    },
                    function (er) {
                        console.log(er.message)
                    });
                }
            }
            catch (e) {
            }
        }
    }
}
//Description:filter bank account theo ben b
function get_allbankaccountfromsitebonload() {
    var account = getValue("bsd_account");
    var xmlbankaccount_C = [];
    getControl("bsd_bankaccountb").removePreSearch(presearch_bankaccount);
    if (account != null) {
        fetch(xmlbankaccount_C, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand"
                           , "bsd_bankaccount", "bsd_account", "bsd_bankgroup"]
                           , ["bsd_name"], false, null,
                           ["bsd_account", "statecode"], ["eq", "eq"], [0, account[0].id, 1, "0"]);
        LookUp_After_Load("bsd_bankaccountb", "bsd_bankaccount", "bsd_name", "Bank Account Customer", xmlbankaccount_C.join(""));
    }
    else {
        clear_bankaccount();
    }
}
//Description:filter account have principal contract
function filter_customerhaveprincipalcontract() {
    var data = [];
    var xmlprincipal = [];
    var xmlaccountprincipal = [];
    var xmlaccount = [];
    xmlprincipal.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xmlprincipal.push("<entity name='bsd_principalcontract'>");
    xmlprincipal.push("<attribute name='bsd_principalcontractid'/>");
    xmlprincipal.push("<attribute name='bsd_name' />");
    xmlprincipal.push("<attribute name='createdon'/>");
    xmlprincipal.push("<attribute name='bsd_account'/>");
    xmlprincipal.push("<order attribute='bsd_name' descending='false' />");
    xmlprincipal.push("</entity>");
    xmlprincipal.push("</fetch>");
    CrmFetchKit.Fetch(xmlprincipal.join(""), false).then(function (rsprincipal) {
        if (rsprincipal.length > 0) {
            for (var i = 0; i < rsprincipal.length; i++) {
                if (rsprincipal[i].attributes.bsd_account != null) {
                    data.push(rsprincipal[i].attributes.bsd_account.guid);
                }
            }
            xmlaccountprincipal.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xmlaccountprincipal.push("<entity name='account'>");
            xmlaccountprincipal.push("<attribute name='name'/>");
            xmlaccountprincipal.push("<attribute name='primarycontactid' />");
            xmlaccountprincipal.push("<attribute name='telephone1'/>");
            xmlaccountprincipal.push("<attribute name='accountid'/>");
            xmlaccountprincipal.push("<order attribute='name' descending='false' />");
            xmlaccountprincipal.push("<filter type='and'>");
            xmlaccountprincipal.push("<condition attribute='bsd_accounttype' operator='in'>");
            xmlaccountprincipal.push("<value>");
            xmlaccountprincipal.push("861450000");
            xmlaccountprincipal.push("</value>");
            xmlaccountprincipal.push("<value>");
            xmlaccountprincipal.push("100000000");
            xmlaccountprincipal.push("</value>");
            xmlaccountprincipal.push("</condition>");
            xmlaccountprincipal.push("<condition attribute='statecode' operator='eq' value='0'/>");
            xmlaccountprincipal.push("<condition attribute='accountid' operator='not-in'>");
            for (var k = 0; k < data.length; k++) {
                xmlaccountprincipal.push("<value uitype='account'>" + '{' + data[k] + '}' + "");
                xmlaccountprincipal.push("</value>");
            }
            xmlaccountprincipal.push("</condition>");
            xmlaccountprincipal.push("</filter>");
            xmlaccountprincipal.push("</entity>");
            xmlaccountprincipal.push("</fetch>");
            LookUp_After_Load("bsd_account", "account", "name", "Account have principal contract", xmlaccountprincipal.join(""));
        }
        else {
            xmlaccount.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xmlaccount.push("<entity name='account'>");
            xmlaccount.push("<attribute name='name'/>");
            xmlaccount.push("<attribute name='primarycontactid' />");
            xmlaccount.push("<attribute name='telephone1'/>");
            xmlaccount.push("<attribute name='accountid'/>");
            xmlaccount.push("<order attribute='name' descending='false' />");
            xmlaccount.push("<filter type='and'>");
            xmlaccount.push("<condition attribute='bsd_accounttype' operator='in'>");
            xmlaccount.push("<value>");
            xmlaccount.push("861450000");
            xmlaccount.push("</value>");
            xmlaccount.push("<value>");
            xmlaccount.push("100000000");
            xmlaccount.push("</value>");
            xmlaccount.push("</condition>");
            xmlaccount.push("<condition attribute='statecode' operator='eq' value='0'/>");
            xmlaccount.push("</filter>");
            xmlaccount.push("</entity>");
            xmlaccount.push("</fetch>");
            LookUp_After_Load("bsd_account", "account", "name", "Account", xmlaccount.join(""));
        }
    });
}
//Description:load bank account number and bank group from bank acccount site b
function get_bankaccountnumberandbankgroupb() {
    debugger;
    var account = getValue("bsd_account");
    var bankaccount = getValue("bsd_bankaccountb");
    var bankgroup = getAttribute("bsd_bankgroupb");
    var bankaccountnumber = getAttribute("bsd_bankaccountnumberb");
    var brand = getAttribute("bsd_brandb");
    var xmlbankaccount_C = [];
    if (bankaccount != null) {
        fetch(xmlbankaccount_C, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand"
                          , "bsd_bankaccount", "bsd_account", "bsd_bankgroup"]
                          , ["bsd_name"], false, null,
                          ["bsd_bankaccountid", "statecode"], ["eq", "eq"], [0, bankaccount[0].id, 1, "0"]);
        CrmFetchKit.Fetch(xmlbankaccount_C.join(""), true).then(function (rsbankaccount_C) {
            if (rsbankaccount_C.length > 0) {
                if (rsbankaccount_C[0].attributes.bsd_bankgroup != null) {
                    bankgroup.setValue([{
                        id: rsbankaccount_C[0].attributes.bsd_bankgroup.guid,
                        name: rsbankaccount_C[0].attributes.bsd_bankgroup.name,
                        entityType: rsbankaccount_C[0].attributes.bsd_bankgroup.logicalName
                    }]);
                }
                if (rsbankaccount_C[0].attributes.bsd_bankaccount != null) {
                    bankaccountnumber.setValue(rsbankaccount_C[0].attributes.bsd_bankaccount.value);
                }
                if (rsbankaccount_C[0].attributes.bsd_brand != null) {
                    brand.setValue(rsbankaccount_C[0].attributes.bsd_brand.value);
                }
            }
        });
    }
    else {
        setNull(["bsd_bankgroupb", "bsd_bankaccountb", "bsd_brandb", "bsd_bankaccountnumberb"])
    }
}
//Description:gan gia tri cho section BHS
function setvalue_sectionBHS() {
    debugger;
    var xmlaccount_B = [];
    var xmladdress_B = [];
    var xmladdress_T = [];
    var xmlbankaccount = [];
    var xmlprimarycontact = [];
    var xmlcontact = [];
    var xmljobtitle = [];
    var xmlcontactaccount = [];
    var xmljobtitleaccount = [];
    var fieldrenter = getAttribute("bsd_renter");
    var fieldaddressrenter = getAttribute("bsd_addressrenter");
    var fieldbankaccountrenter = getAttribute("bsd_bankaccounta");
    var fieldbrandrenter = getAttribute("bsd_branda");
    var fieldbankaccountnumberrenter = getAttribute("bsd_bankaccountnumbera");
    var fieldbankgroupewnter = getAttribute("bsd_bankgroupa");
    var fieldcontactrenter = getAttribute("bsd_nguoidaidiena");
    getControl("bsd_renter").removePreSearch(presearch_Loadtrader);
    getControl("bsd_bankaccounta").removePreSearch(presearch_LoadBankAccountTrader);
    getControl("bsd_nguoidaidiena").removePreSearch(presearch_Loadrepresentative);
    fetch(xmlaccount_B, "account", ["accountid", "name", "primarycontactid", "telephone1", "bsd_taxregistration", "fax"], ["name"], false, null,
        ["bsd_accounttype", "statecode"], ["eq", "eq"], [0, "861450005", 1, "0"]);
    CrmFetchKit.Fetch(xmlaccount_B.join(""), true).then(function (rsaccount_B) {
        if (rsaccount_B.length > 0) {
            if (fieldrenter.getValue() == null) {
                if (rsaccount_B[0].attributes.name != null) {
                    fieldrenter.setValue([{
                        id: '{' + rsaccount_B[0].Id.toUpperCase() + '}',
                        name: rsaccount_B[0].attributes.name.value,
                        entityType: rsaccount_B[0].logicalName
                    }]);
                }
                else {
                    setNull("bsd_renter");
                }
                if (rsaccount_B[0].attributes.telephone1 != null) {
                    Xrm.Page.getAttribute("bsd_phonerenter").setValue(rsaccount_B[0].attributes.telephone1.value);
                }
                else {
                    setNull("bsd_phonerenter");
                }
                if (rsaccount_B[0].attributes.fax != null) {
                    Xrm.Page.getAttribute("bsd_faxrenter").setValue(rsaccount_B[0].attributes.fax.value);
                }
                else {
                    setNull("bsd_faxrenter");
                }
                if (rsaccount_B[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistrationrenter").setValue(rsaccount_B[0].attributes.bsd_taxregistration.value);
                }
                else {
                    setNull("bsd_taxregistrationrenter");
                }
                LookUp_After_Load("bsd_renter", "account", "name", "Account BHS", xmlaccount_B.join(""));
                //Address renter              
                fetch(xmladdress_B, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                    ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, rsaccount_B[0].Id]);
                CrmFetchKit.Fetch(xmladdress_B.join(""), true).then(function (rsaddress_B) {
                    if (rsaddress_B.length > 0) {
                        if (rsaddress_B[0].attributes.bsd_name != null) {
                            fieldaddressrenter.setValue([{
                                id: '{' + rsaddress_B[0].Id.toUpperCase() + '}',
                                name: rsaddress_B[0].attributes.bsd_name.value,
                                entityType: rsaddress_B[0].logicalName
                            }]);
                        }
                        else {
                            setNull("bsd_addressrenter");
                        }
                        LookUp_After_Load("bsd_addressrenter", "bsd_address", "bsd_name", "Address BHS", xmladdress_B.join(""));
                    }
                    else {
                        fetch(xmladdress_T, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                       ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, rsaccount_B[0].Id]);
                        CrmFetchKit.Fetch(xmladdress_T.join(""), true).then(function (rsaddress_T) {
                            if (rsaddress_T.length > 0) {
                                if (rsaddress_T[0].attributes.bsd_name != null) {
                                    fieldaddressrenter.setValue([{
                                        id: '{' + rsaddress_T[0].Id.toUpperCase() + '}',
                                        name: rsaddress_T[0].attributes.bsd_name.value,
                                        entityType: rsaddress_T[0].logicalName
                                    }]);
                                }
                                else {
                                    setNull("bsd_addressrenter");
                                }
                                LookUp_After_Load("bsd_addresstrader", "bsd_address", "bsd_name", "Address BHS", xmladdress_T.join(""));
                            }
                            else {
                                setNull("bsd_addressrenter");
                                clear_LoadAddressTrader();
                            }
                        });
                    }
                });
                //Bank Account renter            
                fetch(xmlbankaccount, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
                    ["bsd_account", "statecode"], ["eq", "eq"], [0, rsaccount_B[0].Id, 1, "0"]);
                CrmFetchKit.Fetch(xmlbankaccount.join(""), true).then(function (rsbankaccont) {
                    if (rsbankaccont.length > 0) {
                        if (rsbankaccont[0].attributes.bsd_name != null) {
                            fieldbankaccountrenter.setValue([{
                                id: '{' + rsbankaccont[0].Id.toUpperCase() + '}',
                                name: rsbankaccont[0].attributes.bsd_name.value,
                                entityType: rsbankaccont[0].logicalName
                            }]);
                        }
                        else {
                            setNull("bsd_bankaccounta");
                        }
                        if (rsbankaccont[0].attributes.bsd_brand != null) {
                            fieldbrandrenter.setValue(rsbankaccont[0].attributes.bsd_brand.value);
                        }
                        else {
                            setNull("bsd_branda");
                        }
                        if (rsbankaccont[0].attributes.bsd_bankaccount != null) {
                            fieldbankaccountnumberrenter.setValue(rsbankaccont[0].attributes.bsd_bankaccount.value);
                        }
                        else {
                            fieldbankaccountnumberrenter.setValue(null);
                        }
                        if (rsbankaccont[0].attributes.bsd_bankgroup != null) {
                            fieldbankgroupewnter.setValue([{
                                id: '{' + rsbankaccont[0].attributes.bsd_bankgroup.guid.toUpperCase() + '}',
                                name: rsbankaccont[0].attributes.bsd_bankgroup.name,
                                entityType: 'bsd_bankgroup'
                            }]);
                        }
                        else {
                            setNull("bsd_bankgroupa");

                        }
                        LookUp_After_Load("bsd_bankaccounta", "bsd_bankaccount", "bsd_name", "Bank Account BHS", xmlbankaccount.join(""));
                    }
                    else {
                        setNull(["bsd_bankaccounta", "bsd_bankgroupa", "bsd_bankaccountnumbera", "bsd_branda"]);
                        clear_LoadBankAccountTrader();
                    }
                });
                //Nguoi dai dien renter
                xmlprimarycontact.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
                xmlprimarycontact.push("<entity name='account'>");
                xmlprimarycontact.push("<attribute name='name' />");
                xmlprimarycontact.push("<attribute name='primarycontactid'/>");
                xmlprimarycontact.push("<attribute name='telephone1'/>");
                xmlprimarycontact.push("<attribute name='accountid'/>");
                xmlprimarycontact.push("<order attribute='name' descending='false'/> ");
                xmlprimarycontact.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
                xmlprimarycontact.push("<attribute name='jobtitle'/>");
                xmlprimarycontact.push("<filter type='and'>");
                xmlprimarycontact.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + rsaccount_B[0].Id + "'/>");
                xmlprimarycontact.push("</filter>");
                xmlprimarycontact.push("</link-entity>");
                xmlprimarycontact.push("</entity>");
                xmlprimarycontact.push("</fetch>");
                CrmFetchKit.Fetch(xmlprimarycontact.join(""), true).then(function (rsprimarycontact_T) {
                    if (rsprimarycontact_T.length > 0) {
                        fieldcontactrenter.setValue([{
                            id: '{' + rsprimarycontact_T[0].attributes.primarycontactid.guid.toUpperCase() + '}',
                            name: rrsprimarycontact_Ts[0].attributes.primarycontactid.name,
                            entityType: 'contact'
                        }]);
                        fetch(xmlcontact, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                           ["parentcustomerid", "statecode"], ["eq", "eq"], [0, rsaccount_B[0].Id, 1, "0"]);
                        var layoutXmlcontact = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                        LookUp_After_Load("bsd_nguoidaidiena", "contact", "fullname", "Trader Contact", xmlcontact.join(""));
                        fetch(xmljobtitle, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                        ["contactid"], ["eq"], [0, rsprimarycontact_T[0].attributes.primarycontactid.guid, 1]);
                        CrmFetchKit.Fetch(xmljobtitle.join(""), true).then(function (rsjobtitle_T) {
                            if (rsjobtitle_T.length > 0) {
                                Xrm.Page.getAttribute("bsd_jobtitlea").setValue(rsjobtitle_T[0].attributes.jobtitle.value);
                            }
                            else {
                                setNull("bsd_jobtitlea");
                            }
                        });
                    }
                    else {
                        fetch(xmlcontactaccount, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                            ["parentcustomerid", "statecode"], ["eq", "eq"], [0, rsaccount_B[0].Id, 1, "0"]);
                        CrmFetchKit.Fetch(xmlcontactaccount.join(""), true).then(function (rscontactaccount) {
                            if (rscontactaccount.length > 0) {
                                fieldcontactrenter.setValue([{
                                    id: '{' + rscontactaccount[0].Id.toUpperCase() + '}',
                                    name: rscontactaccount[0].attributes.fullname.value,
                                    entityType: 'contact'
                                }]);
                                LookUp_After_Load("bsd_nguoidaidiena", "contact", "fullname", "Trader Contact", xmlcontactaccount.join(""));
                                fetch(xmljobtitleaccount, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                                ["fullname"], ["eq"], [0, rscontactaccount[0].attributes.fullname.value, 1]);
                                CrmFetchKit.Fetch(xmljobtitleaccount.join(""), true).then(function (rsjobtitle_T) {
                                    if (rrsjobtitle_Ts.length > 0) {
                                        Xrm.Page.getAttribute("bsd_jobtitlea").setValue(rsjobtitle_T[0].attributes.jobtitle.value);
                                    }
                                    else {
                                        setNull("bsd_jobtitlea");
                                    }
                                });
                            }
                            else {
                                setNull("bsd_jobtitlea");
                                Xrm.Page.getAttribute("bsd_jobtitlea").setValue(null);
                            }
                        });
                    }
                });
            }
        }
        else {
            clear_Loadtrader();
            clear_LoadBankAccountTrader();
            clear_Loadrepresentative();
        }
    });
}
//Description:gan gia tri cho section BHS
function setvalue_sectionBHSonload() {
    var xmlaccount = [];
    var xmladdressbusiness = [];
    var xmladdressrenter = [];
    var xmlbankaccountrenter = [];
    var xmlcontactrenter = [];
    var fieldrenter = getValue("bsd_renter");

    getControl("bsd_renter").removePreSearch(presearch_Loadtrader);
    getControl("bsd_bankaccounta").removePreSearch(presearch_LoadBankAccountTrader);
    getControl("bsd_nguoidaidiena").removePreSearch(presearch_Loadrepresentative);
    //Account type BHS
    fetch(xmlaccount, "account", ["accountid", "name", "primarycontactid", "telephone1", "bsd_taxregistration", "fax"], ["name"], false, null,
        ["bsd_accounttype", "statecode"], ["eq", "eq"], [0, "861450005", 1, "0"]);
    LookUp_After_Load("bsd_renter", "account", "name", "Account BHS", xmlaccount.join(""));
    if (fieldrenter != null) {
        //Address renter           
        fetch(xmladdressbusiness, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
            ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, fieldrenter[0].id]);
        var layoutXmladdressbusiness = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                   "<row name='result'  " + "id='bsd_addressid'>  " +
                   "<cell name='bsd_name'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                   "<cell name='bsd_account'    " + "width='100' />  " +
                   "</row>   " +
                "</grid>   ";
        CrmFetchKit.Fetch(xmladdressbusiness.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                LookUp_After_Load("bsd_addressrenter", "bsd_address", "bsd_name", "Address Account BHS", xmladdressbusiness.join(""));
            }
            else {
                fetch(xmladdressrenter, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, fieldrenter[0].id]);
                CrmFetchKit.Fetch(xmladdressrenter.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        LookUp_After_Load("bsd_addressrenter", "bsd_address", "bsd_name", "Address Account BHS", xmladdressrenter.join(""));
                    }
                    else {
                        clear_LoadAddressTrader();
                    }
                });
            }
        });
        //Bank Account renter          
        fetch(xmlbankaccountrenter, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_account", "statecode"], ["eq", "eq"], [0, fieldrenter[0].id, 1, "0"]);
        CrmFetchKit.Fetch(xmlbankaccountrenter.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                LookUp_After_Load("bsd_bankaccounta", "bsd_bankaccount", "bsd_name", "Bank Account BHS", xmlbankaccountrenter.join(""));
            }
            else {
                setNull("bsd_bankaccounta");
                clear_LoadBankAccountTrader();
            }
        });
        //Nguoi dai dien renter    
        fetch(xmlcontactrenter, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
            ["parentcustomerid", "statecode"], ["eq", "eq"], [0, fieldrenter[0].id, 1, "0"]);
        CrmFetchKit.Fetch(xmlcontactrenter.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                LookUp_After_Load("bsd_nguoidaidiena", "contact", "fullname", "Contact Account BHS", xmlcontactrenter.join(""));
            }
            else {
                setNull("bsd_jobtitlea", "bsd_nguoidaidiena");
                clear_Loadrepresentative();
            }
        });
    }
}
//Description:action change filed renter
function action_changefieldtrader() {
    debugger;
    var trader = getValue("bsd_renter");
    var fieldaddress = getAttribute("bsd_addressrenter");
    var fieldbankaccount = getAttribute("bsd_bankaccounta");
    var fieldbankgroup = getAttribute("bsd_bankgroupa");
    var fieldbankaccountnumber = getAttribute("bsd_bankaccountnumbera");
    var fieldbrand = getAttribute("bsd_branda");
    var fieldcontact = getAttribute("bsd_nguoidaidiena");
    getControl("bsd_addressrenter").removePreSearch(presearch_LoadAddressTrader);
    getControl("bsd_bankaccounta").removePreSearch(presearch_LoadBankAccountTrader);
    getControl("bsd_nguoidaidiena").removePreSearch(presearch_Loadrepresentative);
    var xmlaccount = [];
    var xmladdress_B = [];
    var xmladdress_T = [];
    var xmlbankaccount_T = [];
    var xmlrep_T = [];
    var xmlre_T = [];
    var xmljob_T = [];
    if (trader != null) {
        //load thong tin cua BHS  
        fetch(xmlaccount, "account", ["accountid", "name", "primarycontactid", "telephone1", "bsd_taxregistration", "fax"]
            , ["name"], false, null, ["accountid"], ["eq"]
            , [0, trader[0].id, 1]);
        CrmFetchKit.Fetch(xmlaccount.join(""), true).then(function (rsaccount) {
            if (rsaccount.length > 0) {
                if (rsaccount[0].attributes.telephone1 != null) {
                    setValue("bsd_phonerenter", rsaccount[0].attributes.telephone1.value);
                }
                else {
                    setNull("bsd_phonerenter");
                }
                if (rsaccount[0].attributes.bsd_taxregistration != null) {
                    setValue("bsd_taxregistrationrenter", rsaccount[0].attributes.bsd_taxregistration.value);
                }
                else {
                    setNull("bsd_taxregistrationrenter")
                }
                if (rsaccount[0].attributes.fax != null) {
                    setValue("bsd_faxrenter", rsaccount[0].attributes.fax.value);
                }
                else {
                    setNull("bsd_faxrenter")
                }
            }
        });
        //load thong tin dia chi cua BHS      
        fetch(xmladdress_B, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
            ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, trader[0].id]);
        CrmFetchKit.Fetch(xmladdress_B.join(""), true).then(function (rsaddress_b) {
            if (rsaddress_b.length > 0) {
                if (rsaddress_b[0].attributes.bsd_name != null) {
                    fieldaddress.setValue([{
                        id: rsaddress_b[0].Id,
                        name: rsaddress_b[0].attributes.bsd_name.value,
                        entityType: rsaddress_b[0].logicalName
                    }]);
                }
                else {
                    fieldaddress.setValue(null);
                }
                LookUp_After_Load("bsd_addressrenter", "bsd_address", "bsd_name", "Trader Address", xmladdress_B.join(""));
            }
            else {
                fetch(xmladdress_T, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, trader[0].id]);
                CrmFetchKit.Fetch(xmladdress_T.join(""), true).then(function (rsaddress_T) {
                    if (rsaddress_T.length > 0) {
                        LookUp_After_Load("bsd_addressrenter", "bsd_address", "bsd_name", "Trader Address", xmladdress_T.join(""));
                    }
                    else {
                        setNull("bsd_addressrenter");
                        clear_LoadAddressTrader();
                    }
                });
            }
        });
        //load bank account cua BHS      
        fetch(xmlbankaccount_T, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand"
                            , "bsd_bankaccount", "bsd_account", "bsd_bankgroup"]
                            , ["bsd_name"], false, null,
                            ["bsd_account", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
        CrmFetchKit.Fetch(xmlbankaccount_T.join(""), true).then(function (rsbankaccount_T) {
            if (rsbankaccount_T.length > 0) {
                if (rsbankaccount_T[0].attributes.bsd_name != null) {
                    fieldbankaccount.setValue([{
                        id: rsbankaccount_T[0].Id,
                        name: rsbankaccount_T[0].attributes.bsd_name.value,
                        entityType: rsbankaccount_T[0].logicalName
                    }]);
                    fieldbankgroup.setValue([{
                        id: rsbankaccount_T[0].attributes.bsd_bankgroup.guid,
                        name: rsbankaccount_T[0].attributes.bsd_bankgroup.name,
                        entityType: rsbankaccount_T[0].attributes.bsd_bankgroup.logicalName
                    }]);
                    fieldbankaccountnumber.setValue(rsbankaccount_T[0].attributes.bsd_bankaccount.value);
                    fieldbrand.setValue(rsbankaccount_T[0].attributes.bsd_brand.value);
                }
                else {
                    setNull(["bsd_bankaccounta", "bsd_bankgroupa", "bsd_bankaccountnumbera", "bsd_branda"]);
                    clear_LoadBankAccountTrader();
                }
                LookUp_After_Load("bsd_bankaccounta", "bsd_bankaccount", "bsd_name", "Bank Account Trader", xmlbankaccount_T.join(""));
            }
            else {
                setNull(["bsd_bankaccounta", "bsd_bankgroupa", "bsd_bankaccountnumbera", "bsd_branda"]);
                clear_LoadBankAccountTrader();
            }
        },
            function (er) {
                console.log(er.message)
            });
        //load nguoi dai dien BHS
        xmlrep_T.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xmlrep_T.push("<entity name='account'>");
        xmlrep_T.push("<attribute name='name' />");
        xmlrep_T.push("<attribute name='primarycontactid'/>");
        xmlrep_T.push("<attribute name='telephone1'/>");
        xmlrep_T.push("<attribute name='accountid'/>");
        xmlrep_T.push("<order attribute='name' descending='false'/> ");
        xmlrep_T.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
        xmlrep_T.push("<attribute name='jobtitle'/>");
        xmlrep_T.push("<filter type='and'>");
        xmlrep_T.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + trader[0].id + "'/>");
        xmlrep_T.push("</filter>");
        xmlrep_T.push("</link-entity>");
        xmlrep_T.push("</entity>");
        xmlrep_T.push("</fetch>");
        CrmFetchKit.Fetch(xmlrep_T.join(""), true).then(function (rsrep_T) {
            if (rsrep_T.length > 0) {
                fieldcontact.setValue([{
                    id: rsrep_T[0].attributes.primarycontactid.guid,
                    name: rsrep_T[0].attributes.primarycontactid.name,
                    entityType: 'contact'
                }]);
                fetch(xmlre_T, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                   ["parentcustomerid", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
                LookUp_After_Load("bsd_nguoidaidiena", "contact", "fullname", "Contact Trader", xmlre_T.join(""));
                fetch(xmljob_T, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                ["contactid"], ["eq"], [0, rsrep_T[0].attributes.primarycontactid.guid, 1]);
                CrmFetchKit.Fetch(xmljob_T.join(""), true).then(function (rsjob_T) {
                    if (rsjob_T.length > 0) {
                        setValue("bsd_jobtitlea", rsjob_T[0].attributes.jobtitle.value);
                    }
                    else {
                        setNull("bsd_jobtitlea");
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
            else {
                fetch(xmlre_T, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                    ["parentcustomerid", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
                CrmFetchKit.Fetch(xmlre_T.join(""), true).then(function (rsre_T) {
                    if (rsre_T.length > 0) {
                        fieldcontact.setValue([{
                            id: rsre_T[0].Id,
                            name: rsre_T[0].attributes.fullname.value,
                            entityType: 'contact'
                        }]);
                        LookUp_After_Load("bsd_nguoidaidiena", "contact", "fullname", "Trader Contact", xmlre_T.join(""));
                        fetch(xmljob_T, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                        ["fullname"], ["eq"], [0, rsre_T[0].attributes.fullname.value, 1]);
                        CrmFetchKit.Fetch(xmljob_T.join(""), true).then(function (rsjob_T) {
                            if (rsjob_T.length > 0) {
                                setValue("bsd_jobtitlea", rsjob_T[0].attributes.jobtitle.value);
                            }
                            else {
                                setNull("bsd_jobtitlea");
                            }
                        },
                    function (er) {
                        console.log(er.message)
                    });
                    }
                    else {
                        setNull(["bsd_jobtitlea", "bsd_nguoidaidiena"])
                        clear_Loadrepresentative();
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
        },
            function (er) {
                console.log(er.message)
            });
    }
    else {
        fieldaddress.setValue(null);
        fieldbankaccount.setValue(null);
        fieldbankgroup.setValue(null);
        fieldbankaccountnumber.setValue(null);
        fieldbrand.setValue(null);
        fieldcontact.setValue(null);
        set_value(["bsd_phonerenter", "bsd_taxregistrationrenter", "bsd_faxrenter", "bsd_jobtitlea"], null);
        clear_LoadAddressTrader();
        clear_LoadBankAccountTrader();
        clear_Loadrepresentative();
    }
}
//Description:lay du lieu bank account,bank group,brand tu trader
function get_bankaccountnumberandbankgrouptrader() {
    var bankaccounttrader = getValue("bsd_bankaccounta");
    var fieldbankgroup = getAttribute("bsd_bankgroupa");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbera");
    var fieldbrand = getAttribute("bsd_branda");
    var xml = [];
    if (bankaccounttrader != null) {
        //load bank account cua BHS            
        fetch(xml, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_bankaccountid", "statecode"], ["eq", "eq"], [0, bankaccounttrader[0].id, 1, "0"]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_bankgroup != null) {
                    fieldbankgroup.setValue([{
                        id: '{' + rs[0].attributes.bsd_bankgroup.guid.toUpperCase() + '}',
                        name: rs[0].attributes.bsd_bankgroup.name,
                        entityType: rs[0].attributes.bsd_bankgroup.logicalName
                    }]);
                }
                if (rs[0].attributes.bsd_bankaccount != null) {
                    fieldbankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                }
                if (rs[0].attributes.bsd_brand != null) {
                    fieldbrand.setValue(rs[0].attributes.bsd_brand.value);
                }
            }
            else {
                setNull("bsd_bankgroupa", "bsd_bankaccountnumbera", "bsd_branda");
            }
        });
    }
    else {
        setNull(["bsd_bankgroupa", "bsd_bankaccountnumbera", "bsd_branda"]);
    }
}
//Description:lay thong tin chuc vu nguoi dai dien tu BHS
function get_thongtinchucvu() {
    var present = Xrm.Page.getAttribute("bsd_nguoidaidiena").getValue();
    var xml = [];
    if (present != null) {
        fetch(xml, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
              ["contactid"], ["eq"], [0, present[0].id, 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.jobtitle != null) {
                    Xrm.Page.getAttribute("bsd_jobtitlea").setValue(rs[0].attributes.jobtitle.value);
                }
                else {
                    setNull("bsd_jobtitlea");
                }
            }
        });
    }
    if (present == null) {
        setNull("bsd_jobtitlea");
    }
}
function check_date() {
    var fromdate = getValue("bsd_fromdate");
    var todate = getValue("bsd_todate");
    var currentDateTime = getCurrentDateTime();
    if (fromdate != null) {
        if (todate < fromdate) {
            setNotification("bsd_todate", "The To Date Date cannot occur after From Date");
        }
        else if (todate > fromdate) {
            Xrm.Page.getControl("bsd_todate").clearNotification();
        }
    }
}
function getCurrentDateTime() {
    var d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
}

//Description:change status when stage final onload
function changestatuswhenstagefinalonload() {
    debugger;
    stagename = Xrm.Page.data.process.getActiveStage().getId();
    var formState = Xrm.Page.ui.getFormType();
    var nhanvien = getValue("bsd_tennguoiduyetnhanvien");
    var truongphong = getValue("bsd_tennguoiduyettruongphong");
    var numberprocess = getValue("bsd_numberprocess");
    var numberprocessctq = getValue("bsd_numberprocessctq");
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    for (var i = 0; i < rnames.length; i++) {
        //stage la:finnish
        if (stagename == "b3a98d01-c522-a437-ad09-0b351d82a709") {
            Xrm.Page.getAttribute("statuscode").setValue(100000002);//status:Chief Officer Approved
        }
        //nhan vien va stage la:staff
        if (rnames[i] == "Nhân Viên" && stagename == "cb938220-fddc-4fb1-aad5-f0d0c56bcb6c") {
            Xrm.Page.getAttribute("statuscode").setValue(100000000);//status:Staff Approved
        }
        //nhan vien va stage la:staff va fied ten nhan vien lap !null
        if (rnames[i] == "Nhân Viên" && stagename == "cb938220-fddc-4fb1-aad5-f0d0c56bcb6c" && nhanvien != null) {
            Xrm.Page.getAttribute("statuscode").setValue(861450001);//status:Manager Unapproved
        }
        //nhan vien va stage la:manager 
        if (rnames[i] == "Nhân Viên" && stagename == "3a55186c-3472-471a-305d-e424e7264367") {
            Xrm.Page.getAttribute("statuscode").setValue(861450000);//status:Manager Pending Approval
        }
        //truong phong va stage la:manager
        if (rnames[i] == "Trưởng Phòng" && stagename == "3a55186c-3472-471a-305d-e424e7264367") {
            Xrm.Page.getAttribute("statuscode").setValue(100000001);//status:Manager Approved
        }
        //truong phong va stage la:staff va fied ten nhan vien lap !null
        if (rnames[i] == "Trưởng Phòng" && stagename == "cb938220-fddc-4fb1-aad5-f0d0c56bcb6c" && nhanvien != null) {
            Xrm.Page.getAttribute("statuscode").setValue(861450001);//status:Manager Unapproved
        }
        //truong phong va stage la:staff va fied ten nhan vien lap =null
        if (rnames[i] == "Trưởng Phòng" && stagename == "cb938220-fddc-4fb1-aad5-f0d0c56bcb6c" && nhanvien == null && numberprocess == false) {
            Xrm.Page.getAttribute("statuscode").setValue(100000001);//status:Manager Approved
            Xrm.Page.getAttribute("bsd_numberprocess").setValue(true);
            setValue("bsd_tennguoiduyettruongphong", [{
                name: Xrm.Page.context.getUserName(),
                id: Xrm.Page.context.getUserId(),
                type: "8"
            }]);
        }
        //Cấp Thẩm Quyền va stage la:staff va fied ten nhan vien lap =null va fied ten truong phong =null
        if (rnames[i] == "Cấp Thẩm Quyền" && stagename == "cb938220-fddc-4fb1-aad5-f0d0c56bcb6c" && nhanvien == null && truongphong == null && numberprocessctq == false) {
            Xrm.Page.getAttribute("statuscode").setValue(100000002);//status:Chief Officer Approved
            Xrm.Page.getAttribute("bsd_numberprocessctq").setValue(true);
            setValue("bsd_tennguoiduyetcapthamquyen", [{
                name: Xrm.Page.context.getUserName(),
                id: Xrm.Page.context.getUserId(),
                type: "8"
            }]);
        }
        //truong phong va stage la:Chief Officer 
        if (rnames[i] == "Trưởng Phòng" && stagename == "011cd773-a1cd-9eb5-2aba-6466b7c7cb41") {
            Xrm.Page.getAttribute("statuscode").setValue(861450002);//status:Chief Officer Pending Approval
        }
        //Trưởng Phòng va stage la:manager va fied ten truong phong lap !null
        if (rnames[i] == "Trưởng Phòng" && stagename == "3a55186c-3472-471a-305d-e424e7264367" && truongphong != null) {
            Xrm.Page.getAttribute("statuscode").setValue(861450003);//status:Chief Officer Unapproved
        }
        //Cấp Thẩm Quyền va stage la:Chief Officer 
        if (rnames[i] == "Cấp Thẩm Quyền" && stagename == "011cd773-a1cd-9eb5-2aba-6466b7c7cb41") {
            Xrm.Page.getAttribute("statuscode").setValue(100000002);//status:Chief Officer Approved
        }
    }
    Xrm.Page.data.save();
}
function GetRoleName(roleIds) {
    var serverUrl = location.protocol + "//" + location.host + "/" + Xrm.Page.context.getOrgUniqueName();
    var odataSelect = serverUrl + "/XRMServices/2011/OrganizationData.svc" + "/" + "RoleSet?$select=Name";
    var cdn = "";
    if (roleIds != null && roleIds.length > 0) {
        for (var i = 0; i < roleIds.length; i++) {
            if (i == roleIds.length - 1)
                cdn += "RoleId eq guid'" + roleIds[i] + "'";
            else
                cdn += "RoleId eq guid'" + roleIds[i] + "' or ";
        }
        if (cdn.length > 0)
            cdn = "&$filter=" + cdn;
        odataSelect += cdn;
        var roleName = [];
        $.ajax(
            {
                type: "GET",
                async: false,
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                url: odataSelect,
                beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
                success: function (data, textStatus, XmlHttpRequest) {
                    if (data.d != null && data.d.results != null) {
                        var len = data.d.results.length;
                        for (var k = 0; k < len; k++)
                            roleName.push(data.d.results[k].Name);
                    }
                },
                error: function (XmlHttpRequest, textStatus, errorThrown) { alert('OData Select Failed: ' + textStatus + errorThrown + odataSelect); }
            }
        );
    }
    return roleName;
}
function switchProcess() {
    var formState = Xrm.Page.ui.getFormType();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var formState = Xrm.Page.ui.getFormType();
    for (var i = 0; i < rnames.length; i++) {
        if (rnames[i] = "Trưởng Phòng") {
            var formState = Xrm.Page.ui.getFormType();
            stageId = Xrm.Page.data.process.getActiveStage().getId();
            Xrm.Page.data.process.setActiveStage(stageId, switchProcessEnd);
        }
    }
}
function switchProcessEnd() {
    stagename = Xrm.Page.data.process.getActiveStage().getId();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var numberprocess = Xrm.Page.getAttribute("bsd_numberprocess").getValue();
    for (var i = 0; i < rnames.length; i++) {
        //stage:staff
        if (stagename == "cb938220-fddc-4fb1-aad5-f0d0c56bcb6c" && rnames[i] == "Trưởng Phòng" && numberprocess == true) {
            Xrm.Page.data.process.moveNext(function (e) {
                if (e == "success") {
                    Xrm.Page.ui.refreshRibbon();
                }
            });
        }
    }

}
function switchProcessctq() {
    debugger;
    var formState = Xrm.Page.ui.getFormType();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var formState = Xrm.Page.ui.getFormType();
    for (var i = 0; i < rnames.length; i++) {
        if (rnames[i] = "Cấp Thẩm Quyền") {
            var formState = Xrm.Page.ui.getFormType();
            stageId = Xrm.Page.data.process.getActiveStage().getId();
            Xrm.Page.data.process.setActiveStage(stageId, switchProcessEndctq);
        }
    }
}
function switchProcessEndctq() {
    var stagename = Xrm.Page.data.process.getActiveStage().getId();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var numberprocessctq = Xrm.Page.getAttribute("bsd_numberprocessctq").getValue();
    for (var i = 0; i < rnames.length; i++) {
        //stage:staff
        if (stagename == "cb938220-fddc-4fb1-aad5-f0d0c56bcb6c" && rnames[i] == "Cấp Thẩm Quyền" && numberprocessctq == true) {
            Xrm.Page.data.process.moveNext(function (e) {
                if (e == "success") {
                    Xrm.Page.ui.refreshRibbon();
                    Xrm.Page.data.process.moveNext(function (e) {
                        if (e == "success") {
                            Xrm.Page.ui.refreshRibbon();
                        }
                    });
                }
            });
        }
    }
}
function setdisableallfield() {
    debugger;
    var userid = Xrm.Page.context.getUserId();
    var owner = Xrm.Page.getAttribute("ownerid").getValue();
    var stagename = Xrm.Page.data.process.getActiveStage().getId();
    var statuscode = Xrm.Page.getAttribute("statuscode").getValue();
    //stage:manager va status:Chờ Trưởng phòng duyệt
    if (stagename == "3a55186c-3472-471a-305d-e424e7264367" && statuscode == 861450000) {
        setTimeout(DisabledForm, 1);
    }
    //stage:manager va status:Chờ Cấp Thẩm Quyền Duyệt
    if (stagename == "3a55186c-3472-471a-305d-e424e7264367" && statuscode == 861450002) {
        setTimeout(DisabledForm, 1);
    }
        //stage:staff va status:Nhân Viên Lập va nguoi tao ra khac vs nguoi dang dang nhap
    else if (stagename == "cb938220-fddc-4fb1-aad5-f0d0c56bcb6c" && statuscode == 861450000 && userid != owner[0].id) {
        setTimeout(DisabledForm, 1);
    }
        //stage:manager va status:Trưởng Phòng Duyệt va nguoi tao ra khac vs nguoi dang dang nhap
    else if (stagename == "b80a51c4-4782-2899-d060-5488b453c388" && statuscode == 100000000 && userid != owner[0].id) {
        setTimeout(DisabledForm, 1);
    }
        //stage:Cấp Thẩm Quyền Duyệt va status:Cấp Thẩm Quyền Duyệt va nguoi tao ra khac vs nguoi dang dang nhap
    else if (stagename == "011cd773-a1cd-9eb5-2aba-6466b7c7cb41" && statuscode == 100000002 && userid != owner[0].id) {
        setTimeout(DisabledForm, 1);
    }
        //stage:Cấp Thẩm Quyền Duyệt va status:Chờ Cấp Thẩm Quyền Duyệt va nguoi tao ra khac vs nguoi dang dang nhap
    else if (stagename == "011cd773-a1cd-9eb5-2aba-6466b7c7cb41" && statuscode == 861450002 && userid != owner[0].id) {
        setTimeout(DisabledForm, 1);
    }
        //stage:finnish
    else if (stagename == "b3a98d01-c522-a437-ad09-0b351d82a709") {
        setTimeout(DisabledForm, 1);
    }
    Xrm.Page.data.save();
}
//End
var purpose = null;
function khachhang_change(reset) {
    debugger;
    var khachhang = getValue("bsd_account");
    var nguoidaidienb = Xrm.Page.getAttribute("bsd_nguoidaidienb");

    var fieldbankaccount = Xrm.Page.getAttribute("bsd_bankaccountb");
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgroupb");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumberb");
    var fieldbrand = Xrm.Page.getAttribute("bsd_brandb");

    Xrm.Page.getControl("bsd_address").removePreSearch(presearch_addressaccount);
    Xrm.Page.getControl("bsd_bankaccountb").removePreSearch(presearch_bankaccount);
    Xrm.Page.getControl("bsd_nguoidaidienb").removePreSearch(presearch_contactaccount);

    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    var xmlre_T = [];
    getControl("bsd_address").removePreSearch(presearch_addressaccount);
    if (reset != false) setNull(["bsd_address", "bsd_warehouseaddress"]);
    if (khachhang != null) {
        fetch(xml, "bsd_address", ["bsd_name", "createdon", "bsd_purpose", "bsd_account", "bsd_addressid"], ["bsd_name"], false, null
            , ["bsd_account", "bsd_purpose_tmpvalue", "statecode"], ["eq", "like", "eq"], [0, khachhang[0].id, 1, "%" + "Business" + "%", 2, "0"]);
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                var first = rs[0];
                if (getValue("bsd_address") == null) {
                    setValue("bsd_address", [{
                        id: first.Id,
                        name: first.attributes.bsd_name.value,
                        entityType: first.logicalName
                    }]
                   );
                    set_DisableWarehouse(reset);
                }
                LookUp_After_Load("bsd_address", "bsd_address", "bsd_name", "Account Address", xml.join(""));
                fetch(xml2, "account", ["name", "primarycontactid", "telephone1", "accountid", "bsd_taxregistration", "fax", "websiteurl", "emailaddress1", "accountnumber"], ["name"], false, null
                , ["accountid"], ["eq"], [0, khachhang[0].id, 1]);
                CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs2) {
                    if (rs2.length > 0) {
                        if (Xrm.Page.getAttribute("bsd_accountnumberb").getValue() == null) {
                            if (rs2[0].attributes.accountnumber != null) {
                                var accountnumber = rs2[0].attributes.accountnumber.value;
                                Xrm.Page.getAttribute("bsd_accountnumberb").setValue(accountnumber);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_mainphoneb").getValue() == null) {
                            if (rs2[0].attributes.telephone1 != null) {
                                var telephone = rs2[0].attributes.telephone1.value;
                                Xrm.Page.getAttribute("bsd_mainphoneb").setValue(telephone);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_faxb").getValue() == null) {
                            if (rs2[0].attributes.fax != null) {
                                var fax = rs2[0].attributes.fax.value;
                                Xrm.Page.getAttribute("bsd_faxb").setValue(fax);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_taxregistrationb").getValue() == null) {
                            if (rs2[0].attributes.bsd_taxregistration != null) {
                                var taxres = rs2[0].attributes.bsd_taxregistration.value;
                                Xrm.Page.getAttribute("bsd_taxregistrationb").setValue(taxres);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_emailb").getValue() == null) {
                            if (rs2[0].attributes.emailaddress1 != null) {
                                var email = rs2[0].attributes.emailaddress1.value;
                                Xrm.Page.getAttribute("bsd_emailb").setValue(email);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_websiteb").getValue() == null) {
                            if (rs2[0].attributes.websiteurl != null) {
                                var website = rs2[0].attributes.websiteurl.value;
                                Xrm.Page.getAttribute("bsd_websiteb").setValue(website);
                            }
                        }
                        if (rs2[0].attributes.primarycontactid != null) {
                            if (nguoidaidienb.getValue() == null) {
                                nguoidaidienb.setValue([{
                                    id: '{' + rs2[0].attributes.primarycontactid.guid.toUpperCase() + '}',
                                    name: rs2[0].attributes.primarycontactid.name,
                                    entityType: 'contact'
                                }]);
                            }
                            fetch(xmlre_T, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                          ["parentcustomerid", "statecode"], ["eq", "eq"], [0, khachhang[0].id, 1, "0"]);
                            LookUp_After_Load("bsd_nguoidaidienb", "contact", "fullname", "Account Contact", xmlre_T.join(""));
                            var id = rs2[0].attributes.primarycontactid.guid;
                            fetch(xml3, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null
                            , ["contactid"], ["eq"], [0, id, 1]);
                            CrmFetchKit.Fetch(xml3.join(""), false).then(function (rs3) {
                                if (rs3.length > 0) {
                                    if (Xrm.Page.getAttribute("bsd_jobtitle").getValue() == null) {
                                        if (rs3[0].attributes.jobtitle != null) {
                                            var job = rs3[0].attributes.jobtitle.value;
                                            Xrm.Page.getAttribute("bsd_jobtitle").setValue(job);
                                        }
                                    }
                                }
                                else {
                                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
                                }
                            }, function (er) {
                                console.log(er.message)
                            });
                        }
                        if (rs2[0].attributes.primarycontactid == null) {
                            fetch(xml4, "contact", ["fullname", "telephone1", "contactid", "jobtitle", "parentcustomerid"], ["fullname"], false, null
                           , ["parentcustomerid"], ["eq"], [0, khachhang[0].id, 1]);
                            CrmFetchKit.Fetch(xml4.join(""), false).then(function (rs4) {
                                if (rs4.length > 0) {
                                    LookUp_After_Load("bsd_nguoidaidienb", "contact", "fullname", "Account Contact", xml4.join(""));
                                }
                                else {
                                    clear_contactaccount();
                                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
                                }
                            });
                        }
                    }
                });
            }
            else {
                fetch(xml1, "bsd_address", ["bsd_name", "createdon", "bsd_purpose", "bsd_account", "bsd_addressid"], ["bsd_name"], false, null
                    , ["bsd_account", "statecode"], ["eq", "eq"], [0, khachhang[0].id, 1, 0]);
                LookUp_After_Load("bsd_address", "bsd_address", "bsd_name", "Account Address", xml1.join(""));
                fetch(xml2, "account", ["name", "primarycontactid", "telephone1", "accountid", "bsd_taxregistration", "fax", "websiteurl", "emailaddress1"], ["name"], false, null
              , ["accountid"], ["eq"], [0, khachhang[0].id, 1]);
                CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs2) {
                    if (rs2.length > 0) {
                        if (Xrm.Page.getAttribute("bsd_mainphoneb").getValue() == null) {
                            if (rs2[0].attributes.telephone1 != null) {
                                var telephone = rs2[0].attributes.telephone1.value;
                                Xrm.Page.getAttribute("bsd_mainphoneb").setValue(telephone);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_faxb").getValue() == null) {
                            if (rs2[0].attributes.fax != null) {
                                var fax = rs2[0].attributes.fax.value;
                                Xrm.Page.getAttribute("bsd_faxb").setValue(fax);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_taxregistrationb").getValue() == null) {
                            if (rs2[0].attributes.bsd_taxregistration != null) {
                                var taxres = rs2[0].attributes.bsd_taxregistration.value;
                                Xrm.Page.getAttribute("bsd_taxregistrationb").setValue(taxres);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_emailb").getValue() == null) {
                            if (rs2[0].attributes.emailaddress1 != null) {
                                var email = rs2[0].attributes.emailaddress1.value;
                                Xrm.Page.getAttribute("bsd_emailb").setValue(email);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_websiteb").getValue() == null) {
                            if (rs2[0].attributes.websiteurl != null) {
                                var website = rs2[0].attributes.websiteurl.value;
                                Xrm.Page.getAttribute("bsd_websiteb").setValue(website);
                            }
                        }
                        if (rs2[0].attributes.primarycontactid != null) {
                            var contact = rs2[0].attributes.primarycontactid.name;
                            if (nguoidaidienb.getValue() == null) {
                                nguoidaidienb.setValue([{
                                    id: rs2[0].attributes.primarycontactid.guid,
                                    name: rs2[0].attributes.primarycontactid.name,
                                    entityType: 'contact'
                                }]);
                            }
                            fetch(xmlre_T, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                            ["parentcustomerid", "statecode"], ["eq", "eq"], [0, khachhang[0].id, 1, "0"]);
                            LookUp_After_Load("bsd_nguoidaidienb", "contact", "fullname", "Account Contact", xmlre_T.join(""));
                            var id = rs2[0].attributes.primarycontactid.guid;
                            fetch(xml3, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null
                            , ["contactid"], ["eq"], [0, id, 1]);
                            CrmFetchKit.Fetch(xml3.join(""), false).then(function (rs3) {
                                if (rs3.length > 0) {
                                    if (Xrm.Page.getAttribute("bsd_jobtitle").getValue() == null) {
                                        if (rs3[0].attributes.jobtitle != null) {
                                            var job = rs3[0].attributes.jobtitle.value;
                                            Xrm.Page.getAttribute("bsd_jobtitle").setValue(job);
                                        }
                                    }
                                }
                                else {
                                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
                                }
                            });
                        }
                        if (rs2[0].attributes.primarycontactid == null) {
                            fetch(xml4, "contact", ["fullname", "telephone1", "contactid", "jobtitle", "parentcustomerid"], ["fullname"], false, null
                           , ["parentcustomerid", "bsd_contacttype", "statecode"], ["eq", "eq", "eq"], [0, khachhang[0].id, 1, "861450000", 2, "0"]);
                            CrmFetchKit.Fetch(xml4.join(""), false).then(function (rs4) {
                                if (rs4.length > 0) {
                                    LookUp_After_Load("bsd_nguoidaidienb", "contact", "fullname", "Account Contact", xml4.join(""));
                                }
                                else {
                                    clear_contactaccount();
                                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
                                }
                            }, function (er) {
                                console.log(er.message)
                            });
                        }
                    }
                });
            }
        });
        fetch(xml5, "bsd_bankaccount", ["bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankaccountid", "bsd_bankgroup"], ["bsd_name"], false, null
                          , ["bsd_account", "statecode"], ["eq", "eq"], [0, khachhang[0].id, 1, 0]);
        CrmFetchKit.Fetch(xml5.join(""), false).then(function (rs5) {
            if (rs5.length > 0) {
                if (rs5[0].attributes.bsd_name != null) {
                    fieldbankaccount.setValue([{
                        id: '{' + rs5[0].Id.toUpperCase() + '}',
                        name: rs5[0].attributes.bsd_name.value,
                        entityType: rs5[0].logicalName
                    }]);
                    fieldbankgroup.setValue([{
                        id: '{' + rs5[0].attributes.bsd_bankgroup.guid.toUpperCase() + '}',
                        name: rs5[0].attributes.bsd_bankgroup.name,
                        entityType: rs5[0].attributes.bsd_bankgroup.logicalName
                    }]);
                    fieldbankaccountnumber.setValue(rs5[0].attributes.bsd_bankaccount.value);
                    fieldbrand.setValue(rs5[0].attributes.bsd_brand.value);
                }
                else {
                    fieldbankaccount.setValue(null);
                    fieldbankgroup.setValue(null);
                    fieldbankaccountnumber.setValue(null);
                    fieldbrand.setValue(null);
                }
                LookUp_After_Load("bsd_bankaccountb", "bsd_bankaccount", "bsd_name", "Account Bank Account", xml5.join(""));
            }
            if (rs5.length == 0) {
                clear_bankaccount();
            }
        });

    } else if (reset != false) {
        set_DisableWarehouse();
        clear_addressaccount();
        set_value(["bsd_mainphoneb", "bsd_faxb", "bsd_taxregistrationb", "bsd_brandb", "bsd_bankaccountnumberb", "bsd_bankgroupb", "bsd_accountnumberb"
                    , "bsd_emailb", "bsd_websiteb", "bsd_bankaccountb", "bsd_nguoidaidienb", "bsd_jobtitle"], null);
        clear_bankaccount();
        clear_contactaccount();
    }
}
function setValueDefault() {
    setValue("bsd_fromdate", getCurrentDateTime());
}
//Author:Mr.Đăng
//Description: Disable Outlet Type
function set_DisableWarehouse(reset) {
    debugger;
    var fetchxml = null
    var khachhang = getValue("bsd_account");
    var shipping = getValue("bsd_shipping");
    //var address_xhd = getValue("bsd_address");
    getControl("bsd_warehouseaddress").removePreSearch(presearch_AddressDeliveryShipping);
    if (shipping) {
        setValue("bsd_cachthucgiaonhan", shipping);
        setVisible("bsd_warehouse", false);
        if (reset != false) setNull(["bsd_warehouse", "bsd_warehouseaddress"]);
    } else {
        setVisible("bsd_warehouse", true);
        get_warehouseadd(reset);
    }
    if (khachhang != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                       '<entity name="bsd_address">',
                       '<attribute name="bsd_name" />',
                       '<attribute name="bsd_purpose" />',
                       '<attribute name="bsd_account" />',
                       '<attribute name="createdon" />',
                       '<attribute name="bsd_addressid" />',
                       '<order attribute="bsd_name" descending="false" />',
                       '<filter type="and">',
                       '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + khachhang[0].id + '" />',
                       '<condition attribute="bsd_purpose_tmpvalue" operator="like" value="%delivery%" />',
                       '</filter>',
                       '</entity>',
                       '</fetch>'].join("");
        LookUp_After_Load("bsd_warehouseaddress", "bsd_address", "bsd_name", "Address", fetchxml);
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs1) {
            if (shipping) {
                if (reset != false) setNull("bsd_warehouse");
                if (khachhang != null && fetchxml != null) {
                    console.log("ok" + rs1);
                    if (rs1.length > 0) {
                        if (getValue("bsd_warehouseaddress") == null) {
                            setValue("bsd_warehouseaddress", [{
                                id: rs1[0].Id,
                                name: rs1[0].attributes.bsd_name.value,
                                entityType: rs1[0].logicalName
                            }]);
                        }
                    }
                } else {
                    setNull("bsd_warehouseaddress");
                }
            }
        }, function (er) { });
    } else if (reset != false) {
        clear_AddressDeliveryShipping();
    }
}
//Author:Mr.Đăng
//Description: Load Dia Chi When Shipping: Yes
function Load_AddressDeliveryShipping(shipping) {
    debugger;
    var khachhang = getValue("bsd_account");
    var warehouse = getValue("bsd_warehouse");
    if (khachhang == null && shipping) setNull("bsd_warehouseaddress");
    if (warehouse != null) {
        setNull(["bsd_warehouse", "bsd_warehouseaddress"]);
    }
    shipping = getValue("bsd_shipping");
    getControl("bsd_warehouseaddress").removePreSearch(presearch_AddressDeliveryShipping);
    if (khachhang != null && shipping) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_purpose" />',
                        '<attribute name="bsd_account" />',
                        '<attribute name="createdon" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + khachhang[0].id + '" />',
                        '<condition attribute="bsd_purpose_tmpvalue" operator="like" value="' + "%" + "Delivery" + "%" + '" />',
                         '<condition attribute="statecode" operator="eq" value="0" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml)
        console.log(rs);
        LookUp_After_Load("bsd_warehouseaddress", "bsd_address", "bsd_name", "Address", fetchxml);
    }
    else if (khachhang == null && shipping) {
        clear_AddressDeliveryShipping();
    }
}
//author:Mr.Đăng
//Description: Load Đia chỉ Warehouse mặc định
function get_warehouseadd(reset) {
    debugger;
    if (reset != false) setNull("bsd_warehouseaddress");
    getControl("bsd_warehouseaddress").removePreSearch(presearch_AddressDeliveryShipping);
    var warehouse = getValue("bsd_warehouse");
    if (warehouse != null) {
        var shipping = getValue("bsd_shipping");
        if (!shipping) {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                            '<entity name="bsd_address">',
                            '<attribute name="bsd_name" />',
                            '<attribute name="bsd_addressid" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<link-entity name="bsd_warehouseentity" from="bsd_address" to="bsd_addressid" alias="ab">',
                            '<filter type="and">',
                            '<condition attribute="bsd_warehouseentityid" operator="eq" uitype="bsd_warehouseentity" value="' + warehouse[0].id + '" />',
                            '</filter>',
                            '</link-entity>',
                            '</entity>',
                            '</fetch>'].join("");
            LookUp_After_Load("bsd_warehouseaddress", "bsd_address", "bsd_name", "Warehouse", fetchxml);

            CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
                console.log(rs);
                if (rs.length > 0) {
                    var first = rs[0];
                    if (getValue("bsd_warehouseaddress") == null) {
                        setValue("bsd_warehouseaddress", [{
                            id: first.Id,
                            name: first.attributes.bsd_name.value,
                            entityType: first.logicalName
                        }]
                       );
                    }
                }

            }, function (er) { });
        }
    } else if (reset != false) {
        clear_AddressDeliveryShipping();
    }
}
function addressxhd_change(reset) {
    set_DisableWarehouse(reset);
}
function clear_AddressDeliveryShipping() {
    getControl("bsd_warehouseaddress").addPreSearch(presearch_AddressDeliveryShipping);
}
function presearch_AddressDeliveryShipping() {
    getControl("bsd_warehouseaddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}
//end

function clear_addressaccount() {
    getControl("bsd_address").addPreSearch(presearch_addressaccount);
}
function presearch_addressaccount() {
    getControl("bsd_address").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_bankaccount() {
    getControl("bsd_bankaccountb").addPreSearch(presearch_bankaccount);
}
function presearch_bankaccount() {
    getControl("bsd_bankaccountb").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_contactaccount() {
    getControl("bsd_nguoidaidienb").addPreSearch(presearch_contactaccount);
}
function presearch_contactaccount() {
    getControl("bsd_nguoidaidienb").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Loadtrader() {
    getControl("bsd_renter").addPreSearch(presearch_Loadtrader);
}
function presearch_Loadtrader() {
    getControl("bsd_renter").addPreSearch(presearch_Loadtrader);
}
function clear_LoadBankAccountTrader() {
    getControl("bsd_bankaccounta").addPreSearch(presearch_LoadBankAccountTrader);
}
function presearch_LoadBankAccountTrader() {
    getControl("bsd_bankaccounta").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Loadrepresentative() {
    getControl("bsd_nguoidaidiena").addPreSearch(presearch_Loadrepresentative);
}
function presearch_Loadrepresentative() {
    getControl("bsd_nguoidaidiena").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_LoadAddressTrader() {
    getControl("bsd_addressrenter").addPreSearch(presearch_LoadAddressTrader);
}
function presearch_LoadAddressTrader() {
    getControl("bsd_addressrenter").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function saveclose() {
    var param = Xrm.Page.context.getQueryStringParameters();
    var param1 = param["regarding_Id"];
    if (param1 != null) {
        Xrm.Page.ui.close();
    }
}