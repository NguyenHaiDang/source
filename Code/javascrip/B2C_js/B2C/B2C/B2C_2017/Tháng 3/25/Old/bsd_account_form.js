// JavaScript source code
// Global
var diachi = null;
var phuong = null;
var quanhuyen = null;
var tinhthanh = null;
var quocgia = null;

function OnLoad() {
    debugger;
    //Load dia chi
    anhiensetting();
    if (Xrm.Page.getAttribute("bsd_diachi").getValue() != null)
        MakeAddress();
    if (formType() == 2) {
        SetVisibale_LK_PriceGroup_Change();
        Load_LK_PriceList_PG_Change();
        Load_PriceList(false);
        setNull_Pricelist();
    }
    else {
        clear_pricelist();
    }
}
//
function Email_Change() {

    var email = getValue("emailaddress1");
    if (email != null) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email)) {
            Xrm.Page.getControl("emailaddress1").setNotification("Invalid email address");
        } else {
            Xrm.Page.getControl("emailaddress1").clearNotification();
        }
    }
}

//Diệm: 12/3/2017 Setnull price khi price list 
function setNull_Pricelist() {
    var pricelist = getValue("bsd_pricelist");
    if (pricelist != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="pricelevel">',
                        '    <attribute name="name" />',
                        '    <attribute name="transactioncurrencyid" />',
                        '    <attribute name="enddate" />',
                        '    <attribute name="begindate" />',
                        '    <attribute name="statecode" />',
                        '    <attribute name="pricelevelid" />',
                        '    <order attribute="name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="pricelevelid" operator="eq" uitype="pricelevel" value="' + pricelist[0].id + '" />',
                        '      <condition attribute="statecode" operator="eq" value="1" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            if (rs.length > 0) {
                setNull("bsd_pricelist");
                m_alert("Account không có price list hoặc price list đã hết hạn");
            }
        }, function (er) { });

        var layngayhientai = getFullDaySystem(new Date());
        var fetchxmldate = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="pricelevel">',
                        '    <attribute name="name" />',
                        '    <attribute name="transactioncurrencyid" />',
                        '    <attribute name="enddate" />',
                        '    <attribute name="begindate" />',
                        '    <attribute name="statecode" />',
                        '    <attribute name="pricelevelid" />',
                        '    <order attribute="name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="pricelevelid" operator="eq" uitype="pricelevel" value="' + pricelist[0].id + '" />',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '      <condition attribute="begindate" operator="on-or-before" value="' + layngayhientai + '" />',
                        '      <condition attribute="enddate" operator="on-or-after" value="' + layngayhientai + '" />',
                        '    </filter>',
                       '   </entity>',
                       ' </fetch>'].join('');
        CrmFetchKit.Fetch(fetchxmldate, false).then(function (rsdate) {
            if (rsdate.length == 0) {
                setNull("bsd_pricelist");
                m_alert("Account không có price list hoặc price list đã hết hạn");
            }
        }, function (er) { });
    }
}
/*Diệm: 24/03/2017 hiện branch là distributor*/
function setDisabledBranch(acctype) {
    var accounttype = getValue(acctype);
    if (accounttype != null) {
        if (accounttype == 100000000) {
            setVisibleSection("SUMMARY_TAB", "SUMMARY_TAB_section_12", true);
            setRequired("bsd_timeship", "required");
            setVisible(["bsd_distancetodistributor", "bsd_distanttooutlet", "bsd_timeship", "bsd_duetimeship", "bsd_minquantity", "bsd_allowreturnitem"], true);
        } else {
            setVisibleSection("SUMMARY_TAB", "SUMMARY_TAB_section_12", false);
            setNull(["bsd_distancetodistributor", "bsd_distanttooutlet", "bsd_timeship", "bsd_duetimeship", "bsd_minquantity", "bsd_allowreturnitem"]);
            setRequired("bsd_timeship", "none");
            setVisible(["bsd_distancetodistributor", "bsd_distanttooutlet", "bsd_timeship", "bsd_duetimeship", "bsd_minquantity", "bsd_allowreturnitem"], false);
        }
    }
}
/*
    Mr:Diệm
    Note: SetVisible, require và set null.
*/
function SetVisibale_LK_PriceGroup_Change() {
    //debugger;
    //if (getValue("bsd_pricegroups") != null) {
    //    setVisible("bsd_discountline", false);
    //    setNull("bsd_discountline");
    //    setRequired(["bsd_discountline"], "none");
    //    setVisible("bsd_multiline", false);
    //    setNull("bsd_multiline");
    //    setRequired(["bsd_multiline"], "none");
    //    setVisible("bsd_total", false);
    //    setNull("bsd_total");
    //    setRequired(["bsd_total"], "none");
    //}
    //else if (getValue("bsd_discountline") != null) {
    //    setVisible("bsd_pricegroups", false);
    //    setNull("bsd_pricegroups");
    //    setRequired(["bsd_pricegroups"], "none");
    //    setVisible("bsd_multiline", false);
    //    setNull("bsd_multiline");
    //    setRequired(["bsd_multiline"], "none");
    //    setVisible("bsd_total", false);
    //    setNull("bsd_total");
    //    setRequired(["bsd_total"], "none");
    //}
    //else if (getValue("bsd_multiline") != null) {
    //    setVisible("bsd_discountline", false);
    //    setNull("bsd_discountline");
    //    setRequired(["bsd_discountline"], "none");
    //    setVisible("bsd_pricegroups", false);
    //    setNull("bsd_pricegroups");
    //    setRequired(["bsd_pricegroups"], "none");
    //    setVisible("bsd_total", false);
    //    setNull("bsd_total");
    //    setRequired(["bsd_total"], "none");
    //}
    //else if (getValue("bsd_total") != null) {
    //    setVisible("bsd_discountline", false);
    //    setNull("bsd_discountline");
    //    setRequired(["bsd_discountline"], "none");
    //    setVisible("bsd_pricegroups", false);
    //    setNull("bsd_pricegroups");
    //    setRequired(["bsd_pricegroups"], "none");
    //    setVisible("bsd_multiline", false);
    //    setNull("bsd_multiline");
    //    setRequired(["bsd_multiline"], "none");
    //}
    //else {
    //    setVisible("bsd_discountline", true);
    //    setNull("bsd_discountline");
    //    setRequired("bsd_discountline", "required");
    //    setVisible("bsd_multiline", true);
    //    setNull("bsd_multiline");
    //    setRequired("bsd_multiline", "required");
    //    setVisible("bsd_pricegroups", true);
    //    setNull("bsd_pricegroups");
    //    setRequired("bsd_pricegroups", "required");
    //    setVisible("bsd_total", true);
    //    setNull("bsd_total");
    //    setRequired("bsd_total", "required");
    //    Load_LK_PriceList_PG_Change();
    //}
}
/*
    Mr:Diệm
    Note: Load price List.
*/
function Load_LK_PriceList_PG_Change() {
    if (getValue("bsd_pricegroups") == null) {
        Load_PriceGroup_change_Auto("bsd_pricegroups", "861450000");
    }
    if (getValue("bsd_discountline") == null) {
        Load_PriceGroup_change_Auto("bsd_discountline", "861450001");
    }
    if (getValue("bsd_multiline") == null) {
        Load_PriceGroup_change_Auto("bsd_multiline", "861450002");
    }
    if (getValue("bsd_total") == null) {
        Load_PriceGroup_change_Auto("bsd_total", "861450003");
    }
}

/*
    Mr:Diệm
    Note: ham Load Chung.
*/
function Load_PriceGroup_change_Auto(attributes, value) {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                       '  <entity name="bsd_pricegroups">',
                       '    <attribute name="bsd_pricegroupsid" />',
                       '    <attribute name="bsd_name" />',
                       '    <attribute name="createdon" />',
                       '    <order attribute="bsd_name" descending="false" />',
                       '    <filter type="and">',
                       '      <condition attribute="bsd_pricetype" operator="eq" value="' + value + '" />',
                       '    </filter>',
                       '  </entity>',
                       '</fetch>'].join();

    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_pricegroupsid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                    "<row name='result'  " + "id='bsd_pricegroupsid'>  " +
                    "<cell name='bsd_name'   " + "width='200' />  " +
                    "<cell name='createdon'    " + "width='100' />  " +
                    "</row>   " +
                 "</grid>";
    getControl(attributes).addCustomView(getControl(attributes).getDefaultView(), "bsd_pricegroups", "Price group", fetchxml, layoutXml, true);

}
//Diệm: 13/3/2017: load price list theo account.
function Load_PriceList(reset) {
    var layngayhientai = getFullDaySystem(new Date());
    if (reset != false) setNull("bsd_pricelist");
    getControl("bsd_pricelist").removePreSearch(presearch_pricelist);
    var account = getId();
    if (account != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="pricelevel">',
                        '    <attribute name="name" />',
                        '    <attribute name="transactioncurrencyid" />',
                        '    <attribute name="enddate" />',
                        '    <attribute name="begindate" />',
                        '    <attribute name="statecode" />',
                        '    <attribute name="pricelevelid" />',
                        '    <order attribute="name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '      <condition attribute="bsd_account" operator="eq" uitype="account" value="' + account + '" />',
                       '       <condition attribute="begindate" operator="on-or-before" value="' + layngayhientai + '" />',
                        '      <condition attribute="enddate" operator="on-or-after" value="' + layngayhientai + '" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        LookUp_After_Load("bsd_pricelist", "pricelevel", "name", "pricelevel", fetchxml);
    } else if (reset != false) {
        clear_pricelist();
    }
}
function clear_pricelist() {
    getControl("bsd_pricelist").addPreSearch(presearch_pricelist);
}
function presearch_pricelist() {
    getControl("bsd_pricelist").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
// Quoc Gia OnChange
function Onchange_QuocGia() {

    quanhuyen = "bsd_quan_thuongtru";
    tinhthanh = "bsd_thanhphotinhtieubang_thuongtru";
    Xrm.Page.getAttribute(tinhthanh).setValue(null);
    Xrm.Page.getAttribute(quanhuyen).setValue(null);
    MakeAddress();
}

// Tinh Thanh OnChange
function Onchange_TinhThanh() {
    Xrm.Page.getAttribute("bsd_quan_thuongtru").setValue(null);
    MakeAddress();
}

// Copy tinh thanh vao Address Composite
function MakeAddress() {
    debugger;
    //UnBlock 
    Xrm.Page.ui.controls.get("bsd_diachi").setDisabled(false);

    var tmp = null;
    var fulltmp = null;
    var sonha = null;
    var fulladdress = null;

    address = "address1_line1";
    province = "address1_stateorprovince";
    country = "address1_country";

    // field
    fulladdress = "bsd_diachi";
    sonha = "bsd_duongsonha_thuongtru";
    phuong = "bsd_phuong_thuongtru";
    quanhuyen = "bsd_quan_thuongtru";
    tinhthanh = "bsd_thanhphotinhtieubang_thuongtru";
    quocgia = "bsd_quocgialanhtho_thuongtru";

    //Duong & so nha
    if (Xrm.Page.getAttribute(sonha).getValue()) {
        tmp = Xrm.Page.getAttribute(sonha).getValue();
    }

    //phuong  
    if (Xrm.Page.getAttribute(phuong).getValue()) {
        if (tmp != "")
            tmp += ", " + Xrm.Page.getAttribute(phuong).getValue();
    }

    //quanhuyen  
    if (Xrm.Page.getAttribute(quanhuyen).getValue()) {
        if (tmp != null)
            tmp += ", " + Xrm.Page.getAttribute(quanhuyen).getValue()[0].name;
    }

    fulltmp = tmp;
    Xrm.Page.getAttribute(address).setValue(tmp);

    //tinh thanh
    if (Xrm.Page.getAttribute(tinhthanh).getValue()) {
        tmp = Xrm.Page.getAttribute(tinhthanh).getValue()[0].name;
        Xrm.Page.getAttribute(province).setValue(tmp);
        fulltmp += ", " + tmp;

    }

    //quoc gia
    if (Xrm.Page.getAttribute(quocgia).getValue()) {
        tmp = Xrm.Page.getAttribute(quocgia).getValue()[0].name;
        Xrm.Page.getAttribute(country).setValue(tmp);
        fulltmp += ", " + tmp;
    }
    Xrm.Page.getAttribute(fulladdress).setValue(fulltmp);
    Xrm.Page.ui.controls.get("bsd_diachi").setDisabled(true);
}

function hide_Industrial() {
    debugger;
    var loai = Xrm.Page.getAttribute("bsd_industry_vl").getValue();

    if (loai != null) {
        Xrm.Page.getControl("bsd_beverage").setVisible(loai.indexOf(1) != -1);
        Xrm.Page.getControl("bsd_food").setVisible(loai.indexOf(2) != -1);
        Xrm.Page.getControl("bsd_milkicecream").setVisible(loai.indexOf(3) != -1);
        Xrm.Page.getControl("bsd_confectionary").setVisible(loai.indexOf(4) != -1);

    }
}
function filter_subgriddkbylookupkh() {
    if (Xrm.Page.ui.getFormType() == 2) {
        //var objSubGrid = document.getElementById("gridaddress");
        //var objSubGrid = window.parent.document.getElementById("gridaddress");
        //var objSubGrid = Xrm.Page.getControl("gridaddress").getGrid();
        var objSubGrid = document.getElementById("gridaddress");
        if (objSubGrid == null) {
            setTimeout(filter_subgriddkbylookupkh, 2000);
            return;
        } else {
            var lookup = Xrm.Page.getAttribute("customerid").getValue();
            //if (lookup != null) {
            var FetchXml = "<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0'>" +
                                "<entity name='bsd_address'>" +
                                    "<attribute name='bsd_addressid'/>" +
                                    "<attribute name='bsd_name'/>" +
                                    "<attribute name='bsd_name'/>" +
                                    "<attribute name='createdon'/>" +
                                    "<attribute name='bsd_account'/>" +
                                    "<order attribute='bsd_name' descending='false' />" +
                                    "<filter type='and'>" +
                                        "<condition attribute='bsd_account' operator='null' />" +
                                     "</filter>" +
                                "</entity>" +
                            "</fetch>";
            if (objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", FetchXml);
                objSubGrid.control.Refresh();
            } else {
                setTimeout(filter_subgriddkbylookupkh, 500);
            }
            //}
        }
    }
}
//auhthor:Mr.Phong
//desciption:hide button add subgrid
function set_hidebutton() {
    if (Xrm.Page.ui.getFormType() == 2) {
        try {
            window.parent.frames[0].document.getElementById('Contacts_contextualButtonsContainer').style.visibility = "hidden";
            window.parent.frames[0].document.getElementById('Contacts_openAssociatedGridViewImageButton').style.visibility = "hidden";
        } catch (err) {

        }
    }
    if (Xrm.Page.ui.getFormType() == 1) {
        try {
            window.parent.frames[0].document.getElementById('Contacts_contextualButtonsContainer').style.visibility = "hidden";
            window.parent.frames[0].document.getElementById('Contacts_openAssociatedGridViewImageButton').style.visibility = "hidden";
        } catch (err) {

        }

    }
}
//author:Mr.Phong
//description:filter customer contact
function filter_customertheocontact() {
    var objSubGrid = document.getElementById("Contacts");
    if (objSubGrid == null) {
        setTimeout(filter_customertheocontact, 2000);
        return;
    } else {
        var lookup = Xrm.Page.getAttribute("name").getValue();
        if (lookup != null) {
            var FetchXml = "<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0'>" +
                                "<entity name='contact'>" +
                                    "<attribute name='fullname'/>" +
                                    "<attribute name='telephone1'/>" +
                                    "<attribute name='contactid'/>" +
                                    "<order attribute='fullname' descending='false' />" +
                                    "<filter type='and'>" +
                                        "<condition attribute='jobtitle' operator='null'/>" +
                                        "<condition attribute='parentcustomeridname' operator='like' value='" % lookup % "' />" +
                                     "</filter>" +
                                "</entity>" +
                            "</fetch>";
            if (objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", FetchXml);
                objSubGrid.control.Refresh();
            } else {
                setTimeout(filter_customertheocontact, 500);
            }
        }
    }
}
//author:Mr.Phong
//description:set contact type contact account
function set_contactloaicontactaccount() {
    var lookup = Xrm.Page.getAttribute("name").getValue();
    var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
    var entityName = "contact";
    var viewDisplayName = "test";
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='contact'>");
    xml.push("<attribute name='fullname'/>");
    xml.push("<attribute name='telephone1' />")
    xml.push("<attribute name='contactid' />");
    xml.push("<attribute name='accountid' />");
    xml.push("<order attribute='fullname' descending='false' />");
    xml.push("<filter type='and' >");
    xml.push("<condition attribute='bsd_contacttype' operator='eq' value='861450000' />");
    xml.push("<condition attribute='parentcustomeridname' operator='like' value='" % lookup % "' />");
    xml.push(" <condition attribute='statecode' operator='eq' value='0' />");
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='contactid'>  " +
                       "<cell name='fullname'   " + "width='200' />  " +
                       "<cell name='telephone1'    " + "width='100' />  " +
                         "<cell name='accountid'    " + "width='100' />  " +
                       "</row>   " +
                    "</grid>   ";
    Xrm.Page.getControl("primarycontactid").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
}
//Author:Mr.Phong
//Description:check same tax registration for each account
function check_sametaxregistrationonchange() {
    var taxregistration = Xrm.Page.getAttribute("bsd_taxregistration").getValue();
    var name = Xrm.Page.getAttribute("name").getValue();
    var noidentify = Xrm.Page.getAttribute("bsd_noidentify").getValue();
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='account'>");
    xml.push("<attribute name='name' />");
    xml.push("<attribute name='primarycontactid' />");
    xml.push("<attribute name='telephone1' />");
    xml.push("<attribute name='accountid' />");
    xml.push("<order attribute='name' descending='false' />");
    xml.push("<filter type='and'>");
    xml.push("<condition attribute='bsd_taxregistration' operator='eq'   value='" + taxregistration + "' />");
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
        if (rs.length > 0) {
            alert("Tax Registration is exist!!!");
            if (Xrm.Page.ui.getFormType() == 1) {
                Xrm.Page.getAttribute("name").setValue(null);
            }
            Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
        }
    }, function (er) {
        console.log(er.message)
    });
}
//Author:Mr.phong
//Description:set no identify
function set_noindentify() {
    if (Xrm.Page.getAttribute("bsd_noidentify").getValue() == null) {
        if (Xrm.Page.ui.getFormType() == 1) {
            Xrm.Page.getAttribute("bsd_noidentify").setValue("1");
        }
    }
}
//Author:Mr.Phong
//Description:create principal contract
function createprincipalcontract() {
    var noidentify = Xrm.Page.getAttribute("bsd_noidentify").getValue();
    if (noidentify == "0") {
        Notify.add("Bạn có muốn tạo hợp đồng nguyên tắc không?", "LOADING", "sale",
     [{
         type: "button",
         text: "Create Principal Contract",
         callback: function createprincipal(parameters) {
             var parameters = {};
             parameters["regarding_Id"] = Xrm.Page.data.entity.getId();
             Xrm.Utility.openEntityForm("bsd_principalcontract", null, parameters);
         }
     },
     {
         type: "link",
         text: "Tạo sau!",
         callback: function () {
             Notify.remove("sale");
         }
     }]);
    }


}
// trung. kiem tra da co tax chua.
function check_exist_tax_registration() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var number = getValue("bsd_taxregistration");
    if (number != null) {
        if (mikExp.test(number)) {
            setNotification("bsd_taxregistration", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var xml = [];
            var bsd_taxregistration = Xrm.Page.getAttribute("bsd_taxregistration").getValue();
            xml.push('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">');
            xml.push('<entity name="account">');
            xml.push('<attribute name="name" />');
            xml.push('<attribute name="telephone1" />');
            xml.push('<attribute name="accountid" />');
            xml.push('<filter type="and">');
            xml.push('<condition attribute="bsd_taxregistration" operator="eq" value="' + bsd_taxregistration + '" />');
            var id = Xrm.Page.data.entity.getId();
            if (id) {
                xml.push('<condition attribute="accountid" operator="ne" uitype="account" value="' + id + '" />');
            }
            xml.push('</filter>');
            xml.push('</entity>');
            xml.push('</fetch>');
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    Xrm.Page.getControl("bsd_taxregistration").setNotification("Tax Registration is exist!!!");
                } else {
                    Xrm.Page.getControl("bsd_taxregistration").clearNotification();
                }
            }, function () { });
        }
    }
}
//Author:Mr.Phong
//Description:check same tax registration for each account
function check_sametaxregistrationonsave() {
    var taxregistration = Xrm.Page.getAttribute("bsd_taxregistration").getValue();
    var name = Xrm.Page.getAttribute("name").getValue();
    var noidentify = Xrm.Page.getAttribute("bsd_noidentify").getValue();
    var xml = [];
    if (noidentify == "1") {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='account'>");
        xml.push("<attribute name='name' />");
        xml.push("<attribute name='primarycontactid' />");
        xml.push("<attribute name='telephone1' />");
        xml.push("<attribute name='accountid' />");
        xml.push("<order attribute='name' descending='false' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='bsd_taxregistration' operator='eq'   value='" + taxregistration + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length == 0) {
                Xrm.Page.getAttribute("bsd_noidentify").setValue("0");
                window.top.$ui.Confirm("Xác nhận", "Bạn có muốn tạo HDNT không ?", function (e) {
                    var windowOptions = {
                        openInNewWindow: true
                    };
                    var parameters = {};
                    parameters["regarding_Id"] = Xrm.Page.data.entity.getId();
                    Xrm.Utility.openEntityForm("bsd_principalcontract", null, parameters, windowOptions);
                }, null);
            }
        }, function (er) {
            console.log(er.message)
        });

    }
}
//author:Mr.Phong
function hide_button() {
    window.parent.document.getElementById("bsd_subSaleman_contextualButtonsContainer").style.display = "none";
    window.parent.document.getElementById("bsd_subSaleman_openAssociatedGridViewImageButton").style.visibility = "hidden";
    window.parent.document.getElementById("subgrid_employee_contextualButtonsContainer").style.display = "none";
    window.parent.document.getElementById("subgrid_employee_openAssociatedGridViewImageButton").style.visibility = "hidden";
}
//Author:Mr.Phong
//Description:an hoac hien field setting khi accountype la NPP & business registration certificate
function anhiensetting() {
    var accounttype = Xrm.Page.getAttribute("bsd_accounttype").getValue();
    if (accounttype == 100000000) {
        setVisible(["bsd_setting", "bsd_subSaleman", "subgrid_employee", "bsd_packinglimit", "bsd_prefix", "bsd_areaofactivity"], true);
        setRequired("bsd_setting", "required")
    }
    else {
        setVisible(["bsd_setting", "bsd_subSaleman", "subgrid_employee", "bsd_packinglimit", "bsd_prefix", "bsd_areaofactivity"], false);
        setNull(["bsd_setting", "bsd_areaofactivity"]);
        setRequired("bsd_setting", "none")
    }

    if (accounttype == 861450005) {
        setVisible("bsd_businessregistrationcertificate", true);
        setRequired("bsd_businessregistrationcertificate", "required");
    }
    else if (accounttype != 861450005) {
        setVisible("bsd_businessregistrationcertificate", false);
        setRequired("bsd_businessregistrationcertificate", "none");
    }
    if (accounttype == 861450005 || accounttype == 100000000) {
        setVisible("bsd_businesslicensenumber", true);
        setRequired("bsd_businesslicensenumber", "required");
    }
    else {
        setVisible("bsd_businesslicensenumber", false);
        setNull(["bsd_businesslicensenumber"])
        setRequired("bsd_businesslicensenumber", "none");
    }
    setDisabledBranch("bsd_accounttype");
}
//Author:Mr.Đăng
//Description:Check Name
function Check_Businesslicensenumber() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var businesslicensenumber = getValue("bsd_businesslicensenumber");
    var id = getId();
    if (businesslicensenumber != null) {
        if (mikExp.test(businesslicensenumber)) {
            setNotification("bsd_businesslicensenumber", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_businesslicensenumber");
        }
    }
}
function clear_contact() {
    Xrm.Page.getControl("primarycontactid").addPreSearch(presearch_contact);
}
function presearch_contact() {
    Xrm.Page.getControl("primarycontactid").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Description:set contact onload
function set_contactonload() {
    if (formType() == 1) {
        //setDisabled("primarycontactid", true);
        clear_contact();

    }
}
