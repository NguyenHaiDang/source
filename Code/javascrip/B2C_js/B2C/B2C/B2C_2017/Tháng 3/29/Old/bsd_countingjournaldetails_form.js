function AutoLoad() {
    LK_LoadAccountDistributor();
    if (formType() == 2) {
        LK_LoadWarehouse(false);
        LK_LoadProductDistributor(false);
        //LK_LoadQuantityProduct();
    } else {
        clear_Warehouse();
        clear_Product();
    }
}
//Author:Mr.Đăng
//Description: Load Warehouse theo Account
function LK_LoadAccountDistributor() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="primarycontactid" />',
                    '<attribute name="telephone1" />',
                    '<attribute name="accountid" />',
                    '<order attribute="name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="100000000" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    var rs = CrmFetchKit.FetchSync(fetchxml)
    LookUp_After_Load("bsd_distributor", "account", "name", "Distributor", fetchxml);
    LK_LoadWarehouse(false);
}
//Author:Mr.Đăng
//Description: Load Warehouse theo Distributor Account 
function LK_LoadWarehouse(reset) {
    if (reset != false) setNull(["bsd_warehousedms", "bsd_product", "bsd_netquatity"]);
    var distributor = getValue("bsd_distributor");
    getControl("bsd_warehousedms").removePreSearch(presearch_Warehouse);
    if (distributor != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_warehousedms">',
                        '<attribute name="bsd_warehousedmsid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + distributor[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml)
        if (rs.length > 0) {
            var first = rs[0];
            if (getValue("bsd_warehousedms") == null) {
                setValue("bsd_warehousedms", [{
                    id: first.Id,
                    name: first.attributes.bsd_name.value,
                    entityType: first.logicalName
                }]
               );
                LK_LoadProductDistributor(false);
            }
        }
        LookUp_After_Load("bsd_warehousedms", "bsd_warehousedms", "bsd_name", "Warehouse DMS", fetchxml);
    }
    else if (reset != false) {
        clear_Warehouse();
        clear_Product();
    }
}
function clear_Warehouse() {
    getControl("bsd_warehousedms").addPreSearch(presearch_Warehouse);
}
function presearch_Warehouse() {
    getControl("bsd_warehousedms").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.Đăng
//Description: Load Warehouse theo Account
function LK_LoadProductDistributor(reset) {
    if (reset != false) setNull(["bsd_product", "bsd_netquatity"]);
    var warehouse = getValue("bsd_warehousedms");
    getControl("bsd_product").removePreSearch(presearch_Product);
    if (warehouse != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                        '<entity name="product">',
                        '<attribute name="name" />',
                        '<attribute name="productnumber" />',
                        '<attribute name="description" />',
                        '<attribute name="statecode" />',
                        '<attribute name="productid" />',
                        '<attribute name="productstructure" />',
                        '<order attribute="productnumber" descending="false" />',
                        '<link-entity name="bsd_warehourseproductdms" from="bsd_product" to="productid" alias="ac">',
                        '<filter type="and">',
                        '<condition attribute="bsd_warehouses" operator="eq" uitype="bsd_warehousedms" value="' + warehouse[0].id + '" />',
                        '</filter>',
                        '</link-entity>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml)
        LookUp_After_Load("bsd_product", "product", "name", "Product", fetchxml);
    }
    else if (reset != false) clear_Product();
}
function clear_Product() {
    getControl("bsd_product").addPreSearch(presearch_Product);
}
function presearch_Product() {
    getControl("bsd_product").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.Đăng
//Description: Load Quantity
function LK_LoadQuantityProduct() {
    var product = getValue("bsd_product");
    var warehouse = getValue("bsd_warehousedms");
    if (product != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_warehourseproductdms">',
                        '<attribute name="bsd_warehourseproductdmsid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<attribute name="bsd_quantity" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_product" operator="eq" uitype="product" value="' + product[0].id + '" />',
                        '<condition attribute="bsd_warehouses" operator="eq" uitype="bsd_warehousedms" value="' + warehouse[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        console.log(rs);
        if (rs.length > 0) {
            setValue("bsd_netquatity", rs[0].attributes.bsd_quantity.value);
        } else {
            setNull("bsd_netquatity");
        }
    } else {
        setNull("bsd_netquatity");
    }
}
//Author:Mr.Diệm
//Description: LookUp_After_Load
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}
//Author:Mr.Đăng
//Description: Load Quantity
function QuantityDifference() {
    var netquantity = 0;
    if (getValue("bsd_netquatity") != null) {
        netquantity = getValue("bsd_netquatity");
    }
    var quantity = 0;
    if (getValue("bsd_quantity") != null) {
        quantity = getValue("bsd_quantity");
    }
    var quantitydifference = 0;
    quantitydifference = netquantity - quantity;
    setValue("bsd_quantitydifference", quantitydifference);
}