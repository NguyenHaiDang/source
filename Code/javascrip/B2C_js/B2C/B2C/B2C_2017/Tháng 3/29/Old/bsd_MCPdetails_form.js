function AutoLoad() {
    get_alloutletonload();
    set_valuedate();
    //setvalue_filedcount();
    set_startdate();
    //displayorhide_fieldnextweekonload();
    var statuscodet = getValue("statuscode");
    if (formType() == 1) {
        setValue("statuscode", 100000000);
    } else {
        Active_Change();
    }
}
//Author:Mr.Phong
//Desciption:get all outlet from employee frm MCP
function get_alloutlet() {
    debugger;
    Xrm.Page.getAttribute("bsd_name").setSubmitMode("always");
    var MCP = Xrm.Page.getAttribute("bsd_mcp").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    var a = [];
    Xrm.Page.getControl("bsd_outlet").removePreSearch(presearch_outlet);
    if (MCP != null) {
        setNull(["bsd_numberofvisit", "bsd_outlet", "bsd_outetcode", "bsd_visitschedule", "bsd_numberofvisit", "bsd_numericalorder", "bsd_startdate", "bsd_enddate"]);
        setvalue(["bsd_mondayvaluee", "bsd_tuesdayvaluee", "bsd_wednesdayvaluee", "bsd_thursdayvaluee", "bsd_fridayvaluee", "bsd_saturdayvaluee", "bsd_sundayvaluee", "bsd_countnew"], 0);
        xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
        xml3.push("<entity name='bsd_mcpdetail'>");
        xml3.push("<attribute name='bsd_mcpdetailid' />");
        xml3.push("<attribute name='bsd_name' />");
        xml3.push("<attribute name='createdon' />");
        xml3.push("<attribute name='bsd_outlet' />");
        xml3.push("<order attribute='bsd_name' descending='false' />");
        xml3.push("<filter type='and'>");
        xml3.push("<condition attribute='bsd_mcp' operator='eq' uitype='bsd_mcp' value='" + MCP[0].id + "' />");
        xml3.push("</filter>");
        xml3.push("</entity>");
        xml3.push("</fetch>");
        CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
            if (rs3.length > 0) {
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                xml.push("<entity name='bsd_mcp'>");
                xml.push("<attribute name='bsd_mcpid' />");
                xml.push("<attribute name='bsd_name' />");
                xml.push("<attribute name='createdon' />");
                xml.push("<attribute name='bsd_employee' />");
                xml.push("<order attribute='bsd_name' descending='false' />");
                xml.push("<filter type='and'>");
                xml.push("<condition attribute='bsd_mcpid' operator='eq' uitype='bsd_mcp' value='" + MCP[0].id + "' />");
                xml.push("</filter>");
                xml.push("</entity>");
                xml.push("</fetch>");
                CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                    if (rs.length > 0 && rs[0].attributes.bsd_employee != null) {
                        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                        xml1.push("<entity name='bsd_employee'>");
                        xml1.push("<attribute name='bsd_name' />");
                        xml1.push("<attribute name='createdon' />");
                        xml1.push("<attribute name='bsd_account' />");
                        xml1.push("<attribute name='bsd_employeeid' />");
                        xml1.push("<attribute name='bsd_parentaccount' />");
                        xml1.push("<order attribute='bsd_name' descending='false' />");
                        xml1.push("<filter type='and'>");
                        xml1.push("<condition attribute='bsd_employeeid' operator='eq' uitype='bsd_employee' value='" + rs[0].attributes.bsd_employee.guid + "' />");
                        xml1.push("</filter>");
                        xml1.push("</entity>");
                        xml1.push("</fetch>");
                        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                            if (rs1.length > 0 && rs1[0].attributes.bsd_account != null) {
                                var entityName = "contact";
                                var viewDisplayName = "test";
                                var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                                xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                xml2.push("<entity name='contact'>");
                                xml2.push("<attribute name='fullname' />");
                                xml2.push("<attribute name='telephone1' />");
                                xml2.push("<attribute name='contactid' />");
                                xml2.push("<order attribute='fullname' descending='false' />");
                                xml2.push("<filter type='and'>");
                                xml2.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + rs1[0].attributes.bsd_account.guid + "' />");
                                xml2.push("<condition attribute='bsd_contacttype' operator='eq'  value='861450001' />");
                                xml2.push("<condition attribute='bsd_employee' operator='eq' uitype='bsd_employee' value='" + rs[0].attributes.bsd_employee.guid + "' />");
                                xml2.push("</filter>");
                                xml2.push("</entity>");
                                xml2.push("</fetch>");
                                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='contactid'>  " +
                                   "<cell name='fullname'   " + "width='200' />  " +
                                   "<cell name='telephone1'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                                CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                                    if (rs2.length > 0) {
                                        Xrm.Page.getControl("bsd_outlet").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
                                        //Xrm.Page.getControl("bsd_outlet").setDisabled(false);
                                    }
                                    if (rs2.length == 0) {
                                        //Xrm.Page.getControl("bsd_outlet").setDisabled(true);
                                    }
                                }, function (er) {
                                    console.log(er.message)
                                });
                            }
                            else if (rs1.length > 0 && rs1[0].attributes.bsd_parentaccount != null) {
                                var entityName = "contact";
                                var viewDisplayName = "test";
                                var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                                xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                xml2.push("<entity name='contact'>");
                                xml2.push("<attribute name='fullname' />");
                                xml2.push("<attribute name='telephone1' />");
                                xml2.push("<attribute name='contactid' />");
                                xml2.push("<order attribute='fullname' descending='false' />");
                                xml2.push("<filter type='and'>");
                                xml2.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + rs1[0].attributes.bsd_parentaccount.guid + "' />");
                                xml2.push("<condition attribute='bsd_contacttype' operator='eq'  value='861450001' />");
                                xml2.push("<condition attribute='bsd_employee' operator='eq' uitype='bsd_employee' value='" + rs[0].attributes.bsd_employee.guid + "' />");
                                xml2.push("</filter>");
                                xml2.push("</entity>");
                                xml2.push("</fetch>");
                                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='contactid'>  " +
                                   "<cell name='fullname'   " + "width='200' />  " +
                                   "<cell name='telephone1'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                                CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                                    if (rs2.length > 0) {
                                        Xrm.Page.getControl("bsd_outlet").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
                                        // Xrm.Page.getControl("bsd_outlet").setDisabled(false);
                                    }
                                    if (rs2.length == 0) {
                                        //  Xrm.Page.getControl("bsd_outlet").setDisabled(true);
                                    }
                                }, function (er) {
                                    console.log(er.message)
                                });
                            }
                        }, function (er) {
                            console.log(er.message)
                        });
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
            if (rs3.length == 0) {
                xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                xml4.push("<entity name='bsd_mcp'>");
                xml4.push("<attribute name='bsd_mcpid' />");
                xml4.push("<attribute name='bsd_name' />");
                xml4.push("<attribute name='createdon' />");
                xml4.push("<attribute name='bsd_employee' />");
                xml4.push("<order attribute='bsd_name' descending='false' />");
                xml4.push("<filter type='and'>");
                xml4.push("<condition attribute='bsd_mcpid' operator='eq' uitype='bsd_mcp' value='" + MCP[0].id + "' />");
                xml4.push("</filter>");
                xml4.push("</entity>");
                xml4.push("</fetch>");
                CrmFetchKit.Fetch(xml4.join(""), true).then(function (rs4) {
                    if (rs4.length > 0 && rs4[0].attributes.bsd_employee != null) {
                        xml5.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                        xml5.push("<entity name='bsd_employee'>");
                        xml5.push("<attribute name='bsd_name' />");
                        xml5.push("<attribute name='createdon' />");
                        xml5.push("<attribute name='bsd_account' />");
                        xml5.push("<attribute name='bsd_employeeid' />");
                        xml5.push("<order attribute='bsd_name' descending='false' />");
                        xml5.push("<filter type='and'>");
                        xml5.push("<condition attribute='bsd_employeeid' operator='eq' uitype='bsd_employee' value='" + rs4[0].attributes.bsd_employee.guid + "' />");
                        xml5.push("</filter>");
                        xml5.push("</entity>");
                        xml5.push("</fetch>");
                        CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                            if (rs5.length > 0 && rs5[0].attributes.bsd_account != null) {
                                var entityName = "contact";
                                var viewDisplayName = "test";
                                var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                                xml6.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                xml6.push("<entity name='contact'>");
                                xml6.push("<attribute name='fullname' />");
                                xml6.push("<attribute name='telephone1' />");
                                xml6.push("<attribute name='contactid' />");
                                xml6.push("<order attribute='fullname' descending='false' />");
                                xml6.push("<filter type='and'>");
                                xml6.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + rs5[0].attributes.bsd_account.guid + "' />");
                                xml6.push("<condition attribute='bsd_contacttype' operator='eq'  value='861450001' />");
                                xml6.push("<condition attribute='bsd_employee' operator='eq' uitype='bsd_employee' value='" + rs4[0].attributes.bsd_employee.guid + "' />");
                                xml6.push("</filter>");
                                xml6.push("</entity>");
                                xml6.push("</fetch>");
                                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='contactid'>  " +
                                   "<cell name='fullname'   " + "width='200' />  " +
                                   "<cell name='telephone1'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                                //Xrm.Page.getControl("bsd_outlet").addCustomView(viewId, entityName, viewDisplayName, xml6.join(""), layoutXml, true);
                                CrmFetchKit.Fetch(xml6.join(""), true).then(function (rs6) {
                                    if (rs6.length > 0) {
                                        Xrm.Page.getControl("bsd_outlet").addCustomView(viewId, entityName, viewDisplayName, xml6.join(""), layoutXml, true);
                                        // Xrm.Page.getControl("bsd_outlet").setDisabled(false);
                                    }
                                    if (rs6.length == 0) {
                                        // Xrm.Page.getControl("bsd_outlet").setDisabled(true);
                                    }
                                }, function (er) {
                                    console.log(er.message)
                                });
                            }
                        }, function (er) {
                            console.log(er.message)
                        });
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    else {
        //setNull(["bsd_numberofvisit", "bsd_outlet", "bsd_outetcode", "bsd_visitschedule", "bsd_numberofvisit", "bsd_numericalorder", "bsd_startdate", "bsd_enddate"]);
        //setvalue(["bsd_mondayvaluee", "bsd_tuesdayvaluee", "bsd_wednesdayvaluee", "bsd_thursdayvaluee", "bsd_fridayvaluee", "bsd_saturdayvaluee", "bsd_sundayvaluee", "bsd_countnew"], 0);
        clear_outlet();
    }
}
//Author:Mr.Phong
//Desciption:get all outlet from employee frm MCP
function get_alloutletonload() {
    Xrm.Page.getAttribute("bsd_name").setSubmitMode("always");
    var MCP = Xrm.Page.getAttribute("bsd_mcp").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    var a = [];
    Xrm.Page.getControl("bsd_outlet").removePreSearch(presearch_outlet);
    if (MCP != null) {
        xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
        xml3.push("<entity name='bsd_mcpdetail'>");
        xml3.push("<attribute name='bsd_mcpdetailid' />");
        xml3.push("<attribute name='bsd_name' />");
        xml3.push("<attribute name='createdon' />");
        xml3.push("<attribute name='bsd_outlet' />");
        xml3.push("<order attribute='bsd_name' descending='false' />");
        xml3.push("<filter type='and'>");
        xml3.push("<condition attribute='bsd_mcp' operator='eq' uitype='bsd_mcp' value='" + MCP[0].id + "' />");
        xml3.push("</filter>");
        xml3.push("</entity>");
        xml3.push("</fetch>");
        CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
            if (rs3.length > 0) {
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                xml.push("<entity name='bsd_mcp'>");
                xml.push("<attribute name='bsd_mcpid' />");
                xml.push("<attribute name='bsd_name' />");
                xml.push("<attribute name='createdon' />");
                xml.push("<attribute name='bsd_employee' />");
                xml.push("<order attribute='bsd_name' descending='false' />");
                xml.push("<filter type='and'>");
                xml.push("<condition attribute='bsd_mcpid' operator='eq' uitype='bsd_mcp' value='" + MCP[0].id + "' />");
                xml.push("</filter>");
                xml.push("</entity>");
                xml.push("</fetch>");
                CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                    if (rs.length > 0 && rs[0].attributes.bsd_employee != null) {
                        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                        xml1.push("<entity name='bsd_employee'>");
                        xml1.push("<attribute name='bsd_name' />");
                        xml1.push("<attribute name='createdon' />");
                        xml1.push("<attribute name='bsd_account' />");
                        xml1.push("<attribute name='bsd_employeeid' />");
                        xml1.push("<attribute name='bsd_parentaccount' />");
                        xml1.push("<order attribute='bsd_name' descending='false' />");
                        xml1.push("<filter type='and'>");
                        xml1.push("<condition attribute='bsd_employeeid' operator='eq' uitype='bsd_employee' value='" + rs[0].attributes.bsd_employee.guid + "' />");
                        xml1.push("</filter>");
                        xml1.push("</entity>");
                        xml1.push("</fetch>");
                        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                            if (rs1.length > 0 && rs1[0].attributes.bsd_account != null) {
                                var entityName = "contact";
                                var viewDisplayName = "test";
                                var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                                xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                xml2.push("<entity name='contact'>");
                                xml2.push("<attribute name='fullname' />");
                                xml2.push("<attribute name='telephone1' />");
                                xml2.push("<attribute name='contactid' />");
                                xml2.push("<order attribute='fullname' descending='false' />");
                                xml2.push("<filter type='and'>");
                                xml2.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + rs1[0].attributes.bsd_account.guid + "' />");
                                xml2.push("<condition attribute='bsd_contacttype' operator='eq'  value='861450001' />");
                                xml2.push("<condition attribute='bsd_employee' operator='eq' uitype='bsd_employee' value='" + rs[0].attributes.bsd_employee.guid + "' />");
                                xml2.push("</filter>");
                                xml2.push("</entity>");
                                xml2.push("</fetch>");
                                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='contactid'>  " +
                                   "<cell name='fullname'   " + "width='200' />  " +
                                   "<cell name='telephone1'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                                CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                                    if (rs2.length > 0) {
                                        Xrm.Page.getControl("bsd_outlet").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
                                        //Xrm.Page.getControl("bsd_outlet").setDisabled(false);
                                    }
                                    if (rs2.length == 0) {
                                        //Xrm.Page.getControl("bsd_outlet").setDisabled(true);
                                    }
                                }, function (er) {
                                    console.log(er.message)
                                });
                            }
                            else if (rs1.length > 0 && rs1[0].attributes.bsd_parentaccount != null) {
                                var entityName = "contact";
                                var viewDisplayName = "test";
                                var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                                xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                xml2.push("<entity name='contact'>");
                                xml2.push("<attribute name='fullname' />");
                                xml2.push("<attribute name='telephone1' />");
                                xml2.push("<attribute name='contactid' />");
                                xml2.push("<order attribute='fullname' descending='false' />");
                                xml2.push("<filter type='and'>");
                                xml2.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + rs1[0].attributes.bsd_parentaccount.guid + "' />");
                                xml2.push("<condition attribute='bsd_contacttype' operator='eq'  value='861450001' />");
                                xml2.push("<condition attribute='bsd_employee' operator='eq' uitype='bsd_employee' value='" + rs[0].attributes.bsd_employee.guid + "' />");
                                xml2.push("</filter>");
                                xml2.push("</entity>");
                                xml2.push("</fetch>");
                                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='contactid'>  " +
                                   "<cell name='fullname'   " + "width='200' />  " +
                                   "<cell name='telephone1'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                                CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                                    if (rs2.length > 0) {
                                        Xrm.Page.getControl("bsd_outlet").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
                                        // Xrm.Page.getControl("bsd_outlet").setDisabled(false);
                                    }
                                    if (rs2.length == 0) {
                                        //  Xrm.Page.getControl("bsd_outlet").setDisabled(true);
                                    }
                                }, function (er) {
                                    console.log(er.message)
                                });
                            }
                        }, function (er) {
                            console.log(er.message)
                        });
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
            if (rs3.length == 0) {
                xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                xml4.push("<entity name='bsd_mcp'>");
                xml4.push("<attribute name='bsd_mcpid' />");
                xml4.push("<attribute name='bsd_name' />");
                xml4.push("<attribute name='createdon' />");
                xml4.push("<attribute name='bsd_employee' />");
                xml4.push("<order attribute='bsd_name' descending='false' />");
                xml4.push("<filter type='and'>");
                xml4.push("<condition attribute='bsd_mcpid' operator='eq' uitype='bsd_mcp' value='" + MCP[0].id + "' />");
                xml4.push("</filter>");
                xml4.push("</entity>");
                xml4.push("</fetch>");
                CrmFetchKit.Fetch(xml4.join(""), true).then(function (rs4) {
                    if (rs4.length > 0 && rs4[0].attributes.bsd_employee != null) {
                        xml5.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                        xml5.push("<entity name='bsd_employee'>");
                        xml5.push("<attribute name='bsd_name' />");
                        xml5.push("<attribute name='createdon' />");
                        xml5.push("<attribute name='bsd_account' />");
                        xml5.push("<attribute name='bsd_employeeid' />");
                        xml5.push("<order attribute='bsd_name' descending='false' />");
                        xml5.push("<filter type='and'>");
                        xml5.push("<condition attribute='bsd_employeeid' operator='eq' uitype='bsd_employee' value='" + rs4[0].attributes.bsd_employee.guid + "' />");
                        xml5.push("</filter>");
                        xml5.push("</entity>");
                        xml5.push("</fetch>");
                        CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                            if (rs5.length > 0 && rs5[0].attributes.bsd_account != null) {
                                var entityName = "contact";
                                var viewDisplayName = "test";
                                var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                                xml6.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                xml6.push("<entity name='contact'>");
                                xml6.push("<attribute name='fullname' />");
                                xml6.push("<attribute name='telephone1' />");
                                xml6.push("<attribute name='contactid' />");
                                xml6.push("<order attribute='fullname' descending='false' />");
                                xml6.push("<filter type='and'>");
                                xml6.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + rs5[0].attributes.bsd_account.guid + "' />");
                                xml6.push("<condition attribute='bsd_contacttype' operator='eq'  value='861450001' />");
                                xml6.push("<condition attribute='bsd_employee' operator='eq' uitype='bsd_employee' value='" + rs4[0].attributes.bsd_employee.guid + "' />");
                                xml6.push("</filter>");
                                xml6.push("</entity>");
                                xml6.push("</fetch>");
                                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='contactid'>  " +
                                   "<cell name='fullname'   " + "width='200' />  " +
                                   "<cell name='telephone1'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                                //Xrm.Page.getControl("bsd_outlet").addCustomView(viewId, entityName, viewDisplayName, xml6.join(""), layoutXml, true);
                                CrmFetchKit.Fetch(xml6.join(""), true).then(function (rs6) {
                                    if (rs6.length > 0) {
                                        Xrm.Page.getControl("bsd_outlet").addCustomView(viewId, entityName, viewDisplayName, xml6.join(""), layoutXml, true);
                                        // Xrm.Page.getControl("bsd_outlet").setDisabled(false);
                                    }
                                    if (rs6.length == 0) {
                                        // Xrm.Page.getControl("bsd_outlet").setDisabled(true);
                                    }
                                }, function (er) {
                                    console.log(er.message)
                                });
                            }
                        }, function (er) {
                            console.log(er.message)
                        });
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.Phong
//Description:get Frequency from outlet
function get_Frequencyfromoutlet() {
    debugger;
    var id = Xrm.Page.data.entity.getId();
    var outlet = Xrm.Page.getAttribute("bsd_outlet").getValue();
    var outlet2 = Xrm.Page.getAttribute("bsd_outlet2");
    var visitschedule = Xrm.Page.getAttribute("bsd_visitschedule");
    var mcp = Xrm.Page.getAttribute("bsd_mcp").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xmloutlet_no = [];
    if (outlet != null && mcp != null) {
        setNull(["bsd_numericalorder", "bsd_startdate", "bsd_enddate"]);
        setvalue(["bsd_mondayvaluee", "bsd_tuesdayvaluee", "bsd_wednesdayvaluee", "bsd_thursdayvaluee", "bsd_fridayvaluee", "bsd_saturdayvaluee", "bsd_sundayvaluee", "bsd_countnew"], 0);
        fetch(xml1, "bsd_mcpdetail", ["bsd_mcpdetailid", "bsd_name", "createdon"], "bsd_name", false, null
                      , ["bsd_mcp", "bsd_outlet", "statuscode"], ["eq", "eq", "eq"], [0, mcp[0].id, 1, outlet[0].id, 2, "100000000"]);
        CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                alert("This outlet have record in this MCP");
                setNull(["bsd_outlet", "bsd_visitschedule", "bsd_numberofvisit", "bsd_outlet", "bsd_numberofvisit", "bsd_numericalorder", "bsd_startdate", "bsd_enddate"]);
                setvalue(["bsd_mondayvaluee", "bsd_tuesdayvaluee", "bsd_wednesdayvaluee", "bsd_thursdayvaluee", "bsd_fridayvaluee", "bsd_saturdayvaluee", "bsd_sundayvaluee", "bsd_countnew"], 0);
            }
            else {
                if (outlet != null) {
                    //displayorhide_fieldnextweek();
                    outlet2.setValue([{
                        id: outlet[0].id,
                        name: outlet[0].name,
                        entityType: 'contact'
                    }]);
                    fetch(xml2, "bsd_mcpdetail", ["bsd_mcpdetailid", "bsd_name", "createdon", "bsd_countnew"], "bsd_name", false, null
                     , ["statecode", "bsd_outlet", "bsd_mcp", "statuscode"], ["eq", "eq", "eq", "eq"], [0, "0", 1, outlet[0].id, 2, mcp[0].id, 3, "100000000"]);
                    CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs) {
                        if (rs.length > 0) {
                            Xrm.Page.getAttribute("bsd_countnew").setValue(rs[0].attributes.bsd_countnew.value);
                        }
                        else {
                            setValue("bsd_countnew", 0);
                        }                 
                    });
                    fetch(xml, "contact", ["fullname", "telephone1", "contactid", "bsd_schedulevisit", "bsd_customercode"], "fullname", false, null
                               , ["contactid", "statecode"], ["eq", "eq"], [0, outlet[0].id, 1, "0"]);
                    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                        if (rs.length > 0) {
                            if (rs[0].attributes.bsd_customercode != null) {
                                Xrm.Page.getAttribute("bsd_outetcode").setValue(rs[0].attributes.bsd_customercode.value);
                            }

                            if (rs[0].attributes.bsd_schedulevisit != null) {
                                visitschedule.setValue([{
                                    id:'{' + rs[0].attributes.bsd_schedulevisit.guid.toUpperCase() + '}',
                                    name: rs[0].attributes.bsd_schedulevisit.name,
                                    entityType: rs[0].attributes.bsd_schedulevisit.logicalName
                                }]);
                                //displayorhide_fieldnextweek(rs[0].attributes.bsd_schedulevisit.guid);
                            }
                            Xrm.Page.getAttribute("bsd_name").setValue(outlet[0].name);
                            setvalue(["bsd_mondayvaluee", "bsd_tuesdayvaluee", "bsd_wednesdayvaluee", "bsd_thursdayvaluee", "bsd_fridayvaluee", "bsd_saturdayvaluee", "bsd_sundayvaluee"], 0);
                            setvalue(["bsd_monday", "bsd_tuesday", "bsd_wednesday", "bsd_thursday", "bsd_friday", "bsd_saturday", "bsd_sunday"], false);
                        }                 
                    });
                    //fetch(xmloutlet_no, "bsd_mcpdetail", ["bsd_mcpdetailid", "bsd_name", "createdon", "bsd_countnew", "bsd_numericalorder"], "bsd_name", false, null
                    //, ["statecode", "bsd_outlet", "bsd_mcp"], ["eq", "eq", "eq", "eq"], [0, "0", 1, outlet[0].id, 2, mcp[0].id]);
                    //CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                    //    if (rs.length > 0) {
                    //        if (rs[0].attributes.bsd_numericalorder != null) {
                    //           setValue("bsd_numericalorder", rs[0].attributes.bsd_numericalorder.value);
                    //        }
                    //        else {
                    //            setNull("bsd_numericalorder");
                    //        }
                    //    }
                    //});
                }
                else {
                    setvalue(["bsd_mondayvaluee", "bsd_tuesdayvaluee", "bsd_wednesdayvaluee", "bsd_thursdayvaluee", "bsd_fridayvaluee", "bsd_saturdayvaluee", "bsd_sundayvaluee"], 0);
                    setvalue(["bsd_monday", "bsd_tuesday", "bsd_wednesday", "bsd_thursday", "bsd_friday", "bsd_saturday", "bsd_sunday"], false);
                    Xrm.Page.getAttribute("bsd_visitschedule").setValue(null);
                }
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.Phong
//Description:an hien field next week
function displayorhide_fieldnextweek(schedulevisitid) {
    var xmlfrequency = [];
    fetch(xmlfrequency, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
                    , ["bsd_frequencyid", "statecode"], ["eq", "eq"], [0, schedulevisitid, 1, "0"]);
    CrmFetchKit.Fetch(xmlfrequency.join(""), false).then(function (rs) {
        if (rs.length > 0) {
            if (rs[0].attributes.bsd_numberofvisit.value == 0) {
                setDisabled("bsd_numberofvisit", false);

            }
            else {
                setDisabled("bsd_numberofvisit", true);
                setNull("bsd_numberofvisit");
            }
        }
    }, function (er) {
        console.log(er.message)
    });
}
//Author:Mr.Phong
//Description:an hien field next week onload
function displayorhide_fieldnextweekonload() {
    var frequency = getValue("bsd_visitschedule");
    var xmlfrequency = [];
    if (frequency != null) {
        fetch(xmlfrequency, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
                   , ["bsd_frequencyid", "statecode"], ["eq", "eq"], [0, frequency[0].id, 1, "0"]);
        CrmFetchKit.Fetch(xmlfrequency.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_numberofvisit.value == 0) {
                    setDisabled("bsd_numberofvisit", false);

                }
                else {
                    setDisabled("bsd_numberofvisit", true);
                    setNull("bsd_numberofvisit");
                }
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.Phong
function set_valuedate() {
    var mondayvalue = Xrm.Page.getAttribute("bsd_mondayvaluee").getValue();
    var tuesdayvalue = Xrm.Page.getAttribute("bsd_tuesdayvaluee").getValue();
    var wednesdayvalue = Xrm.Page.getAttribute("bsd_wednesdayvaluee").getValue();
    var thursdayvalue = Xrm.Page.getAttribute("bsd_thursdayvaluee").getValue();
    var fridayvalue = Xrm.Page.getAttribute("bsd_fridayvaluee").getValue();
    var saturdayvalue = Xrm.Page.getAttribute("bsd_saturdayvaluee").getValue();
    var sundayvalue = Xrm.Page.getAttribute("bsd_sundayvaluee").getValue();
    if (Xrm.Page.ui.getFormType() == 1 || mondayvalue == null && tuesdayvalue == null && wednesdayvalue == null && thursdayvalue == null && fridayvalue == null && saturdayvalue == null && sundayvalue == null) {
        Xrm.Page.getAttribute("bsd_mondayvaluee").setValue(0);
        Xrm.Page.getAttribute("bsd_tuesdayvaluee").setValue(0);
        Xrm.Page.getAttribute("bsd_wednesdayvaluee").setValue(0);
        Xrm.Page.getAttribute("bsd_thursdayvaluee").setValue(0);
        Xrm.Page.getAttribute("bsd_fridayvaluee").setValue(0);
        Xrm.Page.getAttribute("bsd_saturdayvaluee").setValue(0);
        Xrm.Page.getAttribute("bsd_sundayvaluee").setValue(0);
    }
}
//Author:Mr.Phong
//Description:not to save if checkbox is false
function check_nulltwooption() {
    var monday = Xrm.Page.getAttribute("bsd_monday").getValue();
    var tuesday = Xrm.Page.getAttribute("bsd_tuesday").getValue();
    var wednesday = Xrm.Page.getAttribute("bsd_wednesday").getValue();
    var thursday = Xrm.Page.getAttribute("bsd_thursday").getValue();
    var friday = Xrm.Page.getAttribute("bsd_friday").getValue();
    var saturday = Xrm.Page.getAttribute("bsd_saturday").getValue();
    var sunday = Xrm.Page.getAttribute("bsd_sunday").getValue();
    if (monday == false && tuesday == false && wednesday == false && thursday == false
        && friday == false && saturday == false && sunday == false) {
        Xrm.Page.getAttribute("bsd_outlet2").setValue(null);
        Xrm.Page.getAttribute("bsd_outlet").setValue(null);
        Xrm.Page.getAttribute("bsd_visitschedule").setValue(null);
        Xrm.Page.getControl("bsd_outlet2").setNotification("Can't Create!!!");
        //alert("Can't create");
    }
}
function check_test() {
    var id = Xrm.Page.data.entity.getId();
    var monday = Xrm.Page.getAttribute("bsd_monday").getValue();
    var tuesday = Xrm.Page.getAttribute("bsd_tuesday").getValue();
    var wednesday = Xrm.Page.getAttribute("bsd_wednesday").getValue();
    var thursday = Xrm.Page.getAttribute("bsd_thursday").getValue();
    var friday = Xrm.Page.getAttribute("bsd_friday").getValue();
    var saturday = Xrm.Page.getAttribute("bsd_saturday").getValue();
    var sunday = Xrm.Page.getAttribute("bsd_sunday").getValue();
    var mondayvalue = Xrm.Page.getAttribute("bsd_mondayvaluee").getValue();
    var tuesdayvalue = Xrm.Page.getAttribute("bsd_tuesdayvaluee").getValue();
    var wednesdayvalue = Xrm.Page.getAttribute("bsd_wednesdayvaluee").getValue();
    var thursdayvalue = Xrm.Page.getAttribute("bsd_thursdayvaluee").getValue();
    var fridayvalue = Xrm.Page.getAttribute("bsd_fridayvaluee").getValue();
    var saturdayvalue = Xrm.Page.getAttribute("bsd_saturdayvaluee").getValue();
    var sundayvalue = Xrm.Page.getAttribute("bsd_sundayvaluee").getValue();
    var outlet = Xrm.Page.getAttribute("bsd_outlet").getValue();
    var MCP = Xrm.Page.getAttribute("bsd_mcp").getValue();
    var visitschedule = Xrm.Page.getAttribute("bsd_visitschedule").getValue();
    var xmlfrequency = [];
    var countnew;
    var xmlmcpdetail = [];
    var xml2 = [];
    var datamcpoutlet = [];
    var frequencyespecial;
    var kt = false;
    if (visitschedule != null) {
        fetch(xmlfrequency, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
                     , ["bsd_frequencyid", "statecode"], ["eq", "eq"], [0, visitschedule[0].id, 1, "0"]);
        CrmFetchKit.Fetch(xmlfrequency.join(""), false).then(function (rs1) {
            if (rs1.length > 0) {
                debugger;
                if (mondayvalue == 0) {
                    if (monday == true) {
                        setValue("bsd_mondayvaluee", 1);
                        countnew = getValue("bsd_countnew") + 1;
                        setValue("bsd_countnew", countnew);
                        if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                            frequencyespecial = 1;
                            if (getValue("bsd_countnew") > frequencyespecial) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_monday", false);
                                setValue("bsd_mondayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                        else {
                            if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_monday", false);
                                setValue("bsd_mondayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                    }
                }
                else {
                    if (monday == false) {
                        setValue("bsd_mondayvaluee", 0);
                        countnew = getValue("bsd_countnew") - 1;
                        setValue("bsd_countnew", countnew);
                    }
                }
                if (tuesdayvalue == 0) {
                    if (tuesday == true) {
                        setValue("bsd_tuesdayvaluee", 1);
                        countnew = getValue("bsd_countnew") + 1;
                        setValue("bsd_countnew", countnew);
                        if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                            frequencyespecial = 1;
                            if (getValue("bsd_countnew") > frequencyespecial) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_tuesday", false);
                                setValue("bsd_tuesdayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                        else {
                            if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_tuesday", false);
                                setValue("bsd_tuesdayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                    }
                }
                else {
                    if (tuesday == false) {
                        setValue("bsd_tuesdayvaluee", 0);
                        countnew = getValue("bsd_countnew") - 1;
                        setValue("bsd_countnew", countnew);
                    }
                }
                if (wednesdayvalue == 0) {
                    if (wednesday == true) {
                        setValue("bsd_wednesdayvaluee", 1);
                        countnew = getValue("bsd_countnew") + 1;
                        setValue("bsd_countnew", countnew);
                        if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                            frequencyespecial = 1;
                            if (getValue("bsd_countnew") > frequencyespecial) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_wednesday", false);
                                setValue("bsd_wednesdayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                        else {
                            if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_wednesday", false);
                                setValue("bsd_wednesdayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                    }
                }
                else {
                    if (wednesday == false) {
                        setValue("bsd_wednesdayvaluee", 0);
                        countnew = getValue("bsd_countnew") - 1;
                        setValue("bsd_countnew", countnew);
                    }
                }
                if (thursdayvalue == 0) {
                    if (thursday == true) {
                        setValue("bsd_thursdayvaluee", 1);
                        countnew = getValue("bsd_countnew") + 1;
                        setValue("bsd_countnew", countnew);
                        if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                            frequencyespecial = 1;
                            if (getValue("bsd_countnew") > frequencyespecial) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_thursday", false);
                                setValue("bsd_thursdayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                        else {
                            if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_thursday", false);
                                setValue("bsd_thursdayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                    }
                }
                else {
                    if (thursday == false) {
                        setValue("bsd_thursdayvaluee", 0);
                        countnew = getValue("bsd_countnew") - 1;
                        setValue("bsd_countnew", countnew);
                    }
                }
                if (fridayvalue == 0) {
                    if (friday == true) {
                        setValue("bsd_fridayvaluee", 1);
                        countnew = getValue("bsd_countnew") + 1;
                        setValue("bsd_countnew", countnew);
                        if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                            frequencyespecial = 1;
                            if (getValue("bsd_countnew") > frequencyespecial) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_friday", false);
                                setValue("bsd_fridayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                        else {
                            if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_friday", false);
                                setValue("bsd_fridayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                    }
                }
                else {
                    if (friday == false) {
                        setValue("bsd_fridayvaluee", 0);
                        countnew = getValue("bsd_countnew") - 1;
                        setValue("bsd_countnew", countnew);
                    }
                }
                if (saturdayvalue == 0) {
                    if (saturday == true) {
                        setValue("bsd_saturdayvaluee", 1);
                        countnew = getValue("bsd_countnew") + 1;
                        setValue("bsd_countnew", countnew);
                        if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                            frequencyespecial = 1;
                            if (getValue("bsd_countnew") > frequencyespecial) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_saturday", false);
                                setValue("bsd_saturdayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                        else {
                            if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_saturday", false);
                                setValue("bsd_saturdayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                    }
                }
                else {
                    if (saturday == false) {
                        setValue("bsd_saturdayvaluee", 0);
                        countnew = getValue("bsd_countnew") - 1;
                        setValue("bsd_countnew", countnew);
                    }
                }
                if (sundayvalue == 0) {
                    if (sunday == true) {
                        setValue("bsd_sundayvaluee", 1);
                        countnew = getValue("bsd_countnew") + 1;
                        setValue("bsd_countnew", countnew);
                        if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                            frequencyespecial = 1;
                            if (getValue("bsd_countnew") > frequencyespecial) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_sunday", false);
                                setValue("bsd_sundayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                        else {
                            if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                                var message = "Cant Check More!";
                                var type = "INFO"; //INFO, WARNING, ERROR
                                var id = "Info1"; //Notification Id
                                var time = 3000; //Display time in milliseconds
                                //Display the notification
                                Xrm.Page.ui.setFormNotification(message, type, id);

                                //Wait the designated time and then remove
                                setTimeout(
                                    function () {
                                        Xrm.Page.ui.clearFormNotification(id);
                                    },
                                    time
                                );
                                setValue("bsd_sunday", false);
                                setValue("bsd_sundayvaluee", 0);
                                countnew = getValue("bsd_countnew") - 1;
                                setValue("bsd_countnew", countnew);
                            }
                        }
                    }
                }
                else {
                    if (sunday == false) {
                        setValue("bsd_sundayvaluee", 0);
                        countnew = getValue("bsd_countnew") - 1;
                        setValue("bsd_countnew", countnew);
                    }
                }
                //fetch(xmlmcpdetail, "bsd_mcpdetail", ["bsd_mcpdetailid", "bsd_name", "createdon", "bsd_wednesday", "bsd_tuesday", "bsd_thursday", "bsd_sunday"
                //                     , "bsd_saturday", "bsd_monday", "bsd_friday"], "bsd_name", false, null
                //         , ["bsd_outlet", "statecode", "bsd_status"], ["eq", "eq", "eq"], [0, outlet[0].id, 1, "0", 2, "1"]);
                //CrmFetchKit.Fetch(xmlmcpdetail.join(""), false).then(function (rs) {
                //    //neu cua hang nay da co lich vieng tham roi
                //    if (rs.length > 0) {
                //        for (var j = 0; j < rs.length; j++) {
                //            if (rs[j].attributes.bsd_wednesday.value == true) {
                //                datamcpoutlet.push("3");
                //            }
                //            if (rs[j].attributes.bsd_tuesday.value == true) {
                //                datamcpoutlet.push("2");
                //            }
                //            if (rs[j].attributes.bsd_sunday.value == true) {
                //                datamcpoutlet.push("7");
                //            }
                //            if (rs[j].attributes.bsd_saturday.value == true) {
                //                datamcpoutlet.push("6");
                //            }
                //            if (rs[j].attributes.bsd_monday.value == true) {
                //                datamcpoutlet.push("1");
                //            }
                //            if (rs[j].attributes.bsd_friday.value == true) {
                //                datamcpoutlet.push("5");
                //            }
                //            if (rs[j].attributes.bsd_thursday.value == true) {
                //                datamcpoutlet.push("4");
                //            }
                //        }
                //        if (mondayvalue == 0 && monday == true) {
                //            setValue("bsd_mondaycheck", 1);
                //            for (var j = 0; j < datamcpoutlet.length; j++) {
                //                if (Xrm.Page.getAttribute("bsd_mondaycheck").getValue() == datamcpoutlet[j]) {
                //                    kt = true;
                //                    break;
                //                }
                //            }
                //            if (kt) {
                //                setvalue(["bsd_mondaycheck", "bsd_mondayvaluee"], 0);
                //                setValue("bsd_monday", false);
                //                alert("Monday is checked");
                //            }
                //            else {
                //                setValue("bsd_mondayvaluee", 1);
                //                countnew = getValue("bsd_countnew") + 1;
                //                setValue("bsd_countnew", countnew);
                //                if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                //                    frequencyespecial = 1;
                //                    if (getValue("bsd_countnew") > frequencyespecial) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_monday", false);
                //                        setValue("bsd_mondayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //                else {
                //                    if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_monday", false);
                //                        setValue("bsd_mondayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //            }

                //        }
                //        else if (mondayvalue != 0 && monday == false) {
                //            setValue("bsd_mondayvaluee", 0);
                //            countnew = getValue("bsd_countnew") - 1;
                //            setValue("bsd_countnew", countnew);

                //        }
                //        else if (tuesdayvalue == 0 && tuesday == true) {
                //            setValue("bsd_tuesdaycheck", 2);
                //            for (var j = 0; j < datamcpoutlet.length; j++) {
                //                if (Xrm.Page.getAttribute("bsd_tuesdaycheck").getValue() == datamcpoutlet[j]) {
                //                    kt = true;
                //                    break;
                //                }
                //            }
                //            if (kt) {
                //                setvalue(["bsd_tuesdaycheck", "bsd_tuesdayvaluee"], 0);
                //                setValue("bsd_tuesday", false);
                //                alert("Tuesday is checked");

                //            }
                //            else {
                //                setValue("bsd_tuesdayvaluee", 1);
                //                countnew = getValue("bsd_countnew") + 1;
                //                setValue("bsd_countnew", countnew);
                //                if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                //                    frequencyespecial = 1;
                //                    if (getValue("bsd_countnew") > frequencyespecial) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_tuesday", false);
                //                        setValue("bsd_tuesdayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //                else {
                //                    if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_tuesday", false);
                //                        setValue("bsd_tuesdayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //            }
                //        }
                //        else if (tuesdayvalue != 0 && tuesday == false) {
                //            setValue("bsd_tuesdayvaluee", 0);
                //            countnew = getValue("bsd_countnew") - 1;
                //            setValue("bsd_countnew", countnew);
                //        }
                //        else if (wednesdayvalue == 0 && wednesday == true) {
                //            setValue("bsd_wednesdaycheck", 3);
                //            for (var j = 0; j < datamcpoutlet.length; j++) {
                //                if (Xrm.Page.getAttribute("bsd_wednesdaycheck").getValue() == datamcpoutlet[j]) {
                //                    kt = true;
                //                    break;
                //                }
                //            }
                //            if (kt) {
                //                setvalue(["bsd_wednesdaycheck", "bsd_wednesdayvaluee"], 0);
                //                setValue("bsd_wednesday", false);
                //                alert("wednesday is checked");
                //            }
                //            else {
                //                setValue("bsd_wednesdayvaluee", 1);
                //                countnew = getValue("bsd_countnew") + 1;
                //                setValue("bsd_countnew", countnew);
                //                if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                //                    frequencyespecial = 1;
                //                    if (getValue("bsd_countnew") > frequencyespecial) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_wednesday", false);
                //                        setValue("bsd_wednesdayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //                else {
                //                    if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_wednesday", false);
                //                        setValue("bsd_wednesdayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //            }
                //        }
                //        else if (wednesdayvalue != 0 && wednesday == false) {
                //            setValue("bsd_wednesdayvaluee", 0);
                //            countnew = getValue("bsd_countnew") - 1;
                //            setValue("bsd_countnew", countnew);
                //        }
                //        else if (thursdayvalue == 0 && thursday == true) {
                //            setValue("bsd_thursdaycheck", 4);
                //            for (var j = 0; j < datamcpoutlet.length; j++) {
                //                if (Xrm.Page.getAttribute("bsd_thursdaycheck").getValue() == datamcpoutlet[j]) {
                //                    kt = true;
                //                    break;
                //                }
                //            }
                //            if (kt) {
                //                setvalue(["bsd_thursdaycheck", "bsd_thursdayvaluee"], 0);
                //                setValue("bsd_thursday", false);
                //                alert("Thursday is checked");

                //            }
                //            else {
                //                setValue("bsd_thursdayvaluee", 1);
                //                countnew = getValue("bsd_countnew") + 1;
                //                setValue("bsd_countnew", countnew);
                //                if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                //                    frequencyespecial = 1;
                //                    if (getValue("bsd_countnew") > frequencyespecial) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_thursday", false);
                //                        setValue("bsd_thursdayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //                else {
                //                    if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_thursday", false);
                //                        setValue("bsd_thursdayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //            }
                //        }
                //        else if (thursdayvalue != 0 && thursday == false) {
                //            setValue("bsd_thursdayvaluee", 0);
                //            countnew = getValue("bsd_countnew") - 1;
                //            setValue("bsd_countnew", countnew);
                //        }
                //        else if (fridayvalue == 0 && friday == true) {
                //            setValue("bsd_fridaycheck", 5);
                //            Xrm.Page.getAttribute("bsd_fridaycheck").setValue(5);
                //            for (var j = 0; j < datamcpoutlet.length; j++) {
                //                if (Xrm.Page.getAttribute("bsd_fridaycheck").getValue() == datamcpoutlet[j]) {
                //                    kt = true;
                //                    break;
                //                }
                //            }
                //            if (kt) {
                //                setvalue(["bsd_fridaycheck", "bsd_fridayvaluee"], 0);
                //                setValue("bsd_friday", false);
                //                alert("Friday is checked");
                //            }
                //            else {
                //                setValue("bsd_fridayvaluee", 1);
                //                countnew = getValue("bsd_countnew") + 1;
                //                setValue("bsd_countnew", countnew);
                //                if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                //                    frequencyespecial = 1;
                //                    if (getValue("bsd_countnew") > frequencyespecial) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_friday", false);
                //                        setValue("bsd_fridayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //                else {
                //                    if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_friday", false);
                //                        setValue("bsd_fridayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }

                //            }
                //        }
                //        else if (fridayvalue != 0 && friday == false) {
                //            setValue("bsd_fridayvaluee", 0);
                //            countnew = getValue("bsd_countnew") - 1;
                //            setValue("bsd_countnew", countnew);
                //        }
                //        else if (saturdayvalue == 0 && saturday == true) {
                //            setValue("bsd_saturdaycheck", 6);
                //            for (var j = 0; j < datamcpoutlet.length; j++) {
                //                if (Xrm.Page.getAttribute("bsd_saturdaycheck").getValue() == datamcpoutlet[j]) {
                //                    kt = true;
                //                    break;
                //                }
                //            }
                //            if (kt) {
                //                setvalue(["bsd_saturdaycheck", "bsd_saturdayvaluee"], 0);
                //                setValue("bsd_saturday", false);
                //                alert("Saturday is checked");
                //            }
                //            else {
                //                setValue("bsd_saturdayvaluee", 1);
                //                countnew = getValue("bsd_countnew") + 1;
                //                setValue("bsd_countnew", countnew);
                //                if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                //                    frequencyespecial = 1;
                //                    if (getValue("bsd_countnew") > frequencyespecial) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_saturday", false);
                //                        setValue("bsd_saturdayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //                else {
                //                    if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_saturday", false);
                //                        setValue("bsd_saturdayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //            }
                //        }
                //        else if (saturdayvalue != 0 && saturday == false) {
                //            setValue("bsd_saturdayvaluee", 0);
                //            countnew = getValue("bsd_countnew") - 1;
                //            setValue("bsd_countnew", countnew);
                //        }
                //        else if (sundayvalue == 0 && sunday == true) {
                //            setValue("bsd_sundaycheck", 7);
                //            for (var j = 0; j < datamcpoutlet.length; j++) {
                //                if (Xrm.Page.getAttribute("bsd_sundaycheck").getValue() == datamcpoutlet[j]) {
                //                    kt = true;
                //                    break;
                //                }
                //            }
                //            if (kt) {
                //                setvalue(["bsd_sundaycheck", "bsd_sundayvaluee"], 0);
                //                setValue("bsd_sunday", false);
                //                alert("Sunday is checked");
                //            }
                //            else {
                //                setValue("bsd_sundayvaluee", 1);
                //                countnew = getValue("bsd_countnew") + 1;
                //                setValue("bsd_countnew", countnew);
                //                if (rs1[0].attributes.bsd_numberofvisit.value == 0) {
                //                    frequencyespecial = 1;
                //                    if (getValue("bsd_countnew") > frequencyespecial) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_sunday", false);
                //                        setValue("bsd_sundayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //                else {
                //                    if (getValue("bsd_countnew") > rs1[0].attributes.bsd_numberofvisit.value) {
                //                        var message = "Cant Check More!";
                //                        var type = "INFO"; //INFO, WARNING, ERROR
                //                        var id = "Info1"; //Notification Id
                //                        var time = 3000; //Display time in milliseconds
                //                        //Display the notification
                //                        Xrm.Page.ui.setFormNotification(message, type, id);

                //                        //Wait the designated time and then remove
                //                        setTimeout(
                //                            function () {
                //                                Xrm.Page.ui.clearFormNotification(id);
                //                            },
                //                            time
                //                        );
                //                        setValue("bsd_sunday", false);
                //                        setValue("bsd_sundayvaluee", 0);
                //                        countnew = getValue("bsd_countnew") - 1;
                //                        setValue("bsd_countnew", countnew);
                //                    }
                //                }
                //            }
                //        }
                //        else if (sundayvalue != 0 && sunday == false) {
                //            setValue("bsd_sundayvaluee", 0);
                //            countnew = getValue("bsd_countnew") - 1;
                //            setValue("bsd_countnew", countnew);
                //        }
                //    }
                //        //neu cua hang chua co lich vieng tham
                //    else {                   
                //    }
                //}, function (er) {
                //    console.log(er.message)
                //});
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.Phong
//Description:kiem tra tan suat khi thay doi frequency
function check_frequencychange() {
    if (Xrm.Page.ui.getFormType() == 2) {
        var id = Xrm.Page.data.entity.getId();
        var monday = Xrm.Page.getAttribute("bsd_monday").getValue();
        var tuesday = Xrm.Page.getAttribute("bsd_tuesday").getValue();
        var wednesday = Xrm.Page.getAttribute("bsd_wednesday").getValue();
        var thursday = Xrm.Page.getAttribute("bsd_thursday").getValue();
        var friday = Xrm.Page.getAttribute("bsd_friday").getValue();
        var saturday = Xrm.Page.getAttribute("bsd_saturday").getValue();
        var sunday = Xrm.Page.getAttribute("bsd_sunday").getValue();
        var outlet = Xrm.Page.getAttribute("bsd_outlet").getValue();
        var visitschedule = Xrm.Page.getAttribute("bsd_visitschedule").getValue();
        var xml = [];
        var xml1 = [];
        var xml2 = [];
        var a = [];
        if (visitschedule != null) {
            fetch(xml, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_count"], "bsd_name", false, null
                         , ["bsd_frequencyid", "statecode"], ["eq", "eq"], [0, visitschedule[0].id, 1, "0"]);
            CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
                if (rs.length > 0) {
                    //dung cau fetch de bo~ cai id do ra
                    fetch(xml1, "bsd_mcpdetail", ["bsd_mcpdetailid", "bsd_name", "createdon", "bsd_wednesday", "bsd_tuesday", "bsd_thursday", "bsd_sunday"
                                       , "bsd_saturday", "bsd_monday", "bsd_friday"], "bsd_name", false, null
                           , ["bsd_mcpdetailid", "statecode", "bsd_outlet"], ["ne", "eq", "eq"], [0, id, 1, "0", 2, outlet[0].id]);
                    CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
                        if (rs1.length > 0) {
                            fetch(xml2, "bsd_mcpdetail", ["bsd_mcpdetailid", "bsd_name", "createdon", "bsd_wednesday", "bsd_tuesday", "bsd_thursday", "bsd_sunday"
                                     , "bsd_saturday", "bsd_monday", "bsd_friday"], "bsd_name", false, null
                         , ["statecode", "bsd_mcpdetailid"], ["eq", "eq"], [0, "0", 1, Xrm.Page.data.entity.getId()]);
                            CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs2) {
                                if (rs2.length > 0) {
                                    if (monday == true) {
                                        a.push("1");
                                    }
                                    if (tuesday == true) {
                                        a.push("1");
                                    }
                                    if (wednesday == true) {
                                        a.push("1");
                                    }
                                    if (thursday == true) {
                                        a.push("1");
                                    }
                                    if (friday == true) {
                                        a.push("1");
                                    }
                                    if (saturday == true) {
                                        a.push("1");
                                    }
                                    if (sunday == true) {
                                        a.push("1");
                                    }
                                    for (var j = 0; j < rs1.length; j++) {
                                        if (rs1[j].attributes.bsd_wednesday.value == true) {
                                            a.push("3");
                                        }
                                        if (rs1[j].attributes.bsd_tuesday.value == true) {
                                            a.push("2");
                                        }
                                        if (rs1[j].attributes.bsd_sunday.value == true) {
                                            a.push("7");
                                        }
                                        if (rs1[j].attributes.bsd_saturday.value == true) {
                                            a.push("6");
                                        }
                                        if (rs1[j].attributes.bsd_monday.value == true) {
                                            a.push("1");
                                        }
                                        if (rs1[j].attributes.bsd_friday.value == true) {
                                            a.push("5");
                                        }
                                        if (rs1[j].attributes.bsd_thursday.value == true) {
                                            a.push("4");
                                        }
                                    }
                                    if (rs[0].attributes.bsd_count.value - a.length < 0) {
                                        //alert("This account has no pricelist.Please create an pricelist for this account!");
                                        var message = "tan suat da thay doi ban phai bo di" + " " + (rs[0].attributes.bsd_count.value - a.length) * (-1) + " " + "ngay";
                                        var type = "INFO"; //INFO, WARNING, ERROR
                                        var id = "Info1"; //Notification Id
                                        var time = 3000; //Display time in milliseconds
                                        //Display the notification
                                        Xrm.Page.ui.setFormNotification(message, type, id);

                                        //Wait the designated time and then remove
                                        setTimeout(
                                            function () {
                                                Xrm.Page.ui.clearFormNotification(id);
                                            },
                                            time
                                        );
                                    }
                                    if (rs[0].attributes.bsd_count.value - a.length > 0) {
                                        //alert("This account has no pricelist.Please create an pricelist for this account!");
                                        var message = "tan suat da thay doi ban phai them" + " " + (rs[0].attributes.bsd_count.value - a.length) + " " + "ngay";
                                        var type = "INFO"; //INFO, WARNING, ERROR
                                        var id = "Info1"; //Notification Id
                                        var time = 3000; //Display time in milliseconds
                                        //Display the notification
                                        Xrm.Page.ui.setFormNotification(message, type, id);

                                        //Wait the designated time and then remove
                                        setTimeout(
                                            function () {
                                                Xrm.Page.ui.clearFormNotification(id);
                                            },
                                            time
                                        );
                                    }
                                }
                            }, function (er) {
                                console.log(er.message)
                            });
                        }
                    }, function (er) {
                        console.log(er.message)
                    });
                }
            }, function (er) {
                console.log(er.message)
            });
        }
    }
}
//Description:gan gia tri cho 2 filed count new va count old
function setvalue_filedcount() {
    var data = [];
    data = getvalue(["bsd_countold", "bsd_countnew"]);
    if (data[0] == null && data[1] == null) {
        setvalue(["bsd_countold", "bsd_countnew"], 0);
    }

}
//Description:thay doi gia tri field khi doi frequency
function change_fieldfrequency() {
    var outlet = Xrm.Page.getAttribute("bsd_outlet").getValue();
    var id = Xrm.Page.data.entity.getId();
    var frequencynew = getValue("bsd_visitschedule");
    var frequency = Xrm.Page.getAttribute("bsd_visitschedule");
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var frequencyold;
    var frequencynew;
    if (frequency != null)
    {        
        setNull(["bsd_numericalorder", "bsd_startdate", "bsd_enddate"]);
        setvalue(["bsd_mondayvaluee", "bsd_tuesdayvaluee", "bsd_wednesdayvaluee", "bsd_thursdayvaluee", "bsd_fridayvaluee", "bsd_saturdayvaluee", "bsd_sundayvaluee", "bsd_countnew"], 0);
        setvalue(["bsd_monday", "bsd_tuesday", "bsd_wednesday", "bsd_thursday", "bsd_friday", "bsd_saturday", "bsd_sunday"], false);
    }
    //if (id != "" && outlet != null && frequency.getValue()!=null) {
    //    var frequencychild = frequency.getValue();
        //displayorhide_fieldnextweek(frequencychild[0].id);
        //xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        //xml.push("<entity name='bsd_mcpdetail'>");
        //xml.push("<attribute name='bsd_mcpdetailid' />");
        //xml.push("<attribute name='bsd_name'/>");
        //xml.push("<attribute name='createdon'/>");
        //xml.push("<attribute name='bsd_visitschedule'/>");
        //xml.push("<order attribute='bsd_name' descending='false' />");
        //xml.push("<filter type='and' >");
        //xml.push("<condition attribute='statecode' operator='eq' value='0'/>");
        //xml.push("<condition attribute='bsd_outlet' operator='eq' uitype='contact' value='" + outlet[0].id + "'/>");
        //xml.push("<condition attribute='bsd_mcpdetailid' operator='ne' uitype='bsd_mcpdetail' value='" + id + "'/>");
        //xml.push("</filter>");
        //xml.push("</entity>");
        //xml.push("</fetch>");
        //CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
        //    if (rs.length > 0) {
        //        fetch(xml1, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
        //                  , ["bsd_frequencyid"], ["eq"], [0, rs[0].attributes.bsd_visitschedule.guid, 1]);
        //        CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs) {
        //            if (rs.length > 0) {
        //                if (rs[0].attributes.bsd_numberofvisit.value == 0) {
        //                    frequencyold = 1;
        //                }
        //                else {
        //                    frequencyold = rs[0].attributes.bsd_numberofvisit.value;
        //                }
        //            }
        //        }, function (er) {
        //            console.log(er.message)
        //        });
        //        fetch(xml2, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
        //                  , ["bsd_frequencyid"], ["eq"], [0, frequencynew[0].id, 1]);
        //        CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs) {
        //            if (rs.length > 0) {
        //                if (rs[0].attributes.bsd_numberofvisit.value == 0) {
        //                    frequencynew = 1;
        //                }
        //                else {
        //                    frequencynew = rs[0].attributes.bsd_numberofvisit.value;
        //                }
        //                if (frequencyold > frequencynew) {
        //                    setvalue(["bsd_mondayvaluee", "bsd_tuesdayvaluee", "bsd_wednesdayvaluee", "bsd_thursdayvaluee", "bsd_fridayvaluee", "bsd_saturdayvaluee", "bsd_sundayvaluee", "bsd_countold", "bsd_countnew"], 0);
        //                    setvalue(["bsd_monday", "bsd_tuesday", "bsd_wednesday", "bsd_thursday", "bsd_friday", "bsd_saturday", "bsd_sunday"], false);
        //                }
        //            }
        //        }, function (er) {
        //            console.log(er.message)
        //        });
        //    }
        //    else {
        //        xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        //        xml3.push("<entity name='bsd_mcpdetail'>");
        //        xml3.push("<attribute name='bsd_mcpdetailid' />");
        //        xml3.push("<attribute name='bsd_name'/>");
        //        xml3.push("<attribute name='createdon'/>");
        //        xml3.push("<attribute name='bsd_visitschedule'/>");
        //        xml3.push("<order attribute='bsd_name' descending='false' />");
        //        xml3.push("<filter type='and' >");
        //        xml3.push("<condition attribute='statecode' operator='eq' value='0'/>");
        //        xml3.push("<condition attribute='bsd_outlet' operator='eq' uitype='contact' value='" + outlet[0].id + "'/>");
        //        xml3.push("<condition attribute='bsd_mcpdetailid' operator='eq' uitype='bsd_mcpdetail' value='" + id + "'/>");
        //        xml3.push("</filter>");
        //        xml3.push("</entity>");
        //        xml3.push("</fetch>");
        //        CrmFetchKit.Fetch(xml3.join(""), false).then(function (rs4) {
        //            if (rs4.length > 0) {
        //                fetch(xml4, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
        //                          , ["bsd_frequencyid"], ["eq"], [0, rs4[0].attributes.bsd_visitschedule.guid, 1]);
        //                CrmFetchKit.Fetch(xml4.join(""), false).then(function (rs) {
        //                    if (rs.length > 0) {
        //                        if (rs[0].attributes.bsd_numberofvisit.value == 0) {
        //                            frequencyold = 1;
        //                        }
        //                        else {
        //                            frequencyold = rs[0].attributes.bsd_numberofvisit.value;
        //                        }
        //                    }
        //                }, function (er) {
        //                    console.log(er.message)
        //                });
        //                fetch(xml5, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
        //                          , ["bsd_frequencyid"], ["eq"], [0, frequencynew[0].id, 1]);
        //                CrmFetchKit.Fetch(xml5.join(""), false).then(function (rs) {
        //                    if (rs.length > 0) {
        //                        if (rs[0].attributes.bsd_numberofvisit.value == 0) {
        //                            frequencynew = 1;
        //                        }
        //                        else {
        //                            frequencynew = rs[0].attributes.bsd_numberofvisit.value;
        //                        }
        //                        if (frequencyold > frequencynew) {
        //                            if (!confirm("All record wil be deleted!Do you want?")) {
        //                                frequency.setValue([{
        //                                    id: rs4[0].attributes.bsd_visitschedule.guid,
        //                                    name: rs4[0].attributes.bsd_visitschedule.name,
        //                                    entityType: 'bsd_frequency'
        //                                }]);
        //                                Xrm.Page.data.save();
        //                                eventArgs.preventDefault();
        //                            }
        //                            setvalue(["bsd_mondayvaluee", "bsd_tuesdayvaluee", "bsd_wednesdayvaluee", "bsd_thursdayvaluee", "bsd_fridayvaluee", "bsd_saturdayvaluee", "bsd_sundayvaluee", "bsd_countold", "bsd_countnew"], 0);
        //                            setvalue(["bsd_monday", "bsd_tuesday", "bsd_wednesday", "bsd_thursday", "bsd_friday", "bsd_saturday", "bsd_sunday"], false);
        //                        }
        //                    }
        //                }, function (er) {
        //                    console.log(er.message)
        //                });
        //            }
        //        }, function (er) {
        //            console.log(er.message)
        //        });
        //    }
        //}, function (er) {
        //    console.log(er.message)
        //});
    //}
    //if (id == "") {
    //    if (frequency.getValue()!=null) {
    //        var frequencychild = frequency.getValue();
    //        displayorhide_fieldnextweek(frequencychild[0].id);
    //    }       
    //}
}
function AutoSave(econtext) {
    //var eventArgs = econtext.getEventArgs();
    //var result = CheckChangeFrequency();
    //if (result == 1) {
    //    eventArgs.preventDefault();
    //}
    CheckSave(econtext);   
}
function CheckChangeFrequency() {
    //var flag = true;
    //debugger;
    //var outlet = Xrm.Page.getAttribute("bsd_outlet").getValue();
    //var id = Xrm.Page.data.entity.getId();
    //var eventArgs = econtext.getEventArgs();
    //var frequencynew = getValue("bsd_visitschedule");
    //var xml = [];
    //var xml1 = [];
    //var xml2 = [];
    //var frequencyold;
    //var frequencynew;
    //if (id != "" && outlet != null) {
    //    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    //    xml.push("<entity name='bsd_mcpdetail'>");
    //    xml.push("<attribute name='bsd_mcpdetailid' />");
    //    xml.push("<attribute name='bsd_name'/>");
    //    xml.push("<attribute name='createdon'/>");
    //    xml.push("<attribute name='bsd_visitschedule'/>");
    //    xml.push("<order attribute='bsd_name' descending='false' />");
    //    xml.push("<filter type='and' >");
    //    xml.push("<condition attribute='statecode' operator='eq' value='0'/>");
    //    xml.push("<condition attribute='bsd_outlet' operator='eq' uitype='contact' value='" + outlet[0].id + "'/>");
    //    xml.push("<condition attribute='bsd_mcpdetailid' operator='ne' uitype='bsd_mcpdetail' value='" + id + "'/>");
    //    xml.push("</filter>");
    //    xml.push("</entity>");
    //    xml.push("</fetch>");
    //    CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
    //        if (rs.length > 0) {
    //            fetch(xml1, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
    //                      , ["bsd_frequencyid"], ["eq"], [0, rs[0].attributes.bsd_visitschedule.guid, 1]);
    //            CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs) {
    //                if (rs.length > 0) {
    //                    if (rs[0].attributes.bsd_numberofvisit.value == 0) {
    //                        frequencyold = 1;
    //                    }
    //                    else {
    //                        frequencyold = rs[0].attributes.bsd_numberofvisit.value;
    //                    }
    //                }
    //            }, function (er) {
    //                console.log(er.message)
    //            });
    //            fetch(xml2, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
    //                      , ["bsd_frequencyid"], ["eq"], [0, frequencynew[0].id, 1]);
    //            CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs) {
    //                if (rs.length > 0) {
    //                    if (rs[0].attributes.bsd_numberofvisit.value == 0) {
    //                        frequencynew = 1;
    //                    }
    //                    else {
    //                        frequencynew = rs[0].attributes.bsd_numberofvisit.value;
    //                    }
    //                    if (frequencyold > frequencynew) {
    //                        if (!confirm("All record wil be deleted!Do you want?")) {
    //                            return false;
    //                        }
    //                    }
    //                }
    //            }, function (er) {
    //                console.log(er.message)
    //            });
    //        }
    //    }, function (er) {
    //        console.log(er.message)
    //    });
    //}
    var result = 0;
    var outlet = Xrm.Page.getAttribute("bsd_outlet").getValue();
    var id = Xrm.Page.data.entity.getId();
    var frequencynew = getValue("bsd_visitschedule");
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var frequencyold;
    var frequencynew;
    if (id != "" && outlet != null)
    {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_mcpdetail'>");
        xml.push("<attribute name='bsd_mcpdetailid' />");
        xml.push("<attribute name='bsd_name'/>");
        xml.push("<attribute name='createdon'/>");
        xml.push("<attribute name='bsd_visitschedule'/>");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='statecode' operator='eq' value='0'/>");
        xml.push("<condition attribute='bsd_outlet' operator='eq' uitype='contact' value='" + outlet[0].id + "'/>");
        xml.push("<condition attribute='bsd_mcpdetailid' operator='ne' uitype='bsd_mcpdetail' value='" + id + "'/>");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var rs1 = CrmFetchKit.FetchSync(xml.join(''));
        if (rs1.length>0) {
            fetch(xml1, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
                 , ["bsd_frequencyid"], ["eq"], [0, rs1[0].attributes.bsd_visitschedule.guid, 1]);
            var rs_xml1 = CrmFetchKit.FetchSync(xml1.join(''));
            if (rs_xml1.length > 0) {
                if (rs_xml1[0].attributes.bsd_numberofvisit.value == 0) {
                    frequencyold = 1;
                }
                else {
                    frequencyold = rs_xml1[0].attributes.bsd_numberofvisit.value;
                }
                fetch(xml2, "bsd_frequency", ["bsd_frequencyid", "bsd_name", "createdon", "bsd_numberofvisit"], "bsd_name", false, null
                      , ["bsd_frequencyid"], ["eq"], [0, frequencynew[0].id, 1]);
                var rs_xml2 = CrmFetchKit.FetchSync(xml2.join(''));
                if (rs_xml2.length > 0) {
                    if (rs_xml2[0].attributes.bsd_numberofvisit.value == 0) {
                        frequencynew = 1;
                    }
                    else {
                        frequencynew = rs_xml2[0].attributes.bsd_numberofvisit.value;
                    }
                    if (frequencyold > frequencynew) {
                        if (!confirm("All record wil be deleted!Do you want?")) {
                            result = 1;
                        }
                    }
                }
            }
        }      
    }
    return result;
}
//Description:set value start date filed
function set_startdate() {
    var currentDateTime = getCurrentDateTime();
    var startdate = getValue("bsd_startdate")
    if (startdate == null) {
        Xrm.Page.getAttribute("bsd_startdate").setValue(new Date(currentDateTime).addDays(1));
    }
}
function check_date() {
    var fromdate = getValue("bsd_startdate");
    var currentDateTime = getCurrentDateTime();
    if (fromdate != null) {
        if (fromdate < currentDateTime) {
            var message = "The From Date cannot occur after current date";
            var type = "INFO"; //INFO, WARNING, ERROR
            var id = "Info1"; //Notification Id
            var time = 3000; //Display time in milliseconds
            //Display the notification
            Xrm.Page.ui.setFormNotification(message, type, id);

            //Wait the designated time and then remove
            setTimeout(
                function () {
                    Xrm.Page.ui.clearFormNotification(id);
                },
                time
            );
            setValue("bsd_startdate", currentDateTime);
        }
    }
}
function getCurrentDateTime() {
    var d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
}
//Description:kiem tra strart date
function check_startdate() {
    debugger;
    var data = [];
    var startdate = getValue("bsd_startdate");
    var currentDateTime = getCurrentDateTime();
    data = getvalue(["bsd_mcp", "bsd_outlet"]);
    var xml = [];
    var currentDateTime = getCurrentDateTime();
 
    if (startdate != null) {
        if (startdate<currentDateTime) {
            setValue("bsd_startdate", new Date(currentDateTime).addDays(1));
        }
      
    }
    else {
        setNull("bsd_startdate");
    }
    //if (data[0] != null && data[2] != null) {
    //    fetch(xml, "bsd_mcpdetail", ["bsd_mcpdetailid", "bsd_name", "createdon", "bsd_startdate"], ["bsd_startdate"], true, null
    //               , ["bsd_mcp", "bsd_outlet", "statecode", "statuscode"], ["eq", "eq", "eq", "eq"], [0, data[0], 1, data[2], 2, "0", 3, "1"]);
    //    CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
    //        if (rs.length > 0) {
    //            if (startdate < rs[0].attributes.bsd_startdate.value) {
    //                Xrm.Page.getControl("bsd_startdate").setNotification("Invalid stratdate!");
    //            }
    //            else {
    //                if (startdate < currentDateTime) {
    //                    Xrm.Page.getControl("bsd_startdate").setNotification("The From Date cannot occur after current date!");
    //                }
    //                else {
    //                    Xrm.Page.getControl("bsd_startdate").clearNotification();
    //                }
    //            }
    //        }
    //        else {
    //            if (startdate < currentDateTime) {
    //                Xrm.Page.getControl("bsd_startdate").setNotification("The From Date cannot occur after current date!");
    //            }
    //            else {
    //                Xrm.Page.getControl("bsd_startdate").clearNotification();
    //            }
    //        }
    //    }, function (er) {
    //        console.log(er.message)
    //    });
    //}
    //else {
    //    setNull("bsd_startdate");
    //}
}
//Description:kiem tra end date so vs start date
function end_date() {
    var fromdate = getValue("bsd_startdate");
    var todate = getValue("bsd_enddate");
    var currentDateTime = getCurrentDateTime();
    var statuscode = getValue("statuscode")
    if (fromdate != null) {
        if (statuscode == 100000000)//open
        {
            if (todate < fromdate) {
                setNotification("bsd_enddate", "The To Date Date cannot occur after From Date");
            }
            else if (todate > fromdate) {
                Xrm.Page.getControl("bsd_enddate").clearNotification();           
            }
        }
        if (statuscode == 1)//active
        {
            if (todate < fromdate) {
                setNotification("bsd_enddate", "The To Date Date cannot occur after From Date");
            }
            else if (todate > fromdate) {
                Xrm.Page.getControl("bsd_enddate").clearNotification();
                if (todate < currentDateTime) {
                    setValue("bsd_startdate", new Date(currentDateTime).addDays(1));
                }
            }
        }
       
    }
}
function getCurrentDateTime() {
    var d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
}
//End Phong

function clear_outlet() {
    Xrm.Page.getControl("bsd_outlet").addPreSearch(presearch_outlet);
}
function presearch_outlet() {
    Xrm.Page.getControl("bsd_outlet").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

//Diệm
function Active_Change() {
    if (getValue("statuscode") == 1) {
        DisabledForm();
        setDisabled(["bsd_enddate", "bsd_description"], false);
    }
}
function CheckSave(econtext) {
    var eventArgs = econtext.getEventArgs();
    var outlet = getValue("bsd_outlet");
    var MCP = getValue("bsd_mcp");
    if (outlet != null) {
        var fetchxml_Active = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="bsd_mcpdetail">',
                        '    <attribute name="bsd_mcpdetailid" />',
                        '    <attribute name="bsd_name" />',
                        '    <attribute name="createdon" />',
                        '    <order attribute="bsd_name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="bsd_outlet" operator="eq" uitype="contact" value="' + outlet[0].id + '" />',
                        '      <condition attribute="bsd_mcp" operator="eq" uitype="bsd_mcp" value="' + MCP[0].id + '" />',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '      <condition attribute="statuscode" operator="eq" value="1" />',
                         '   </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        var rsActive = CrmFetchKit.FetchSync(fetchxml_Active);

        if (rsActive.length > 0) {

            if (!confirm("WARNING!!! This outlet " + outlet[0].name + " had a actived. Do you want to create a new?")) {
                eventArgs.preventDefault();
            }
            else /*Kiem tra xem outlet nay co */ {
                var fetchxml_Open = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '  <entity name="bsd_mcpdetail">',
                      '    <attribute name="bsd_mcpdetailid" />',
                      '    <attribute name="bsd_name" />',
                      '    <attribute name="createdon" />',
                      '    <order attribute="bsd_name" descending="false" />',
                      '    <filter type="and">',
                      '      <condition attribute="bsd_outlet" operator="eq" uitype="contact" value="' + outlet[0].id + '" />',
                      '      <condition attribute="bsd_mcp" operator="eq" uitype="bsd_mcp" value="' + MCP[0].id + '" />',
                      '      <condition attribute="statecode" operator="eq" value="0" />',
                      '      <condition attribute="statuscode" operator="eq" value="100000000" />',
                       '   </filter>',
                      '  </entity>',
                      '</fetch>'].join('');
                var rsOpen = CrmFetchKit.FetchSync(fetchxml_Open);
                if (rsOpen.length > 0) {
                    m_alert("Unable to create a new record. Data existed for this customer."
                           + "This " + MCP[0].id + " had a record. Please!!! Repair it.");
                    //  m_alert("Account không co´ price list ho?c price list dã h?t h?n");

                    setNotification("bsd_mcp", "Please!!! Repair it");
                } else {
                    clearNotification("bsd_mcp");
                }
            }
        }
    }
    else {
        var fetchxml_Open = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                       '  <entity name="bsd_mcpdetail">',
                       '    <attribute name="bsd_mcpdetailid" />',
                       '    <attribute name="bsd_name" />',
                       '    <attribute name="createdon" />',
                       '    <order attribute="bsd_name" descending="false" />',
                       '    <filter type="and">',
                       '      <condition attribute="bsd_outlet" operator="eq" uitype="contact" value="' + outlet[0].id + '" />',
                       '      <condition attribute="bsd_mcp" operator="eq" uitype="bsd_mcp" value="' + MCP[0].id + '" />',
                       '      <condition attribute="statecode" operator="eq" value="0" />',
                       '      <condition attribute="statuscode" operator="eq" value="100000000" />',
                        '   </filter>',
                       '  </entity>',
                       '</fetch>'].join('');
        var rsOpen = CrmFetchKit.FetchSync(fetchxml_Open);
        if (rsOpen.length > 0) {
            m_alert("Unable to create a new record. Data existed for this customer."
                   + "This " + MCP[0].id + " had a record. Please!!! Repair it.");

            setNotification("bsd_mcp", "Please!!! Repair it");
        } else {
            clearNotification("bsd_mcp");
        }
    }
}
//End Diệm