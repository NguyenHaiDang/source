//Author:Mr.Đăng
//Description: Load Warehouse theo Account
function LK_Warehouse(reset) {
    if (reset != false) setNull("bsd_warehouse");
    var distributor = getValue("bsd_account");
    getControl("bsd_warehouse").removePreSearch(presearch_Warehouse);
    if (distributor != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_warehousedms">',
                        '<attribute name="bsd_warehousedmsid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + distributor[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml)
        console.log(rs);
        if (rs.length > 0) {
            var first = rs[0];
            if (getValue("bsd_warehouse") == null) {
                setValue("bsd_warehouse", [{
                    id: first.Id,
                    name: first.attributes.bsd_name.value,
                    entityType: first.logicalName
                }]
               );
            }
        }
        LookUp_After_Load("bsd_warehouse", "bsd_warehousedms", "bsd_name", "Warehouse DMS", fetchxml);
    }
    else if (reset != false) clear_Warehouse();
}
function clear_Warehouse() {
    getControl("bsd_warehouse").addPreSearch(presearch_Warehouse);
}
function presearch_Warehouse() {
    getControl("bsd_warehouse").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}