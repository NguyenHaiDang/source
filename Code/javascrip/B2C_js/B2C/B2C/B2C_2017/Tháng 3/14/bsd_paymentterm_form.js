//Author:Mr.Đăng
//Description:Check Code
function Check_Code() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var termofpayment = getValue("bsd_termofpayment");
    var id = getId();
    if (termofpayment != null) {
        if (mikExp.test(termofpayment)) {
            setNotification("bsd_termofpayment", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_paymentterm">',
                            '<attribute name="bsd_paymenttermid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_termofpayment" operator="eq" value="' + termofpayment + '" />',
                            '<condition attribute="bsd_paymenttermid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join('');
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_termofpayment", "Đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_termofpayment");
                setValue("bsd_name", termofpayment);
            }
        }
    }
    else {
        setNull("bsd_name");
    }
}