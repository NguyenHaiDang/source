var formtype = 1;
//Description:Load Form
function init() {
    //Diệm: 15/3/2017
    setVisible(["bsd_quoteidnpp"], false);
    //end Diệm
    Load_UseAccountId();
    var statecode = Xrm.Page.getAttribute("statecode").getValue();
    //1 la active
    if (statecode != 1 && statecode != 3 && statecode != 2) {
        //set_currentdate();
        set_addressonload();
        setshiptoaddressonload();
        //hide_button();
        //switchProcess();       
        //showhide_fiedshiptofreightterms();
        //changestatuswhenstagefinalonload();
        set_DisableDeliveryPort();
        //showBusinessProcessFlowonload();                        

    }
    // Trung.
    if (formType() == 1) {
        check_enable_shipping();
        Load_FromDate_Auto();
        load_unitdefault(true);
        load_currencydefault(true);
        clear_LoadAddressPort();
        clear_priceleve();
        set_currentdate();
        LK_PotentialCustomer_Change(true);
        setDisabled(["bsd_address", "bsd_addressinvoiceaccount", "bsd_shiptoaddress"], true);
    } else {
        load_unitdefault(false);
        load_currencydefault(false);
        check_enable_shipping(false);
        porteroption_change(false);
        LK_PotentialCustomer_Change(false);
        Load_MaNhaPhanPhoi();
    }
    // End Trung
    filter_gridQuotebyID();
    set_customer();
}
//Diệm: 14/3/2016: load account theo user.
var accountid = null;
function Load_UseAccountId() {
    if (getUserId() != null && getUserId() != "") {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '  <entity name="systemuser">',
                   '    <attribute name="fullname" />',
                   '    <attribute name="businessunitid" />',
                   '    <attribute name="title" />',
                   '    <attribute name="address1_telephone1" />',
                   '    <attribute name="positionid" />',
                   '    <attribute name="systemuserid" />',
                   '    <attribute name="bsd_account" />',
                   '    <order attribute="fullname" descending="false" />',
                   '    <filter type="and">',
                   '      <condition attribute="isdisabled" operator="eq" value="0" />',
                   '      <condition attribute="systemuserid" operator="eq" uitype="systemuser" value="' + getUserId() + '" />',
                   '      <condition attribute="bsd_account" operator="not-null" />',
                   '    </filter>',
                   '  </entity>',
                   '</fetch>'].join('');
        var rsSystemUser = CrmFetchKit.FetchSync(fetchxml);
        if (rsSystemUser.length > 0) {
            accountid = rsSystemUser[0].attributes.bsd_account.guid;
            var fetchAccount = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                '  <entity name="account">',
                                '    <attribute name="name" />',
                                '    <attribute name="primarycontactid" />',
                                '    <attribute name="telephone1" />',
                                '    <attribute name="accountid" />',
                                '    <attribute name="bsd_accounttype" />',
                                '    <order attribute="name" descending="false" />',
                                '    <filter type="and">',
                                '      <condition attribute="accountid" operator="eq" uitype="account" value="' + accountid + '" />',
                                '      <condition attribute="statecode" operator="eq" value="0" />',
                                '      <condition attribute="bsd_accounttype" operator="not-null" />',
                                '    </filter>',
                                '  </entity>',
                                '</fetch>'].join('');
            var rsAccount = CrmFetchKit.FetchSync(fetchAccount);
            if (rsAccount.length > 0) {
                console.log(rsAccount);
                if (rsAccount[0].attributes.bsd_accounttype.value == 100000000) {
                    window.parent.document.getElementById("header_crmFormSelector").innerHTML = "Request Order Distributor";
                    if (getValue("customerid") == null) {
                        setValue("customerid", [{
                            id: rsAccount[0].Id,
                            name: rsAccount[0].attributes.name.value,
                            entityType: "account"
                        }]);
                    }
                    setValue("bsd_quotationtype", 100000000);
                    setDisabled(["customerid","bsd_quotationtype"], true);
                    setVisible(["quotenumber"], false);
                    setVisible(["bsd_quoteidnpp"], true);
                    Customerid_function();
                }
            }
        }
    }
}
// kiem tra co phải là nha phân phối hay không.
function Load_MaNhaPhanPhoi() {
    var customer = getValue("customerid");
    if (customer != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="account">',
                        '    <attribute name="name" />',
                        '    <attribute name="primarycontactid" />',
                        '    <attribute name="telephone1" />',
                        '    <attribute name="accountid" />',
                        '    <attribute name="bsd_accounttype" />',
                        '    <order attribute="name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                        '      <condition attribute="bsd_accounttype" operator="not-null" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            if (rs[0].attributes.bsd_accounttype.value == 100000000) {
                setVisible("quotenumber", false);
                setVisible("bsd_quoteidnpp", true);
            } else {
                setVisible("quotenumber", true);
                setVisible("bsd_quoteidnpp", false);
            }
        }
    }
}

function Customerid_function() {
    get_customerinformation();
    get_contact();
    LK_PotentialCustomer_Change();
    check_enable_shipping();
    load_sale_tax_group();
    Load_MaNhaPhanPhoi();
}
// End Diệm
//huy- 26/2/2017 2h41
//code from B2B -> B2C
function check_enable_shipping(reset) {
    // call when customername or warehouse or address change.
    var customername = getValue("customerid");
    var warehousefrom = getValue("bsd_warehouseto");
    var warehouseaddress = getValue("bsd_warehouseaddress");
    var shiptoaddress = getValue("bsd_shiptoaddress");
    var effectivefrom = getValue("effectivefrom");
    var effectiveto = getValue("effectiveto");
    if (customername != null && warehousefrom != null && shiptoaddress != null && warehouseaddress != null && effectivefrom != null && effectiveto != null) {
        setDisabled("bsd_transportation", false);
    } else {
        setValue("bsd_transportation", false);
        setDisabled("bsd_transportation", true);
    }
    shipping_change(reset);
}
function shipping_change(reset) {
    clearNotification("bsd_shippingpricelistname");
    if (reset != false) {
        setNull(["bsd_truckload", "bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
    }

    var bsd_transportation = getValue("bsd_transportation");
    if (bsd_transportation) { // có ship
        setRequired(["bsd_partnerscompany", "bsd_shiptoaddress"], "required");
        setVisible(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], true);
        setRequired(["bsd_shippingdeliverymethod", "bsd_shippingpricelistname", "bsd_priceoftransportationn"], "required");
        shipping_deliverymethod_change(reset);
    } else {
        setRequired(["bsd_partnerscompany", "bsd_shiptoaddress"], "none");
        setVisible(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], false);
        setRequired(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], "none");

        if (reset != false) {
            setValue("bsd_shippingporter", false);
            porteroption_change(reset);
        }
    }
}
function requestporter_change(reset) {
    shipping_change(reset);
}
function shipping_deliverymethod_change(reset) {

    if (reset != false) setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"]);
    var method;
    if (getValue("bsd_shippingdeliverymethod") == null) {
        setValue("bsd_shippingdeliverymethod", 861450000);
        method = 861450000;
    } else {
        method = getValue("bsd_shippingdeliverymethod");
    }

    if (method == 861450000) { // ton
        setVisible("bsd_unitshipping", true);
        setRequired("bsd_unitshipping", "required");
        if (getValue("bsd_unitshipping") == null) {
            setValue("bsd_unitshipping", [{
                id: "{7E3545D7-0AA0-E611-93F6-000C29EBC66A}",
                name: "Tấn",
                entityType: "uom"
            }]);
        }


        setVisible("bsd_truckload", false);
        setNull("bsd_truckload");
        setRequired("bsd_truckload", "none");

        load_shippingpricelist_ton(reset);

    } else if (861450001) { // trip

        setVisible("bsd_unitshipping", false);
        setNull("bsd_unitshipping");
        setRequired("bsd_unitshipping", "none");

        setVisible("bsd_truckload", true);
        setRequired("bsd_truckload", "required");

        if (reset != false) {
            setNull("bsd_truckload");
        }
        load_truckload(reset);
        truckload_change(reset);
    }
}
function shipping_pricelist_change(reset) {
    // code
    if (reset != false) setNull("bsd_priceoftransportationn");
    var pricelist = getValue("bsd_shippingpricelistname");
    if (pricelist != null) {

    } else if (reset != false) {

    }
    shipping_price_change();
}
function load_shippingpricelist_ton(reset) {
    if (reset != false) {
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var effective_from = getValue("effectivefrom");
        var effective_to = getValue("effectiveto");

        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();
        var district_to = null;
        var ward_to = null;
        // lấy district + ward từ Ship To Address
        var fetch_get_shiptoaddress = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_address">',
            '<attribute name="bsd_addressid" />',
            '<attribute name="bsd_ward" />',
            '<attribute name="bsd_district" />',
            '<filter type="and">',
              '<condition attribute="bsd_addressid" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetch_get_shiptoaddress);
        if (rs.length > 0 && rs[0].getValue("bsd_district") != null && rs[0].getValue("bsd_ward") != null) {
            district_to = rs[0].getValue("bsd_district");
            ward_to = rs[0].getValue("bsd_ward");
        }
        var data = null;

        if (request_porter == true) {
            // Có yêu cầu bốc xếp
            //  // nếu có yêu cầu porter, và có theo xã.
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceunitporter" />',
                        '<order attribute="bsd_priceunitporter" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                          '<condition attribute="bsd_priceunitporter" operator="not-null" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");

            var rs = CrmFetchKit.FetchSync(xml);
            if (rs.length > 0) {
                data = {
                    pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                    pricelist_name: rs[0].attributes.bsd_name.value,
                    price: rs[0].attributes.bsd_priceunitporter.value,
                    porter: true
                };
            } else {
                // nếu không có theo xã thì lấy theo quận  huyện
                xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                          '<entity name="bsd_shippingpricelist">',
                            '<attribute name="bsd_shippingpricelistid" />',
                            '<attribute name="bsd_name" />',
                            '<attribute name="bsd_priceunitporter" />',
                            '<order attribute="bsd_priceunitporter" descending="true" />',
                            '<filter type="and">',
                              '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                              '<condition attribute="bsd_priceunitporter" operator="not-null" />',
                              '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                              '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                              '<condition attribute="bsd_wardto" operator="null" />',
                              '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                              ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                            '</filter>',
                          '</entity>',
                        '</fetch>'].join("");
                var rs = CrmFetchKit.FetchSync(xml);
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_priceunitporter.value,
                        porter: true
                    };
                } else {
                    data = null;
                }
            }
        }
        if (request_porter == false || (request_porter == true && data == null)) {
            // Không yêu cầu giao hàng, hoặc là yêu cầu mà không có !
            // Lấy theo xã. + Không lấy giá porter
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceofton" />',
                        '<order attribute="bsd_priceofton" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                          '<condition attribute="bsd_priceofton" operator="not-null" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(xml);
            if (rs.length > 0) {
                data = {
                    pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                    pricelist_name: rs[0].attributes.bsd_name.value,
                    price: rs[0].attributes.bsd_priceofton.value,
                    porter: false
                };
            } else {
                // Không có theo xã thì lấy theo huyện, không yêu cầu hoặc yêu cầu mà không có !
                var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                  '<entity name="bsd_shippingpricelist">',
                    '<attribute name="bsd_shippingpricelistid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="bsd_priceofton" />',
                    '<order attribute="bsd_priceofton" descending="true" />',
                    '<filter type="and">',
                      '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                      '<condition attribute="bsd_priceofton" operator="not-null" />',
                      '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                      '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                      '<condition attribute="bsd_wardto" operator="null" />',
                      '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                      ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                    '</filter>',
                  '</entity>',
                '</fetch>'].join("");
                var rs = CrmFetchKit.FetchSync(xml);
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_priceofton.value,
                        porter: false
                    };
                } else {
                    data = null;
                }
            }
        }

        if (data != null) {
            set_shipping_pricelist(data);
            clearNotification("bsd_shippingpricelistname");
        } else {
            set_shipping_pricelist(null);
        }
    } else {
        porteroption_change(reset);
    }
}
function set_shipping_pricelist(data) {
    if (data != null) {
        setValue("bsd_shippingpricelistname", [{
            id: data.pricelist_id,
            name: data.pricelist_name,
            entityType: "bsd_shippingpricelist"
        }]);
        setValue("bsd_priceoftransportationn", data.price);
        if (data.porter == true) {
            setValue("bsd_shippingporter", true);
            setValue("bsd_porter", true);
        } else {
            setValue("bsd_shippingporter", false);
        }
        if (getValue("bsd_shippingpricelistname") == null) {
            setTimeout(function () {
                set_shipping_pricelist(data);
            }, 50);
        } else {
            porteroption_change(true);
        }
    } else {
        setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
        porteroption_change(true);
    }
}
function shipping_price_change() {
    // validate price
    var price = getValue("bsd_priceoftransportationn");
    if (price == null) {
        //setNotification("bsd_priceoftransportationn", "You must provide a value for Price");
    } else if (price <= 0) {
        setNotification("bsd_priceoftransportationn", "Enter a value from 0");
    } else {
        clearNotification("bsd_priceoftransportationn");
    }
}
function load_truckload(reset) {
    var shiptoaddress = getValue("bsd_shiptoaddress");
    var warehouse_address = getValue("bsd_warehouseaddress");

    var xml_shiptoaddress = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
     '<entity name="bsd_address">',
        '<attribute name="bsd_addressid" />',
        '<attribute name="bsd_ward" />',
        '<attribute name="bsd_province" />',
        '<attribute name="bsd_district" />',
        '<order attribute="bsd_ward" descending="false" />',
        '<filter type="and">',
          '<condition attribute="bsd_addressid" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    CrmFetchKit.Fetch(xml_shiptoaddress, false).then(function (rs) {
        if (rs.length > 0) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
             '<entity name="bsd_truckload">',
               '<attribute name="bsd_truckloadid" />',
               '<attribute name="bsd_name" />',
               '<order attribute="bsd_name" descending="false" />',
               '<link-entity name="bsd_shippingpricelist" from="bsd_truckload" to="bsd_truckloadid" alias="ab">',
                 '<filter>',
                   '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                   '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouse_address[0].id + '" />',
                   '<filter type="or">',
                       '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + rs[0].getValue("bsd_district") + '" />',
                       '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + rs[0].getValue("bsd_ward") + '" />',
                   '</filter>',
                 '</filter>',
               '</link-entity>',
             '</entity>',
           '</fetch>'].join("");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_truckloadid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                                    "<row name='result'  " + "id='bsd_truckloadid'>  " +
                                                    "<cell name='bsd_name'   " + "width='200' />  " +
                                                    "</row>   " +
                                                 "</grid>   ";

            getControl("bsd_truckload").addCustomView(getDefaultView("bsd_truckload"), "bsd_truckload", "bsd_truckload", xml, layoutXml, true);
        }
    });

}
function truckload_change(reset) {
    // load shipping theo trip
    if (reset != false) {
        setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
    }

    var truckload = getValue("bsd_truckload");
    if (truckload != null && reset != false) {
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var effective_from = getValue("effectivefrom");
        var effective_to = getValue("effectiveto");
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");

        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();
        var district_to = null;
        var ward_to = null;
        var data = null;
        // lấy district + ward từ Ship To Address
        var fetch_get_shiptoaddress = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_address">',
            '<attribute name="bsd_addressid" />',
            '<attribute name="bsd_ward" />',
            '<attribute name="bsd_district" />',
            '<filter type="and">',
              '<condition attribute="bsd_addressid" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetch_get_shiptoaddress);
        if (rs.length > 0 && rs[0].getValue("bsd_district") != null && rs[0].getValue("bsd_ward") != null) {
            district_to = rs[0].getValue("bsd_district");
            ward_to = rs[0].getValue("bsd_ward");
        }


        if (request_porter == true) {
            // Có yêu cầu bốc xếp
            //  // nếu có yêu cầu porter, và có theo xã.
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_pricetripporter" />',
                        '<order attribute="bsd_pricetripporter" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                          '<condition attribute="bsd_pricetripporter" operator="not-null" />',
                          '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(xml);
            if (rs.length > 0) {
                console.log(rs[0]);
                data = {
                    pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                    pricelist_name: rs[0].attributes.bsd_name.value,
                    price: rs[0].attributes.bsd_pricetripporter.value,
                    porter: true
                };
            } else {
                // nếu không có theo xã thì lấy theo quận  huyện
                xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                  '<entity name="bsd_shippingpricelist">',
                    '<attribute name="bsd_shippingpricelistid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="bsd_pricetripporter" />',
                    '<order attribute="bsd_pricetripporter" descending="true" />',
                    '<filter type="and">',
                      '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                      '<condition attribute="bsd_pricetripporter" operator="not-null" />',
                      '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                      '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                      '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                      '<condition attribute="bsd_wardto" operator="null" />',
                      '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                      ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                    '</filter>',
                  '</entity>',
                '</fetch>'].join("");
                var rs = CrmFetchKit.FetchSync(xml);
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_pricetripporter.value,
                        porter: true
                    };
                } else {
                    data = null;
                }
            }
        }

        if (request_porter == false || (request_porter == true && data == null)) {
            // Không yêu cầu giao hàng, hoặc là yêu cầu mà không có !
            // Lấy theo xã. + Không lấy giá porter
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceoftrip" />',
                        '<order attribute="bsd_priceoftrip" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                          '<condition attribute="bsd_priceoftrip" operator="not-null" />',
                          '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(xml);
            if (rs.length > 0) {
                data = {
                    pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                    pricelist_name: rs[0].attributes.bsd_name.value,
                    price: rs[0].attributes.bsd_priceoftrip.value,
                    porter: false
                };
            } else {
                // Không có theo xã thì lấy theo huyện, không yêu cầu hoặc yêu cầu mà không có !
                var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                  '<entity name="bsd_shippingpricelist">',
                    '<attribute name="bsd_shippingpricelistid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="bsd_priceoftrip" />',
                    '<order attribute="bsd_priceoftrip" descending="true" />',
                    '<filter type="and">',
                      '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                      '<condition attribute="bsd_priceoftrip" operator="not-null" />',
                      '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                      '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                      '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                      '<condition attribute="bsd_wardto" operator="null" />',
                      '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                      ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                    '</filter>',
                  '</entity>',
                '</fetch>'].join("");
                var rs = CrmFetchKit.FetchSync(xml);
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_priceoftrip.value,
                        porter: false
                    };
                } else {
                    data = null;
                }
            }
        }

        if (data != null) {
            set_shipping_pricelist(data);
            clearNotification("bsd_shippingpricelistname");
        } else {
            set_shipping_pricelist(null);
        }
    } else {
        porteroption_change(reset);
    }
}
function load_warehouse_address(reset) {

    if (reset != false) setNull("bsd_warehouseaddress");

    var warehouse = getValue("bsd_warehouseto");
    if (warehouse != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="bsd_warehouseentity">',
        '<attribute name="bsd_warehouseentityid" />',
        '<attribute name="bsd_address" />',
        '<filter type="and">',
          '<condition attribute="bsd_warehouseentityid" operator="eq" uitype="bsd_warehouseentity" value="' + warehouse[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && reset != false) {
                var first = rs[0];
                console.log(first);
                setValue("bsd_warehouseaddress", [{
                    id: first.attributes.bsd_address.guid,
                    name: first.attributes.bsd_address.name,
                    entityType: first.attributes.bsd_address.logicalName
                }]);
            }
        });
    }
    check_enable_shipping(reset);
}
function porteroption_change(reset) {
    //if (reset != false) setNull(["bsd_priceofporter", "bsd_pricepotter"]);
    var request_porter = getValue("bsd_requestporter");
    var porteroption = getValue("bsd_porteroption");
    var shipping_porter = getValue("bsd_shippingporter");
    if (request_porter == false) {
        // Không yêu cầu !
        setDisabled(["bsd_porteroption"], true);

        setVisible(["bsd_priceofporter", "bsd_pricepotter"], false);
        setRequired(["bsd_priceofporter", "bsd_pricepotter"], "none");
        if (reset != false) {
            setValue("bsd_porteroption", false);
            setValue("bsd_porter", 861450001);
            setNull(["bsd_priceofporter", "bsd_pricepotter"]);
        }

    } else if (request_porter == true) { // Có yêu cầu
        if (shipping_porter == true) { // Giá đã gồm Porter
            setDisabled(["bsd_porteroption"], true);
            setVisible(["bsd_priceofporter", "bsd_pricepotter"], false);
            setRequired(["bsd_priceofporter", "bsd_pricepotter"], "none");
            if (reset != false) {
                setValue("bsd_porteroption", true);
                setValue("bsd_porter", 861450000);
                setNull(["bsd_priceofporter", "bsd_pricepotter"]);
            }
        } else { // Không có giá  porter
            setDisabled(["bsd_porteroption"], true);
            setVisible(["bsd_priceofporter", "bsd_pricepotter"], true);
            setRequired(["bsd_priceofporter", "bsd_pricepotter"], "required");
            if (reset != false) {
                setValue("bsd_porteroption", true);
                setValue("bsd_porter", 861450000);
            }
            load_porter();
            porterprice_change(reset);
        }
    }
}
function porterprice_change(reset) {
    if (reset != false) {
        var bsd_priceofporter = getValue("bsd_priceofporter");
        if (bsd_priceofporter != null) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_porter">',
                '<attribute name="bsd_porterid" />',
                '<attribute name="bsd_price" />',
                '<order attribute="bsd_price" descending="false" />',
                '<filter type="and">',
                  '<condition attribute="bsd_porterid" operator="eq" uitype="bsd_porter" value="' + bsd_priceofporter[0].id + '" />',
                  '<condition attribute="bsd_price" operator="not-null" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    setValue("bsd_pricepotter", first.getValue("bsd_price"));
                }
            });
        } else {
            setNull("bsd_pricepotter");
        }
    }
}
function check_date() {
    // effective date
    clearFormNotification("1");
    clearNotification("bsd_date");

    var effective_from = getValue("effectivefrom");
    var currentDateTime = getCurrentDateTime();

    if (effective_from != null) {

        var date = getValue("bsd_date");
        if (effective_from < currentDateTime) {
            setFormNotification("The Effective From Date cannot occur after current date", "INFO", "1");
            setValue("effectivefrom", currentDateTime);
        }
        if (date > effective_from) {
            setNotification("bsd_date", "The Effective From Date cannot occur after Date");
        }
    }
    check_enable_shipping(true);
    check_fromdate_todate();
}
function getCurrentDateTime() {
    var d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
}
function check_fromdate_todate() {
    clearNotification("bsd_fromdate");
    clearNotification("bsd_todate");
    var from = getValue("bsd_fromdate");
    var to = getValue("bsd_todate");
    var effective_from = getValue("effectivefrom")
    var currentDateTime = getCurrentDateTime();
    if (from != null) {
        if (from <= effective_from) {
            setNotification("bsd_fromdate", "The From Date is must after the Effective From");
        }
    }
    if (to != null) {
        if (to <= effective_from) {
            setNotification("bsd_todate", "The To Date is must after the Effective From");
        }
    }
    if (from != null && to != null) {
        if (from > to) {
            setNotification("bsd_fromdate", "The From Date cannot occur before the To Date");
        }
    }
}
function modified_on_change() {
    shipping_change(false);
    if (formtype == 1) {
        Xrm.Page.data.save();
    }
}
function onsave(econtext) {
    var bsd_transportation = getValue("bsd_transportation");
    if (bsd_transportation == true) {
        var pricelist = getValue("bsd_shippingpricelistname");
        if (pricelist == null) {
            setNotification("bsd_shippingpricelistname", "You must provide a value for Price List !");
        }
    }
}
function load_sale_tax_group() {
    var customer = getValue("customerid");
    if (customer != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="account">',
           ' <attribute name="name" />',
           ' <attribute name="bsd_saletaxgroup" />',
           ' <filter type="and">',
            '  <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
           ' </filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && rs[0].getValue("bsd_saletaxgroup") != null) {
                console.log(rs[0]);
                setValue("bsd_saletaxgroup", [{
                    id: rs[0].getValue("bsd_saletaxgroup"),
                    name: rs[0].attributes.bsd_saletaxgroup.name,
                    entityType: rs[0].attributes.bsd_saletaxgroup.logicalName
                }]);
            }
        });
    } else {
        setNull("bsd_saletaxgroup");
    }
}
function getDate(d) {
    return new Date('' + d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear() + ' 12:00:00 AM');
}
//end code from B2B -> B2C

//huy: add function 11h50 27/2/2017
function load_porter() {
    var effective_to = getValue("effectiveto");
    var effective_from = getValue("effectivefrom");
    getControl("bsd_priceofporter").removePreSearch(presearch_priceofporter);
    if (effective_from != null && effective_to != null) {
        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();

        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_porter">',
                   '     <attribute name="bsd_porterid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_effectiveto" />',
                   '     <attribute name="bsd_effectivefrom" />',
                   '     <attribute name="bsd_unit" />',
                   '     <attribute name="bsd_price" />',
                   '     <order attribute="bsd_effectiveto" descending="true" />',
                   '     <order attribute="createdon" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");

        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_porterid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                           "<row name='result'  " + "id='bsd_porterid'>  " +
                                           "<cell name='bsd_name'   " + "width='200' />  " +
                                           "</row>" +
                                        "</grid>";
        getControl("bsd_priceofporter").addCustomView(getDefaultView("bsd_priceofporter"), "bsd_porter", "bsd_porter", xml, layoutXml, true);
        var priceofporter = getValue("bsd_priceofporter");
        if (priceofporter != null) {
            var xml2 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_porter">',
                   '     <attribute name="bsd_porterid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_effectiveto" />',
                   '     <attribute name="bsd_effectivefrom" />',
                   '     <attribute name="bsd_unit" />',
                   '     <attribute name="bsd_price" />',
                   '     <order attribute="bsd_effectiveto" descending="true" />',
                   '     <order attribute="createdon" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="bsd_porterid" operator="eq" uitype="bsd_porter" value="' + priceofporter[0].id + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join('');
            CrmFetchKit.Fetch(xml2, false).then(function (rs) {
                if (rs.length > 0) {

                } else {
                    setTimeout(function () {
                        setNull(["bsd_priceofporter", "bsd_pricepotter"]);
                    }, 10);
                }
            });
        }
    } else {
        clear_priceofporter();
        setNull(["bsd_priceofporter", "bsd_pricepotter"]);
    }
}
function clear_priceofporter() {
    getControl("bsd_priceofporter").addPreSearch(presearch_priceofporter);
}
function presearch_priceofporter() {
    getControl("bsd_priceofporter").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//end

//huy -8h20h00  14/3/2017
function load_unitdefault(reset) {

    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="uom">',
        '<attribute name="name" />',
        '<filter type="and">',
          '<condition attribute="uomscheduleid" operator="eq" uitype="uomschedule" value="{A8C3859A-0095-E611-80CC-000C294C7A2D}" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' object='1' jump='uomid' select='1' icon='0' preview='0'>  " +
                     "<row name='result'  " + "id='uomid'><cell name='name'   " + "width='200' /> </row></grid>";
    getControl("bsd_unitdefault").addCustomView(getDefaultView("bsd_unitdefault"), "uom", "uom", xml, layoutXml, true);

    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs[0].getValue("bsd_unitdefault") != null) {
                setValue("bsd_unitdefault", [{
                    id: rs[0].attributes.bsd_unitdefault.guid,
                    name: rs[0].attributes.bsd_unitdefault.name,
                    entityType: rs[0].attributes.bsd_unitdefault.logicalName
                }]);
            }
        });

    }
}

//huy
function quotationtype_change() {
    load_account_whithtype();
}

//huy 11h30 20/3/2017
//load account NPP theo Quotation Type: De Nghi Dat Hang
function load_account_whithtype() {
    var quotation_type = getValue("bsd_quotationtype");
    if (quotation_type != null) {
        var fetchxml = '';
        if (quotation_type == 100000000) {
            fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                       '   <entity name="account">',
                       '     <attribute name="name" />',
                       '     <attribute name="primarycontactid" />',
                       '     <attribute name="telephone1" />',
                       '     <attribute name="accountid" />',
                       '     <order attribute="name" descending="false" />',
                       '     <filter type="and">',
                       '       <condition attribute="bsd_accounttype" operator="eq" value="100000000" />',
                       '       <condition attribute="statecode" operator="eq" value="0" />',
                       '     </filter>',
                       '   </entity>',
                       ' </fetch>'].join('');
        } else {
            fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '      <entity name="account">',
                            '        <attribute name="name" />',
                            '        <attribute name="primarycontactid" />',
                            '        <attribute name="telephone1" />',
                            '        <attribute name="accountid" />',
                            '        <order attribute="name" descending="false" />',
                            '        <filter type="and">',
                            '          <condition attribute="bsd_accounttype" operator="in">',
                            '            <value>861450000</value>',
                            '            <value>861450001</value>',
                            '          </condition>',
                            '          <condition attribute="statecode" operator="eq" value="0" />',
                            '        </filter>',
                            '      </entity>',
                            '    </fetch>'].join("");
        }
        LookUp_After_Load("customerid", "account", "name", "Account", fetchxml);
    }
}

//huy
function load_currencydefault(reset) {
    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs[0].getValue("bsd_currencydefault") != null) {
                setValue("bsd_currencydefault", [{
                    id: rs[0].attributes.bsd_currencydefault.guid,
                    name: rs[0].attributes.bsd_currencydefault.name,
                    entityType: rs[0].attributes.bsd_currencydefault.logicalName
                }]);
            }
        });

    }
}
/*
    Mr: Diệm
    Note: Hàm lấy ngày...dùng chung
*/
function getFullDay(day) {
    var fullday = null;
    var createdatefrom = getValue(day);
    if (createdatefrom != null) {
        var year = createdatefrom.getFullYear();
        var date = createdatefrom.getDate();

        var laymonth = (createdatefrom.getMonth() + 1);
        var month;
        if (laymonth > 0 && laymonth <= 9) {
            month = "0" + laymonth;
        } else {
            month = laymonth;
        }
        fullday = year + "-" + month + "-" + date;
    }
    return fullday;
}
/*
   Mr: Diệm
   Note: Load Price group theo fromdate.
*/
function LK_PotentialCustomer_Change(reset) {
    debugger;
    var customer = getValue("customerid");
    var fullday = getFullDay("effectivefrom");
    var currency = getValue("transactioncurrencyid");
    if (reset != false) setNull("pricelevelid");
    var first;
    if (customer != null && currency != null && fullday != null) {
        var rsaccount = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" count="1"  mapping="logical" distinct="true">',
                           '    <entity name="pricelevel">',
                           '        <all-attributes />',
                           '        <order attribute="createdon" descending="false" />',
                           '    <filter type="and">',
                           '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                           '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                           '        <condition attribute="bsd_account" operator="eq" uitype="account" value="' + customer[0].id + '"/>',
                           '        <condition attribute="statecode" operator="eq" value="0" />',
                           '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                           '    </filter>',
                           '    </entity>',
                           '</fetch>'].join(''));
        console.log(rsaccount);
        if (rsaccount.length > 0) /*Nếu dữ liệu Account*/ {
            first = rsaccount[0];
            if (getValue("pricelevelid") == null) {
                setValue("pricelevelid",
                     [{
                         id: first.Id,
                         name: first.attributes.name.value,
                         entityType: first.logicalName
                     }]);
            }
        }
        else /*Price group*/ {
            var rspricegroup = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                                '   <entity name="pricelevel">',
                                '        <all-attributes />',
                                '     <order attribute="createdon" descending="false" />',
                                '     <filter type="and">',
                                '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                                '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                                '        <condition attribute="statecode" operator="eq" value="0" />',
                                '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                                '     </filter>',
                                '     <link-entity name="bsd_pricegroups" from="bsd_pricegroupsid" to="bsd_pricegroups" alias="ai">',
                                '       <link-entity name="account" from="bsd_pricegroups" to="bsd_pricegroupsid" alias="aj">',
                                '         <filter type="and">',
                                '           <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                                '         </filter>',
                                '       </link-entity>',
                                '     </link-entity>',
                                '    </entity>',
                                '</fetch>'].join(''));
            console.log(rspricegroup);
            if (rspricegroup.length > 0) {

                first = rspricegroup[0];
                if (getValue("pricelevelid") == null) {
                    setValue("pricelevelid",
                         [{
                             id: first.Id,
                             name: first.attributes.name.value,
                             entityType: first.logicalName
                         }]);
                }
            }
            else /*All*/ {
                var rsaccount = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                 ' <entity name="account">',
                                 '   <attribute name="accountid" />',
                                 '   <attribute name="bsd_accounttype" />',
                                 '   <order attribute="bsd_accounttype" descending="false" />',
                                 '   <filter type="and">',
                                 '     <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                                 '     <condition attribute="statecode" operator="eq" value="0" />',
                                 '   </filter>',
                                 ' </entity>',
                                '</fetch>'].join(''));
                var typeaccount = rsaccount[0].attributes.bsd_accounttype.value;
                var fetchxml = "";
                if (typeaccount == 861450000 || typeaccount == 861450001) {
                    fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                              '    <entity name="pricelevel">',
                              '        <all-attributes />',
                              '      <order attribute="createdon" descending="false" />',
                              '      <filter type="and">',
                              '        <condition attribute="statecode" operator="eq" value="0" />',
                              '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                              '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                              '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                              '        <condition attribute="bsd_typeprice" operator="eq" value="2" />',
                              '        <condition attribute="bsd_type" operator="eq" value="861450000" />',
                              '     </filter>',
                              '   </entity>',
                              '</fetch>'].join('');
                }
                else if (typeaccount == 100000000) {
                    fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                              '    <entity name="pricelevel">',
                              '        <all-attributes />',
                              '      <order attribute="createdon" descending="false" />',
                              '      <filter type="and">',
                              '        <condition attribute="statecode" operator="eq" value="0" />',
                              '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                              '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                              '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                              '        <condition attribute="bsd_typeprice" operator="eq" value="2" />',
                              '        <condition attribute="bsd_type" operator="eq" value="861450001" />',
                              '     </filter>',
                              '   </entity>',
                              '</fetch>'].join('');

                } else {
                    fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                              '    <entity name="pricelevel">',
                              '        <all-attributes />',
                              '      <order attribute="createdon" descending="false" />',
                              '      <filter type="and">',
                              '        <condition attribute="statecode" operator="eq" value="0" />',
                              '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                              '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                              '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                              '        <condition attribute="bsd_typeprice" operator="eq" value="2" />',
                              '     </filter>',
                              '   </entity>',
                              '</fetch>'].join('');
                }
                var rsAll = CrmFetchKit.FetchSync(fetchxml);
                if (rsAll.length > 0) {
                    first = rsAll[0];
                    if (getValue("pricelevelid") == null) {
                        setValue("pricelevelid",
                             [{
                                 id: first.Id,
                                 name: first.attributes.name.value,
                                 entityType: first.logicalName
                             }]);
                    }
                } else {
                    m_alert("Price List for this Account does not exist. Please define Price List first.");
                }
            }
        }
    }
    else if (reset != false) {
        clear_priceleve();
    }
    Load_exchangerate(currency);
}
/*Diê?m: Load exchangerate tu` currency*/
function Load_exchangerate(currency) {
    if (currency != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="transactioncurrency">',
                        '    <attribute name="transactioncurrencyid" />',
                        '    <attribute name="currencyname" />',
                        '    <attribute name="isocurrencycode" />',
                        '    <attribute name="currencysymbol" />',
                        '    <attribute name="exchangerate" />',
                        '    <attribute name="currencyprecision" />',
                        '    <order attribute="currencyname" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            if (getValue("exchangerate") == null) {
                setValue("exchangerate", rs[0].attributes.exchangerate.value);
            }
        }
    }
    else {
        setNull(["exchangerate"])
    }
}
/*
    Mr: Diệm
    Note: Set Function null and clear.
*/
function clear_priceleve() {
    getControl("pricelevelid").addPreSearch(presearch_priceleve);
}
function presearch_priceleve() {
    getControl("pricelevelid").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//End Diệm



//Đăng
//Description: Set Disable when Quote type
function setdisable_deliveryattheport() {
    var quotationtype = getValue("bsd_quotationtype");
    if (quotationtype == 861450001) // Nc ngoài
    {
        setDisabled("bsd_deliveryattheport", false);
    }
    else {
        setDisabled("bsd_deliveryattheport", true);
        setValue("bsd_deliveryattheport", 0);
        set_DisableDeliveryPort();
    }
}
//Description: Disable when Delivery At The Port is No
function set_DisableDeliveryPort() {
    var deliveryport = getValue("bsd_deliveryattheport");
    if (deliveryport == "0") {
        setVisible(["bsd_port", "bsd_addressport"], false);
        set_value(["bsd_port", "bsd_addressport"], null);
        clear_LoadAddressPort();
    }
    if (deliveryport == "1") {
        setVisible(["bsd_port", "bsd_addressport"], true);
        ChooseAccountPort();
    }
}
//Description: Choose Account in Port
function ChooseAccountPort() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="accountid" />',
                    '<order attribute="name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="861450004" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");

    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='accountid'>  " +
                               "<cell name='name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>";
    getControl("bsd_port").addCustomView(getControl("bsd_port").getDefaultView(), "account", "Port", fetchxml, layoutXml, true);
    LoadAddressPort(false);
}
//Description: Load Address Port
function LoadAddressPort(result) {
    if (result != false) setNull("bsd_addressport");
    var accountport = getValue("bsd_port");
    if (accountport != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + accountport[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            console.log(rs);
            if (rs.length > 0 && result != false) {
                var first = rs[0];
                if (getValue("bsd_addressport") == null) {
                    setValue("bsd_addressport", [{
                        id: first.Id,
                        name: first.attributes.bsd_name.value,
                        entityType: first.logicalName
                    }]
                   );
                }
            }

        }, function (er) { });
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_addressid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>";
        getControl("bsd_addressport").removePreSearch(presearch_LoadAddressPort);
        getControl("bsd_addressport").addCustomView(getControl("bsd_addressport").getDefaultView(), "bsd_address", "Address", fetchxml, layoutXml, true);
    }
    else if (result != false) {
        clear_LoadAddressPort();
    }
}
//Description:set Date
function Load_FromDate_Auto() {
    var date = new Date();
    setValue("bsd_date", date);
}
//End Đăng

//Description:Filter_subgrid
function filter_gridQuotebyID() {
    debugger;
    //RelatedCars : is name of subgrid given on Form.
    //var objSubGrid = document.getElementById("subgridQuote");
    //var objSubGrid = Xrm.Page.getControl("subgridQuote").getGrid();
    var objSubGrid = window.parent.document.getElementById("subgridQuote");
    //CRM loads subgrid after form is loaded.. so when we are adding script on form load.. need to wait unitil subgrid is loaded. Thats why adding delay..
    if (objSubGrid == null) {
        setTimeout(filter_gridQuotebyID, 2000);
        return;
    } else {
        //when subgrid is loaded, get category value
        var name = Xrm.Page.getAttribute("quotenumber").getValue();
        if (name != null) {
            //Create FetchXML for sub grid to filter records based on category
            var FetchXml = "<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0'>" +
                              "<entity name='quote'>" +
                                    "<attribute name='name' />" +
                                    "<attribute name='customerid' />" +
                                    "<attribute name='statecode' />" +
                                    "<attribute name='totalamount' />" +
                                    "<attribute name='quoteid' />" +
                                    "<attribute name='createdon' />" +
                                    "<order attribute='name' descending='false' />" +
                                    "<filter type='and'>" +
                                        "<condition attribute='quotenumber' operator='eq' value='" + name + "' />" +
                                    "</filter>" +
                                "</entity>" +
                              "</fetch>";
            //apply layout and filtered fetchXML
            if (objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", FetchXml);
                objSubGrid.control.Refresh();
                //document.getElementById("subgridQuote").control.SetParameter("fetchXml", FetchXml)
                ////objSubGrid.control.SetParameter("fetchXml", FetchXml);
                ////Refresh grid to show filtered records only. 
                //document.getElementById("subgridQuote").control.Refresh();
            } else {
                setTimeout(filter_gridQuotebyID, 500);
            }
        }
    }
}
//Description:filter type customer and distributor
function set_customer() {
    Xrm.Page.getAttribute("bsd_priceoftransportationn").setSubmitMode("always");
    Xrm.Page.getAttribute("bsd_unitshipping").setSubmitMode("always");
    var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    var entityName = "account";
    var viewDisplayName = "test";
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='account'>");
    xml.push("<attribute name='accountid' />");
    xml.push("<attribute name='name'/>");
    xml.push("<order attribute='name' descending='false' />");
    xml.push("<filter type='and' >");
    xml.push("<condition attribute='bsd_accounttype' operator='in'>");
    xml.push("<value>");
    xml.push("861450000");
    xml.push("</value>");
    xml.push("<value>");
    xml.push("100000000");
    xml.push("</value>");
    xml.push("<value>");
    xml.push("861450005");
    xml.push("</value>");
    xml.push("</condition>");
    xml.push("<condition attribute='statecode' operator='eq' value='0'/>");
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='accountid'>  " +
                       "<cell name='name'    " + "width='200' />  " +
                       "</row>   " +
                    "</grid>   ";
    Xrm.Page.getControl("customerid").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    Xrm.Page.getControl("customerid").addPreSearch(addFilter);
    Xrm.Page.getControl("bsd_partnerscompany").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    Xrm.Page.getControl("bsd_invoicenameaccount").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
}
function addFilter() {
    var customerAccountFilter = "<filter type='and'><condition attribute='contactid' operator='null' /></filter>";
    Xrm.Page.getControl("customerid").addCustomFilter(customerAccountFilter, "contact");
}
//Description:get customer information
function get_customerinformation() {
    Xrm.Page.getAttribute("bsd_paymentterm").setSubmitMode("always");
    Xrm.Page.getAttribute("bsd_paymentmethod").setSubmitMode("always");
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    var fielddefault = Xrm.Page.getAttribute("bsd_warehouseto");
    var fieldaddress = Xrm.Page.getAttribute("bsd_address");
    var fieldpaymentterm = Xrm.Page.getAttribute("bsd_paymentterm");
    var fieldpaymentmethod = Xrm.Page.getAttribute("bsd_paymentmethod");
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount");
    var addressinvoicenameaccount = Xrm.Page.getAttribute("bsd_addressinvoiceaccount");
    var partnerscompany = Xrm.Page.getAttribute("bsd_partnerscompany");
    var fieldsaletaxgroup = Xrm.Page.getAttribute("bsd_saletaxgroup");
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var xmlaccount = [];
    var xmladdresscustomerbusiness = [];
    var xmladdresscustomer = [];
    var xmladdressdelivery = [];
    var xmlreceiptaccount = [];
    var xmlinvoiceaddress = [];
    var xmlinvoiceaccountaddress = [];
    if (customer != null) {
        setDisabled(["bsd_address", "bsd_addressinvoiceaccount", "bsd_shiptoaddress"], false);
        Xrm.Page.getAttribute("bsd_partnerscompany").setValue(null);
        Xrm.Page.getAttribute("bsd_shiptoaddress").setValue(null);
        //Thông tin customer
        fetch(xmlaccount, "account", ["name", "primarycontactid", "telephone1", "accountid"
                              , "bsd_taxregistration", "fax", "accountnumber", "emailaddress1"
                              , "bsd_paymentterm", "bsd_paymentmethod", "transactioncurrencyid", "bsd_saletaxgroup"], ["createdon"], false, null,
                              ["accountid"], ["eq"], [0, customer[0].id, 1]);
        CrmFetchKit.Fetch(xmlaccount.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                invoicenameaccount.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.name.value,
                    entityType: rs[0].logicalName
                }]);
                partnerscompany.setValue([{
                    id: customer[0].id,
                    name: customer[0].name,
                    entityType: 'account'
                }]);
                Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(rs[0].attributes.accountnumber.value);
                if (rs[0].attributes.telephone1 != null) {
                    Xrm.Page.getAttribute("bsd_telephone").setValue(rs[0].attributes.telephone1.value);
                }
                if (rs[0].attributes.telephone1 == null) {
                    Xrm.Page.getAttribute("bsd_telephone").setValue(null);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(rs[0].attributes.bsd_taxregistration.value);
                }
                if (rs[0].attributes.bsd_taxregistration == null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                }
                if (rs[0].attributes.fax != null) {
                    Xrm.Page.getAttribute("bsd_fax").setValue(rs[0].attributes.fax.value);
                }
                if (rs[0].attributes.fax == null) {
                    Xrm.Page.getAttribute("bsd_fax").setValue(null);
                }
                if (rs[0].attributes.accountnumber != null) {
                    var no = rs[0].attributes.accountnumber.value;
                    Xrm.Page.getAttribute("bsd_customercode").setValue(no);
                }
                if (rs[0].attributes.accountnumber == null) {
                    Xrm.Page.getAttribute("bsd_customercode").setValue(null);
                }
                if (rs[0].attributes.emailaddress1 != null) {
                    Xrm.Page.getAttribute("bsd_mail").setValue(rs[0].attributes.emailaddress1.value);
                }
                if (rs[0].attributes.emailaddress1 == null) {
                    Xrm.Page.getAttribute("bsd_mail").setValue(null);
                }
                if (rs[0].attributes.bsd_paymentterm != null) {
                    //fieldpaymentterm.setValue(rs[0].attributes.bsd_paymentterm.name);
                    fieldpaymentterm.setValue([{
                        id: rs[0].attributes.bsd_paymentterm.guid,
                        name: rs[0].attributes.bsd_paymentterm.name,
                        entityType: 'bsd_paymentterm'
                    }]);
                }
                if (rs[0].attributes.bsd_paymentterm == null) {
                    //fieldpaymentterm.setValue(rs[0].attributes.bsd_paymentterm.name);
                    fieldpaymentterm.setValue(null);
                }
                if (rs[0].attributes.bsd_paymentmethod != null) {
                    //fieldpaymentmethod.setValue(rs[0].attributes.bsd_paymentmethod.name);
                    fieldpaymentmethod.setValue([{
                        id: rs[0].attributes.bsd_paymentmethod.guid,
                        name: rs[0].attributes.bsd_paymentmethod.name,
                        entityType: 'bsd_methodofpayment'
                    }]);
                }
                if (rs[0].attributes.bsd_paymentmethod == null) {
                    //fieldpaymentmethod.setValue(rs[0].attributes.bsd_paymentmethod.name);
                    fieldpaymentmethod.setValue(null);
                }
                if (rs[0].attributes.bsd_saletaxgroup != null) {
                    fieldsaletaxgroup.setValue([{
                        id: rs[0].attributes.bsd_saletaxgroup.guid,
                        name: rs[0].attributes.bsd_saletaxgroup.name,
                        entityType: 'bsd_saletaxgroup'
                    }]);
                }
                else if (rs[0].attributes.bsd_saletaxgroup == null) {
                    fieldsaletaxgroup.setValue(null);
                }
            }
        },
        function (er) {
            console.log(er.message)
        });
        //Địa chỉ customer
        var viewIdaddresscustomerbusiness = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameaddresscustomerbusiness = "bsd_address";
        var viewDisplayNameaddresscustomerbusiness = "Address Customer Business";
        fetch(xmladdresscustomerbusiness, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
        ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, customer[0].id]);
        var layoutXmladdresscustomerbusiness = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmladdresscustomerbusiness.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    fieldaddress.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
                Xrm.Page.getControl("bsd_address").addCustomView(viewIdaddresscustomerbusiness, entityNameaddresscustomerbusiness, viewDisplayNameaddresscustomerbusiness, xmladdresscustomerbusiness.join(""), layoutXmladdresscustomerbusiness, true);
            }
            else {
                var viewIdaddresscustomer = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameaddresscustomer = "bsd_address";
                var viewDisplayNameaddresscustomer = "Customer Address";
                fetch(xmladdresscustomer, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXmladdresscustomer = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";

                CrmFetchKit.Fetch(xmladdresscustomer.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        if (rs[0].attributes.bsd_name != null) {
                            fieldaddress.setValue([{
                                id: rs[0].Id,
                                name: rs[0].attributes.bsd_name.value,
                                entityType: rs[0].logicalName
                            }]);
                            Xrm.Page.getControl("bsd_address").addCustomView(viewIdaddresscustomer, entityNameaddresscustomer, viewDisplayNameaddresscustomer, xmladdresscustomer.join(""), layoutXmladdresscustomer, true);
                        }
                    }
                    else {
                        fieldaddress.setValue(null);
                        Xrm.Page.getControl("bsd_address").addCustomView(viewIdaddresscustomer, entityNameaddresscustomer, viewDisplayNameaddresscustomer, xmladdresscustomer.join(""), layoutXmladdresscustomer, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }
        },
        function (er) {
            console.log(er.message)
        });
        //Địa chỉ ship to address của receipt customer
        var viewIdaddressdelivery = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameaddressdelivery = "bsd_address";
        var viewDisplayNameaddressdelivery = "Ship To Address";
        fetch(xmladdressdelivery, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
           ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customer[0].id]);
        var layoutXmladdressdelivery = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmladdressdelivery.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                fieldshiptoaddress.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.bsd_name.value,
                    entityType: rs[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdaddressdelivery, entityNameaddressdelivery, viewDisplayNameaddressdelivery, xmladdressdelivery.join(""), layoutXmladdressdelivery, true);
            }
            else {
                var viewIdreceiptaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNamereceiptaddress = "bsd_address";
                var viewDisplayNamereceiptaddress = "Receipt Account Address";
                fetch(xmlreceiptaccount, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                    ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXmlreceiptaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdreceiptaddress, entityNamereceiptaddress, viewDisplayNamereceiptaddress, xmlreceiptaccount.join(""), layoutXmlreceiptaddress, true);
            }

        }, function (er) {
            console.log(er.message)
        });
        //Địa chỉ Invoice của invoice account
        var viewIdinvoiceaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameinvoiceaddress = "bsd_address";
        var viewDisplayNameinvoiceaddress = "Inovice Addeess";
        fetch(xmlinvoiceaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
          ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Invoice" + "%", 1, "0", 2, customer[0].id]);
        var layoutXmlinvoiceaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlinvoiceaddress.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    addressinvoicenameaccount.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdinvoiceaddress, entityNameinvoiceaddress, viewDisplayNameinvoiceaddress, xmlinvoiceaddress.join(""), layoutXmlinvoiceaddress, true);
            }
            else {
                var viewIdinvoiceaccountaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameinvoiceaccountaddress = "bsd_address";
                var viewDisplayNameinvoiceaccountaddress = "Invoice Account Address";
                fetch(xmlinvoiceaccountaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                  ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXmlinvoiceaccountaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                CrmFetchKit.Fetch(xmlinvoiceaccountaddress.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        if (rs[0].attributes.bsd_name != null) {
                            addressinvoicenameaccount.setValue([{
                                id: rs[0].Id,
                                name: rs[0].attributes.bsd_name.value,
                                entityType: rs[0].logicalName
                            }]);
                        }
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdinvoiceaccountaddress, entityNameinvoiceaccountaddress, viewDisplayNameinvoiceaccountaddress, xmlinvoiceaccountaddress.join(""), layoutXmlinvoiceaccountaddress, true);
                    }
                    else {
                        addressinvoicenameaccount.setValue(null);
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdinvoiceaccountaddress, entityNameinvoiceaccountaddress, viewDisplayNameinvoiceaccountaddress, xmlinvoiceaccountaddress.join(""), layoutXmlinvoiceaccountaddress, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }

        },
         function (er) {
             console.log(er.message)
         });
    }
    else {
        setDisabled(["bsd_address", "bsd_addressinvoiceaccount", "bsd_shiptoaddress"], true);
        setNull(["pricelevelid", "bsd_telephone", "bsd_taxregistration", "bsd_fax", "bsd_customercode", "bsd_mail"
                    , "bsd_paymentterm", "bsd_paymentmethod", "bsd_contact", "bsd_invoicenameaccount", "bsd_invoiceaccount", "bsd_addressinvoiceaccount"
                    , "bsd_partnerscompany", "pricelevelid"], null);
        fieldaddress.setValue(null);
        fieldshiptoaddress.setValue(null);
        fielddefault.setValue(null);
    }
}
//Description:load contact theo account
function get_contact() {
    var contactdefault = Xrm.Page.getAttribute("bsd_contact");
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var xmlprimarycontact = [];
    var xmlcontactcustomer = [];
    if (customer != null) {
        xmlprimarycontact.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xmlprimarycontact.push("<entity name='account'>");
        xmlprimarycontact.push("<attribute name='name' />");
        xmlprimarycontact.push("<attribute name='primarycontactid'/>");
        xmlprimarycontact.push("<attribute name='telephone1'/>");
        xmlprimarycontact.push("<attribute name='accountid'/>");
        xmlprimarycontact.push("<order attribute='name' descending='false'/> ");
        xmlprimarycontact.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
        xmlprimarycontact.push("<filter type='and'>");
        xmlprimarycontact.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "'/>");
        xmlprimarycontact.push("<condition attribute='bsd_contacttype' operator='eq' value='861450000' />");
        xmlprimarycontact.push("</filter>");
        xmlprimarycontact.push("</link-entity>");
        xmlprimarycontact.push("</entity>");
        xmlprimarycontact.push("</fetch>");
        CrmFetchKit.Fetch(xmlprimarycontact.join(""), true).then(function (rs) {
            if (rs.length > 0 && rs[0].attributes.primarycontactid != null) {
                contactdefault.setValue([{
                    id: rs[0].attributes.primarycontactid.guid,
                    name: rs[0].attributes.primarycontactid.name,
                    entityType: 'contact'
                }]);
                var viewIdcontactcustomer = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityNamecontactcustomer = "contact";
                var viewDisplayNamecontactcustomer = "Contact Customer";
                fetch(xmlcontactcustomer, "contact", ["fullname", "telephone1", "contactid", "accountid"], ["fullname"], true, null,
                            ["parentcustomerid", "bsd_contacttype", "statecode"], ["eq", "eq", "eq"], [0, customer[0].id, 1, "861450000", 2, "0"]);
                var layoutXmlcontactcustomer = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_contact").addCustomView(viewIdcontactcustomer, entityNamecontactcustomer, viewDisplayNamecontactcustomer, xmlcontactcustomer.join(""), layoutXmlcontactcustomer, true);
            }
            else {
                var viewIdcontactcustomer = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityNamecontactcustomer = "contact";
                var viewDisplayName1 = "Contact Customer";
                fetch(xmlcontactcustomer, "contact", ["fullname", "telephone1", "contactid", "accountid"], ["fullname"], true, null,
                           ["parentcustomerid", "bsd_contacttype", "statecode"], ["eq", "eq", "eq"], [0, customer[0].id, 1, "861450000", 2, "0"]);
                var layoutXmlcontactcustomer = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_contact").addCustomView(viewIdcontactcustomer, entityNamecontactcustomer, viewDisplayNamecontactcustomer, xmlcontactcustomer.join(""), layoutXmlcontactcustomer, true);
                CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                    if (rs.length > 0 && rs[0].attributes.fullname != null) {
                        contactdefault.setValue([{
                            id: rs[0].Id,
                            name: rs[0].attributes.fullname.value,
                            entityType: rs[0].logicalName
                        }]);
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Description:gan gia tri cho filed invoice address
function set_invoiceaddress() {
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    var addressinvoicenameaccount = Xrm.Page.getAttribute("bsd_addressinvoiceaccount");
    var xmladdressinvoice = [];
    var xmladdressinvoiceaccount = [];
    var viewIdaddressinvoice = [];
    var xmlinvoiceaccount = [];
    var xml4 = [];
    if (invoicenameaccount != null) {
        //Địa chỉ invoice account
        Xrm.Page.getControl("bsd_addressinvoiceaccount").setDisabled(false);
        var viewIdaddressinvoice = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameaddressinvoice = "bsd_address";
        var viewDisplayNameaddressinvoice = "Invoice Address";
        fetch(xmladdressinvoice, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Invoice" + "%", 1, "0", 2, invoicenameaccount[0].id]);
        var layoutXmladdressinvoice = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmladdressinvoice.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    addressinvoicenameaccount.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdaddressinvoice, entityNameaddressinvoice, viewDisplayNameaddressinvoice, xmladdressinvoice.join(""), layoutXmladdressinvoice, true);
            }
            else {
                var viewIdaddressinvoiceaccount = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameaddressinvoiceaccount = "bsd_address";
                var viewDisplayNameaddressinvoiceaccount = "Address Invoice Account";
                fetch(xmladdressinvoiceaccount, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                         ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, invoicenameaccount[0].id]);
                var layoutXmladdressinvoiceaccount = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                CrmFetchKit.Fetch(xmladdressinvoiceaccount.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        if (rs[0].attributes.bsd_name != null) {
                            addressinvoicenameaccount.setValue([{
                                id: rs[0].Id,
                                name: rs[0].attributes.bsd_name.value,
                                entityType: rs[0].logicalName
                            }]);
                        }
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdaddressinvoiceaccount, entityNameaddressinvoiceaccount, viewDisplayNameaddressinvoiceaccount, xmladdressinvoiceaccount.join(""), layoutXmladdressinvoiceaccount, true);
                    }
                    if (rs.length == 0) {
                        addressinvoicenameaccount.setValue(null);
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdaddressinvoiceaccount, entityNameaddressinvoiceaccount, viewDisplayNameaddressinvoiceaccount, xmladdressinvoiceaccount.join(""), layoutXmladdressinvoiceaccount, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }

        },
    function (er) {
        console.log(er.message)
    });
        //Thông tin invoice account
        fetch(xmlinvoiceaccount, "account", ["name", "primarycontactid", "telephone1", "accountid"
                                  , "bsd_taxregistration", "fax", "accountnumber", "emailaddress1"
                                  , "bsd_paymentterm", "bsd_paymentmethod", "transactioncurrencyid", "bsd_saletaxgroup"], ["createdon"], false, null,
                                  ["accountid"], ["eq"], [0, invoicenameaccount[0].id, 1]);
        CrmFetchKit.Fetch(xmlinvoiceaccount.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.accountnumber != null) {
                    Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(rs[0].attributes.accountnumber.value);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(rs[0].attributes.bsd_taxregistration.value);
                }

                if (rs[0].attributes.bsd_taxregistration == null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                }
            }
        },
    function (er) {
        console.log(er.message)
    });
    }
    else {
        addressinvoicenameaccount.setValue(null);
        Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(null);
        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
        Xrm.Page.getControl("bsd_addressinvoiceaccount").setDisabled(true);
    }
}
//Description:gan gia tri cho fied ship to address
function set_addressfromcustomerb() {
    Xrm.Page.getControl("bsd_shiptoaddress").setDisabled(false);
    var customerb = Xrm.Page.getAttribute("bsd_partnerscompany").getValue();
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    Xrm.Page.getControl("bsd_shiptoaddress").removePreSearch(presearch_LoadShiptoaddress);
    var xmlshiptoaddress = [];
    var xmlreceiptcustomer = [];
    if (customerb != null) {
        var viewIdshiptoaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameshiptoaddress = "bsd_address";
        var viewDisplayNameshiptoaddress = "Ship To Address";
        fetch(xmlshiptoaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customerb[0].id]);
        var layoutXmlshiptoaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlshiptoaddress.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                fieldshiptoaddress.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.bsd_name.value,
                    entityType: rs[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdshiptoaddress, entityNameshiptoaddress, viewDisplayNameshiptoaddress, xmlshiptoaddress.join(""), layoutXmlshiptoaddress, true);
            }
            else {
                var viewIdreceiptcustomer = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNamereceiptcustomer = "bsd_address";
                var viewDisplayNamereceiptcustomer = "Receipt Customer Address";
                fetch(xmlreceiptcustomer, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customerb[0].id]);
                var layoutXmlreceiptcustomer = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdreceiptcustomer, entityNamereceiptcustomer, viewDisplayNamereceiptcustomer, xmlreceiptcustomer.join(""), layoutXmlreceiptcustomer, true);
            }

        }, function (er) {
            console.log(er.message)
        });
    }
    else {
        clear_LoadShiptoaddress();
        Xrm.Page.getAttribute("bsd_shiptoaddress").setValue(null);
    }
}
//Description:gan gia tri cho cac field address khi form la edit
function set_addressonload() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    Xrm.Page.getControl("bsd_address").removePreSearch(presearch_addresscustomer);
    Xrm.Page.getControl("bsd_contact").removePreSearch(presearch_Contact);
    Xrm.Page.getControl("bsd_addressinvoiceaccount").removePreSearch(presearch_invoiceaddress);
    var xmladdresscustomer = [];
    var xmlcontactcustomer = [];
    var xmladdressinvoiceaccount = [];
    if (customer != null) {
        var viewIdaddresscustomer = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameaddresscustomer = "bsd_address";
        var viewDisplayNameaddresscustomer = "Address Customer";
        fetch(xmladdresscustomer, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                     ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
        var layoutXmladdresscustomer = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='bsd_addressid'>  " +
              "<cell name='bsd_name'   " + "width='200' />  " +
              "<cell name='createdon'    " + "width='100' />  " +
                "<cell name='bsd_purpose'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
        Xrm.Page.getControl("bsd_address").addCustomView(viewIdaddresscustomer, entityNameaddresscustomer, viewDisplayNameaddresscustomer, xmladdresscustomer.join(""), layoutXmladdresscustomer, true);
        var viewIdcontactcustomer = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
        var entityNamecontactcustomer = "contact";
        var viewDisplayNamecontactcustomer = "Contact Customer";
        fetch(xmlcontactcustomer, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                     ["statecode", "parentcustomerid"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
        var layoutXmlcontactcustomer = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='contactid'>  " +
                         "<cell name='fullname'   " + "width='200' />  " +
                         "<cell name='telephone1'    " + "width='100' />  " +
                         "</row>   " +
                      "</grid>   ";
        Xrm.Page.getControl("bsd_contact").addCustomView(viewIdcontactcustomer, entityNamecontactcustomer, viewDisplayNamecontactcustomer, xmlcontactcustomer.join(""), layoutXmlcontactcustomer, true);
    }
    //else {
    //    clear_addresscustomer();
    //    clear_Contact();
    //}
    if (invoicenameaccount != null) {
        var viewIdaddressinvoiceaccount = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameaddressinvoiceaccount = "bsd_address";
        var viewDisplayNameaddressinvoiceaccount = "Address Invoice Account";
        fetch(xmladdressinvoiceaccount, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, invoicenameaccount[0].id]);
        var layoutXmladdressinvoiceaccount = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='bsd_addressid'>  " +
              "<cell name='bsd_name'   " + "width='200' />  " +
              "<cell name='createdon'    " + "width='100' />  " +
                "<cell name='bsd_purpose'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewDisplayNameaddressinvoiceaccount, entityNameaddressinvoiceaccount, viewDisplayNameaddressinvoiceaccount, xmladdressinvoiceaccount.join(""), layoutXmladdressinvoiceaccount, true);
    }
    //else {
    //    clear_invoiceaddress();
    //}
}
//Description:gan gia tri cho field ship to address khi form la edit
function setshiptoaddressonload() {
    var customerb = Xrm.Page.getAttribute("bsd_partnerscompany").getValue();
    var shiptoadress = Xrm.Page.getAttribute("bsd_shiptoaddress").getValue();
    Xrm.Page.getControl("bsd_shiptoaddress").removePreSearch(presearch_LoadShiptoaddress);
    var xmlshiptoaddress = [];
    var xmladdresspartners = [];
    if (customerb != null) {
        var viewIdshiptoaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameshiptoaddress = "bsd_address";
        var viewDisplayNameshiptoaddress = "Ship To Address";
        fetch(xmlshiptoaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customerb[0].id]);
        var layoutXmlshiptoaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";

        CrmFetchKit.Fetch(xmlshiptoaddress.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdshiptoaddress, entityNameshiptoaddress, viewDisplayNameshiptoaddress, xmlshiptoaddress.join(""), layoutXmlshiptoaddress, true);
            }
            else {
                var viewIdaddresspartners = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameaddresspartners = "bsd_address";
                var viewDisplayNameaddresspartners = "Address Partners Company";
                fetch(xmladdresspartners, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                    ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customerb[0].id]);
                var layoutXmladdresspartners = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdaddresspartners, entityNameaddresspartners, viewDisplayNameaddresspartners, xmladdresspartners.join(""), layoutXmladdresspartners, true);
            }

        }, function (er) {
            console.log(er.message)
        });
    }
    //else {
    //    clear_LoadShiptoaddress();
    //}
}
function hide_button() {
    try {
        if (window.parent.document.getElementById("stageBackActionContainer") != null) {
            window.parent.document.getElementById("stageBackActionContainer").style.display = "none";
        }
    } catch (e) { }
    try {
        if (window.parent.document.getElementById("stageBackActionContainer") != null) {
            window.parent.document.getElementById("stageBackActionContainer").style.visibility = "hidden";
        }
    } catch (e) {

    }
    try {
        if (window.parent.document.getElementById("stageAdvanceActionContainer") != null) {
            window.parent.document.getElementById("stageAdvanceActionContainer").style.display = "none";
        }
    } catch (e) {

    }
    try {
        if (window.parent.document.getElementById("stageAdvanceActionContainer") != null) {
            window.parent.document.getElementById("stageAdvanceActionContainer").style.visibility = "hidden";
        }
    } catch (e) {

    }
    try {
        if (window.parent.document.getElementById("stageSetActiveActionContainer") != null) {
            window.parent.document.getElementById("stageSetActiveActionContainer").style.display = "none";
        }
    } catch (e) {

    }
    try {
        if (window.parent.document.getElementById("stageSetActiveActionContainer") != null) {

            window.parent.document.getElementById("stageSetActiveActionContainer").style.visibility = "hidden";
        }
    } catch (e) {

    }
}
function validation() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    if (customer == null) {
        Xrm.Page.getAttribute("bsd_address").setValue(null);
    }
    if (invoicenameaccount == null) {
        Xrm.Page.getAttribute("bsd_addressinvoiceaccount").setValue(null);
    }
}
function set_currentdate() {
    if (Xrm.Page.ui.getFormType() == 1) {
        var currentDateTime = new Date();
        var d = new Date();
        var currentDateTime = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
        Xrm.Page.getAttribute("effectivefrom").setValue(currentDateTime);
        Xrm.Page.getAttribute("bsd_date").setValue(currentDateTime);
    }
}
//End





function clear_LoadShiptoaddress() {
    getControl("bsd_shiptoaddress").addPreSearch(presearch_LoadShiptoaddress);
}
function presearch_LoadShiptoaddress() {
    getControl("bsd_shiptoaddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_addresscustomer() {
    getControl("bsd_address").addPreSearch(presearch_addresscustomer);
}
function presearch_addresscustomer() {
    getControl("bsd_address").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Contact() {
    getControl("bsd_contact").addPreSearch(presearch_Contact);
}
function presearch_Contact() {
    getControl("bsd_contact").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_invoiceaddress() {
    getControl("bsd_addressinvoiceaccount").addPreSearch(presearch_invoiceaddress);
}
function presearch_invoiceaddress() {
    getControl("bsd_addressinvoiceaccount").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_LoadAddressPort() {
    getControl("bsd_addressport").addPreSearch(presearch_LoadAddressPort);
}
function presearch_LoadAddressPort() {
    getControl("bsd_addressport").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

//Button
function BtnActiveQuoteEnableRule() {
    debugger;
    var result;
    //var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var id = Xrm.Page.data.entity.getId();
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var totaltax = Xrm.Page.getAttribute("bsd_totalamount").getValue();
    //stagename = Xrm.Page.data.process.getActiveStage().getName();
    var xmlaccount = [];
    if (id != " ") {
        if (totaltax == null) {
            return false;
        }
        else {
            if (totaltax != 0) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    if (id == " ") {
        return false;
    }
    //return true;
}
function btnSubOrder() {
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
               '   <entity name="salesorder">',
               '     <attribute name="name" />',
               '     <attribute name="customerid" />',
               '     <attribute name="statuscode" />',
               '     <attribute name="totalamount" />',
               '     <attribute name="salesorderid" />',
               '     <order attribute="name" descending="false" />',
               '     <filter type="and">',
               '       <condition attribute="quoteid" operator="eq" uitype="quote" value="' + getId() + '" />',
               '     </filter>',
               '   </entity>',
               ' </fetch>'].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {

        if (rs.length > 0) {
            alert("Đã tạo hợp đồng, không thể tạo suborder");
        } else {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                          '<entity name="quotedetail">',
                            '<attribute name="quotedetailid" />',
                            '<filter type="and">',
                              '<condition attribute="quoteid" operator="eq" uitype="quote" value="' + getId() + '" />',
                              '<condition attribute="bsd_remainingquantity" operator="gt" value="0" />',
                            '</filter>',
                          '</entity>',
                        '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    if (confirm("Are you sure to create new sub order ?")) {
                        ExecuteAction(getId(), "quote", "bsd_Action_QuoteToSubOrder", null, function (result) {
                            if (result != null && result.status != null) {
                                if (result.status == "success") {
                                    reload_page();
                                } else if (result.status == "error") {
                                    alert(result.data);
                                } else {
                                    alert(result.data);
                                }
                            }
                        });
                    }
                } else {
                    alert("Không sản phẩm nào còn dư số lượng để tạo suborder");
                }
            });
        }
    });

}
function BtnSubOrderEnableRule() {
    var statecode = Xrm.Page.getAttribute("statecode").getValue();
    if (statecode == 1 || statecode == 2) {
        return true;
    } else {
        return false;
    }
}