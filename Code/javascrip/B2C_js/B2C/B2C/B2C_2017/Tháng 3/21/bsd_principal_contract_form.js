function AutoLoad() {
    set_DisableWarehouse();
    if (formType() == 2) {
        set_DisableWarehouse(false);
        khachhang_change(false);
        set_require();
        set_defaultaddress();
        get_allbankaccountfromsitebonload();
        filter_bankaccounta();
        //get_warehouseadd();

    } else {
        setValueDefault();
        clear_AddressDeliveryShipping();
        clear_addressaccount();
        creat_principalcontractfromcreate();

    }
    setvalue_sectionBHS();
    //set_hidebutton();
    filter_customerhaveprincipalcontract();
}
function set_require() {
    Xrm.Page.getControl("bsd_code").setDisabled(false);
}
//author:Mr.Phong
function set_hide() {
    if (Xrm.Page.getAttribute("bsd_porter").getValue() == true) {
        Xrm.Page.getAttribute("bsd_portertype").setValue(861450000);
    }
    if (Xrm.Page.getAttribute("bsd_porter").getValue() == false) {
        Xrm.Page.getAttribute("bsd_portertype").setValue(861450001);
    }
}
//Auhthor:Mr Phong
//Description:load thong tin cua khach hang
function set_defaultaddress() {
    var customer = Xrm.Page.getAttribute("bsd_account").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    if (customer != null) {
        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName = "bsd_address";
        var viewDisplayName = "test";
        fetch(xml, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
           ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, customer[0].id]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='bsd_addressid'>  " +
                       "<cell name='bsd_name'   " + "width='200' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                       "<cell name='createdon'    " + "width='100' />  " +
                       "</row>   " +
                    "</grid>   ";
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            }
            else {
                var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName2 = "bsd_address";
                var viewDisplayName2 = "test";
                fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='bsd_addressid'>  " +
                       "<cell name='bsd_name'   " + "width='200' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                       "<cell name='createdon'    " + "width='100' />  " +
                       "</row>   " +
                    "</grid>   ";
                Xrm.Page.getControl("bsd_address").addCustomView(viewId2, entityName2, viewDisplayName2, xml1.join(""), layoutXml2, true);
            }
        },
            function (er) {
                console.log(er.message)
            });
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xml2.push("<entity name='account'>");
        xml2.push("<attribute name='name' />");
        xml2.push("<attribute name='primarycontactid'/>");
        xml2.push("<attribute name='telephone1'/>");
        xml2.push("<attribute name='accountid'/>");
        xml2.push("<order attribute='name' descending='false'/> ");
        xml2.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
        xml2.push("<attribute name='jobtitle'/>");
        xml2.push("<filter type='and'>");
        xml2.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "'/>");
        xml2.push("</filter>");
        xml2.push("</link-entity>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
            if (rs2.length > 0) {
                var viewId1 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityName1 = "contact";
                var viewDisplayName1 = "test";
                xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml4.push("<entity name='contact'>");
                xml4.push("<attribute name='fullname'/>");
                xml4.push("<attribute name='telephone1'/>")
                xml4.push("<attribute name='contactid'/>");
                xml4.push("<order attribute='fullname' descending='true' />");
                xml4.push("<filter type='and'>");
                xml4.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
                xml4.push("</filter>");
                xml4.push("</entity>");
                xml4.push("</fetch>");
                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_nguoidaidienb").addCustomView(viewId1, entityName1, viewDisplayName1, xml4.join(""), layoutXml1, true);
            }
            if (rs2.length == 0) {
                var viewId1 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityName1 = "contact";
                var viewDisplayName1 = "test";
                xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml4.push("<entity name='contact'>");
                xml4.push("<attribute name='fullname'/>");
                xml4.push("<attribute name='telephone1'/>")
                xml4.push("<attribute name='contactid'/>");
                xml4.push("<order attribute='fullname' descending='true' />");
                xml4.push("<filter type='and'>");
                xml4.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
                xml4.push("</filter>");
                xml4.push("</entity>");
                xml4.push("</fetch>");
                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_nguoidaidienb").addCustomView(viewId1, entityName1, viewDisplayName1, xml4.join(""), layoutXml1, true);
            }
        },
            function (er) {
                console.log(er.message)
            });
    }
}
//Auhthor:Mr.Phong
//Desciption:hide button add subgrid
function set_hidebutton() {
    window.parent.document.getElementById("stageBackActionContainer").style.display = "none";
    window.parent.document.getElementById("stageBackActionContainer").style.visibility = "hidden";
    window.parent.document.getElementById("stageAdvanceActionContainer").style.display = "none";
    window.parent.document.getElementById("stageAdvanceActionContainer").style.visibility = "hidden";
    window.parent.document.getElementById("stageSetActiveActionContainer").style.display = "none";
    window.parent.document.getElementById("stageSetActiveActionContainer").style.visibility = "hidden";
}
//Author:Mr.Phong
//Description:lay thong tin tu nguoi dai dien ben b
function get_thongtinchucvu() {
    var present = Xrm.Page.getAttribute("bsd_nguoidaidienb").getValue();
    var xml = [];
    if (present != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='contact'>");
        xml.push("<attribute name='fullname'/>");
        xml.push("<attribute name='telephone1'/>")
        xml.push("<attribute name='contactid'/>");
        xml.push("<attribute name='jobtitle'/>");
        xml.push("<order attribute='fullname' descending='true' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='contactid' operator='eq' uitype='contact'  value='" + present[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.jobtitle != null) {
                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(rs[0].attributes.jobtitle.value);
                }
            }
        },
         function (er) {
             console.log(er.message)
         });
    }
    if (present == null) {
        Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
    }
}
//Author:Mr.Phong
//Description:create principal contract from create account
function creat_principalcontractfromcreate() {
    var param = Xrm.Page.context.getQueryStringParameters();
    var param1 = param["regarding_Id"];
    if (param1 != null) {
        var source = param["regarding_Id"].toString();
        var fieldcustomer = Xrm.Page.getAttribute("bsd_account");
        var contactdefault = Xrm.Page.getAttribute("bsd_nguoidaidienb");
        var fielddefault = Xrm.Page.getAttribute("bsd_address");
        var fieldpaymentterm = Xrm.Page.getAttribute("bsd_paymentterms");
        var fieldpaymentmethod = Xrm.Page.getAttribute("bsd_paymentmethod");

        var fieldbankaccountcustomer = Xrm.Page.getAttribute("bsd_bankaccountb");
        var fieldbrandcustomer = Xrm.Page.getAttribute("bsd_brandb");
        var fieldbankaccountnumbercustomer = Xrm.Page.getAttribute("bsd_bankaccountnumberb");
        var fieldbankgroupcustomer = Xrm.Page.getAttribute("bsd_bankgroupb");
        Xrm.Page.getControl("bsd_bankaccountb").removePreSearch(presearch_bankaccount);
        Xrm.Page.getControl("bsd_address").removePreSearch(presearch_addressaccount);

        var xml = [];
        var xml1 = [];
        var xml2 = [];
        var xml3 = [];
        var xml4 = [];
        var xml5 = [];
        var xml6 = [];
        var xml7 = [];
        var xml8 = [];
        if (source != null) {
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
            xml.push("<entity name='account'>");
            xml.push("<attribute name='name'/>");
            xml.push("<attribute name='primarycontactid'/>");
            xml.push("<attribute name='telephone1'/>");
            xml.push("<attribute name='accountid'/>");
            xml.push("<attribute name='websiteurl'/>");
            xml.push("<attribute name='bsd_taxregistration'/>");
            xml.push("<attribute name='fax'/>");
            xml.push("<attribute name='emailaddress1'/>");
            xml.push("<attribute name='accountnumber'/>");
            xml.push("<order attribute='name' descending='false' />");
            xml.push("<filter type='and'>");
            xml.push("<condition attribute='accountid' operator='eq' uitype='account' value='" + source + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
                if (rs.length > 0) {
                    if (rs[0].attributes.accountnumber != null) {
                        Xrm.Page.getAttribute("bsd_accountnumberb").setValue(rs[0].attributes.accountnumber.value);
                    }
                    if (rs[0].attributes.bsd_taxregistration != null) {
                        Xrm.Page.getAttribute("bsd_taxregistrationb").setValue(rs[0].attributes.bsd_taxregistration.value);
                    }
                    if (rs[0].attributes.telephone1 != null) {
                        Xrm.Page.getAttribute("bsd_mainphoneb").setValue(rs[0].attributes.telephone1.value);
                    }
                    if (rs[0].attributes.fax != null) {
                        Xrm.Page.getAttribute("bsd_faxb").setValue(rs[0].attributes.fax.value);
                    }
                    if (rs[0].attributes.websiteurl != null) {
                        Xrm.Page.getAttribute("bsd_websiteb").setValue(rs[0].attributes.websiteurl.value);
                    }
                    if (rs[0].attributes.emailaddress1 != null) {
                        Xrm.Page.getAttribute("bsd_emailb").setValue(rs[0].attributes.emailaddress1.value);
                    }
                    fieldcustomer.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
            },
           function (er) {
               console.log(er.message)
           });
            var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
            var entityName = "bsd_address";
            var viewDisplayName = "test";
            fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
            ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, source]);
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_addressid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='bsd_purpose'    " + "width='100' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
            Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
            CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                if (rs1.length > 0) {
                    var address = rs1[0].attributes.bsd_name;
                    if (address != null) {
                        fielddefault.setValue([{
                            id: rs1[0].Id,
                            name: rs1[0].attributes.bsd_name.value,
                            entityType: rs1[0].logicalName
                        }]);
                    }
                }
                if (rs.length == 0) {
                    var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                    var entityName2 = "bsd_address";
                    var viewDisplayName2 = "test";
                    xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                    xml2.push("<entity name='bsd_address'>");
                    xml2.push("<attribute name='bsd_addressid' />");
                    xml2.push("<attribute name='bsd_name' />");
                    xml2.push("<attribute name='createdon'/>");
                    xml2.push("<attribute name='bsd_purpose' />");
                    xml2.push("<order attribute='bsd_name' descending='false' />");
                    xml2.push("<filter type='and' >");
                    xml2.push("<condition attribute='bsd_account' operator='eq' value='" + source + "' />");
                    xml2.push("<condition attribute='statecode' operator='eq'  value='0' />");
                    xml2.push("</filter>");
                    xml2.push("</entity>");
                    xml2.push("</fetch>");
                    var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                       "<row name='result'  " + "id='bsd_addressid'>  " +
                                       "<cell name='bsd_name'    " + "width='200' />  " +
                                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                                        "<cell name='createdon'    " + "width='100' />  " +
                                       "</row>   " +
                                    "</grid>   ";
                    CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                        if (rs2.length > 0) {
                            Xrm.Page.getControl("bsd_address").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
                        }
                        if (rs2.length == 0) {
                            fielddefault.setValue(null);
                            clear_addressaccount();
                        }
                    },
                    function (er) {
                        console.log(er.message)
                    });
                }
            },
           function (er) {
               console.log(er.message)
           });
            xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
            xml3.push("<entity name='account'>");
            xml3.push("<attribute name='name' />");
            xml3.push("<attribute name='primarycontactid'/>");
            xml3.push("<attribute name='telephone1'/>");
            xml3.push("<attribute name='accountid'/>");
            xml3.push("<order attribute='name' descending='false'/> ");
            xml3.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
            xml3.push("<attribute name='jobtitle'/>");
            xml3.push("<filter type='and'>");
            xml3.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + source + "'/>");
            xml3.push("</filter>");
            xml3.push("</link-entity>");
            xml3.push("</entity>");
            xml3.push("</fetch>");
            CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                if (rs3.length > 0) {
                    if (rs3[0].attributes.primarycontactid != null) {
                        contactdefault.setValue([{
                            id: rs3[0].attributes.primarycontactid.guid,
                            name: rs3[0].attributes.primarycontactid.name,
                            entityType: 'contact'
                        }]);
                        xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                        xml4.push("<entity name='contact'>");
                        xml4.push("<attribute name='fullname'/>");
                        xml4.push("<attribute name='telephone1'/>")
                        xml4.push("<attribute name='contactid'/>");
                        xml4.push("<attribute name='jobtitle'/>");
                        xml4.push("<order attribute='fullname' descending='true' />");
                        xml4.push("<filter type='and'>");
                        xml4.push("<condition attribute='fullname' operator='eq'  value='" + rs3[0].attributes.primarycontactid.name + "' />");
                        xml4.push("</filter>");
                        xml4.push("</entity>");
                        xml4.push("</fetch>");
                        CrmFetchKit.Fetch(xml4.join(""), true).then(function (rs4) {
                            if (rs4.length > 0) {
                                if (rs4[0].attributes.jobtitle != null) {
                                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(rs4[0].attributes.jobtitle.value);
                                }
                            }
                        },
                function (er) {
                    console.log(er.message)
                });
                    }
                    var viewId3 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                    var entityName3 = "contact";
                    var viewDisplayName3 = "test";
                    xml5.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                    xml5.push("<entity name='contact'>");
                    xml5.push("<attribute name='fullname'/>");
                    xml5.push("<attribute name='telephone1'/>")
                    xml5.push("<attribute name='contactid'/>");
                    xml5.push("<order attribute='fullname' descending='true' />");
                    xml5.push("<filter type='and'>");
                    xml5.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + source + "' />");
                    xml5.push("</filter>");
                    xml5.push("</entity>");
                    xml5.push("</fetch>");
                    var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                     "<row name='result'  " + "id='contactid'>  " +
                                     "<cell name='fullname'   " + "width='200' />  " +
                                     "<cell name='telephone1'    " + "width='100' />  " +
                                     "</row>   " +
                                  "</grid>   ";
                    Xrm.Page.getControl("bsd_nguoidaidienb").addCustomView(viewId3, entityName3, viewDisplayName3, xml5.join(""), layoutXml3, true);
                }
                if (rs3.length == 0) {
                    var viewId4 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                    var entityName4 = "contact";
                    var viewDisplayName4 = "test";
                    xml6.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                    xml6.push("<entity name='contact'>");
                    xml6.push("<attribute name='fullname'/>");
                    xml6.push("<attribute name='telephone1'/>")
                    xml6.push("<attribute name='contactid'/>");
                    xml6.push("<order attribute='fullname' descending='true' />");
                    xml6.push("<filter type='and'>");
                    xml6.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + source + "' />");
                    xml6.push("</filter>");
                    xml6.push("</entity>");
                    xml6.push("</fetch>");
                    var layoutXml4 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                     "<row name='result'  " + "id='contactid'>  " +
                                     "<cell name='fullname'   " + "width='200' />  " +
                                     "<cell name='telephone1'    " + "width='100' />  " +
                                     "</row>   " +
                                  "</grid>   ";
                    Xrm.Page.getControl("bsd_nguoidaidienb").addCustomView(viewId4, entityName4, viewDisplayName4, xml6.join(""), layoutXml4, true);
                }
            },
                function (er) {
                    console.log(er.message)
                });
            xml7.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml7.push("<entity name='account'>");
            xml7.push("<attribute name='name'/>");
            xml7.push("<attribute name='primarycontactid'/>");
            xml7.push("<attribute name='telephone1'/>");
            xml7.push("<attribute name='accountid'/>");
            xml7.push("<attribute name='bsd_paymentterm'/>");
            xml7.push("<attribute name='bsd_paymentmethod'/>");
            xml7.push("<order attribute='name' descending='false'/>");
            xml7.push("<filter type='and'>");
            xml7.push("<condition attribute='accountid' operator='eq' uitype='account' value='" + source + "'/>");
            xml7.push("</filter>");
            xml7.push("</entity>");
            xml7.push("</fetch>");
            CrmFetchKit.Fetch(xml7.join(""), true).then(function (rs7) {
                if (rs7.length > 0) {
                    if (rs7[0].attributes.bsd_paymentterm != null) {
                        fieldpaymentterm.setValue([{
                            id: rs7[0].attributes.bsd_paymentterm.guid,
                            name: rs7[0].attributes.bsd_paymentterm.name,
                            entityType: 'bsd_paymentterm'
                        }]);
                    }
                    if (rs7[0].attributes.bsd_paymentmethod != null) {
                        fieldpaymentmethod.setValue([{
                            id: rs7[0].attributes.bsd_paymentmethod.guid,
                            name: rs7[0].attributes.bsd_paymentmethod.name,
                            entityType: 'bsd_methodofpayment'
                        }]);
                    }
                }
            },
                function (er) {
                    console.log(er.message)
                });
            var viewId8 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
            var entityName8 = "bsd_bankaccount";
            var viewDisplayName8 = "Bank Account View";
            fetch(xml8, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
                ["bsd_account", "statecode"], ["eq", "eq"], [0, source, 1, "0"]);
            var layoutXml8 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
            "<row name='result'  " + "id='bsd_bankaccountid'>  " +
            "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
            "<cell name='bsd_brand'    " + "width='100' />  " +
            "<cell name='bsd_bankaccount'    " + "width='100' />  " +
            "<cell name='bsd_account'    " + "width='100' />  " +
            "<cell name='bsd_bankgroup'    " + "width='100' />  " +
            "</row>   " +
            "</grid>   ";
            CrmFetchKit.Fetch(xml8.join(""), true).then(function (rs8) {
                if (rs8.length > 0) {
                    if (rs8[0].attributes.bsd_name != null) {
                        fieldbankaccountcustomer.setValue([{
                            id: rs8[0].Id,
                            name: rs8[0].attributes.bsd_name.value,
                            entityType: rs8[0].logicalName
                        }]);
                    }
                    else if (rs[0].attributes.bsd_paymentmethod == null) {
                        fieldbankaccountcustomer.setValue(null);
                    }
                    if (rs8[0].attributes.bsd_brand != null) {
                        fieldbrandcustomer.setValue(rs8[0].attributes.bsd_brand.name);
                    }
                    else if (rs[0].attributes.bsd_brand == null) {
                        fieldbrandcustomer.setValue(null);
                    }
                    if (rs8[0].attributes.bsd_bankaccount != null) {
                        fieldbankaccountnumbercustomer.setValue(rs8[0].attributes.bsd_bankaccount.name);
                    }
                    else if (rs[0].attributes.bsd_bankaccount == null) {
                        fieldbankaccountnumbercustomer.setValue(null);
                    }
                    if (rs8[0].attributes.bsd_bankgroup != null) {
                        fieldbankgroupcustomer.setValue([{
                            id: rs8[0].attributes.bsd_bankgroup.guid,
                            name: rs8[0].attributes.bsd_bankgroup.value,
                            entityType: rs8[0].attributes.bsd_bankgroup.logicalname
                        }]);
                    }
                    else if (rs[0].attributes.bsd_bankgroup == null) {
                        fieldbankgroupcustomer.setValue(null);
                    }
                    Xrm.Page.getControl("bsd_bankaccountb").addCustomView(viewId8, entityName8, viewDisplayName8, xml8.join(""), layoutXml8, true);
                }
                else {
                    fieldbankaccountcustomer.setValue(null);
                    fieldbrandcustomer.setValue(null);
                    fieldbankaccountnumbercustomer.setValue(null);
                    fieldbankgroupcustomer.setValue(null);
                    clear_bankaccount();
                }
            },
                function (er) {
                    console.log(er.message)
                });
        }
    }
}
//Author:Mr.Phong
//Description:filter bank account theo ben A
function filter_bankaccounta() {
    var trader = Xrm.Page.getAttribute("bsd_renter").getValue();
    if (trader != null) {
        var xml = [];
        var viewId = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
        var entityName = "bsd_bankaccount";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_bankaccount'>");
        xml.push("<attribute name='bsd_name'/>");
        xml.push("<attribute name='createdon'/>");
        xml.push("<attribute name='bsd_brand'/>");
        xml.push("<attribute name='bsd_bankaccount'/>");
        xml.push("<attribute name='bsd_account'/>");
        xml.push("<attribute name='bsd_bankaccountid'/>");
        xml.push("<attribute name='bsd_bankgroup'/>");
        xml.push("<order attribute='bsd_name' descending='true' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='" + trader[0].id + "' />");
        xml.push("<condition attribute='statecode' operator='eq'  value='0' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
            "<row name='result'  " + "id='bsd_bankaccountid'>  " +
            "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
            "<cell name='bsd_brand'    " + "width='100' />  " +
            "<cell name='bsd_bankaccount'    " + "width='100' />  " +
            "<cell name='bsd_account'    " + "width='100' />  " +
            "<cell name='bsd_bankgroup'    " + "width='100' />  " +
            "</row>   " +
            "</grid>   ";
        Xrm.Page.getControl("bsd_bankaccounta").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    }
}
//Author:Mr.Phong
//Description:filter bank account theo ben b
function get_allbankaccountfromsiteboncreate() {
    if (Xrm.Page.ui.getFormType() == 1) {
        var bankaccount = Xrm.Page.getAttribute("bsd_bankaccountb");
        var param = Xrm.Page.context.getQueryStringParameters();
        var param1 = param["regarding_Id"];
        var bankgroup = Xrm.Page.getAttribute("bsd_bankgroupb");
        var bankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumberb");
        var brand = Xrm.Page.getAttribute("bsd_brandb");
        if (param1 != null) {
            var source = param["regarding_Id"].toString();
            var xml = [];
            try {
                if (source != null) {
                    var viewId = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
                    var entityName = "bsd_bankaccount";
                    var viewDisplayName = "test";
                    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                    xml.push("<entity name='bsd_bankaccount'>");
                    xml.push("<attribute name='bsd_name'/>");
                    xml.push("<attribute name='createdon'/>");
                    xml.push("<attribute name='bsd_brand'/>");
                    xml.push("<attribute name='bsd_bankaccount'/>");
                    xml.push("<attribute name='bsd_account'/>");
                    xml.push("<attribute name='bsd_bankaccountid'/>");
                    xml.push("<attribute name='bsd_bankgroup'/>");
                    xml.push("<order attribute='bsd_name' descending='true' />");
                    xml.push("<filter type='and'>");
                    xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='" + source + "' />");
                    xml.push("<condition attribute='statecode' operator='eq'  value='0' />");
                    xml.push("</filter>");
                    xml.push("</entity>");
                    xml.push("</fetch>");
                    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
       "<row name='result'  " + "id='bsd_bankaccountid'>  " +
       "<cell name='bsd_name'   " + "width='200' />  " +
       "<cell name='createdon'    " + "width='100' />  " +
       "<cell name='bsd_brand'    " + "width='100' />  " +
       "<cell name='bsd_bankaccount'    " + "width='100' />  " +
       "<cell name='bsd_account'    " + "width='100' />  " +
       "</row>   " +
       "</grid>   ";

                    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                        if (rs.length > 0) {
                            if (rs[0].attributes.bsd_name != null) {
                                bankaccount.setValue([{
                                    id: rs[0].Id,
                                    name: rs[0].attributes.bsd_name.value,
                                    entityType: rs[0].logicalName
                                }]);
                            }
                            if (rs[0].attributes.bsd_bankgroup != null) {
                                bankgroup.setValue([{
                                    id: rs[0].attributes.bsd_bankgroup.guid,
                                    name: rs[0].attributes.bsd_bankgroup.name,
                                    entityType: rs[0].attributes.bsd_bankgroup.logicalName
                                }]);
                            }
                            if (rs[0].attributes.bsd_bankaccount != null) {
                                bankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                            }
                            if (rs[0].attributes.bsd_brand != null) {
                                brand.setValue(rs[0].attributes.bsd_brand.value);
                            }
                            Xrm.Page.getControl("bsd_bankaccountb").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
                        }
                        if (rs.length == 0) {
                            bankaccount.setValue(null);
                            bankgroup.setValue(null);
                            bankaccountnumber.setValue(null);
                            brand.setValue(null);
                            Xrm.Page.getControl("bsd_bankaccountb").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
                        }
                    },
                    function (er) {
                        console.log(er.message)
                    });
                }
            }
            catch (e) {
            }
        }
    }
}
//Author:Mr.Phong
//Description:filter bank account theo ben b
function get_allbankaccountfromsitebonload() {
    var account = Xrm.Page.getAttribute("bsd_account").getValue();
    if (account != null) {
        var xml = [];
        var viewId = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
        var entityName = "bsd_bankaccount";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_bankaccount'>");
        xml.push("<attribute name='bsd_name'/>");
        xml.push("<attribute name='createdon'/>");
        xml.push("<attribute name='bsd_brand'/>");
        xml.push("<attribute name='bsd_bankaccount'/>");
        xml.push("<attribute name='bsd_account'/>");
        xml.push("<attribute name='bsd_bankaccountid'/>");
        xml.push("<attribute name='bsd_bankgroup'/>");
        xml.push("<order attribute='bsd_name' descending='true' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='" + account[0].id + "' />");
        xml.push("<condition attribute='statecode' operator='eq'  value='0' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
       "<row name='result'  " + "id='bsd_bankaccountid'>  " +
       "<cell name='bsd_name'   " + "width='200' />  " +
       "<cell name='createdon'    " + "width='100' />  " +
       "<cell name='bsd_brand'    " + "width='100' />  " +
       "<cell name='bsd_bankaccount'    " + "width='100' />  " +
       "<cell name='bsd_account'    " + "width='100' />  " +
       "</row>   " +
       "</grid>   ";
        Xrm.Page.getControl("bsd_bankaccountb").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    }
}
//Author:Mr.Phong
//Description:filter account have principal contract
function filter_customerhaveprincipalcontract() {
    var a = [];
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='bsd_principalcontract'>");
    xml.push("<attribute name='bsd_principalcontractid'/>");
    xml.push("<attribute name='bsd_name' />");
    xml.push("<attribute name='createdon'/>");
    xml.push("<attribute name='bsd_account'/>");
    xml.push("<order attribute='bsd_name' descending='false' />");
    xml.push("</entity>");
    xml.push("</fetch>");
    CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
        if (rs.length > 0) {
            for (var i = 0; i < rs.length; i++) {
                if (rs[i].attributes.bsd_account != null) {
                    a.push(rs[i].attributes.bsd_account.guid);
                }
            }
            var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
            var entityName = "account";
            var viewDisplayName = "test";
            xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml1.push("<entity name='account'>");
            xml1.push("<attribute name='name'/>");
            xml1.push("<attribute name='primarycontactid' />");
            xml1.push("<attribute name='telephone1'/>");
            xml1.push("<attribute name='accountid'/>");
            xml1.push("<order attribute='name' descending='false' />");
            xml1.push("<filter type='and'>");
            xml1.push("<condition attribute='bsd_accounttype' operator='in'>");
            xml1.push("<value>");
            xml1.push("861450000");
            xml1.push("</value>");
            xml1.push("<value>");
            xml1.push("100000000");
            xml1.push("</value>");
            xml1.push("</condition>");
            xml1.push("<condition attribute='statecode' operator='eq' value='0'/>");
            xml1.push("<condition attribute='accountid' operator='not-in'>");
            for (var k = 0; k < a.length; k++) {
                xml1.push("<value uitype='account'>" + '{' + a[k] + '}' + "");
                xml1.push("</value>");
            }
            xml1.push("</condition>");
            xml1.push("</filter>");
            xml1.push("</entity>");
            xml1.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                   "<row name='result'  " + "id='accountid'>  " +
                   "<cell name='name'    " + "width='200' />  " +
                    "<cell name='telephone1'    " + "width='100' />  " +
                     "<cell name='primarycontactid'    " + "width='100' />  " +
                   "</row>   " +
                "</grid>   ";
            Xrm.Page.getControl("bsd_account").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
        }
        else {
            var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
            var entityName = "account";
            var viewDisplayName = "test";
            xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml1.push("<entity name='account'>");
            xml1.push("<attribute name='name'/>");
            xml1.push("<attribute name='primarycontactid' />");
            xml1.push("<attribute name='telephone1'/>");
            xml1.push("<attribute name='accountid'/>");
            xml1.push("<order attribute='name' descending='false' />");
            xml1.push("<filter type='and'>");
            xml1.push("<condition attribute='bsd_accounttype' operator='in'>");
            xml1.push("<value>");
            xml1.push("861450000");
            xml1.push("</value>");
            xml1.push("<value>");
            xml1.push("100000000");
            xml1.push("</value>");
            xml1.push("</condition>");
            xml1.push("<condition attribute='statecode' operator='eq' value='0'/>");
            xml1.push("</filter>");
            xml1.push("</entity>");
            xml1.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='accountid'>  " +
                  "<cell name='name'    " + "width='200' />  " +
                   "<cell name='telephone1'    " + "width='100' />  " +
                    "<cell name='primarycontactid'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>   ";
            Xrm.Page.getControl("bsd_account").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
        }
    },
    function (er) {
        console.log(er.message)
    });

}
//Author:Mr.Phong
//Description:load bank account number and bank group from bank acccount site b
function get_bankaccountnumberandbankgroupb() {
    var account = Xrm.Page.getAttribute("bsd_account").getValue();
    var bankaccount = Xrm.Page.getAttribute("bsd_bankaccountb").getValue();
    var bankgroup = Xrm.Page.getAttribute("bsd_bankgroupb");
    var bankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumberb");
    var brand = Xrm.Page.getAttribute("bsd_brandb");
    var xml = [];
    if (bankaccount != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_bankaccount'>");
        xml.push("<attribute name='bsd_name'/>");
        xml.push("<attribute name='createdon'/>");
        xml.push("<attribute name='bsd_brand'/>");
        xml.push("<attribute name='bsd_bankaccount'/>");
        xml.push("<attribute name='bsd_account'/>");
        xml.push("<attribute name='bsd_bankaccountid'/>");
        xml.push("<attribute name='bsd_bankgroup'/>");
        xml.push("<order attribute='bsd_name' descending='true' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='bsd_bankaccountid' operator='eq' uitype='bsd_bankaccount' value='" + bankaccount[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_bankgroup != null) {
                    bankgroup.setValue([{
                        id: rs[0].attributes.bsd_bankgroup.guid,
                        name: rs[0].attributes.bsd_bankgroup.name,
                        entityType: rs[0].attributes.bsd_bankgroup.logicalName
                    }]);
                }
                if (rs[0].attributes.bsd_bankaccount != null) {
                    bankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                }
                if (rs[0].attributes.bsd_brand != null) {
                    brand.setValue(rs[0].attributes.bsd_brand.value);
                }
            }
        },
      function (er) {
          console.log(er.message)
      });
    }
    else {
        bankgroup.setValue(null);
        bankaccountnumber.setValue(null);
        brand.setValue(null);
    }
}
//Author:Mr.Phong
//Description:gan gia tri cho section BHS
function setvalue_sectionBHS() {
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    var xml7 = [];
    var xml8 = [];

    var fieldrenter = Xrm.Page.getAttribute("bsd_renter");
    var fieldaddressrenter = Xrm.Page.getAttribute("bsd_addressrenter");
    var fieldbankaccountrenter = Xrm.Page.getAttribute("bsd_bankaccounta");
    var fieldbrandrenter = Xrm.Page.getAttribute("bsd_branda");
    var fieldbankaccountnumberrenter = Xrm.Page.getAttribute("bsd_bankaccountnumbera");
    var fieldbankgroupewnter = Xrm.Page.getAttribute("bsd_bankgroupa");
    var fieldcontactrenter = Xrm.Page.getAttribute("bsd_nguoidaidiena");

    Xrm.Page.getControl("bsd_renter").removePreSearch(presearch_Loadtrader);
    Xrm.Page.getControl("bsd_bankaccounta").removePreSearch(presearch_LoadBankAccountTrader);
    Xrm.Page.getControl("bsd_nguoidaidiena").removePreSearch(presearch_Loadrepresentative);

    var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    var entityName = "account";
    var viewDisplayName = "Account View";
    fetch(xml, "account", ["accountid", "name", "primarycontactid", "telephone1", "bsd_taxregistration", "fax"], ["name"], false, null,
        ["bsd_accounttype", "statecode"], ["eq", "eq"], [0, "861450005", 1, "0"]);
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='accountid'>  " +
              "<cell name='name'   " + "width='200' />  " +
             "<cell name='createdon'    " + "width='100' />  " +
              "<cell name='primarycontactid'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
        if (rs.length > 0) {
            if (rs[0].attributes.name != null) {
                fieldrenter.setValue([{
                    id: '{' + rs[0].Id.toUpperCase() + '}',
                    name: rs[0].attributes.name.value,
                    entityType: rs[0].logicalName
                }]);
            }
            else {
                fieldrenter.setValue(null);
            }
            if (rs[0].attributes.telephone1 != null) {
                Xrm.Page.getAttribute("bsd_phonerenter").setValue(rs[0].attributes.telephone1.value);
            }
            else {
                Xrm.Page.getAttribute("bsd_phonerenter").setValue(null);
            }
            if (rs[0].attributes.fax != null) {
                Xrm.Page.getAttribute("bsd_faxrenter").setValue(rs[0].attributes.fax.value);
            }
            else {
                Xrm.Page.getAttribute("bsd_faxrenter").setValue(null);
            }
            if (rs[0].attributes.bsd_taxregistration != null) {
                Xrm.Page.getAttribute("bsd_taxregistrationrenter").setValue(rs[0].attributes.bsd_taxregistration.value);
            }
            else {
                Xrm.Page.getAttribute("bsd_taxregistrationrenter").setValue(null);
            }
            Xrm.Page.getControl("bsd_renter").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            //Address renter
            var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
            var entityName1 = "bsd_address";
            var viewDisplayName1 = "Address View";
            fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, rs[0].Id]);
            var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='bsd_addressid'>  " +
                       "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_account'    " + "width='100' />  " +
                       "</row>   " +
                    "</grid>   ";
            CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                if (rs1.length > 0) {
                    if (rs1[0].attributes.bsd_name != null) {
                        fieldaddressrenter.setValue([{
                            id: '{' + rs1[0].Id.toUpperCase() + '}',
                            name: rs1[0].attributes.bsd_name.value,
                            entityType: rs1[0].logicalName
                        }]);
                    }
                    else {
                        fieldaddressrenter.setValue(null);
                    }
                    Xrm.Page.getControl("bsd_addressrenter").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                }
                else {
                    var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                    var entityName2 = "bsd_address";
                    var viewDisplayName2 = "Address View";
                    fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                   ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, rs[0].Id]);
                    var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_addressid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";

                    CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                        if (rs2.length > 0) {
                            if (rs2[0].attributes.bsd_name != null) {
                                fieldaddressrenter.setValue([{
                                    id: '{' + rs2[0].Id.toUpperCase() + '}',
                                    name: rs2[0].attributes.bsd_name.value,
                                    entityType: rs2[0].logicalName
                                }]);
                            }
                            else {
                                fieldaddressrenter.setValue(null);
                            }
                            Xrm.Page.getControl("bsd_addresstrader").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
                        }
                        else {
                            clear_LoadAddressTrader();
                        }
                    },
                function (er) {
                    console.log(er.message)
                });
                }
            },
               function (er) {
                   console.log(er.message)
               });
            //Bank Account renter
            var viewId3 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
            var entityName3 = "bsd_bankaccount";
            var viewDisplayName3 = "Bank Account View";
            fetch(xml3, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
                ["bsd_account", "statecode"], ["eq", "eq"], [0, rs[0].Id, 1, "0"]);
            var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
            "<row name='result'  " + "id='bsd_bankaccountid'>  " +
            "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
            "<cell name='bsd_brand'    " + "width='100' />  " +
            "<cell name='bsd_bankaccount'    " + "width='100' />  " +
            "<cell name='bsd_account'    " + "width='100' />  " +
            "<cell name='bsd_bankgroup'    " + "width='100' />  " +
            "</row>   " +
            "</grid>   ";
            CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                if (rs3.length > 0) {
                    if (rs3[0].attributes.bsd_name != null) {
                        fieldbankaccountrenter.setValue([{
                            id: '{' + rs3[0].Id.toUpperCase() + '}',
                            name: rs3[0].attributes.bsd_name.value,
                            entityType: rs3[0].logicalName
                        }]);
                    }
                    else {
                        fieldbankaccountrenter.setValue(null);
                    }
                    if (rs3[0].attributes.bsd_brand != null) {
                        fieldbrandrenter.setValue(rs3[0].attributes.bsd_brand.value);
                    }
                    else {
                        fieldbrandrenter.setValue(null);
                    }
                    if (rs3[0].attributes.bsd_bankaccount != null) {
                        fieldbankaccountnumberrenter.setValue(rs3[0].attributes.bsd_bankaccount.value);
                    }
                    else {
                        fieldbankaccountnumberrenter.setValue(null);
                    }
                    if (rs3[0].attributes.bsd_bankgroup != null) {
                        fieldbankgroupewnter.setValue([{
                            id: '{' + rs3[0].attributes.bsd_bankgroup.guid.toUpperCase() + '}',
                            name: rs3[0].attributes.bsd_bankgroup.name,
                            entityType: 'bsd_bankgroup'
                        }]);
                    }
                    else {
                        fieldbankgroupewnter.setValue(null);
                    }
                    Xrm.Page.getControl("bsd_bankaccounta").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
                }
                else {
                    fieldbankaccountrenter.setValue(null);
                    fieldbrandrenter.setValue(null);
                    fieldbrandrenter.setValue(null);
                    fieldbankgroupewnter.setValue(null);
                    clear_LoadBankAccountTrader();
                }
            },
                function (er) {
                    console.log(er.message)
                });
            //Nguoi dai dien renter
            xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
            xml4.push("<entity name='account'>");
            xml4.push("<attribute name='name' />");
            xml4.push("<attribute name='primarycontactid'/>");
            xml4.push("<attribute name='telephone1'/>");
            xml4.push("<attribute name='accountid'/>");
            xml4.push("<order attribute='name' descending='false'/> ");
            xml4.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
            xml4.push("<attribute name='jobtitle'/>");
            xml4.push("<filter type='and'>");
            xml4.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + rs[0].Id + "'/>");
            xml4.push("</filter>");
            xml4.push("</link-entity>");
            xml4.push("</entity>");
            xml4.push("</fetch>");
            CrmFetchKit.Fetch(xml4.join(""), true).then(function (rs4) {
                if (rs4.length > 0) {
                    fieldcontactrenter.setValue([{
                        id: '{' + rs4[0].attributes.primarycontactid.guid.toUpperCase() + '}',
                        name: rs4[0].attributes.primarycontactid.name,
                        entityType: 'contact'
                    }]);
                    var viewId4 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                    var entityName4 = "contact";
                    var viewDisplayName4 = "Contact View";
                    fetch(xml8, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                       ["parentcustomerid", "statecode"], ["eq", "eq"], [0, rs[0].Id, 1, "0"]);
                    var layoutXml4 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='contactid'>  " +
                             "<cell name='fullname'   " + "width='200' />  " +
                             "<cell name='telephone1'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                    Xrm.Page.getControl("bsd_nguoidaidiena").addCustomView(viewId4, entityName4, viewDisplayName4, xml8.join(""), layoutXml4, true);
                    fetch(xml5, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                    ["contactid"], ["eq"], [0, rs4[0].attributes.primarycontactid.guid, 1]);
                    CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                        if (rs5.length > 0) {
                            Xrm.Page.getAttribute("bsd_jobtitlea").setValue(rs5[0].attributes.jobtitle.value);
                        }
                        else {
                            Xrm.Page.getAttribute("bsd_jobtitlea").setValue(null);
                        }
                    },
                function (er) {
                    console.log(er.message)
                });
                }
                else {
                    var viewId6 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                    var entityName6 = "contact";
                    var viewDisplayName6 = "test";
                    fetch(xml6, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                        ["parentcustomerid", "statecode"], ["eq", "eq"], [0, rs[0].Id, 1, "0"]);
                    var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                     "<row name='result'  " + "id='contactid'>  " +
                                     "<cell name='fullname'   " + "width='200' />  " +
                                     "<cell name='telephone1'    " + "width='100' />  " +
                                     "</row>   " +
                                  "</grid>   ";
                    CrmFetchKit.Fetch(xml6.join(""), true).then(function (rs6) {
                        if (rs6.length > 0) {
                            fieldcontactrenter.setValue([{
                                id: '{' + rs6[0].Id.toUpperCase() + '}',
                                name: rs6[0].attributes.fullname.value,
                                entityType: 'contact'
                            }]);
                            Xrm.Page.getControl("bsd_nguoidaidiena").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
                            fetch(xml7, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                            ["fullname"], ["eq"], [0, rs6[0].attributes.fullname.value, 1]);
                            CrmFetchKit.Fetch(xml7.join(""), true).then(function (rs7) {
                                if (rs7.length > 0) {
                                    Xrm.Page.getAttribute("bsd_jobtitlea").setValue(rs7[0].attributes.jobtitle.value);
                                }
                                else {
                                    Xrm.Page.getAttribute("bsd_jobtitlea").setValue(null);
                                }
                            },
                        function (er) {
                            console.log(er.message)
                        });
                        }
                        else {
                            Xrm.Page.getAttribute("bsd_jobtitlea").setValue(null);
                            clear_Loadrepresentative();
                        }
                    },
                function (er) {
                    console.log(er.message)
                });
                }
            },
                function (er) {
                    console.log(er.message)
                });
        }
        else {
            clear_Loadtrader();
            clear_LoadBankAccountTrader();
            clear_Loadrepresentative();
        }
    },
             function (er) {
                 console.log(er.message)
             });
}
//Author:Mr.Phong
//Description:action change filed renter
function action_changefieldtrader() {
    var trader = Xrm.Page.getAttribute("bsd_renter").getValue();
    var fieldaddress = Xrm.Page.getAttribute("bsd_addressrenter");
    var fieldbankaccount = Xrm.Page.getAttribute("bsd_bankaccounta");
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgroupa");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbera");
    var fieldbrand = Xrm.Page.getAttribute("bsd_branda");
    var fieldcontact = Xrm.Page.getAttribute("bsd_nguoidaidiena");
    Xrm.Page.getControl("bsd_addressrenter").removePreSearch(presearch_LoadAddressTrader);
    Xrm.Page.getControl("bsd_bankaccounta").removePreSearch(presearch_LoadBankAccountTrader);
    Xrm.Page.getControl("bsd_nguoidaidiena").removePreSearch(presearch_Loadrepresentative);
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    var xml7 = [];
    var xml8 = [];
    if (trader != null) {
        //load thong tin cua BHS  
        fetch(xml, "account", ["accountid", "name", "primarycontactid", "telephone1", "bsd_taxregistration", "fax"]
            , ["name"], false, null, ["accountid"], ["eq"]
            , [0, trader[0].id, 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.telephone1 != null) {
                    Xrm.Page.getAttribute("bsd_phonerenter").setValue(rs[0].attributes.telephone1.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_phonerenter").setValue(null);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistrationrenter").setValue(rs[0].attributes.bsd_taxregistration.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_taxregistrationrenter").setValue(null);
                }
                if (rs[0].attributes.fax != null) {
                    Xrm.Page.getAttribute("bsd_faxrenter").setValue(rs[0].attributes.fax.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_faxrenter").setValue(null);
                }
            }
        },
                 function (er) {
                     console.log(er.message)
                 });
        //load thong tin dia chi cua BHS       
        var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName1 = "bsd_address";
        var viewDisplayName1 = "Address View";
        fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
            ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, trader[0].id]);
        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
             "<row name='result'  " + "id='bsd_addressid'>  " +
             "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
             "<cell name='bsd_account'    " + "width='100' />  " +
             "</row>   " +
          "</grid>   ";
        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
            if (rs1.length > 0) {
                if (rs1[0].attributes.bsd_name != null) {
                    fieldaddress.setValue([{
                        id: rs1[0].Id,
                        name: rs1[0].attributes.bsd_name.value,
                        entityType: rs1[0].logicalName
                    }]);
                }
                else {
                    fieldaddress.setValue(null);
                }
                Xrm.Page.getControl("bsd_addressrenter").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
            }
            else {
                var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName2 = "bsd_address";
                var viewDisplayName2 = "Address View";
                fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, fieldtrader[0].id]);
                var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_addressid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";

                CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                    if (rs2.length > 0) {
                        Xrm.Page.getControl("bsd_addressrenter").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
                    }
                    else {
                        clear_LoadAddressTrader();
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
        },
         function (er) {
             console.log(er.message)
         });
        //load bank account cua BHS      
        var viewId3 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
        var entityName3 = "bsd_bankaccount";
        var viewDisplayName3 = "Bank Account Customer View";
        fetch(xml3, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_account", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
        var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
        "<row name='result'  " + "id='bsd_bankaccountid'>  " +
        "<cell name='bsd_name'   " + "width='200' />  " +
        "<cell name='createdon'    " + "width='100' />  " +
        "<cell name='bsd_brand'    " + "width='100' />  " +
        "<cell name='bsd_bankaccount'    " + "width='100' />  " +
        "<cell name='bsd_account'    " + "width='100' />  " +
        "<cell name='bsd_bankgroup'    " + "width='100' />  " +
        "</row>   " +
        "</grid>   ";
        CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
            if (rs3.length > 0) {
                if (rs3[0].attributes.bsd_name != null) {
                    fieldbankaccount.setValue([{
                        id: rs3[0].Id,
                        name: rs3[0].attributes.bsd_name.value,
                        entityType: rs3[0].logicalName
                    }]);
                    fieldbankgroup.setValue([{
                        id: rs3[0].attributes.bsd_bankgroup.guid,
                        name: rs3[0].attributes.bsd_bankgroup.name,
                        entityType: rs3[0].attributes.bsd_bankgroup.logicalName
                    }]);
                    fieldbankaccountnumber.setValue(rs3[0].attributes.bsd_bankaccount.value);
                    fieldbrand.setValue(rs3[0].attributes.bsd_brand.value);
                }
                else {
                    fieldbankaccount.setValue(null);
                    fieldbankgroup.setValue(null);
                    fieldbankaccountnumber.setValue(null);
                    fieldbrand.setValue(null);
                }
                Xrm.Page.getControl("bsd_bankaccounta").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
            }
            else {
                fieldbankaccount.setValue(null);
                fieldbrand.setValue(null);
                fieldbankaccountnumber.setValue(null);
                clear_LoadBankAccountTrader();
            }
        },
            function (er) {
                console.log(er.message)
            });
        //load nguoi dai dien

        xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xml4.push("<entity name='account'>");
        xml4.push("<attribute name='name' />");
        xml4.push("<attribute name='primarycontactid'/>");
        xml4.push("<attribute name='telephone1'/>");
        xml4.push("<attribute name='accountid'/>");
        xml4.push("<order attribute='name' descending='false'/> ");
        xml4.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
        xml4.push("<attribute name='jobtitle'/>");
        xml4.push("<filter type='and'>");
        xml4.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + trader[0].id + "'/>");
        xml4.push("</filter>");
        xml4.push("</link-entity>");
        xml4.push("</entity>");
        xml4.push("</fetch>");
        CrmFetchKit.Fetch(xml4.join(""), true).then(function (rs4) {
            if (rs4.length > 0) {
                fieldcontact.setValue([{
                    id: rs4[0].attributes.primarycontactid.guid,
                    name: rs4[0].attributes.primarycontactid.name,
                    entityType: 'contact'
                }]);
                var viewId4 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityName4 = "account";
                var viewDisplayName4 = "Contact View";
                fetch(xml8, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                   ["parentcustomerid", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
                var layoutXml4 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='contactid'>  " +
                         "<cell name='fullname'   " + "width='200' />  " +
                         "<cell name='telephone1'    " + "width='100' />  " +
                         "</row>   " +
                      "</grid>   ";
                Xrm.Page.getControl("bsd_nguoidaidiena").addCustomView(viewId4, entityName4, viewDisplayName4, xml8.join(""), layoutXml4, true);
                fetch(xml5, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
           ["contactid"], ["eq"], [0, rs4[0].attributes.primarycontactid.guid, 1]);
                CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                    if (rs5.length > 0) {
                        Xrm.Page.getAttribute("bsd_jobtitlea").setValue(rs5[0].attributes.jobtitle.value);
                    }
                    else {
                        Xrm.Page.getAttribute("bsd_jobtitlea").setValue(null);
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
            else {
                var viewId6 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityName6 = "contact";
                var viewDisplayName6 = "test";
                fetch(xml6, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                    ["parentcustomerid", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
                var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                CrmFetchKit.Fetch(xml6.join(""), true).then(function (rs6) {
                    if (rs6.length > 0) {
                        fieldcontact.setValue([{
                            id: rs6[0].Id,
                            name: rs6[0].attributes.fullname.value,
                            entityType: 'contact'
                        }]);
                        Xrm.Page.getControl("bsd_nguoidaidiena").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
                        fetch(xml7, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                        ["fullname"], ["eq"], [0, rs6[0].attributes.fullname.value, 1]);
                        CrmFetchKit.Fetch(xml7.join(""), true).then(function (rs7) {
                            if (rs7.length > 0) {
                                Xrm.Page.getAttribute("bsd_jobtitlea").setValue(rs7[0].attributes.jobtitle.value);
                            }
                            else {
                                Xrm.Page.getAttribute("bsd_jobtitlea").setValue(null);
                            }
                        },
                    function (er) {
                        console.log(er.message)
                    });
                    }
                    else {
                        Xrm.Page.getAttribute("bsd_jobtitlea").setValue(null);
                        clear_Loadrepresentative();
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
        },
            function (er) {
                console.log(er.message)
            });
    }
    else {
        fieldaddress.setValue(null);
        fieldbankaccount.setValue(null);
        fieldbankgroup.setValue(null);
        fieldbankaccountnumber.setValue(null);
        fieldbrand.setValue(null);
        fieldcontact.setValue(null);
        set_value(["bsd_phonerenter", "bsd_taxregistrationrenter", "bsd_faxrenter", "bsd_jobtitlea"], null);
        clear_LoadAddressTrader();
        clear_LoadBankAccountTrader();
        clear_Loadrepresentative();
    }
}
//Author:Mr.Phong
//Description:lay du lieu bank account,bank group,brand tu trader
function get_bankaccountnumberandbankgrouptrader() {
    var bankaccounttrader = Xrm.Page.getAttribute("bsd_bankaccounta").getValue();
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgroupa");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbera");
    var fieldbrand = Xrm.Page.getAttribute("bsd_branda");
    var xml = [];
    if (bankaccounttrader != null) {
        //load bank account cua BHS            
        fetch(xml, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_bankaccountid", "statecode"], ["eq", "eq"], [0, bankaccounttrader[0].id, 1, "0"]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_bankgroup != null) {
                    fieldbankgroup.setValue([{
                        id: '{' + rs[0].attributes.bsd_bankgroup.guid.toUpperCase() + '}',
                        name: rs[0].attributes.bsd_bankgroup.name,
                        entityType: rs[0].attributes.bsd_bankgroup.logicalName
                    }]);
                }
                if (rs[0].attributes.bsd_bankaccount != null) {
                    fieldbankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                }
                if (rs[0].attributes.bsd_brand != null) {
                    fieldbrand.setValue(rs[0].attributes.bsd_brand.value);
                }
            }
            else {
                fieldbankgroup.setValue(null);
                fieldbrand.setValue(null);
                fieldbankaccountnumber.setValue(null);
            }
        },
            function (er) {
                console.log(er.message)
            });
    }
    else {
        fieldbankgroup.setValue(null);
        fieldbrand.setValue(null);
        fieldbankaccountnumber.setValue(null);
    }
}
//Author:Mr.Phong
//Description:lay thong tin chuc vu nguoi dai dien tu BHS
function get_thongtinchucvu() {
    var present = Xrm.Page.getAttribute("bsd_nguoidaidiena").getValue();
    var xml = [];
    if (present != null) {
        fetch(xml, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
              ["contactid"], ["eq"], [0, present[0].id, 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.jobtitle != null) {
                    Xrm.Page.getAttribute("bsd_jobtitlea").setValue(rs[0].attributes.jobtitle.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_jobtitlea").setValue(null);
                }
            }
        },
         function (er) {
             console.log(er.message)
         });
    }
    if (present == null) {
        Xrm.Page.getAttribute("bsd_jobtitlea").setValue(null);
    }
}
//Author:Mr.Đăng
//Description: Auto Load
function check_date() {
    var fromdate = getValue("bsd_fromdate");
    var todate = getValue("bsd_todate");
    var currentDateTime = getCurrentDateTime();
    if (fromdate != null) {
        if (fromdate < currentDateTime) {
            var message = "The From Date cannot occur after current date";
            var type = "INFO"; //INFO, WARNING, ERROR
            var id = "Info1"; //Notification Id
            var time = 3000; //Display time in milliseconds
            //Display the notification
            Xrm.Page.ui.setFormNotification(message, type, id);

            //Wait the designated time and then remove
            setTimeout(
                function () {
                    Xrm.Page.ui.clearFormNotification(id);
                },
                time
            );
            setValue("bsd_fromdate", currentDateTime);
        }
        if (todate < fromdate) {

            setNotification("bsd_todate", "The To Date Date cannot occur after From Date");
        }
        else if (todate > fromdate) {
            Xrm.Page.getControl("bsd_todate").clearNotification();
        }
    }
}
function getCurrentDateTime() {
    var d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
}
var purpose = null;
function khachhang_change(reset) {
    var khachhang = getValue("bsd_account");
    var nguoidaidienb = Xrm.Page.getAttribute("bsd_nguoidaidienb");

    var fieldbankaccount = Xrm.Page.getAttribute("bsd_bankaccountb");
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgroupb");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumberb");
    var fieldbrand = Xrm.Page.getAttribute("bsd_brandb");

    Xrm.Page.getControl("bsd_address").removePreSearch(presearch_addressaccount);
    Xrm.Page.getControl("bsd_bankaccountb").removePreSearch(presearch_bankaccount);
    Xrm.Page.getControl("bsd_nguoidaidienb").removePreSearch(presearch_contactaccount);

    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    getControl("bsd_address").removePreSearch(presearch_addressaccount);
    if (reset != false) setNull(["bsd_address", "bsd_warehouseaddress"]);
    if (khachhang != null) {
        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName = "bsd_address";
        var viewDisplayName = "test";
        fetch(xml, "bsd_address", ["bsd_name", "createdon", "bsd_purpose", "bsd_account", "bsd_addressid"], ["bsd_name"], false, null
            , ["bsd_account", "bsd_purpose_tmpvalue", "statecode"], ["eq", "like", "eq"], [0, khachhang[0].id, 1, "%" + "Business" + "%", 2, "0"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                          "<row name='result'  " + "id='bsd_addressid'>  " +
                          "<cell name='bsd_name'    " + "width='100' />  " +
                          "<cell name='createdon'    " + "width='100' />  " +
                          "<cell name='bsd_purpose'    " + "width='100' />  " +
                          "<cell name='bsd_account'    " + "width='100' />  " +
                          "</row>   " +
                       "</grid>   ";
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                var first = rs[0];
                if (getValue("bsd_address") == null) {
                    setValue("bsd_address", [{
                        id: first.Id,
                        name: first.attributes.bsd_name.value,
                        entityType: first.logicalName
                    }]
                   );
                    set_DisableWarehouse(reset);
                }
                Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
                fetch(xml2, "account", ["name", "primarycontactid", "telephone1", "accountid", "bsd_taxregistration", "fax", "websiteurl", "emailaddress1", "accountnumber"], ["name"], false, null
                , ["accountid"], ["eq"], [0, khachhang[0].id, 1]);
                CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs2) {
                    if (rs2.length > 0) {
                        if (Xrm.Page.getAttribute("bsd_accountnumberb").getValue() == null) {
                            if (rs2[0].attributes.accountnumber != null) {
                                var accountnumber = rs2[0].attributes.accountnumber.value;
                                Xrm.Page.getAttribute("bsd_accountnumberb").setValue(accountnumber);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_mainphoneb").getValue() == null) {
                            if (rs2[0].attributes.telephone1 != null) {
                                var telephone = rs2[0].attributes.telephone1.value;
                                Xrm.Page.getAttribute("bsd_mainphoneb").setValue(telephone);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_faxb").getValue() == null) {
                            if (rs2[0].attributes.fax != null) {
                                var fax = rs2[0].attributes.fax.value;
                                Xrm.Page.getAttribute("bsd_faxb").setValue(fax);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_taxregistrationb").getValue() == null) {
                            if (rs2[0].attributes.bsd_taxregistration != null) {
                                var taxres = rs2[0].attributes.bsd_taxregistration.value;
                                Xrm.Page.getAttribute("bsd_taxregistrationb").setValue(taxres);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_emailb").getValue() == null) {
                            if (rs2[0].attributes.emailaddress1 != null) {
                                var email = rs2[0].attributes.emailaddress1.value;
                                Xrm.Page.getAttribute("bsd_emailb").setValue(email);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_websiteb").getValue() == null) {
                            if (rs2[0].attributes.websiteurl != null) {
                                var website = rs2[0].attributes.websiteurl.value;
                                Xrm.Page.getAttribute("bsd_websiteb").setValue(website);
                            }
                        }
                        if (rs2[0].attributes.primarycontactid != null) {
                            if (nguoidaidienb.getValue() == null) {
                                nguoidaidienb.setValue([{
                                    id: '{' + rs2[0].attributes.primarycontactid.guid.toUpperCase() + '}',
                                    name: rs2[0].attributes.primarycontactid.name,
                                    entityType: 'contact'
                                }]);
                            }
                            var viewId6 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                            var entityName6 = "account";
                            var viewDisplayName6 = "Contact View";
                            fetch(xml6, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                               ["parentcustomerid", "statecode"], ["eq", "eq"], [0, khachhang[0].id, 1, "0"]);
                            var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                     "<row name='result'  " + "id='contactid'>  " +
                                     "<cell name='fullname'   " + "width='200' />  " +
                                     "<cell name='telephone1'    " + "width='100' />  " +
                                     "</row>   " +
                                  "</grid>   ";
                            Xrm.Page.getControl("bsd_nguoidaidienb").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
                            var id = rs2[0].attributes.primarycontactid.guid;
                            fetch(xml3, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null
                            , ["contactid"], ["eq"], [0, id, 1]);
                            CrmFetchKit.Fetch(xml3.join(""), false).then(function (rs3) {
                                if (rs3.length > 0) {
                                    if (Xrm.Page.getAttribute("bsd_jobtitle").getValue() == null) {
                                        if (rs3[0].attributes.jobtitle != null) {
                                            var job = rs3[0].attributes.jobtitle.value;
                                            Xrm.Page.getAttribute("bsd_jobtitle").setValue(job);
                                        }
                                    }
                                }
                                else {
                                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
                                }
                            }, function (er) {
                                console.log(er.message)
                            });
                        }
                        if (rs2[0].attributes.primarycontactid == null) {
                            var viewId4 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                            var entityName4 = "contact";
                            var viewDisplayName4 = "test";
                            fetch(xml4, "contact", ["fullname", "telephone1", "contactid", "jobtitle", "parentcustomerid"], ["fullname"], false, null
                           , ["parentcustomerid"], ["eq"], [0, khachhang[0].id, 1]);
                            var layoutXml4 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                             "<row name='result'  " + "id='contactid'>  " +
                                             "<cell name='fullname'   " + "width='200' />  " +
                                             "<cell name='telephone1'    " + "width='100' />  " +
                                              "<cell name='jobtitle'    " + "width='100' />  " +
                                                "<cell name='parentcustomerid'    " + "width='100' />  " +
                                             "</row>   " +
                                          "</grid>   ";
                            CrmFetchKit.Fetch(xml4.join(""), false).then(function (rs4) {
                                if (rs4.length > 0) {
                                    Xrm.Page.getControl("bsd_nguoidaidienb").addCustomView(viewId4, entityName4, viewDisplayName4, xml4.join(""), layoutXml4, true);
                                }
                                else {
                                    clear_contactaccount();
                                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
                                }
                            }, function (er) {
                                console.log(er.message)
                            });

                        }
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
            else {
                var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName1 = "bsd_address";
                var viewDisplayName1 = "test";
                fetch(xml1, "bsd_address", ["bsd_name", "createdon", "bsd_purpose", "bsd_account", "bsd_addressid"], ["bsd_name"], false, null
                    , ["bsd_account", "statecode"], ["eq", "eq"], [0, khachhang[0].id, 1, 0]);
                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='bsd_addressid'>  " +
                                  "<cell name='bsd_name'    " + "width='100' />  " +
                                  "<cell name='createdon'    " + "width='100' />  " +
                                  "<cell name='bsd_purpose'    " + "width='100' />  " +
                                  "<cell name='bsd_account'    " + "width='100' />  " +
                                  "</row>   " +
                               "</grid>   ";
                Xrm.Page.getControl("bsd_address").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                fetch(xml2, "account", ["name", "primarycontactid", "telephone1", "accountid", "bsd_taxregistration", "fax", "websiteurl", "emailaddress1"], ["name"], false, null
              , ["accountid"], ["eq"], [0, khachhang[0].id, 1]);
                CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs2) {
                    if (rs2.length > 0) {
                        if (Xrm.Page.getAttribute("bsd_mainphoneb").getValue() == null) {
                            if (rs2[0].attributes.telephone1 != null) {
                                var telephone = rs2[0].attributes.telephone1.value;
                                Xrm.Page.getAttribute("bsd_mainphoneb").setValue(telephone);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_faxb").getValue() == null) {
                            if (rs2[0].attributes.fax != null) {
                                var fax = rs2[0].attributes.fax.value;
                                Xrm.Page.getAttribute("bsd_faxb").setValue(fax);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_taxregistrationb").getValue() == null) {
                            if (rs2[0].attributes.bsd_taxregistration != null) {
                                var taxres = rs2[0].attributes.bsd_taxregistration.value;
                                Xrm.Page.getAttribute("bsd_taxregistrationb").setValue(taxres);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_emailb").getValue() == null) {
                            if (rs2[0].attributes.emailaddress1 != null) {
                                var email = rs2[0].attributes.emailaddress1.value;
                                Xrm.Page.getAttribute("bsd_emailb").setValue(email);
                            }
                        }
                        if (Xrm.Page.getAttribute("bsd_websiteb").getValue() == null) {
                            if (rs2[0].attributes.websiteurl != null) {
                                var website = rs2[0].attributes.websiteurl.value;
                                Xrm.Page.getAttribute("bsd_websiteb").setValue(website);
                            }
                        }
                        if (rs2[0].attributes.primarycontactid != null) {
                            var contact = rs2[0].attributes.primarycontactid.name;
                            if (nguoidaidienb.getValue() == null) {
                                nguoidaidienb.setValue([{
                                    id: rs2[0].attributes.primarycontactid.guid,
                                    name: rs2[0].attributes.primarycontactid.name,
                                    entityType: 'contact'
                                }]);
                            }
                            var id = rs2[0].attributes.primarycontactid.guid;
                            fetch(xml3, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null
                            , ["contactid"], ["eq"], [0, id, 1]);
                            CrmFetchKit.Fetch(xml3.join(""), false).then(function (rs3) {
                                if (rs3.length > 0) {
                                    if (Xrm.Page.getAttribute("bsd_jobtitle").getValue() == null) {
                                        if (rs3[0].attributes.jobtitle != null) {
                                            var job = rs3[0].attributes.jobtitle.value;
                                            Xrm.Page.getAttribute("bsd_jobtitle").setValue(job);
                                        }
                                    }
                                }
                                else {
                                    //Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
                                }
                            }, function (er) {
                                console.log(er.message)
                            });
                        }
                        if (rs2[0].attributes.primarycontactid == null) {
                            var viewId4 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                            var entityName4 = "contact";
                            var viewDisplayName4 = "test";
                            fetch(xml4, "contact", ["fullname", "telephone1", "contactid", "jobtitle", "parentcustomerid"], ["fullname"], false, null
                           , ["parentcustomerid", "bsd_contacttype", "statecode"], ["eq", "eq", "eq"], [0, khachhang[0].id, 1, "861450000", 2, "0"]);
                            var layoutXml4 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                             "<row name='result'  " + "id='contactid'>  " +
                                             "<cell name='fullname'   " + "width='200' />  " +
                                             "<cell name='telephone1'    " + "width='100' />  " +
                                              "<cell name='jobtitle'    " + "width='100' />  " +
                                                "<cell name='parentcustomerid'    " + "width='100' />  " +
                                             "</row>   " +
                                          "</grid>   ";
                            CrmFetchKit.Fetch(xml4.join(""), false).then(function (rs4) {
                                if (rs4.length > 0) {
                                    Xrm.Page.getControl("bsd_nguoidaidienb").addCustomView(viewId4, entityName4, viewDisplayName4, xml4.join(""), layoutXml4, true);
                                }
                                else {
                                    clear_contactaccount();
                                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
                                }
                            }, function (er) {
                                console.log(er.message)
                            });

                        }
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }, function (er) {
            console.log(er.message)
        });
        var viewId5 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
        var entityName5 = "bsd_bankaccount";
        var viewDisplayName5 = "test";
        fetch(xml5, "bsd_bankaccount", ["bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankaccountid", "bsd_bankgroup"], ["bsd_name"], false, null
                          , ["bsd_account", "statecode"], ["eq", "eq"], [0, khachhang[0].id, 1, 0]);
        var layoutXml5 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
            "<row name='result'  " + "id='bsd_bankaccountid'>  " +
            "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
            "<cell name='bsd_brand'    " + "width='100' />  " +
            "<cell name='bsd_bankaccount'    " + "width='100' />  " +
            "<cell name='bsd_account'    " + "width='100' />  " +
            "<cell name='bsd_bankgroup'    " + "width='100' />  " +
            "</row>   " +
            "</grid>   ";
        CrmFetchKit.Fetch(xml5.join(""), false).then(function (rs5) {
            if (rs5.length > 0) {
                if (rs5[0].attributes.bsd_name != null) {
                    fieldbankaccount.setValue([{
                        id: '{' + rs5[0].Id.toUpperCase() + '}',
                        name: rs5[0].attributes.bsd_name.value,
                        entityType: rs5[0].logicalName
                    }]);
                    fieldbankgroup.setValue([{
                        id: '{' + rs5[0].attributes.bsd_bankgroup.guid.toUpperCase() + '}',
                        name: rs5[0].attributes.bsd_bankgroup.name,
                        entityType: rs5[0].attributes.bsd_bankgroup.logicalName
                    }]);
                    fieldbankaccountnumber.setValue(rs5[0].attributes.bsd_bankaccount.value);
                    fieldbrand.setValue(rs5[0].attributes.bsd_brand.value);
                }
                else {
                    fieldbankaccount.setValue(null);
                    fieldbankgroup.setValue(null);
                    fieldbankaccountnumber.setValue(null);
                    fieldbrand.setValue(null);
                }
                Xrm.Page.getControl("bsd_bankaccountb").addCustomView(viewId5, entityName5, viewDisplayName5, xml5.join(""), layoutXml5, true);
            }
            if (rs5.length == 0) {
                clear_bankaccount();
            }
        }, function (er) {
            console.log(er.message)
        });

    } else if (reset != false) {
        set_DisableWarehouse();
        clear_addressaccount();
        set_value(["bsd_mainphoneb", "bsd_faxb", "bsd_taxregistrationb", "bsd_brandb", "bsd_bankaccountnumberb", "bsd_bankgroupb", "bsd_accountnumberb"
                    , "bsd_emailb", "bsd_websiteb", "bsd_bankaccountb", "bsd_nguoidaidienb", "bsd_jobtitle"], null);
        clear_bankaccount();
        clear_contactaccount();
    }
}

//Author:Mr.Đăng
//Description: Disable Outlet Type

function set_DisableWarehouse(reset) {
    debugger;
    var fetchxml = null
    var khachhang = getValue("bsd_account");
    var shipping = getValue("bsd_shipping");
    getControl("bsd_warehouseaddress").removePreSearch(presearch_AddressDeliveryShipping);
    if (shipping) {
        setValue("bsd_cachthucgiaonhan", shipping);
        setVisible("bsd_warehouse", false);
        if (reset != false) setNull(["bsd_warehouse", "bsd_warehouseaddress"]);
    } else {
        setVisible("bsd_warehouse", true);
        get_warehouseadd(reset);
    }
    if (khachhang != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                       '<entity name="bsd_address">',
                       '<attribute name="bsd_name" />',
                       '<attribute name="bsd_purpose" />',
                       '<attribute name="bsd_account" />',
                       '<attribute name="createdon" />',
                       '<attribute name="bsd_addressid" />',
                       '<order attribute="bsd_name" descending="false" />',
                       '<filter type="and">',
                       '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + khachhang[0].id + '" />',
                       '<condition attribute="bsd_purpose_tmpvalue" operator="like" value="%delivery%" />',
                       '</filter>',
                       '</entity>',
                       '</fetch>'].join("");
        LookUp_After_Load("bsd_warehouseaddress", "bsd_address", "bsd_name", "Address", fetchxml);
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs1) {
            if (shipping) {
                if (reset != false) setNull("bsd_warehouse");
                if (khachhang != null && fetchxml != null) {
                    console.log("ok" + rs1);
                    if (rs1.length > 0) {
                        if (getValue("bsd_warehouseaddress") == null) {
                            setValue("bsd_warehouseaddress", [{
                                id: rs1[0].Id,
                                name: rs1[0].attributes.bsd_name.value,
                                entityType: rs1[0].logicalName
                            }]);
                        }
                    }
                } else {
                    setNull("bsd_warehouseaddress");
                }

            }
        }, function (er) { });
    } else if (reset != false) {
        clear_AddressDeliveryShipping();
    }
}
//Author:Mr.Đăng
//Description: Load Dia Chi When Shipping: Yes
function Load_AddressDeliveryShipping(shipping) {
    debugger;
    var khachhang = getValue("bsd_account");
    var warehouse = getValue("bsd_warehouse");
    if (khachhang == null && shipping) setNull("bsd_warehouseaddress");
    if (warehouse != null) {
        setNull(["bsd_warehouse", "bsd_warehouseaddress"]);
    }
    shipping = getValue("bsd_shipping");
    getControl("bsd_warehouseaddress").removePreSearch(presearch_AddressDeliveryShipping);
    if (khachhang != null && shipping) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_purpose" />',
                        '<attribute name="bsd_account" />',
                        '<attribute name="createdon" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + khachhang[0].id + '" />',
                        '<condition attribute="bsd_purpose_tmpvalue" operator="like" value="' + "%" + "Delivery" + "%" + '" />',
                         '<condition attribute="statecode" operator="eq" value="0" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml)
        console.log(rs);
        LookUp_After_Load("bsd_warehouseaddress", "bsd_address", "bsd_name", "Address", fetchxml);
    }
    else if (khachhang == null && shipping) {
        clear_AddressDeliveryShipping();
    }
}
//author:Mr.Đăng
//Description: Load Đia chỉ Warehouse mặc định
function get_warehouseadd(reset) {
    debugger;
    if (reset != false) setNull("bsd_warehouseaddress");
    getControl("bsd_warehouseaddress").removePreSearch(presearch_AddressDeliveryShipping);
    var warehouse = getValue("bsd_warehouse");
    if (warehouse != null) {
        var shipping = getValue("bsd_shipping");
        if (!shipping) {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                            '<entity name="bsd_address">',
                            '<attribute name="bsd_name" />',
                            '<attribute name="bsd_addressid" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<link-entity name="bsd_warehouseentity" from="bsd_address" to="bsd_addressid" alias="ab">',
                            '<filter type="and">',
                            '<condition attribute="bsd_warehouseentityid" operator="eq" uitype="bsd_warehouseentity" value="' + warehouse[0].id + '" />',
                            '</filter>',
                            '</link-entity>',
                            '</entity>',
                            '</fetch>'].join("");
            LookUp_After_Load("bsd_warehouseaddress", "bsd_address", "bsd_name", "Warehouse", fetchxml);

            CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
                console.log(rs);
                if (rs.length > 0) {
                    var first = rs[0];
                    if (getValue("bsd_warehouseaddress") == null) {
                        setValue("bsd_warehouseaddress", [{
                            id: first.Id,
                            name: first.attributes.bsd_name.value,
                            entityType: first.logicalName
                        }]
                       );
                    }
                }

            }, function (er) { });
        }
    } else if (reset != false) {
        clear_AddressDeliveryShipping();
    }
}

function addressxhd_change(reset) {
    set_DisableWarehouse(reset);
}
function clear_AddressDeliveryShipping() {
    getControl("bsd_warehouseaddress").addPreSearch(presearch_AddressDeliveryShipping);
}
function presearch_AddressDeliveryShipping() {
    getControl("bsd_warehouseaddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
// 21.03.2017 Đăng: Set fromdate when create new
function setValueDefault() {
    setValue("bsd_fromdate", getCurrentDateTime());
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}



function clear_addressaccount() {
    getControl("bsd_address").addPreSearch(presearch_addressaccount);
}
function presearch_addressaccount() {
    getControl("bsd_address").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_bankaccount() {
    getControl("bsd_bankaccountb").addPreSearch(presearch_bankaccount);
}
function presearch_bankaccount() {
    getControl("bsd_bankaccountb").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_contactaccount() {
    getControl("bsd_nguoidaidienb").addPreSearch(presearch_contactaccount);
}
function presearch_contactaccount() {
    getControl("bsd_nguoidaidienb").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Loadtrader() {
    getControl("bsd_renter").addPreSearch(presearch_Loadtrader);
}
function presearch_Loadtrader() {
    getControl("bsd_renter").addPreSearch(presearch_Loadtrader);
}
function clear_LoadBankAccountTrader() {
    getControl("bsd_bankaccounta").addPreSearch(presearch_LoadBankAccountTrader);
}
function presearch_LoadBankAccountTrader() {
    getControl("bsd_bankaccounta").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Loadrepresentative() {
    getControl("bsd_nguoidaidiena").addPreSearch(presearch_Loadrepresentative);
}
function presearch_Loadrepresentative() {
    getControl("bsd_nguoidaidiena").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_LoadAddressTrader() {
    getControl("bsd_addressrenter").addPreSearch(presearch_LoadAddressTrader);
}
function presearch_LoadAddressTrader() {
    getControl("bsd_addressrenter").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}