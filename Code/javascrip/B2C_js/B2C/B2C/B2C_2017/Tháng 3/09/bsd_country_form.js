function set_disablefiedcountrycode() {
    if (Xrm.Page.ui.getFormType() == 2) {
        Xrm.Page.getControl("bsd_id").setDisabled(true);
    }
    else {
        Xrm.Page.getControl("bsd_id").setDisabled(false);
    }
}
//Author:Mr.Đăng
//Description: Check CountryID
function Check_CountryID() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var id = getValue("bsd_id");
    if (id != null) {
        if (mikExp.test(id)) {
            setNotification("bsd_id", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_country">',
                            '<attribute name="bsd_countryid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_id" operator="eq" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            console.log(rs);
            if (rs.length > 0) {
                setNotification("bsd_id", "Mã quốc gia đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_id");
                setValue("bsd_id", id.toUpperCase());
            }
        }
    }
}
//author:Mr.Phong
//description: Kiểm tra không cho phép nhập kí tự đặc biệt
function Check_id() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var name = getValue("bsd_name");
    if (name != null) {
        if (mikExp.test(name)) {
            setNotification("bsd_name", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_name");
        }
    }
}