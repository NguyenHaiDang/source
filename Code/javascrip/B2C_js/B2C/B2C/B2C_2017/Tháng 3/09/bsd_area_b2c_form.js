//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    set_disableArea();
}
//Author:Mr.Đăng
//Description: Disable Area
function set_disableArea() {
    if (formType() == 2) {
        setDisabled("bsd_id", true);
    }
    else {
        setDisabled("bsd_id", false);
    }
}
//Author:Mr.Đăng
//Description:Check AreaID
function Check_AreaID() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var id = getValue("bsd_id");
    if (id != null) {
        if (mikExp.test(id)) {
            setNotification("bsd_id", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_areab2c">',
                        '<attribute name="bsd_areab2cid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_id" operator="eq" value="' + id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_id", "Mã vùng đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_id");
                setValue("bsd_id", id.toUpperCase());
            }
        }
    }
}
//author:Mr.Đăng
//description: Kiểm tra không cho phép nhập kí tự đặc biệt
function Check_id() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var name = getValue("bsd_name");
    if (name != null) {
        if (mikExp.test(name)) {
            setNotification("bsd_name", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_name");
        }
    }
}