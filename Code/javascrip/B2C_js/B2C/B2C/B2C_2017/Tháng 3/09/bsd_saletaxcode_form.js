//Author:Mr.Đăng
//Description:Check Name
function Check_Name() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var name = getValue("bsd_name");
    if (name != null) {
        if (mikExp.test(name)) {
            setNotification("bsd_name", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_saletaxcode">',
                            '<attribute name="bsd_saletaxcodeid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_name" operator="eq" value="' + name + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_name", "Mã đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_name");
                setValue("bsd_name", name.toUpperCase());
            }
        }
    }
}
//Author:Mr.Đăng
//Description:Check Percentage Amount
function Check_PercentageAmount() {
    var percentageamount = getValue("bsd_percentageamount");
    var id = getId();
    if (percentageamount != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_saletaxcode">',
                        '<attribute name="bsd_saletaxcodeid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_percentageamount" operator="eq" value="' + percentageamount + '" />',
                        '<condition attribute="bsd_saletaxcodeid" operator="ne" value="' + id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setNotification("bsd_percentageamount", "Số thuế đã tồn tại, vui lòng kiểm tra lại.");
        } else {
            clearNotification("bsd_percentageamount");
        }
    }
}