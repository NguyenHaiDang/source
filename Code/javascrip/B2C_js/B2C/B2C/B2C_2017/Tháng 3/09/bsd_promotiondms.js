/*
Huy 30/12/2016
note: update setDisabled

Diệm 3/3/2017:  bỏ field quantityto và priceto

Đăng 9/3/2017:  Thêm country, region, province + xóa fields: Type, price group
*/
function AutoLoad() {
    V_PriceGroup_Distributor();
    Load_Quantity_from_to_Visible();
    if (formType() == 2) {
        LK_Area_change(false);
        setDisabled(["bsd_name", "bsd_code", "bsd_area", "bsd_type", "bsd_type", "bsd_distributor", "bsd_typeprice", "bsd_channel"], true);
    }
    else {
        clear_distributor();
    }
}
/*
Diệm
Note: change area.
*/


function LK_Area_change(reset) {
    if (reset != false) setNull("bsd_distributor");
    var area = getValue("bsd_area");
    getControl("bsd_distributor").removePreSearch(presearch_distributor);
    if (area != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                        '  <entity name="account">',
                        '    <attribute name="name" />',
                        '    <attribute name="primarycontactid" />',
                        '    <attribute name="telephone1" />',
                        '    <attribute name="accountid" />',
                        '    <order attribute="name" descending="false" />',
                       '     <link-entity name="bsd_area" from="bsd_distributor" to="accountid" alias="aw">',
                        '      <link-entity name="bsd_wardarea" from="bsd_areadms" to="bsd_areaid" alias="ax">',
                         '       <filter type="and">',
                        '          <condition attribute="bsd_area" operator="eq" uitype="bsd_areab2c" value="' + area[0].id + '" />',
                        '        </filter>',
                        '      </link-entity>',
                        '    </link-entity>',
                        '  </entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);

        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                 "<row name='result'  " + "id='accountid'>  " +
                 "<cell name='name'   " + "width='200' />  " +
                 "<cell name='createdon'    " + "width='100' />  " +
                 "</row>   " +
              "</grid>";
        getControl("bsd_distributor").addCustomView(getControl("bsd_distributor").getDefaultView(), "account", "Account", fetchxml, layoutXml, true);
    } else {
        clear_distributor();
    }
}

function clear_distributor() {
    getControl("bsd_distributor").addPreSearch(presearch_distributor);
}
function presearch_distributor() {
    getControl("bsd_distributor").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
/*
Huy 29/12/2016
Note: update visible.
*/
function V_PriceGroup_Distributor() {
    var type = getValue("bsd_type");
    if (type != null) {
        if (type == "861450000")/*price group*/ {
            setVisible("bsd_pricegroup", true);
            setVisible("bsd_distributor", false);
            setNull("bsd_distributor");
            setRequired("bsd_pricegroup", "required");
            setRequired("bsd_distributor", "none");
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_5").setVisible(false); // province
        }
        else if (type == "861450001")/*Distributor*/ {
            setVisible("bsd_pricegroup", false);
            setVisible("bsd_distributor", true);
            setNull("bsd_pricegroup");
            setRequired("bsd_distributor", "required");
            setRequired("bsd_pricegroup", "none");
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_5").setVisible(false); // province
        }
        else/*All*/ {
            setVisible(["bsd_pricegroup", "bsd_distributor"], false);
            setNull(["bsd_pricegroup", "bsd_distributor"]);
            setRequired(["bsd_pricegroup", "bsd_distributor"], "none");
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_5").setVisible(true); // province
        }
    }
}

//Diệm
//Note: kiểm tra số lượng từ và đến(khong su dung nua)
function CheckQuantityFromTo() {
    //debugger;
    //if (getValue("bsd_toquantity") != null && getValue("bsd_fromquantity") != null) {
    //    if (getValue("bsd_toquantity") < getValue("bsd_fromquantity")) {
    //        setNotification("bsd_fromquantity", "Vui lòng nhập From quantity nhỏ hơn To quantity");
    //    } else {
    //        clearNotification("bsd_fromquantity");
    //    }
    //}
}
//Diệm
// Kiểm tra nhập từ ngày đến ngày
function CheckDateFromTo() {
    if (getValue("bsd_fromdate") != null && getValue("bsd_todate") != null) {
        var todate = getValue("bsd_todate");
        var fromdate = getValue("bsd_fromdate");

        if (todate.getFullYear() > fromdate.getFullYear()) {
            clearNotification("bsd_fromdate");
        }
        else if (todate.getFullYear() == fromdate.getFullYear()) {
            if ((todate.getMonth() + 1) > (fromdate.getMonth() + 1)) {
                clearNotification("bsd_fromdate");
            }
            else if ((todate.getMonth() + 1) == (fromdate.getMonth() + 1)) {
                if (todate.getDate() >= fromdate.getDate()) {
                    clearNotification("bsd_fromdate");
                } else {
                    setNotification("bsd_fromdate", "Vui lòng nhập From date nhỏ hơn To date");
                }
            }
            else {
                setNotification("bsd_fromdate", "Vui lòng nhập From date nhỏ hơn To date");
            }
        } else {
            setNotification("bsd_fromdate", "Vui lòng nhập From date nhỏ hơn To date");
        }
    }
}

//Huy
//Note: khong su dung nua
function CheckPriceFromTo() {
    //debugger;
    //if (getValue("bsd_toprice") != null && getValue("bsd_fromprice") != null) {
    //    if (getValue("bsd_toprice") < getValue("bsd_fromprice")) {
    //        setNotification("bsd_fromprice", "Vui lòng nhập From Price nhỏ hơn To Price");
    //    } else {
    //        clearNotification("bsd_fromprice");
    //    }
    //}
}

//Diệm.
//hàm lookupchung
function LookUp_After(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).removePreSearch(presearch_distributor);
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}
/*
Huy: 29/12/2016: update visible
*/

function Load_Quantity_from_to_Visible() {
    debugger;
    var typeprice = getValue("bsd_typeprice");
    if (typeprice!=null) {
        if (typeprice == "861450001") {//discount
            setVisible(["bsd_fromquantity", "bsd_percent", "bsd_fromprice"], false);
            setNull(["bsd_fromquantity", "bsd_percent", "bsd_fromprice"]);
            setRequired(["bsd_fromquantity", "bsd_percent", "bsd_fromprice"], "none");
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_3").setVisible(true); // detal promotion
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_6").setVisible(false);// detal promotion
        }
        else if (typeprice == "861450002") {//multi
            setVisible(["bsd_fromquantity", "bsd_percent"], true);
            setVisible(["bsd_fromprice"], false);
            setRequired(["bsd_fromquantity", "bsd_percent"], "required");
            setRequired(["bsd_fromprice"], "none");
            setNull(["bsd_fromprice"]);
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_3").setVisible(false);// detal promotion
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_6").setVisible(true);// detal promotion
        }
        else {//total
            setVisible(["bsd_fromquantity"], false);
            setVisible(["bsd_fromprice", "bsd_percent"], true);
            setRequired(["bsd_fromprice","bsd_fromquantity"], "none");
            setNull(["bsd_fromquantity", "bsd_fromquantity"]);
            setRequired(["bsd_fromprice", "bsd_percent"], "required");
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_3").setVisible(false);// detal promotion
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_6").setVisible(false);// detal promotion
        }
    }
    else {
        setVisible(["bsd_fromquantity", "bsd_percent", "bsd_fromprice"], false);
        setRequired(["bsd_fromquantity", "bsd_percent", "bsd_fromprice"], "none");
        setNull(["bsd_fromquantity", "bsd_percent", "bsd_fromprice"]);
    }
}