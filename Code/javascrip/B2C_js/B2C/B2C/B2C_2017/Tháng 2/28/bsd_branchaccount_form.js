﻿//Author:Mr.Đăng
//Description:Check Branch
function Check_branch() {
    var account = getValue("bsd_account");
    var branch = getValue("bsd_branch");
    var id = getId();
    if (branch != null) {
        setValue("bsd_name", branch[0].name);
        if (account != null) {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_branchaccount">',
                            '<attribute name="bsd_branchaccountid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + account[0].id + '" />',
                            '<condition attribute="bsd_branch" operator="eq" uitype="bsd_branch" value="' + branch[0].id + '" />',
                            '<condition attribute="bsd_branchaccountid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_branch", "Đã tồn tại, vui lòng kiểm tra lại.");
            }
            else {
                clearNotification("bsd_branch");
            }
        }
    }
    else {
        setNull("bsd_name");
    }
}