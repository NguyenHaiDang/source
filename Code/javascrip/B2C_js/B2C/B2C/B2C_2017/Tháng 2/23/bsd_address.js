﻿//author:Mr.Phong
//description: Autoload
function Autoload() {
    if (formType() == 2) {
        set_requireornot(false);
    } else {
        set_requireornot();
    }
}
//author:Mr.Phong
//description:set province theo area
function filter_province()   {
    var area = Xrm.Page.getAttribute("bsd_area").getValue();
    var provincedefault = Xrm.Page.getAttribute("bsd_province");
    var districdefault = Xrm.Page.getAttribute("bsd_district");
    var warddefault = Xrm.Page.getAttribute("bsd_ward");
    Xrm.Page.getControl("bsd_province").removePreSearch(presearch_province);
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
    Xrm.Page.getControl("bsd_ward").removePreSearch(presearch_ward);
    var xml = [];
    var xml1 = [];
    var xml3 = [];
    var xml4 = [];
    if (Xrm.Page.ui.getFormType() == 2 || Xrm.Page.ui.getFormType() == 1) {
        if (area != null) {
            var viewId = "{831690EC-0614-46D2-8BF5-423CE68B6BF8}";
            var entityName = "bsd_province";
            var viewDisplayName = "test";
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_province'>");
            xml.push("<attribute name='bsd_provinceid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_area' uitype='bsd_areab2c' operator='eq' value='" + area[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_provinceid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_provinceid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_province").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            provincedefault.setValue(null);
            districdefault.setValue(null);
            clear_district();
            clear_ward();
        }
        else {
            Xrm.Page.getAttribute("bsd_province").setValue(null);
            Xrm.Page.getAttribute("bsd_district").setValue(null);
            Xrm.Page.getAttribute("bsd_ward").setValue(null);
            clear_province();
            clear_district();
            clear_ward();
        }
    }
}
//author:Mr.Phong
//description:load area
function load_area() {
    var region = Xrm.Page.getAttribute("bsd_region").getValue();
    var xml = [];
    if (Xrm.Page.ui.getFormType() == 2) {
        if (region != null) {
            var viewId = "{32F4BE54-EB7D-4FDA-A5CC-CF7D17572AC2}";
            var entityName = "bsd_areab2c";
            var viewDisplayName = "test";
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_areab2c'>");
            xml.push("<attribute name='bsd_areab2cid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_region' uitype='bsd_region' operator='eq' value='" + region[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_areab2cid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_areab2cid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_area").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        if (region == null) {
            Xrm.Page.getAttribute("bsd_province").setValue(null);
            Xrm.Page.getAttribute("bsd_district").setValue(null);
            Xrm.Page.getAttribute("bsd_ward").setValue(null);
            Xrm.Page.getAttribute("bsd_street").setValue(null);
        }
    }
}
//author:Mr.Phong
//description:load region
function load_region() {
    var country = Xrm.Page.getAttribute("bsd_country").getValue();
    if (Xrm.Page.ui.getFormType() == 2) {
        if (country != null) {
            var viewId = "{4871A8DD-C3B5-4EFC-B91B-9B9CF9E88306}";
            var entityName = "bsd_region";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_region'>");
            xml.push("<attribute name='bsd_regionid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_country' uitype='bsd_country' operator='eq' value='" + country[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_regionid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_regionid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_region").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        if (country == null) {
            Xrm.Page.getAttribute("bsd_region").setValue(null);
            Xrm.Page.getAttribute("bsd_province").setValue(null);
            Xrm.Page.getAttribute("bsd_district").setValue(null);
            Xrm.Page.getAttribute("bsd_ward").setValue(null);
            Xrm.Page.getAttribute("bsd_street").setValue(null);
        }
    }
}
//author:Mr.Phong
//description:load province
function load_province() {
    var area = Xrm.Page.getAttribute("bsd_area").getValue();
    if (Xrm.Page.ui.getFormType() == 2) {
        if (area != null) {
            var viewId = "{831690EC-0614-46D2-8BF5-423CE68B6BF8}";
            var entityName = "bsd_province";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_province'>");
            xml.push("<attribute name='bsd_provinceid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_area' uitype='bsd_areab2c' o            perator='eq' value='" + area[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_provinceid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_provinceid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_province").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        if (region == null) {
            Xrm.Page.getAttribute("bsd_province").setValue(null);
            Xrm.Page.getAttribute("bsd_district").setValue(null);
            Xrm.Page.getAttribute("bsd_ward").setValue(null);
            Xrm.Page.getAttribute("bsd_street").setValue(null);
        }
    }
}
//author:Mr.Phong
//description:load district
function load_district() {
    var city = Xrm.Page.getAttribute("bsd_province").getValue();
    var districdefault = Xrm.Page.getAttribute("bsd_district");
    var warddefault = Xrm.Page.getAttribute("bsd_ward");
    if (Xrm.Page.ui.getFormType() == 2) {
        if (city != null) {
            var viewId = "{5F60D1E4-F7DF-476A-88B9-AC0EDE10E321}";
            var entityName = "bsd_district";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_district'>");
            xml.push("<attribute name='bsd_districtid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_province' operator='eq' value='" + city[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_districtid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_districtid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_district").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        else {
            Xrm.Page.getAttribute("bsd_district").setValue(null);
            Xrm.Page.getAttribute("bsd_ward").setValue(null);
            Xrm.Page.getAttribute("bsd_street").setValue(null);
        }
    }
}
//author:Mr.Phong
//description:load ward
function load_ward() {
    var city = Xrm.Page.getAttribute("bsd_province").getValue();
    var district = Xrm.Page.getAttribute("bsd_district").getValue();
    var warddefault = Xrm.Page.getAttribute("bsd_ward");
    if (Xrm.Page.ui.getFormType() == 2) {
        if (city != null && district != null) {
            var viewId = "{AFD95BCE-6F3C-4879-8CBB-888A04FE2F73}";
            var entityName = "bsd_ward";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_ward'>");
            xml.push("<attribute name='bsd_wardid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='new_province' operator='eq' value='" + city[0].id + "' />");
            xml.push("<condition attribute='bsd_district' operator='eq' value='" + district[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_wardid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_wardid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_ward").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        if (district == null) {
            Xrm.Page.getAttribute("bsd_ward").setValue(null);
        }
    }
}
//author:Mr.Đăng
//description:set Required
function set_requireornot(result) {
    debugger;
    var account = getValue("bsd_account");
    var lead = getValue("bsd_lead");
    if (account != null) {
        setRequired("bsd_lead", "none");
        setVisible("bsd_lead", false);
    }
    else if (lead != null) {
        setRequired("bsd_account", "none");
        setVisible("bsd_account", false);
    }
    else if (result != false) {
        setRequired("bsd_account", "none");
        setRequired("bsd_lead", "none");
        setVisible("bsd_account", false);
        setVisible("bsd_lead", false);
    }
}
//Author:Mr.Phong
//Description:hide or show field account
function hideorshowfieldaccount() {
    var account = Xrm.Page.getAttribute("bsd_account").getValue();
    if (account != null) {
        Xrm.Page.ui.controls.get("bsd_account").setVisible(true);
        Xrm.Page.getAttribute("bsd_account").setRequiredLevel("required");
    }
    else {
        Xrm.Page.ui.controls.get("bsd_account").setVisible(false);
        Xrm.Page.getAttribute("bsd_account").setRequiredLevel("none");
    }
}
//Author:Mr.Phong
//Description:filter region
function filter_region() {
    var country = Xrm.Page.getAttribute("bsd_country").getValue();
    Xrm.Page.getControl("bsd_region").removePreSearch(presearch_region);
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    Xrm.Page.getControl("bsd_province").removePreSearch(presearch_province);
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
    Xrm.Page.getControl("bsd_ward").removePreSearch(presearch_ward);
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    if (country != null) {
        var viewId = "{4871A8DD-C3B5-4EFC-B91B-9B9CF9E88306}";
        var entityName = "bsd_region";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_region'>");
        xml.push("<attribute name='bsd_regionid'/>");
        xml.push("<attribute name='bsd_name' />")
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_country' operator='eq' uitype='bsd_country' value='" + country[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_regionid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_regionid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_region").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);     
        Xrm.Page.getAttribute("bsd_region").setValue(null);
        Xrm.Page.getAttribute("bsd_district").setValue(null);
        Xrm.Page.getAttribute("bsd_ward").setValue(null);
        Xrm.Page.getAttribute("bsd_street").setValue(null);
        clear_area();
        clear_province();
        clear_district();
        clear_ward();
    }
    if (country == null) {
        Xrm.Page.getAttribute("bsd_region").setValue(null);
        Xrm.Page.getAttribute("bsd_district").setValue(null);
        Xrm.Page.getAttribute("bsd_ward").setValue(null);
        Xrm.Page.getAttribute("bsd_street").setValue(null);
        clear_region();
        clear_area();
        clear_province();
        clear_district();
        clear_ward();
    }
}
function clear_ward() {
    Xrm.Page.getControl("bsd_ward").addPreSearch(presearch_ward);
}
function presearch_ward() {
    Xrm.Page.getControl("bsd_ward").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_district() {
    Xrm.Page.getControl("bsd_district").addPreSearch(presearch_district);
}
function presearch_district() {
    Xrm.Page.getControl("bsd_district").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_province() {
    Xrm.Page.getControl("bsd_province").addPreSearch(presearch_province);
}
function presearch_province() {
    Xrm.Page.getControl("bsd_province").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_area() {
    Xrm.Page.getControl("bsd_area").addPreSearch(presearch_area);
}
function presearch_area() {
    Xrm.Page.getControl("bsd_area").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_region() {
    Xrm.Page.getControl("bsd_region").addPreSearch(presearch_region);
}
function presearch_region() {
    Xrm.Page.getControl("bsd_region").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.Phong
//Description:set filter lookup when form on create
function setfilterlookupwhenformoncreate() {
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    if (Xrm.Page.ui.getFormType() == 1) {
        var viewId = "{4871A8DD-C3B5-4EFC-B91B-9B9CF9E88306}";
        var entityName = "bsd_region";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_region'>");
        xml.push("<attribute name='bsd_regionid'/>");
        xml.push("<attribute name='bsd_name' />")
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_country' operator='eq' uitype='bsd_country' value='{579BF46F-6396-E611-85CC-000C254C8A2D}' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_regionid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_regionid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_region").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        var viewId1 = "{831690EC-0614-46D2-8BF5-423CE68B6BF8}";
        var entityName1 = "bsd_province";
        var viewDisplayName1 = "test";
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='bsd_province'>");
        xml1.push("<attribute name='bsd_provinceid'/>");
        xml1.push("<attribute name='bsd_name' />")
        xml1.push("<attribute name='createdon' />");
        xml1.push("<order attribute='bsd_name' descending='false' />");
        xml1.push("<filter type='and' >");
        xml1.push("<condition attribute='bsd_region' operator='eq' uitype='bsd_region' value='{CF410709-289B-E641-95F3-000C26EBC56A}' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_provinceid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_provinceid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_province").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
        var viewId2 = "{5F60D1E4-F7DF-476A-88B9-AC0EDE10E321}";
        var entityName2 = "bsd_district";
        var viewDisplayName2 = "test";
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml2.push("<entity name='bsd_district'>");
        xml2.push("<attribute name='bsd_districtid'/>");
        xml2.push("<attribute name='bsd_name' />")
        xml2.push("<attribute name='createdon' />");
        xml2.push("<order attribute='bsd_name' descending='false' />");
        xml2.push("<filter type='and' >");
        xml2.push("<condition attribute='bsd_province' operator='eq' uitype='bsd_province' value='{7DD7FC2F-6695-E611-81CC-000C294C7A3D}' />");
        xml2.push("</filter>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_districtid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_districtid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_district").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
        var viewId3 = "{AFD95BCE-6F3C-4879-8CBB-888A04FE2F73}";
        var entityName3 = "bsd_ward";
        var viewDisplayName3 = "test";
        xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml3.push("<entity name='bsd_ward'>");
        xml3.push("<attribute name='bsd_wardid'/>");
        xml3.push("<attribute name='bsd_name' />")
        xml3.push("<attribute name='createdon' />");
        xml3.push("<order attribute='bsd_name' descending='false' />");
        xml3.push("<filter type='and' >");
        xml3.push("<condition attribute='new_province' operator='eq' uitype='bsd_province' value='{7DD7FC2F-6695-E611-81CC-000C294C7A3D}' />");
        xml3.push("<condition attribute='bsd_district' operator='eq' uitype='bsd_district' value='{09B50BAE-6398-E611-82CC-000C294C7A3D}' />");
        xml3.push("</filter>");
        xml3.push("</entity>");
        xml3.push("</fetch>");
        var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_wardid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_wardid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_ward").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
    }
}

function test()
{
    setvalue("dsad", null);
}

function setvalue(a,b)
{
    debugger;
    if (a.length>0) {
        alert("hello");
    }
 
}
