function AutoLoad() {
    var statecode = Xrm.Page.getAttribute("statecode").getValue();
    //1 la active
    if (statecode != 1 && statecode != 3) {
        set_DisableDeliveryPort();
        set_customer();
        sethideall();
        sethidetax();
        sethideporter();
        sethideshipping();
        set_currentdate();
        loadvaluefieldpricelistonload();
        setshiptoaddressonload();
        filter_gridQuotebyID();
        hidebuttonactivequote();
        setvaluefieldexchangerateonload();
        setdisable_deliveryattheport();
        if (formType() == 2) {
            LK_PotentialCustomer_Change(false);
        } else {
            Load_FromDate_Auto();
            clear_LoadAddressPort();
            clear_priceleve();
        }
    }
}
/*
    Mr: Diệm
    Note: Hàm lấy ngày...dùng chung
*/
function getFullDay(day) {
    var fullday = null;
    var createdatefrom = getValue(day);
    if (createdatefrom != null) {
        var year = createdatefrom.getFullYear();
        var date = createdatefrom.getDate();

        var laymonth = (createdatefrom.getMonth() + 1);
        var month;
        if (laymonth > 0 && laymonth <= 9) {
            month = "0" + laymonth;
        } else {
            month = laymonth;
        }
        fullday = year + "-" + month + "-" + date;
    }
    return fullday;
}
/*
   Mr: Diệm
   Note: Load Price group theo fromdate.
*/
function LK_PotentialCustomer_Change(reset) {
    debugger;
    var customer = getValue("customerid");
    var fullday = getFullDay("effectivefrom");
    if (reset != false) setNull("pricelevelid");
    if (customer != null) {
        CrmFetchKit.Fetch(['<fetch version="1.0" output-format="xml-platform" count="1"  mapping="logical" distinct="true">',
                           '    <entity name="pricelevel">',
                           '        <all-attributes />',
                           '        <order attribute="createdon"/>',
                           '    <filter type="and">',
                           '        <condition attribute="begindate" operator="le" value="' + fullday + '" />',
                           '        <condition attribute="enddate" operator="ge" value="' + fullday + '" />',
                           '        <condition attribute="bsd_account" operator="eq" uitype="account" value="' + customer[0].id + '"/>',
                           '    </filter>',
                           '    </entity>',
                           '</fetch>'].join("")).then(function (rs) {
                               console.log(rs);
                               if (rs.length > 0) /*Nếu dữ liệu Account*/ {
                                   var first = rs[0];
                                   if (getValue("pricelevelid") == null) {
                                       setValue("pricelevelid",
                                            [{
                                                id: first.Id,
                                                name: first.attributes.name.value,
                                                entityType: first.logicalName
                                            }]);
                                   }
                               }
                               else /*Price group*/ {
                                   CrmFetchKit.Fetch(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                                                      '   <entity name="pricelevel">',
                                                      '     <attribute name="name" />',
                                                      '     <attribute name="enddate" />',
                                                      '     <attribute name="begindate" />',
                                                      '     <attribute name="pricelevelid" />',
                                                      '     <order attribute="name" descending="false" />',
                                                      '     <filter type="and">',
                                                      '        <condition attribute="begindate" operator="le" value="' + fullday + '" />',
                                                      '        <condition attribute="enddate" operator="ge" value="' + fullday + '" />',
                                                      '     </filter>',
                                                      '     <link-entity name="bsd_pricegroups" from="bsd_pricegroupsid" to="bsd_pricegroups" alias="ai">',
                                                      '       <link-entity name="account" from="bsd_pricegroups" to="bsd_pricegroupsid" alias="aj">',
                                                      '         <filter type="and">',
                                                      '           <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                                                      '         </filter>',
                                                      '       </link-entity>',
                                                      '     </link-entity>',
                                                      '    </entity>',
                                                      '</fetch>'].join("")).then(function (rs1) {
                                                          console.log(rs1);
                                                          if (rs1.length > 0) {
                                                              var first = rs1[0];
                                                              if (getValue("pricelevelid") == null) {
                                                                  setValue("pricelevelid",
                                                                       [{
                                                                           id: first.Id,
                                                                           name: first.attributes.name.value,
                                                                           entityType: first.logicalName
                                                                       }]);
                                                              }
                                                          } else {
                                                              m_alert("Account không chứa price list hoặc price list đã hết hạn");
                                                          }
                                                      }, function (er) {
                                                          alert("không lấy được bảng");
                                                      });
                               }
                           }, function (er) {
                               alert("không lấy được bảng");
                           });
    }
    else if (reset != false) {
        clear_priceleve();
    }
}
/*
    Mr: Diệm
    Note: Set Function null and clear.
*/
function clear_priceleve() {
    getControl("pricelevelid").addPreSearch(presearch_priceleve);
}
function presearch_priceleve() {
    getControl("pricelevelid").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author: Mr.Phong
function filter_gridQuotebyID() {
   
    //RelatedCars : is name of subgrid given on Form.
    // var objSubGrid = document.getElementById("subgridQuote");
    //var objSubGrid = Xrm.Page.getControl("subgridQuote").getGrid();
    var objSubGrid = window.parent.document.getElementById("subgridQuote");
    //CRM loads subgrid after form is loaded.. so when we are adding script on form load.. need to wait unitil subgrid is loaded. Thats why adding delay..
    if (objSubGrid == null) {
        setTimeout(filter_gridQuotebyID, 2000);
        return;
    } else {
        //when subgrid is loaded, get category value
        var name = Xrm.Page.getAttribute("name").getValue();
        if (name != null) {
            //Create FetchXML for sub grid to filter records based on category
            var FetchXml = "<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0'>" +
                              "<entity name='quote'>" +
                                    "<attribute name='name' />" +
                                    "<attribute name='customerid' />" +
                                    "<attribute name='statecode' />" +
                                    "<attribute name='totalamount' />" +
                                    "<attribute name='quoteid' />" +
                                    "<attribute name='createdon' />" +
                                    "<order attribute='name' descending='false' />" +
                                    "<filter type='and'>" +
                                        "<condition attribute='name' operator='eq' value='" + name + "' />" +
                                    "</filter>" +
                                "</entity>" +
                              "</fetch>";
            //apply layout and filtered fetchXML
            if (objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", FetchXml);
                objSubGrid.control.Refresh();
                //document.getElementById("subgridQuote").control.SetParameter("fetchXml", FetchXml)
                ////objSubGrid.control.SetParameter("fetchXml", FetchXml);
                ////Refresh grid to show filtered records only. 
                //document.getElementById("subgridQuote").control.Refresh();
            } else {
                setTimeout(filter_gridQuotebyID, 500);
            }
        }
    }
}
//Author:Mr.Phong
//description:filter type customer and distributor
function set_customer() {
    Xrm.Page.getAttribute("bsd_priceoftransportationn").setSubmitMode("always");
    Xrm.Page.getAttribute("bsd_unitshipping").setSubmitMode("always");
    var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    var entityName = "account";
    var viewDisplayName = "test";
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='account'>");
    xml.push("<attribute name='accountid' />");
    xml.push("<attribute name='name'/>");
    xml.push("<order attribute='name' descending='false' />");
    xml.push("<filter type='and' >");
    xml.push("<condition attribute='bsd_accounttype' operator='in'>");
    xml.push("<value>");
    xml.push("861450000");
    xml.push("</value>");
    xml.push("<value>");
    xml.push("100000000");
    xml.push("</value>");
    xml.push("</condition>");
    xml.push("<condition attribute='statecode' operator='eq' value='0'/>");
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='accountid'>  " +
                       "<cell name='name'    " + "width='200' />  " +
                       "</row>   " +
                    "</grid>   ";
    Xrm.Page.getControl("customerid").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    Xrm.Page.getControl("customerid").addPreSearch(addFilter);
    Xrm.Page.getControl("bsd_partnerscompany").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    Xrm.Page.getControl("bsd_invoicenameaccount").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
}
function addFilter() {
    var customerAccountFilter = "<filter type='and'><condition attribute='contactid' operator='null' /></filter>";
    Xrm.Page.getControl("customerid").addCustomFilter(customerAccountFilter, "contact");
}
//Author:Mr.Phong
//Description:an tat ca cac filed trong section tax,shipping,porter
function sethideall() {
    if (Xrm.Page.ui.getFormType() == 1) {
        set_visible(["bsd_priceofporter", "bsd_priceporter", "bsd_value", "bsd_typeofunit", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_unitshipping", "bsd_portertype"], 0);
    }
}
//Author:Mr.Phong
//Description:lay ngay hien tai gan vao field effective from
function set_currentdate() {
    if (Xrm.Page.ui.getFormType() == 1) {
        var currentDateTime = new Date();
        Xrm.Page.getAttribute("effectivefrom").setValue(currentDateTime);
    }
}
//author:Mr.Phong
//description:show only account not contact
function OnLoad() {
    addEventHandler();
}
//author:Mr.Phong
//description:show only account not contact
function addEventHandler() {
    Xrm.Page.getControl("customerid").addPreSearch(addFilterToShowAccounts);
}
//author:Mr.Phong
//description:show only account not contact
function addFilterToShowAccounts() {
    var account_filter = "<filter type='and'>" +
    "<condition attribute='contactid' operator='null' />" +
    "</filter>";
    Xrm.Page.getControl("customerid").addCustomFilter(account_filter, "contact");
}
//author:Mr.Phong
//description:get customer information
function get_customerinformation() {
    Xrm.Page.getAttribute("bsd_paymentterm").setSubmitMode("always");
    Xrm.Page.getAttribute("bsd_paymentmethodd").setSubmitMode("always");
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    var fielddefault = Xrm.Page.getAttribute("bsd_warehouseto");
    var fieldaddress = Xrm.Page.getAttribute("bsd_address");
    var fieldpaymentterm = Xrm.Page.getAttribute("bsd_paymentterm");
    var fieldpaymentmethod = Xrm.Page.getAttribute("bsd_paymentmethodd");
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount");
    var addressinvoicenameaccount = Xrm.Page.getAttribute("bsd_addressinvoiceaccount");
    var partnerscompany = Xrm.Page.getAttribute("bsd_partnerscompany");
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml5 = [];
    var xml6 = [];
    var xml3 = [];
    var xml7 = [];
    var xml8 = [];
    var xml12 = [];
    var xml13 = [];
    var xml14 = [];
    var xml15 = [];
    if (customer != null) {
        if (customer[0].type == 1) {
            Xrm.Page.getControl("bsd_address").setDisabled(false);
            Xrm.Page.getControl("bsd_shiptoaddress").setDisabled(false);
            Xrm.Page.getControl("bsd_addressinvoiceaccount").setDisabled(false);
            Xrm.Page.getAttribute("bsd_tax").setValue(0);
            Xrm.Page.getAttribute("bsd_value").setValue(null);
            Xrm.Page.ui.controls.get("bsd_value").setVisible(false);
            Xrm.Page.getAttribute("bsd_transportation").setValue(0);
            Xrm.Page.getAttribute("bsd_shippingpricelistname").setValue(null);
            Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(0);
            Xrm.Page.getAttribute("bsd_unitshipping").setValue(null);
            Xrm.Page.ui.controls.get("bsd_shippingpricelistname").setVisible(false);
            Xrm.Page.ui.controls.get("bsd_priceoftransportationn").setVisible(false);
            Xrm.Page.ui.controls.get("bsd_unitshipping").setVisible(false);
            Xrm.Page.ui.controls.get("bsd_typeofunit").setVisible(false);
            Xrm.Page.ui.controls.get("bsd_portertype").setVisible(false);
            Xrm.Page.getAttribute("bsd_porteroption").setValue(0);
            Xrm.Page.getAttribute("bsd_priceofporter").setValue(null);
            Xrm.Page.getAttribute("bsd_priceporter").setValue(0);
            Xrm.Page.ui.controls.get("bsd_priceofporter").setVisible(false);
            Xrm.Page.ui.controls.get("bsd_priceporter").setVisible(false);
            Xrm.Page.getControl("bsd_porteroption").setDisabled(false);
            Xrm.Page.getAttribute("bsd_partnerscompany").setValue(null);
            Xrm.Page.getAttribute("bsd_shiptoaddress").setValue(null);
            Xrm.Page.getAttribute("bsd_warehouseto").setValue(null);
            Xrm.Page.getControl("bsd_warehouseto").setDisabled(false);
            var acc = customer;
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='account'>");
            xml.push("<attribute name='name' />");
            xml.push("<attribute name='primarycontactid' />");
            xml.push("<attribute name='telephone1' />");
            xml.push("<attribute name='accountid' />");
            xml.push("<attribute name='bsd_taxregistration' />");
            xml.push("<attribute name='fax' />");
            xml.push("<attribute name='accountnumber'/>");
            xml.push("<attribute name='emailaddress1'/>");
            xml.push("<attribute name='bsd_paymentterm'/>");
            xml.push("<attribute name='bsd_paymentmethod'/>");
            xml.push("<order attribute='createdon' descending='false' />");
            xml.push("<filter type='and'>");
            xml.push("<condition attribute='accountid' operator='eq' uitype='account ' value='" + acc[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    invoicenameaccount.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.name.value,
                        entityType: rs[0].logicalName
                    }]);
                    partnerscompany.setValue([{
                        id: customer[0].id,
                        name: customer[0].name,
                        entityType: 'account'
                    }]);
                    Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(rs[0].attributes.accountnumber.value);
                    if (rs[0].attributes.telephone1 != null) {
                        Xrm.Page.getAttribute("bsd_telephone").setValue(rs[0].attributes.telephone1.value);
                    }
                    if (rs[0].attributes.telephone1 == null) {
                        Xrm.Page.getAttribute("bsd_telephone").setValue(null);
                    }
                    if (rs[0].attributes.bsd_taxregistration != null) {
                        Xrm.Page.getAttribute("bsd_taxregistration").setValue(rs[0].attributes.bsd_taxregistration.value);
                    }
                    if (rs[0].attributes.bsd_taxregistration == null) {
                        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                    }
                    if (rs[0].attributes.fax != null) {
                        Xrm.Page.getAttribute("bsd_fax").setValue(rs[0].attributes.fax.value);
                    }
                    if (rs[0].attributes.fax == null) {
                        Xrm.Page.getAttribute("bsd_fax").setValue(null);
                    }
                    if (rs[0].attributes.accountnumber != null) {
                        var no = rs[0].attributes.accountnumber.value;
                        Xrm.Page.getAttribute("bsd_customercode").setValue(no);
                    }
                    if (rs[0].attributes.accountnumber == null) {
                        Xrm.Page.getAttribute("bsd_customercode").setValue(null);
                    }
                    if (rs[0].attributes.emailaddress1 != null) {
                        Xrm.Page.getAttribute("bsd_mail").setValue(rs[0].attributes.emailaddress1.value);
                    }
                    if (rs[0].attributes.emailaddress1 == null) {
                        Xrm.Page.getAttribute("bsd_mail").setValue(null);
                    }
                    if (rs[0].attributes.bsd_paymentterm != null) {
                        //fieldpaymentterm.setValue(rs[0].attributes.bsd_paymentterm.name);
                        fieldpaymentterm.setValue([{
                            id: rs[0].attributes.bsd_paymentterm.guid,
                            name: rs[0].attributes.bsd_paymentterm.name,
                            entityType: 'bsd_paymentterm'
                        }]);
                    }
                    if (rs[0].attributes.bsd_paymentterm == null) {
                        //fieldpaymentterm.setValue(rs[0].attributes.bsd_paymentterm.name);
                        fieldpaymentterm.setValue(null);
                    }
                    if (rs[0].attributes.bsd_paymentmethod != null) {
                        //fieldpaymentmethod.setValue(rs[0].attributes.bsd_paymentmethod.name);
                        fieldpaymentmethod.setValue([{
                            id: rs[0].attributes.bsd_paymentmethod.guid,
                            name: rs[0].attributes.bsd_paymentmethod.name,
                            entityType: 'bsd_methodofpayment'
                        }]);
                    }
                    if (rs[0].attributes.bsd_paymentmethod == null) {
                        //fieldpaymentmethod.setValue(rs[0].attributes.bsd_paymentmethod.name);
                        fieldpaymentmethod.setValue(null);
                    }
                }
                var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName1 = "bsd_address";
                var viewDisplayName1 = "test";
                fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, customer[0].id]);                
                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                    if (rs1.length > 0) {
                        if (rs1[0].attributes.bsd_name != null) {
                            fieldaddress.setValue([{
                                id: rs1[0].Id,
                                name: rs1[0].attributes.bsd_name.value,
                                entityType: rs1[0].logicalName
                            }]);
                        }
                        Xrm.Page.getControl("bsd_address").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                    }
                    if (rs1.length == 0) {
                        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                        var entityName = "bsd_address";
                        var viewDisplayName = "test";
                        fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                        ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);                      
                        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                              "<row name='result'  " + "id='bsd_addressid'>  " +
                              "<cell name='bsd_name'   " + "width='200' />  " +
                              "<cell name='createdon'    " + "width='100' />  " +
                                "<cell name='bsd_purpose'    " + "width='100' />  " +
                              "</row>   " +
                           "</grid>   ";

                        CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                            if (rs2.length > 0) {
                                if (rs2[0].attributes.bsd_name != null) {
                                    fieldaddress.setValue([{
                                        id: rs2[0].Id,
                                        name: rs2[0].attributes.bsd_name.value,
                                        entityType: rs2[0].logicalName
                                    }]);
                                    Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
                                }
                            }
                            if (rs2.length == 0) {
                                fieldaddress.setValue(null);
                                Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
                            }
                        },
            function (er) {
                console.log(er.message)
            });
                    }
                },
            function (er) {
                console.log(er.message)
            });
            },
            function (er) {
                console.log(er.message)
            });
            var viewId14 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
            var entityName14 = "bsd_address";
            var viewDisplayName14 = "test";
            fetch(xml14, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customer[0].id]);         
            var layoutXml14 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='bsd_addressid'>  " +
                         "<cell name='bsd_name'   " + "width='200' />  " +
                         "<cell name='createdon'    " + "width='100' />  " +
                           "<cell name='bsd_purpose'    " + "width='100' />  " +
                         "</row>   " +
                      "</grid>   ";
            CrmFetchKit.Fetch(xml14.join(""), true).then(function (rs14) {
                if (rs14.length > 0) {
                    fieldshiptoaddress.setValue([{
                        id: rs14[0].Id,
                        name: rs14[0].attributes.bsd_name.value,
                        entityType: rs14[0].logicalName
                    }]);
                    Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId14, entityName14, viewDisplayName14, xml14.join(""), layoutXml14, true);
                }
                if (rs14.length == 0) {
                    var viewId15 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                    var entityName15 = "bsd_address";
                    var viewDisplayName15 = "test";
                    fetch(xml15, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                        ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);                  
                    var layoutXml15 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='bsd_addressid'>  " +
                                 "<cell name='bsd_name'   " + "width='200' />  " +
                                 "<cell name='createdon'    " + "width='100' />  " +
                                   "<cell name='bsd_purpose'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                    Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId15, entityName15, viewDisplayName15, xml15.join(""), layoutXml15, true);
                }

            }, function (er) {
                console.log(er.message)
            });
            var viewId12 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
            var entityName12 = "bsd_address";
            var viewDisplayName12 = "test";
            fetch(xml12, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
              ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Invoice" + "%", 1, "0", 2, customer[0].id]);     
            var layoutXml12 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='bsd_addressid'>  " +
                         "<cell name='bsd_name'   " + "width='200' />  " +
                         "<cell name='createdon'    " + "width='100' />  " +
                           "<cell name='bsd_purpose'    " + "width='100' />  " +
                         "</row>   " +
                      "</grid>   ";
            CrmFetchKit.Fetch(xml12.join(""), true).then(function (rs12) {
                if (rs12.length > 0) {
                    if (rs12[0].attributes.bsd_name != null) {
                        addressinvoicenameaccount.setValue([{
                            id: rs12[0].Id,
                            name: rs12[0].attributes.bsd_name.value,
                            entityType: rs12[0].logicalName
                        }]);
                    }
                    Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId12, entityName12, viewDisplayName12, xml12.join(""), layoutXml12, true);
                }
                if (rs12.length == 0) {
                    var viewId13 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                    var entityName13 = "bsd_address";
                    var viewDisplayName13 = "test";
                    fetch(xml13, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);                   
                    var layoutXml13 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                          "<row name='result'  " + "id='bsd_addressid'>  " +
                          "<cell name='bsd_name'   " + "width='200' />  " +
                          "<cell name='createdon'    " + "width='100' />  " +
                            "<cell name='bsd_purpose'    " + "width='100' />  " +
                          "</row>   " +
                       "</grid>   ";
                    CrmFetchKit.Fetch(xml13.join(""), true).then(function (rs13) {
                        if (rs13.length > 0) {
                            if (rs13[0].attributes.bsd_name != null) {
                                if (rs13[0].attributes.bsd_name != null) {
                                    addressinvoicenameaccount.setValue([{
                                        id: rs13[0].Id,
                                        name: rs13[0].attributes.bsd_name.value,
                                        entityType: rs13[0].logicalName
                                    }]);
                                }
                                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId13, entityName13, viewDisplayName13, xml13.join(""), layoutXml13, true);
                            }
                        }
                        if (rs13.length == 0) {
                            addressinvoicenameaccount.setValue(null);
                            Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId13, entityName13, viewDisplayName13, xml13.join(""), layoutXml13, true);
                        }
                    },
        function (er) {
            console.log(er.message)
        });
                }

            },
        function (er) {
            console.log(er.message)
        });
        }
        //xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        //xml3.push("<entity name='bsd_address'>");
        //xml3.push("<attribute name='bsd_addressid' />");
        //xml3.push("<attribute name='bsd_name' />");
        //xml3.push("<attribute name='createdon' />");
        //xml3.push("<attribute name='bsd_purpose' />");
        //xml3.push("<order attribute='bsd_name' descending='false' />");
        //xml3.push("<filter type='and'>");
        //xml3.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='" + acc[0].id + "' />");
        //xml3.push("<condition attribute='bsd_purpose' operator='eq' uitype='account ' value='861450002' />");
        //xml3.push("</filter>");
        //xml3.push("</entity>");
        //xml3.push("</fetch>");
        //CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
        //    if (rs3.length > 0) {
        //        Xrm.Page.getControl("bsd_shiptoaddress").setDisabled(false);
        //        if (rs3[0].attributes.bsd_name != null) {
        //            fieldshiptoaddress.setValue([{
        //                id: rs3[0].Id,
        //                name: rs3[0].attributes.bsd_name.value,
        //                entityType: rs3[0].logicalName
        //            }]);
        //            var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        //            var entityName = "bsd_address";
        //            var viewDisplayName = "test";
        //            xml7.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        //            xml7.push("<entity name='bsd_address'>");
        //            xml7.push("<attribute name='bsd_addressid' />");
        //            xml7.push("<attribute name='bsd_name' />");
        //            xml7.push("<attribute name='createdon' />");
        //            xml7.push("<attribute name='bsd_purpose' />");
        //            xml7.push("<order attribute='bsd_name' descending='false' />");
        //            xml7.push("<filter type='and'>");
        //            xml7.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='" + acc[0].id + "' />");
        //            xml7.push("</filter>");
        //            xml7.push("</entity>");
        //            xml7.push("</fetch>");
        //            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
        //                  "<row name='result'  " + "id='bsd_addressid'>  " +
        //                  "<cell name='bsd_name'   " + "width='200' />  " +
        //                  "<cell name='createdon'    " + "width='100' />  " +
        //                    "<cell name='bsd_purpose'    " + "width='100' />  " +
        //                  "</row>   " +
        //               "</grid>   ";
        //            Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId, entityName, viewDisplayName, xml7.join(""), layoutXml, true);
        //        }
        //    }
        //    else
        //    {
        //        var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        //        var entityName2 = "bsd_address";
        //        var viewDisplayName2 = "test";
        //        xml8.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        //        xml8.push("<entity name='bsd_address'>");
        //        xml8.push("<attribute name='bsd_addressid' />");
        //        xml8.push("<attribute name='bsd_name' />");
        //        xml8.push("<attribute name='createdon' />");
        //        xml8.push("<attribute name='bsd_purpose' />");
        //        xml8.push("<order attribute='bsd_name' descending='false' />");
        //        xml8.push("<filter type='and'>");
        //        xml8.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='" + acc[0].id + "' />");
        //        xml8.push("</filter>");
        //        xml8.push("</entity>");
        //        xml8.push("</fetch>");
        //        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
        //              "<row name='result'  " + "id='bsd_addressid'>  " +
        //              "<cell name='bsd_name'   " + "width='200' />  " +
        //              "<cell name='createdon'    " + "width='100' />  " +
        //                "<cell name='bsd_purpose'    " + "width='100' />  " +
        //              "</row>   " +
        //           "</grid>   ";
        //        Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId2, entityName2, viewDisplayName2, xml8.join(""), layoutXml2, true);
        //        fieldshiptoaddress.setValue(null);
        //    }
        //},
        //function (er) {
        //    console.log(er.message)
        //});

    }
    else {
        Xrm.Page.getAttribute("bsd_telephone").setValue(null);
        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
        Xrm.Page.getAttribute("bsd_fax").setValue(null);
        Xrm.Page.getAttribute("bsd_customercode").setValue(null);
        Xrm.Page.getAttribute("bsd_mail").setValue(null);
        Xrm.Page.getAttribute("bsd_paymentterm").setValue(null);
        Xrm.Page.getAttribute("bsd_paymentmethodd").setValue(null);
        Xrm.Page.getAttribute("bsd_contact").setValue(null);
        Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(null);
        Xrm.Page.getAttribute("bsd_invoicenameaccount").setValue(null);
        Xrm.Page.getAttribute("bsd_addressinvoiceaccount").setValue(null);
        Xrm.Page.getControl("bsd_address").setDisabled(true);
        Xrm.Page.getControl("bsd_shiptoaddress").setDisabled(true);
        fieldaddress.setValue(null);
        fieldshiptoaddress.setValue(null);
        fielddefault.setValue(null);
    }
}
//author:Mr.Phong
//description:load contact theo account
function get_contact() {
    var contactdefault = Xrm.Page.getAttribute("bsd_contact");
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var xml = [];
    var xml1 = [];
    if (customer != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xml.push("<entity name='account'>");
        xml.push("<attribute name='name' />");
        xml.push("<attribute name='primarycontactid'/>");
        xml.push("<attribute name='telephone1'/>");
        xml.push("<attribute name='accountid'/>");
        xml.push("<order attribute='name' descending='false'/> ");
        xml.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "'/>");
        xml.push("<condition attribute='bsd_contacttype' operator='eq' value='861450000' />");
        xml.push("</filter>");
        xml.push("</link-entity>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                contactdefault.setValue([{
                    id: rs[0].attributes.primarycontactid.guid,
                    name: rs[0].attributes.primarycontactid.name,
                    entityType: 'contact'
                }]);
                var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityName = "contact";
                var viewDisplayName = "test";
                xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml1.push("<entity name='contact'>");
                xml1.push("<attribute name='fullname'/>");
                xml1.push("<attribute name='telephone1'/>")
                xml1.push("<attribute name='contactid'/>");
                xml1.push("<order attribute='fullname' descending='true' />");
                xml1.push("<filter type='and'>");
                xml1.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
                xml1.push("<condition attribute='bsd_contacttype' operator='eq' value='861450000' />");
                xml1.push("</filter>");
                xml1.push("</entity>");
                xml1.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_contact").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
            }
            if (rs.length == 0) {
                var viewId1 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityName1 = "contact";
                var viewDisplayName1 = "test";
                xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml1.push("<entity name='contact'>");
                xml1.push("<attribute name='fullname'/>");
                xml1.push("<attribute name='telephone1'/>")
                xml1.push("<attribute name='contactid'/>");
                xml1.push("<order attribute='fullname' descending='true' />");
                xml1.push("<filter type='and'>");
                xml1.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
                xml1.push("<condition attribute='bsd_contacttype' operator='eq' value='861450000' />");
                xml1.push("</filter>");
                xml1.push("</entity>");
                xml1.push("</fetch>");
                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_contact").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                    if (rs1.length > 0 && rs1[0].attributes.fullname != null) {
                        contactdefault.setValue([{
                            id: rs1[0].Id,
                            name: rs1[0].attributes.fullname.value,
                            entityType: rs1[0].logicalName
                        }]);
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.Đăng
//Description: Set Disable when Quote type
function setdisable_deliveryattheport() {
    var quotationtype = getValue("bsd_quotationtype");
    if (quotationtype == 861450001) // Nc ngoài
    {
        setDisabled("bsd_deliveryattheport", false);
    }
    else {
        setDisabled("bsd_deliveryattheport", true);
        setValue("bsd_deliveryattheport", 0);
        set_DisableDeliveryPort();
    }
}
//Author:Mr.Đăng
//Description: Disable when Delivery At The Port is No
function set_DisableDeliveryPort() {
    var deliveryport = getValue("bsd_deliveryattheport");
    if (deliveryport == "0") {
        setVisible(["bsd_port", "bsd_addressport"], false);
        set_value(["bsd_port", "bsd_addressport"], null);
        clear_LoadAddressPort();
    }
    if (deliveryport == "1") {
        setVisible(["bsd_port", "bsd_addressport"], true);
        ChooseAccountPort();
    }
}
//Author:Mr.Đăng
//Description: Choose Account in Port
function ChooseAccountPort() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="accountid" />',
                    '<order attribute="name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="861450004" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");

    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
							   "<row name='result'  " + "id='accountid'>  " +
							   "<cell name='name'   " + "width='200' />  " +
							   "<cell name='createdon'    " + "width='100' />  " +
							   "</row>   " +
							"</grid>";
    getControl("bsd_port").addCustomView(getControl("bsd_port").getDefaultView(), "account", "Port", fetchxml, layoutXml, true);
    LoadAddressPort(false);
}
//Author:Mr.Đăng
//Description: Load Address Port
function LoadAddressPort(result) {
    if (result != false) setNull("bsd_addressport");
    var accountport = getValue("bsd_port");
    if (accountport != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + accountport[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            console.log(rs);
            if (rs.length > 0 && result != false) {
                var first = rs[0];
                if (getValue("bsd_addressport") == null) {
                    setValue("bsd_addressport", [{
                        id: first.Id,
                        name: first.attributes.bsd_name.value,
                        entityType: first.logicalName
                    }]
                   );
                }
            }

        }, function (er) { });
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_addressid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>";
        getControl("bsd_addressport").removePreSearch(presearch_LoadAddressPort);
        getControl("bsd_addressport").addCustomView(getControl("bsd_addressport").getDefaultView(), "bsd_address", "Address", fetchxml, layoutXml, true);
    }
    else if (result != false) {
        clear_LoadAddressPort();
    }
}
function clear_LoadAddressPort() {
    getControl("bsd_addressport").addPreSearch(presearch_LoadAddressPort);
}
function presearch_LoadAddressPort() {
    getControl("bsd_addressport").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.phong
//Description:set value field exchange rate
function setvaluefieldexchangerateonload() {  
    var currency = Xrm.Page.getAttribute("transactioncurrencyid").getValue();
    var exchangerate = Xrm.Page.getAttribute("exchangerate").getValue();
    var xml = [];
    if (currency != null && exchangerate == null) {
        fetch(xml, "transactioncurrency", ["transactioncurrencyid", "currencyname", "isocurrencycode", "currencysymbol", "exchangerate", "currencyprecision"], "currencyname", false, null
           , ["transactioncurrencyid"], ["eq"], [0, currency[0].id, 1]);
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(rs[0].attributes.exchangerate.value);
            }
            if (rs.length == 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(null);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.phong
//Description:set value field exchange rate
function setvaluefieldexchangerateonchange() {
    var currency = Xrm.Page.getAttribute("transactioncurrencyid").getValue();
    var xml = [];
    if (currency != null) {
        fetch(xml, "transactioncurrency", ["transactioncurrencyid", "currencyname", "isocurrencycode", "currencysymbol", "exchangerate", "currencyprecision"], "currencyname", false, null
           , ["transactioncurrencyid"], ["eq"], [0, currency[0].id, 1]);
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(rs[0].attributes.exchangerate.value);
            }
            if (rs.length == 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(null);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    if (currency == null) {
        Xrm.Page.getAttribute("exchangerate").setValue(null);
    }
}
//Author:Mr.Đăng
//Description:set Date
function Load_FromDate_Auto() {
    var date = new Date();
    setValue("bsd_date", date);
}
//Author:Mr.Phong
//Description:an field trong section shipping
function sethideshipping() {
    var a = [];
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    a = get_value(["bsd_shiptoaddress", "bsd_warehouseto", "bsd_typeofunit", "bsd_transportation"]);
    var filedunit = Xrm.Page.getAttribute("bsd_unitshipping");
    var shippingpricelist = Xrm.Page.getAttribute("bsd_shippingpricelistname");
    if (a[5] == 1)//shipping = yes
    {
        if (Xrm.Page.getAttribute("bsd_portertype").getValue() == null) {
            set_visible(["bsd_typeofunit", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_unitshipping", "bsd_portertype"], 1);
            set_requirelevel(["bsd_shippingpricelistname"], 1);
            var effectivefrom = Xrm.Page.getAttribute("effectivefrom").getValue();
            var year = effectivefrom.getFullYear() + "";
            if (effectivefrom.getMonth() + 1 <= 9) {
                var month = "0" + (effectivefrom.getMonth() + 1) + "";
            }
            else {
                var month = (effectivefrom.getMonth() + 1) + "";
            }
            var day = effectivefrom.getDate() + "";
            var dateFormat = year + "-" + month + "-" + day;
            if (a[0] != null && a[2] != null) {
                fetch(xml, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_ward", "bsd_street", "bsd_province", "bsd_district"], "bsd_name", false, null
                    , ["bsd_addressid"], ["eq"], [0, a[0], 1]);
                CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
                    if (rs.length > 0) {
                        if (rs[0].attributes.bsd_district != null) {
                            var province = rs[0].attributes.bsd_province.guid;
                            var district = rs[0].attributes.bsd_district.guid;
                            fetch(xml1, "bsd_addressshipping", ["bsd_addressshippingid", "bsd_name", "createdon"], "bsd_name", false, null
                            , ["bsd_province", "bsd_district"], ["eq", "eq"], [0, province, 1, district]);
                            CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
                                if (rs1.length > 0) {
                                    fetch(xml2, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom"], "bsd_name", false, null
                                     , ["bsd_warehousefrom", "bsd_addressto", "bsd_deliverymethod", "bsd_effectivefrom"], ["eq", "eq", "eq", "on-or-before"]
                                     , [0, a[2], 1, rs1[0].Id, 2, a[4], 3, dateFormat]);
                                    CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs2) {
                                        if (rs2.length > 0) {
                                            var dayfrom = Xrm.Page.getAttribute("effectivefrom").getValue();
                                            var yearfrom = dayfrom.getFullYear();
                                            if (dayfrom.getMonth() + 1 <= 9) {
                                                var monthfrom = "0" + (dayfrom.getMonth() + 1);
                                            }
                                            else {
                                                var monthfrom = (dayfrom.getMonth() + 1);
                                            }
                                            var dayeffectivefrom = dayfrom.getDate();
                                            var b = [];
                                            for (var i = 0; i < rs2.length; i++) {
                                                var dayfromshipping = rs2[i].attributes.bsd_effectivefrom.value;
                                                var yearfromshipping = dayfromshipping.getFullYear();
                                                if (dayfromshipping.getMonth() + 1 <= 9) {
                                                    var monthfromshipping = "0" + (dayfromshipping.getMonth() + 1);
                                                }
                                                else {
                                                    var monthfromshipping = (dayfromshipping.getMonth() + 1);
                                                }
                                                var dayeffectivefromshipping = dayfromshipping.getDate();
                                                var daytoshipping = rs2[i].attributes.bsd_effectiveto.value;
                                                var yeartoshipping = daytoshipping.getFullYear();
                                                if (daytoshipping.getMonth() + 1 <= 9) {
                                                    var monthtoshipping = "0" + (daytoshipping.getMonth() + 1);
                                                }
                                                else {
                                                    var monthtoshipping = (daytoshipping.getMonth() + 1);
                                                }
                                                var dayeffectivetoshipping = daytoshipping.getDate();
                                                if (yearfromshipping < yearfrom && yeartoshipping < yearfrom) {
                                                    a.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                }
                                                if (yearfromshipping < yearfrom && yeartoshipping == yearfrom) {
                                                    if (monthtoshipping < monthfrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                    if (monthtoshipping == monthfrom) {
                                                        if (dayeffectivetoshipping < dayeffectivefrom) {
                                                            b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                        }
                                                    }
                                                }
                                                if (yearfromshipping == yearfrom && yeartoshipping == yearfrom) {
                                                    if (monthfromshipping > monthfrom && monthtoshipping < monthfrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                    if (monthfromshipping > monthfrom && monthtoshipping == monthfrom) {
                                                        if (dayeffectivetoshipping < dayeffectivefrom) {
                                                            b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                        }
                                                    }
                                                    if (monthfromshipping < monthfrom && monthtoshipping < monthfrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                    if (monthfromshipping < monthfrom && monthtoshipping == monthfrom) {
                                                        if (dayeffectivetoshipping < dayeffectivefrom) {
                                                            b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                        }

                                                    }
                                                    if (monthfromshipping == monthfrom && monthtoshipping < monthfrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                    if (monthfromshipping == monthfrom && monthtoshipping == monthfrom) {
                                                        if (dayeffectivetoshipping < dayeffectivefrom) {
                                                            b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                        }
                                                    }
                                                }
                                            }
                                            if (b.length > 0) {
                                                var viewId3 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                                var entityName3 = "bsd_shippingpricelist";
                                                var viewDisplayName3 = "test";
                                                xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                                xml3.push("<entity name='bsd_shippingpricelist'>");
                                                xml3.push("<attribute name='bsd_shippingpricelistid'/>");
                                                xml3.push("<attribute name='bsd_name'/>");
                                                xml3.push("<attribute name='createdon'/>");
                                                xml3.push("<attribute name='bsd_effectiveto'/>");
                                                xml3.push("<attribute name='bsd_effectivefrom'/>");
                                                xml3.push("<attribute name='bsd_priceunitporter'/>");
                                                xml3.push("<attribute name='bsd_priceofton'/>");
                                                xml3.push("<attribute name='bsd_pricetripporter'/>");
                                                xml3.push("<attribute name='bsd_priceoftrip'/>");
                                                xml3.push("<attribute name='bsd_unit'/>");
                                                xml3.push("<attribute name='bsd_porter'/>");
                                                xml3.push("<order attribute='createdon' descending='true' />");
                                                xml3.push("<filter type='and'>");
                                                xml3.push("<condition attribute='bsd_warehousefrom' operator='eq' uitype='bsd_warehouseentity' value='" + a[2] + "'/>");
                                                xml3.push("<condition attribute='bsd_addressto' operator='eq' uitype='bsd_addressshipping'  value='" + rs1[0].Id + "'/>");
                                                xml3.push("<condition attribute='bsd_deliverymethod' operator='eq' value='" + a[4] + "'/>");
                                                xml3.push("<condition attribute='bsd_effectivefrom' operator='on-or-before' value='" + dateFormat + "'/>");
                                                xml3.push("<condition attribute='bsd_key' operator='eq' value='1'/>");
                                                xml3.push("<condition attribute='bsd_shippingpricelistid' operator='not-in'>");
                                                for (k = 0; k < b.length; k++) {
                                                    xml3.push("<value uitype='bsd_shippingpricelist'>" + '{' + b[k] + '}' + "");
                                                    xml3.push("</value>");
                                                }
                                                xml3.push("</condition>");
                                                xml3.push("</filter>");
                                                xml3.push("</entity>");
                                                xml3.push("</fetch>");
                                                var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                             "<cell name='bsd_name'    " + "width='200' />  " +
                             "</row>   " +
                          "</grid>   ";
                                            }
                                            if (b.length == 0) {
                                                var viewId3 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                                var entityName3 = "bsd_shippingpricelist";
                                                var viewDisplayName3 = "test";
                                                fetch(xml3, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom", "bsd_priceunitporter", "bsd_priceofton"
                                                , "bsd_pricetripporter", "bsd_priceoftrip", "bsd_unit", "bsd_porter"]
                                                , "bsd_name", false, null
                                                , ["bsd_warehousefrom", "bsd_addressto", "bsd_deliverymethod", "bsd_effectivefrom", "bsd_key"], ["eq", "eq", "eq", "on-or-before", "eq"]
                                                , [0, a[2], 1, rs1[0].Id, 2, a[4], 3, dateFormat, 4, "1"]);
                                                var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                                   "<cell name='bsd_name'    " + "width='200' />  " +
                                   "</row>   " +
                                "</grid>   ";
                                            }
                                            CrmFetchKit.Fetch(xml3.join(""), false).then(function (rs3) {
                                                if (rs3.length > 0) {                                                
                                                    Xrm.Page.getControl("bsd_shippingpricelistname").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
                                                    shippingpricelist.setValue([{
                                                        id: rs3[0].Id,
                                                        name: rs3[0].attributes.bsd_name.value,
                                                        entityType: rs3[0].logicalName
                                                    }]);
                                                    if (rs3[0].attributes.bsd_priceunitporter != null) {
                                                        Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs3[0].attributes.bsd_priceunitporter.value);
                                                    }
                                                    if (rs3[0].attributes.bsd_priceofton != null) {
                                                        Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs3[0].attributes.bsd_priceofton.value);
                                                    }
                                                    if (rs3[0].attributes.bsd_pricetripporter != null) {
                                                        Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs3[0].attributes.bsd_pricetripporter.value);
                                                    }
                                                    if (rs3[0].attributes.bsd_priceoftrip != null) {
                                                        Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs3[0].attributes.bsd_priceoftrip.value);
                                                    }
                                                    if (rs3[0].attributes.bsd_unit != null) {
                                                        filedunit.setValue([{
                                                            id: rs3[0].attributes.bsd_unit.guid,
                                                            name: rs3[0].attributes.bsd_unit.name,
                                                            entityType: 'uom'
                                                        }]);
                                                    }
                                                    if (rs3[0].attributes.bsd_porter != null) {
                                                        Xrm.Page.getAttribute("bsd_portertype").setValue(rs3[0].attributes.bsd_porter.value);
                                                        if (rs3[0].attributes.bsd_porter.value == 861450000)//porter shipping Yes
                                                        {

                                                            Xrm.Page.getAttribute("bsd_porteroption").setValue(1);
                                                            Xrm.Page.getControl("bsd_porteroption").setDisabled(true);
                                                            set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                                                            set_visible(["bsd_porter"], 1);
                                                            Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
                                                            Xrm.Page.getAttribute("bsd_priceofporter").setValue(null);
                                                            Xrm.Page.getAttribute("bsd_porter").setValue(861450000);
                                                            set_requirelevel(["bsd_priceofporter"], 0);
                                                        }
                                                        if (rs3[0].attributes.bsd_porter.value == 861450001)//porter shipping No
                                                        {
                                                            Xrm.Page.getAttribute("bsd_porter").setValue(861450001);
                                                            Xrm.Page.getAttribute("bsd_porteroption").setValue(0);
                                                            Xrm.Page.getControl("bsd_porteroption").setDisabled(false);
                                                            Xrm.Page.getAttribute("bsd_pricepotter").setValue(null);
                                                            Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
                                                            set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                                                            set_visible(["bsd_porter"], 1);
                                                            set_requirelevel(["bsd_priceofporter"], 0);
                                                        }
                                                    }
                                                }
                                                if (rs3.length == 0) {
                                                    if (b.length > 0) {
                                                        var viewId6 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                                        var entityName6 = "bsd_shippingpricelist";
                                                        var viewDisplayName6 = "test";
                                                        xml6.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                                        xml6.push("<entity name='bsd_shippingpricelist'>");
                                                        xml6.push("<attribute name='bsd_shippingpricelistid'/>");
                                                        xml6.push("<attribute name='bsd_name'/>");
                                                        xml6.push("<attribute name='createdon'/>");
                                                        xml6.push("<attribute name='bsd_effectiveto'/>");
                                                        xml6.push("<attribute name='bsd_effectivefrom'/>");
                                                        xml6.push("<attribute name='bsd_priceunitporter'/>");
                                                        xml6.push("<attribute name='bsd_priceofton'/>");
                                                        xml6.push("<attribute name='bsd_pricetripporter'/>");
                                                        xml6.push("<attribute name='bsd_priceoftrip'/>");
                                                        xml6.push("<attribute name='bsd_unit'/>");
                                                        xml6.push("<attribute name='bsd_porter'/>");
                                                        xml6.push("<order attribute='createdon' descending='true' />");
                                                        xml6.push("<filter type='and'>");
                                                        xml6.push("<condition attribute='bsd_warehousefrom' operator='eq' uitype='bsd_warehouseentity' value='" + a[2] + "'/>");
                                                        xml6.push("<condition attribute='bsd_addressto' operator='eq' uitype='bsd_addressshipping'  value='" + rs1[0].Id + "'/>");
                                                        xml6.push("<condition attribute='bsd_deliverymethod' operator='eq' value='" + a[4] + "'/>");
                                                        xml6.push("<condition attribute='bsd_effectivefrom' operator='on-or-before' value='" + dateFormat + "'/>");
                                                        xml6.push("<condition attribute='bsd_shippingpricelistid' operator='not-in'>");
                                                        for (k = 0; k < b.length; k++) {
                                                            xml6.push("<value uitype='bsd_shippingpricelist'>" + '{' + b[k] + '}' + "");
                                                            xml6.push("</value>");
                                                        }
                                                        xml6.push("</condition>");
                                                        xml6.push("</filter>");
                                                        xml6.push("</entity>");
                                                        xml6.push("</fetch>");
                                                        var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                     "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                                     "<cell name='bsd_name'    " + "width='200' />  " +
                                     "</row>   " +
                                  "</grid>   ";
                                                    }
                                                    if (b.length == 0) {
                                                        var viewId6 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                                        var entityName6 = "bsd_shippingpricelist";
                                                        var viewDisplayName6 = "test";
                                                        fetch(xml6, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom", "bsd_priceunitporter", "bsd_priceofton"
                                                        , "bsd_pricetripporter", "bsd_priceoftrip", "bsd_unit", "bsd_porter"]
                                                        , "bsd_name", false, null
                                                        , ["bsd_warehousefrom", "bsd_addressto", "bsd_deliverymethod", "bsd_effectivefrom"], ["eq", "eq", "eq", "on-or-before"]
                                                        , [0, a[2], 1, rs1[0].Id, 2, a[4], 3, dateFormat]);
                                                        var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                           "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                                           "<cell name='bsd_name'    " + "width='200' />  " +
                                           "</row>   " +
                                        "</grid>   ";
                                                    }
                                                    CrmFetchKit.Fetch(xml6.join(""), false).then(function (rs6) {
                                                        if (rs6.length > 0) {
                                                            Xrm.Page.getControl("bsd_shippingpricelistname").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
                                                            shippingpricelist.setValue([{
                                                                id: rs6[0].Id,
                                                                name: rs6[0].attributes.bsd_name.value,
                                                                entityType: rs6[0].logicalName
                                                            }]);
                                                            if (rs6[0].attributes.bsd_priceunitporter != null) {
                                                                Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs6[0].attributes.bsd_priceunitporter.value);
                                                            }
                                                            if (rs6[0].attributes.bsd_priceofton != null) {
                                                                Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs6[0].attributes.bsd_priceofton.value);
                                                            }
                                                            if (rs6[0].attributes.bsd_pricetripporter != null) {
                                                                Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs6[0].attributes.bsd_pricetripporter.value);
                                                            }
                                                            if (rs6[0].attributes.bsd_priceoftrip != null) {
                                                                Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs6[0].attributes.bsd_priceoftrip.value);
                                                            }
                                                            if (rs6[0].attributes.bsd_unit != null) {
                                                                filedunit.setValue([{
                                                                    id: rs6[0].attributes.bsd_unit.guid,
                                                                    name: rs6[0].attributes.bsd_unit.name,
                                                                    entityType: 'uom'
                                                                }]);
                                                            }
                                                            if (rs6[0].attributes.bsd_porter != null) {
                                                                Xrm.Page.getAttribute("bsd_portertype").setValue(rs6[0].attributes.bsd_porter.value);
                                                                if (rs6[0].attributes.bsd_porter.value == 861450000)//porter shipping Yes
                                                                {

                                                                    Xrm.Page.getAttribute("bsd_porteroption").setValue(1);
                                                                    Xrm.Page.getControl("bsd_porteroption").setDisabled(true);
                                                                    set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                                                                    set_visible(["bsd_porter"], 1);
                                                                    Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
                                                                    Xrm.Page.getAttribute("bsd_priceofporter").setValue(null);
                                                                    Xrm.Page.getAttribute("bsd_porter").setValue(861450000);
                                                                    set_requirelevel(["bsd_priceofporter"], 0);
                                                                }
                                                                if (rs6[0].attributes.bsd_porter.value == 861450001)//porter shipping No
                                                                {
                                                                    Xrm.Page.getAttribute("bsd_porter").setValue(861450001);
                                                                    Xrm.Page.getAttribute("bsd_porteroption").setValue(0);
                                                                    Xrm.Page.getControl("bsd_porteroption").setDisabled(false);
                                                                    Xrm.Page.getAttribute("bsd_pricepotter").setValue(null);
                                                                    Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
                                                                    set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                                                                    set_visible(["bsd_porter"], 1);
                                                                    set_requirelevel(["bsd_priceofporter"], 0);
                                                                }
                                                            }
                                                        }
                                                        if (rs6.length == 0) {
                                                            Xrm.Page.getControl("bsd_shippingpricelistname").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
                                                            set_value(["bsd_portertype", "bsd_shippingpricelistname", "bsd_unitshipping"], null);
                                                            set_value(["bsd_priceoftransportationn"], 0);                                          
                                                        }
                                                    }, function (er) {
                                                        console.log(er.message)
                                                    });
                                                }
                                            }, function (er) {
                                                console.log(er.message)
                                            });
                                        }
                                        if (rs2.length == 0) {                           
                                            set_value(["bsd_shippingpricelistname", "bsd_unitshipping"], null);
                                            set_disable(["bsd_porter"], 0);
                                            set_value(["bsd_priceoftransportationn"], 0);
                                            clear_shippingpricelistname();
                                        }
                                    }, function (er) {
                                        console.log(er.message)
                                    });
                                }
                                else {
                                    clear_shippingpricelistname();
                                }
                            }, function (er) {
                                console.log(er.message)
                            });
                        }
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }
    }
    if (a[5] == 0)//shipping = no
    {
        set_visible(["bsd_typeofunit", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_unitshipping", "bsd_portertype"], 0);
        set_value(["bsd_portertype", "bsd_shippingpricelistname", "bsd_unitshipping"], null);
        set_value(["bsd_priceoftransportationn"], 0);
        set_requirelevel(["bsd_shippingpricelistname"], 0);
        if (Xrm.Page.getAttribute("bsd_porteroption").getValue() == 1)//porter = yes
        {
            //Xrm.Page.getAttribute("bsd_porter").setValue(null);
            //Xrm.Page.getAttribute("bsd_porteroption").setValue(0);
            Xrm.Page.getControl("bsd_porteroption").setDisabled(false);
            // Xrm.Page.getAttribute("bsd_pricepotter").setValue(null);
            //Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
            set_visible(["bsd_priceofporter", "bsd_priceporter"], 1);
            set_visible(["bsd_porter"], 1);
            set_requirelevel(["bsd_priceofporter"], 1);
            set_value(["bsd_porter"], 861450000);
        }
        if (Xrm.Page.getAttribute("bsd_porteroption").getValue() == 0)//porter = no
        {
            Xrm.Page.getAttribute("bsd_porter").setValue(null);
            Xrm.Page.getAttribute("bsd_porteroption").setValue(0);
            Xrm.Page.getControl("bsd_porteroption").setDisabled(false);
            Xrm.Page.getAttribute("bsd_priceofporter").setValue(null);
            Xrm.Page.getAttribute("bsd_priceporter").setValue(0);
            set_visible(["bsd_priceofporter", "bsd_priceporter"], 0);
            set_visible(["bsd_porter"], 1);
            set_requirelevel(["bsd_priceofporter"], 0);
            set_value(["bsd_porter"], 861450001);
        }
    }
}
//Author:Mr.Phong
//Description:an field trong section porter
function sethideporter() {
    var a = [];
    a = get_value(["bsd_porteroption", "bsd_portertype"])
    if (a[0] == 1 && a[1] == 861450000)//porter = yes && porter shipping Yes
    {
        Xrm.Page.getControl("bsd_porteroption").setDisabled(true);
        set_visible(["bsd_priceofporter", "bsd_priceporter"], 0);
        set_visible(["bsd_porter"], 1);
        set_requirelevel(["bsd_priceofporter"], 0);
    }
    else if (a[0] == 0 && a[1] == 861450001)//porter = no && porter shipping No
    {
        set_visible(["bsd_priceofporter", "bsd_priceporter"], 0);
        set_visible(["bsd_porter"], 1);
        set_requirelevel(["bsd_priceofporter"], 0);
        set_value(["bsd_porter"], 861450001);
    }
    else if (a[0] == 1 && a[1] == 861450001)//porter = yes && porter shipping No
    {
        set_visible(["bsd_priceofporter", "bsd_priceporter"], 1);
        set_visible(["bsd_porter"], 1);
        set_requirelevel(["bsd_priceofporter"], 1);
        set_value(["bsd_porter"], 861450000);
    }
    else if (a[0] == 1 && a[1] == null)//porter = yes && porter shipping null
    {
        set_visible(["bsd_priceofporter", "bsd_priceporter", "bsd_porter"], 1);
        set_value(["bsd_priceofporter", "bsd_porter"], null);
        set_value(["bsd_priceporter"], 0);
        set_requirelevel(["bsd_priceofporter"], 1);
        set_value(["bsd_porter"], 861450000);
    }
    else if (a[0] == 0 && a[1] == null)//porter = no && porter shipping null
    {
        set_visible(["bsd_priceofporter", "bsd_priceporter"], 0);
        set_value(["bsd_priceofporter", "bsd_porter"], null);
        set_value(["bsd_priceporter"], 0);
        set_requirelevel(["bsd_priceofporter"], 0);
        set_value(["bsd_porter"], 861450001);
    }
}
//Author:Mr.Phong
//Description:an field trong section tax
function sethidetax() {
    var a = [];
    a = get_value(["bsd_tax"])
    if (a[0] == 1)//tax = yes
    {
        set_visible(["bsd_value"], 1);
        set_requirelevel(["bsd_value"], 1);
    }
    if (a[0] == 0)//tax = no
    {
        set_visible(["bsd_value"], 0);
        set_requirelevel(["bsd_value"], 0);
        set_value(["bsd_value"], null);
    }
}
//Author:Mr.Phong
//Description:thay doi gia tri field shipping price list khi thay doi field type of unit
function setvaluefiledshippingpricelistchangetypeofunit() {
    var a = [];
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    a = get_value(["bsd_shiptoaddress", "bsd_warehouseto", "bsd_typeofunit"]);
    var filedunit = Xrm.Page.getAttribute("bsd_unitshipping");
    var shippingpricelist = Xrm.Page.getAttribute("bsd_shippingpricelistname");
    if (a[0] != null && a[2] != null) {
        var effectivefrom = Xrm.Page.getAttribute("effectivefrom").getValue();
        var year = effectivefrom.getFullYear() + "";
        if (effectivefrom.getMonth() + 1 <= 9) {
            var month = "0" + (effectivefrom.getMonth() + 1) + "";
        }
        else {
            var month = (effectivefrom.getMonth() + 1) + "";
        }
        var day = effectivefrom.getDate() + "";
        var dateFormat = year + "-" + month + "-" + day;
        if (a[0] != null && a[2] != null) {
            fetch(xml, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_ward", "bsd_street", "bsd_province", "bsd_district"], "bsd_name", false, null
                , ["bsd_addressid"], ["eq"], [0, a[0], 1]);
            CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
                if (rs.length > 0) {
                    if (rs[0].attributes.bsd_district != null) {
                        var province = rs[0].attributes.bsd_province.guid;
                        var district = rs[0].attributes.bsd_district.guid;
                        fetch(xml1, "bsd_addressshipping", ["bsd_addressshippingid", "bsd_name", "createdon"], "bsd_name", false, null
                        , ["bsd_province", "bsd_district"], ["eq", "eq"], [0, province, 1, district]);
                        CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
                            if (rs1.length > 0) {
                                fetch(xml2, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom"], "bsd_name", false, null
                                 , ["bsd_warehousefrom", "bsd_addressto", "bsd_deliverymethod", "bsd_effectivefrom"], ["eq", "eq", "eq", "on-or-before"]
                                 , [0, a[2], 1, rs1[0].Id, 2, a[4], 3, dateFormat]);
                                CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs2) {
                                    if (rs2.length > 0) {
                                        var dayfrom = Xrm.Page.getAttribute("effectivefrom").getValue();
                                        var yearfrom = dayfrom.getFullYear();
                                        if (dayfrom.getMonth() + 1 <= 9) {
                                            var monthfrom = "0" + (dayfrom.getMonth() + 1);
                                        }
                                        else {
                                            var monthfrom = (dayfrom.getMonth() + 1);
                                        }
                                        var dayeffectivefrom = dayfrom.getDate();
                                        var b = [];
                                        for (var i = 0; i < rs2.length; i++) {
                                            var dayfromshipping = rs2[i].attributes.bsd_effectivefrom.value;
                                            var yearfromshipping = dayfromshipping.getFullYear();
                                            if (dayfromshipping.getMonth() + 1 <= 9) {
                                                var monthfromshipping = "0" + (dayfromshipping.getMonth() + 1);
                                            }
                                            else {
                                                var monthfromshipping = (dayfromshipping.getMonth() + 1);
                                            }
                                            var dayeffectivefromshipping = dayfromshipping.getDate();
                                            var daytoshipping = rs2[i].attributes.bsd_effectiveto.value;
                                            var yeartoshipping = daytoshipping.getFullYear();
                                            if (daytoshipping.getMonth() + 1 <= 9) {
                                                var monthtoshipping = "0" + (daytoshipping.getMonth() + 1);
                                            }
                                            else {
                                                var monthtoshipping = (daytoshipping.getMonth() + 1);
                                            }
                                            var dayeffectivetoshipping = daytoshipping.getDate();
                                            if (yearfromshipping < yearfrom && yeartoshipping < yearfrom) {
                                                b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                            }
                                            if (yearfromshipping < yearfrom && yeartoshipping == yearfrom) {
                                                if (monthtoshipping < monthfrom) {
                                                    b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                }
                                                if (monthtoshipping == monthfrom) {
                                                    if (dayeffectivetoshipping < dayeffectivefrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                }
                                            }
                                            if (yearfromshipping == yearfrom && yeartoshipping == yearfrom) {
                                                if (monthfromshipping > monthfrom && monthtoshipping < monthfrom) {
                                                    b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                }
                                                if (monthfromshipping > monthfrom && monthtoshipping == monthfrom) {
                                                    if (dayeffectivetoshipping < dayeffectivefrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                }
                                                if (monthfromshipping < monthfrom && monthtoshipping < monthfrom) {
                                                    b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                }
                                                if (monthfromshipping < monthfrom && monthtoshipping == monthfrom) {
                                                    if (dayeffectivetoshipping < dayeffectivefrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }

                                                }
                                                if (monthfromshipping == monthfrom && monthtoshipping < monthfrom) {
                                                    b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                }
                                                if (monthfromshipping == monthfrom && monthtoshipping == monthfrom) {
                                                    if (dayeffectivetoshipping < dayeffectivefrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                }
                                            }
                                        }
                                        if (b.length > 0) {
                                            var viewId3 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                            var entityName3 = "bsd_shippingpricelist";
                                            var viewDisplayName3 = "test";
                                            xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                            xml3.push("<entity name='bsd_shippingpricelist'>");
                                            xml3.push("<attribute name='bsd_shippingpricelistid'/>");
                                            xml3.push("<attribute name='bsd_name'/>");
                                            xml3.push("<attribute name='createdon'/>");
                                            xml3.push("<attribute name='bsd_effectiveto'/>");
                                            xml3.push("<attribute name='bsd_effectivefrom'/>");
                                            xml3.push("<attribute name='bsd_priceunitporter'/>");
                                            xml3.push("<attribute name='bsd_priceofton'/>");
                                            xml3.push("<attribute name='bsd_pricetripporter'/>");
                                            xml3.push("<attribute name='bsd_priceoftrip'/>");
                                            xml3.push("<attribute name='bsd_unit'/>");
                                            xml3.push("<attribute name='bsd_porter'/>");
                                            xml3.push("<order attribute='createdon' descending='true' />");
                                            xml3.push("<filter type='and'>");
                                            xml3.push("<condition attribute='bsd_warehousefrom' operator='eq' uitype='bsd_warehouseentity' value='" + a[2] + "'/>");
                                            xml3.push("<condition attribute='bsd_addressto' operator='eq' uitype='bsd_addressshipping'  value='" + rs1[0].Id + "'/>");
                                            xml3.push("<condition attribute='bsd_deliverymethod' operator='eq' value='" + a[4] + "'/>");
                                            xml3.push("<condition attribute='bsd_effectivefrom' operator='on-or-before' value='" + dateFormat + "'/>");
                                            xml3.push("<condition attribute='bsd_key' operator='eq' value='1'/>");
                                            xml3.push("<condition attribute='bsd_shippingpricelistid' operator='not-in'>");
                                            for (k = 0; k < b.length; k++) {
                                                xml3.push("<value uitype='bsd_shippingpricelist'>" + '{' + b[k] + '}' + "");
                                                xml3.push("</value>");
                                            }
                                            xml3.push("</condition>");
                                            xml3.push("</filter>");
                                            xml3.push("</entity>");
                                            xml3.push("</fetch>");
                                            var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                         "<cell name='bsd_name'    " + "width='200' />  " +
                         "</row>   " +
                      "</grid>   ";
                                        }
                                        if (b.length == 0) {
                                            var viewId3 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                            var entityName3 = "bsd_shippingpricelist";
                                            var viewDisplayName3 = "test";
                                            fetch(xml3, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom", "bsd_priceunitporter", "bsd_priceofton"
                                                     , "bsd_pricetripporter", "bsd_priceoftrip", "bsd_unit", "bsd_porter"]
                                                     , "bsd_name", false, null
                                                     , ["bsd_warehousefrom", "bsd_addressto", "bsd_deliverymethod", "bsd_effectivefrom", "bsd_key"], ["eq", "eq", "eq", "on-or-before", "eq"]
                                                     , [0, a[2], 1, rs1[0].Id, 2, a[4], 3, dateFormat, 4, "1"]);
                                            var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                               "<cell name='bsd_name'    " + "width='200' />  " +
                               "</row>   " +
                            "</grid>   ";
                                        }
                                        CrmFetchKit.Fetch(xml3.join(""), false).then(function (rs3) {
                                            if (rs3.length > 0) {                                              
                                                Xrm.Page.getControl("bsd_shippingpricelistname").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
                                                shippingpricelist.setValue([{
                                                    id: rs3[0].Id,
                                                    name: rs3[0].attributes.bsd_name.value,
                                                    entityType: rs3[0].logicalName
                                                }]);
                                                if (rs3[0].attributes.bsd_priceunitporter != null) {
                                                    Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs3[0].attributes.bsd_priceunitporter.value);
                                                }
                                                if (rs3[0].attributes.bsd_priceofton != null) {
                                                    Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs3[0].attributes.bsd_priceofton.value);
                                                }
                                                if (rs3[0].attributes.bsd_pricetripporter != null) {
                                                    Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs3[0].attributes.bsd_pricetripporter.value);
                                                }
                                                if (rs3[0].attributes.bsd_priceoftrip != null) {
                                                    Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs3[0].attributes.bsd_priceoftrip.value);
                                                }
                                                if (rs3[0].attributes.bsd_unit != null) {
                                                    filedunit.setValue([{
                                                        id: rs3[0].attributes.bsd_unit.guid,
                                                        name: rs3[0].attributes.bsd_unit.name,
                                                        entityType: 'uom'
                                                    }]);
                                                }
                                                if (rs3[0].attributes.bsd_porter != null) {
                                                    Xrm.Page.getAttribute("bsd_portertype").setValue(rs3[0].attributes.bsd_porter.value);
                                                    if (rs3[0].attributes.bsd_porter.value == 861450000)//porter shipping Yes
                                                    {

                                                        Xrm.Page.getAttribute("bsd_porteroption").setValue(1);
                                                        Xrm.Page.getControl("bsd_porteroption").setDisabled(true);
                                                        set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                                                        set_visible(["bsd_porter"], 1);
                                                        Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
                                                        Xrm.Page.getAttribute("bsd_priceofporter").setValue(null);
                                                        Xrm.Page.getAttribute("bsd_porter").setValue(861450000);
                                                        set_requirelevel(["bsd_priceofporter"], 0);
                                                    }
                                                    if (rs3[0].attributes.bsd_porter.value == 861450001)//porter shipping No
                                                    {
                                                        Xrm.Page.getAttribute("bsd_porter").setValue(861450001);
                                                        Xrm.Page.getAttribute("bsd_porteroption").setValue(0);
                                                        Xrm.Page.getControl("bsd_porteroption").setDisabled(false);
                                                        Xrm.Page.getAttribute("bsd_pricepotter").setValue(null);
                                                        Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
                                                        set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                                                        set_visible(["bsd_porter"], 1);
                                                        set_requirelevel(["bsd_priceofporter"], 0);

                                                    }
                                                }
                                            }
                                            if (rs3.length == 0) {
                                                if (b.length > 0) {
                                                    var viewId6 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                                    var entityName6 = "bsd_shippingpricelist";
                                                    var viewDisplayName6 = "test";
                                                    xml6.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                                    xml6.push("<entity name='bsd_shippingpricelist'>");
                                                    xml6.push("<attribute name='bsd_shippingpricelistid'/>");
                                                    xml6.push("<attribute name='bsd_name'/>");
                                                    xml6.push("<attribute name='createdon'/>");
                                                    xml6.push("<attribute name='bsd_effectiveto'/>");
                                                    xml6.push("<attribute name='bsd_effectivefrom'/>");
                                                    xml6.push("<attribute name='bsd_priceunitporter'/>");
                                                    xml6.push("<attribute name='bsd_priceofton'/>");
                                                    xml6.push("<attribute name='bsd_pricetripporter'/>");
                                                    xml6.push("<attribute name='bsd_priceoftrip'/>");
                                                    xml6.push("<attribute name='bsd_unit'/>");
                                                    xml6.push("<attribute name='bsd_porter'/>");
                                                    xml6.push("<order attribute='createdon' descending='true' />");
                                                    xml6.push("<filter type='and'>");
                                                    xml6.push("<condition attribute='bsd_warehousefrom' operator='eq' uitype='bsd_warehouseentity' value='" + a[2] + "'/>");
                                                    xml6.push("<condition attribute='bsd_addressto' operator='eq' uitype='bsd_addressshipping'  value='" + rs1[0].Id + "'/>");
                                                    xml6.push("<condition attribute='bsd_deliverymethod' operator='eq' value='" + a[4] + "'/>");
                                                    xml6.push("<condition attribute='bsd_effectivefrom' operator='on-or-before' value='" + dateFormat + "'/>");
                                                    xml6.push("<condition attribute='bsd_shippingpricelistid' operator='not-in'>");
                                                    for (k = 0; k < b.length; k++) {
                                                        xml6.push("<value uitype='bsd_shippingpricelist'>" + '{' + b[k] + '}' + "");
                                                        xml6.push("</value>");
                                                    }
                                                    xml6.push("</condition>");
                                                    xml6.push("</filter>");
                                                    xml6.push("</entity>");
                                                    xml6.push("</fetch>");
                                                    var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                                 "<cell name='bsd_name'    " + "width='200' />  " +
                                 "</row>   " +
                              "</grid>   ";
                                                }
                                                if (b.length == 0) {
                                                    var viewId6 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                                    var entityName6 = "bsd_shippingpricelist";
                                                    var viewDisplayName6 = "test";
                                                    fetch(xml6, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom", "bsd_priceunitporter", "bsd_priceofton"
                                                    , "bsd_pricetripporter", "bsd_priceoftrip", "bsd_unit", "bsd_porter"]
                                                    , "bsd_name", false, null
                                                    , ["bsd_warehousefrom", "bsd_addressto", "bsd_deliverymethod", "bsd_effectivefrom"], ["eq", "eq", "eq", "on-or-before"]
                                                    , [0, a[2], 1, rs1[0].Id, 2, a[4], 3, dateFormat]);
                                                    var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                       "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                                       "<cell name='bsd_name'    " + "width='200' />  " +
                                       "</row>   " +
                                    "</grid>   ";
                                                }
                                                CrmFetchKit.Fetch(xml6.join(""), false).then(function (rs6) {
                                                    if (rs6.length > 0) {
                                                        Xrm.Page.getControl("bsd_shippingpricelistname").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
                                                        shippingpricelist.setValue([{
                                                            id: rs6[0].Id,
                                                            name: rs6[0].attributes.bsd_name.value,
                                                            entityType: rs6[0].logicalName
                                                        }]);
                                                        if (rs6[0].attributes.bsd_priceunitporter != null) {
                                                            Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs6[0].attributes.bsd_priceunitporter.value);
                                                        }
                                                        if (rs6[0].attributes.bsd_priceofton != null) {
                                                            Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs6[0].attributes.bsd_priceofton.value);
                                                        }
                                                        if (rs6[0].attributes.bsd_pricetripporter != null) {
                                                            Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs6[0].attributes.bsd_pricetripporter.value);
                                                        }
                                                        if (rs6[0].attributes.bsd_priceoftrip != null) {
                                                            Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs6[0].attributes.bsd_priceoftrip.value);
                                                        }
                                                        if (rs6[0].attributes.bsd_unit != null) {
                                                            filedunit.setValue([{
                                                                id: rs6[0].attributes.bsd_unit.guid,
                                                                name: rs6[0].attributes.bsd_unit.name,
                                                                entityType: 'uom'
                                                            }]);
                                                        }
                                                        if (rs6[0].attributes.bsd_porter != null) {
                                                            Xrm.Page.getAttribute("bsd_portertype").setValue(rs6[0].attributes.bsd_porter.value);
                                                            if (rs6[0].attributes.bsd_porter.value == 861450000)//porter shipping Yes
                                                            {

                                                                Xrm.Page.getAttribute("bsd_porteroption").setValue(1);
                                                                Xrm.Page.getControl("bsd_porteroption").setDisabled(true);
                                                                set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                                                                set_visible(["bsd_porter"], 1);
                                                                Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
                                                                Xrm.Page.getAttribute("bsd_priceofporter").setValue(null);
                                                                Xrm.Page.getAttribute("bsd_porter").setValue(861450000);
                                                                set_requirelevel(["bsd_priceofporter"], 0);
                                                            }
                                                            if (rs6[0].attributes.bsd_porter.value == 861450001)//porter shipping No
                                                            {
                                                                Xrm.Page.getAttribute("bsd_porter").setValue(861450001);
                                                                Xrm.Page.getAttribute("bsd_porteroption").setValue(0);
                                                                Xrm.Page.getControl("bsd_porteroption").setDisabled(false);
                                                                Xrm.Page.getAttribute("bsd_pricepotter").setValue(null);
                                                                Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
                                                                set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                                                                set_visible(["bsd_porter"], 1);
                                                                set_requirelevel(["bsd_priceofporter"], 0);
                                                            }
                                                        }
                                                    }
                                                    if (rs6.length == 0) {
                                                        Xrm.Page.getControl("bsd_shippingpricelistname").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
                                                        set_value(["bsd_portertype", "bsd_shippingpricelistname", "bsd_unitshipping"], null);
                                                        set_value(["bsd_priceoftransportationn"], 0);
                                                    }
                                                }, function (er) {
                                                    console.log(er.message)
                                                });
                                            }
                                        }, function (er) {
                                            console.log(er.message)
                                        });
                                    }
                                    if (rs2.length == 0) {
                                        var viewId4 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                        var entityName4 = "bsd_shippingpricelist";
                                        var viewDisplayName4 = "test";
                                        fetch(xml4, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom", "bsd_priceunitporter", "bsd_priceofton"
                                            , "bsd_pricetripporter", "bsd_priceoftrip", "bsd_unit", "bsd_porter"]
                                            , "bsd_name", false, null
                                            , ["bsd_warehousefrom", "bsd_addressto", "bsd_deliverymethod", "bsd_effectivefrom"], ["eq", "eq", "eq", "on-or-before"]
                                            , [0, "{9BB0F624-E6BD-E621-95F1-000C29E57EAC}", 1, "{57B42CCF-48AB-E632-93F0-000C29D57FAB}", 2, a[4], 3, dateFormat]);
                                        var layoutXml4 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                           "<cell name='bsd_name'    " + "width='200' />  " +
                           "</row>   " +
                        "</grid>   ";
                                        Xrm.Page.getControl("bsd_shippingpricelistname").addCustomView(viewId4, entityName4, viewDisplayName4, xml4.join(""), layoutXml4, true);
                                        set_value(["bsd_shippingpricelistname", "bsd_unitshipping", "bsd_portertype"], null);
                                        set_value(["bsd_priceoftransportationn"], 0);
                                    }
                                }, function (er) {
                                    console.log(er.message)
                                });
                            }
                            else {
                                clear_shippingpricelistname();
                            }
                        }, function (er) {
                            console.log(er.message)
                        });
                    }
                }
                else {
                    clear_shippingpricelistname();
                }
            }, function (er) {
                console.log(er.message)
            });
        }
    }
}
//Author:Mr.Phong
//Description:thay doi gia shipping khi thay doi fied shipping price list
function getvaluefieldshippingpricelistchangeshippingpricelist() {
    var a = [];
    var xml = [];
    var shippingprice = Xrm.Page.getAttribute("bsd_shippingpricelistname").getValue();
    a = get_value(["bsd_shippingpricelistname"]);
    var filedunit = Xrm.Page.getAttribute("bsd_unitshipping");
    if (a[0] != null) {
        fetch(xml, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom", "bsd_priceunitporter", "bsd_priceofton", "bsd_pricetripporter", "bsd_priceoftrip", "bsd_unit", "bsd_porter"]
                                          , "createdon", true, null
                                          , ["bsd_shippingpricelistid"], ["eq"]
                                          , [0, a[0], 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {

                if (rs[0].attributes.bsd_priceunitporter != null) {
                    Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs[0].attributes.bsd_priceunitporter.value);
                }
                if (rs[0].attributes.bsd_priceoftrip != null) {
                    Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs[0].attributes.bsd_priceoftrip.value);
                }
                if (rs[0].attributes.bsd_priceofton != null) {
                    Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs[0].attributes.bsd_priceofton.value);
                }
                if (rs[0].attributes.bsd_pricetripporter != null) {
                    Xrm.Page.getAttribute("bsd_priceoftransportationn").setValue(rs[0].attributes.bsd_pricetripporter.value);
                }
                if (rs[0].attributes.bsd_unit != null) {
                    filedunit.setValue([{
                        id: rs[0].attributes.bsd_unit.guid,
                        name: rs[0].attributes.bsd_unit.name,
                        entityType: 'uom'
                    }]);
                }
                if (rs[0].attributes.bsd_porter != null) {
                    Xrm.Page.getAttribute("bsd_portertype").setValue(rs[0].attributes.bsd_porter.value);
                    if (rs[0].attributes.bsd_porter.value == 861450000)//porter shipping Yes
                    {
                        Xrm.Page.getAttribute("bsd_porteroption").setValue(1);
                        Xrm.Page.getControl("bsd_porteroption").setDisabled(true);
                        set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                        set_visible(["bsd_porter"], 1);
                        Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
                        Xrm.Page.getAttribute("bsd_priceofporter").setValue(null);
                        Xrm.Page.getAttribute("bsd_porter").setValue(861450000);
                        set_requirelevel(["bsd_priceofporter"], 0);
                    }
                    if (rs[0].attributes.bsd_porter.value == 861450001 && Xrm.Page.getAttribute("bsd_priceofporter").getValue() == null)//porter shipping No
                    {
                        Xrm.Page.getAttribute("bsd_porter").setValue(861450001);
                        Xrm.Page.getAttribute("bsd_porteroption").setValue(0);
                        Xrm.Page.getControl("bsd_porteroption").setDisabled(false);
                        Xrm.Page.getAttribute("bsd_pricepotter").setValue(null);
                        Xrm.Page.getAttribute("bsd_pricepotter").setValue(0);
                        set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                        set_visible(["bsd_porter"], 1);
                        set_requirelevel(["bsd_priceofporter"], 0);
                    }
                }
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    if (a[0] == null) {
        set_value(["bsd_unitshipping", "bsd_portertype"], null);
        set_value(["bsd_priceoftransportationn"], 0);
    }
}
//Author:Mr.Phong
//Description:thay doi gia tri price khi thay doi porter
function getvaluefieldpricechangepriceofpotter() {
    var xml = [];
    var a = [];
    a = get_value(["bsd_priceofporter"]);
    if (a[0] != null) {
        fetch(xml, "bsd_porter", ["bsd_porterid", "bsd_name", "createdon", "transactioncurrencyid", "bsd_price"]
                                       , "bsd_name", false, null
                                       , ["bsd_porterid"], ["eq"]
                                       , [0, a[0], 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0 && rs[0].attributes.bsd_price != null) {
                Xrm.Page.getAttribute("bsd_priceporter").setValue(rs[0].attributes.bsd_price.value);
            }
            if (rs.length == 0) {
                Xrm.Page.getAttribute("bsd_priceporter").setValue(0);
            }

        },
           function (er) {
               console.log(er.message)
           });
    }
    if (a[0] == null) {
        Xrm.Page.getAttribute("bsd_priceporter").setValue(0);
    }
}
//Authpr:Mr.Phong
//Description:load gia tri filed shipping price list name khi form edit
function loadvaluefieldpricelistonload() {
    var a = [];
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    a = get_value(["bsd_shiptoaddress", "bsd_warehouseto", "bsd_typeofunit"]);
    var filedunit = Xrm.Page.getAttribute("bsd_unitshipping");
    var shippingpricelist = Xrm.Page.getAttribute("bsd_shippingpricelistname");
    if (Xrm.Page.ui.getFormType() == 2) {
        if (a[0] != null && a[2] != null) {
            var effectivefrom = Xrm.Page.getAttribute("effectivefrom").getValue();
            var year = effectivefrom.getFullYear() + "";
            if (effectivefrom.getMonth() + 1 <= 9) {
                var month = "0" + (effectivefrom.getMonth() + 1) + "";
            }
            else {
                var month = (effectivefrom.getMonth() + 1) + "";
            }
            var day = effectivefrom.getDate() + "";
            var dateFormat = year + "-" + month + "-" + day;
            if (a[0] != null && a[2] != null) {
                fetch(xml, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_ward", "bsd_street", "bsd_province", "bsd_district"], "bsd_name", false, null
                    , ["bsd_addressid"], ["eq"], [0, a[0], 1]);
                CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
                    if (rs.length > 0) {
                        if (rs[0].attributes.bsd_district != null) {
                            var province = rs[0].attributes.bsd_province.guid;
                            var district = rs[0].attributes.bsd_district.guid;
                            fetch(xml1, "bsd_addressshipping", ["bsd_addressshippingid", "bsd_name", "createdon"], "bsd_name", false, null
                            , ["bsd_province", "bsd_district"], ["eq", "eq"], [0, province, 1, district]);
                            CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
                                if (rs1.length > 0) {
                                    fetch(xml2, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom"], "bsd_name", false, null
                                     , ["bsd_warehousefrom", "bsd_addressto", "bsd_deliverymethod", "bsd_effectivefrom"], ["eq", "eq", "eq", "on-or-before"]
                                     , [0, a[2], 1, rs1[0].Id, 2, a[4], 3, dateFormat]);
                                    CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs2) {
                                        if (rs2.length > 0) {
                                            var dayfrom = Xrm.Page.getAttribute("effectivefrom").getValue();
                                            var yearfrom = dayfrom.getFullYear();
                                            if (dayfrom.getMonth() + 1 <= 9) {
                                                var monthfrom = "0" + (dayfrom.getMonth() + 1);
                                            }
                                            else {
                                                var monthfrom = (dayfrom.getMonth() + 1);
                                            }
                                            var dayeffectivefrom = dayfrom.getDate();
                                            var b = [];
                                            for (var i = 0; i < rs2.length; i++) {
                                                var dayfromshipping = rs2[i].attributes.bsd_effectivefrom.value;
                                                var yearfromshipping = dayfromshipping.getFullYear();
                                                if (dayfromshipping.getMonth() + 1 <= 9) {
                                                    var monthfromshipping = "0" + (dayfromshipping.getMonth() + 1);
                                                }
                                                else {
                                                    var monthfromshipping = (dayfromshipping.getMonth() + 1);
                                                }
                                                var dayeffectivefromshipping = dayfromshipping.getDate();
                                                var daytoshipping = rs2[i].attributes.bsd_effectiveto.value;
                                                var yeartoshipping = daytoshipping.getFullYear();
                                                if (daytoshipping.getMonth() + 1 <= 9) {
                                                    var monthtoshipping = "0" + (daytoshipping.getMonth() + 1);
                                                }
                                                else {
                                                    var monthtoshipping = (daytoshipping.getMonth() + 1);
                                                }
                                                var dayeffectivetoshipping = daytoshipping.getDate();
                                                if (yearfromshipping < yearfrom && yeartoshipping < yearfrom) {
                                                    b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                }
                                                if (yearfromshipping < yearfrom && yeartoshipping == yearfrom) {
                                                    if (monthtoshipping < monthfrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                    if (monthtoshipping == monthfrom) {
                                                        if (dayeffectivetoshipping < dayeffectivefrom) {
                                                            b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                        }
                                                    }
                                                }
                                                if (yearfromshipping == yearfrom && yeartoshipping == yearfrom) {
                                                    if (monthfromshipping > monthfrom && monthtoshipping < monthfrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                    if (monthfromshipping > monthfrom && monthtoshipping == monthfrom) {
                                                        if (dayeffectivetoshipping < dayeffectivefrom) {
                                                            b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                        }
                                                    }
                                                    if (monthfromshipping < monthfrom && monthtoshipping < monthfrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                    if (monthfromshipping < monthfrom && monthtoshipping == monthfrom) {
                                                        if (dayeffectivetoshipping < dayeffectivefrom) {
                                                            b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                        }

                                                    }
                                                    if (monthfromshipping == monthfrom && monthtoshipping < monthfrom) {
                                                        b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                    }
                                                    if (monthfromshipping == monthfrom && monthtoshipping == monthfrom) {
                                                        if (dayeffectivetoshipping < dayeffectivefrom) {
                                                            b.push(rs2[i].attributes.bsd_shippingpricelistid.value);
                                                        }
                                                    }
                                                }
                                            }
                                            if (b.length > 0) {
                                                var viewId3 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                                var entityName3 = "bsd_shippingpricelist";
                                                var viewDisplayName3 = "test";
                                                xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                                                xml3.push("<entity name='bsd_shippingpricelist'>");
                                                xml3.push("<attribute name='bsd_shippingpricelistid'/>");
                                                xml3.push("<attribute name='bsd_name'/>");
                                                xml3.push("<attribute name='createdon'/>");
                                                xml3.push("<attribute name='bsd_effectiveto'/>");
                                                xml3.push("<attribute name='bsd_effectivefrom'/>");
                                                xml3.push("<attribute name='bsd_priceunitporter'/>");
                                                xml3.push("<attribute name='bsd_priceofton'/>");
                                                xml3.push("<attribute name='bsd_pricetripporter'/>");
                                                xml3.push("<attribute name='bsd_priceoftrip'/>");
                                                xml3.push("<attribute name='bsd_unit'/>");
                                                xml3.push("<attribute name='bsd_porter'/>");
                                                xml3.push("<order attribute='createdon' descending='true' />");
                                                xml3.push("<filter type='and'>");
                                                xml3.push("<condition attribute='bsd_warehousefrom' operator='eq' uitype='bsd_warehouseentity' value='" + warehouse[0].id + "'/>");
                                                xml3.push("<condition attribute='bsd_addressto' operator='eq' uitype='bsd_addressshipping'  value='" + rs1[0].Id + "'/>");
                                                xml3.push("<condition attribute='bsd_deliverymethod' operator='eq' value='" + typeofunit + "'/>");
                                                xml3.push("<condition attribute='bsd_effectivefrom' operator='on-or-before' value='" + dateFormat + "'/>");
                                                xml3.push("<condition attribute='bsd_shippingpricelistid' operator='not-in'>");
                                                for (k = 0; k < b.length; k++) {
                                                    xml3.push("<value uitype='bsd_shippingpricelist'>" + '{' + b[k] + '}' + "");
                                                    xml3.push("</value>");
                                                }
                                                xml3.push("</condition>");
                                                xml3.push("</filter>");
                                                xml3.push("</entity>");
                                                xml3.push("</fetch>");
                                                var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                             "<cell name='bsd_name'    " + "width='200' />  " +
                             "</row>   " +
                          "</grid>   ";
                                            }
                                            if (b.length == 0) {
                                                var viewId3 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                                var entityName3 = "bsd_shippingpricelist";
                                                var viewDisplayName3 = "test";
                                                fetch(xml3, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom", "bsd_priceunitporter", "bsd_priceofton"
                                                , "bsd_pricetripporter", "bsd_priceoftrip", "bsd_unit", "bsd_porter"]
                                                , "bsd_name", false, null
                                                , ["bsd_warehousefrom", "bsd_addressto", "bsd_deliverymethod", "bsd_effectivefrom"], ["eq", "eq", "eq", "on-or-before"]
                                                , [0, a[2], 1, rs1[0].Id, 2, a[4], 3, dateFormat]);
                                                var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                                   "<cell name='bsd_name'    " + "width='200' />  " +
                                   "</row>   " +
                                "</grid>   ";
                                            }
                                            Xrm.Page.getControl("bsd_shippingpricelistname").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
                                        }
                                        if (rs2.length == 0) {
                                            var viewId4 = "{FC32A794-CBF0-4F91-808E-6272A9E2910C}";
                                            var entityName4 = "bsd_shippingpricelist";
                                            var viewDisplayName4 = "test";
                                            fetch(xml4, "bsd_shippingpricelist", ["bsd_shippingpricelistid", "bsd_name", "createdon", "bsd_effectiveto", "bsd_effectivefrom", "bsd_priceunitporter", "bsd_priceofton"
                                                , "bsd_pricetripporter", "bsd_priceoftrip", "bsd_unit", "bsd_porter"]
                                                , "bsd_name", false, null
                                                , ["bsd_warehousefrom", "bsd_addressto", "bsd_deliverymethod", "bsd_effectivefrom"], ["eq", "eq", "eq", "on-or-before"]
                                                , [0, "{9BB0F624-E6BD-E621-95F1-000C29E57EAC}", 1, "{57B42CCF-48AB-E632-93F0-000C29D57FAB}", 2, a[4], 3, dateFormat]);
                                            var layoutXml4 = "<grid name='resultset' " + "object='1' " + "jump='bsd_shippingpricelistid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_shippingpricelistid'>  " +
                               "<cell name='bsd_name'    " + "width='200' />  " +
                               "</row>   " +
                            "</grid>   ";
                                            Xrm.Page.getControl("bsd_shippingpricelistname").addCustomView(viewId4, entityName4, viewDisplayName4, xml4.join(""), layoutXml4, true);
                                            set_value(["bsd_shippingpricelistname", "bsd_unitshipping", "bsd_porter", "bsd_portertype", "bsd_priceofporter"], null);
                                            set_visible(["bsd_priceofporter", "bsd_pricepotter"], 0);
                                            set_value(["bsd_priceoftransportationn", "bsd_pricepotter"], 0);
                                            Xrm.Page.getControl("bsd_porteroption").setDisabled(false);
                                        }
                                    }, function (er) {
                                        console.log(er.message)
                                    });
                                }
                                else {
                                    clear_shippingpricelistname();
                                }
                            }, function (er) {
                                console.log(er.message)
                            });
                        }
                    }
                    else {
                        clear_shippingpricelistname();
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }
    }
}
//Author:Mr.Phong
//Description:gan gia tri cho filed invoice address
function set_invoiceaddress() {
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    var addressinvoicenameaccount = Xrm.Page.getAttribute("bsd_addressinvoiceaccount");
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    if (invoicenameaccount != null) {
        Xrm.Page.getControl("bsd_addressinvoiceaccount").setDisabled(false);
        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName = "bsd_address";
        var viewDisplayName = "test";
        fetch(xml, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Invoice" + "%", 1, "0", 2, invoicenameaccount[0].id]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    if (addressinvoicenameaccount == null) {
                        addressinvoicenameaccount.setValue([{
                            id: rs[0].Id,
                            name: rs[0].attributes.bsd_name.value,
                            entityType: rs[0].logicalName
                        }]);

                    }
                }
                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            }
            if (rs.length == 0) {
                var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName1 = "bsd_address";
                var viewDisplayName1 = "test";
                fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                         ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, invoicenameaccount[0].id]);
                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                    if (rs1.length > 0) {
                        if (rs1[0].attributes.bsd_name != null) {
                            if (addressinvoicenameaccount == null) {
                                addressinvoicenameaccount.setValue([{
                                    id: rs1[0].Id,
                                    name: rs1[0].attributes.bsd_name.value,
                                    entityType: rs1[0].logicalName
                                }]);
                            }
                        }
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                    }
                    if (rs1.length == 0) {
                        addressinvoicenameaccount.setValue(null);
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }

        },
    function (er) {
        console.log(er.message)
    });
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml2.push("<entity name='account'>");
        xml2.push("<attribute name='name' />");
        xml2.push("<attribute name='primarycontactid' />");
        xml2.push("<attribute name='telephone1' />");
        xml2.push("<attribute name='accountid' />");
        xml2.push("<attribute name='bsd_taxregistration' />");
        xml2.push("<attribute name='fax' />");
        xml2.push("<attribute name='accountnumber'/>");
        xml2.push("<attribute name='emailaddress1'/>");
        xml2.push("<attribute name='bsd_paymentterm'/>");
        xml2.push("<attribute name='bsd_paymentmethod'/>");
        xml2.push("<order attribute='createdon' descending='false' />");
        xml2.push("<filter type='and'>");
        xml2.push("<condition attribute='accountid' operator='eq' uitype='account ' value='" + invoicenameaccount[0].id + "' />");
        xml2.push("</filter>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
            if (rs2.length > 0) {
                if (Xrm.Page.getAttribute("bsd_invoiceaccount").getValue == null) {
                    if (rs2[0].attributes.accountnumber != null) {
                        Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(rs2[0].attributes.accountnumber.value);
                    }
                }
                if (Xrm.Page.getAttribute("bsd_taxregistration").getValue == null) {
                    if (rs2[0].attributes.bsd_taxregistration != null) {
                        Xrm.Page.getAttribute("bsd_taxregistration").setValue(rs2[0].attributes.bsd_taxregistration.value);
                    }
                }
                if (Xrm.Page.getAttribute("bsd_taxregistration").getValue == null) {
                    if (rs2[0].attributes.bsd_taxregistration == null) {
                        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                    }
                }

            }
        },
    function (er) {
        console.log(er.message)
    });
    }
    else {
        addressinvoicenameaccount.setValue(null);
        Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(null);
        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
        Xrm.Page.getControl("bsd_addressinvoiceaccount").setDisabled(true);
    }
}
//Author:Mr.Phong
//Description:gan gia tri cho fied ship to address
function set_addressfromcustomerb() {
    Xrm.Page.getControl("bsd_shiptoaddress").setDisabled(false);
    var customerb = Xrm.Page.getAttribute("bsd_partnerscompany").getValue();
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    if (customerb != null) {
        var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName2 = "bsd_address";
        var viewDisplayName2 = "test";
        fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customerb[0].id]);
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
            if (rs2.length > 0) {
                fieldshiptoaddress.setValue([{
                    id: rs2[0].Id,
                    name: rs2[0].attributes.bsd_name.value,
                    entityType: rs2[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
            }
            if (rs2.length == 0) {
                var viewId3 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName3 = "bsd_address";
                var viewDisplayName3 = "test";
                fetch(xml3, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customerb[0].id]);
                var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
            }

        }, function (er) {
            console.log(er.message)
        });
    }
    if (customerb == null) {
        var viewId3 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName3 = "bsd_address";
        var viewDisplayName3 = "test";
        xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml3.push("<entity name='bsd_address'>");
        xml3.push("<attribute name='bsd_addressid' />");
        xml3.push("<attribute name='bsd_name' />");
        xml3.push("<attribute name='createdon' />");
        xml3.push("<attribute name='bsd_purpose' />");
        xml3.push("<order attribute='bsd_name' descending='false' />");
        xml3.push("<filter type='and'>");
        xml3.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='{A42A355A-EF95-E821-90CC-000C294C7A2D}' />");
        xml3.push("<condition attribute='statecode' operator='eq' value='0' />");
        xml3.push("</filter>");
        xml3.push("</entity>");
        xml3.push("</fetch>");
        var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
        Xrm.Page.getAttribute("bsd_shiptoaddress").setValue(null);
    }
}
//Author:Mr.Phong
//Description:kiem tra ngay chon fromdate va todate
function checkdateonquote() {
    var fromday = Xrm.Page.getAttribute("bsd_fromdate").getValue();
    var today = Xrm.Page.getAttribute("bsd_todate").getValue();
    if (fromday != null) {
        Xrm.Page.getControl("bsd_fromdate").clearNotification();
        var year = fromday.getFullYear() + "";
        if (fromday.getMonth() + 1 <= 9) {
            var month = "0" + (fromday.getMonth() + 1);
        }
        else {
            var month = (fromday.getMonth() + 1);
        }
        var dayy = fromday.getDate();
    }
    if (fromday == null) {
        Xrm.Page.getAttribute("bsd_todate").setValue(null);
        Xrm.Page.getControl("bsd_fromdate").setNotification("From date is not null!!!");
    }
    if (today != null && fromday != null) {
        var yearr = today.getFullYear();
        if (fromday.getMonth() + 1 <= 9) {
            var monthh = "0" + (today.getMonth() + 1);
        }
        else {
            var monthh = (today.getMonth() + 1);
        }
        var day = today.getDate();
        if (yearr < year) {
            Xrm.Page.getControl("bsd_todate").setNotification("To date is not smaller than from date");
        }
        if (yearr == year) {
            if (monthh < month) {
                Xrm.Page.getControl("bsd_todate").setNotification("To date is not smaller than from date");
                Xrm.Page.getAttribute("bsd_todate").setValue(null);
            }
            if (monthh == month) {
                if (day => dayy) {
                    Xrm.Page.getControl("bsd_todate").clearNotification();
                }
                if (day < dayy) {
                    Xrm.Page.getControl("bsd_todate").setNotification("To date is not smaller than from date");
                    Xrm.Page.getAttribute("bsd_todate").setValue(null);

                }
            }
            if (monthh > month) {
                Xrm.Page.getControl("bsd_todate").clearNotification();
            }
        }
        if (yearr > year) {
            Xrm.Page.getControl("bsd_todate").clearNotification();
        }
    }
}
//Author:Mr.Phong
//Description:gan gia tri cho cac field address khi form la edit
function set_addressonload() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var factory = Xrm.Page.getAttribute("bsd_factory").getValue();
    var day = Xrm.Page.getAttribute("effectivefrom").getValue();
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    var year = day.getFullYear() + "";
    if (day.getMonth() + 1 <= 9) {
        var month = "0" + (day.getMonth() + 1) + "";
    }
    else {
        var month = (day.getMonth() + 1) + "";
    }
    var dayy = day.getDate() + "";
    var dateFormat = year + "-" + month + "-" + dayy;
    var xml1 = [];
    var xml2 = [];
    var xml4 = [];
    var xml7 = [];
    var xml8 = [];
    var xml9 = [];
    var xml10 = [];
    if (customer != null) {
        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName = "bsd_address";
        var viewDisplayName = "test";
        fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                     ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='bsd_addressid'>  " +
              "<cell name='bsd_name'   " + "width='200' />  " +
              "<cell name='createdon'    " + "width='100' />  " +
                "<cell name='bsd_purpose'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
        Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
        var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
        var entityName = "contact";
        var viewDisplayName = "test";
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='contact'>");
        xml1.push("<attribute name='fullname'/>");
        xml1.push("<attribute name='telephone1'/>")
        xml1.push("<attribute name='contactid'/>");
        xml1.push("<order attribute='fullname' descending='true' />");
        xml1.push("<filter type='and'>");
        xml1.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='contactid'>  " +
                         "<cell name='fullname'   " + "width='200' />  " +
                         "<cell name='telephone1'    " + "width='100' />  " +
                         "</row>   " +
                      "</grid>   ";
        Xrm.Page.getControl("bsd_contact").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
    }
    if (invoicenameaccount != null) {
        var viewId10 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName10 = "bsd_address";
        var viewDisplayName10 = "test";
        fetch(xml10, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, invoicenameaccount[0].id]);
        var layoutXml10 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='bsd_addressid'>  " +
              "<cell name='bsd_name'   " + "width='200' />  " +
              "<cell name='createdon'    " + "width='100' />  " +
                "<cell name='bsd_purpose'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId10, entityName10, viewDisplayName10, xml10.join(""), layoutXml10, true);
    }
}
//Author:Mr.Phong
//Description:gan gia tri cho field ship to address khi form la edit
function setshiptoaddressonload() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var customerb = Xrm.Page.getAttribute("bsd_partnerscompany").getValue();
    var shiptoadress = Xrm.Page.getAttribute("bsd_shiptoaddress").getValue();
    var xml2 = [];
    var xml3 = [];
    if (customer != null && customerb == null) {
        var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName2 = "bsd_address";
        var viewDisplayName2 = "test";
        fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customer[0].id]);
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";

        CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
            if (rs2.length > 0) {
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
            }
            if (rs2.length == 0) {
                var viewId3 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName3 = "bsd_address";
                var viewDisplayName3 = "test";
                fetch(xml3, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                    ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
            }

        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.Phong
//Description:hide button active quote
function hidebuttonactivequote() {
    var id = Xrm.Page.data.entity.getId();
    var xml = [];
    if (id != null) {
        fetch(xml, "quotedetail", ["productid", "productdescription", "priceperunit", "quantity", "extendedamount", "quotedetailid"], "productid", false, null
            , ["quoteid"], ["eq"], [0, id, 1]);
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length == 0) {
                if (window.top.document.getElementById("quote|NoRelationship|Form|Mscrm.Form.quote.ActivateQuote") != null) {
                    window.top.document.getElementById("quote|NoRelationship|Form|Mscrm.Form.quote.ActivateQuote").style.display = "none";

                }

            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.phong
//Description:set value field exchange rate
function setvaluefieldexchangerateonload() {
    var a = [];
    a = get_value(["transactioncurrencyid", "exchangerate"]);
    var xml = [];
    if (a[0] != null && a[2] == null) {
        fetch(xml, "transactioncurrency", ["transactioncurrencyid", "currencyname", "isocurrencycode", "currencysymbol", "exchangerate", "currencyprecision"], "currencyname", false, null
           , ["transactioncurrencyid"], ["eq"], [0, a[0], 1]);
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(rs[0].attributes.exchangerate.value);
            }
            if (rs.length == 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(null);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.phong
//Description:set value field exchange rate
function setvaluefieldexchangerateonchange() {
    var a = [];
    a = get_value(["transactioncurrencyid"]);
    var xml = [];
    if (a[0] != null) {
        fetch(xml, "transactioncurrency", ["transactioncurrencyid", "currencyname", "isocurrencycode", "currencysymbol", "exchangerate", "currencyprecision"], "currencyname", false, null
           , ["transactioncurrencyid"], ["eq"], [0, a[0], 1]);
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(rs[0].attributes.exchangerate.value);
            }
            if (rs.length == 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(null);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    if (a[0] == null) {
        Xrm.Page.getAttribute("exchangerate").setValue(null);
    }
}
//Author:Mr.Phong
//Description:chuyen tat ca cac field o cac section porter,shipping,tax ve trang thai nhu form tao moi
function setvaluesectionshippingwhenchangeshiptoaddressandwarehousefromfield()
{
    var warehouse = Xrm.Page.getAttribute("bsd_warehouseto").getValue();
    var shiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress").getValue();
    if (warehouse == null || shiptoaddress == null)
    {
        Xrm.Page.getAttribute("bsd_transportation").setValue(0);
        Xrm.Page.getAttribute("bsd_porteroption").setValue(0);
        set_value(["bsd_portertype", "bsd_shippingpricelistname", "bsd_unitshipping", "bsd_priceofporter"], null);
        set_value(["bsd_priceoftransportationn", "bsd_priceporter"], 0);
        Xrm.Page.getControl("bsd_porteroption").setDisabled(false);
        set_visible(["bsd_priceofporter", "bsd_priceporter", "bsd_typeofunit", "bsd_portertype", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn"], 0);
        set_visible(["bsd_porter"], 1);
        set_requirelevel(["bsd_priceofporter"], 0);
        set_value(["bsd_porter"], 861450001);
        setRequired(["bsd_priceoftransportationn", "bsd_shippingpricelistname", "bsd_priceofporter"], "none");
    }
}
//Author:Mr.Phong
//Description:gan gia tri cho field tax
function load_taxvalue() {
    var taxdefault = Xrm.Page.getAttribute("bsd_value");
    var xml = [];
    if (Xrm.Page.getAttribute("bsd_tax").getValue() == "1") {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xml.push("<entity name='bsd_saletaxcode'> ");
        xml.push("<attribute name='bsd_saletaxcodeid' /> ");
        xml.push("<attribute name='bsd_name' /> ");
        xml.push("<attribute name='createdon' /> ");
        xml.push("<order attribute='bsd_name' descending='false' /> ");
        xml.push("</entity> ");
        xml.push("</fetch> ");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0 && rs[0].attributes.bsd_name != null) {
                taxdefault.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.bsd_name.value,
                    entityType: rs[0].logicalName
                }]);
            }
        }, function (er) {
            console.log(er.message)
        });
    }

}
//Author:Mr.Phong
function validation() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    if (customer == null) {
        Xrm.Page.getAttribute("bsd_address").setValue(null);
    }
    if (invoicenameaccount == null) {
        Xrm.Page.getAttribute("bsd_addressinvoiceaccount").setValue(null);
    }
}
function clear_shippingpricelistname() {
    getControl("bsd_shippingpricelistname").addPreSearch(presearch_shippingpricelistname);
}
function presearch_shippingpricelistname() {
    getControl("bsd_shippingpricelistname").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}