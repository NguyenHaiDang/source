﻿//Author:Mr.Đăng
//Description:Check Name
function Check_Name() {
    var name = getValue("bsd_name");
    if (name != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_saletaxcode">',
                        '<attribute name="bsd_saletaxcodeid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_name" operator="eq" value="' + name + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setNotification("bsd_name", "Mã đã tồn tại, vui lòng kiểm tra lại.");
        } else {
            clearNotification("bsd_name");
        }
    }
}