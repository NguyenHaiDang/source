﻿//Author:Mr.Đăng
//Description:Check Code & set Name
function Check_Code() {
    var code = getValue("bsd_code");
    if (code != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_itemsalestaxgroup">',
                        '<attribute name="bsd_itemsalestaxgroupid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_code" operator="eq" value="' + code + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setNotification("bsd_code", "Mã đã tồn tại, vui lòng kiểm tra lại.");
        } else {
            clearNotification("bsd_code");
        }
        setValue("bsd_name", code);
    }
    else {
        setNull("bsd_name");
    }
}
//Author:Mr.Đăng
//Description:Check Code
function load_description() {
    var saletaxcode = getValue("bsd_saletaxcode");
    if (saletaxcode != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_saletaxcode">',
                        '<attribute name="bsd_saletaxcodeid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_description" />',
                        '<attribute name="bsd_percentageamount" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_saletaxcodeid" operator="eq" uitype="bsd_saletaxcode" value="' + saletaxcode[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        console.log(rs);
        if (rs.length > 0) {
            setValue("bsd_percentageamount", rs[0].attributes.bsd_percentageamount.value);
            setValue("bsd_description", rs[0].attributes.bsd_description.value);
        }
    }
    else {
        setValue("bsd_percentageamount", null);
        setValue("bsd_description", null);
    }
}