function init() {
    load_congno();
    load_contact_invoice();
    load_contact_shiptoaccount();
    load_shiptoaddress();
    filter_orderdeliverystatus_grid();
    var status = getValue("bsd_status");
    if (status == 861450003 || check_disabled_form()) {
        DisabledForm();
    }
    hide_button();
    Load_DatepaymentTerm();
    Duedate_change();
}

function filter_orderdeliverystatus_grid() {
    var objSubGrid = window.parent.document.getElementById("orderdelierystatusSubgrid");
    if (objSubGrid != null && objSubGrid.control != null) {
        var xml = [
            '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_orderdeliverystatus">',
            '<attribute name="bsd_orderdeliverystatusid" />',
            '<attribute name="bsd_name" />',
            '<attribute name="createdon" />',
            '<attribute name="statecode" />',
            '<attribute name="bsd_shippedquantity" />',
            '<attribute name="bsd_remainingquantity" />',
            '<attribute name="bsd_product" />',
            '<attribute name="bsd_orderquantity" />',
            '<attribute name="bsd_order" />',
            '<order attribute="bsd_name" descending="false" />',
            '<filter type="and">',
              '<condition attribute="bsd_order" operator="eq" uiname="Bảng báo giá" uitype="salesorder" value="' + getValue("bsd_order")[0].id + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'];
        objSubGrid.control.SetParameter("fetchXML", xml);
        objSubGrid.control.Refresh();
    } else {
        setTimeout('filter_orderdeliverystatus_grid()', 1000);
    }
}

function check_disabled_form() {
    // kiếm tra nếu đa tạo kế hoạch giao hàng thì readonly form
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_deliveryplan">',
            '<attribute name="bsd_deliveryplanid" />',
            '<attribute name="bsd_name" />',
            '<attribute name="createdon" />',
            '<order attribute="bsd_name" descending="false" />',
            '<filter type="and">',
              '<condition attribute="bsd_suborder" operator="eq" uitype="bsd_suborder" value="' + Xrm.Page.data.entity.getId() + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
    var rs = CrmFetchKit.FetchSync(xml);
    if (rs.length > 0) {
        return true;
    } else {
        return false;
    }
}
function load_contact_invoice() {
    var invoice_account = getValue("bsd_invoicenameaccount");
    if (invoice_account != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="contact">',
        '<attribute name="fullname" />',
        '<attribute name="telephone1" />',
        '<attribute name="contactid" />',
        '<order attribute="fullname" descending="false" />',
        '<filter type="and">',
          '<condition attribute="parentcustomerid" operator="eq" uitype="account" value="' + invoice_account[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                            "<row name='result'  " + "id='contactid'>  " +
                                            "<cell name='fullname'   " + "width='200' />  " +
                                            "</row>" +
                                         "</grid>";
        getControl("bsd_contactinvoiceaccount").addCustomView(getDefaultView("bsd_contactinvoiceaccount"), "contact", "contact", xml, layoutXml, true);
    } else {
        setDisabled("bsd_contactinvoiceaccount", true);
    }
}
function load_contact_shiptoaccount() {
    var bsd_shiptoaccount = getValue("bsd_shiptoaccount");
    if (bsd_shiptoaccount != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="contact">',
        '<attribute name="fullname" />',
        '<attribute name="telephone1" />',
        '<attribute name="contactid" />',
        '<order attribute="fullname" descending="false" />',
        '<filter type="and">',
          '<condition attribute="parentcustomerid" operator="eq" uitype="account" value="' + bsd_shiptoaccount[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                            "<row name='result'  " + "id='contactid'>  " +
                                            "<cell name='fullname'   " + "width='200' />  " +
                                            "</row>" +
                                         "</grid>";
        getControl("bsd_contactshiptoaccount").addCustomView(getDefaultView("bsd_contactshiptoaccount"), "contact", "contact", xml, layoutXml, true);
    } else {
        setDisabled("bsd_contactshiptoaccount", true);
    }
}
function load_shiptoaddress() {
    var xml = [
		'<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
	  '<entity name="bsd_address">',
	    '<attribute name="bsd_name" />',
	    '<attribute name="bsd_purpose" />',
	    '<attribute name="bsd_account" />',
	    '<attribute name="bsd_contact" />',
	    '<attribute name="bsd_addressid" />',
	    '<order attribute="bsd_name" descending="false" />',
	    '<filter type="and">',
	      '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + getValue("bsd_potentialcustomer")[0].id + '" />',
	      '<condition attribute="bsd_purpose" operator="eq" value="861450002" />',
	      '<condition attribute="statecode" operator="eq" value="0" />',
	    '</filter>',
	  '</entity>',
	'</fetch>'
    ].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                            "<row name='result'  " + "id='bsd_addressid'>  " +
                                            "<cell name='bsd_name'   " + "width='200' />  " +
                                            "</row>" +
                                         "</grid>";
    getControl("bsd_shiptoaddress").addCustomView(getDefaultView("bsd_shiptoaddress"), "bsd_address", "address", xml, layoutXml, true);
}
function load_timeship() {
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="account">',
        '<attribute name="accountid" />',
        '<attribute name="bsd_timeship" />',
        '<filter type="and">',
          '<condition attribute="accountid" operator="eq" uitype="account" value="' + getValue("bsd_potentialcustomer")[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        var timeship = rs[0].getValue("bsd_timeship");
        console.log(timeship);
        setValue("bsd_timeship", timeship);
    });
}
function load_congno() {
    var amount = 0;

    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="account">',
      '<attribute name="accountid" />',
        '<attribute name="bsd_amountbalance" />',
        '<attribute name="creditlimit" />',
        '<filter type="and">',
          '<condition attribute="accountid" operator="eq" uitype="account" value="' + getValue("bsd_potentialcustomer")[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    var rs = CrmFetchKit.FetchSync(xml);
    var account = rs[0];
    if (account.getValue("creditlimit") != null) {
        setValue("bsd_creditlimit", account.getValue("creditlimit"))
    } else {
        setValue("bsd_creditlimit", 0)
    }
    if (account.getValue("bsd_amountbalance")) {
        amount = account.getValue("bsd_amountbalance");
    }
    var duyet = getValue("bsd_duyet");
    setDisabled("bsd_duyet", duyet);
    if (duyet == true) {
        setValue("bsd_customerdebt", amount);
        setVisible(["bsd_olddebt", "bsd_newdebt"], false);
        setVisible(["bsd_customerdebt"], true);
    } else {
        var suborder_amount = getValue("bsd_totalamount");
        setVisible(["bsd_olddebt", "bsd_newdebt"], true);
        setVisible(["bsd_customerdebt"], false);
        setValue("bsd_olddebt", amount);
        setValue("bsd_newdebt", amount + suborder_amount);
        setValue("bsd_customerdebt", amount + suborder_amount);
    }
    Xrm.Page.data.save();
}
function duyet() {
    var id = Xrm.Page.data.entity.getId();
    var duyet = getValue("bsd_duyet");
    if (duyet == true) {
        window.top.$ui.Confirm("Confirm", "Are you sure ?", function (e) {
            ExecuteAction(id, Xrm.Page.data.entity.getEntityName(), "bsd_duyetcongno", null, function (result) {
                console.log(result);
                if (result != null && result.status != null) {
                    if (result.status == "success") {
                        setDisabled("bsd_duyet", true);
                        setValue("bsd_duyet", true);
                        setVisible(["bsd_customerdebt"], true);
                        setVisible(["bsd_olddebt", "bsd_newdebt"], false);
                        Xrm.Page.ui.setFormNotification("Suborder đã duyệt thành công. ", "INFO", "1");
                        setTimeout(function () {
                            Xrm.Page.ui.clearFormNotification('1');
                        }, 10000);

                    } else if (result.status == "error") {
                        m_alert(result.data);
                        setValue("bsd_duyet", false);
                        setDisabled("bsd_duyet", false);
                    } else {
                        m_alert(result.data);
                        setValue("bsd_duyet", false);
                        setDisabled("bsd_duyet", false);
                    }
                }
            });
        }, function () {
            setValue("bsd_duyet", false);
        });
    }
}

function BtnDeliveryPlanClick() {
    Xrm.Page.data.save().then(function () {
        var id = Xrm.Page.data.entity.getId();
        var xml = [
            '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_deliveryplan">',
                '<attribute name="bsd_deliveryplanid" />',
                '<filter type="and">',
                  '<condition attribute="bsd_suborder" operator="eq" uitype="bsd_suborder" value="' + id + '" />',
                '</filter>',
              '</entity>',
            '</fetch>'
        ].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Utility.openEntityForm("bsd_deliveryplan", rs[0].getValue("bsd_deliveryplanid"));
            } else {
                var check = getValue("bsd_duyet");
                if (check) {
                    if (confirm("Are you sure to create the Delivery Plan ?")) {
                        ExecuteAction(id, Xrm.Page.data.entity.getEntityName(), "bsd_CreateDeliveryPlan", null, function (result) {
                            if (result != null && result.status != null) {
                                if (result.status == "success") {
                                    Xrm.Utility.openEntityForm("bsd_deliveryplan", result.data.ReturnId.value);
                                } else if (result.status == "error") {
                                    m_alert(result.data);
                                } else {
                                    m_alert(result.data);
                                }
                            }
                        });
                    }
                } else {
                    m_alert("Duyệt Sub Order trước khi tạo kế hoạch giao hàng ");
                }
            }
        });
    });
}
function BtnDeliveryPlanEnableRule() {
    if (getValue("bsd_status") != 861450003) {
        return true;
    } else {
        return false;
    }
}

function BtnApprove(Selected) {
    console.log(Selected);
    alert("ok");
    alert("Chon : " + Selected.length)
}
function BtnApproveEnableRule() {
    return true;
}

function BtnCancelSubOrder() {
    var duyet = getValue("bsd_duyet");
    if (duyet == false) {
        m_confirm("Are you sure to cancel suborder ?", function (e) {
            var id = Xrm.Page.data.entity.getId();
            ExecuteAction(id, Xrm.Page.data.entity.getEntityName(), "bsd_CancelSubOrder", null, function (result) {
                if (result != null && result.status != null) {
                    if (result.status == "success") {
                        Xrm.Utility.openEntityForm(Xrm.Page.data.entity.getEntityName(), id);
                    } else if (result.status == "error") {
                        m_alert(result.data);
                    } else {
                        m_alert(result.data);
                    }
                }
            });
        });
    } else {
        m_alert("Đã duyệt rồi");
    }

}
function BtnCancelSubOrderEnableRule() {
    var duyet = getValue("bsd_duyet");
    if (duyet == false && getValue("bsd_status") != 861450003) {
        return true;
    } else {
        return false;
    }
}

function request_receipt_date_change() {
    var date = getValue("bsd_requestedreceiptdate");
    if (date != null) {
        var timeship = getValue("bsd_timeship");
        if (getValue("bsd_timeship") == null) {
            timeship = 0;
        }
        date.setDate(date.getDate() - timeship);
        setValue("bsd_requestedshipdate", date);
    }
}

function request_ship_date_change() {
    var date = getValue("bsd_requestedshipdate");
    if (date != null) {
        var timeship = getValue("bsd_timeship");
        if (getValue("bsd_timeship") == null) {
            timeship = 0;
        }
        date.setDate(date.getDate() + timeship);
        setValue("bsd_requestedreceiptdate", date);
    }
}

function preventAutoSave(econtext) {
    var eventArgs = econtext.getEventArgs();
    if (eventArgs.getSaveMode() == 70 || eventArgs.getSaveMode() == 2) {
        eventArgs.preventDefault();
    }
}
//author:Mr.Phong
function hide_button() {
    //window.parent.document.getElementById("stageBackActionContainer").style.display = "none";
    //window.parent.document.getElementById("stageBackActionContainer").style.visibility = "hidden";
    //window.parent.document.getElementById("stageAdvanceActionContainer").style.display = "none";
    //window.parent.document.getElementById("stageAdvanceActionContainer").style.visibility = "hidden";
    //window.parent.document.getElementById("stageSetActiveActionContainer").style.display = "none";
    //window.parent.document.getElementById("stageSetActiveActionContainer").style.visibility = "hidden";
}
//Author:Mr.Đăng
//Description: Load Date PaymentTerm
function Load_DatepaymentTerm() {
    var paymentterm = getValue("bsd_paymentterm");
    if (paymentterm != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_paymentterm">',
                        '<attribute name="bsd_paymenttermid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_date" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_paymenttermid" operator="eq" uiname="5D" value="' + paymentterm[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setValue("bsd_datept", rs[0].attributes.bsd_date.value);
        } else {
            setValue("bsd_datept", null);
        }
    }
}
//Author:Mr.Đăng
//Description: Load Due Date
function Duedate_change() {
    var date = getValue("bsd_date");
    var duedate = getValue("bsd_duedate");
    if (date != null && duedate == null) {
        var datept = getValue("bsd_datept");
        if (datept != null) {
            date.setDate(date.getDate() + datept);
            setValue("bsd_duedate", date);
        }
    }
}