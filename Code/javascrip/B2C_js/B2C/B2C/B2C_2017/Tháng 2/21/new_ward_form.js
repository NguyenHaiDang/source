﻿//author:Mr.Phong
//description:set district theo province
function set_district() {
    var city = Xrm.Page.getAttribute("new_province").getValue();
    var districdefault = Xrm.Page.getAttribute("bsd_district");
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
        if (city != null) {
            var viewId = "{5F60D1E4-F7DF-476A-88B9-AC0EDE10E321}";
            var entityName = "bsd_district";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_district'>");
            xml.push("<attribute name='bsd_districtid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_province' operator='eq' value='" + city[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_districtid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_districtid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";       
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {              
                    districdefault.setValue(null);
                     Xrm.Page.getControl("bsd_district").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);                
            }, function (er) {
                console.log(er.message)
            });
        }
        else {
            Xrm.Page.getAttribute("bsd_district").setValue(null);
            clear_district();
        }
}
function clear_district() {
    Xrm.Page.getControl("bsd_district").addPreSearch(presearch_district);
}
function presearch_district() {
    Xrm.Page.getControl("bsd_district").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//author:Mr.Phong
function validation_district() {
    var district = Xrm.Page.getAttribute("bsd_district").getValue();
    var project = Xrm.Page.getAttribute("new_province").getValue();
    if (project == null ) {
        Xrm.Page.getAttribute("bsd_district").setValue(null);
    } 
        if (district == null) {
            Xrm.Page.getAttribute("bsd_ward").setValue(null);
        }
}
//author:Mr.Phong
//description:set district theo province
function set_districtonload() {
    var city = Xrm.Page.getAttribute("new_province").getValue();
    var districdefault = Xrm.Page.getAttribute("bsd_district");
    if (city != null) {
        var viewId = "{5F60D1E4-F7DF-476A-88B9-AC0EDE10E321}";
        var entityName = "bsd_district";
        var viewDisplayName = "test";
        var xml = [];
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_district'>");
        xml.push("<attribute name='bsd_districtid'/>");
        xml.push("<attribute name='bsd_name' />")
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_province' operator='eq' value='" + city[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_districtid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_districtid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_district").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);

    }
    else {
        Xrm.Page.getAttribute("bsd_district").setValue(null);
    }
}
//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    set_disableWard();
}
//Author:Mr.Đăng
//Description: Disable Ward
function set_disableWard() {
    if (formType() == 2) {
        setDisabled("bsd_id", true);
    }
    else {
        setDisabled("bsd_id", false);
    }
}
//Author:Mr.Đăng
//Description:Check Wardid
function Check_WardID() {
    var id = getValue("bsd_id");
    if (id != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_ward">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="new_province" />',
                        '<attribute name="bsd_wardid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_id" operator="eq" value="'+ id +'" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setNotification("bsd_id", "Mã phường/xã đã tồn tại, vui lòng kiểm tra lại.");
        } else {
            clearNotification("bsd_id");
        }
    }
}