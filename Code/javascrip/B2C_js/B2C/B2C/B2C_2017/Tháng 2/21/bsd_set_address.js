function set_address() {
    Xrm.Page.getAttribute("bsd_name").setSubmitMode("always");
    var country = Xrm.Page.getAttribute("bsd_country").getValue();
    var region = Xrm.Page.getAttribute("bsd_region").getValue();
    var city = Xrm.Page.getAttribute("bsd_province").getValue();
    var district = Xrm.Page.getAttribute("bsd_district").getValue();
    var ward = Xrm.Page.getAttribute("bsd_ward").getValue();
    var street = Xrm.Page.getAttribute("bsd_street").getValue();

    if (street == null) {
        Xrm.Page.getAttribute("bsd_name").setValue(ward[0].name + ", " + district[0].name + ", " + city[0].name + ", " + region[0].name + ", " + country[0].name);
    }
    else if (street != null) {
        Xrm.Page.getAttribute("bsd_name").setValue(street + ", " + ward[0].name + ", " + district[0].name + ", " + city[0].name + ", " + region[0].name + ", " + country[0].name);
    }
    /*
    if (city != null && district != null && ward != null && street != null) {
        Xrm.Page.getAttribute("bsd_name").setValue(street + " " + "," + ward[0].name + " " + "," + district[0].name + " " + "," + city[0].name);
    }
    if (city != null && district == null && ward != null && street != null) {
        Xrm.Page.getAttribute("bsd_name").setValue(street + " " + "," + ward[0].name + " " + "," + city[0].name);
    }
    if (city != null && district != null && ward == null && street != null) {
        Xrm.Page.getAttribute("bsd_name").setValue(street + " " + "," + district[0].name + " " + "," + city[0].name);
    }
    if (city != null && district != null && ward != null && street == null) {
        Xrm.Page.getAttribute("bsd_name").setValue(ward[0].name + " " + "," + district[0].name + " " + "," + city[0].name);
    }
    if (city != null && district == null && ward == null && street != null) {
        Xrm.Page.getAttribute("bsd_name").setValue(street + " " + "," + city[0].name);
    }
    if (city != null && district != null && ward == null && street == null) {
        Xrm.Page.getAttribute("bsd_name").setValue(district[0].name + " " + "," +city[0].name);
    }
    */
}