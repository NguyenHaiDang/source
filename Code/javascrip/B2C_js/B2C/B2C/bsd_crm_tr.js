function getId() {
    return Xrm.Page.data.entity.getId();
}
function getEntityName() {
    return Xrm.Page.data.entity.getEntityName();
}
function setRequired(attributeName, level) {
    if (typeof attributeName == 'object') {
        for (var i = 0; i < attributeName.length; i++) {
            Xrm.Page.getAttribute(attributeName[i]).setRequiredLevel(level);
        }
    } else if (typeof attributeName == 'string') {
        Xrm.Page.getAttribute(attributeName).setRequiredLevel(level);
    }
}
function setVisible(attributeName, visibiled) {
    if (typeof attributeName == 'object') {
        for (var i = 0; i < attributeName.length; i++) {
            Xrm.Page.ui.controls.get(attributeName[i]).setVisible(visibiled);
        }
    } else if (typeof attributeName == 'string') {
        Xrm.Page.ui.controls.get(attributeName).setVisible(visibiled);
    }
}
function setDisabled(attributeName, disabled) {
    if (typeof attributeName == 'object') {
        for (var i = 0; i < attributeName.length; i++) {
            Xrm.Page.getControl(attributeName[i]).setDisabled(disabled);
        }
    } else if (typeof attributeName == 'string') {
        Xrm.Page.getControl(attributeName).setDisabled(disabled);
    }
}
function setValue(attributeName, value) {
    Xrm.Page.getAttribute(attributeName).setValue(value);
}
function setNull(attributeName) {
    if (typeof attributeName == 'object') {
        for (var i = 0; i < attributeName.length; i++) {
            Xrm.Page.getAttribute(attributeName[i]).setValue(null);
        }
    } else if (typeof attributeName == 'string') {
        Xrm.Page.getAttribute(attributeName).setValue(null);
    }
}
function getValue(attributeName) {
    return Xrm.Page.getAttribute(attributeName).getValue();
}
function getControl(attributeName) {
    return Xrm.Page.getControl(attributeName);
}
function setFormNotification(message, type, id) {
    Xrm.Page.ui.setFormNotification(message, type, id);
}
function clearFormNotification(id) {
    Xrm.Page.ui.clearFormNotification(id);
}
function setNotification(attributeName, message) {
    Xrm.Page.getControl(attributeName).setNotification(message);
} // set form
function clearNotification(attributeName) {
    Xrm.Page.getControl(attributeName).clearNotification();
} // clear form
function formType() {
    return Xrm.Page.ui.getFormType();
}
function getDefaultView(lookupfield) {
    return Xrm.Page.getControl(lookupfield).getDefaultView();
}
function check_all_field_required_valid() {
    var populated = true;
    Xrm.Page.getAttribute(function (attribute, index) {
        if (attribute.getRequiredLevel() == "required") {
            if (attribute.getValue() === null) {
                populated = false;
            }
        }
    });
    return populated;
}
function DisabledForm() {
    Xrm.Page.data.entity.attributes.forEach(function (attribute, index) {
        var control = Xrm.Page.getControl(attribute.getName());
        if (control) {
            control.setDisabled(true)
        }
    });
}
function errorHandler(err) {

}
function m_confirm(content, callback) {
    window.top.$ui.Confirm("Confirm", content, function (e) {
        callback();
    });
}
function m_alert(message) {
    window.top.$ui.Dialog("Infomation", message);
}
function setSubmitMode(attributeName, type) {
    if (typeof attributeName == 'object') {
        for (var i = 0; i < attributeName.length; i++) {
            Xrm.Page.getAttribute(attributeName[i]).setSubmitMode(type);
        }
    } else if (typeof attributeName == 'string') {
        Xrm.Page.getAttribute(attributeName).setSubmitMode(type);
    }
}
// Xrm.Page.data.save().then(function () {});
//Xrm.Page.getAttribute("bsd_quantity").setSubmitMode("always");
//Phong
// JavaScript library
function fetch(xml, entity, attribute, orderattribute, descendingvalue, ascendingvalue, conditionattribute, operator, conditionvalue) {
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='" + entity + "'>");
    for (var i = 0; i < attribute.length; i++) {
        xml.push("<attribute name='" + attribute[i] + "' />");
    }
    if (descendingvalue != null) {
        xml.push("<order attribute='" + orderattribute + "' descending='" + descendingvalue + "' />");
    }
    if (ascendingvalue != null) {
        xml.push("<order attribute='" + orderattribute + "' descending='" + ascendingvalue + "' />");
    }
    xml.push("<filter type='and'>");
    for (var j = 0; j < conditionattribute.length; j++) {
        if (operator[j] == "inn") {
            xml.push("<condition attribute='" + conditionattribute[j] + "' operator='in'>");
            for (var k = 0; k < conditionvalue.length; k++) {
                if (conditionvalue[k] === parseInt(conditionvalue[k], 10)) {
                    if (j == conditionvalue[k]) {
                        for (var p = 1; p < conditionvalue.length; p++) {
                            if (conditionvalue[k + p] === parseInt(conditionvalue[k + p], 10) || conditionvalue[k + p] == null) {
                                xml.push("</condition>");
                                break;
                            }
                            else {
                                xml.push("<value>" + conditionvalue[k + p] + "");
                                xml.push("</value>");
                            }
                        }
                    }
                }
            }
        }
        else {
            for (var k = 0; k < conditionvalue.length; k++) {
                if (conditionvalue[k] === parseInt(conditionvalue[k], 10)) {
                    if (j == conditionvalue[k]) {
                        for (var p = 1; p < conditionvalue.length; p++) {
                            if (conditionvalue[k + p] === parseInt(conditionvalue[k + p], 10) || conditionvalue[k + p] == null) {
                                break;
                            }
                            else {
                                xml.push("<condition attribute='" + conditionattribute[j] + "' operator='" + operator[j] + "'  value='" + conditionvalue[k + p] + "' />");
                            }
                        }
                    }
                }
            }
        }
    }
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
}
function setvalue(a, b) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getAttribute(a[i]).setValue(b);
    }
}
function getvalue(a) {
    var b = [];
    for (var i = 0; i < a.length; i++) {
        var j = Xrm.Page.getAttribute(a[i]).getValue();
        if (j != null) {
            var string = typeof j;
            if (string == "string") {
                b.push(j);
            }
            if (string == "boolean") {
                if (j == true) {
                    b.push("1");
                }
                if (j == false) {
                    b.push("0");
                }
            }
            if (string == "number") {
                b.push(j);
            }
            if (string == "object") {
                if (j[0].id != null) {
                    b.push(j[0].id);
                }
                if (j[0].name != null) {
                    b.push(j[0].name);
                }
            }
        }
        if (j == null) {
            b.push(null);
        }
    }
    return b;
}
//end
function reload_page() {
    Xrm.Utility.openEntityForm(getEntityName(), getId());
}