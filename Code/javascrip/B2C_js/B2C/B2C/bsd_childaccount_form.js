﻿//Descriptiom:set null filed  child account when formtype ==1
function init() {
    setDisabled_ParentAccount();
    if (formType() == 1) {
        setNull("bsd_childaccount");
    }
}
// 22.03.2017 Đăng: setDisabled Account
function setDisabled_ParentAccount() {
    var parentaccount = getValue("bsd_parentaccount");
    if (parentaccount != null) {
        setDisabled("bsd_parentaccount", true);
    }
}