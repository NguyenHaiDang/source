function init() {
    if (getValue("bsd_quote") != null) {
        setVisible("bsd_quote", true);
    } else if (getValue("bsd_order") != null) {
        setVisible("bsd_order", true);
    }
}


function BtnAddTruck(SelectedIds) {
    var grid = window.parent.document.getElementById("deliveryplanproductSubgrid").control.get_selectedRecords();
    if (grid.length > 0) {

        var total_quantity = 0;
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_deliveryplanproduct">',
            '<attribute name="bsd_deliveryplanproductid" />',
            '<attribute name="bsd_shipquantity" />',
            '<attribute name="bsd_warehouse" />',
            '<attribute name="bsd_unit" />',
             '<attribute name="bsd_product" />',
            '<filter type="and">',
              '<condition attribute="bsd_deliveryplanproductid" operator="eq" uitype="bsd_deliveryplanproduct" value="' + grid[0].Id + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            total_quantity = rs[0].getValue("bsd_shipquantity");
            var xml2 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_deliveryplantruck">',
                '<attribute name="bsd_deliveryplantruckid" />',
                '<attribute name="bsd_quantity" />',
                '<filter type="and">',
                  '<condition attribute="bsd_deliveryplanproduct" operator="eq" uitype="bsd_deliveryplanproduct" value="' + grid[0].Id + '" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join("");
            CrmFetchKit.Fetch(xml2, false).then(function (result) {
                var shipped_quantity = 0;
                if (result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].getValue("bsd_quantity") != null) {
                            shipped_quantity += result[i].getValue("bsd_quantity");
                        }
                    }
                }
                if (shipped_quantity < total_quantity) {
                    var params = {};
                    params["bsd_totalquantity"] = total_quantity;
                    params["bsd_name"] = grid[0].Name;

                    params["bsd_deliveryplan"] = Xrm.Page.data.entity.getId();
                    params["bsd_deliveryplanname"] = getValue("bsd_name");

                    params["bsd_deliveryplanproduct"] = grid[0].Id;
                    params["bsd_deliveryplanproductname"] = grid[0].Name;

                    params["bsd_warehouse"] = rs[0].getValue("bsd_warehouse");
                    params["bsd_warehousename"] = rs[0].attributes.bsd_warehouse.name;

                    params["bsd_unit"] = rs[0].getValue("bsd_unit");
                    params["bsd_unitname"] = rs[0].attributes.bsd_unit.name;

                    params["bsd_product"] = rs[0].getValue("bsd_product");
                    params["bsd_productname"] = rs[0].attributes.bsd_product.name;

                    Xrm.Utility.openEntityForm("bsd_deliveryplantruck", null, params, { openInNewWindow: false });
                    Xrm.Page.ui.clearFormNotification('1');
                } else {
                    Xrm.Page.ui.setFormNotification("Đã đủ số lượng ", "INFO", "1");
                }
            });

        });
    } else {
        alert("Select product to create Add truck !");
    }
}
function BtnAddTruckEnableRule() {
    return true;
}

function BtnViewTruck() {
    var grid = window.parent.document.getElementById("deliveryplanproductSubgrid").control.get_selectedRecords();
    var xml = [];
    if (grid.length > 0) {
        xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_deliveryplantruck">',
                '<attribute name="bsd_deliveryplantruckid" />',
                '<attribute name="bsd_quantity" />',
                '<filter type="and">',
                  '<condition attribute="bsd_deliveryplanproduct" operator="eq" uitype="bsd_deliveryplanproduct" value="' + grid[0].Id + '" />',
                '</filter>',
                '<link-entity name="product" from="productid" to="bsd_product" visible="false" link-type="outer" alias="a_1f6f33e4bbd3e61193f1000c29d47eab">',
                '  <attribute name="productnumber" />',
                '</link-entity>',
              '</entity>',
            '</fetch>'].join("");
    } else {
        xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_deliveryplantruck">',
                '<attribute name="bsd_deliveryplantruckid" />',
                '<attribute name="bsd_quantity" />',
                '<filter type="and">',
                  '<condition attribute="bsd_deliveryplan" operator="eq" uitype="bsd_deliveryplan" value="' + Xrm.Page.data.entity.getId() + '" />',
                '</filter>',
                '<link-entity name="product" from="productid" to="bsd_product" visible="false" link-type="outer" alias="a_1f6f33e4bbd3e61193f1000c29d47eab">',
                '  <attribute name="productnumber" />',
                '</link-entity>',
              '</entity>',
            '</fetch>'].join("");
    }
    var objSubGrid = window.parent.document.getElementById("truckSubgrid");
    objSubGrid.control.SetParameter("fetchXML", xml);
    objSubGrid.control.Refresh();
}
function BtnViewTruckEnableRule() {
    return true;
}


function BtnRequestDelivery(Selected) {
    var id = Xrm.Page.data.entity.getId();
    var entityname = "bsd_deliveryplan";
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="bsd_deliveryplantruck">',
        '<attribute name="bsd_deliveryplantruckid" />',
        '<attribute name="bsd_name" />',
        '<attribute name="createdon" />',
        '<order attribute="bsd_name" descending="false" />',
        '<filter type="and">',
          '<condition attribute="bsd_status" operator="eq" value="861450001" />',
          '<condition attribute="bsd_deliveryplan" operator="eq" uitype="bsd_deliveryplan" value="' + id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            if (confirm("Do you want to create Request Delivery ?")) {
                ExecuteAction(id, entityname, "bsd_CreateRequestDelivery", null, function (result) {
                    if (result != null && result.status != null) {
                        if (result.status == "success") {
                            Xrm.Utility.openEntityForm(Xrm.Page.data.entity.getEntityName(), Xrm.Page.data.entity.getId());
                        } else {
                            alert(result.data);
                        }
                    }
                });
            }
        } else {
            alert("Please select the Addtruck before Create Request Delivery!");
        }
    });
}

function BtnDeliveryBill() {
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
     '<entity name="bsd_requestdelivery">',
       '<attribute name="bsd_requestdeliveryid" />',
       '<attribute name="bsd_name" />',
       '<filter type="and">',
         '<condition attribute="bsd_deliveryplan" operator="eq" uitype="bsd_deliveryplan" value="' + getId() + '" />',
         '<condition attribute="bsd_createddeliverybill" operator="eq" value="0" />',
         '<condition attribute="bsd_warehousestatus" operator="eq" value="1" />',
       '</filter>',
     '</entity>',
   '</fetch>'].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            Xrm.Page.ui.clearFormNotification('1');
            if (confirm("Are you sure to create new delivery bill")) {
                ExecuteAction(getId(), "bsd_deliveryplan", "bsd_Action_CreateDeliveryBill", null, function (result) {
                    if (result != null && result.status != null) {
                        if (result.status == "success") {
                            Xrm.Utility.openEntityForm(Xrm.Page.data.entity.getEntityName(), Xrm.Page.data.entity.getId());
                        } else if (result.status == "error") {
                            alert(result.data);
                        } else {
                            alert(result.data);
                        }
                    }
                });
            }
        } else {
            Xrm.Page.ui.setFormNotification("Không có yêu cầu giao hàng nào đủ điều kiện tạo phiếu xuất kho !", "INFO", "1");
        }
    });;
}
function BtnDeliveryBillEnableRule() {
    return false;
}