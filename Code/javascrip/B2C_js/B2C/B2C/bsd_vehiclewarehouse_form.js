﻿//23.03.2017 Đăng: Bắt trùng Employee
function check_Employee() {
    var employee = getValue("bsd_employee");
    var id = getId();
    if (employee != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_vehiclewarehouse">',
                        '<attribute name="bsd_vehiclewarehouseid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_employee" operator="eq" uiname="Trần Thanh Tú" uitype="bsd_employee" value="' + employee[0].id + '" />',
                        '<condition attribute="statecode" operator="eq" value="0" />',
                        '<condition attribute="bsd_vehiclewarehouseid" operator="ne" value="' + id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            if (rs.length > 0) {
                setNotification("bsd_employee", "Nhân viên đã tồn tại, vui lòng kiểm tra lại.");
            }
            else {
                clearNotification("bsd_employee");
            }
        }, function (er) { });
    }
}