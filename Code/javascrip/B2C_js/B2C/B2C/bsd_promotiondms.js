/*
Huy 30/12/2016
note: update setDisabled

Diệm 3/3/2017:  bỏ field quantityto và priceto
     10/3/2017 thay đổi 80% giao diện.

*/
function AutoLoad() {
    if (formType() == 2) {
        if (getValue("bsd_haduse")) {
            DisabledForm();
        } else {
            setDisabled(["bsd_name", "bsd_typeprice"], true);
        }
        LK_country(false);
        Load_Quantity_from_to_Visible();
        Load_Amount_Type();
        Account_Loadding();
    } else {
        Load_Amount_Type();
        Load_Quantity_from_to_Visible();
        clear_region();
        clear_area();
        clear_province();
    }
}

//Diệm
// Kiểm tra nhập từ ngày đến ngày
function checkDateFromTo() {
    CheckDateFromTo("bsd_fromdate", "bsd_todate");
}
/*
    Diệm hàm kiểm tra save auto.
*/
function AutoSave() {
    Load_SubripSectionAccount();
}
/*
Diệm
Note: change area.
*/
function clear_distributor() {
    getControl("bsd_distributor").addPreSearch(presearch_distributor);
}
function presearch_distributor() {
    getControl("bsd_distributor").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Mr:Diệm
//Note: search region follow country
function LK_country(reset) {
    LK_Region(reset);
    if (reset != false) setNull(["bsd_region", "bsd_area", "bsd_province"]);
    getControl("bsd_region").removePreSearch(presearch_region);
    var country = getValue("bsd_country");
    if (country != null) {
        setRequired("bsd_country", "required");
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_region">',
                        '<attribute name="bsd_regionid"/>',
                        '<attribute name="bsd_name"/>',
                        '<attribute name="createdon"/>',
                        '<order attribute="bsd_name" descending="false"/>',
                        '<filter type="and">',
                        '<condition attribute="bsd_country" operator="eq" uitype="bsd_country" value="' + country[0].id + '"/>',
                        '<condition attribute="statecode" operator="eq" value="0" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_region", "bsd_region", "bsd_name", "Region", fetchxml);
       Load_SubripSectionAccount();
    }
    else if (reset != false) {
        clear_region();
    }
}
function clear_region() {
    getControl("bsd_region").addPreSearch(presearch_region);
}
function presearch_region() {
    getControl("bsd_region").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

//Mr:Diệm
//Note: search area follow region
function LK_Region(reset) {
   LK_Area(reset);
    if (reset != false) setNull(["bsd_area", "bsd_province"]);
    getControl("bsd_area").removePreSearch(presearch_area);
    var region = getValue("bsd_region");
    if (region != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="bsd_areab2c">',
                        '    <attribute name="bsd_areab2cid" />',
                        '    <attribute name="bsd_name" />',
                        '    <attribute name="createdon" />',
                        '    <order attribute="bsd_name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="bsd_region" operator="eq" uitype="bsd_region" value="' + region[0].id + '" />',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_area", "bsd_areab2c", "bsd_name", "Area", fetchxml);
    }
    else if (reset != false) {
        clear_area();
    }
    setrequire();

}
function clear_area() {
    getControl("bsd_area").addPreSearch(presearch_area);
}
function presearch_area() {
    getControl("bsd_area").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

//Mr:Diệm
//Note: search province follow area
function LK_Area(reset) {
    LK_Province_change(reset);
    if (reset != false) setNull(["bsd_province"]);
    getControl("bsd_province").removePreSearch(presearch_province);
    var area = getValue("bsd_area");
    if (area != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="bsd_province">',
                        '    <attribute name="bsd_provinceid" />',
                        '    <attribute name="bsd_name" />',
                        '    <attribute name="createdon" />',
                        '    <order attribute="bsd_name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '      <condition attribute="bsd_area" operator="eq" uitype="bsd_areab2c" value="' + area[0].id + '" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_province", "bsd_province", "bsd_name", "province", fetchxml);
    }
    else if (reset != false) {
        clear_province();
    }
    setrequire();
}
function clear_province() {
    getControl("bsd_province").addPreSearch(presearch_province);
}
function presearch_province() {
    getControl("bsd_province").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
/*
Diệm
Note: change province.
*/
function LK_Province_change(reset) {
    if (reset != false) setNull("bsd_distributor");
    var province = getValue("bsd_province");
    if (province != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                        '  <entity name="account">',
                        '    <attribute name="name" />',
                        '    <attribute name="primarycontactid" />',
                        '    <attribute name="telephone1" />',
                        '    <attribute name="accountid" />',
                        '    <order attribute="name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '    </filter>',
                        '    <link-entity name="bsd_address" from="bsd_account" to="accountid" alias="ad">',
                        '      <filter type="and">',
                        '        <condition attribute="bsd_province" operator="eq" uitype="bsd_province" value="' + province[0].id + '" />',
                        '      </filter>',
                        '    </link-entity>',
                        '  </entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_distributor", "account", "name", "Account", fetchxml);
    }
    setrequire();
}
/*Diệm: Set require cho region, area, province.*/
function setrequire() {
    setRequired(["bsd_region", "bsd_area", "bsd_province"], "none");
    if (getValue("bsd_region") != null) {
        setRequired("bsd_region", "required");
        setRequired(["bsd_area", "bsd_province"], "none");
        if (getValue("bsd_area") != null) {
            setRequired("bsd_area", "required");
            setRequired("bsd_province", "none");
            if (getValue("bsd_province")) {
                setRequired("bsd_province", "required");
            }
        }
    }
}

/*Diệm: set country required/none */
function Account_Loadding() {
    setRequired("bsd_country", "required");
    if (getValue("bsd_distributor") != null) {
        if (getValue("bsd_country") == null) {
            setRequired("bsd_country", "none");
        } else {
            if (getValue("bsd_province") == null) {
                setNull(["bsd_country", "bsd_region", "bsd_area", "bsd_province"]);
                setRequired(["bsd_country", "bsd_region", "bsd_area", "bsd_province"], "none");
            }
        }
    }
    Load_SubripSectionAccount();
}

/*
Huy: 29/12/2016: update visible
*/

function Load_Quantity_from_to_Visible() {
    debugger;
    var typeprice = getValue("bsd_typeprice");
    if (typeprice != null) {
        if (typeprice == 861450001) {//discount
            setVisible(["bsd_fromquantity", "bsd_fromprice", "bsd_typemutiline"], false);
            setNull(["bsd_fromquantity", "bsd_fromprice", "bsd_typemutiline"]);
            setRequired(["bsd_fromquantity", "bsd_fromprice", "bsd_typemutiline"], "none");
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_3").setVisible(true); // detal promotion
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_6").setVisible(false);// detal promotion
        }
        else if (typeprice == 861450002) {//multi
            setVisible(["bsd_fromquantity", "bsd_typemutiline"], true);
            setRequired(["bsd_fromquantity", "bsd_typemutiline"], "required");
            setVisible(["bsd_fromprice"], false);
            setRequired(["bsd_fromprice"], "none");
            setNull(["bsd_fromprice"]);
            setValue("bsd_typemutiline", 861450000)
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_3").setVisible(false);// detal promotion
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_6").setVisible(true);// detal promotion
        }
        else {//total
            setVisible(["bsd_fromquantity", "bsd_typemutiline"], false);
            setRequired(["bsd_fromquantity", "bsd_typemutiline"], "none");
            setNull(["bsd_fromquantity"]);
            setVisible(["bsd_fromprice"], true);
            setRequired(["bsd_fromprice"], "required");
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_3").setVisible(false);// detal promotion
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_6").setVisible(false);// detal promotion
        }
    }
    else {
        setVisible(["bsd_fromquantity", "bsd_fromprice"], false);
        setRequired(["bsd_fromquantity", "bsd_fromprice"], "none");
        setNull(["bsd_fromquantity", "bsd_fromprice"]);
    }
    Load_Amount_Type();
    Load_SubripSectionAccount();
}

/*Diệm: load amount type*/
function Load_Amount_Type() {
    debugger;
    var amounttype = getValue("bsd_amounttype");
    var typeprice = getValue("bsd_typeprice");
    if (typeprice != 861450001) {
        if (amounttype != null) {
            if (amounttype == 861450000)/*percent*/ {
                setVisible(["bsd_percent", "bsd_applytax", "bsd_priceof"], true);
                setRequired(["bsd_percent"], "required");
                setVisible(["bsd_quantity", "bsd_amount"], false);
                setNull(["bsd_quantity", "bsd_amount"]);
                setRequired(["bsd_quantity", "bsd_amount"], "none");
                setValue("bsd_applytax", false);
                setValue("bsd_priceof", false);
            } else if (amounttype == 861450001)/*amount*/ {
                setVisible("bsd_amount", true);
                setRequired(["bsd_amount"], "required");
                setVisible(["bsd_quantity", "bsd_percent", "bsd_applytax", "bsd_priceof"], false);
                setNull(["bsd_quantity", "bsd_percent", "bsd_applytax", "bsd_priceof"]);
                setRequired(["bsd_quantity", "bsd_percent", "bsd_applytax", "bsd_priceof"], "none");
            } else {
                setVisible(["bsd_quantity", "bsd_applytax", "bsd_priceof"], true);
                setRequired(["bsd_quantity"], "required");
                setVisible(["bsd_amount", "bsd_percent"], false);
                setNull(["bsd_amount", "bsd_percent"]);
                setRequired(["bsd_amount", "bsd_percent"], "none");
                setValue("bsd_applytax", false);
                setValue("bsd_priceof", false);
            }
        }
    } else {
        setVisible(["bsd_amount", "bsd_percent", "bsd_quantity"], false);
        setNull(["bsd_amount", "bsd_percent", "bsd_quantity"]);
        setRequired(["bsd_amount", "bsd_percent", "bsd_quantity"], "none");
    }
}


/*Diệm: check ẩn hiện subrip distributor*/
function Load_SubripSectionAccount() {
    if (getValue("bsd_country") == null) {
        if (getValue("bsd_distributor") != null) {
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_5").setVisible(false);
        } else {
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_5").setVisible(true);
        }
    } else {
        if (getValue("bsd_distributor") != null) {
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_5").setVisible(false);
        } else {
            Xrm.Page.ui.tabs.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}").sections.get("{658d5f92-bf9f-45db-9e7e-fd51677fdefb}_section_5").setVisible(true);
        }
    }
}