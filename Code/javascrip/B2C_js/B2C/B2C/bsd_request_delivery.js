function init() {

    deliverytrucktype_change(false);
    carrierpartner_change(false);
    var check_createdeliverybill = getValue("bsd_createddeliverybill");
    //if (check_createdeliverybill) DisabledForm();
    if (formType() == 1) {
        Load_FromDate_Auto();
    }
}

function BtnSplit() {
    var check_createdeliverybill = getValue("bsd_createddeliverybill");
    if (check_createdeliverybill) {
        Xrm.Page.ui.setFormNotification("Đã tạo phiếu xuất kho. ", "WARNING", "1");
    } else {
        Xrm.Page.ui.clearFormNotification('1');
        var grid = window.parent.document.getElementById("subgridProduct").control.get_selectedRecords();
        if (grid.length > 0) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_requestdeliveryproduct">',
                '<attribute name="bsd_requestdeliveryproductid" />',
                '<attribute name="bsd_quantity" />',
                '<attribute name="bsd_product" />',
                '<attribute name="bsd_name" />',
                '<filter type="and">',
                  '<condition attribute="bsd_requestdeliveryproductid" operator="eq" uitype="bsd_requestdeliveryproduct" value="' + grid[0].Id + '" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                console.log(rs[0]);
                var parameters = {};
                parameters["bsd_name"] = "Split Request Delivery";
                parameters["bsd_requestdelivery"] = getId();
                parameters["bsd_requestdeliveryname"] = getValue("bsd_name");

                parameters["bsd_requestdeliveryproduct"] = rs[0].Id;
                parameters["bsd_requestdeliveryproductname"] = rs[0].getValue("bsd_name");

                parameters["bsd_product"] = rs[0].getValue("bsd_product");
                parameters["bsd_productname"] = rs[0].attributes.bsd_name.value;
                parameters["bsd_totalquantity"] = rs[0].attributes.bsd_quantity.value;
                Xrm.Utility.openEntityForm("bsd_splitrequestdeliveryproduct", null, parameters);
            });
        } else {
            alert("Chọn sản phẩm để tách !");
        }
    }
}
function BtnSplitEnableRule() {
    var bsd_createddeliverybill = getValue("bsd_createddeliverybill");
    if (bsd_createddeliverybill == true) {
        // Đã tạo phiếu xuất kho thì không tách.
        return false;
    } else {
        return true;
    }
}

function BtnDeleteDeliveryBill() {
    var bsd_createddeliverybill = getValue("bsd_createddeliverybill");
    var bsd_createddeliverynote = getValue("bsd_createddeliverynote");
    if (bsd_createddeliverybill == false) {
        alert("Chưa tạo Phiếu Xuất Kho");
    } else if (bsd_createddeliverynote == true) {
        alert("Đã tạo Phiếu Giao Hàng, không thể xóa Phiếu Xuất Kho");
    } else {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
         '<entity name="bsd_requestdelivery">',
           '<attribute name="bsd_requestdeliveryid" />',
            '<attribute name="bsd_createddeliverynote" />',
            '<attribute name="bsd_createddeliverybill" />',
            '<filter type="and">',
              '<condition attribute="bsd_requestdeliveryid" operator="eq" uitype="bsd_requestdelivery" value="' + getId() + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].getValue("bsd_createddeliverybill") == true) {
                    if (rs[0].getValue("bsd_createddeliverynote") == false) {
                        if (confirm("Are you sure to delete delivery bill?")) {
                            ExecuteAction(getId(), getEntityName(), "bsd_Action_DeleteDeliveryBill", null, function (result) {
                                if (result != null && result.status != null) {
                                    if (result.status == "success") {
                                        Xrm.Utility.openEntityForm(getEntityName(), getId(), null);
                                    } else if (result.status == "error") {
                                        alert(result.data);
                                    } else {
                                        alert(result.data);
                                    }
                                }
                            });
                        }
                    } else {
                        alert("Đã tạo Phiếu Giao Hàng !");
                    }
                } else {
                    alert("Chưa tạo Phiếu Xuất Kho !");
                }
            }
        });
    }
}
function BtnDeleteDelieryBillEnableRule() {
    var bsd_createddeliverybill = getValue("bsd_createddeliverybill");
    var bsd_createddeliverynote = getValue("bsd_createddeliverynote");
    // và bsd_createddeliverynote = false;
    if (bsd_createddeliverybill == true) {
        // Đã tạo phiếu xuất kho nhưng chưa tạo phiếu giao hàng thì hiện nút xóa.
        return true;
    } else {
        return false;
    }
}

function BtnCreateDeliveryBill() {
    alert("ok");
}
function BtnCreateDeliveryBillEnableRule() {
    var bsd_createddeliverybill = getValue("bsd_createddeliverybill");
    var bsd_warehousestatus = getValue("bsd_warehousestatus");
    if (bsd_createddeliverybill == false && bsd_warehousestatus == true) {
        // Chưa tạo phiếu xuất kho và phải đủ số lượng mới hiển thị nút.
        //return true;
    } else {
        //return false;
    }
    return false;
}

function BtnDeliveryNote() {
    Xrm.Page.data.save().then(function () {
        var bsd_createddeliverybill = getValue("bsd_createddeliverybill");
        var bsd_warehousestatus = getValue("bsd_warehousestatus");
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_deliverynote">',
            '<attribute name="bsd_deliverynoteid" />',
            '<attribute name="bsd_name" />',
            '<order attribute="bsd_name" descending="false" />',
            '<filter type="and">',
              '<condition attribute="bsd_requestdelivery" operator="eq" uitype="bsd_requestdelivery" value="' + getId() + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Utility.openEntityForm("bsd_deliverynote", rs[0].Id);
            } else if (bsd_createddeliverybill == false) {
                setFormNotification("Yêu cầu giao hàng này chưa tạo Phiếu xuất kho !", "INFO", "1");
            } else if (bsd_warehousestatus == false) {
                setFormNotification("Yêu cầu giao hàng này chưa đủ số lượng !", "INFO", "1");
            } else {
                clearFormNotification("1");
                if (confirm("Bạn có muốn tạo Phiếu Giao Hàng không ?")) {
                    ExecuteAction(getId(), getEntityName(), "bsd_Action_Create_DeliveryNote", null, function (result) {
                        if (result != null && result.status != null) {
                            if (result.status == "success") {
                                Xrm.Utility.openEntityForm("bsd_deliverynote", result.data.ReturnId.value);
                            } else if (result.status == "error") {
                                alert(result.data);
                            } else {
                                alert(result.data);
                            }
                        }
                    });
                }
            }
        });
    });
}
function BtnDeliveryNoteEnableRule() {
    var bsd_createddeliverybill = getValue("bsd_createddeliverybill");
    if (bsd_createddeliverybill == true) {
        // Đã tạo phiếu xuất kho thì hiện nút tạo phiếu giao hàng.
        //return true;
    } else {
        //return false;
    }
    return true;
}

function deliverytrucktype_change(reset) {
    debugger;
    if (reset != false) setNull(["bsd_deliverytruck", "bsd_driver", "bsd_carrierpartner", "bsd_deliverytrucktext"]);
    var type = getValue("bsd_deliverytrucktype");

    if (type == 861450000) { // bhs
        setVisible(["bsd_deliverytruck", "bsd_driver"], true);
        setVisible(["bsd_carrierpartner", "bsd_deliverytrucktext"], false);
        setRequired(["bsd_deliverytruck", "bsd_driver"], "required");
        setRequired(["bsd_carrierpartner", "bsd_deliverytrucktext"], "none");

        setNull(["bsd_carrierpartner", "bsd_deliverytrucktext"]);
        load_deliverytruck_bhs(reset);
    } else if (type == 861450001) { // khach hang
        setVisible(["bsd_deliverytrucktext"], true);
        setVisible(["bsd_carrierpartner", "bsd_deliverytruck", "bsd_driver"], false);

        setRequired(["bsd_deliverytrucktext"], "required");
        setRequired(["bsd_carrierpartner", "bsd_deliverytruck", "bsd_driver"], "none");

        setNull(["bsd_carrierpartner", "bsd_deliverytruck", "bsd_driver"]);
    } else if (type == 861450002) {
        setVisible(["bsd_carrierpartner", "bsd_deliverytruck"], true);
        setVisible(["bsd_deliverytrucktext", "bsd_driver"], false);

        setRequired(["bsd_carrierpartner", "bsd_deliverytruck"], "required");
        setRequired(["bsd_deliverytrucktext", "bsd_driver"], "none");

        setNull(["bsd_deliverytrucktext", "bsd_driver"]);
        carrierpartner_change(reset);
    }
}

function carrierpartner_change(reset) {
    if (reset != false) setNull("bsd_deliverytruck");
    getControl("bsd_deliverytruck").removePreSearch(presearch_deliverytruck);
    deliverytruck_change(reset);
    var bsd_carrierpartner = getValue("bsd_carrierpartner");
    if (bsd_carrierpartner != null) {
        var xml = xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
             '<entity name="bsd_deliverytruck">',
               '<attribute name="bsd_deliverytruckid" />',
               '<attribute name="bsd_name" />',
               '<attribute name="createdon" />',
               '<order attribute="bsd_name" descending="false" />',
               '<filter type="and">',
                   '<condition attribute="bsd_shipper" operator="eq" uitype="account" value="' + bsd_carrierpartner[0].id + '" />',
                    '<condition attribute="bsd_companytype" operator="eq" value="861450002" />',
               '</filter>',
             '</entity>',
            '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_deliverytruckid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                       "<row name='result'  " + "id='bsd_deliverytruckid'>  " +
                                       "<cell name='bsd_name'   " + "width='200' />  " +
                                       "</row>" +
                                    "</grid>";
        getControl("bsd_deliverytruck").addCustomView(getDefaultView("bsd_deliverytruck"), "bsd_deliverytruck", "bsd_deliverytruck", xml, layoutXml, true);
    } else if (reset != false) {
        clear_deliverytruck();
    }
}

function deliverytruck_change(reset) {
    if (reset != false) setNull("bsd_driver");
    getControl("bsd_driver").removePreSearch(presearch_driver);
    var deliverytruck = getValue("bsd_deliverytruck");
    if (deliverytruck != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_driver">',
            '<attribute name="bsd_driverid" />',
            '<attribute name="bsd_name" />',
            '<attribute name="createdon" />',
            '<order attribute="bsd_name" descending="false" />',
            '<filter type="and">',
              '<condition attribute="bsd_deliverytruck" operator="eq" uitype="bsd_deliverytruck" value="' + deliverytruck[0].id + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_driverid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                       "<row name='result'  " + "id='bsd_driverid'>  " +
                                       "<cell name='bsd_name'   " + "width='200' />  " +
                                       "</row>" +
                                    "</grid>";
        getControl("bsd_driver").addCustomView(getDefaultView("bsd_driver"), "bsd_driver", "bsd_driver", xml, layoutXml, true);
    } else if (reset != false) {
        clear_driver();
    }
}

function load_deliverytruck_bhs(reset) {
    if (reset != false) setNull("bsd_deliverytruck");
    getControl("bsd_deliverytruck").removePreSearch(presearch_deliverytruck);
    deliverytruck_change(reset);
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
     '<entity name="bsd_deliverytruck">',
       '<attribute name="bsd_deliverytruckid" />',
       '<attribute name="bsd_name" />',
       '<attribute name="createdon" />',
       '<order attribute="bsd_name" descending="false" />',
       '<filter type="and">',
         '<condition attribute="bsd_companytype" operator="eq" value="861450000" />',
       '</filter>',
     '</entity>',
    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_deliverytruckid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_deliverytruckid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "</row>" +
                                "</grid>";
    getControl("bsd_deliverytruck").addCustomView(getDefaultView("bsd_deliverytruck"), "bsd_deliverytruck", "bsd_deliverytruck", xml, layoutXml, true);
}

function clear_deliverytruck() {
    getControl("bsd_deliverytruck").addPreSearch(presearch_deliverytruck);
}

function presearch_deliverytruck() {
    getControl("bsd_deliverytruck").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_driver() {
    getControl("bsd_driver").addPreSearch(presearch_driver);
}

function presearch_driver() {
    getControl("bsd_driver").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function modify_on() {
    deliverytrucktype_change(false);
}

function preventAutoSave(econtext) {
    deliverytrucktype_change(false);
}
//Author:Mr.Đăng
//Description:set Date
function Load_FromDate_Auto() {
    var date = new Date();
    setValue("bsd_date", date);
}