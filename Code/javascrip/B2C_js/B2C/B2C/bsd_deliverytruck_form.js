//author:Mr.Phong
//description:filter Shipper/Account theo company type
function set_transportation() {
    var companytype = Xrm.Page.getAttribute("bsd_companytype").getValue();
    if (companytype != null) {

        if (companytype == 861450002) {
            Xrm.Page.getAttribute("bsd_shipper").setValue(null);
            var entityName = "account";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='account'>");
            xml.push("<attribute name='accountid' />");
            xml.push("<attribute name='name'/>");
            xml.push("<order attribute='name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_accounttype' operator='eq' value='861450002' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='accountid'>  " +
                               "<cell name='name'    " + "width='200' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_shipper").addCustomView(getDefaultView("bsd_shipper"), entityName, viewDisplayName, xml.join(""), layoutXml, true);
            Xrm.Page.ui.controls.get("bsd_shipper").setVisible(true);
        }
        if (companytype == 861450001) {
            Xrm.Page.getAttribute("bsd_shipper").setValue(null);
            var entityName = "account";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='account'>");
            xml.push("<attribute name='accountid' />");
            xml.push("<attribute name='name'/>");
            xml.push("<order attribute='name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_accounttype' operator='eq' value='861450000' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='accountid'>  " +
                               "<cell name='name'    " + "width='200' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_shipper").addCustomView(getDefaultView("bsd_shipper"), entityName, viewDisplayName, xml.join(""), layoutXml, true);
            Xrm.Page.ui.controls.get("bsd_shipper").setVisible(true);
        }
        if (companytype == 861450000 || companytype == null) {
            Xrm.Page.ui.controls.get("bsd_shipper").setVisible(false);
            Xrm.Page.getAttribute("bsd_shipper").setValue(null);
        }
    }
    if (companytype == null) {
        Xrm.Page.getAttribute("bsd_shipper").setValue(null);
    }
}
//author:Mr.Phong
//description:filter Shipper/Account theo company type
function set_transportationonload() {
    var companytype = Xrm.Page.getAttribute("bsd_companytype").getValue();
    if (companytype != null) {
        if (companytype == 861450002) {           
            var entityName = "account";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='account'>");
            xml.push("<attribute name='accountid' />");
            xml.push("<attribute name='name'/>");
            xml.push("<order attribute='name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_accounttype' operator='eq' value='861450001' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='accountid'>  " +
                               "<cell name='name'    " + "width='200' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_shipper").addCustomView(getDefaultView("bsd_shipper"), entityName, viewDisplayName, xml.join(""), layoutXml, true);
            Xrm.Page.ui.controls.get("bsd_shipper").setVisible(true);
        }
        if (companytype == 861450001) {           
            var entityName = "account";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='account'>");
            xml.push("<attribute name='accountid' />");
            xml.push("<attribute name='name'/>");
            xml.push("<order attribute='name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_accounttype' operator='eq' value='861450000' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='accountid'>  " +
                               "<cell name='name'    " + "width='200' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_shipper").addCustomView(getDefaultView("bsd_shipper"), entityName, viewDisplayName, xml.join(""), layoutXml, true);
            Xrm.Page.ui.controls.get("bsd_shipper").setVisible(true);
        }
        if (companytype == 861450000 || companytype==null)
        {
            Xrm.Page.ui.controls.get("bsd_shipper").setVisible(false);
            Xrm.Page.getAttribute("bsd_shipper").setValue(null);
        }
    }
    if (companytype == null) {
        Xrm.Page.getAttribute("bsd_shipper").setValue(null);
    }
}
//Author:Mr.Phong
//Description:check same name
function checksamename()
{
    var name = Xrm.Page.getAttribute("bsd_name").getValue();
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='bsd_deliverytruck'>");
    xml.push("<attribute name='bsd_deliverytruckid' />");
    xml.push("<attribute name='bsd_name'/>");
    xml.push("<attribute name='createdon'/>");
    xml.push("<order attribute='bsd_name' descending='false' />");
    xml.push("<filter type='and' >");
    xml.push("<condition attribute='bsd_name' operator='eq' value='" + name + "' />");
    xml.push("<condition attribute='statecode' operator='eq' value='0' />");
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");

    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
        if (rs.length > 0)
        {
            Xrm.Page.getAttribute("bsd_name").setValue(null);
            Xrm.Page.getControl("bsd_name").setNotification("Name is exist!!!");
        }
        if (rs.length == 0)
        {
            Xrm.Page.getControl("bsd_name").clearNotification();
        }
    }, function (er) {
        console.log(er.message)
    });

    //var rs = CrmFetchKit.FetchSync(xml.join(""));
    //if (rs.length > 0) {
    //    Xrm.Page.getAttribute("bsd_name").setValue(null);
    //    Xrm.Page.getControl("bsd_name").setNotification("Name is exist!!!");
    //} else {
    //    Xrm.Page.getControl("bsd_name").clearNotification();
    //}
}