//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    LK_CompanyName();
    set_DisabledSite();
    if (formType() == 2) {
    } else {
        clear_addresscompanyname();
    }
}
//Author:Mr.Đăng
//Description: Load Company name
function LK_CompanyName() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="accountid" />',
                    '<order attribute="name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="861450005" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    LookUp_After_Load("bsd_companyname", "account", "name", "CompanyName", fetchxml);
    LK_AddressCompanyName(false);
}
//Author:Mr.Đăng
//Description: Address from CompanyName
function LK_AddressCompanyName(reset) {
    var companyname = getValue("bsd_companyname");
    getControl("bsd_address").removePreSearch(presearch_addresscompanyname);
    if (reset != false) setNull("bsd_address");
    if (companyname != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_purpose" />',
                        '<attribute name="bsd_account" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + companyname[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_address", "bsd_address", "bsd_name", "Address", fetchxml);
    }
    else if (reset != false) {
        clear_addresscompanyname();
    }
}
function clear_addresscompanyname() {
    getControl("bsd_address").addPreSearch(presearch_addresscompanyname);
}
function presearch_addresscompanyname() {
    getControl("bsd_address").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}
//Author:Mr.Phong
//Description:count text length
function counttextlength() {
    var mikExp = /[~`!@#$%\^&*+=[\]\\';,./()_{}|\\":<>\?]/;
    var warehouseid = getValue("bsd_warehouseid");
    var site = getValue("bsd_site");
    var id = getId();
    if (warehouseid != null) {
        if (mikExp.test(warehouseid)) {
            Xrm.Page.getControl("bsd_warehouseid").setNotification("Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else if (!mikExp.test(warehouseid)) {
            setValue("bsd_warehouseid", warehouseid.toUpperCase());
            if (site != null) {
                var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                '<entity name="bsd_warehouseentity">',
                                '<attribute name="bsd_warehouseentityid" />',
                                '<attribute name="bsd_name" />',
                                 '<attribute name="createdon" />',
                                '<order attribute="bsd_name" descending="false" />',
                                '<filter type="and">',
                                '<condition attribute="bsd_warehouseid" operator="eq" value="' + warehouseid + '" />',
                                '<condition attribute="bsd_site" operator="eq" uitype="bsd_site" value="' + site[0].id + '" />',
                                '<condition attribute="bsd_warehouseentityid" operator="ne" value="' + id + '" />',
                                '</filter>',
                                '</entity>',
                                '</fetch>'].join('');
                var rs = CrmFetchKit.FetchSync(fetchxml);
                if (rs.length > 0) {
                    setNotification("bsd_warehouseid", "Mã số nhà kho đã tồn tại, vui lòng kiểm tra lại.");
                }
                else {
                    clearNotification("bsd_warehouseid");
                }
            }
        }
    }
}
//Mr.Phong
//Description:Kiểm tra trùng tên warehouse trong 1 site
function check_samewarehousename() {
    var site = getValue("bsd_site");
    var warehouse = getValue("bsd_name");
    var xmlwarehouse = [];
    var id = Xrm.Page.data.entity.getId();
    if (id != " ") {
        if (site != null && warehouse != null) {
            fetch(xmlwarehouse, "bsd_warehouseentity", ["bsd_warehouseentityid", "bsd_name", "createdon"], ["createdon"], false, null,
                                   ["bsd_site", "bsd_name", "bsd_warehouseentityid", "statecode"]
                                   , ["eq", "eq", "ne", "eq"], [0, site[0].id, 1, warehouse, 2, id, 3, "0"]);
            CrmFetchKit.Fetch(xmlwarehouse.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    alert("Kho đã được tạo trong site này rồi");
                    setNull("bsd_name");
                }
            });
        }
    }
    else {
        if (site != null && warehouse != null) {
            fetch(xmlwarehouse, "bsd_warehouseentity", ["bsd_warehouseentityid", "bsd_name", "createdon"], ["createdon"], false, null,
                                   ["bsd_site", "bsd_name", "statecode"]
                                   , ["eq", "eq", "eq"], [0, site[0].id, 1, warehouse, 2, "0"]);
            CrmFetchKit.Fetch(xmlwarehouse.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    alert("Kho đã được tạo trong site này rồi");
                    setNull("bsd_name");
                }
            });
        }
    }
}
// 28.03.2017 Đăng NH: Disable field Type khi đã tạo mới
function set_DisabledSite() {
    var site = getValue("bsd_site");
    if (site != null) setDisabled("bsd_site", true);
}