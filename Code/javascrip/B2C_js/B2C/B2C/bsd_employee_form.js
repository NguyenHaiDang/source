/*
    Diệm hàm chung cho finish date.
*/
function finishdate() {
    CheckDateFromTo();
    check_finishdate();
}
/*
    Diệm 
    starting date <= finish date.
*/
function CheckDateFromTo() {
    if (getValue("bsd_startingdate") != null && getValue("bsd_finishdate") != null) {
        var todate = getValue("bsd_finishdate");
        var fromdate = getValue("bsd_startingdate");

        if (todate.getFullYear() > fromdate.getFullYear()) {
            clearNotification("bsd_startingdate");
        }
        else if (todate.getFullYear() == fromdate.getFullYear()) {
            if ((todate.getMonth() + 1) > (fromdate.getMonth() + 1)) {
                clearNotification("bsd_startingdate");
            }
            else if ((todate.getMonth() + 1) == (fromdate.getMonth() + 1)) {
                if (todate.getDate() >= fromdate.getDate()) {
                    clearNotification("bsd_startingdate");
                } else {
                    setNotification("bsd_startingdate", "Vui lòng nhập Starting date nhỏ hơn Finish date");
                }
            }
            else {
                setNotification("bsd_startingdate", "Vui lòng nhập Starting date nhỏ hơn Finish date");
            }
        } else {
            setNotification("bsd_startingdate", "Vui lòng nhập Starting date nhỏ hơn Finish date");
        }
    }
}
/*
    Diệm
    26/2/2016: Check status nhan vien
*/

function Check_status() {
    var status = getValue("bsd_status");
    if (!status) {
        setValue("bsd_finishdate", new Date());
    }
}
/*
    Diệm
    26/2/2016: Check finish date nhan vien
*/
function check_finishdate() {
    if (getValue("bsd_startingdate") != null && new Date() != null) {
        var todate = getValue("bsd_finishdate");
        var fromdate = new Date();

        if (todate.getFullYear() > fromdate.getFullYear()) {
            setValue("bsd_status", true);
        }
        else if (todate.getFullYear() == fromdate.getFullYear()) {
            if ((todate.getMonth() + 1) > (fromdate.getMonth() + 1)) {
                setValue("bsd_status", true);
            }
            else if ((todate.getMonth() + 1) == (fromdate.getMonth() + 1)) {
                if (todate.getDate() >= fromdate.getDate()) {
                    setValue("bsd_status", true);
                } else {
                    setValue("bsd_status", false);
                }
            }
            else {
                setValue("bsd_status", false);
            }
        } else {
            setValue("bsd_status", false);
        }
    }
}


//Author:Mr.Phong
//Description:library for this page
function set_value(a, b) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getAttribute(a[i]).setValue(b);
    }
}
function get_value(a) {
    var b = [];
    for (var i = 0; i < a.length; i++) {
        var j = Xrm.Page.getAttribute(a[i]).getValue();
        b.push(j);
    }
    return b;
}
function set_requirelevel(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.getAttribute(a[i]).setRequiredLevel("none");
        }
        if (b == 1) {
            Xrm.Page.getAttribute(a[i]).setRequiredLevel("required");
        }
    }
}
function set_disable(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.getControl(a[i]).setDisabled(false);
        }
        if (b == 1) {
            Xrm.Page.getControl(a[i]).setDisabled(true);
        }
    }
}
function set_visible(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.ui.controls.get(a[i]).setVisible(false);
        }
        if (b == 1) {
            Xrm.Page.ui.controls.get(a[i]).setVisible(true);
        }
    }
}
function set_notification(a, b) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getControl(a[i]).setNotification("" + b);
    }
}
function clear_nofitication(a) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getControl(a[i]).clearNotification();
    }
}
function fetch(xml, entity, attribute, orderattribute, descendingvalue, ascendingvalue, conditionattribute, operator, conditionvalue) {
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='" + entity + "'>");
    for (var i = 0; i < attribute.length; i++) {
        xml.push("<attribute name='" + attribute[i] + "' />");
    }
    if (descendingvalue != null) {
        xml.push("<order attribute='" + orderattribute + "' descending='" + descendingvalue + "' />");
    }
    if (ascendingvalue != null) {
        xml.push("<order attribute='" + orderattribute + "' descending='" + ascendingvalue + "' />");
    }
    xml.push("<filter type='and'>");
    for (var j = 0; j < conditionattribute.length; j++) {
        if (operator[j] == "inn") {
            xml.push("<condition attribute='" + conditionattribute[j] + "' operator='in'>");
            for (var k = 0; k < conditionvalue.length; k++) {
                if (conditionvalue[k] === parseInt(conditionvalue[k], 10)) {
                    if (j == conditionvalue[k]) {
                        for (var p = 1; p < conditionvalue.length; p++) {
                            if (conditionvalue[k + p] === parseInt(conditionvalue[k + p], 10) || conditionvalue[k + p] == null) {
                                xml.push("</condition>");
                                break;
                            }
                            else {
                                xml.push("<value>" + conditionvalue[k + p] + "");
                                xml.push("</value>");
                            }
                        }
                    }
                }
            }
        }
        else {
            for (var k = 0; k < conditionvalue.length; k++) {
                if (conditionvalue[k] === parseInt(conditionvalue[k], 10)) {
                    if (j == conditionvalue[k]) {
                        for (var p = 1; p < conditionvalue.length; p++) {
                            if (conditionvalue[k + p] === parseInt(conditionvalue[k + p], 10) || conditionvalue[k + p] == null) {
                                break;
                            }
                            else {
                                xml.push("<condition attribute='" + conditionattribute[j] + "' operator='" + operator[j] + "'  value='" + conditionvalue[k + p] + "' />");
                            }
                        }
                    }
                }
            }
        }
    }
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
}
//Author:Mr.Phong
//Description:check_position
function check_position() {
    var position = Xrm.Page.getAttribute("bsd_salegroup").getValue();
    if (position == "861450000") {
        Xrm.Page.ui.controls.get("bsd_account").setVisible(true);
        Xrm.Page.ui.controls.get("subgrid_account").setVisible(false);
        var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
        var entityName = "account";
        var viewDisplayName = "test";
        var xml = [];
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='account'>");
        xml.push("<attribute name='accountid' />");
        xml.push("<attribute name='name'/>");
        xml.push("<order attribute='name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_accounttype' operator='in'>");
        xml.push("<value>");
        xml.push("100000000");
        xml.push("</value>");
        xml.push("</condition>");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='accountid'>  " +
                           "<cell name='name'    " + "width='200' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getAttribute("bsd_account").setValue(null);
        Xrm.Page.getControl("bsd_account").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    }
    if (position == null) {
        Xrm.Page.ui.controls.get("bsd_account").setVisible(false);
        Xrm.Page.ui.controls.get("subgrid_account").setVisible(false);
        Xrm.Page.getAttribute("bsd_account").setValue(null);
    }
    if (position == "861450001") {
        Xrm.Page.ui.controls.get("bsd_account").setVisible(false);
        Xrm.Page.ui.controls.get("subgrid_account").setVisible(true);
        Xrm.Page.getAttribute("bsd_account").setValue(null);
    }
}
//Author:Mr.Phong
//Description:check_position
function check_positiononload() {
    var position = Xrm.Page.getAttribute("bsd_salegroup").getValue();
    if (position == "861450000") {
        Xrm.Page.ui.controls.get("bsd_account").setVisible(true);
        Xrm.Page.ui.controls.get("subgrid_account").setVisible(false);
        var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
        var entityName = "account";
        var viewDisplayName = "test";
        var xml = [];
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='account'>");
        xml.push("<attribute name='accountid' />");
        xml.push("<attribute name='name'/>");
        xml.push("<order attribute='name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_accounttype' operator='in'>");
        xml.push("<value>");
        xml.push("100000000");
        xml.push("</value>");
        xml.push("</condition>");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='accountid'>  " +
                           "<cell name='name'    " + "width='200' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_account").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    }
    if (position == null) {
        Xrm.Page.ui.controls.get("bsd_account").setVisible(false);
        Xrm.Page.ui.controls.get("subgrid_account").setVisible(false);
        Xrm.Page.getAttribute("bsd_account").setValue(null);
    }
    if (position == "861450001") {
        Xrm.Page.ui.controls.get("bsd_account").setVisible(false);
        Xrm.Page.ui.controls.get("subgrid_account").setVisible(true);
        Xrm.Page.getAttribute("bsd_account").setValue(null);
    }
}
//Author:Mr.Phong
//Description:set value lookup manager
function setvaluelookupmanager() {// 29.03.2017 Đăng NH: Xóa field Position: bsd_salegroup
    /*
    var position = Xrm.Page.getAttribute("bsd_salegroup").getValue();
    //sale rep
    if (position == "861450000") {
        set_visible(["bsd_parentaccount"], 1);
        set_visible(["bsd_manager"], 0);
        set_value(["bsd_manager"], null);
    }
    //sale sup
    if (position == "861450001") {
        set_visible(["bsd_parentaccount"], 0);
        set_visible(["bsd_manager"], 1);
        set_value(["bsd_parentaccount"], null);
    }
    if (position == null) {
        set_visible(["bsd_manager", "bsd_parentaccount"], 0);
        set_value(["bsd_manager", "bsd_parentaccount"], null);
    }
    */
}
//Author:Mr.Phong
//Description:set value lookup manager
function setvaluelookupmanageronload() { // 29.03.2017 Đăng NH: Xóa field Position: bsd_salegroup
    /*
    var position = Xrm.Page.getAttribute("bsd_salegroup").getValue();
    //sale rep
    if (position == "861450000") {
        set_visible(["bsd_parentaccount"], 1);
        set_visible(["bsd_manager"], 0);
    }
    //sale sup
    if (position == "861450001") {
        set_visible(["bsd_parentaccount"], 0);
        set_visible(["bsd_manager"], 1);
    }
    if (position == null) {
        set_visible(["bsd_manager", "bsd_parentaccount"], 0);
    }
    */
}

//Author:Mr.Phong
//Description:set not same name
function setnotsamename() {
    var name = Xrm.Page.getAttribute("bsd_name").getValue();
    var id = Xrm.Page.data.entity.getId();
    var xml = [];
    if (name != null && id!="") {
        fetch(xml, "bsd_employee", ["bsd_name", "createdon", "bsd_account", "bsd_employeeid", "bsd_employeename"], "bsd_name", false
            , null, ["bsd_name", "bsd_employeeid"], ["eq", "ne"], [0, name, 1, id]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0)
            {             
                Xrm.Page.getControl("bsd_name").setNotification("This name is exist!!!");
                Xrm.Page.getAttribute("bsd_name").setValue(null);
            }
            if (rs.length==0) {
                Xrm.Page.getControl("bsd_name").clearNotification();
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    if (name != null && id == "")
    {
        fetch(xml, "bsd_employee", ["bsd_name", "createdon", "bsd_account", "bsd_employeeid", "bsd_employeename"], "bsd_name", false
           , null, ["bsd_name"], ["eq"], [0, name, 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getControl("bsd_name").setNotification("This name is exist!!!");
                Xrm.Page.getAttribute("bsd_name").setValue(null);
            }
            if (rs.length == 0) {
                Xrm.Page.getControl("bsd_name").clearNotification();
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.Phong
//Description:set not same indentify
function setnotsameidentify() {
    var indentify = Xrm.Page.getAttribute("bsd_indentifyy").getValue();
    var id = Xrm.Page.data.entity.getId();
    var xml = [];
    if (indentify != null && id!="") {
        fetch(xml, "bsd_employee", ["bsd_name", "createdon", "bsd_account", "bsd_employeeid", "bsd_employeename", "bsd_indentifyy"], "bsd_name", false
            , null, ["bsd_indentifyy","bsd_employeeid"], ["eq","ne"], [0, indentify, 1,id]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0)
            {
                if (indentify != rs[0].attributes.bsd_indentifyy.value) {
                    Xrm.Page.getControl("bsd_indentifyy").setNotification("This identify is exist!!!");
                    Xrm.Page.getAttribute("bsd_indentifyy").setValue(null);
                }
            }
            if (rs.length == 0) {
                Xrm.Page.getControl("bsd_indentifyy").clearNotification();
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    if (indentify != null && id == "")
    {
        fetch(xml, "bsd_employee", ["bsd_name", "createdon", "bsd_account", "bsd_employeeid", "bsd_employeename", "bsd_indentifyy"], "bsd_name", false
          , null, ["bsd_indentifyy"], ["eq"], [0, indentify, 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0)
            {
                    Xrm.Page.getControl("bsd_indentifyy").setNotification("This identify is exist!!!");
                    Xrm.Page.getAttribute("bsd_indentifyy").setValue(null);             
            }
            if (rs.length == 0) {
                Xrm.Page.getControl("bsd_indentifyy").clearNotification();
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}