//Description:load form
function init() {
    appearordisappearsectionBHSonload();
    filter_customerprincipalcontract();
    Load_FromDate_Auto();
    set_DisableDeliveryPort();
    set_customer();
    setshiptoaddressonload();
    set_valuecreatefrom();
    set_addressonload();
    if (formType() == 1) {
        check_enable_shipping();
        load_unitdefault(true);
        load_currencydefault(true);
        setValue("bsd_createfrom", 861450003);
        LK_PotentialCustomer_Change(true);
        load_unitdefault(true);

    } else {
        check_enable_shipping(false);
        porteroption_change(false);
        load_unitdefault(false);
        load_currencydefault(false);
        LK_PotentialCustomer_Change(false);
        check_from_quote();
        check_createdsuborder();
    }
}

function ExecuteAction(entityId, entityName, requestName, inputArg, callback, isGlobal) {
    debugger;
    // Creating the request XML for calling the Action
    function GetClientUrl() {
        if (typeof Xrm.Page.context == "object") {
            clientUrl = Xrm.Page.context.getClientUrl();
        }
        var ServicePath = "/XRMServices/2011/Organization.svc/web";
        return clientUrl + ServicePath;
    }
    var requestXML = [];
    requestXML.push("<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">");
    requestXML.push("<s:Body>");
    requestXML.push("<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">");
    requestXML.push("<request xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">");
    requestXML.push("<a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">");
    if (inputArg != null && inputArg.length > 0) {
        for (var i = 0; i < inputArg.length; i++) {
            var tmp = inputArg[i];
            //var tmp = { name: '',type:'',value:'' };
            requestXML.push("<a:KeyValuePairOfstringanyType>");
            requestXML.push("<b:key>" + tmp.name + "</b:key>");
            requestXML.push("<b:value i:type=\"c:" + tmp.type + "\" xmlns:c=\"http://www.w3.org/2001/XMLSchema\">" + tmp.value + "</b:value>");
            requestXML.push("</a:KeyValuePairOfstringanyType>");
        }
    }
    if (isGlobal != true) {
        requestXML.push("<a:KeyValuePairOfstringanyType>");
        requestXML.push("<b:key>Target</b:key>");
        requestXML.push("<b:value i:type=\"a:EntityReference\">");
        requestXML.push("<a:Id>" + entityId + "</a:Id>");
        requestXML.push("<a:LogicalName>" + entityName + "</a:LogicalName>");
        requestXML.push("<a:Name i:nil=\"true\" />");
        requestXML.push("</b:value>");
        requestXML.push("</a:KeyValuePairOfstringanyType>");
    }
    requestXML.push("</a:Parameters>");
    requestXML.push("<a:RequestId i:nil=\"true\" />");
    requestXML.push("<a:RequestName>" + requestName + "</a:RequestName>");
    requestXML.push("</request>");
    requestXML.push("</Execute>");
    requestXML.push("</s:Body>");
    requestXML.push("</s:Envelope>");
    var req = new XMLHttpRequest();
    req.open('POST', GetClientUrl(), true);
    req.setRequestHeader('Accept', 'application/xml, text/xml, */*');
    req.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
    req.setRequestHeader('SOAPAction', 'http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute');
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            if (req.status == 200) {
                debugger;
                var result = {
                    status: 'success',
                    data: null
                }
                if (req.response != null && req.response.length > 0) {
                    try {
                        var rs = $.parseXML(req.response).getElementsByTagName("KeyValuePairOfstringanyType");
                        if (rs.length == 0)
                            rs = $.parseXML(req.response).getElementsByTagName("a:KeyValuePairOfstringanyType");
                        var len = rs.length;
                        if (len > 0) {
                            result.data = {};
                            for (var i = 0; i < len; i++) {
                                var key = rs[i].firstElementChild;
                                var sib = key.nextElementSibling;
                                result.data[key.textContent] = {
                                    type: sib.hasAttribute("i:type") ? sib.attributes["i:type"].value : '',
                                    value: key.nextElementSibling.textContent
                                };
                            }
                        }
                    }
                    catch (ex) {
                        result.status = "error";
                        result.data = ex.message;
                        console.log(ex.message)
                    }
                }
                if (callback != null)
                    callback(result);
            }
            else if (req.status == 500) {
                if (req.responseXML != "") {
                    var mss = req.responseXML.getElementsByTagName("Message");
                    if (mss.length > 0) {
                        if (callback != null)
                            callback({ status: "error", data: mss[0].firstChild.nodeValue });
                        console.log(mss[0].firstChild.nodeValue);
                    }
                }
                else if (callback != null)
                    callback(null);
            }
        }
    };
    req.send(requestXML.join(''));
}
/*
    Mr: Diệm
    Note: Hàm lấy ngày...dùng chung
*/
function getFullDay(day) {
    var fullday = null;
    var createdatefrom = getValue(day);
    if (createdatefrom != null) {
        var year = createdatefrom.getFullYear();
        var date = createdatefrom.getDate();

        var laymonth = (createdatefrom.getMonth() + 1);
        var month;
        if (laymonth > 0 && laymonth <= 9) {
            month = "0" + laymonth;
        } else {
            month = laymonth;
        }
        fullday = year + "-" + month + "-" + date;
    }
    return fullday;
}

//Mr: Diệm
//Note: set date defaut cho fromdate.
function Load_FromDate_Auto() {
    if (formType() == 1) {
        var date = new Date();
        setValue("bsd_fromdate", date);
    }
}

//Trung
function check_enable_shipping(reset) {
    // call when customername or warehouse or address change.
    var customername = getValue("customerid");
    var warehousefrom = getValue("bsd_warehouseto");
    var warehouseaddress = getValue("bsd_warehouseaddress");
    var shiptoaddress = getValue("bsd_shiptoaddress");
    var effectivefrom = getValue("bsd_fromdate");
    var effectiveto = getValue("bsd_todate");
    if (customername != null && warehousefrom != null && shiptoaddress != null && warehouseaddress != null && effectivefrom != null && effectiveto != null) {
        setDisabled("bsd_transportation", false);
    } else {
        setValue("bsd_transportation", false);
        setDisabled("bsd_transportation", true);
    }
    shipping_change(reset);
}
function shipping_change(reset) {
    clearNotification("bsd_shippingpricelistname");
    if (reset != false) {
        setNull(["bsd_truckload", "bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
    }

    var bsd_transportation = getValue("bsd_transportation");
    if (bsd_transportation) { // có ship
        setRequired(["bsd_shiptoaccount", "bsd_shiptoaddress"], "required");
        setVisible(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], true);
        setRequired(["bsd_shippingdeliverymethod", "bsd_shippingpricelistname", "bsd_priceoftransportationn"], "required");
        shipping_deliverymethod_change(reset);
    } else {
        setRequired(["bsd_shiptoaccount", "bsd_shiptoaddress"], "none");
        setVisible(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], false);
        setRequired(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], "none");

        if (reset != false) {
            setValue("bsd_shippingporter", false);
            porteroption_change(reset);
        }
    }
}
function requestporter_change(reset) {
    shipping_change(reset);
}
function shipping_deliverymethod_change(reset) {

    if (reset != false) setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"]);
    var method;
    if (getValue("bsd_shippingdeliverymethod") == null) {
        setValue("bsd_shippingdeliverymethod", 861450000);
        method = 861450000;
    } else {
        method = getValue("bsd_shippingdeliverymethod");
    }

    if (method == 861450000) { // ton
        setVisible("bsd_unitshipping", true);
        setRequired("bsd_unitshipping", "required");
        if (getValue("bsd_unitshipping") == null) {
            setValue("bsd_unitshipping", [{
                id: "{7E3545D7-0AA0-E611-93F6-000C29EBC66A}",
                name: "Tấn",
                entityType: "uom"
            }]);
        }


        setVisible("bsd_truckload", false);
        setNull("bsd_truckload");
        setRequired("bsd_truckload", "none");

        load_shippingpricelist_ton(reset);

    } else if (861450001) { // trip

        setVisible("bsd_unitshipping", false);
        setNull("bsd_unitshipping");
        setRequired("bsd_unitshipping", "none");

        setVisible("bsd_truckload", true);
        setRequired("bsd_truckload", "required");

        if (reset != false) {
            setNull("bsd_truckload");
        }
        load_truckload(reset);
        truckload_change(reset);
    }
}
function shipping_pricelist_change(reset) {
    // code
    if (reset != false) setNull("bsd_priceoftransportationn");
    var pricelist = getValue("bsd_shippingpricelistname");
    if (pricelist != null) {

    } else if (reset != false) {

    }
    shipping_price_change();
}
function load_shippingpricelist_ton(reset) {
    if (reset != false) {
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var effective_from = getValue("bsd_fromdate");
        var effective_to = getValue("bsd_todate");

        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();
        var district_to = null;
        var ward_to = null;
        // lấy district + ward từ Ship To Address
        var fetch_get_shiptoaddress = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_address">',
            '<attribute name="bsd_addressid" />',
            '<attribute name="bsd_ward" />',
            '<attribute name="bsd_district" />',
            '<filter type="and">',
              '<condition attribute="bsd_addressid" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetch_get_shiptoaddress);
        if (rs.length > 0 && rs[0].getValue("bsd_district") != null && rs[0].getValue("bsd_ward") != null) {
            district_to = rs[0].getValue("bsd_district");
            ward_to = rs[0].getValue("bsd_ward");
        }

        var data = null;

        if (request_porter == true) {
            // Có yêu cầu bốc xếp
            //  // nếu có yêu cầu porter, và có theo xã.
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceunitporter" />',
                        '<order attribute="bsd_priceunitporter" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                          '<condition attribute="bsd_priceunitporter" operator="not-null" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(xml);
            if (rs.length > 0) {
                data = {
                    pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                    pricelist_name: rs[0].attributes.bsd_name.value,
                    price: rs[0].attributes.bsd_priceunitporter.value,
                    porter: true
                };
            } else {
                // nếu không có theo xã thì lấy theo quận  huyện
                xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                  '<entity name="bsd_shippingpricelist">',
                    '<attribute name="bsd_shippingpricelistid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="bsd_priceunitporter" />',
                    '<order attribute="bsd_priceunitporter" descending="true" />',
                    '<filter type="and">',
                      '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                      '<condition attribute="bsd_priceunitporter" operator="not-null" />',
                      '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                      '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                      '<condition attribute="bsd_wardto" operator="null" />',
                      '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                      ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                    '</filter>',
                  '</entity>',
                '</fetch>'].join("");
                var rs = CrmFetchKit.FetchSync(xml);
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_priceunitporter.value,
                        porter: true
                    };
                } else {
                    data = null;
                }

            }
        }

        if (request_porter == false || (request_porter == true && data == null)) {
            // Không yêu cầu giao hàng, hoặc là yêu cầu mà không có !
            // Lấy theo xã. + Không lấy giá porter
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceofton" />',
                        '<order attribute="bsd_priceofton" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                          '<condition attribute="bsd_priceofton" operator="not-null" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(xml);
            if (rs.length > 0) {
                data = {
                    pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                    pricelist_name: rs[0].attributes.bsd_name.value,
                    price: rs[0].attributes.bsd_priceofton.value,
                    porter: false
                };
            } else {
                // Không có theo xã thì lấy theo huyện, không yêu cầu hoặc yêu cầu mà không có !
                var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                  '<entity name="bsd_shippingpricelist">',
                    '<attribute name="bsd_shippingpricelistid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="bsd_priceofton" />',
                    '<order attribute="bsd_priceofton" descending="true" />',
                    '<filter type="and">',
                      '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                      '<condition attribute="bsd_priceofton" operator="not-null" />',
                      '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                      '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                      '<condition attribute="bsd_wardto" operator="null" />',
                      '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                      ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                    '</filter>',
                  '</entity>',
                '</fetch>'].join("");
                var rs = CrmFetchKit.FetchSync(xml);
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_priceofton.value,
                        porter: false
                    };
                } else {
                    data = null;
                }
            }
        }

        if (data != null) {
            set_shipping_pricelist(data);
            clearNotification("bsd_shippingpricelistname");
        } else {
            set_shipping_pricelist(null);
        }
    } else {
        porteroption_change(reset);
    }
}
function set_shipping_pricelist(data) {
    if (data != null) {
        setValue("bsd_shippingpricelistname", [{
            id: data.pricelist_id,
            name: data.pricelist_name,
            entityType: "bsd_shippingpricelist"
        }]);
        setValue("bsd_priceoftransportationn", data.price);
        if (data.porter == true) {
            setValue("bsd_shippingporter", true);
            setValue("bsd_porter", true);
        } else {
            setValue("bsd_shippingporter", false);
        }
        if (getValue("bsd_shippingpricelistname") == null) {
            setTimeout(function () {
                set_shipping_pricelist(data);
            }, 50);
        } else {
            porteroption_change(true);
        }
    } else {
        setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
        porteroption_change(true);
    }
}
function shipping_price_change() {
    // validate price
    var price = getValue("bsd_priceoftransportationn");
    if (price == null) {
        //setNotification("bsd_priceoftransportationn", "You must provide a value for Price");
    } else if (price <= 0) {
        setNotification("bsd_priceoftransportationn", "Enter a value from 0");
    } else {
        clearNotification("bsd_priceoftransportationn");
    }
}
function load_truckload(reset) {
    var shiptoaddress = getValue("bsd_shiptoaddress");
    var warehouse_address = getValue("bsd_warehouseaddress");

    var xml_shiptoaddress = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
     '<entity name="bsd_address">',
        '<attribute name="bsd_addressid" />',
        '<attribute name="bsd_ward" />',
        '<attribute name="bsd_province" />',
        '<attribute name="bsd_district" />',
        '<order attribute="bsd_ward" descending="false" />',
        '<filter type="and">',
          '<condition attribute="bsd_addressid" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    CrmFetchKit.Fetch(xml_shiptoaddress, false).then(function (rs) {
        if (rs.length > 0) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
             '<entity name="bsd_truckload">',
               '<attribute name="bsd_truckloadid" />',
               '<attribute name="bsd_name" />',
               '<order attribute="bsd_name" descending="false" />',
               '<link-entity name="bsd_shippingpricelist" from="bsd_truckload" to="bsd_truckloadid" alias="ab">',
                 '<filter>',
                   '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                   '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouse_address[0].id + '" />',
                   '<filter type="or">',
                       '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + rs[0].getValue("bsd_district") + '" />',
                       '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + rs[0].getValue("bsd_ward") + '" />',
                   '</filter>',
                 '</filter>',
               '</link-entity>',
             '</entity>',
           '</fetch>'].join("");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_truckloadid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                                    "<row name='result'  " + "id='bsd_truckloadid'>  " +
                                                    "<cell name='bsd_name'   " + "width='200' />  " +
                                                    "</row>   " +
                                                 "</grid>   ";

            getControl("bsd_truckload").addCustomView(getDefaultView("bsd_truckload"), "bsd_truckload", "bsd_truckload", xml, layoutXml, true);
        }
    });

}
function truckload_change(reset) {
    // load shipping theo trip
    if (reset != false) {
        setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
    }

    var truckload = getValue("bsd_truckload");
    if (truckload != null && reset != false) {
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var effective_from = getValue("bsd_fromdate");
        var effective_to = getValue("bsd_todate");

        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();
        var district_to = null;
        var ward_to = null;
        var data = null;
        // lấy district + ward từ Ship To Address
        var fetch_get_shiptoaddress = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_address">',
            '<attribute name="bsd_addressid" />',
            '<attribute name="bsd_ward" />',
            '<attribute name="bsd_district" />',
            '<filter type="and">',
              '<condition attribute="bsd_addressid" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetch_get_shiptoaddress);
        if (rs.length > 0 && rs[0].getValue("bsd_district") != null && rs[0].getValue("bsd_ward") != null) {
            district_to = rs[0].getValue("bsd_district");
            ward_to = rs[0].getValue("bsd_ward");
        }


        if (request_porter == true) {
            // Có yêu cầu bốc xếp
            //  // nếu có yêu cầu porter, và có theo xã.
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_pricetripporter" />',
                        '<order attribute="bsd_pricetripporter" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                          '<condition attribute="bsd_pricetripporter" operator="not-null" />',
                          '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(xml);
            if (rs.length > 0) {
                data = {
                    pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                    pricelist_name: rs[0].attributes.bsd_name.value,
                    price: rs[0].attributes.bsd_pricetripporter.value,
                    porter: true
                };
            } else {
                // nếu không có theo xã thì lấy theo quận  huyện
                xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                  '<entity name="bsd_shippingpricelist">',
                    '<attribute name="bsd_shippingpricelistid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="bsd_pricetripporter" />',
                    '<order attribute="bsd_pricetripporter" descending="true" />',
                    '<filter type="and">',
                      '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                      '<condition attribute="bsd_pricetripporter" operator="not-null" />',
                      '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                      '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                      '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                      '<condition attribute="bsd_wardto" operator="null" />',
                      '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                      ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                    '</filter>',
                  '</entity>',
                '</fetch>'].join("");
                var rs = CrmFetchKit.FetchSync(xml);
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_pricetripporter.value,
                        porter: true
                    };
                } else {
                    data = null;
                }

            }
        }

        if (request_porter == false || (request_porter == true && data == null)) {
            // Không yêu cầu giao hàng, hoặc là yêu cầu mà không có !
            // Lấy theo xã. + Không lấy giá porter
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceoftrip" />',
                        '<order attribute="bsd_priceoftrip" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                          '<condition attribute="bsd_priceoftrip" operator="not-null" />',
                          '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(xml);
            if (rs.length > 0) {
                data = {
                    pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                    pricelist_name: rs[0].attributes.bsd_name.value,
                    price: rs[0].attributes.bsd_priceoftrip.value,
                    porter: false
                };
            } else {
                // Không có theo xã thì lấy theo huyện, không yêu cầu hoặc yêu cầu mà không có !
                var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                  '<entity name="bsd_shippingpricelist">',
                    '<attribute name="bsd_shippingpricelistid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="bsd_priceoftrip" />',
                    '<order attribute="bsd_priceoftrip" descending="true" />',
                    '<filter type="and">',
                      '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                      '<condition attribute="bsd_priceoftrip" operator="not-null" />',
                      '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                      '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                      '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                      '<condition attribute="bsd_wardto" operator="null" />',
                      '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                      ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                    '</filter>',
                  '</entity>',
                '</fetch>'].join("");
                var rs = CrmFetchKit.FetchSync(xml);
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_priceoftrip.value,
                        porter: false
                    };
                } else {
                    data = null;
                }
            }
        }

        if (data != null) {
            set_shipping_pricelist(data);
            clearNotification("bsd_shippingpricelistname");
        } else {
            set_shipping_pricelist(null);
        }
    } else {
        porteroption_change(reset);
    }
}
function load_warehouse_address(reset) {

    if (reset != false) setNull("bsd_warehouseaddress");

    var warehouse = getValue("bsd_warehouseto");
    if (warehouse != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="bsd_warehouseentity">',
        '<attribute name="bsd_warehouseentityid" />',
        '<attribute name="bsd_address" />',
        '<filter type="and">',
          '<condition attribute="bsd_warehouseentityid" operator="eq" uitype="bsd_warehouseentity" value="' + warehouse[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && reset != false) {
                var first = rs[0];
                console.log(first);
                setValue("bsd_warehouseaddress", [{
                    id: first.attributes.bsd_address.guid,
                    name: first.attributes.bsd_address.name,
                    entityType: first.attributes.bsd_address.logicalName
                }]);
            }
        });
    }
    check_enable_shipping(reset);
}
function porteroption_change(reset) {
    //if (reset != false) setNull(["bsd_priceofporter", "bsd_pricepotter"]);
    var request_porter = getValue("bsd_requestporter");
    var porteroption = getValue("bsd_porteroption");
    var shipping_porter = getValue("bsd_shippingporter");
    if (request_porter == false) {
        // Không yêu cầu !
        setDisabled(["bsd_porteroption"], true);

        setVisible(["bsd_priceofporter", "bsd_pricepotter"], false);
        setRequired(["bsd_priceofporter", "bsd_pricepotter"], "none");
        if (reset != false) {
            setValue("bsd_porteroption", false);
            setValue("bsd_porter", 861450001);
            setNull(["bsd_priceofporter", "bsd_pricepotter"]);
        }

    } else if (request_porter == true) { // Có yêu cầu
        if (shipping_porter == true) { // Giá đã gồm Porter
            setDisabled(["bsd_porteroption"], true);
            setVisible(["bsd_priceofporter", "bsd_pricepotter"], false);
            setRequired(["bsd_priceofporter", "bsd_pricepotter"], "none");
            if (reset != false) {
                setValue("bsd_porteroption", true);
                setValue("bsd_porter", 861450000);
                setNull(["bsd_priceofporter", "bsd_pricepotter"]);
            }
        } else { // Không có giá  porter
            setDisabled(["bsd_porteroption"], true);
            setVisible(["bsd_priceofporter", "bsd_pricepotter"], true);
            setRequired(["bsd_priceofporter", "bsd_pricepotter"], "required");
            if (reset != false) {
                setValue("bsd_porteroption", true);
                setValue("bsd_porter", 861450000);
            }
            load_porter();
            porterprice_change(reset);
        }
    }
}
function porterprice_change(reset) {
    if (reset != false) {
        var bsd_priceofporter = getValue("bsd_priceofporter");
        if (bsd_priceofporter != null) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_porter">',
                '<attribute name="bsd_porterid" />',
                '<attribute name="bsd_price" />',
                '<order attribute="bsd_price" descending="false" />',
                '<filter type="and">',
                  '<condition attribute="bsd_porterid" operator="eq" uitype="bsd_porter" value="' + bsd_priceofporter[0].id + '" />',
                  '<condition attribute="bsd_price" operator="not-null" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    setValue("bsd_pricepotter", first.getValue("bsd_price"));
                }
            });
        } else {
            setNull("bsd_pricepotter");
        }
    }
}
function check_date() {
    // effective date
    check_enable_shipping(true);
    check_fromdate_todate();
    changevalue_pricelistfield();
}
function check_fromdate_todate() {
    clearNotification("bsd_fromdate");
    var from = getValue("bsd_fromdate");
    var to = getValue("bsd_todate");
    if (from != null && to != null) {
        if (from > to) {
            setNotification("bsd_fromdate", "The From Date cannot occur before the To Date");
        }
    }
}
function onsave(econtext) {
    var bsd_transportation = getValue("bsd_transportation");
    if (bsd_transportation == true) {
        var pricelist = getValue("bsd_shippingpricelistname");
        if (pricelist == null) {
            setNotification("bsd_shippingpricelistname", "You must provide a value for Price List !");
        }
    }
}
function load_sale_tax_group() {
    var customer = getValue("customerid");
    if (customer != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="account">',
           ' <attribute name="name" />',
           ' <attribute name="bsd_saletaxgroup" />',
           ' <filter type="and">',
            '  <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
           ' </filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && rs[0].getValue("bsd_saletaxgroup") != null) {
                console.log(rs[0]);
                setValue("bsd_saletaxgroup", [{
                    id: rs[0].getValue("bsd_saletaxgroup"),
                    name: rs[0].attributes.bsd_saletaxgroup.name,
                    entityType: rs[0].attributes.bsd_saletaxgroup.logicalName
                }]);
            }
        });
    } else {
        setNull("bsd_saletaxgroup");
    }
}
//End



//huy: add function from B2Bnew: 3hh20 28/2/2017
function load_porter() {
    var effective_to = getValue("bsd_todate");
    var effective_from = getValue("bsd_fromdate");
    getControl("bsd_priceofporter").removePreSearch(presearch_priceofporter);
    if (effective_from != null && effective_to != null) {
        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();

        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_porter">',
                   '     <attribute name="bsd_porterid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_effectiveto" />',
                   '     <attribute name="bsd_effectivefrom" />',
                   '     <attribute name="bsd_unit" />',
                   '     <attribute name="bsd_price" />',
                   '     <order attribute="bsd_effectiveto" descending="true" />',
                   '     <order attribute="createdon" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");

        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_porterid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                           "<row name='result'  " + "id='bsd_porterid'>  " +
                                           "<cell name='bsd_name'   " + "width='200' />  " +
                                           "</row>" +
                                        "</grid>";
        getControl("bsd_priceofporter").addCustomView(getDefaultView("bsd_priceofporter"), "bsd_porter", "bsd_porter", xml, layoutXml, true);
        var priceofporter = getValue("bsd_priceofporter");
        if (priceofporter != null) {
            var xml2 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_porter">',
                   '     <attribute name="bsd_porterid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_effectiveto" />',
                   '     <attribute name="bsd_effectivefrom" />',
                   '     <attribute name="bsd_unit" />',
                   '     <attribute name="bsd_price" />',
                   '     <order attribute="bsd_effectiveto" descending="true" />',
                   '     <order attribute="createdon" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="bsd_porterid" operator="eq" uitype="bsd_porter" value="' + priceofporter[0].id + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join('');
            CrmFetchKit.Fetch(xml2, false).then(function (rs) {
                if (rs.length > 0) {

                } else {
                    setTimeout(function () {
                        setNull(["bsd_priceofporter", "bsd_pricepotter"]);
                    }, 10);

                }
            });
        }
    } else {
        clear_priceofporter();
        setNull(["bsd_priceofporter", "bsd_pricepotter"]);
    }
}
function clear_priceofporter() {
    getControl("bsd_priceofporter").addPreSearch(presearch_priceofporter);
}
function presearch_priceofporter() {
    getControl("bsd_priceofporter").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function getDate(d) {
    return new Date('' + d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear() + ' 12:00:00 AM');
}
/*  
    Diệm: 
*/

//huy -8h20h00  14/3/2017
function load_unitdefault(reset) {

    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="uom">',
        '<attribute name="name" />',
        '<filter type="and">',
          '<condition attribute="uomscheduleid" operator="eq" uitype="uomschedule" value="{A8C3859A-0095-E611-80CC-000C294C7A2D}" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' object='1' jump='uomid' select='1' icon='0' preview='0'>  " +
                     "<row name='result'  " + "id='uomid'><cell name='name'   " + "width='200' /> </row></grid>";
    getControl("bsd_unitdefault").addCustomView(getDefaultView("bsd_unitdefault"), "uom", "uom", xml, layoutXml, true);

    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs[0].getValue("bsd_unitdefault") != null) {
                setValue("bsd_unitdefault", [{
                    id: rs[0].attributes.bsd_unitdefault.guid,
                    name: rs[0].attributes.bsd_unitdefault.name,
                    entityType: rs[0].attributes.bsd_unitdefault.logicalName
                }]);
            }
        });

    }
}

//huy
function load_currencydefault(reset) {
    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs[0].getValue("bsd_currencydefault") != null) {
                setValue("bsd_currencydefault", [{
                    id: rs[0].attributes.bsd_currencydefault.guid,
                    name: rs[0].attributes.bsd_currencydefault.name,
                    entityType: rs[0].attributes.bsd_currencydefault.logicalName
                }]);
            }
        });

    }
}

//Diem
/*Diê?m: Load exchangerate tu` currency*/
function Load_exchangerate(currency) {
    if (currency != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="transactioncurrency">',
                        '    <attribute name="transactioncurrencyid" />',
                        '    <attribute name="currencyname" />',
                        '    <attribute name="isocurrencycode" />',
                        '    <attribute name="currencysymbol" />',
                        '    <attribute name="exchangerate" />',
                        '    <attribute name="currencyprecision" />',
                        '    <order attribute="currencyname" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            if (getValue("exchangerate") == null) {
                setValue("exchangerate", rs[0].attributes.exchangerate.value);
            }
        }
    }
    else {
        setNull(["exchangerate"])
    }
}

/*
   Mr: Diệm
   Note: Load Price group theo fromdate.
*/
function LK_PotentialCustomer_Change(reset) {
    debugger;
    var customer = getValue("customerid");
    var fullday = getFullDay("bsd_fromdate");
    var currency = getValue("transactioncurrencyid");
    if (reset != false) setNull("pricelevelid");
    var first;
    if (customer != null && currency != null && fullday != null) {
        var rsaccount = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" count="1"  mapping="logical" distinct="true">',
                           '    <entity name="pricelevel">',
                           '        <all-attributes />',
                           '        <order attribute="createdon" descending="false" />',
                           '    <filter type="and">',
                           '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                           '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                           '        <condition attribute="bsd_account" operator="eq" uitype="account" value="' + customer[0].id + '"/>',
                           '        <condition attribute="statecode" operator="eq" value="0" />',
                           '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                           '    </filter>',
                           '    </entity>',
                           '</fetch>'].join(''));
        console.log(rsaccount);
        if (rsaccount.length > 0) /*Nếu dữ liệu Account*/ {
            first = rsaccount[0];
            if (getValue("pricelevelid") == null) {
                setValue("pricelevelid",
                     [{
                         id: first.Id,
                         name: first.attributes.name.value,
                         entityType: first.logicalName
                     }]);
            }
        }
        else /*Price group*/ {

            var rspricegroup = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                                '   <entity name="pricelevel">',
                                '        <all-attributes />',
                                '     <order attribute="createdon" descending="false" />',
                                '     <filter type="and">',
                                '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                                '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                                '        <condition attribute="statecode" operator="eq" value="0" />',
                                '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                                '     </filter>',
                                '     <link-entity name="bsd_pricegroups" from="bsd_pricegroupsid" to="bsd_pricegroups" alias="ai">',
                                '       <link-entity name="account" from="bsd_pricegroups" to="bsd_pricegroupsid" alias="aj">',
                                '         <filter type="and">',
                                '           <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                                '         </filter>',
                                '       </link-entity>',
                                '     </link-entity>',
                                '    </entity>',
                                '</fetch>'].join(''));
            console.log(rspricegroup);
            if (rspricegroup.length > 0) {

                first = rspricegroup[0];
                if (getValue("pricelevelid") == null) {
                    setValue("pricelevelid",
                         [{
                             id: first.Id,
                             name: first.attributes.name.value,
                             entityType: first.logicalName
                         }]);
                }
            }
            else /*All*/ {
                var rsaccount = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                 ' <entity name="account">',
                                 '   <attribute name="accountid" />',
                                 '   <attribute name="bsd_accounttype" />',
                                 '   <order attribute="bsd_accounttype" descending="false" />',
                                 '   <filter type="and">',
                                 '     <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                                 '     <condition attribute="statecode" operator="eq" value="0" />',
                                 '   </filter>',
                                 ' </entity>',
                                '</fetch>'].join(''));
                var typeaccount = rsaccount[0].attributes.bsd_accounttype.value;
                var fetchxml = "";
                if (typeaccount == 861450000 || typeaccount == 861450001) {
                    fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                              '    <entity name="pricelevel">',
                              '        <all-attributes />',
                              '      <order attribute="createdon" descending="false" />',
                              '      <filter type="and">',
                              '        <condition attribute="statecode" operator="eq" value="0" />',
                              '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                              '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                              '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                              '        <condition attribute="bsd_typeprice" operator="eq" value="2" />',
                              '        <condition attribute="bsd_type" operator="eq" value="861450000" />',
                              '     </filter>',
                              '   </entity>',
                              '</fetch>'].join('');
                }
                else if (typeaccount == 100000000) {
                    fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                              '    <entity name="pricelevel">',
                              '        <all-attributes />',
                              '      <order attribute="createdon" descending="false" />',
                              '      <filter type="and">',
                              '        <condition attribute="statecode" operator="eq" value="0" />',
                              '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                              '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                              '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                              '        <condition attribute="bsd_typeprice" operator="eq" value="2" />',
                              '        <condition attribute="bsd_type" operator="eq" value="861450001" />',
                              '     </filter>',
                              '   </entity>',
                              '</fetch>'].join('');

                } else {
                    fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                              '    <entity name="pricelevel">',
                              '        <all-attributes />',
                              '      <order attribute="createdon" descending="false" />',
                              '      <filter type="and">',
                              '        <condition attribute="statecode" operator="eq" value="0" />',
                              '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                              '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                              '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                              '        <condition attribute="bsd_typeprice" operator="eq" value="2" />',
                              '     </filter>',
                              '   </entity>',
                              '</fetch>'].join('');
                }
                var rsAll = CrmFetchKit.FetchSync(fetchxml);
                if (rsAll.length > 0) {
                    first = rsAll[0];
                    if (getValue("pricelevelid") == null) {
                        setValue("pricelevelid",
                             [{
                                 id: first.Id,
                                 name: first.attributes.name.value,
                                 entityType: first.logicalName
                             }]);
                    }
                } else {
                    m_alert("Price List for this Account does not exist. Please define Price List first.");
                }
            }
        }
    }
    else if (reset != false) {
        clear_priceleve();
    }
    Load_exchangerate(currency);
}
function changevalue_pricelistfield() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var day = Xrm.Page.getAttribute("bsd_fromdate").getValue();
    var fieldpricelist = Xrm.Page.getAttribute("pricelevelid");
    var currency = Xrm.Page.getAttribute("transactioncurrencyid").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    if (day != null && currency != null) {
        var year = day.getFullYear() + "";
        if (day.getMonth() + 1 <= 9) {
            var month = "0" + (day.getMonth() + 1) + "";
        }
        else {
            var month = (day.getMonth() + 1) + "";
        }
        var dayy = day.getDate() + "";
        var dateFormat = year + "-" + month + "-" + dayy;
        if (customer != null) {
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='pricelevel'>");
            xml.push("<attribute name='name' />");
            xml.push("<attribute name='transactioncurrencyid' />");
            xml.push("<attribute name='enddate' />");
            xml.push("<attribute name='begindate' />");
            xml.push("<attribute name='statecode' />");
            xml.push("<attribute name='pricelevelid' />");
            xml.push("<attribute name='createdon'/>");
            xml.push("<order attribute='createdon' descending='false' />");
            xml.push("<filter type='and'>");
            xml.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='" + customer[0].id + "' />");
            xml.push("<condition attribute='begindate' operator='on-or-before' value='" + dateFormat + "' />");
            xml.push("<condition attribute='transactioncurrencyid' operator='eq' value='" + currency[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    var i, j, k;
                    var a = [];
                    for (i = 0; i < rs.length; i++) {
                        var enddate = rs[i].attributes.enddate.value;
                        var yearr = enddate.getFullYear() + "";
                        if (enddate.getMonth() + 1 <= 9) {
                            var monthh = "0" + (enddate.getMonth() + 1) + "";
                        }
                        else {
                            var monthh = (enddate.getMonth() + 1) + "";
                        }
                        var dayyy = enddate.getDate() + "";
                        if (year <= yearr) {
                            if (month < monthh) {
                                fieldpricelist.setValue([{
                                    id: rs[i].Id,
                                    name: rs[i].attributes.name.value,
                                    entityType: rs[i].logicalName
                                }]);
                                Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                Xrm.Page.getControl("pricelevelid").clearNotification();
                                Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                break;
                            }
                            if (month == monthh) {
                                if (dayy < dayyy) {
                                    fieldpricelist.setValue([{
                                        id: rs[i].Id,
                                        name: rs[i].attributes.name.value,
                                        entityType: rs[i].logicalName
                                    }]);
                                    Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                    Xrm.Page.getControl("pricelevelid").clearNotification();
                                    Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                    break;
                                }
                            }
                        }
                        Xrm.Page.getAttribute("bsd_hide").setValue("co");
                    }
                    if (Xrm.Page.getAttribute("bsd_hide").getValue() == "co") {
                        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                        xml1.push("<entity name='account'>");
                        xml1.push("<attribute name='name' />");
                        xml1.push("<attribute name='primarycontactid' />");
                        xml1.push("<attribute name='telephone1' />");
                        xml1.push("<attribute name='accountid' />");
                        xml1.push("<attribute name='bsd_pricegroup' />");
                        xml1.push("<order attribute='name' descending='false' />");
                        xml1.push("<filter type='and'>");
                        xml1.push("<condition attribute='accountid' operator='eq' uitype='account ' value='" + customer[0].id + "' />");
                        xml1.push("</filter>");
                        xml1.push("</entity>");
                        xml1.push("</fetch>");
                        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                            if (rs1.length > 0) {
                                if (rs1[0].attributes.bsd_pricegroup != null) {
                                    xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                                    xml2.push("<entity name='pricelevel'>");
                                    xml2.push("<attribute name='name' />");
                                    xml2.push("<attribute name='transactioncurrencyid' />");
                                    xml2.push("<attribute name='enddate' />");
                                    xml2.push("<attribute name='begindate' />");
                                    xml2.push("<attribute name='statecode' />");
                                    xml2.push("<attribute name='pricelevelid' />");
                                    xml2.push("<attribute name='createdon'/>");
                                    xml2.push("<order attribute='createdon' descending='false' />");
                                    xml2.push("<filter type='and'>");
                                    xml2.push("<condition attribute='bsd_pricegroup' operator='eq' uitype='bsd_pricegroups ' value='" + rs1[0].attributes.bsd_pricegroup.guid + "' />");
                                    xml2.push("<condition attribute='begindate' operator='on-or-before' value='" + dateFormat + "' />");
                                    xml2.push("<condition attribute='transactioncurrencyid' operator='eq' value='" + currency[0].id + "' />");
                                    xml2.push("</filter>");
                                    xml2.push("</entity>");
                                    xml2.push("</fetch>");
                                    CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                                        if (rs2.length > 0) {
                                            var i;
                                            for (i = 0; i < rs2.length; i++) {
                                                var enddate = rs2[0].attributes.enddate.value;
                                                var yearr = enddate.getFullYear() + "";
                                                if (enddate.getMonth() + 1 <= 9) {
                                                    var monthh = "0" + (enddate.getMonth() + 1) + "";
                                                }
                                                else {
                                                    var monthh = (enddate.getMonth() + 1) + "";
                                                }
                                                var dayyy = enddate.getDate() + "";
                                                if (year <= yearr) {
                                                    if (month < monthh) {
                                                        fieldpricelist.setValue([{
                                                            id: rs2[i].Id,
                                                            name: rs2[i].attributes.name.value,
                                                            entityType: rs2[i].logicalName
                                                        }]);
                                                        Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                        Xrm.Page.getControl("pricelevelid").clearNotification();
                                                        Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                        break;
                                                    }
                                                    if (month = monthh) {
                                                        if (dayy < dayyy) {
                                                            fieldpricelist.setValue([{
                                                                id: rs2[i].Id,
                                                                name: rs2[i].attributes.name.value,
                                                                entityType: rs2[i].logicalName
                                                            }]);
                                                            Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                            Xrm.Page.getControl("pricelevelid").clearNotification();
                                                            Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                            break;
                                                        }
                                                    }
                                                }
                                                Xrm.Page.getAttribute("bsd_hide").setValue("co");
                                            }
                                            if (Xrm.Page.getAttribute("bsd_hide").getValue() == "co") {
                                                xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                                                xml3.push("<entity name='pricelevel'>");
                                                xml3.push("<attribute name='name' />");
                                                xml3.push("<attribute name='transactioncurrencyid' />");
                                                xml3.push("<attribute name='enddate' />");
                                                xml3.push("<attribute name='begindate' />");
                                                xml3.push("<attribute name='statecode' />");
                                                xml3.push("<attribute name='pricelevelid' />");
                                                xml3.push("<attribute name='createdon'/>");
                                                xml3.push("<order attribute='createdon' descending='false' />");
                                                xml3.push("<filter type='and'>");
                                                xml3.push("<condition attribute='bsd_pricelisttype' operator='eq' value='100000000' />");
                                                xml3.push("<condition attribute='transactioncurrencyid' operator='eq' value='" + currency[0].id + "' />");
                                                xml3.push("</filter>");
                                                xml3.push("</entity>");
                                                xml3.push("</fetch>");
                                                CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                                                    if (rs3.length > 0) {
                                                        var i;
                                                        for (i = 0; i < rs3.length; i++) {
                                                            var enddate = rs3[0].attributes.enddate.value;
                                                            var yearr = enddate.getFullYear() + "";
                                                            if (enddate.getMonth() + 1 <= 9) {
                                                                var monthh = "0" + (enddate.getMonth() + 1) + "";
                                                            }
                                                            else {
                                                                var monthh = (enddate.getMonth() + 1) + "";
                                                            }
                                                            var dayyy = enddate.getDate() + "";
                                                            if (year <= yearr) {
                                                                if (month < monthh) {
                                                                    fieldpricelist.setValue([{
                                                                        id: rs3[i].Id,
                                                                        name: rs3[i].attributes.name.value,
                                                                        entityType: rs3[i].logicalName
                                                                    }]);
                                                                    Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                                    Xrm.Page.getControl("pricelevelid").clearNotification();
                                                                    Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                                    break;
                                                                }
                                                                if (month = monthh) {
                                                                    if (dayy < dayyy) {
                                                                        fieldpricelist.setValue([{
                                                                            id: rs3[i].Id,
                                                                            name: rs3[i].attributes.name.value,
                                                                            entityType: rs3[i].logicalName
                                                                        }]);
                                                                        Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                                        Xrm.Page.getControl("pricelevelid").clearNotification();
                                                                        Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            Xrm.Page.getAttribute("bsd_hide").setValue("co");
                                                        }
                                                        if (Xrm.Page.getAttribute("bsd_hide").getValue() == "co") {
                                                            //alert("This account has no pricelist.Please create an pricelist for this account!");
                                                            var message = "This account has no pricelist.Please create an pricelist for this account";
                                                            var type = "INFO"; //INFO, WARNING, ERROR
                                                            var id = "Info1"; //Notification Id
                                                            var time = 3000; //Display time in milliseconds

                                                            //Display the notification
                                                            Xrm.Page.ui.setFormNotification(message, type, id);

                                                            //Wait the designated time and then remove
                                                            setTimeout(
                                                                function () {
                                                                    Xrm.Page.ui.clearFormNotification(id);
                                                                },
                                                                time
                                                            );
                                                            Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                            Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                                            fieldpricelist.setValue(null);
                                                        }
                                                    }
                                                    else {
                                                        //alert("This account has no pricelist.Please create an pricelist for this account!");
                                                        var message = "This account has no pricelist.Please create an pricelist for this account";
                                                        var type = "INFO"; //INFO, WARNING, ERROR
                                                        var id = "Info1"; //Notification Id
                                                        var time = 3000; //Display time in milliseconds

                                                        //Display the notification
                                                        Xrm.Page.ui.setFormNotification(message, type, id);

                                                        //Wait the designated time and then remove
                                                        setTimeout(
                                                            function () {
                                                                Xrm.Page.ui.clearFormNotification(id);
                                                            },
                                                            time
                                                        );
                                                        Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                        Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                                        fieldpricelist.setValue(null);
                                                    }
                                                },
                                    function (er) {
                                        console.log(er.message)
                                    });
                                            }
                                        }
                                        else {
                                            xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                                            xml3.push("<entity name='pricelevel'>");
                                            xml3.push("<attribute name='name' />");
                                            xml3.push("<attribute name='transactioncurrencyid' />");
                                            xml3.push("<attribute name='enddate' />");
                                            xml3.push("<attribute name='begindate' />");
                                            xml3.push("<attribute name='statecode' />");
                                            xml3.push("<attribute name='pricelevelid' />");
                                            xml3.push("<attribute name='createdon'/>");
                                            xml3.push("<order attribute='createdon' descending='false' />");
                                            xml3.push("<filter type='and'>");
                                            xml3.push("<condition attribute='bsd_pricelisttype' operator='eq' value='100000000' />");
                                            xml3.push("<condition attribute='transactioncurrencyid' operator='eq' value='" + currency[0].id + "' />");
                                            xml3.push("</filter>");
                                            xml3.push("</entity>");
                                            xml3.push("</fetch>");
                                            CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                                                if (rs3.length > 0) {
                                                    var i;
                                                    for (i = 0; i < rs3.length; i++) {
                                                        var enddate = rs3[0].attributes.enddate.value;
                                                        var yearr = enddate.getFullYear() + "";
                                                        if (enddate.getMonth() + 1 <= 9) {
                                                            var monthh = "0" + (enddate.getMonth() + 1) + "";
                                                        }
                                                        else {
                                                            var monthh = (enddate.getMonth() + 1) + "";
                                                        }
                                                        var dayyy = enddate.getDate() + "";
                                                        if (year <= yearr) {
                                                            if (month < monthh) {
                                                                fieldpricelist.setValue([{
                                                                    id: rs3[i].Id,
                                                                    name: rs3[i].attributes.name.value,
                                                                    entityType: rs3[i].logicalName
                                                                }]);
                                                                Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                                Xrm.Page.getControl("pricelevelid").clearNotification();
                                                                Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                                break;
                                                            }
                                                            if (month = monthh) {
                                                                if (dayy < dayyy) {
                                                                    fieldpricelist.setValue([{
                                                                        id: rs3[i].Id,
                                                                        name: rs3[i].attributes.name.value,
                                                                        entityType: rs3[i].logicalName
                                                                    }]);
                                                                    Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                                    Xrm.Page.getControl("pricelevelid").clearNotification();
                                                                    Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        Xrm.Page.getAttribute("bsd_hide").setValue("co");
                                                    }
                                                    if (Xrm.Page.getAttribute("bsd_hide").getValue() == "co") {
                                                        //alert("This account has no pricelist.Please create an pricelist for this account!");
                                                        var message = "This account has no pricelist.Please create an pricelist for this account";
                                                        var type = "INFO"; //INFO, WARNING, ERROR
                                                        var id = "Info1"; //Notification Id
                                                        var time = 3000; //Display time in milliseconds

                                                        //Display the notification
                                                        Xrm.Page.ui.setFormNotification(message, type, id);

                                                        //Wait the designated time and then remove
                                                        setTimeout(
                                                            function () {
                                                                Xrm.Page.ui.clearFormNotification(id);
                                                            },
                                                            time
                                                        );
                                                        Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                        Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                                        fieldpricelist.setValue(null);
                                                    }
                                                }
                                                else {
                                                    //alert("This account has no pricelist.Please create an pricelist for this account!");
                                                    var message = "This account has no pricelist.Please create an pricelist for this account";
                                                    var type = "INFO"; //INFO, WARNING, ERROR
                                                    var id = "Info1"; //Notification Id
                                                    var time = 3000; //Display time in milliseconds

                                                    //Display the notification
                                                    Xrm.Page.ui.setFormNotification(message, type, id);

                                                    //Wait the designated time and then remove
                                                    setTimeout(
                                                        function () {
                                                            Xrm.Page.ui.clearFormNotification(id);
                                                        },
                                                        time
                                                    );
                                                    Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                    Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                                    fieldpricelist.setValue(null);
                                                }
                                            },
                                function (er) {
                                    console.log(er.message)
                                });
                                        }
                                    },
                                    function (er) {
                                        console.log(er.message)
                                    });
                                }
                                else {
                                    xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                                    xml3.push("<entity name='pricelevel'>");
                                    xml3.push("<attribute name='name' />");
                                    xml3.push("<attribute name='transactioncurrencyid' />");
                                    xml3.push("<attribute name='enddate' />");
                                    xml3.push("<attribute name='begindate' />");
                                    xml3.push("<attribute name='statecode' />");
                                    xml3.push("<attribute name='pricelevelid' />");
                                    xml3.push("<attribute name='createdon'/>");
                                    xml3.push("<order attribute='createdon' descending='false' />");
                                    xml3.push("<filter type='and'>");
                                    xml3.push("<condition attribute='bsd_pricelisttype' operator='eq' value='100000000' />");
                                    xml3.push("<condition attribute='transactioncurrencyid' operator='eq' value='" + currency[0].id + "' />");
                                    xml3.push("</filter>");
                                    xml3.push("</entity>");
                                    xml3.push("</fetch>");
                                    CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                                        if (rs3.length > 0) {
                                            var i;
                                            for (i = 0; i < rs3.length; i++) {
                                                var enddate = rs3[0].attributes.enddate.value;
                                                var yearr = enddate.getFullYear() + "";
                                                if (enddate.getMonth() + 1 <= 9) {
                                                    var monthh = "0" + (enddate.getMonth() + 1) + "";
                                                }
                                                else {
                                                    var monthh = (enddate.getMonth() + 1) + "";
                                                }
                                                var dayyy = enddate.getDate() + "";
                                                if (year <= yearr) {
                                                    if (month < monthh) {
                                                        fieldpricelist.setValue([{
                                                            id: rs3[i].Id,
                                                            name: rs3[i].attributes.name.value,
                                                            entityType: rs3[i].logicalName
                                                        }]);
                                                        Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                        Xrm.Page.getControl("pricelevelid").clearNotification();
                                                        Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                        break;
                                                    }
                                                    if (month = monthh) {
                                                        if (dayy < dayyy) {
                                                            fieldpricelist.setValue([{
                                                                id: rs3[i].Id,
                                                                name: rs3[i].attributes.name.value,
                                                                entityType: rs3[i].logicalName
                                                            }]);
                                                            Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                            Xrm.Page.getControl("pricelevelid").clearNotification();
                                                            Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                            break;
                                                        }
                                                    }
                                                }
                                                Xrm.Page.getAttribute("bsd_hide").setValue("co");
                                            }
                                            if (Xrm.Page.getAttribute("bsd_hide").getValue() == "co") {
                                                //alert("This account has no pricelist.Please create an pricelist for this account!");
                                                var message = "This account has no pricelist.Please create an pricelist for this account";
                                                var type = "INFO"; //INFO, WARNING, ERROR
                                                var id = "Info1"; //Notification Id
                                                var time = 3000; //Display time in milliseconds

                                                //Display the notification
                                                Xrm.Page.ui.setFormNotification(message, type, id);

                                                //Wait the designated time and then remove
                                                setTimeout(
                                                    function () {
                                                        Xrm.Page.ui.clearFormNotification(id);
                                                    },
                                                    time
                                                );
                                                Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                                fieldpricelist.setValue(null);
                                            }
                                        }
                                        else {
                                            //alert("This account has no pricelist.Please create an pricelist for this account!");
                                            var message = "This account has no pricelist.Please create an pricelist for this account";
                                            var type = "INFO"; //INFO, WARNING, ERROR
                                            var id = "Info1"; //Notification Id
                                            var time = 3000; //Display time in milliseconds

                                            //Display the notification
                                            Xrm.Page.ui.setFormNotification(message, type, id);

                                            //Wait the designated time and then remove
                                            setTimeout(
                                                function () {
                                                    Xrm.Page.ui.clearFormNotification(id);
                                                },
                                                time
                                            );
                                            Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                            Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                            fieldpricelist.setValue(null);
                                        }
                                    },
                        function (er) {
                            console.log(er.message)
                        });
                                }
                            }
                        },
                        function (er) {
                            console.log(er.message)
                        });
                    }
                }
                else {
                    xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                    xml4.push("<entity name='account'>");
                    xml4.push("<attribute name='name' />");
                    xml4.push("<attribute name='primarycontactid' />");
                    xml4.push("<attribute name='telephone1' />");
                    xml4.push("<attribute name='accountid' />");
                    xml4.push("<attribute name='bsd_pricegroup' />");
                    xml4.push("<order attribute='name' descending='false' />");
                    xml4.push("<filter type='and'>");
                    xml4.push("<condition attribute='accountid' operator='eq' uitype='account ' value='" + customer[0].id + "' />");
                    xml4.push("</filter>");
                    xml4.push("</entity>");
                    xml4.push("</fetch>");
                    CrmFetchKit.Fetch(xml4.join(""), true).then(function (rs4) {
                        if (rs4.length > 0) {
                            if (rs4[0].attributes.bsd_pricegroup != null) {
                                xml5.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                                xml5.push("<entity name='pricelevel'>");
                                xml5.push("<attribute name='name' />");
                                xml5.push("<attribute name='transactioncurrencyid' />");
                                xml5.push("<attribute name='enddate' />");
                                xml5.push("<attribute name='begindate' />");
                                xml5.push("<attribute name='statecode' />");
                                xml5.push("<attribute name='pricelevelid' />");
                                xml5.push("<attribute name='createdon'/>");
                                xml5.push("<order attribute='createdon' descending='false' />");
                                xml5.push("<filter type='and'>");
                                xml5.push("<condition attribute='bsd_pricegroup' operator='eq' uitype='bsd_pricegroups' value='" + rs4[0].attributes.bsd_pricegroup.guid + "' />");
                                xml5.push("<condition attribute='begindate' operator='on-or-before' value='" + dateFormat + "' />");
                                xml5.push("<condition attribute='transactioncurrencyid' operator='eq' value='" + currency[0].id + "' />");
                                xml5.push("</filter>");
                                xml5.push("</entity>");
                                xml5.push("</fetch>");
                                CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                                    if (rs5.length > 0) {
                                        var i;
                                        for (i = 0; i < rs5.length; i++) {
                                            var enddate = rs5[0].attributes.enddate.value;
                                            var yearr = enddate.getFullYear() + "";
                                            if (enddate.getMonth() + 1 <= 9) {
                                                var monthh = "0" + (enddate.getMonth() + 1) + "";
                                            }
                                            else {
                                                var monthh = (enddate.getMonth() + 1) + "";
                                            }
                                            var dayyy = enddate.getDate() + "";
                                            if (year <= yearr) {
                                                if (month < monthh) {
                                                    fieldpricelist.setValue([{
                                                        id: rs5[i].Id,
                                                        name: rs5[i].attributes.name.value,
                                                        entityType: rs5[i].logicalName
                                                    }]);
                                                    Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                    Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                    Xrm.Page.getControl("pricelevelid").clearNotification();
                                                    break;
                                                }
                                                if (month == monthh) {
                                                    if (dayy < dayyy) {
                                                        fieldpricelist.setValue([{
                                                            id: rs5[i].Id,
                                                            name: rs5[i].attributes.name.value,
                                                            entityType: rs5[i].logicalName
                                                        }]);
                                                        Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                        Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                        Xrm.Page.getControl("pricelevelid").clearNotification();
                                                        break;
                                                    }
                                                }
                                            }
                                            Xrm.Page.getAttribute("bsd_hide").setValue("co");
                                        }
                                        if (Xrm.Page.getAttribute("bsd_hide").getValue() == "co") {
                                            xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                                            xml3.push("<entity name='pricelevel'>");
                                            xml3.push("<attribute name='name' />");
                                            xml3.push("<attribute name='transactioncurrencyid' />");
                                            xml3.push("<attribute name='enddate' />");
                                            xml3.push("<attribute name='begindate' />");
                                            xml3.push("<attribute name='statecode' />");
                                            xml3.push("<attribute name='pricelevelid' />");
                                            xml3.push("<attribute name='createdon'/>");
                                            xml3.push("<order attribute='createdon' descending='false' />");
                                            xml3.push("<filter type='and'>");
                                            xml3.push("<condition attribute='bsd_pricelisttype' operator='eq' value='100000000' />");
                                            xml3.push("<condition attribute='transactioncurrencyid' operator='eq' value='" + currency[0].id + "' />");
                                            xml3.push("</filter>");
                                            xml3.push("</entity>");
                                            xml3.push("</fetch>");
                                            CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                                                if (rs3.length > 0) {
                                                    var i;
                                                    for (i = 0; i < rs3.length; i++) {
                                                        var enddate = rs3[0].attributes.enddate.value;
                                                        var yearr = enddate.getFullYear() + "";
                                                        if (enddate.getMonth() + 1 <= 9) {
                                                            var monthh = "0" + (enddate.getMonth() + 1) + "";
                                                        }
                                                        else {
                                                            var monthh = (enddate.getMonth() + 1) + "";
                                                        }
                                                        var dayyy = enddate.getDate() + "";
                                                        if (year <= yearr) {
                                                            if (month < monthh) {
                                                                fieldpricelist.setValue([{
                                                                    id: rs3[i].Id,
                                                                    name: rs3[i].attributes.name.value,
                                                                    entityType: rs3[i].logicalName
                                                                }]);
                                                                Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                                Xrm.Page.getControl("pricelevelid").clearNotification();
                                                                Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                                break;
                                                            }
                                                            if (month == monthh) {
                                                                if (dayy < dayyy) {
                                                                    fieldpricelist.setValue([{
                                                                        id: rs3[i].Id,
                                                                        name: rs3[i].attributes.name.value,
                                                                        entityType: rs3[i].logicalName
                                                                    }]);
                                                                    Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                                    Xrm.Page.getControl("pricelevelid").clearNotification();
                                                                    Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        Xrm.Page.getAttribute("bsd_hide").setValue("co");
                                                    }
                                                    if (Xrm.Page.getAttribute("bsd_hide").getValue() == "co") {
                                                        //alert("This account has no pricelist.Please create an pricelist for this account!");
                                                        var message = "This account has no pricelist.Please create an pricelist for this account";
                                                        var type = "INFO"; //INFO, WARNING, ERROR
                                                        var id = "Info1"; //Notification Id
                                                        var time = 3000; //Display time in milliseconds

                                                        //Display the notification
                                                        Xrm.Page.ui.setFormNotification(message, type, id);

                                                        //Wait the designated time and then remove
                                                        setTimeout(
                                                            function () {
                                                                Xrm.Page.ui.clearFormNotification(id);
                                                            },
                                                            time
                                                        );
                                                        Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                        Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                                        fieldpricelist.setValue(null);
                                                    }
                                                }
                                                else {
                                                    //alert("This account has no pricelist.Please create an pricelist for this account!");
                                                    var message = "This account has no pricelist.Please create an pricelist for this account";
                                                    var type = "INFO"; //INFO, WARNING, ERROR
                                                    var id = "Info1"; //Notification Id
                                                    var time = 3000; //Display time in milliseconds

                                                    //Display the notification
                                                    Xrm.Page.ui.setFormNotification(message, type, id);

                                                    //Wait the designated time and then remove
                                                    setTimeout(
                                                        function () {
                                                            Xrm.Page.ui.clearFormNotification(id);
                                                        },
                                                        time
                                                    );
                                                    Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                    Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                                    fieldpricelist.setValue(null);
                                                }
                                            },
                                function (er) {
                                    console.log(er.message)
                                });

                                        }
                                    }
                                    else {
                                        xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                                        xml3.push("<entity name='pricelevel'>");
                                        xml3.push("<attribute name='name' />");
                                        xml3.push("<attribute name='transactioncurrencyid' />");
                                        xml3.push("<attribute name='enddate' />");
                                        xml3.push("<attribute name='begindate' />");
                                        xml3.push("<attribute name='statecode' />");
                                        xml3.push("<attribute name='pricelevelid' />");
                                        xml3.push("<attribute name='createdon'/>");
                                        xml3.push("<order attribute='createdon' descending='false' />");
                                        xml3.push("<filter type='and'>");
                                        xml3.push("<condition attribute='bsd_pricelisttype' operator='eq' value='100000000' />");
                                        xml3.push("<condition attribute='transactioncurrencyid' operator='eq' value='" + currency[0].id + "' />");
                                        xml3.push("</filter>");
                                        xml3.push("</entity>");
                                        xml3.push("</fetch>");
                                        CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                                            if (rs3.length > 0) {
                                                var i;
                                                for (i = 0; i < rs3.length; i++) {
                                                    var enddate = rs3[0].attributes.enddate.value;
                                                    var yearr = enddate.getFullYear() + "";
                                                    if (enddate.getMonth() + 1 <= 9) {
                                                        var monthh = "0" + (enddate.getMonth() + 1) + "";
                                                    }
                                                    else {
                                                        var monthh = (enddate.getMonth() + 1) + "";
                                                    }
                                                    var dayyy = enddate.getDate() + "";
                                                    if (year <= yearr) {
                                                        if (month < monthh) {
                                                            fieldpricelist.setValue([{
                                                                id: rs3[i].Id,
                                                                name: rs3[i].attributes.name.value,
                                                                entityType: rs3[i].logicalName
                                                            }]);
                                                            Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                            Xrm.Page.getControl("pricelevelid").clearNotification();
                                                            Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                            break;
                                                        }
                                                        if (month == monthh) {
                                                            if (dayy < dayyy) {
                                                                fieldpricelist.setValue([{
                                                                    id: rs3[i].Id,
                                                                    name: rs3[i].attributes.name.value,
                                                                    entityType: rs3[i].logicalName
                                                                }]);
                                                                Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                                Xrm.Page.getControl("pricelevelid").clearNotification();
                                                                Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    Xrm.Page.getAttribute("bsd_hide").setValue("co");
                                                }
                                                if (Xrm.Page.getAttribute("bsd_hide").getValue() == "co") {
                                                    //alert("This account has no pricelist.Please create an pricelist for this account!");
                                                    var message = "This account has no pricelist.Please create an pricelist for this account";
                                                    var type = "INFO"; //INFO, WARNING, ERROR
                                                    var id = "Info1"; //Notification Id
                                                    var time = 3000; //Display time in milliseconds

                                                    //Display the notification
                                                    Xrm.Page.ui.setFormNotification(message, type, id);

                                                    //Wait the designated time and then remove
                                                    setTimeout(
                                                        function () {
                                                            Xrm.Page.ui.clearFormNotification(id);
                                                        },
                                                        time
                                                    );
                                                    Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                    Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                                    fieldpricelist.setValue(null);
                                                }
                                            }
                                            else {
                                                //alert("This account has no pricelist.Please create an pricelist for this account!");
                                                var message = "This account has no pricelist.Please create an pricelist for this account";
                                                var type = "INFO"; //INFO, WARNING, ERROR
                                                var id = "Info1"; //Notification Id
                                                var time = 3000; //Display time in milliseconds

                                                //Display the notification
                                                Xrm.Page.ui.setFormNotification(message, type, id);

                                                //Wait the designated time and then remove
                                                setTimeout(
                                                    function () {
                                                        Xrm.Page.ui.clearFormNotification(id);
                                                    },
                                                    time
                                                );
                                                Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                                fieldpricelist.setValue(null);
                                            }
                                        },
                                function (er) {
                                    console.log(er.message)
                                });
                                    }
                                },
                                function (er) {
                                    console.log(er.message)
                                });
                            }
                            else {
                                xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                                xml3.push("<entity name='pricelevel'>");
                                xml3.push("<attribute name='name' />");
                                xml3.push("<attribute name='transactioncurrencyid' />");
                                xml3.push("<attribute name='enddate' />");
                                xml3.push("<attribute name='begindate' />");
                                xml3.push("<attribute name='statecode' />");
                                xml3.push("<attribute name='pricelevelid' />");
                                xml3.push("<attribute name='createdon'/>");
                                xml3.push("<order attribute='createdon' descending='false' />");
                                xml3.push("<filter type='and'>");
                                xml3.push("<condition attribute='bsd_pricelisttype' operator='eq' value='100000000' />");
                                xml3.push("<condition attribute='transactioncurrencyid' operator='eq' value='" + currency[0].id + "' />");
                                xml3.push("</filter>");
                                xml3.push("</entity>");
                                xml3.push("</fetch>");
                                CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                                    if (rs3.length > 0) {
                                        var i;
                                        for (i = 0; i < rs3.length; i++) {
                                            var enddate = rs3[0].attributes.enddate.value;
                                            var yearr = enddate.getFullYear() + "";
                                            if (enddate.getMonth() + 1 <= 9) {
                                                var monthh = "0" + (enddate.getMonth() + 1) + "";
                                            }
                                            else {
                                                var monthh = (enddate.getMonth() + 1) + "";
                                            }
                                            var dayyy = enddate.getDate() + "";
                                            if (year <= yearr) {
                                                if (month < monthh) {
                                                    fieldpricelist.setValue([{
                                                        id: rs3[i].Id,
                                                        name: rs3[i].attributes.name.value,
                                                        entityType: rs3[i].logicalName
                                                    }]);
                                                    Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                    Xrm.Page.getControl("pricelevelid").clearNotification();
                                                    Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                    break;
                                                }
                                                if (month == monthh) {
                                                    if (dayy < dayyy) {
                                                        fieldpricelist.setValue([{
                                                            id: rs3[i].Id,
                                                            name: rs3[i].attributes.name.value,
                                                            entityType: rs3[i].logicalName
                                                        }]);
                                                        Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                                        Xrm.Page.getControl("pricelevelid").clearNotification();
                                                        Xrm.Page.getAttribute("bsd_hide").setValue("khong");
                                                        break;
                                                    }
                                                }
                                            }
                                            Xrm.Page.getAttribute("bsd_hide").setValue("co");
                                        }
                                        if (Xrm.Page.getAttribute("bsd_hide").getValue() == "co") {
                                            //alert("This account has no pricelist.Please create an pricelist for this account!");
                                            var message = "This account has no pricelist.Please create an pricelist for this account";
                                            var type = "INFO"; //INFO, WARNING, ERROR
                                            var id = "Info1"; //Notification Id
                                            var time = 3000; //Display time in milliseconds

                                            //Display the notification
                                            Xrm.Page.ui.setFormNotification(message, type, id);

                                            //Wait the designated time and then remove
                                            setTimeout(
                                                function () {
                                                    Xrm.Page.ui.clearFormNotification(id);
                                                },
                                                time
                                            );
                                            Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                            Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                            fieldpricelist.setValue(null);
                                        }
                                    }
                                    else {
                                        //alert("This account has no pricelist.Please create an pricelist for this account!");
                                        var message = "This account has no pricelist.Please create an pricelist for this account";
                                        var type = "INFO"; //INFO, WARNING, ERROR
                                        var id = "Info1"; //Notification Id
                                        var time = 3000; //Display time in milliseconds

                                        //Display the notification
                                        Xrm.Page.ui.setFormNotification(message, type, id);

                                        //Wait the designated time and then remove
                                        setTimeout(
                                            function () {
                                                Xrm.Page.ui.clearFormNotification(id);
                                            },
                                            time
                                        );
                                        Xrm.Page.getControl("pricelevelid").setDisabled(true);
                                        Xrm.Page.getControl("pricelevelid").setNotification("This account has no pricelist.Please create an pricelist for this account");
                                        fieldpricelist.setValue(null);
                                    }
                                },
                    function (er) {
                        console.log(er.message)
                    });
                            }
                        }
                    },
                    function (er) {
                        console.log(er.message)
                    });
                }
            },
    function (er) {
        console.log(er.message)
    });
        }
    }
    if (currency == null) {
        fieldpricelist.setValue(null);
    }
}
/*
    Mr: Diệm
    Note: Set Function null and clear.
*/
function clear_priceleve() {
    getControl("pricelevelid").addPreSearch(presearch_priceleve);
}
function presearch_priceleve() {
    getControl("pricelevelid").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function errorHandle(err) {
    console.log(err);
}
function check_from_quote() {
    if (getValue("quoteid") != null) {
        setDisabled(["bsd_accompanyingdocuments", "customerid","bsd_customercode", "bsd_address", "bsd_invoicenameaccount","bsd_invoiceaccount", "bsd_addressinvoiceaccount","bsd_taxregistration", "bsd_fromdate", "bsd_todate"], true);
        setDisabled(["bsd_warehouseto", "bsd_warehouseaddress", "bsd_shiptoaddress", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_unitshipping"], true);
        setDisabled(["bsd_partialshipping", "bsd_requestporter", "bsd_transportation", "bsd_shippingdeliverymethod", "bsd_truckload", "bsd_porter", "bsd_priceofporter", "bsd_shippingporter"], true);
        setDisabled(["bsd_currencydefault", "bsd_unitdefault", "bsd_paymentterm", "bsd_paymentmethod", "bsd_porteroption", "bsd_pricepotter"], true);
    }
}
function ordertype_change() {
    alert("ok");
}
function check_createdsuborder() {
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="bsd_suborder">',
        '<attribute name="bsd_suborderid" />',
        '<attribute name="bsd_name" />',
        '<filter type="and">',
          '<condition attribute="bsd_order" operator="eq" uiname="2017 huy test 4" uitype="salesorder" value="' + getId() + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            DisabledForm();
            Xrm.Page.ui.setFormNotification("Đơn hàng đã tạo Suborder, không thể chỉnh sửa. ", "INFO", "1");
        }
    });
}
function CheckQuantityRemaining() {
    var result = false;
    var gridOrderProduct = window.parent.document.getElementById("salesorderdetailsGrid");
    if (gridOrderProduct != null && gridOrderProduct.control != null && gridOrderProduct.control.get_selectedRecords().length > 0) {
        var selectedRow = gridOrderProduct.control.get_selectedRecords()[0];
        var rs = CrmFetchKit.FetchSync([
            '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="salesorderdetail">',
                '<attribute name="productid" />',
                '<attribute name="salesorderid" />',
                '<attribute name="quantity" />',
                '<order attribute="productid" descending="false" />',
                '<filter type="and">',
                  '<condition attribute="salesorderdetailid" operator="eq" value="' + selectedRow.Id + '" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join(""));
        if (rs.length > 0) {
            // số lượng đặt hàng của sản phẩm này
            var ProductQuantity = rs[0].attributes.quantity.value;
            var rs1 = CrmFetchKit.FetchSync([
                        '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                              '<entity name="bsd_scheduledelivery">',
                                    '<attribute name="bsd_scheduledeliveryid" />',
                                    '<attribute name="bsd_shipquantity" />',
                                            '<attribute name="createdon" />',
                                            '<order attribute="bsd_name" descending="false" />',
                                            '<link-entity name="salesorder" from="salesorderid" to="bsd_orderid" alias="ae">',
                                                  '<filter type="and">',
                                                    '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + rs[0].attributes.salesorderid.guid + '" />',
                                                  '</filter>',
                                            '</link-entity>',
                                            '<link-entity name="product" from="productid" to="bsd_itemid" alias="af">',
                                                  '<filter type="and">',
                                                    '<condition attribute="productid" operator="eq" uitype="product" value="' + rs[0].attributes.productid.guid + '" />',
                                                  '</filter>',
                                            '</link-entity>',
                                      '</entity>',
                                '</fetch>'].join(""));
            var TotalShipped = 0;
            if (rs1.length > 0) {
                for (var i = 0; i < rs1.length; i++) {
                    TotalShipped += rs1[i].attributes.bsd_shipquantity.value;
                }
            }
            if (ProductQuantity > TotalShipped) {
                Xrm.Page.ui.clearFormNotification('1');
                result = true;
            } else {
                Xrm.Page.ui.setFormNotification("Sản phẩm này đã giao đủ số lượng. ", "WARNING", "1");
                result = false;
            }
        }
    }
    else {
        var rs = CrmFetchKit.FetchSync([
        '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
         '<entity name="salesorderdetail">',
           '<attribute name="productid" />',
            '<attribute name="salesorderid" />',
            '<attribute name="quantity" />',
            '<order attribute="productid" descending="false" />',
            '<filter type="and">',
            '<condition attribute="salesorderid" operator="eq" value="' + Xrm.Page.data.entity.getId() + '" />',
            '</filter>',
            '</entity>',
            '</fetch>'
        ].join(""));
        Xrm.Page.ui.clearFormNotification('1');
        if (rs.length > 0) {
            var OrderQuantity = 0;
            for (var i = 0; i < rs.length; i++) {
                OrderQuantity += rs[i].attributes.quantity.value;
            }
            var rs1 = CrmFetchKit.FetchSync([
                '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                          '<entity name="bsd_scheduledelivery">',
                                '<attribute name="bsd_scheduledeliveryid" />',
                                '<attribute name="bsd_shipquantity" />',
                                '<attribute name="createdon" />',
                                '<order attribute="bsd_name" descending="false" />',
                                '<link-entity name="salesorder" from="salesorderid" to="bsd_orderid" alias="ae">',
                                      '<filter type="and">',
                                        '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + Xrm.Page.data.entity.getId() + '" />',
                                      '</filter>',
                                '</link-entity>',
                          '</entity>',
                    '</fetch>',
            ].join(""));
            var TotalShipped = 0;
            if (rs1.length > 0) {
                for (var i = 0; i < rs1.length; i++) {
                    TotalShipped += rs1[i].attributes.bsd_shipquantity.value;
                }
            }
            if (OrderQuantity > TotalShipped) {
                result = true;
            } else {
                Xrm.Page.ui.setFormNotification("Đơn hàng này đã giao đủ số lượng. ", "WARNING", "1");
                result = false;
            }

        } else {
            Xrm.Page.ui.setFormNotification("Chưa có sản phẩm. ", "WARNING", "1");
        }
    }
    return result;
}
//End

//Button
//function BtnDeleteOrderProductEnableRule() {
//    return true;
//}
function BtnAddSalesorderdetail() {
    var check = false;
    var rs = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="salesorder">',
            '<attribute name="name" />',
            '<attribute name="bsd_status" />',
            '<attribute name="bsd_ordertype" />',
            '<attribute name="bsd_createfrom" />',
            '<attribute name="salesorderid" />',
            '<order attribute="name" descending="false" />',
            '<filter type="and">',
              '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + Xrm.Page.data.entity.getId() + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join(''));
    if (rs.length > 0) {
        var salesorder = rs[0];
        var type = salesorder.attributes.bsd_ordertype.guid;
        var bsd_createfrom = salesorder.attributes.bsd_createfrom.value;
        var bsd_status = salesorder.attributes.bsd_status.value;

        if (type == "f0dd845a-92c3-e611-93f1-000c29d47eab" && bsd_createfrom == 861450003) {
            check = true;
        }
    }
    return check;
}
function set_currenciesdefault() {
    var xml = [
           '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
           '<entity name="transactioncurrency">',
           '<attribute name="transactioncurrencyid" />',
           '<attribute name="currencyname" />',
           '<attribute name="isocurrencycode" />',
           '<attribute name="currencysymbol" />',
           '<attribute name="exchangerate" />',
           '<attribute name="currencyprecision" />',
           '<order attribute="currencyname" descending="false" />',
           '<filter type="and">',
             '<condition attribute="isocurrencycode" operator="eq" value="VND" />',
           '</filter>',
           '</entity>',
           '</fetch>'
    ].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            setValue("transactioncurrencyid", [{
                id: rs[0].Id,
                name: rs[0].attributes.currencyname.value,
                entityType: "transactioncurrency"
            }]);
        }
    }, function (er) {
        console.log(er.message);
    });
}
function BtnScheduleDeliveryClick() {
    console.log("BtnScheduleDeliveryClick");
    debugger;
    var grid = window.parent.document.getElementById("salesorderdetailsGrid").control.get_selectedRecords();
    if (grid.length > 0) {
        var selectedRow = grid[0];
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                      '<entity name="salesorderdetail">',
                                        '<attribute name="productid" />',
                                        '<attribute name="salesorderid" />',
                                        '<order attribute="productid" descending="false" />',
                                        '<filter type="and">',
                                          '<condition attribute="salesorderdetailid" operator="eq" value="' + selectedRow.Id + '" />',
                                        '</filter>',
                                      '</entity>',
                                    '</fetch>'
        ];
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            var productId = rs[0].attributes.productid.guid;
            var orderId = rs[0].attributes.salesorderid.guid;
            var xmlSchedule = [
            '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_scheduledelivery">',
                '<attribute name="bsd_scheduledeliveryid" />',
                '<attribute name="bsd_name" />',
                '<attribute name="createdon" />',
                '<order attribute="bsd_name" descending="false" />',
                '<filter type="and">',
                  '<condition attribute="bsd_itemid" operator="eq" uitype="product" value="' + productId + '" />',
                  '<condition attribute="bsd_orderid" operator="eq" uitype="salesorder" value="' + orderId + '" />',
                '</filter>',
              '</entity>',
            '</fetch>',
            ].join("");
            var objSubGrid = window.parent.document.getElementById("bsd_scheduledelivery");
            if (objSubGrid != null && objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", xmlSchedule);
                objSubGrid.control.Refresh();
            } else {
                setTimeout('BtnScheduleDeliveryClick()', 2000);
            }
        }, function (err) {
            console.log(err)
        });
    }
}
/*
    Mr: Diệm
    Note: kiểm tra so lượng tối thiểu và số lượng trong order
*/
function Check_QuantityMinimize() {
    var soluongtoithieu = 0;
    var totalquantityorderproduct = 0;
    var PotentialCustomer = getValue("customerid");
    if (PotentialCustomer != null) {
        var rs = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                        '  <entity name="account">',
                                        '    <attribute name="name" />',
                                        '    <attribute name="accountid" />',
                                        '    <attribute name="bsd_minquantity" />',
                                        '    <order attribute="name" descending="false" />',
                                        '    <filter type="and">',
                                        '      <condition attribute="accountid" operator="eq" uitype="account" value="' + PotentialCustomer[0].id + '" />',
                                        '    </filter>',
                                        '  </entity>',
                                        '</fetch>'].join(""));
        if (rs.length > 0) {
            console.log(rs);
            if (rs[0].attributes.bsd_minquantity.value != null) {
                soluongtoithieu = rs[0].attributes.bsd_minquantity.value;
            }
        }
    }
    alert(soluongtoithieu);
    var order = Xrm.Page.data.entity.getId();
    if (order != null) {
        var order = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                              '   <entity name="salesorderdetail">',
                              '     <attribute name="quantity" />',
                              '     <attribute name="salesorderdetailid" />',
                              '     <order attribute="quantity" descending="false" />',
                              '     <filter type="and">',
                              '       <condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + order + '" />',
                              '     </filter>',
                              '   </entity>',
                              ' </fetch>'].join(""));
        if (order.length > 0) {
            console.log(order);
            for (var i = 0; i < order.length; i++) {
                totalquantityorderproduct += order[0].attributes.quantity.value;
            }
        }
    }
    if (totalquantityorderproduct - soluongtoithieu >= 0) {
        return true;
    } else {
        return false;
    }
}
function BtnNewScheduleDeliveryClick() {
    if (CheckQuantityRemaining()) {
        debugger;
        var grid = window.parent.document.getElementById("salesorderdetailsGrid").control.get_selectedRecords();
        if (grid.length > 0) {
            Xrm.Page.data.save().then(function () {
                var selectedRow = grid[0];
                var data = {};
                SDK.REST.retrieveRecord(selectedRow.Id, "SalesOrderDetail", "ProductId,SalesOrderId,bsd_giasauthue", null, function (salesorderdetail) {

                    var parameters = {};
                    var account = getValue("customerid");
                    parameters["bsd_name"] = "Lịch giao hàng - " + getValue("name") + " - " + salesorderdetail.ProductId.Name;

                    parameters["bsd_account"] = account[0].id;
                    parameters["bsd_accountname"] = account[0].name;

                    parameters["bsd_orderid"] = salesorderdetail.SalesOrderId.Id;
                    parameters["bsd_orderidname"] = salesorderdetail.SalesOrderId.Name;

                    parameters["bsd_itemid"] = salesorderdetail.ProductId.Id;
                    parameters["bsd_itemidname"] = salesorderdetail.ProductId.Name;

                    parameters["bsd_price"] = salesorderdetail.bsd_giasauthue.Value;

                    var warehousefrom = getValue("bsd_warehouseto");
                    parameters["bsd_warehousefrom"] = warehousefrom[0].id;
                    parameters["bsd_warehousefromname"] = warehousefrom[0].name;


                    var bsd_shiptoaccount = getValue("bsd_shiptoaccount");
                    if (bsd_shiptoaccount != null) {
                        parameters["bsd_shiptoaccount"] = bsd_shiptoaccount[0].id;
                        parameters["bsd_shiptoaccountname"] = bsd_shiptoaccount[0].name;
                    } else {
                        parameters["bsd_shiptoaccount"] = account[0].id;
                        parameters["bsd_shiptoaccountname"] = account[0].name;
                    }


                    var bsd_addresscustomeraccount = getValue("bsd_address");
                    if (bsd_addresscustomeraccount != null) {
                        parameters["bsd_addresscustomeraccount"] = bsd_addresscustomeraccount[0].id;
                        parameters["bsd_addresscustomeraccountname"] = bsd_addresscustomeraccount[0].name;
                    }


                    var shiptoaddress = getValue("bsd_shiptoaddress");
                    if (shiptoaddress != null) {
                        parameters["bsd_shiptoaddress"] = shiptoaddress[0].id;
                        parameters["bsd_shiptoaddressname"] = shiptoaddress[0].name;
                    }


                    Xrm.Utility.openEntityForm("bsd_scheduledelivery", null, parameters);
                }, errorHandle);
            });
        } else {
            m_alert("Please select a product to create Delivery Schedule");
        }
    }


    //older - trung.
    //if (CheckQuantityRemaining()) {
    //    var grid = window.parent.document.getElementById("salesorderdetailsGrid").control.get_selectedRecords();
    //    if (grid.length > 0) {
    //        //m_confirm("Are you sure to create new Delivery Plan for product ?", function (e) {
    //        var selectedRow = grid[0];
    //        var data = {};
    //        SDK.REST.retrieveRecord(selectedRow.Id, "SalesOrderDetail", "ProductId,SalesOrderId,PricePerUnit", null, function (salesorderdetail) {
    //            debugger;
    //            data.bsd_price = { Value: salesorderdetail.PricePerUnit.Value };
    //            data.bsd_itemid = salesorderdetail.ProductId;
    //            data.bsd_orderid = salesorderdetail.SalesOrderId;
    //            var account = getValue("customerid");
    //            data.bsd_account = {
    //                Id: account[0].id,
    //                Name: account[0].name,
    //                LogicalName: account[0].typename
    //            };
    //            data.bsd_name = "Kế hoạch giao hàng - " + getValue("name") + " - " + data.bsd_itemid.Name;
    //            var data_ship = getShipQuantity(data.bsd_orderid.Id, data.bsd_itemid.Id);
    //            var warehouse = getWareHouseByOrder(data.bsd_orderid.Id);
    //            if (warehouse != null) {
    //                data.bsd_warehousefrom = {
    //                    Id: warehouse.Id,
    //                    Name: warehouse.attributes.bsd_name.value,
    //                    LogicalName: warehouse.logicalName
    //                };
    //                data.bsd_quantityinwarehouse = getWarehouseQuantityOfProduct(data.bsd_itemid.Id, warehouse.Id) + '';
    //            }
    //            SDK.REST.retrieveRecord(data.bsd_account.Id, "Account", "bsd_timeship", null, function (account) {
    //                if (account.bsd_timeship != null) {
    //                    data.bsd_TimeShip = account.bsd_timeship + '';
    //                }
    //                data.bsd_totalquantity = data_ship.total + '';
    //                data.bsd_shippedquantity = data_ship.shipped + '';
    //                data.bsd_remainingquantity = data_ship.remaining + '';
    //                data.bsd_shipquantity = data_ship.remaining + '';
    //                SDK.REST.createRecord(data,"bsd_scheduledelivery","", null,function (rs) {
    //                    debugger;
    //                    console.log(rs);
    //                    Xrm.Utility.openEntityForm("bsd_scheduledelivery", rs.bsd_scheduledeliveryId);
    //                }, errorHandle);
    //            }, function () { alert("loi") });
    //        }, function () { alert("loi") });
    //        //});
    //    } else {
    //        m_alert("Please select a product to create Delivery Plan");
    //    }
    //}
}
function BtnReviseOrderClick() {
    Xrm.Page.data.refresh(true).then(function () {
        var status = getValue("bsd_status");
        if (status == 1) {
            var column = null;
            SDK.REST.retrieveRecord(Xrm.Page.data.entity.getId(), "SalesOrder", column, null, function (order) {
                console.log(getValue("bsd_parentorder"));
                var SalesOrderId = order.SalesOrderId;
                delete order["SalesOrderId"];
                delete order["CreatedOn"];
                if (order.bsd_revisionid) {
                    order.bsd_revisionid += 1;
                } else {
                    order.bsd_revisionid = 1;
                }
                order.bsd_checkrevise = true;
                order.bsd_checkfromquote = false;
                order.bsd_parentorder = {
                    Id: Xrm.Page.data.entity.getId(),
                    Name: getValue("name"),
                    LogicalName: "salesorder"
                }
                if (getValue("bsd_rootorder") != null) {
                    var rootOrder = getValue("bsd_rootorder")[0];
                    order.bsd_rootorder = {
                        Id: rootOrder.id,
                        Name: rootOrder.name,
                        LogicalName: "salesorder"
                    };
                } else {
                    order.bsd_rootorder = order.bsd_parentorder;
                }
                SDK.REST.createRecord(order, "SalesOrder", function (rs) {
                    var updateOrder = {
                        bsd_status: { Value: 0 },
                    }
                    SDK.REST.updateRecord(SalesOrderId, updateOrder, "SalesOrder", function () {
                        Xrm.Utility.openEntityForm("salesorder", rs.SalesOrderId);
                    }, errorHandle);
                }, function (error) {
                    console.log(error);
                });
            }, errorHandle);
        } else {
            alert("This Sales Order be closed");
        }
    });
    //var populated = true;
    //Xrm.Page.getAttribute(function (attribute, index) {
    //    if (attribute.getRequiredLevel() == "required") {
    //        if (attribute.getValue() === null) {
    //            populated = false;
    //        }
    //    }
    //});
    //var ismodified = Xrm.Page.data.entity.getIsDirty();
    //if (populated && !ismodified) {

    //} else {
    //    alert("Please fill all form required and save to continue !");
    //}
}
function set_valuecreatefrom() {
    if (formType() == 1) {
        setValue("bsd_createfrom", 861450003);
    }
}
function getShipQuantity(orderId, productId) {
    // Lấy tổng số lượng của sản phẩm trong order.
    // Lấy số lượng đã ship.
    // lấy số lượng còn lại.
    var data = {
        total: 0,
        shipped: 0,
        remaining: 0
    }
    data.total = getQuantityProductInOrder(orderId, productId);
    var xml = [
        '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_scheduledelivery">',
                    '<attribute name="bsd_scheduledeliveryid" />',
                    '<attribute name="bsd_shipquantity" />',
                    '<attribute name="createdon" />',
                    '<order attribute="bsd_name" descending="false" />',
                    '<link-entity name="salesorder" from="salesorderid" to="bsd_orderid" alias="ae">',
                          '<filter type="and">',
                            '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + orderId + '" />',
                          '</filter>',
                    '</link-entity>',
                    '<link-entity name="product" from="productid" to="bsd_itemid" alias="af">',
                          '<filter type="and">',
                            '<condition attribute="productid" operator="eq" uitype="product" value="' + productId + '" />',
                          '</filter>',
                    '</link-entity>',
              '</entity>',
        '</fetch>',
    ].join("");
    var rs = CrmFetchKit.FetchSync(xml);
    if (rs.length > 0) {
        for (var i = 0; i < rs.length; i++) {
            data.shipped += rs[i].attributes.bsd_shipquantity.value;
        }
        data.remaining = data.total - data.shipped;
    } else {
        data.remaining = data.total;
    }

    //CrmFetchKit.Fetch(xml, false).then(function (rs) {
    //    if (rs.length > 0) {
    //        for (var i = 0; i < rs.length; i++) {
    //            data.shipped += rs[i].attributes.bsd_shipquantity.value;
    //        }
    //        data.remaining = data.total - data.shipped;
    //    } else {
    //        data.remaining = data.total;
    //    }
    //}, function (er) { });
    return data;
}
function getWareHouseByOrder(orderId) {
    var warehouse = {};
    var rs = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                      '<entity name="bsd_warehouseentity">',
                        '<attribute name="bsd_warehouseentityid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<link-entity name="salesorder" from="bsd_warehouseto" to="bsd_warehouseentityid" alias="ab">',
                            '<filter type="and">',
                                    '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + orderId + '" />',
                            '</filter>',
                         '</link-entity>',
                        '</entity>',
                '</fetch>'].join(""));
    if (rs.length > 0) {
        console.log(rs);
        warehouse = rs[0];
    } else {
        warehouse = null;
    }
    return warehouse;
}
function getWarehouseQuantityOfProduct(productId, warehouseId) {
    var quantity = 0;
    var xml = [
        '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_warehourseproduct">',
            '<attribute name="bsd_warehourseproductid" />',
            '<attribute name="bsd_name" />',
            '<attribute name="bsd_quantity" />',
            '<attribute name="createdon" />',
            '<order attribute="bsd_name" descending="false" />',
            '<filter type="and">',
              '<condition attribute="bsd_warehouses" operator="eq" uitype="bsd_warehouseentity" value="' + warehouseId + '" />',
              '<condition attribute="bsd_product" operator="eq" uitype="product" value="' + productId + '" />',
            '</filter>',
          '</entity>',
        '</fetch>',
    ].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            quantity = rs[0].attributes.bsd_quantity.value;
        }
    }, function (er) { });
    return quantity;
    debugger;
}
function getQuantityProductInOrder(orderId, productId) {
    // Lấy quantity đặt hàng trong order của product này. !
    var total_quantity = 0;
    var xml = [
          '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
             '<entity name="salesorderdetail">',
                   '<attribute name="productid" />',
                   '<attribute name="quantity" />',
                   '<attribute name="salesorderdetailid" />',
                   '<order attribute="productid" descending="false" />',
                   '<filter type="and">',
                   '<condition attribute="productid" operator="eq" uitype="product" value="' + productId + '" />',
                   '</filter>',
                   '<link-entity name="salesorder" from="salesorderid" to="salesorderid" alias="ae">',
                   '<filter type="and">',
                   '<condition attribute="salesorderid" operator="eq" value="' + orderId + '" />',
                   '</filter>',
               '</link-entity>',
             '</entity>',
         '</fetch>',
    ].join("");
    var rs = CrmFetchKit.FetchSync(xml);
    if (rs.length > 0) {
        total_quantity = rs[0].attributes.quantity.value;
    }
    return total_quantity;
    debugger;
}

function create_suborder(Selected) {
    var ListId = "";
    for (var i = 0; i < Selected.length; i++) {
        ListId += Selected[i].Id + "@";
    }
    ListId = ListId.substr(0, ListId.length - 1);
    var params = new Array();
    params[0] = { name: 'ListId', type: 'string', value: ListId };
    ExecuteAction(Xrm.Page.data.entity.getId(), "salesorder", "bsd_Action_CreateSubOrder", params, function (result) {
        console.log(result);
        if (result != null && result.status != null) {
            if (result.status == "success") {
                console.log(result.data.Returnid);
                debugger;
                if (result.data.Returnid.value != "") {
                    Xrm.Utility.openEntityForm("bsd_suborder", result.data.Returnid.value);
                } else {
                    alert("Đã tạo đủ số lượng ! vui lòng chọn sản phẩm khác !");
                }
            } else if (result.status == "error") {
                alert(result.data);
            } else {
                alert(result.data);
            }
        }
    });
}
function BtnSuborderEnableRule() {
    //if (formType() == 2) {
    //    return true;
    //} else {
    //    return false;
    //}
    return false;
}
function BtnCreateSubOrder() {
    Xrm.Page.data.save().then(function () {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="salesorderdetail">',
        '<attribute name="salesorderdetailid" />',
        '<filter type="and">',
          '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + getId() + '" />',
          '<condition attribute="bsd_remainingquantity" operator="gt" value="0" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                if (confirm("Are you sure to create new sub order ?")) {
                    ExecuteAction(Xrm.Page.data.entity.getId(), "salesorder", "bsd_Action_CreateSubOrder", null, function (result) {
                        if (result != null && result.status != null) {
                            if (result.status == "success") {
                                reload_page();
                            } else if (result.status == "error") {
                                alert(result.data);
                            } else {
                                alert(result.data);
                            }
                        }
                    });
                }
            } else {
                alert("Không sản phẩm nào còn dư số lượng để tạo suborder");
            }
        });
    });

    
}
function BtnCreateSubOrderEnableRule() {
    return true;
}
function BtnCreateContractOrderEnable() {
    return false;
}

// bấm xóa sản phẩm trên subgrid product
function BtnDeleteProduct() {
    var result = false;
    var quote = getValue("quoteid");
    if (quote != null) {
        alert("Cannot delete this product.");
    } else {

        // nếu từ new. check suborder
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_suborder">',
            '<attribute name="bsd_suborderid" />',
            '<attribute name="bsd_name" />',
            '<filter type="and">',
              '<condition attribute="bsd_order" operator="eq" uitype="salesorder" value="' + getId() + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(xml);
        if (rs.length == 0) {
            result = true;
        } else {
            alert("Đã tạo suborder");
        }
    }
    setTimeout(function () {
        //Xrm.Page.ui.refreshRibbon();
    }, 3000);
    return result;
}

// bấm nút thêm mới trên subgrid, associated view.
function BtnAddNewProduct() {
    var result = false;
    var quote = Xrm.Page.getAttribute("quoteid").getValue();
    if (quote != null) {
        result = false;
    } else {
        result = true;
    }
    return result;
}
//End

//Mr.Đăng
//Description: Disable when Delivery At The Port is No
function set_DisableDeliveryPort() {
    var deliveryport = getValue("bsd_deliveryattheport");
    if (deliveryport == "0") {
        setVisible(["bsd_port", "bsd_addressport"], false);
    }
    if (deliveryport == "1") {
        setVisible(["bsd_port", "bsd_addressport"], true);
        ChooseAccountPort();
    }
}
//Author:Mr.Đăng
//Description: Choose Account in Port
function ChooseAccountPort() {
    var fetchxml = ['<fetch mapping="logical" version="1.0">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="accountid" />',
                    '<filter>',
                    '<condition attribute="bsd_accounttype" operator="eq" value="861450003" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");

    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
							   "<row name='result'  " + "id='accountid'>  " +
							   "<cell name='name'   " + "width='200' />  " +
							   "<cell name='createdon'    " + "width='100' />  " +
							   "</row>   " +
							"</grid>";
    getControl("bsd_port").addCustomView(getControl("bsd_port").getDefaultView(), "account", "Port", fetchxml, layoutXml, true);
}
//End

//Mr.Phong
//Description:gan gia tri cho field ship to address khi form la edit
function setshiptoaddressonload() {
    var customerb = Xrm.Page.getAttribute("bsd_shiptoaccount").getValue();
    var xmlshitoaddress = [];
    var xmladdressshiptoaccount = [];
    if (customerb != null) {
        var viewIdshiptoaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameshiptoaddress = "bsd_address";
        var viewDisplayNameshiptoaddress = "Ship to Address View";
        fetch(xmlshitoaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customerb[0].id]);
        var layoutXmlshiptoaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";

        CrmFetchKit.Fetch(xmlshitoaddress.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdshiptoaddress, entityNameshiptoaddress, viewDisplayNameshiptoaddress, xmlshitoaddress.join(""), layoutXmlshiptoaddress, true);
            }
            else {
                var viewIdaddressshiptoaccount = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameaddressshiptoaccount = "bsd_address";
                var viewDisplayNameaddressshiptoaccount = "Address Ship To Account";
                fetch(xmladdressshiptoaccount, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                    ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customerb[0].id]);
                var layoutXmladdressshiptoaccount = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdaddressshiptoaccount, entityNameaddressshiptoaccount, viewDisplayNameaddressshiptoaccount, xmladdressshiptoaccount.join(""), layoutXmladdressshiptoaccount, true);
            }

        }, function (er) {
            console.log(er.message)
        });
    }
}
//Description:gan gia tri cho cac field address khi form la edit
function set_addressonload() {
    var type = Xrm.Page.getAttribute("bsd_type").getValue();
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    var xmlcontactcustomer = [];
    var xmladdresscustomer = [];
    var xmladdressinvoiceaccount = [];
    if (type != 861450001) {
        if (customer != null) {
            var viewIdaddresscustomer = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
            var entityNameaddresscustomer = "bsd_address";
            var viewDisplayNameaddresscustomer = "Address Customer";
            xmladdresscustomer.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xmladdresscustomer.push("<entity name='bsd_address'>");
            xmladdresscustomer.push("<attribute name='bsd_addressid' />");
            xmladdresscustomer.push("<attribute name='bsd_name' />");
            xmladdresscustomer.push("<attribute name='createdon' />");
            xmladdresscustomer.push("<attribute name='bsd_purpose' />");
            xmladdresscustomer.push("<order attribute='bsd_name' descending='false' />");
            xmladdresscustomer.push("<filter type='and'>");
            xmladdresscustomer.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='" + customer[0].id + "' />");
            xmladdresscustomer.push("<condition attribute='statecode' operator='eq' value='0' />");
            xmladdresscustomer.push("</filter>");
            xmladdresscustomer.push("</entity>");
            xmladdresscustomer.push("</fetch>");
            var layoutXmladdresscustomer = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='bsd_addressid'>  " +
                  "<cell name='bsd_name'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                    "<cell name='bsd_purpose'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>   ";
            Xrm.Page.getControl("bsd_address").addCustomView(viewIdaddresscustomer, entityNameaddresscustomer, viewDisplayNameaddresscustomer, xmladdresscustomer.join(""), layoutXmladdresscustomer, true);
            var viewIdcontactcustomer = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
            var entityNamecontactcustomer = "contact";
            var viewDisplayNamecontactcustomer = "Contact Customer";
            xmlcontactcustomer.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xmlcontactcustomer.push("<entity name='contact'>");
            xmlcontactcustomer.push("<attribute name='fullname'/>");
            xmlcontactcustomer.push("<attribute name='telephone1'/>")
            xmlcontactcustomer.push("<attribute name='contactid'/>");
            xmlcontactcustomer.push("<order attribute='fullname' descending='true' />");
            xmlcontactcustomer.push("<filter type='and'>");
            xmlcontactcustomer.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
            xmlcontactcustomer.push("<condition attribute='bsd_contacttype' operator='eq' value='861450000' />");
            xmlcontactcustomer.push("</filter>");
            xmlcontactcustomer.push("</entity>");
            xmlcontactcustomer.push("</fetch>");
            var layoutXmlcontactcustomer = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='contactid'>  " +
                             "<cell name='fullname'   " + "width='200' />  " +
                             "<cell name='telephone1'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
            Xrm.Page.getControl("bsd_contact").addCustomView(viewIdcontactcustomer, entityNamecontactcustomer, viewDisplayNamecontactcustomer, xmlcontactcustomer.join(""), layoutXmlcontactcustomer, true);
        }
        if (invoicenameaccount != null) {
            var viewIdinvoiceaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
            var entityNameinvoiceaddress = "bsd_address";
            var viewDisplayNameinvoiceaddress = "Address Invoice Account";
            xmladdressinvoiceaccount.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xmladdressinvoiceaccount.push("<entity name='bsd_address'>");
            xmladdressinvoiceaccount.push("<attribute name='bsd_addressid' />");
            xmladdressinvoiceaccount.push("<attribute name='bsd_name' />");
            xmladdressinvoiceaccount.push("<attribute name='createdon' />");
            xmladdressinvoiceaccount.push("<attribute name='bsd_purpose' />");
            xmladdressinvoiceaccount.push("<order attribute='bsd_name' descending='false' />");
            xmladdressinvoiceaccount.push("<filter type='and'>");
            xmladdressinvoiceaccount.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='" + invoicenameaccount[0].id + "' />");
            xmladdressinvoiceaccount.push("<condition attribute='statecode' operator='eq' value='0' />");
            xmladdressinvoiceaccount.push("</filter>");
            xmladdressinvoiceaccount.push("</entity>");
            xmladdressinvoiceaccount.push("</fetch>");
            var layoutXmlinvoiceaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='bsd_addressid'>  " +
                  "<cell name='bsd_name'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                    "<cell name='bsd_purpose'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>   ";
            Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdinvoiceaddress, entityNameinvoiceaddress, viewDisplayNameinvoiceaddress, xmladdressinvoiceaccount.join(""), layoutXmlinvoiceaddress, true);
        }
    }
}
//Description:gan gia tri cho fied ship to address
function set_addressfromcustomerb() {
    Xrm.Page.getControl("bsd_shiptoaddress").setDisabled(false);
    var customerb = Xrm.Page.getAttribute("bsd_shiptoaccount").getValue();
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    Xrm.Page.getControl("bsd_shiptoaddress").removePreSearch(presearch_Shiptoaddress);
    var xmlshiptoaddress = [];
    var xmladdresscustomerreceipt = [];
    if (customerb != null) {
        var viewIdshiptoaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameshiptoaddress = "bsd_address";
        var viewDisplayNameshiptoaddress = "Ship to address view";
        xmlshiptoaddress.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xmlshiptoaddress.push("<entity name='bsd_address'>");
        xmlshiptoaddress.push("<attribute name='bsd_addressid' />");
        xmlshiptoaddress.push("<attribute name='bsd_name' />");
        xmlshiptoaddress.push("<attribute name='createdon' />");
        xmlshiptoaddress.push("<attribute name='bsd_purpose' />");
        xmlshiptoaddress.push("<order attribute='bsd_name' descending='false' />");
        xmlshiptoaddress.push("<filter type='and'>");
        xmlshiptoaddress.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='" + customerb[0].id + "' />");
        xmlshiptoaddress.push("<condition attribute='bsd_purpose' operator='eq' uitype='account ' value='861450002' />");
        xmlshiptoaddress.push("<condition attribute='statecode' operator='eq' value='0' />");
        xmlshiptoaddress.push("</filter>");
        xmlshiptoaddress.push("</entity>");
        xmlshiptoaddress.push("</fetch>");
        var layoutXmlshiptoaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlshiptoaddress.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                fieldshiptoaddress.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.bsd_name.value,
                    entityType: rs[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdshiptoaddress, entityNameshiptoaddress, viewDisplayNameshiptoaddress, xmlshiptoaddress.join(""), layoutXmlshiptoaddress, true);
            }
            else {
                var viewIdaddresscustomerreceipt = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameaddresscustomerreceipt = "bsd_address";
                var viewDisplayNameaddresscustomerreceipt = "Address Customer Receipt";
                xmladdresscustomerreceipt.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xmladdresscustomerreceipt.push("<entity name='bsd_address'>");
                xmladdresscustomerreceipt.push("<attribute name='bsd_addressid' />");
                xmladdresscustomerreceipt.push("<attribute name='bsd_name' />");
                xmladdresscustomerreceipt.push("<attribute name='createdon' />");
                xmladdresscustomerreceipt.push("<attribute name='bsd_purpose' />");
                xmladdresscustomerreceipt.push("<order attribute='bsd_name' descending='false' />");
                xmladdresscustomerreceipt.push("<filter type='and'>");
                xmladdresscustomerreceipt.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='" + customerb[0].id + "' />");
                xmladdresscustomerreceipt.push("<condition attribute='statecode' operator='eq' value='0' />");
                xmladdresscustomerreceipt.push("</filter>");
                xmladdresscustomerreceipt.push("</entity>");
                xmladdresscustomerreceipt.push("</fetch>");
                var layoutXmladdresscustomerreceipt = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdaddresscustomerreceipt, entityNameaddresscustomerreceipt, viewDisplayNameaddresscustomerreceipt, xmladdresscustomerreceipt.join(""), layoutXmladdresscustomerreceipt, true);
            }

        }, function (er) {
            console.log(er.message)
        });
    }
    else {
        clear_Shiptoaddress();
        Xrm.Page.getAttribute("bsd_shiptoaddress").setValue(null);
    }
}
function validation() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    if (customer == null) {
        Xrm.Page.getAttribute("bsd_address").setValue(null);
    }
    if (invoicenameaccount == null) {
        Xrm.Page.getAttribute("bsd_addressinvoiceaccount").setValue(null);
    }
}
//Description:set invoice address from invoice name account
function set_invoiceaddress() {
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    var addressinvoicenameaccount = Xrm.Page.getAttribute("bsd_addressinvoiceaccount");
    Xrm.Page.getControl("bsd_addressinvoiceaccount").removePreSearch(presearch_Addressinvoiceaccount);
    var xmladdressinvoice = [];
    var xmladdressinvoiceaccount = [];
    var xmlinvoiceaccount = [];
    if (invoicenameaccount != null) {
        Xrm.Page.getControl("bsd_addressinvoiceaccount").setDisabled(false);
        var viewIdaddressinvoice = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameaddressinvoice = "bsd_address";
        var viewDisplayNameaddressinvoice = "Address Invoice View";
        fetch(xmladdressinvoice, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Invoice" + "%", 1, "0", 2, invoicenameaccount[0].id]);
        var layoutXmladdressinvoice = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmladdressinvoice.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    addressinvoicenameaccount.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdaddressinvoice, entityNameaddressinvoice, viewDisplayNameaddressinvoice, xmladdressinvoice.join(""), layoutXmladdressinvoice, true);
            }
            else {
                var viewIdaddressinvoiceaccount = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameaddressinvoiceaccount = "bsd_address";
                var viewDisplayNameaddressinvoiceaccount = "Address Invoice Account View";
                fetch(xmladdressinvoiceaccoun, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                         ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, invoicenameaccount[0].id]);
                var layoutXmladdressinvoiceaccount = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                CrmFetchKit.Fetch(xmladdressinvoiceaccount.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        if (rs1[0].attributes.bsd_name != null) {
                            if (rs1[0].attributes.bsd_name != null) {
                                addressinvoicenameaccount.setValue([{
                                    id: rs1[0].Id,
                                    name: rs1[0].attributes.bsd_name.value,
                                    entityType: rs1[0].logicalName
                                }]);
                            }
                            Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdaddressinvoiceaccount, entityNameaddressinvoiceaccount, viewDisplayNameaddressinvoiceaccount, xmladdressinvoiceaccount.join(""), layoutXmladdressinvoiceaccount, true);
                        }
                    }
                    else {
                        addressinvoicenameaccount.setValue(null);
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdaddressinvoiceaccount, entityNameaddressinvoiceaccount, viewDisplayNameaddressinvoiceaccount, xmladdressinvoiceaccount.join(""), layoutXmladdressinvoiceaccount, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }

        },
    function (er) {
        console.log(er.message)
    });
        xmlinvoiceaccount.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xmlinvoiceaccount.push("<entity name='account'>");
        xmlinvoiceaccount.push("<attribute name='name' />");
        xmlinvoiceaccount.push("<attribute name='primarycontactid' />");
        xmlinvoiceaccount.push("<attribute name='telephone1' />");
        xmlinvoiceaccount.push("<attribute name='accountid' />");
        xmlinvoiceaccount.push("<attribute name='bsd_taxregistration' />");
        xmlinvoiceaccount.push("<attribute name='fax' />");
        xmlinvoiceaccount.push("<attribute name='accountnumber'/>");
        xmlinvoiceaccount.push("<attribute name='emailaddress1'/>");
        xmlinvoiceaccount.push("<attribute name='bsd_paymentterm'/>");
        xmlinvoiceaccount.push("<attribute name='bsd_paymentmethod'/>");
        xmlinvoiceaccount.push("<order attribute='createdon' descending='false' />");
        xmlinvoiceaccount.push("<filter type='and'>");
        xmlinvoiceaccount.push("<condition attribute='accountid' operator='eq' uitype='account ' value='" + invoicenameaccount[0].id + "' />");
        xmlinvoiceaccount.push("</filter>");
        xmlinvoiceaccount.push("</entity>");
        xmlinvoiceaccount.push("</fetch>");
        CrmFetchKit.Fetch(xmlinvoiceaccount.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.accountnumber != null) {
                    Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(rs[0].attributes.accountnumber.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(null);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(rs[0].attributes.bsd_taxregistration.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                }
            }
        },
    function (er) {
        console.log(er.message)
    });
    }
    else {
        clear_Addressinvoiceaccount();
        addressinvoicenameaccount.setValue(null);
        Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(null);
        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
        Xrm.Page.getControl("bsd_addressinvoiceaccount").setDisabled(true);

    }
}
function appearordisappearsectionBHSonload() {
    var type = Xrm.Page.getAttribute("bsd_type").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    var xml7 = [];
    var xml8 = [];
    var xml9 = [];
    //BHS  
    var fieldtrader = Xrm.Page.getAttribute("bsd_trader").getValue();
    var ordername = Xrm.Page.getAttribute("bsd_ordername");
    var name = Xrm.Page.getAttribute("name");
    var economicno = Xrm.Page.getAttribute("bsd_economiccontractno").getValue();
    Xrm.Page.getControl("bsd_trader").removePreSearch(presearch_Loadtrader);
    Xrm.Page.getControl("bsd_addresstrader").removePreSearch(presearch_LoadAddressTrader);
    Xrm.Page.getControl("bsd_bankaccounttrader").removePreSearch(presearch_LoadBankAccountTrader);
    Xrm.Page.getControl("bsd_representative").removePreSearch(presearch_Loadrepresentative);
    //customer
    var fieldcontact = Xrm.Page.getAttribute("bsd_contact").getValue();
    var fieldcustomer = Xrm.Page.getAttribute("customerid").getValue();
    Xrm.Page.getControl("bsd_bankaccountcustomer").removePreSearch(presearch_LoadBankAccountCustomer);
    Xrm.Page.getControl("bsd_contact").removePreSearch(presearch_Loadrepresentativecustomer);
    if (type == 861450000)//type đơn hàng
    {
        Xrm.Page.ui.tabs.get("summary_tab").sections.get("section_contactinformation").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_order").sections.get("section_bhs").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_order").sections.get("section_packingandproductquality").setVisible(false);
        setVisible(["bsd_bankaccountcustomer", "bsd_dateauthorizedcustomer", "bsd_numberauthorizedcustomer", "bsd_brandcustomer"
            , "bsd_bankgroupcustomer", "bsd_bankaccountnumbercustomer", "bsd_positioncustomer", "bsd_dateauthorizedcustomer", "bsd_numberauthorizedcustomer", "bsd_deliveryattheport", "bsd_deliverymethod", "shipto_freighttermscode"], false);
    }
    if (type == 861450001)//type HĐKT 
    {
        Xrm.Page.ui.tabs.get("tab_order").sections.get("section_bhs").setVisible(true);
        Xrm.Page.ui.tabs.get("tab_order").sections.get("section_packingandproductquality").setVisible(true);
        Xrm.Page.data.entity.attributes.forEach(function (attribute, index) {
            var control = Xrm.Page.getControl(attribute.getName());
            if (control != null) {
                control.setDisabled(true);
                setDisabled(["bsd_bankaccountcustomer", "bsd_contact", "bsd_dateauthorizedcustomer", "bsd_numberauthorizedcustomer"
                    , "bsd_contracttype", "bsd_daystartcontract", "bsd_dayendcontract", "bsd_trader", "bsd_bankaccounttrader"
                    , "bsd_representative", "bsd_dateauthorized", "bsd_numberauthorized", "bsd_packingspecification", "bsd_principleoftally", "bsd_addresstrader", "bsd_pono"], false);
                setVisible(["ordernumber", "bsd_economiccontract", "bsd_deliveryattheport", "bsd_deliverymethod", "shipto_freighttermscode"], false);
                setRequired(["bsd_contracttype", "bsd_daystartcontract", "bsd_dayendcontract"], "required");
            }
        })
        ordername.setValue(economicno);
        name.setValue(economicno);      
        if (fieldtrader != null) {
            //load thong tin cua BHS
            var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
            var entityName = "account";
            var viewDisplayName = "Account View";
            fetch(xml, "account", ["accountid", "name", "primarycontactid", "telephone1", "bsd_taxregistration", "fax"], ["name"], false, null,
                ["bsd_accounttype", "statecode"], ["eq", "eq"], [0, "861450005", 1, "0"]);
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='accountid'>  " +
                      "<cell name='name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                      "<cell name='primarycontactid'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    Xrm.Page.getControl("bsd_trader").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
                }
                else {
                    clear_Loadtrader();
                }
            },
                function (er) {
                    console.log(er.message)
                });
            Xrm.Page.getControl("bsd_trader").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            //load thong tin dia chi cua BHS
            var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
            var entityName1 = "bsd_address";
            var viewDisplayName1 = "Address View";
            fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null, ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, fieldtrader[0].id]);
            var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='bsd_addressid'>  " +
                       "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_account'    " + "width='100' />  " +
                       "</row>   " +
                    "</grid>   ";
            CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                if (rs1.length > 0) {
                    Xrm.Page.getControl("bsd_addresstrader").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                }
                else {
                    var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                    var entityName2 = "bsd_address";
                    var viewDisplayName2 = "Address View";
                    fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                   ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, fieldtrader[0].id]);
                    var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_addressid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";

                    CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                        if (rs2.length > 0) {
                            Xrm.Page.getControl("bsd_addresstrader").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
                        }
                        else {
                            clear_LoadAddressTrader();
                        }
                    },
                function (er) {
                    console.log(er.message)
                });
                }
            },
                function (er) {
                    console.log(er.message)
                });
            //load bank account cua BHS      
            var viewId3 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
            var entityName3 = "bsd_bankaccount";
            var viewDisplayName3 = "Bank Account View";
            fetch(xml3, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
                ["bsd_account", "statecode"], ["eq", "eq"], [0, fieldtrader[0].id, 1, "0"]);
            var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
            "<row name='result'  " + "id='bsd_bankaccountid'>  " +
            "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
            "<cell name='bsd_brand'    " + "width='100' />  " +
            "<cell name='bsd_bankaccount'    " + "width='100' />  " +
            "<cell name='bsd_account'    " + "width='100' />  " +
            "<cell name='bsd_bankgroup'    " + "width='100' />  " +
            "</row>   " +
            "</grid>   ";
            CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                if (rs3.length > 0) {
                    Xrm.Page.getControl("bsd_bankaccounttrader").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
                }
                else {
                    clear_LoadBankAccountTrader();
                }
            },
                function (er) {
                    console.log(er.message)
                });
            //load nguoi dai dien      
            var viewId5 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
            var entityName5 = "contact";
            var viewDisplayName5 = "Contact View";
            xml5.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml5.push("<entity name='contact'>");
            xml5.push("<attribute name='fullname'/>");
            xml5.push("<attribute name='telephone1'/>")
            xml5.push("<attribute name='contactid'/>");
            xml5.push("<order attribute='fullname' descending='true' />");
            xml5.push("<filter type='and'>");
            xml5.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + fieldtrader[0].id + "' />");
            xml5.push("</filter>");
            xml5.push("</entity>");
            xml5.push("</fetch>");
            var layoutXml5 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='contactid'>  " +
                     "<cell name='fullname'   " + "width='200' />  " +
                     "<cell name='telephone1'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
            CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                if (rs5.length > 0) {
                    Xrm.Page.getControl("bsd_representative").addCustomView(viewId5, entityName5, viewDisplayName5, xml5.join(""), layoutXml5, true);
                }
                else {
                    clear_Loadrepresentative();
                }
            },
        function (er) {
            console.log(er.message)
        });
            //load bank account cua customer      
            var viewId6 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
            var entityName6 = "bsd_bankaccount";
            var viewDisplayName6 = "Bank Account View";
            fetch(xml6, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
                ["bsd_account", "statecode"], ["eq", "eq"], [0, fieldcustomer[0].id, 1, "0"]);
            var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
            "<row name='result'  " + "id='bsd_bankaccountid'>  " +
            "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
            "<cell name='bsd_brand'    " + "width='100' />  " +
            "<cell name='bsd_bankaccount'    " + "width='100' />  " +
            "<cell name='bsd_account'    " + "width='100' />  " +
            "<cell name='bsd_bankgroup'    " + "width='100' />  " +
            "</row>   " +
            "</grid>   ";
            CrmFetchKit.Fetch(xml6.join(""), true).then(function (rs6) {
                if (rs6.length > 0) {
                    Xrm.Page.getControl("bsd_bankaccountcustomer").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
                }
                else {
                    clear_LoadBankAccountCustomer();
                }
            },
                function (er) {
                    console.log(er.message)
                });
            //load nguoi dai dien customer         
            var viewId8 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
            var entityName8 = "contact";
            var viewDisplayName8 = "Contact View";
            xml8.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml8.push("<entity name='contact'>");
            xml8.push("<attribute name='fullname'/>");
            xml8.push("<attribute name='telephone1'/>")
            xml8.push("<attribute name='contactid'/>");
            xml8.push("<order attribute='fullname' descending='true' />");
            xml8.push("<filter type='and'>");
            xml8.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + fieldcustomer[0].id + "' />");
            xml8.push("<condition attribute='bsd_contacttype' operator='eq' value='861450000' />");
            xml8.push("</filter>");
            xml8.push("</entity>");
            xml8.push("</fetch>");
            var layoutXml8 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='contactid'>  " +
                     "<cell name='fullname'   " + "width='200' />  " +
                     "<cell name='telephone1'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
            CrmFetchKit.Fetch(xml8.join(""), true).then(function (rs8) {
                if (rs8.length > 0) {
                    if (Xrm.Page.getAttribute("bsd_positioncustomer").getValue() == null && fieldcontact != null) {
                        fetch(xml9, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                        ["contactid"], ["eq"], [0, fieldcontact[0].id, 1]);
                        CrmFetchKit.Fetch(xml9.join(""), true).then(function (rs9) {
                            if (rs9.length > 0) {
                                Xrm.Page.getAttribute("bsd_positioncustomer").setValue(rs9[0].attributes.jobtitle.value);
                            }
                            else {
                                Xrm.Page.getAttribute("bsd_positioncustomer").setValue(null);
                            }
                        },
                    function (er) {
                        console.log(er.message)
                    });
                    }
                    Xrm.Page.getControl("bsd_contact").addCustomView(viewId8, entityName8, viewDisplayName8, xml8.join(""), layoutXml8, true);
                }
                else {
                    clear_Loadrepresentativecustomer();
                }
            },
        function (er) {
            console.log(er.message)
        });
        }
    }
}
function filter_customerprincipalcontract() {
    //var fromdate = Xrm.Page.getAttribute("bsd_fromdatehdkt").getValue();
    //var fieldprincipalcontract = Xrm.Page.getAttribute("bsd_principalcontract");
    //if (fromdate != null) {
    //    var year = fromdate.getFullYear() + "";
    //    if (fromdate.getMonth() + 1 <= 9) {
    //        var month = "0" + (fromdate.getMonth() + 1) + "";
    //    }
    //    else {
    //        var month = (fromdate.getMonth() + 1) + "";
    //    }
    //    var dayy = fromdate.getDate();
    //    var dateFormat = year + "-" + month + "-" + dayy;
    //}
    //var customer = Xrm.Page.getAttribute("bsd_customername").getValue();
    //var xml = [];
    //if (fromdate != null && customer != null) {
    //    fetch(xml, "bsd_principalcontract", ["bsd_principalcontractid", "bsd_name", "createdon", "bsd_code", "bsd_fromdate", "bsd_todate"], ["bsd_name"], false, null,
    //         ["bsd_account", "bsd_fromdate"], ["eq", "on-or-before"], [0, customer[0].id, 1, dateFormat]);
    //    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
    //        if (rs.length > 0) {
    //            var i
    //            for (i = 0; i < rs.length; i++) {
    //                var enddate = rs[i].attributes.bsd_todate.value;
    //                var yearr = enddate.getFullYear() + "";
    //                if (enddate.getMonth() + 1 <= 9) {
    //                    var monthh = "0" + (enddate.getMonth() + 1) + "";
    //                }
    //                else {
    //                    var monthh = (enddate.getMonth() + 1) + "";
    //                }
    //                var dayyy = enddate.getDate() + "";
    //                if (year <= yearr) {
    //                    if (month < monthh) {
    //                        if (fieldprincipalcontract.getValue() == null) {
    //                            fieldprincipalcontract.setValue([{
    //                                id: '{' + rs[i].Id.toUpperCase() + '}',
    //                                name: rs[i].attributes.bsd_name.value,
    //                                entityType: rs[i].logicalName
    //                            }]);
    //                            Xrm.Page.getAttribute("bsd_principalcontractno").setValue(rs[i].attributes.bsd_code.value);
    //                        }
    //                        Xrm.Page.getAttribute("bsd_hidehdkt").setValue("khong");
    //                        break;
    //                    }
    //                    if (month == monthh) {
    //                        if (dayy < dayyy) {
    //                            if (fieldprincipalcontract.getValue() == null) {
    //                                fieldprincipalcontract.setValue([{
    //                                    id: '{' + rs[i].Id.toUpperCase() + '}',
    //                                    name: rs[i].attributes.bsd_name.value,
    //                                    entityType: rs[i].logicalName
    //                                }]);
    //                                Xrm.Page.getAttribute("bsd_principalcontractno").setValue(rs[i].attributes.bsd_code.value);
    //                            }
    //                            Xrm.Page.getAttribute("bsd_hidehdkt").setValue("khong");
    //                            break;
    //                        }
    //                    }
    //                }
    //                Xrm.Page.getAttribute("bsd_hidehdkt").setValue("co");
    //            }
    //            if (Xrm.Page.getAttribute("bsd_hidehdkt").getValue() == "co") {
    //                fieldprincipalcontract.setValue(null);
    //                Xrm.Page.getAttribute("bsd_principalcontractno").setValue(null);
    //            }

    //        }
    //    },
    //        function (er) {
    //            console.log(er.message)
    //        });
    //}
}
//Description:action change filed trader
function action_changefieldtrader() {
    var trader = Xrm.Page.getAttribute("bsd_trader").getValue();
    var fieldaddress = Xrm.Page.getAttribute("bsd_addresstrader");
    var fieldbankaccount = Xrm.Page.getAttribute("bsd_bankaccounttrader");
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgrouptrader");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbertrader");
    var fieldbrand = Xrm.Page.getAttribute("bsd_brandtrader");
    var fieldcontact = Xrm.Page.getAttribute("bsd_representative");
    Xrm.Page.getControl("bsd_addresstrader").removePreSearch(presearch_LoadAddressTrader);
    Xrm.Page.getControl("bsd_bankaccounttrader").removePreSearch(presearch_LoadBankAccountTrader);
    Xrm.Page.getControl("bsd_representative").removePreSearch(presearch_Loadrepresentative);
    var xmlbhsinformation = [];
    var xmlbhsaddressbusiness = [];
    var xmlbhsaddress = [];
    var xmlbhsbankaccount = [];
    var xmlbhsrepresentative = [];
    var xmlbhsrepresentativejobtitle = [];
    var xmlbhsrepresentativenone = [];
    var xmlbhsrepresentativejobtitlenone = [];
    var xmlbhsrepresentativeall = [];
    if (trader != null) {
        //load thong tin cua BHS  
        fetch(xmlbhsinformation, "account", ["accountid", "name", "primarycontactid", "telephone1", "bsd_taxregistration", "fax"]
            , ["name"], false, null, ["accountid"], ["eq"]
            , [0, trader[0].id, 1]);
        CrmFetchKit.Fetch(xmlbhsinformation.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.telephone1 != null) {
                    Xrm.Page.getAttribute("bsd_telephonetrader").setValue(rs[0].attributes.telephone1.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_telephonetrader").setValue(null);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistrationtrader").setValue(rs[0].attributes.bsd_taxregistration.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_taxregistrationtrader").setValue(null);
                }
                if (rs[0].attributes.fax != null) {
                    Xrm.Page.getAttribute("bsd_faxtrader").setValue(rs[0].attributes.fax.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_faxtrader").setValue(null);
                }
            }
        },
                 function (er) {
                     console.log(er.message)
                 });
        //load thong tin dia chi cua BHS       
        var viewIdbhsaddressbusiness = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNamebhsaddressbusiness = "bsd_address";
        var viewDisplayNamebhsaddressbusiness = "BHS Address Business View";
        fetch(xmlbhsaddressbusiness, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
            ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, trader[0].id]);
        var layoutXmlbhsaddressbusiness = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
             "<row name='result'  " + "id='bsd_addressid'>  " +
             "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
             "<cell name='bsd_account'    " + "width='100' />  " +
             "</row>   " +
          "</grid>   ";
        CrmFetchKit.Fetch(xmlbhsaddressbusiness.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    fieldaddress.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
                else {
                    fieldaddress.setValue(null);
                }
                Xrm.Page.getControl("bsd_addresstrader").addCustomView(viewIdbhsaddressbusiness, entityNamebhsaddressbusiness, viewDisplayNamebhsaddressbusiness, xmlbhsaddressbusiness.join(""), layoutXmlbhsaddressbusiness, true);
            }
            else {
                var viewIdbhsaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNamebhsaddress = "bsd_address";
                var viewDisplayNamebhsaddress = "BHS Address View";
                fetch(xmlbhsaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, fieldtrader[0].id]);
                var layoutXmlbhsaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_addressid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";

                CrmFetchKit.Fetch(xmlbhsaddress.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        Xrm.Page.getControl("bsd_addresstrader").addCustomView(viewIdbhsaddress, entityNamebhsaddress, viewDisplayNamebhsaddress, xmlbhsaddress.join(""), layoutXmlbhsaddress, true);
                    }
                    else {
                        clear_LoadAddressTrader();
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
        },
         function (er) {
             console.log(er.message)
         });
        //load bank account cua BHS      
        var viewIdbhsbankaccount = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
        var entityNamebhsbankaccount = "bsd_bankaccount";
        var viewDisplayNamebhsbankaccount = "Bank Account Customer View";
        fetch(xmlbhsbankaccount, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_account", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
        var layoutXmlbhsbankaccount = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
        "<row name='result'  " + "id='bsd_bankaccountid'>  " +
        "<cell name='bsd_name'   " + "width='200' />  " +
        "<cell name='createdon'    " + "width='100' />  " +
        "<cell name='bsd_brand'    " + "width='100' />  " +
        "<cell name='bsd_bankaccount'    " + "width='100' />  " +
        "<cell name='bsd_account'    " + "width='100' />  " +
        "<cell name='bsd_bankgroup'    " + "width='100' />  " +
        "</row>   " +
        "</grid>   ";
        CrmFetchKit.Fetch(xmlbhsbankaccount.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    fieldbankaccount.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                    fieldbankgroup.setValue([{
                        id: rs[0].attributes.bsd_bankgroup.guid,
                        name: rs[0].attributes.bsd_bankgroup.name,
                        entityType: rs[0].attributes.bsd_bankgroup.logicalName
                    }]);
                    fieldbankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                    fieldbrand.setValue(rs[0].attributes.bsd_brand.value);
                }
                else {
                    fieldbankaccount.setValue(null);
                    fieldbankgroup.setValue(null);
                    fieldbankaccountnumber.setValue(null);
                    fieldbrand.setValue(null);
                }
                Xrm.Page.getControl("bsd_bankaccounttrader").addCustomView(viewIdbhsbankaccount, entityNamebhsbankaccount, viewDisplayNamebhsbankaccount, xmlbhsbankaccount.join(""), layoutXmlbhsbankaccount, true);
            }
            else {
                fieldbankaccount.setValue(null);
                fieldbrand.setValue(null);
                fieldbankaccountnumber.setValue(null);
                clear_LoadBankAccountTrader();
            }
        },
            function (er) {
                console.log(er.message)
            });
        //load nguoi dai dien

        xmlbhsrepresentative.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xmlbhsrepresentative.push("<entity name='account'>");
        xmlbhsrepresentative.push("<attribute name='name' />");
        xmlbhsrepresentative.push("<attribute name='primarycontactid'/>");
        xmlbhsrepresentative.push("<attribute name='telephone1'/>");
        xmlbhsrepresentative.push("<attribute name='accountid'/>");
        xmlbhsrepresentative.push("<order attribute='name' descending='false'/> ");
        xmlbhsrepresentative.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
        xmlbhsrepresentative.push("<attribute name='jobtitle'/>");
        xmlbhsrepresentative.push("<filter type='and'>");
        xmlbhsrepresentative.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + trader[0].id + "'/>");
        xmlbhsrepresentative.push("</filter>");
        xmlbhsrepresentative.push("</link-entity>");
        xmlbhsrepresentative.push("</entity>");
        xmlbhsrepresentative.push("</fetch>");
        CrmFetchKit.Fetch(xmlbhsrepresentative.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                fieldcontact.setValue([{
                    id: rs[0].attributes.primarycontactid.guid,
                    name: rs[0].attributes.primarycontactid.name,
                    entityType: 'contact'
                }]);
                var viewIdbhsrepresentative = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityNamebhsrepresentative = "account";
                var viewDisplayNamebhsrepresentative = "BHS Contact View";
                fetch(xmlbhsrepresentativeall, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                   ["parentcustomerid", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
                var layoutXmlbhsrepresentative = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='contactid'>  " +
                         "<cell name='fullname'   " + "width='200' />  " +
                         "<cell name='telephone1'    " + "width='100' />  " +
                         "</row>   " +
                      "</grid>   ";
                Xrm.Page.getControl("bsd_representative").addCustomView(viewIdbhsrepresentative, entityNamebhsrepresentative, viewDisplayNamebhsrepresentative, xmlbhsrepresentativeall.join(""), layoutXmlbhsrepresentative, true);
                fetch(xmlbhsrepresentativejobtitle, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
           ["contactid"], ["eq"], [0, rs[0].attributes.primarycontactid.guid, 1]);
                CrmFetchKit.Fetch(xmlbhsrepresentativejobtitle.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        Xrm.Page.getAttribute("bsd_position").setValue(rs[0].attributes.jobtitle.value);
                    }
                    else {
                        Xrm.Page.getAttribute("bsd_position").setValue(null);
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
            else {
                var viewIdbhsrepresentative = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityNamebhsrepresentative = "contact";
                var viewDisplayNamebhsrepresentative = "BHS Contact View";
                fetch(xmlbhsrepresentativenone, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                    ["parentcustomerid", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
                var layoutXmlbhsrepresentative = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                CrmFetchKit.Fetch(xmlbhsrepresentativenone.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        fieldcontact.setValue([{
                            id: rs[0].Id,
                            name: rs[0].attributes.fullname.value,
                            entityType: 'contact'
                        }]);
                        Xrm.Page.getControl("bsd_representative").addCustomView(viewIdbhsrepresentative, entityNamebhsrepresentative, viewDisplayNamebhsrepresentative, xmlbhsrepresentativenone.join(""), layoutXmlbhsrepresentative, true);
                        fetch(xmlbhsrepresentativejobtitlenone, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                        ["fullname"], ["eq"], [0, rs[0].attributes.fullname.value, 1]);
                        CrmFetchKit.Fetch(xmlbhsrepresentativejobtitlenone.join(""), true).then(function (rs) {
                            if (xmlbhsrepresentativejobtitlenone.length > 0) {
                                Xrm.Page.getAttribute("bsd_position").setValue(rs[0].attributes.jobtitle.value);
                            }
                            else {
                                Xrm.Page.getAttribute("bsd_position").setValue(null);
                            }
                        },
                    function (er) {
                        console.log(er.message)
                    });
                    }
                    else {
                        Xrm.Page.getAttribute("bsd_position").setValue(null);
                        clear_Loadrepresentative();
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
        },
            function (er) {
                console.log(er.message)
            });
    }
    else {
        fieldaddress.setValue(null);
        fieldbankaccount.setValue(null);
        fieldbankgroup.setValue(null);
        fieldbankaccountnumber.setValue(null);
        fieldbrand.setValue(null);
        fieldcontact.setValue(null);
        set_value(["bsd_telephonetrader", "bsd_taxregistrationtrader", "bsd_faxtrader", "bsd_position"], null);
        clear_LoadAddressTrader();
        clear_LoadBankAccountTrader();
        clear_Loadrepresentative();
    }
}
//Description:lay du lieu bank account,bank group,brand tu trader
function get_bankaccountnumberandbankgrouptrader() {
    var bankaccounttrader = Xrm.Page.getAttribute("bsd_bankaccounttrader").getValue();
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgrouptrader");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbertrader");
    var fieldbrand = Xrm.Page.getAttribute("bsd_brandtrader");
    var xmlbankaccounttrader = [];
    if (bankaccounttrader != null) {
        //load bank account cua BHS            
        fetch(xmlbankaccounttrader, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_bankaccountid", "statecode"], ["eq", "eq"], [0, bankaccounttrader[0].id, 1, "0"]);
        CrmFetchKit.Fetch(xmlbankaccounttrader.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_bankgroup != null) {
                    fieldbankgroup.setValue([{
                        id: rs[0].attributes.bsd_bankgroup.guid,
                        name: rs[0].attributes.bsd_bankgroup.name,
                        entityType: rs[0].attributes.bsd_bankgroup.logicalName
                    }]);
                }
                if (rs[0].attributes.bsd_bankaccount != null) {
                    fieldbankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                }
                if (rs[0].attributes.bsd_brand != null) {
                    fieldbrand.setValue(rs[0].attributes.bsd_brand.value);
                }
            }
            else {
                fieldbankgroup.setValue(null);
                fieldbrand.setValue(null);
                fieldbankaccountnumber.setValue(null);
            }
        },
            function (er) {
                console.log(er.message)
            });
    }
    else {
        fieldbankgroup.setValue(null);
        fieldbrand.setValue(null);
        fieldbankaccountnumber.setValue(null);
    }
}
//Description:lay thong tin chuc vu nguoi dai dien tu BHS
function get_thongtinchucvu() {
    var present = Xrm.Page.getAttribute("bsd_representative").getValue();
    var xmljobtitlebhs = [];
    if (present != null) {
        fetch(xmljobtitlebhs, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
              ["contactid"], ["eq"], [0, present[0].id, 1]);
        CrmFetchKit.Fetch(xmljobtitlebhs.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.jobtitle != null) {
                    Xrm.Page.getAttribute("bsd_position").setValue(rs[0].attributes.jobtitle.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_position").setValue(null);
                }
            }
        },
         function (er) {
             console.log(er.message)
         });
    }
    if (present == null) {
        Xrm.Page.getAttribute("bsd_position").setValue(null);
    }
}
//Description:lay du lieu bank account,bank group,brand tu customer
function get_bankaccountnumberandbankgroupcustomer() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var fieldbankaccount = Xrm.Page.getAttribute("bsd_bankaccountcustomer").getValue();
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgroupcustomer");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbercustomer");
    var fieldbrand = Xrm.Page.getAttribute("bsd_brandcustomer");
    var xmlbankaccountcustomer = [];
    if (fieldbankaccount != null) {
        fetch(xmlbankaccountcustomer, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_bankaccountid"], ["eq"], [0, fieldbankaccount[0].id, 1]);
        CrmFetchKit.Fetch(xmlbankaccountcustomer.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                fieldbankgroup.setValue([{
                    id: rs[0].attributes.bsd_bankgroup.guid,
                    name: rs[0].attributes.bsd_bankgroup.name,
                    entityType: rs[0].attributes.bsd_bankgroup.logicalName
                }]);
                fieldbankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                fieldbrand.setValue(rs[0].attributes.bsd_brand.value);
            }
        },
      function (er) {
          console.log(er.message)
      });
    }
    else {
        fieldbankgroup.setValue(null);
        fieldbankaccountnumber.setValue(null);
        fieldbrand.setValue(null);

    }
}
//Description:lay thong tin chuc vu nguoi dai dien tu customer
function get_thongtinchucvucustomer() {
    var type = Xrm.Page.getAttribute("bsd_type").getValue();
    if (type == 861450001) {
        var present = Xrm.Page.getAttribute("bsd_contact").getValue();
        var xmljobtitlecustomer = [];
        if (present != null) {
            fetch(xmljobtitlecustomer, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                  ["contactid"], ["eq"], [0, present[0].id, 1]);
            CrmFetchKit.Fetch(xmljobtitlecustomer.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    if (rs[0].attributes.jobtitle != null) {
                        Xrm.Page.getAttribute("bsd_positioncustomer").setValue(rs[0].attributes.jobtitle.value);
                    }
                }
                else {
                    Xrm.Page.getAttribute("bsd_positioncustomer").setValue(null);
                }
            },
             function (er) {
                 console.log(er.message)
             });
        }
        if (present == null) {
            Xrm.Page.getAttribute("bsd_positioncustomer").setValue(null);
        }
    }
}
//Description:filter type customer and distributor
function set_customer() {
    Xrm.Page.getAttribute("bsd_priceoftransportationn").setSubmitMode("always");
    Xrm.Page.getAttribute("bsd_unitshipping").setSubmitMode("always");
    var viewIdtypecusotmer = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    var entityNametypecustomer = "account";
    var viewDisplayNametypecustomer = "Account Type Customer View";
    var xmltypecustomer = [];
    xmltypecustomer.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xmltypecustomer.push("<entity name='account'>");
    xmltypecustomer.push("<attribute name='accountid' />");
    xmltypecustomer.push("<attribute name='name'/>");
    xmltypecustomer.push("<order attribute='name' descending='false' />");
    xmltypecustomer.push("<filter type='and' >");
    xmltypecustomer.push("<condition attribute='bsd_accounttype' operator='in'>");
    xmltypecustomer.push("<value>");
    xmltypecustomer.push("861450000");
    xmltypecustomer.push("</value>");
    xmltypecustomer.push("<value>");
    xmltypecustomer.push("100000000");
    xmltypecustomer.push("</value>");
    xmltypecustomer.push("</condition>");
    xmltypecustomer.push("<condition attribute='statecode' operator='eq' value='0'/>");
    xmltypecustomer.push("</filter>");
    xmltypecustomer.push("</entity>");
    xmltypecustomer.push("</fetch>");
    var layoutXmltypecustomer = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='accountid'>  " +
                       "<cell name='name'    " + "width='200' />  " +
                       "</row>   " +
                    "</grid>   ";
    Xrm.Page.getControl("customerid").addCustomView(viewIdtypecusotmer, entityNametypecustomer, viewDisplayNametypecustomer, xmltypecustomer.join(""), layoutXmltypecustomer, true);
    Xrm.Page.getControl("bsd_invoicenameaccount").addCustomView(viewIdtypecusotmer, entityNametypecustomer, viewDisplayNametypecustomer, xmltypecustomer.join(""), layoutXmltypecustomer, true);
    Xrm.Page.getControl("bsd_shiptoaccount").addCustomView(viewIdtypecusotmer, entityNametypecustomer, viewDisplayNametypecustomer, xmltypecustomer.join(""), layoutXmltypecustomer, true);
}
//Description:lay thong tin khach hang
function get_customerinformation() {
    Xrm.Page.getAttribute("bsd_paymentterm").setSubmitMode("always");
    Xrm.Page.getAttribute("bsd_paymentmethod").setSubmitMode("always");
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    var fielddefault = Xrm.Page.getAttribute("bsd_warehouseto");
    var fieldaddress = Xrm.Page.getAttribute("bsd_address");
    var fieldpaymentterm = Xrm.Page.getAttribute("bsd_paymentterm");
    var fieldpaymentmethod = Xrm.Page.getAttribute("bsd_paymentmethod");
    var fieldsaletaxgroup = Xrm.Page.getAttribute("bsd_saletaxgroup");
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount");
    var addressinvoicenameaccount = Xrm.Page.getAttribute("bsd_addressinvoiceaccount");
    var partnerscompany = Xrm.Page.getAttribute("bsd_shiptoaccount");
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var xmlaccount = [];
    var xmlaccountaddressbusiness = [];
    var xmlaccountaddress = [];
    var xmlinvoiceaddress = [];
    var xmlshitoaccountadress = [];
    var xmlshiptoaddress = [];
    var xmlreceiptaddress = [];
    if (customer != null) {
        //Account Information
        fetch(xmlaccount, "account", ["name", "primarycontactid", "telephone1", "accountid"
                              , "bsd_taxregistration", "fax", "accountnumber", "emailaddress1"
                              , "bsd_paymentterm", "bsd_paymentmethod", "transactioncurrencyid", "bsd_saletaxgroup"], ["createdon"], false, null,
                              ["accountid"], ["eq"], [0, customer[0].id, 1]);
        CrmFetchKit.Fetch(xmlaccount.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                invoicenameaccount.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.name.value,
                    entityType: rs[0].logicalName
                }]);
                partnerscompany.setValue([{
                    id: customer[0].id,
                    name: customer[0].name,
                    entityType: 'account'
                }]);
                Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(rs[0].attributes.accountnumber.value);
                if (rs[0].attributes.telephone1 != null) {
                    Xrm.Page.getAttribute("bsd_telephone").setValue(rs[0].attributes.telephone1.value);
                }
                if (rs[0].attributes.telephone1 == null) {
                    Xrm.Page.getAttribute("bsd_telephone").setValue(null);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(rs[0].attributes.bsd_taxregistration.value);
                }
                if (rs[0].attributes.bsd_taxregistration == null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                }
                if (rs[0].attributes.fax != null) {
                    Xrm.Page.getAttribute("bsd_fax").setValue(rs[0].attributes.fax.value);
                }
                if (rs[0].attributes.fax == null) {
                    Xrm.Page.getAttribute("bsd_fax").setValue(null);
                }
                if (rs[0].attributes.accountnumber != null) {
                    var no = rs[0].attributes.accountnumber.value;
                    Xrm.Page.getAttribute("bsd_customercode").setValue(no);
                }
                if (rs[0].attributes.accountnumber == null) {
                    Xrm.Page.getAttribute("bsd_customercode").setValue(null);
                }
                if (rs[0].attributes.emailaddress1 != null) {
                    Xrm.Page.getAttribute("bsd_mail").setValue(rs[0].attributes.emailaddress1.value);
                }
                if (rs[0].attributes.emailaddress1 == null) {
                    Xrm.Page.getAttribute("bsd_mail").setValue(null);
                }
                if (rs[0].attributes.bsd_paymentterm != null) {
                    //fieldpaymentterm.setValue(rs[0].attributes.bsd_paymentterm.name);
                    fieldpaymentterm.setValue([{
                        id: rs[0].attributes.bsd_paymentterm.guid,
                        name: rs[0].attributes.bsd_paymentterm.name,
                        entityType: 'bsd_paymentterm'
                    }]);
                }
                if (rs[0].attributes.bsd_paymentterm == null) {
                    //fieldpaymentterm.setValue(rs[0].attributes.bsd_paymentterm.name);
                    fieldpaymentterm.setValue(null);
                }
                if (rs[0].attributes.bsd_paymentmethod != null) {
                    //fieldpaymentmethod.setValue(rs[0].attributes.bsd_paymentmethod.name);
                    fieldpaymentmethod.setValue([{
                        id: rs[0].attributes.bsd_paymentmethod.guid,
                        name: rs[0].attributes.bsd_paymentmethod.name,
                        entityType: 'bsd_methodofpayment'
                    }]);
                }
                if (rs[0].attributes.bsd_paymentmethod == null) {
                    //fieldpaymentmethod.setValue(rs[0].attributes.bsd_paymentmethod.name);
                    fieldpaymentmethod.setValue(null);
                }
                if (rs[0].attributes.bsd_saletaxgroup != null) {
                    fieldsaletaxgroup.setValue([{
                        id: rs[0].attributes.bsd_saletaxgroup.guid,
                        name: rs[0].attributes.bsd_saletaxgroup.name,
                        entityType: 'bsd_saletaxgroup'
                    }]);
                }
                else if (rs[0].attributes.bsd_saletaxgroup == null) {
                    fieldsaletaxgroup.setValue(null);
                }
            }
        },
        function (er) {
            console.log(er.message)
        });
        //Account Address
        var viewIdaccountaddressbusiness = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameaccountaddressbusiness = "bsd_address";
        var viewDisplayNameaccountaddressbusiness = "test";
        fetch(xmlaccountaddressbusiness, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
        ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, customer[0].id]);
        var layoutXmlaccountaddressbusiness = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlaccountaddressbusiness.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    fieldaddress.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
            }
            else {
                var viewIdaccountaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameaccountaddress = "bsd_address";
                var viewDisplayNameaccountaddress = "test";
                fetch(xmlaccountaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXmlaccountaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";

                CrmFetchKit.Fetch(xmlaccountaddress.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        if (rs[0].attributes.bsd_name != null) {
                            fieldaddress.setValue([{
                                id: rs[0].Id,
                                name: rs[0].attributes.bsd_name.value,
                                entityType: rs[0].logicalName
                            }]);
                            Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
                        }
                    }
                    if (rs.length == 0) {
                        fieldaddress.setValue(null);
                        Xrm.Page.getControl("bsd_address").addCustomView(viewIdaccountaddress, entityNameaccountaddress, viewDisplayNameaccountaddress, xmlaccountaddress.join(""), layoutXmlaccountaddress, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }
        },
        function (er) {
            console.log(er.message)
        });
        //Ship To address
        var viewIdshiptoaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameshiptoaddress = "bsd_address";
        var viewDisplayNameshitoaddress = "test";
        fetch(xmlshiptoaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
          ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customer[0].id]);
        var layoutXmlshiptoaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlshiptoaddress.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                fieldshiptoaddress.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.bsd_name.value,
                    entityType: rs[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdshiptoaddress, entityNameshiptoaddress, viewDisplayNameshitoaddress, xmlshiptoaddress.join(""), layoutXmlshiptoaddress, true);
            }
            else {
                var viewIdreceiptaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNamereceiptaddress = "bsd_address";
                var viewDisplayNamereceiptaddress = "Receipt Account Address View";
                fetch(xmlreceiptaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                    ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXmlreceiptaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdreceiptaddress, entityNamereceiptaddress, viewDisplayNamereceiptaddress, xmlreceiptaddress.join(""), layoutXmlreceiptaddress, true);
            }

        }, function (er) {
            console.log(er.message)
        });
        //Invoice Address
        var viewIdinvoiceaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameinvoiceaddress = "bsd_address";
        var viewDisplayNameinvoiceaddress = "Inovoice Address View";
        fetch(xmlinvoiceaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
         ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Invoice" + "%", 1, "0", 2, customer[0].id]);
        var layoutXmlinvoiceaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlinvoiceaddress.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    addressinvoicenameaccount.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdinvoiceaddress, entityNameininvoiceaddress, viewDisplayNameinvoiceaddress, xmlinvoiceaddress.join(""), layoutXmlinvoiceaddress, true);
            }
            else {
                var viewIdshitoaccountadress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameshitoaccountadress = "bsd_address";
                var viewDisplayNameshitoaccountadress = "Shio To Account View";
                fetch(xmlshitoaccountadress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                   ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXmlshitoaccountadress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                CrmFetchKit.Fetch(xmlshitoaccountadress.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        if (rs[0].attributes.bsd_name != null) {
                            if (rs[0].attributes.bsd_name != null) {
                                addressinvoicenameaccount.setValue([{
                                    id: rs[0].Id,
                                    name: rs[0].attributes.bsd_name.value,
                                    entityType: rs[0].logicalName
                                }]);
                            }
                            Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdshitoaccountadress, entityNameshitoaccountadress, viewDisplayNameshitoaccountadress, xmlshitoaccountadress.join(""), layoutXmlshitoaccountadress, true);
                        }
                    }
                    else {
                        addressinvoicenameaccount.setValue(null);
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdshitoaccountadress, entityNameshitoaccountadress, viewDisplayNameshitoaccountadress, xmlshitoaccountadress.join(""), layoutXmlshitoaccountadress, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }

        },
    function (er) {
        console.log(er.message)
    });
    }
    else {
        setNull(["bsd_telephone"
            , "bsd_taxregistration", "bsd_fax", "bsd_customercode"
            , "bsd_mail", "bsd_paymentterm", "bsd_paymentmethod"
            , "bsd_contact", "bsd_invoiceaccount", "bsd_invoicenameaccount", "bsd_addressinvoiceaccount", "bsd_shiptoaccount"])
        fieldaddress.setValue(null);
        fieldshiptoaddress.setValue(null);
        fielddefault.setValue(null);
    }
}
//Description:load contact theo account
function get_contact() {
    var contactdefault = Xrm.Page.getAttribute("bsd_contact");
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var xmlcontactaccount = [];
    var xmlallcontactaccount = [];
    if (customer != null) {
        xmlcontactaccount.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xmlcontactaccount.push("<entity name='account'>");
        xmlcontactaccount.push("<attribute name='name' />");
        xmlcontactaccount.push("<attribute name='primarycontactid'/>");
        xmlcontactaccount.push("<attribute name='telephone1'/>");
        xmlcontactaccount.push("<attribute name='accountid'/>");
        xmlcontactaccount.push("<order attribute='name' descending='false'/> ");
        xmlcontactaccount.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
        xmlcontactaccount.push("<filter type='and'>");
        xmlcontactaccount.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "'/>");
        xmlcontactaccount.push("<condition attribute='bsd_contacttype' operator='eq' value='861450000' />");
        xmlcontactaccount.push("</filter>");
        xmlcontactaccount.push("</link-entity>");
        xmlcontactaccount.push("</entity>");
        xmlcontactaccount.push("</fetch>");
        CrmFetchKit.Fetch(xmlcontactaccount.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                contactdefault.setValue([{
                    id: rs[0].attributes.primarycontactid.guid,
                    name: rs[0].attributes.primarycontactid.name,
                    entityType: 'contact'
                }]);
                var viewIdallcontactaccount = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityNameallcontactaccount = "contact";
                var viewDisplayNameallcontactaccount = "test";
                xmlallcontactaccount.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xmlallcontactaccount.push("<entity name='contact'>");
                xmlallcontactaccount.push("<attribute name='fullname'/>");
                xmlallcontactaccount.push("<attribute name='telephone1'/>")
                xmlallcontactaccount.push("<attribute name='contactid'/>");
                xmlallcontactaccount.push("<order attribute='fullname' descending='true' />");
                xmlallcontactaccount.push("<filter type='and'>");
                xmlallcontactaccount.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
                xmlallcontactaccount.push("<condition attribute='bsd_contacttype' operator='eq' value='861450000' />");
                xmlallcontactaccount.push("</filter>");
                xmlallcontactaccount.push("</entity>");
                xmlallcontactaccount.push("</fetch>");
                var layoutXmlallcontactaccount = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_contact").addCustomView(viewIdallcontactaccount, entityNameallcontactaccount, viewDisplayNameallcontactaccount, xmlallcontactaccount.join(""), layoutXmlallcontactaccount, true);
            }
            else {
                var viewIdallcontactaccount = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityNameallcontactaccount = "contact";
                var viewDisplayNameallcontactaccount = "Contact Account View";
                xmlallcontactaccount.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xmlallcontactaccount.push("<entity name='contact'>");
                xmlallcontactaccount.push("<attribute name='fullname'/>");
                xmlallcontactaccount.push("<attribute name='telephone1'/>")
                xmlallcontactaccount.push("<attribute name='contactid'/>");
                xmlallcontactaccount.push("<order attribute='fullname' descending='true' />");
                xmlallcontactaccount.push("<filter type='and'>");
                xmlallcontactaccount.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
                xmlallcontactaccount.push("<condition attribute='bsd_contacttype' operator='eq' value='861450000' />");
                xmlallcontactaccount.push("</filter>");
                xmlallcontactaccount.push("</entity>");
                xmlallcontactaccount.push("</fetch>");
                var layoutXmlallcontactaccount = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_contact").addCustomView(viewIdallcontactaccount, entityNameallcontactaccount, viewDisplayNameallcontactaccount, xmlallcontactaccount.join(""), layoutXmlallcontactaccount, true);
                CrmFetchKit.Fetch(xmlallcontactaccount.join(""), true).then(function (rs) {
                    if (rs.length > 0 && rs[0].attributes.fullname != null) {
                        contactdefault.setValue([{
                            id: rs[0].Id,
                            name: rs[0].attributes.fullname.value,
                            entityType: rs[0].logicalName
                        }]);
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//End

//Description:Clear look up field
function clear_shippingpricelistname() {
    getControl("bsd_shippingpricelistname").addPreSearch(presearch_shippingpricelistname);
}
function presearch_shippingpricelistname() {
    getControl("bsd_shippingpricelistname").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Loadtrader() {
    getControl("bsd_tradersup").addPreSearch(presearch_Loadtrader);
}
function presearch_Loadtrader() {
    getControl("bsd_tradersup").addPreSearch(presearch_Loadtrader);
}
function clear_LoadAddressTrader() {
    getControl("bsd_addresstrader").addPreSearch(presearch_LoadAddressTrader);
}
function presearch_LoadAddressTrader() {
    getControl("bsd_addresstrader").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_LoadAddressTrader() {
    getControl("bsd_addresstrader").addPreSearch(presearch_LoadAddressTrader);
}
function presearch_LoadAddressTrader() {
    getControl("bsd_addresstrader").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_LoadBankAccountTrader() {
    getControl("bsd_bankaccounttrader").addPreSearch(presearch_LoadBankAccountTrader);
}
function presearch_LoadBankAccountTrader() {
    getControl("bsd_bankaccounttrader").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Loadrepresentative() {
    getControl("bsd_representative").addPreSearch(presearch_Loadrepresentative);
}
function presearch_Loadrepresentative() {
    getControl("bsd_representative").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_shippingpricelistname() {
    getControl("bsd_shippingpricelistname").addPreSearch(presearch_shippingpricelistname);
}
function presearch_shippingpricelistname() {
    getControl("bsd_shippingpricelistname").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_LoadBankAccountCustomer() {
    getControl("bsd_bankaccountcustomer").addPreSearch(presearch_LoadBankAccountCustomer);
}
function presearch_LoadBankAccountCustomer() {
    getControl("bsd_bankaccountcustomer").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Loadrepresentativecustomer() {
    getControl("bsd_contact").addPreSearch(presearch_Loadrepresentativecustomer);
}
function presearch_Loadrepresentativecustomer() {
    getControl("bsd_contact").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Shiptoaddress() {
    getControl("bsd_shiptoaddress").addPreSearch(presearch_Shiptoaddress);
}
function presearch_Shiptoaddress() {
    getControl("bsd_shiptoaddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Addressinvoiceaccount() {
    getControl("bsd_addressinvoiceaccount").addPreSearch(presearch_Addressinvoiceaccount);
}
function presearch_Addressinvoiceaccount() {
    getControl("bsd_addressinvoiceaccount").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function change_daystartcontract() {
    debugger;
  
    //create by nudo on 2017-03-15
    clearNotification("bsd_daystartcontract");
    clearNotification("bsd_dayendcontract");
    var from = getValue("bsd_daystartcontract");
    var to = getValue("bsd_dayendcontract");
    if (from != null) {
        //var orderid = getValue("salesorderid");
        var xml1 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="salesorder">',
                      '  <attribute name="name" />',
                      '  <attribute name="salesorderid" />',
                      '  <order attribute="name" descending="false" />',
                      '  <filter type="and">',
                      '    <condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + getId() + '" />',
                      '  </filter>',
                      '  <link-entity name="quote" from="quoteid" to="quoteid" alias="ab">',
                      '    <attribute name="effectiveto" />',
                       '    <attribute name="effectivefrom" />',
                      '  </link-entity>',
                      '</entity>',
                    '</fetch>'].join('');
        console.log(xml1);
        var rs = CrmFetchKit.Fetch(xml1);
        alert(rs.length);
        if (rs.length > 0) {
            var effectivefrom = rs[0].getValue("ab.effectivefrom");
            alert(effectivefrom)
            console.log(effectivefrom);
            if (effectivefrom != null && effectivefrom > from) {
                setNotification("bsd_dayendcontract", "The Day From Start Contract to cannot occur after the Day Effective");
            }

        }


        if (from > to) {
            setNotification("bsd_daystartcontract", "The Day Start Contract cannot occur before the Day End Contract");
        }
        if (to > from) {
            setNotification("bsd_daystartcontract", "The Day End Contract cannot occur after the Day Start Contract");
        }
    }
    if (to != null) {
        //var orderid = getValue("salesorderid");
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="salesorder">',
                      '  <attribute name="name" />',
                      '  <attribute name="salesorderid" />',
                      '  <order attribute="name" descending="false" />',
                      '  <filter type="and">',
                      '    <condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + getId() + '" />',
                      '  </filter>',
                      '  <link-entity name="quote" from="quoteid" to="quoteid" alias="ab">',
                      '    <attribute name="effectiveto" />',
                      '  </link-entity>',
                      '</entity>',
                    '</fetch>'].join('');
        console.log(xml);
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {

                var effectiveto = rs[0].getValue("ab.effectiveto");
                console.log(effectiveto);
                if (effectiveto != null && effectiveto > to) {

                    setNotification("bsd_dayendcontract", "The Day End Start Contract to cannot occur after the Day Effective");
                }

            }

        });
    }
}
//Description:Check_date
function check_datenew() {
    debugger;
    var xmlquoteid = [];
    var fromdate = getValue("bsd_daystartcontract");
    var todate = getValue("bsd_dayendcontract");
    var quoteid = getValue("quoteid");
    if (quoteid != null && fromdate != null) {
        fetch(xmlquoteid, "quote", ["name", "customerid", "statuscode", "totalamount", "quoteid", "effectiveto", "effectivefrom"], ["name"], false, null,
         ["quoteid"], ["eq"], [0, quoteid[0].id, 1]);
        CrmFetchKit.Fetch(xmlquoteid.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (fromdate < rs[0].attributes.effectivefrom.value) {
                    var message = "The From Date cannot occur before effectvie from date";
                    var type = "INFO"; //INFO, WARNING, ERROR
                    var id = "Info1"; //Notification Id
                    var time = 3000; //Display time in milliseconds
                    //Display the notification
                    Xrm.Page.ui.setFormNotification(message, type, id);

                    //Wait the designated time and then remove
                    setTimeout(
                        function () {
                            Xrm.Page.ui.clearFormNotification(id);
                        },
                        time
                    );
                    setValue("bsd_daystartcontract", rs[0].attributes.effectivefrom.value);
                }


            }
        }, function (er) {
            console.log(er.message);
        });
    }
    if (todate < fromdate) {
        setNotification("bsd_dayendcontract", "The To Date Date cannot occur after From Date");
    }
    else {
        Xrm.Page.getControl("bsd_dayendcontract").clearNotification();
    }
}