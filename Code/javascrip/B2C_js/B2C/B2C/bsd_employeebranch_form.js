﻿//Author:Mr.Đăng
//Description:Check Branch
function Check_branch() {
    var employee = getValue("bsd_employee");
    var branch = getValue("bsd_branch");
    var id = getId();
    if (branch != null) {
        setValue("bsd_name", branch[0].name);
        if (employee != null) {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_employeebranch">',
                            '<attribute name="bsd_employeebranchid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_employee" operator="eq" uitype="bsd_employee" value="'+employee[0].id+'" />',
                            '<condition attribute="bsd_branch" operator="eq" uitype="bsd_branch" value="' + branch[0].id + '" />',
                            '<condition attribute="bsd_employeebranchid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_branch", "Đã tồn tại, vui lòng kiểm tra lại.");
            }
            else {
                clearNotification("bsd_branch");
            }
        }
    }
    else {
        setNull("bsd_name");
    }
}