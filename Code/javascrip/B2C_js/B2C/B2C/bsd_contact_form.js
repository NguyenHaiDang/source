//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    set_DisableOutletType();
    setDisabled_Account();
    Xrm.Page.getControl("parentcustomerid").addPreSearch(addFilter);
    if (formType() == 2) {
        Load_Contact();
        LK_Employee_Account_Change(false);
    } else {
        clear_employee();
    }
}
function addFilter() {
    var customerAccountFilter = "<filter type='and'><condition attribute='contactid' operator='null' /></filter>";
    Xrm.Page.getControl("parentcustomerid").addCustomFilter(customerAccountFilter, "contact");
}
/*
    Diệm
    Set value khi chọn older code.
*/
function LK_Contact_change() {
    var contactolder = getValue("bsd_contactolder");
    if (contactolder != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="contact">',
                        '    <attribute name="fullname" />',
                        '    <attribute name="telephone1" />',
                        '    <attribute name="contactid" />',
                        '    <attribute name="bsd_customercode" />',
                        '    <order attribute="fullname" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="contactid" operator="eq" uitype="contact" value="' + contactolder[0].id + '" />',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '      <condition attribute="bsd_status" operator="eq" value="1" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setValue("lastname", rs[0].attributes.fullname.value);
            if (getValue("bsd_customercode") == null) {
                setValue("bsd_customercode", rs[0].attributes.bsd_customercode.value);
            }
        }
    } else {
        setNull(["lastname", "bsd_customercode"]);
    }
}
/*
    Diệm : Khóa contact older khi save
*/
function autosave() {
    setDisabled(["bsd_contactolder", "lastname"], true);
}
/*
    Diệm load con dang tact đang hoat động
*/
function Load_Contact() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '  <entity name="contact">',
                      '    <attribute name="fullname" />',
                      '    <attribute name="telephone1" />',
                      '    <attribute name="contactid" />',
                      '    <attribute name="bsd_customercode" />',
                      '    <order attribute="fullname" descending="false" />',
                      '    <filter type="and">',
                      '      <condition attribute="statecode" operator="eq" value="0" />',
                      '      <condition attribute="bsd_status" operator="eq" value="1" />',
                      '    </filter>',
                      '  </entity>',
                      '</fetch>'].join('');
    LookUp_After_Load("bsd_contactolder", "contact", "fullname", "Contact", fetchxml);
}
/*
    ngày 1/2/2017: Load Employee theo account.
*/
function LK_Employee_Account_Change(reset) {
    if (reset != false) setNull("bsd_employee");
    var account = getValue("parentcustomerid");
    getControl("bsd_employee").removePreSearch(presearch_employee);
    if (account != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                        '  <entity name="bsd_employee">',
                        '    <attribute name="bsd_name" />',
                        '   <attribute name="createdon" />',
                        '    <attribute name="bsd_account" />',
                        '    <attribute name="bsd_employeeid" />',
                        '    <order attribute="bsd_name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '      <condition attribute="bsd_status" operator="eq" value="1" />',
                        '    </filter>',
                        '    <link-entity name="bsd_employeeaccount" from="bsd_employee" to="bsd_employeeid" alias="ac">',
                        '      <filter type="and">',
                        '        <condition attribute="bsd_account" operator="eq" uitype="account" value="'+account[0].id+'" />',
                        '      </filter>',
                        '    </link-entity>',
                        '  </entity>',
                        '</fetch>'].join('');
        LookUp_After_Load("bsd_employee", "bsd_employee", "bsd_name", "Employee", fetchxml);
    } else if (reset != false) {
        clear_employee();
    }
}
function clear_employee() {
    getControl("bsd_employee").addPreSearch(presearch_employee);
}
function presearch_employee() {
    getControl("bsd_employee").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}



/*Diệm: check trung branch*/
function Check_Same_Branch() {
    var cuscode = getValue("bsd_customercode");
    var branch = getValue("bsd_branch");
    if (cuscode != null && branch != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="contact">',
                        '    <attribute name="fullname" />',
                        '    <attribute name="telephone1" />',
                        '    <attribute name="contactid" />',
                        '    <order attribute="fullname" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="bsd_customercode" operator="eq" value="' + cuscode + '" />',
                        '      <condition attribute="bsd_branch" operator="eq" uiname="Cafe" uitype="bsd_branch" value="' + branch[0].id + '" />',
                        '      <condition attribute="bsd_status" operator="eq" value="1" />',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setNotification("bsd_branch", "Ngành hàng đã tồn tại trong contact này. Vui lòng chọn ngành khác.");

        } else {
            clearNotification("bsd_branch");
        }
    }
}

//Author:Mr.Phong
//Description:library for this page
function set_value(a, b) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getAttribute(a[i]).setValue(b);
    }
}
function get_value(a) {
    var b = [];
    for (var i = 0; i < a.length; i++) {
        var j = Xrm.Page.getAttribute(a[i]).getValue();
        b.push(j);
    }
    return b;
}
function set_requirelevel(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.getAttribute(a[i]).setRequiredLevel("none");
        }
        if (b == 1) {
            Xrm.Page.getAttribute(a[i]).setRequiredLevel("required");
        }
    }
}
function set_disable(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.getControl(a[i]).setDisabled(false);
        }
        if (b == 1) {
            Xrm.Page.getControl(a[i]).setDisabled(true);
        }
    }
}
function set_visible(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.ui.controls.get(a[i]).setVisible(false);
        }
        if (b == 1) {
            Xrm.Page.ui.controls.get(a[i]).setVisible(true);
        }
    }
}
function set_notification(a, b) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getControl(a[i]).setNotification("" + b);
    }
}
function clear_nofitication(a) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getControl(a[i]).clearNotification();
    }
}
function fetch(xml, entity, attribute, orderattribute, descendingvalue, ascendingvalue, conditionattribute, operator, conditionvalue) {
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='" + entity + "'>");
    for (var i = 0; i < attribute.length; i++) {
        xml.push("<attribute name='" + attribute[i] + "' />");
    }
    if (descendingvalue != null) {
        xml.push("<order attribute='" + orderattribute + "' descending='" + descendingvalue + "' />");
    }
    if (ascendingvalue != null) {
        xml.push("<order attribute='" + orderattribute + "' descending='" + ascendingvalue + "' />");
    }
    xml.push("<filter type='and'>");
    for (var j = 0; j < conditionattribute.length; j++) {
        if (operator[j] == "inn") {
            xml.push("<condition attribute='" + conditionattribute[j] + "' operator='in'>");
            for (var k = 0; k < conditionvalue.length; k++) {
                if (conditionvalue[k] === parseInt(conditionvalue[k], 10)) {
                    if (j == conditionvalue[k]) {
                        for (var p = 1; p < conditionvalue.length; p++) {
                            if (conditionvalue[k + p] === parseInt(conditionvalue[k + p], 10) || conditionvalue[k + p] == null) {
                                xml.push("</condition>");
                                break;
                            }
                            else {
                                xml.push("<value>" + conditionvalue[k + p] + "");
                                xml.push("</value>");
                            }
                        }
                    }
                }
            }
        }
        else {
            for (var k = 0; k < conditionvalue.length; k++) {
                if (conditionvalue[k] === parseInt(conditionvalue[k], 10)) {
                    if (j == conditionvalue[k]) {
                        for (var p = 1; p < conditionvalue.length; p++) {
                            if (conditionvalue[k + p] === parseInt(conditionvalue[k + p], 10) || conditionvalue[k + p] == null) {
                                break;
                            }
                            else {
                                xml.push("<condition attribute='" + conditionattribute[j] + "' operator='" + operator[j] + "'  value='" + conditionvalue[k + p] + "' />");
                            }
                        }
                    }
                }
            }
        }
    }
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
}
function set_fullname() {
    debugger;

    if (Xrm.Page.getAttribute("bsd_fullname").getValue() != null && Xrm.Page.getAttribute("bsd_fullname").getValue() != "undefined") {
        {
            //Xrm.Page.getAttribute("firstname").setValue("1");
            Xrm.Page.getAttribute("lastname").setValue(Xrm.Page.getAttribute("bsd_fullname").getValue());
            Xrm.Page.getAttribute("lastname").setRequiredLevel("none");
            Xrm.Page.getAttribute("fullname").setRequiredLevel("none");
        }
    }
}

// JavaScript source code

// Global
var diachi = null;
var phuong = null;
var quanhuyen = null;
var tinhthanh = null;
var quocgia = null;

function OnLoad() {
    debugger;

    //Load dia chi
    if (Xrm.Page.getAttribute("bsd_diachi").getValue() != null)
        MakeAddress();
}

// Quoc Gia OnChange
function Onchange_QuocGia() {

    quanhuyen = "bsd_quan_thuongtru";
    tinhthanh = "bsd_thanhphotinhtieubang_thuongtru";
    Xrm.Page.getAttribute(tinhthanh).setValue(null);
    Xrm.Page.getAttribute(quanhuyen).setValue(null);
    MakeAddress();
}

// Tinh Thanh OnChange
function Onchange_TinhThanh() {
    Xrm.Page.getAttribute("bsd_quan_thuongtru").setValue(null);
    MakeAddress();
}

// Copy tinh thanh vao Address Composite
function MakeAddress() {
    debugger;
    //UnBlock 
    Xrm.Page.ui.controls.get("bsd_diachi").setDisabled(false);

    var tmp = null;
    var fulltmp = null;
    var sonha = null;
    var fulladdress = null;

    address = "address1_line1";
    province = "address1_stateorprovince";
    country = "address1_country";

    // field
    fulladdress = "bsd_diachi";
    sonha = "bsd_duongsonha_thuongtru";
    phuong = "bsd_phuong_thuongtru";
    quanhuyen = "bsd_quan_thuongtru";
    tinhthanh = "bsd_thanhphotinhtieubang_thuongtru";
    quocgia = "bsd_quocgialanhtho_thuongtru";

    //Duong & so nha
    if (Xrm.Page.getAttribute(sonha).getValue()) {
        tmp = Xrm.Page.getAttribute(sonha).getValue();
    }

    //phuong  
    if (Xrm.Page.getAttribute(phuong).getValue()) {
        if (tmp != "")
            tmp += ", " + Xrm.Page.getAttribute(phuong).getValue();
    }

    //quanhuyen  
    if (Xrm.Page.getAttribute(quanhuyen).getValue()) {
        if (tmp != null)
            tmp += ", " + Xrm.Page.getAttribute(quanhuyen).getValue()[0].name;
    }

    fulltmp = tmp;
    Xrm.Page.getAttribute(address).setValue(tmp);

    //tinh thanh
    if (Xrm.Page.getAttribute(tinhthanh).getValue()) {
        tmp = Xrm.Page.getAttribute(tinhthanh).getValue()[0].name;
        Xrm.Page.getAttribute(province).setValue(tmp);
        fulltmp += ", " + tmp;

    }

    //quoc gia
    if (Xrm.Page.getAttribute(quocgia).getValue()) {
        tmp = Xrm.Page.getAttribute(quocgia).getValue()[0].name;
        Xrm.Page.getAttribute(country).setValue(tmp);
        fulltmp += ", " + tmp;
    }
    Xrm.Page.getAttribute(fulladdress).setValue(fulltmp);
    Xrm.Page.ui.controls.get("bsd_diachi").setDisabled(true);
}

function hide_Industrial() {
    debugger;
    var loai = Xrm.Page.getAttribute("bsd_industry_vl").getValue();

    if (loai != null) {
        Xrm.Page.getControl("bsd_beverage").setVisible(loai.indexOf(1) != -1);
        Xrm.Page.getControl("bsd_food").setVisible(loai.indexOf(2) != -1);
        Xrm.Page.getControl("bsd_milkicecream").setVisible(loai.indexOf(3) != -1);
        Xrm.Page.getControl("bsd_confectionary").setVisible(loai.indexOf(4) != -1);

    }
}
//author:Mr.Phong
function set_distributor() {
    var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    var entityName = "account";
    var viewDisplayName = "test";
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='account'>");
    xml.push("<attribute name='accountid' />");
    xml.push("<attribute name='name'/>");
    xml.push("<order attribute='name' descending='false' />");
    xml.push("<filter type='and' >");
    xml.push("<condition attribute='bsd_accounttype' operator='in'>");
    xml.push("<value>");
    xml.push("861450000");
    xml.push("</value>");
    xml.push("<value>");
    xml.push("100000000");
    xml.push("</value>");
    xml.push("</condition>");
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='accountid'>  " +
                       "<cell name='primarycontactid'   " + "width='200' />  " +
                       "<cell name='telephone1'    " + "width='100' />  " +
                         "<cell name='name'    " + "width='100' />  " +
                       "</row>   " +
                    "</grid>   ";
    Xrm.Page.getControl("parentcustomerid").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
}
function ContactType_Change() {
    set_appearordisappear();
    set_DisableOutletType();
    Load_account_change();
}
/*Diệm: Load Account type change*/
function Load_account_change() {
    debugger;
    var contacttype = getValue("bsd_contacttype");
    if (contacttype != null) {
        var fetchxml = "";
        if (contacttype == 861450001) {
            fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                           '  <entity name="account">',
                           '    <attribute name="name" />',
                           '    <attribute name="primarycontactid" />',
                           '    <attribute name="telephone1" />',
                           '    <attribute name="accountid" />',
                           '    <order attribute="name" descending="false" />',
                           '    <filter type="and">',
                           '      <condition attribute="bsd_accounttype" operator="eq" value="100000000" />',
                           '      <condition attribute="statecode" operator="eq" value="0" />',
                           '    </filter>',
                           '  </entity>',
                           '</fetch>'].join('');

        } else {
            fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                           '  <entity name="account">',
                           '    <attribute name="name" />',
                           '    <attribute name="primarycontactid" />',
                           '    <attribute name="telephone1" />',
                           '    <attribute name="accountid" />',
                           '    <order attribute="name" descending="false" />',
                           '    <filter type="and">',
                           '      <condition attribute="bsd_accounttypename" operator="not-like" value="%861450001%" />',
                           '      <condition attribute="statecode" operator="eq" value="0" />',
                           '    </filter>',
                           '  </entity>',
                           '</fetch>'].join('');
        }
        LookUp_After_Load("parentcustomerid", "account", "name", "Account", fetchxml);
    }
}


//author:Mr.phong
//description:hide or visible filed job title
function set_appearordisappear() {
    debugger;
    var contracttype = Xrm.Page.getAttribute("bsd_contacttype").getValue();
    if (contracttype == "861450000" || contracttype==null)// account
    {
        setVisible("jobtitle",true);
        setVisible(["bsd_storename", "bsd_employee", "bsd_trade2", "bsd_schedulevisit", "bsd_typestore", "bsd_storelocation", "address1_latitude", "address1_longitude", "bsd_employee", "bsd_address"], false);
        setNull(["bsd_storename", "bsd_employee", "bsd_trade2", "bsd_schedulevisit", "bsd_typestore", "bsd_storelocation", "address1_latitude", "address1_longitude", "bsd_employee", "bsd_address"]);
        setRequired(["bsd_typestore", "bsd_storelocation", "bsd_employee", "bsd_address"], "none");
    }

    if (contracttype == "861450001")// outlet 
    {
        setVisible("jobtitle", false);
        setVisible(["bsd_storename", "bsd_employee", "bsd_trade2", "bsd_schedulevisit", "bsd_typestore", "bsd_storelocation", "address1_latitude", "address1_longitude", "bsd_employee", "bsd_address"], true);
        setRequired(["bsd_employee", "bsd_address"], "required");
        setNull(["jobtitle"]);
    }
 
}
//Author:Mr.Phong
//Description:filter filed address
function filter_addressonaccount() {
    var id = Xrm.Page.data.entity.getId();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var a = [];
    var b = [];
    if (id == "") {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='contact'>");
        xml.push("<attribute name='fullname'/>");
        xml.push("<attribute name='contactid' />");
        xml.push("<attribute name='telephone1'/>");
        xml.push("<attribute name='bsd_address'/>");
        xml.push("<order attribute='fullname' descending='false' />");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                for (var i = 0; i < rs.length; i++) {
                    if (rs[i].attributes.bsd_address != null) {
                        a.push(rs[i].attributes.bsd_address.guid);
                    }
                }
                var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName = "bsd_address";
                var viewDisplayName = "test";
                xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml1.push("<entity name='bsd_address'>");
                xml1.push("<attribute name='bsd_name'/>");
                xml1.push("<attribute name='bsd_purpose' />");
                xml1.push("<attribute name='bsd_account'/>");
                xml1.push("<attribute name='bsd_addressid'/>");
                xml1.push("<attribute name='createdon'/>");
                xml1.push("<order attribute='bsd_name' descending='false' />");
                xml1.push("<filter type='and'>");
                xml1.push("<condition attribute='bsd_account' operator='null'/>");
                xml1.push("<condition attribute='bsd_addressid' operator='not-in'>");
                for (var k = 0; k < a.length; k++) {
                    xml1.push("<value uitype='bsd_address'>" + '{' + a[k] + '}' + "");
                    xml1.push("</value>");
                }
                xml1.push("</condition>");
                xml1.push("</filter>");
                xml1.push("</entity>");
                xml1.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='bsd_addressid'>  " +
                       "<cell name='bsd_name'    " + "width='200' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                         "<cell name='bsd_account'    " + "width='100' />  " +
                          "<cell name='createdon'    " + "width='100' />  " +
                       "</row>   " +
                    "</grid>   ";
                Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
            }
        },
        function (er) {
            console.log(er.message)
        });
    }
    else {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='contact'>");
        xml.push("<attribute name='fullname'/>");
        xml.push("<attribute name='contactid' />");
        xml.push("<attribute name='telephone1'/>");
        xml.push("<attribute name='bsd_address'/>");
        xml.push("<order attribute='fullname' descending='false' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='contactid' operator='eq' uitype='contact' value='" + id + "'/>");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0 && rs[0].attributes.bsd_address != null) {
                xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml4.push("<entity name='contact'>");
                xml4.push("<attribute name='fullname'/>");
                xml4.push("<attribute name='contactid' />");
                xml4.push("<attribute name='telephone1'/>");
                xml4.push("<attribute name='bsd_address'/>");
                xml4.push("<order attribute='fullname' descending='false' />");
                xml4.push("</entity>");
                xml4.push("</fetch>");
                CrmFetchKit.Fetch(xml4.join(""), false).then(function (rs4) {
                    if (rs4.length > 0) {
                        for (var i = 0; i < rs4.length; i++) {
                            if (rs4[i].attributes.bsd_address != null && rs4[i].attributes.bsd_address.guid != rs[0].attributes.bsd_address.guid) {
                                a.push(rs4[i].attributes.bsd_address.guid);
                            }
                        }
                        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                        var entityName = "bsd_address";
                        var viewDisplayName = "test";
                        xml5.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                        xml5.push("<entity name='bsd_address'>");
                        xml5.push("<attribute name='bsd_name'/>");
                        xml5.push("<attribute name='bsd_purpose' />");
                        xml5.push("<attribute name='bsd_account'/>");
                        xml5.push("<attribute name='bsd_addressid'/>");
                        xml5.push("<attribute name='createdon'/>");
                        xml5.push("<order attribute='bsd_name' descending='false' />");
                        xml5.push("<filter type='and'>");
                        xml5.push("<condition attribute='bsd_account' operator='null'/>");
                        xml5.push("<condition attribute='bsd_addressid' operator='not-in'>");
                        for (var k = 0; k < a.length; k++) {
                            xml5.push("<value uitype='bsd_address'>" + '{' + a[k] + '}' + "");
                            xml5.push("</value>");
                        }
                        xml5.push("</condition>");
                        xml5.push("</filter>");
                        xml5.push("</entity>");
                        xml5.push("</fetch>");
                        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_addressid'>  " +
                               "<cell name='bsd_name'    " + "width='200' />  " +
                                "<cell name='bsd_purpose'    " + "width='100' />  " +
                                 "<cell name='bsd_account'    " + "width='100' />  " +
                                  "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
                        Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml5.join(""), layoutXml, true);
                    }
                },
                function (er) {
                    console.log(er.message)
                });
            }
            if (rs.length > 0 && rs[0].attributes.bsd_address == null) {
                xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml2.push("<entity name='contact'>");
                xml2.push("<attribute name='fullname'/>");
                xml2.push("<attribute name='contactid' />");
                xml2.push("<attribute name='telephone1'/>");
                xml2.push("<attribute name='bsd_address'/>");
                xml2.push("<order attribute='fullname' descending='false' />");
                xml2.push("</entity>");
                xml2.push("</fetch>");
                CrmFetchKit.Fetch(xml2.join(""), false).then(function (rs2) {
                    if (rs2.length > 0) {
                        for (var i = 0; i < rs2.length; i++) {
                            if (rs2[i].attributes.bsd_address != null) {
                                b.push(rs2[i].attributes.bsd_address.guid);
                            }
                        }
                        var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                        var entityName1 = "bsd_address";
                        var viewDisplayName1 = "test";
                        xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                        xml3.push("<entity name='bsd_address'>");
                        xml3.push("<attribute name='bsd_name'/>");
                        xml3.push("<attribute name='bsd_purpose' />");
                        xml3.push("<attribute name='bsd_account'/>");
                        xml3.push("<attribute name='bsd_addressid'/>");
                        xml3.push("<attribute name='createdon'/>");
                        xml3.push("<order attribute='bsd_name' descending='false' />");
                        xml3.push("<filter type='and'>");
                        xml3.push("<condition attribute='bsd_account' operator='null'/>");
                        xml3.push("<condition attribute='bsd_addressid' operator='not-in'>");
                        for (var k = 0; k < b.length; k++) {
                            xml3.push("<value uitype='bsd_address'>" + '{' + b[k] + '}' + "");
                            xml3.push("</value>");
                        }
                        xml3.push("</condition>");
                        xml3.push("</filter>");
                        xml3.push("</entity>");
                        xml3.push("</fetch>");
                        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_addressid'>  " +
                               "<cell name='bsd_name'    " + "width='200' />  " +
                                "<cell name='bsd_purpose'    " + "width='100' />  " +
                                 "<cell name='bsd_account'    " + "width='100' />  " +
                                  "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
                        Xrm.Page.getControl("bsd_address").addCustomView(viewId1, entityName1, viewDisplayName1, xml3.join(""), layoutXml1, true);
                    }
                },
                function (er) {
                    console.log(er.message)
                });
            }
        },
        function (er) {
            console.log(er.message)
        });
    }

}
//Author:Mr.Phong
//Description:check contact have mcp details
function checkcontacthavemcpdetails() {
    var id = Xrm.Page.data.entity.getId();
    if (id != null) {
        var xml = [];
        fetch(xml, "bsd_mcpdetail", ["bsd_mcpdetailid", "bsd_name", "createdon", "bsd_wednesday", "bsd_tuesday", "bsd_thursday", "bsd_sunday"
                               , "bsd_saturday", "bsd_monday", "bsd_friday"], "bsd_name", false, null
                   , ["bsd_outlet", "statecode"], ["eq", "eq"], [0, id, 1, "0"]);
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                set_disable(["bsd_schedulevisit"], 1);
            }
            if (rs.length == 0) {
                set_disable(["bsd_schedulevisit"], 0);
            }
        },
        function (er) {
            console.log(er.message)
        });

    }
}
//Author:Mr.Đăng
//Description: Disable Outlet Type
function set_DisableOutletType() {
    var contacttype = getValue("bsd_contacttype");
    if (contacttype == "861450001") {
        setVisible("bsd_outlettype", true);
        setRequired("bsd_outlettype", "required");
    }
    else {
        setVisible("bsd_outlettype", false);
        setNull("bsd_outlettype");
        setRequired("bsd_outlettype", "none");
    }
}
// 22.03.2017 Đăng: setDisabled Account
function setDisabled_Account() {
    var customer = getValue("parentcustomerid");
    if (customer != null) {
        setDisabled("parentcustomerid", true);
    }
}
//Author:Mr.Phong
//Description:gan gia tri cho field account name khi chon employee
function filter_accountnamefield() {
    //var employee = Xrm.Page.getAttribute("bsd_employee").getValue();
    //Xrm.Page.getControl("parentcustomerid").removePreSearch(presearch_accountname);
    //var xml = [];
    //if (employee != null) {
    //    var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    //    var entityName = "account";
    //    var viewDisplayName = "test";
    //    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
    //    xml.push("<entity name='account'>");
    //    xml.push("<attribute name='name'/>");
    //    xml.push("<attribute name='primarycontactid' />");
    //    xml.push("<attribute name='telephone1' />");
    //    xml.push("<attribute name='accountid' />");
    //    xml.push("<order attribute='name' descending='false' />");
    //    xml.push("<link-entity name='bsd_employeeaccount' from='bsd_account' to='accountid' alias='af'>");
    //    xml.push("<filter type='and'>");
    //    xml.push("<condition attribute='bsd_employee' operator='eq' uitype='bsd_employee' value='" + employee[0].id + "' />");
    //    xml.push("</filter>");
    //    xml.push("</link-entity>");
    //    xml.push("</entity>");
    //    xml.push("</fetch>");
    //    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
    //                          "<row name='result'  " + "id='accountid'>  " +
    //                          "<cell name='name'   " + "width='200' />  " +
    //                          "<cell name='primarycontactid'    " + "width='100' />  " +
    //                           "<cell name='telephone1'    " + "width='100' />  " +
    //                          "</row>   " +
    //                       "</grid>   ";
    //    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
    //        if (rs.length > 0) {
    //            Xrm.Page.getControl("parentcustomerid").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    //            Xrm.Page.getControl("parentcustomerid").addPreSearch(addFilter);
    //        }
    //        else {
    //            Xrm.Page.getAttribute("parentcustomerid").setValue(null);
    //            clear_accountname();
    //        }
    //    }, function (er) {
    //        console.log(er.message)
    //    });
    //}
    //else {
    //    Xrm.Page.getAttribute("parentcustomerid").setValue(null);
    //    clear_accountname();
    //}
}
function clear_accountname() {
    Xrm.Page.getControl("parentcustomerid").addPreSearch(presearch_accountname);
}
function presearch_accountname() {
    Xrm.Page.getControl("parentcustomerid").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function addFilter() {
    var customerAccountFilter = "<filter type='and'><condition attribute='contactid' operator='null' /></filter>";
    Xrm.Page.getControl("parentcustomerid").addCustomFilter(customerAccountFilter, "contact");
}
//27.03.2017 Đăng NH: Bắt dữ liệu tại các trường Phone vs Fax
function Check_PhoneFax() {
    debugger;
    var mikExp = /[^0-9.() -]/;
    var telephone1 = getValue("telephone1");
    var telephone2 = getValue("telephone2");
    var mobilephone = getValue("mobilephone");
    var fax = getValue("fax");
    clearNotification("telephone1");
    clearNotification("telephone2");
    clearNotification("mobilephone");
    clearNotification("fax");
    if (telephone1 != null) {
        if (mikExp.test(telephone1)) {
            setNotification("telephone1", "Invalid Bussiness Phone Number.");
        }
        else {
            clearNotification("telephone1");
        }
    }
    if (telephone2 != null) {
        if (mikExp.test(telephone2)) {
            setNotification("telephone2", "Invalid Home Phone Number.");
        }
        else {
            clearNotification("telephone2");
        }
    }
    if (mobilephone != null) {
        if (mikExp.test(mobilephone)) {
            setNotification("mobilephone", "Invalid Mobile Phone Number.");
        }
        else {
            clearNotification("mobilephone");
        }
    }
    if (fax != null) {
        if (mikExp.test(fax)) {
            setNotification("fax", "Invalid Fax Number.");
        }
        else {
            clearNotification("fax");
        }
    }
}