//Mr:Diệm
//Note: Load defaut disable 


function Onload() {
    if (formType() == 2) {
        LK_country(false);
        LK_region(false);
        LK_provice(false);
        LK_district(false);
        Lk_Ward(false);
    } else {
        clear_Region();
        clear_Province();
        clear_District();
        clear_Ward();
    }
}


//Mr:Diệm
//Note: search region follow country
function LK_country(reset) {
    if (reset != false) setNull(["bsd_region", "bsd_province", "bsd_district", "bsd_ward"]);
    console.log(getControl("bsd_region").getDefaultView());
    var country = getValue("bsd_country");
    if (country != null) {
      setValue("bsd_name", country[0].name);  
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_region">',
                        '<attribute name="bsd_regionid"/>',
                        '<attribute name="bsd_name"/>',
                        '<attribute name="createdon"/>',
                        '<order attribute="bsd_name" descending="false"/>',
                        '<filter type="and">',
                        '<condition attribute="bsd_country" operator="eq" uitype="bsd_country" value="' + country[0].id + '"/>',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_regionid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_regionid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "</row>   " +
                        "</grid>";
        getControl("bsd_region").removePreSearch(presearch_Region);
        getControl("bsd_region").addCustomView("{4871A8DD-C3B5-4EFC-B91B-9B9CF9E88306}", "bsd_region", "Region", fetchxml, layoutXml, true);
    }
    else if(reset != false) {
        clear_Region();
        clear_Province();
        clear_District();
        clear_Ward();
    }
}

//Mr:Diệm
//Note: search province follow region

function LK_region(reset) {
    if (reset != false) setNull(["bsd_province", "bsd_district", "bsd_ward"]);
    console.log(getControl("bsd_province").getDefaultView());
    var region = getValue("bsd_region");
    var country = getValue("bsd_country");

    if (region != null) {
        setValue("bsd_name", region[0].name + "," + country[0].name);

        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_province">',
                        '<attribute name="bsd_provinceid"/>',
                        '<attribute name="bsd_name"/>',
                        '<attribute name="createdon"/>',
                        '<order attribute="bsd_name" descending="false"/>',
                        '<filter type="and">',
                        '<condition attribute="bsd_region" operator="eq" uitype="bsd_region" value="' + region[0].id + '"/>',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_provinceid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_provinceid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "</row>   " +
                        "</grid>";
        getControl("bsd_province").removePreSearch(presearch_Province);
        getControl("bsd_province").addCustomView("{831690EC-0614-46D2-8BF5-423CE68B6BF8}", "bsd_province", "Province", fetchxml, layoutXml, true);
    }
    else if(reset != false) {
        clear_Province();
        clear_District();
        clear_Ward();
    }
}

//Mr:Diệm
//Note: search district follow province
function LK_provice(reset) {
    if (reset != false) setNull(["bsd_district", "bsd_ward"]);
    console.log(getControl("bsd_district").getDefaultView());
    var province = getValue("bsd_province");
    var region = getValue("bsd_region");
    var country = getValue("bsd_country");

    if (province != null) {
        setValue("bsd_name", province[0].name + "," + region[0].name + "," + country[0].name);

        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_district">',
                        '<attribute name="bsd_districtid"/>',
                        '<attribute name="bsd_name"/>',
                        '<attribute name="createdon"/>',
                        '<order attribute="bsd_name" descending="false"/>',
                        '<filter type="and">',
                        '<condition attribute="bsd_province" operator="eq" uitype="bsd_province" value="' + province[0].id + '"/>',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_districtid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_districtid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "</row>   " +
                        "</grid>";
        getControl("bsd_district").removePreSearch(presearch_District);
        getControl("bsd_district").addCustomView("{5F60D1E4-F7DF-476A-88B9-AC0EDE10E321}", "bsd_district", "District", fetchxml, layoutXml, true);
    }
    else if(reset != false) {
        clear_District();
        clear_Ward();
    }
}

//Mr:Diệm
//Note: search ward follow district
function LK_district(reset) {
    if (reset != false) setNull(["bsd_ward"]);
    console.log(getControl("bsd_ward").getDefaultView());
    var district = getValue("bsd_district");
    var province = getValue("bsd_province");
    var region = getValue("bsd_region");
    var country = getValue("bsd_country");
    if (district != null) {
        setValue("bsd_name", district[0].name + "," + province[0].name + "," + region[0].name + "," + country[0].name);

 
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                        '<entity name="bsd_ward">',
                        '<attribute name="bsd_name"/>',
                        '<attribute name="createdon"/>',
                        '<attribute name="new_province"/>',
                        '<attribute name="bsd_district"/>',
                        '<attribute name="bsd_areadms"/>',
                        '<attribute name="bsd_wardid"/>',
                        '<order attribute="bsd_name" descending="false"/>',
                            '<link-entity name="bsd_wardarea" from="bsd_ward" to="bsd_wardid" link-type="outer" alias="ad">',
                                '<attribute name="bsd_wardareaid"/>',
                            '</link-entity>',
                            '<filter type="and">',
                                  '<condition attribute="bsd_district" operator="eq" uitype="bsd_district" value="' + district[0].id + '"/>',
                                  '<condition entityname="ad" attribute="bsd_wardareaid" operator="null"/>',
                            '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
       
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_wardid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_wardid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "</row>   " +
                        "</grid>";
        getControl("bsd_ward").removePreSearch(presearch_Ward);
        getControl("bsd_ward").addCustomView("{AFD95BCE-6F3C-4879-8CBB-888A04FE2F73}", "bsd_ward", "Ward", fetchxml, layoutXml, true);
    }
    else if(reset != false) {
        clear_Ward();
    }
}
//Mr:Diệm
//Note: Load again ward
function Lk_Ward() {
    var ward = getValue("bsd_ward");
    var district = getValue("bsd_district");
    var province = getValue("bsd_province");
    var region = getValue("bsd_region");
    var country = getValue("bsd_country");
    if (ward != null) {
        setValue("bsd_name", district[0].name + "," + province[0].name + "," + region[0].name + "," + country[0].name);
        Fullname();
    }
}
//Mr:Diệm
//Note: Set Full name from country, region, province, district, ward
function Fullname() {
    var ward = getValue("bsd_ward");
    var district = getValue("bsd_district");
    var province = getValue("bsd_province");
    var region = getValue("bsd_region");
    var country = getValue("bsd_country");

    if (ward != null) {
        if (district != null) {
            if (province != null) {
                if (region != null) {
                    setValue("bsd_name", ward[0].name + "," + district[0].name + "," + province[0].name + "," + region[0].name + "," + country[0].name);
                }
            }
        }
    }
    else if (district != null) {
        if (province) {
            if (region != null) {
                setValue("bsd_name", district[0].name + "," + province[0].name + "," + region[0].name + "," + country[0].name);
            }
        }
    }
    else if (province != null) {
        if (region != null) {
            setValue("bsd_name", province[0].name + "," + region[0].name + "," + country[0].name);
        }
    }
    else if (region != null) {
        setValue("bsd_name", region[0].name + "," + country[0].name);
    }
    else {
        setValue("bsd_name", country[0].name);
    }
}
//Set Function
function clear_Region() {
    getControl("bsd_region").addPreSearch(presearch_Region);
}
function presearch_Region() {
    getControl("bsd_region").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Province() {
    getControl("bsd_province").addPreSearch(presearch_Province);
}
function presearch_Province() {
    getControl("bsd_province").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_District() {
    getControl("bsd_district").addPreSearch(presearch_District);
}
function presearch_District() {
    getControl("bsd_district").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Ward() {
    getControl("bsd_ward").addPreSearch(presearch_Ward);
}
function presearch_Ward() {
    getControl("bsd_ward").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}