function AutoLoad() {
    debugger;
    filter_grid_orderhistory();
}


function filter_grid_orderhistory() {
    var objSubGrid = window.parent.document.getElementById("SubRipAccount");
    if (objSubGrid != null && objSubGrid.control != null) {
        var type = getValue("bsd_pricetype");
        if (type == "861450000")/*price list*/ {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '   <entity name="account">',
                    '     <attribute name="name" />',
                    '     <attribute name="accountid" />',
                    '     <order attribute="name" descending="false" />',
                    '     <filter type="and">',
                    '       <condition attribute="bsd_pricegroups" operator="eq"  uitype="bsd_pricegroups" value="' + Xrm.Page.data.entity.getId() + '" />',
                    '     </filter>',
                    '   </entity>',
                    ' </fetch>'];
        }
        else if (type == "861450001")/*discount*/ {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
               '   <entity name="account">',
               '     <attribute name="name" />',
               '     <attribute name="accountid" />',
               '     <order attribute="name" descending="false" />',
               '     <filter type="and">',
               '       <condition attribute="bsd_discountline" operator="eq"  uitype="bsd_pricegroups" value="' + Xrm.Page.data.entity.getId() + '" />',
               '     </filter>',
               '   </entity>',
               ' </fetch>'];
        }
        else if (type == "861450002")/*mutiline*/ {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                     '   <entity name="account">',
                     '     <attribute name="name" />',
                     '     <attribute name="accountid" />',
                     '     <order attribute="name" descending="false" />',
                     '     <filter type="and">',
                     '       <condition attribute="bsd_multiline" operator="eq"  uitype="bsd_pricegroups" value="' + Xrm.Page.data.entity.getId() + '" />',
                     '     </filter>',
                     '   </entity>',
                     ' </fetch>'];
        }
        else/*total*/ {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '   <entity name="account">',
                      '     <attribute name="name" />',
                      '     <attribute name="accountid" />',
                      '     <order attribute="name" descending="false" />',
                      '     <filter type="and">',
                      '       <condition attribute="bsd_total" operator="eq"  uitype="bsd_pricegroups" value="' + Xrm.Page.data.entity.getId() + '" />',
                      '     </filter>',
                      '   </entity>',
                      ' </fetch>'];
        }
      
        objSubGrid.control.SetParameter("fetchXML", xml);
        objSubGrid.control.Refresh();
    } else {
        setTimeout('filter_grid_orderhistory()', 1000);
    }
}
