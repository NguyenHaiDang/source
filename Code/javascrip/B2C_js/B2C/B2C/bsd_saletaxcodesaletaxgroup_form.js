﻿//Author:Mr.Đăng
//Description:Check Sales Tax Code
function Check_Salestaxcode() {
    var saletaxgroup = getValue("bsd_saletaxgroup");
    var saletaxcode = getValue("bsd_saletaxcode");
    var id = getId();
    if (saletaxcode != null) {
        setValue("bsd_name", saletaxcode[0].name);
        if (saletaxgroup != null) {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_saletaxcodesaletaxgroup">',
                            '<attribute name="bsd_saletaxcodesaletaxgroupid" />',
                            '<attribute name="bsd_name" />',
                            '<attribute name="createdon" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_saletaxgroup" operator="eq" uitype="bsd_saletaxgroup" value="' + saletaxgroup[0].id + '" />',
                            '<condition attribute="bsd_saletaxcode" operator="eq" uitype="bsd_saletaxcode" value="' + saletaxcode[0].id + '" />',
                            '<condition attribute="bsd_saletaxcodesaletaxgroupid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_saletaxcode", "Đã tồn tại, vui lòng kiểm tra lại.");
            }
            else {
                clearNotification("bsd_saletaxcode");
                load_description();
            }
        }
    }
    else {
        setNull("bsd_name");
        setValue("bsd_percentageamount", null);
        setValue("bsd_description", null);
    }
}
//Author:Mr.Đăng
//Description:load description & Percentage/Amount
function load_description() {
    var saletaxcode = getValue("bsd_saletaxcode");
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_saletaxcode">',
                        '<attribute name="bsd_saletaxcodeid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_percentageamount" />',
                        '<attribute name="bsd_description" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_saletaxcodeid" operator="eq" uitype="bsd_saletaxcode" value="' + saletaxcode[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
    var rs = CrmFetchKit.FetchSync(fetchxml);
    if (rs.length > 0) {
        setValue("bsd_percentageamount", rs[0].attributes.bsd_percentageamount.value);
        setValue("bsd_description", rs[0].attributes.bsd_description.value);
    }
}