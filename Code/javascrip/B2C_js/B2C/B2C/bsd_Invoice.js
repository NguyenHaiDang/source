function Autoload() {
    //debugger;
    //console.log("autoload");
    Lk_Order_AutoLoad_Duyet();
    if (formType() == 2) {
        //Lk_PriceList_From_OrderDMS(false);
        filter_orderproductdms();
        LoadAddressDistributers();
        
    } else {

        clear_distributor();
        clear_pricelist();
        clear_outlet();
        clear_shiptoaddress();
    }

}
function filter_orderproductdms() {
    //console.log("runnnnn");
    //var objSubGrid = window.parent.document.getElementById("orderproductdmsSugrid");
    //var orderdms = getValue("bsd_orderdms");
    //if (objSubGrid != null && objSubGrid.control != null && orderdms != null) {
    //    var xml = [
    //        '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
    //      '<entity name="bsd_orderproductdms">',
    //        '<attribute name="bsd_orderproductdmsid" />',
    //        '<attribute name="bsd_name" />',
    //        '<attribute name="createdon" />',
    //        '<order attribute="bsd_name" descending="false" />',
    //        '<filter type="and">',
    //          '<condition attribute="bsd_orderdms" operator="eq" uiname="Don dat hang" uitype="bsd_orderdms" value="' + orderdms[0].id + '" />',
    //        '</filter>',
    //      '</entity>',
    //    '</fetch>'
    //    ];
    //    objSubGrid.control.SetParameter("fetchXML", xml);
    //    objSubGrid.control.Refresh();
    //} else {
    //    setTimeout('filter_orderproductdms()', 1000);
    //}
}
function Lk_Order_AutoLoad_Duyet() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="bsd_orderdms">',
                    '<attribute name="bsd_orderdmsid" />',
                    '<attribute name="bsd_name" />',
                    '<attribute name="createdon" />',
                    '<order attribute="bsd_name" descending="false" />',
                    '<filter type="and">',
                     ' <condition attribute="bsd_statusorder" operator="eq" value="861450000" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_orderdmsid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='bsd_orderdmsid'>  " +
                         "<cell name='bsd_name'   " + "width='200' />  " +
                         "</row>   " +
                      "</grid>";
    getControl("bsd_orderdms").addCustomView(getControl("bsd_orderdms").getDefaultView(), "bsd_orderdms", "OrderDMS Duyet", fetchxml, layoutXml, true);

}
function Lk_PriceList_From_OrderDMS(reset) {
    if (reset != false) setNull(["bsd_pricelist", "bsd_distributor", "bsd_outlet", "bsd_shiptoaddress", "bsd_amount","bsd_totalpriceinclusive"]);
    var pricelist = getValue("bsd_orderdms")
    if (pricelist != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '              <entity name="bsd_orderdms">',
                        '                <attribute name="bsd_orderdmsid" />',
                        '                <attribute name="bsd_name" />',
                        '                <attribute name="createdon" />',
                        '                <attribute name="bsd_pricelist" />',
                        '                <attribute name="bsd_distributor" />',
                        '                <attribute name="bsd_cusomer" />',
                        '                <attribute name="bsd_currency" />',
                        '                 <attribute name="bsd_totalamounts" />',
                        '                 <attribute name="bsd_toltalpriceinclusivetax" />',
                        '                 <attribute name="bsd_shiptoaddress" />',
                        '                <order attribute="bsd_name" descending="false" />',
                        '                <filter type="and">',
                        '                  <condition attribute="bsd_orderdmsid" operator="eq" uitype="bsd_orderdms" value="' + pricelist[0].id + '" />',
                        //'                   <condition attribute="bsd_totalamounts" operator="not-null" />',
                        //'                   <condition attribute="bsd_toltalpriceinclusivetax" operator="not-null" />',
                        '                   <condition attribute="bsd_statusorder" operator="eq" value="861450000" />',
                        '                </filter>',
                        '              </entity>',
                        '            </fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        console.log(rs);
        if (rs.length > 0) {
            var first = rs[0];

            //if (getValue("bsd_currency") == null) {
            //    setValue("bsd_currency", [{
            //        id: first.attributes.bsd_currency.guid,
            //        name: first.attributes.bsd_currency.name,
            //        entityType: first.attributes.bsd_currency.logicalName
            //    }])
            //}
            if (getValue("bsd_pricelist") == null) {
                setValue("bsd_pricelist", [{
                    id: first.attributes.bsd_pricelist.guid,
                    name: first.attributes.bsd_pricelist.name,
                    entityType: first.attributes.bsd_pricelist.logicalName
                }]
                );
            }
            if (getValue("bsd_distributor") == null) {
                setValue("bsd_distributor", [{
                    id: first.attributes.bsd_distributor.guid,
                    name: first.attributes.bsd_distributor.name,
                    entityType: first.attributes.bsd_distributor.logicalName
                }])
            }
            if (getValue("bsd_outlet") == null) {
                setValue("bsd_outlet", [{
                    id: first.attributes.bsd_cusomer.guid,
                    name: first.attributes.bsd_cusomer.name,
                    entityType: first.attributes.bsd_cusomer.logicalName
                }])
            }
            if (getValue("bsd_shiptoaddress") == null) {
                setValue("bsd_shiptoaddress", [{
                    id: first.attributes.bsd_shiptoaddress.guid,
                    name: first.attributes.bsd_shiptoaddress.name,
                    entityType: first.attributes.bsd_shiptoaddress.logicalName
                }]);
            }
            if (getValue("bsd_totalpriceinclusive") == null) {
                setValue("bsd_totalpriceinclusive", first.attributes.bsd_toltalpriceinclusivetax.value)
            }
            if (getValue("bsd_amount") == null) {
                setValue("bsd_amount", first.attributes.bsd_totalamounts.value)
            }

        }
    }
    else if (reset != false) {

        clear_distributor();
        clear_pricelist();
        clear_outlet();
        clear_shiptoaddress();
    }
}
function clear_amount() {
    getControl("bsd_amount").addPreSearch(presearch_amount);
}
function presearch_amount() {
    getControl("bsd_amount").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_distributor() {
    getControl("bsd_distributor").addPreSearch(presearch_distributor);
}
function presearch_distributor() {
    getControl("bsd_distributor").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_pricelist() {
    getControl("bsd_pricelist").addPreSearch(presearch_pricelist);
}
function presearch_pricelist() {
    getControl("bsd_pricelist").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_outlet() {
    getControl("bsd_outlet").addPreSearch(presearch_outlet);
}
function presearch_outlet() {
    getControl("bsd_outlet").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function clear_shiptoaddress() {
    getControl("bsd_shiptoaddress").addPreSearch(presearch_shiptoaddress);
}
function presearch_shiptoaddress() {
    getControl("bsd_shiptoaddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function reload() {
    //Xrm.Utility.openEntityForm(Xrm.Page.data.entity.getEntityName(), Xrm.Page.data.entity.getId());
}
//author:Mr.Phong
function set_currenciesdefault() {
    var fielddefault = Xrm.Page.getAttribute("bsd_currency");
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='transactioncurrency'>");
    xml.push("<attribute name='transactioncurrencyid' />");
    xml.push("<attribute name='currencyname' />");
    xml.push("<attribute name='isocurrencycode' />");
    xml.push("<attribute name='currencysymbol' />");
    xml.push("<attribute name='exchangerate' />");
    xml.push("<attribute name='currencyprecision' />");
    xml.push("<order attribute='exchangerate' descending='true' />");
    xml.push("</entity>");
    xml.push("</fetch>");
    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
        if (rs.length > 0 && rs[0].attributes.currencyname != null) {
            fielddefault.setValue([{
                id: rs[0].Id,
                name: rs[0].attributes.currencyname.value,
                entityType: rs[0].logicalName
            }]);
        }
    },
       function (er) {
           console.log(er.message)
       });
}
//Author:Mr.Đăng
//Description: Load Address Distributers
function LoadAddressDistributers() {
    var distributers = getValue("bsd_distributor");
    if (distributers != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + distributers[0].id + '" />',
                        '<condition attribute="bsd_purpose" operator="eq" value="861450000" />',
                        '<condition attribute="statecode" operator="eq" value="0" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        //CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
        //    console.log(rs);
        //    if (rs.length > 0) {
        //        var first = rs[0];
        //        if (getValue("bsd_distributoraddress") == null) {
        //            setValue("bsd_distributoraddress", [{
        //                id: first.Id,
        //                name: first.attributes.bsd_name.value,
        //                entityType: first.logicalName
        //            }]
        //           );
        //        }
        //    }
        //}, function (er) { });
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_addressid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>";
        getControl("bsd_distributoraddress").addCustomView(getControl("bsd_distributoraddress").getDefaultView(), "bsd_address", "Address", fetchxml, layoutXml, true);
    }
    else if (distributers == null)
    {
        setnull("bsd_distributoraddress");
    }
}