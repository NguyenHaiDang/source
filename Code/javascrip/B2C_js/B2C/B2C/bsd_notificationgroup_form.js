﻿// 29.03.2017 Đăng NH: Autoload
function AutoLoad() {
    set_DisabledNotification();// 29.03.2017 Đăng NH: Disable field Notification
    if (formType() == 1) {
    } else {
    }
}
//Author:Mr.Phong
//Description:filter employee follow sale sup
function filteremployeefollowsalesup() {
    var a = [];
    var xml = [];
    var xml1 = [];
    a = get_value(["bsd_notification"]);
    fetch(xml, "bsd_notification", ["bsd_notificationid", "bsd_name", "createdon", "bsd_employee"]
         , ["bsd_name"], false, null, ["bsd_notificationid"], ["eq"], [0, a[0], 1]);
    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
        if (rs.length > 0) {
            var viewId1 = "{FF82D3E5-2040-4798-AABE-9ECB298228E9}";
            var entityName1 = "bsd_employee";
            var viewDisplayName1 = "test";
            fetch(xml1, "bsd_employee", ["bsd_employeeid", "bsd_name", "createdon", "bsd_manager", "bsd_account", "bsd_parentaccount"]
        , ["bsd_name"], false, null, ["bsd_parentaccountname"], ["like"], [0, rs[0].attributes.bsd_employee.name + "%", 1]);
            var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_employeeid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                      "<row name='result'  " + "id='bsd_employeeid'>  " +
                                      "<cell name='bsd_name'    " + "width='100' />  " +
                                        "<cell name='createdon'    " + "width='100' />  " +
                                          "<cell name='bsd_manager'    " + "width='100' />  " +
                                           "<cell name='bsd_account'    " + "width='100' />  " +
                                           "<cell name='bsd_parentaccount'    " + "width='100' />  " +
                                      "</row>   " +
                                   "</grid>   ";
            Xrm.Page.getControl("bsd_employee").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
        }
    }, function (er) {
        console.log(er.message)
    });
}
//Author:Mr.Phong
//Description:set name
function setname()
{
    var a = [];
    a = get_value(["bsd_notification"]);
    if (a[0] != null)
    {
        set_value(["bsd_name"], a[1]);
    }
    else {
        set_value(["bsd_name"],null);
    }
}
// 29.03.2017 Đăng NH: Disable field Notification
function set_DisabledNotification() {
    var notification = getValue("bsd_notification");
    if (notification != null) setDisabled("bsd_notification", true);
}
