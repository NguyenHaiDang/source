﻿function Autoload() {
    if (formType() == 1 || formType() == 2) {
        set_DisableTypeAll();
    }
    else {//
    }
}
//Author:Mr.Đăng
//Description: Disable when Type = All
function set_DisableTypeAll() {
    var type = getValue("bsd_type");
    if (type == "861450000") {
        setVisible(["bsd_areab2c", "bsd_employee"], false);
        Xrm.Page.ui.tabs.get("{a3bbbc83-27fb-4532-a740-8a4303be9da3}").sections.get("{a3bbbc83-27fb-4532-a740-8a4303be9da3}_section_2").setVisible(false);
    }
    if (type == "861450001") {
        setVisible(["bsd_employee"], false);
        setVisible(["bsd_areab2c"], true);
        Xrm.Page.ui.tabs.get("{a3bbbc83-27fb-4532-a740-8a4303be9da3}").sections.get("{a3bbbc83-27fb-4532-a740-8a4303be9da3}_section_2").setVisible(false);
    }
    if (type == "861450002") {
        setVisible(["bsd_areab2c"], false);
        setVisible(["bsd_employee"], true);
        LK_LoadSaleSup();
        Xrm.Page.ui.tabs.get("{a3bbbc83-27fb-4532-a740-8a4303be9da3}").sections.get("{a3bbbc83-27fb-4532-a740-8a4303be9da3}_section_2").setVisible(true);
    }
}
//Author:Mr.Đăng
//Description: Load Sale Sup
function LK_LoadSaleSup() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_employee">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<attribute name="bsd_account" />',
                        '<attribute name="bsd_employeeid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_salegroup" operator="eq" value="861450001" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_employeeid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_employeeid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>";
    getControl("bsd_employee").addCustomView(getControl("bsd_employee").getDefaultView(), "bsd_employee", "Employee", fetchxml, layoutXml, true);
    
}