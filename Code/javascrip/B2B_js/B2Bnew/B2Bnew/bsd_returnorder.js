var suborder_update = null;
function init() {
    if (formType() == 1) {
        clear_order();
        clear_suborder();
        setVisible("bsd_returnorder", false);
    } else {
        if (getValue("bsd_neworder") != null) setVisible("bsd_neworder", true);
        if (getValue("bsd_returnorder") != null) setVisible("bsd_returnorder", true);
        setDisabled(["bsd_returnordertype", "bsd_fromsalesorder", "bsd_suborder", "bsd_customer"], true);
        if (getValue("bsd_suborder") != null) {
            suborder_update = getValue("bsd_suborder");
        }
        customer_change(false);
    }
}
function customer_change(rs) {
    if (rs != false) setNull("bsd_fromsalesorder");
    getControl("bsd_fromsalesorder").removePreSearch(presearch_order);
    order_change(rs);
    update_name(rs);
    var customer = getValue("bsd_customer");
    if (customer != null) {
        var xml = [
         '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
         '<entity name="salesorder">',
           '<attribute name="name" />',
           '<attribute name="customerid" />',
           '<attribute name="statuscode" />',
           '<attribute name="totalamount" />',
           '<attribute name="salesorderid" />',
           '<order attribute="name" descending="false" />',
           '<filter type="and">',
             '<condition attribute="bsd_status" operator="eq" value="1" />',
             '<condition attribute="bsd_ordertype" operator="eq" uitype="bsd_ordertype" value="{712569E3-F3BC-E611-93F1-000C29D47EAB}" />',
             '<condition attribute="customerid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
           '</filter>',
           '<link-entity name="bsd_deliverynote" from="bsd_order" to="salesorderid" alias="at">',
             '<filter type="and">',
               '<condition attribute="bsd_status" operator="eq" value="861450001" />',
             '</filter>',
           '</link-entity>',
         '</entity>',
        '</fetch>',
        ].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='salesorderid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                            "<row name='result'  " + "id='salesorderid'>  " +
                                            "<cell name='name'   " + "width='200' />  " +
                                            "</row>   " +
                                         "</grid>   ";

        getControl("bsd_fromsalesorder").addCustomView(getDefaultView("bsd_fromsalesorder"), "salesorder", "salesorder", xml, layoutXml, true);
    } else if (rs != false) clear_order();
}
function order_change(rs) {
    if (rs != false) setNull("bsd_suborder");
    getControl("bsd_suborder").removePreSearch(presearch_suborder);
    update_name(rs);
    var order = getValue("bsd_fromsalesorder");
    if (order != null) {
        var xml = [];
        xml.push('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">');
        xml.push('<entity name="bsd_suborder">');
        xml.push('<attribute name="bsd_suborderid" />');
        xml.push('<attribute name="bsd_name" />');
        xml.push('<link-entity name="bsd_returnorder" from="bsd_suborder" to="bsd_suborderid" alias="aa" link-type="outer">');
        xml.push('<attribute name="bsd_returnorderid" />');
        xml.push('</link-entity>');
        if (suborder_update != null) {
            xml.push('<filter type="or">');
            xml.push('<condition attribute="bsd_suborderidid" operator="eq" uitype="bsd_suborderid" value="' + suborder_update[0].id + '" />');
            xml.push('<filter>');
            xml.push('<condition attribute="bsd_order" operator="eq" uitype="salesorder" value="' + order[0].id + '" />');
            xml.push('<condition entityname="aa" attribute="bsd_returnorderid" operator="null" />');
            xml.push('</filter>');
            xml.push('</filter>');
        } else {
            xml.push('<filter type="and">');
            xml.push('<condition attribute="bsd_order" operator="eq" uitype="salesorder" value="' + order[0].id + '" />');
            xml.push('<condition entityname="aa" attribute="bsd_returnorderid" operator="null" />');
            xml.push('</filter>');
        }
        xml.push('</entity>');
        xml.push('</fetch>');
        console.log(xml.join(""));
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_suborderid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                                "<row name='result'  " + "id='bsd_suborderid'>  " +
                                                "<cell name='bsd_name'   " + "width='200' />  " +
                                                "</row>   " +
                                             "</grid>   ";
        getControl("bsd_suborder").addCustomView(getDefaultView("bsd_suborder"), "bsd_suborder", "bsd_suborder", xml.join(""), layoutXml, true);
    } else if (rs != false) clear_suborder();
}

function update_name(rs) {
    var customer = getValue("bsd_customer") != null ? getValue("bsd_customer")[0].name : "";
    var order = getValue("bsd_fromsalesorder") != null ? getValue("bsd_fromsalesorder")[0].name : "";
    setValue("bsd_name", customer + "-" + order);
}

function clear_suborder() {
    getControl("bsd_suborder").addPreSearch(presearch_suborder);
}
function presearch_suborder() {
    getControl("bsd_suborder").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_order() {
    getControl("bsd_fromsalesorder").addPreSearch(presearch_order);
}
function presearch_order() {
    getControl("bsd_fromsalesorder").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
