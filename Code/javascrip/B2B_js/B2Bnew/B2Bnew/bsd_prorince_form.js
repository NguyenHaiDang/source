//Author:Mr.Phong
//Description:filter region
function filter_region() {
    var country = Xrm.Page.getAttribute("bsd_country").getValue();
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    Xrm.Page.getControl("bsd_region").removePreSearch(presearch_region);
    var xml = [];
    var xml1 = [];
    if (country != null) {
        var viewId = "{FEA802AA-99D2-48BC-AD41-23EBB42E24B4}";
        var entityName = "bsd_countryregion";
        var viewDisplayName = "test";
        fetch(xml, "bsd_countryregion", ["bsd_countryregionid", "bsd_name", "createdon"], "bsd_name", false, null
           , ["bsd_country", "statecode"], ["eq", "eq"], [0, country[0].id, 1, "0"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_countryregionid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_countryregionid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_region").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        set_value(["bsd_region", "bsd_area"], null);
        clear_area();
    }
    if (country == null) {
        set_value(["bsd_region", "bsd_area"], null);
        clear_region();
        clear_area();
    }
}
//Author:Mr.Phong
//Description:filter area
function filter_area() {
    var region = Xrm.Page.getAttribute("bsd_region").getValue();
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    var xml = [];
    if (region != null) {
        var viewId = "{4BA4ADE8-3328-4823-BEC0-536B1006269D}";
        var entityName = "bsd_areab2b";
        var viewDisplayName = "test";
        fetch(xml, "bsd_areab2b", ["bsd_areab2bid", "bsd_name", "bsd_name"], "bsd_name", false, null
        , ["bsd_region", "statecode"], ["eq", "eq"], [0, region[0].id, 1, "0"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_areab2bid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_areab2bid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_area").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        Xrm.Page.getAttribute("bsd_area").setValue(null);
    }
    else {
        Xrm.Page.getAttribute("bsd_area").setValue(null);
        clear_area()
    }
}
//author:Mr.Phong
//description:load country
function loadcountry() {
    var countryFilter = "<filter type='and'><condition attribute='statecode' operator='eq' value='0'/></filter>";
    Xrm.Page.getControl("bsd_country").addCustomFilter(countryFilter, "bsd_country");
}
//author:Mr.Phong
//description:load region
function load_region() {
    var country = Xrm.Page.getAttribute("bsd_country").getValue();
    var xml = [];
    if (country != null) {
        var viewId = "{FEA802AA-99D2-48BC-AD41-23EBB42E24B4}";
        var entityName = "bsd_countryregion";
        var viewDisplayName = "test";
        fetch(xml, "bsd_countryregion", ["bsd_countryregionid", "bsd_name", "createdon"], "bsd_name", false, null
       , ["bsd_country", "statecode"], ["eq", "eq"], [0, country[0].id, 1, "0"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_countryregionid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_countryregionid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_region").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    }
    if (country == null) {
        Xrm.Page.getAttribute("bsd_region").setValue(null);
        //clear_region();
    }
}
//author:Mr.Phong
//description:load area
function load_area() {
    var region = Xrm.Page.getAttribute("bsd_region").getValue();
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    var xml = [];
    if (region != null) {
        var viewId = "{4BA4ADE8-3328-4823-BEC0-536B1006269D}";
        var entityName = "bsd_areab2b";
        var viewDisplayName = "test";
        fetch(xml, "bsd_areab2b", ["bsd_areab2bid", "bsd_name", "bsd_name"], "bsd_name", false, null
     , ["bsd_region", "statecode"], ["eq", "eq"], [0, region[0].id, 1, "0"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_areab2bid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_areab2bid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_area").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    }
    if (region == null) {
        Xrm.Page.getAttribute("bsd_area").setValue(null);
        //clear_area();
    }
}
//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    set_disableProvince();
    loadcountry();
    load_region();
    load_area();
}
//Author:Mr.Đăng
//Description: Disable Province
function set_disableProvince() {
    if (formType() == 2) {
        setDisabled("bsd_id", true);
    }
    else {
        setDisabled("bsd_id", false);
    }
}
//Author:Mr.Đăng
//Description:Check ProvinceID
function Check_ProvinceID() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var id = getValue("bsd_id");
    if (id != null) {
        if (mikExp.test(id)) {
            setNotification("bsd_id", "Special Characters are not allowed.Pls re-enter!");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_province">',
                        '<attribute name="bsd_provinceid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_id" operator="eq" value="' + id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join('');
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_id", "Mã tỉnh/thành phố đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_id");
                setValue("bsd_id", id.toUpperCase());
            }
        }
    }
}
function clear_area() {
    Xrm.Page.getControl("bsd_area").addPreSearch(presearch_area);
}
function presearch_area() {
    Xrm.Page.getControl("bsd_area").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_region() {
    Xrm.Page.getControl("bsd_region").addPreSearch(presearch_region);
}
function presearch_region() {
    Xrm.Page.getControl("bsd_region").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//author:Mr.Phong
//description: Kiểm tra không cho phép nhập kí tự đặc biệt
function Check_id() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var name = getValue("bsd_name");
    if (name != null) {
        if (mikExp.test(name)) {
            setNotification("bsd_name", "Special Characters are not allowed.Pls re-enter!");
        }
        else {
            clearNotification("bsd_name");
        }
    }
}
//Description:kiem tra trung record country
function check_samerecordprovince() {
    var id = Xrm.Page.data.entity.getId();
    var data = [];
    data = getvalue(["bsd_country", "bsd_region", "bsd_area", "bsd_name"]);
    var xmlprovince = [];
    Xrm.Page.getControl("bsd_region").removePreSearch(presearch_region);
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    if (data[0] != null && data[1] != null && data[3] != null && data[5] != null) {
        if (id != "") {
            fetch(xmlprovince, "bsd_province", ["bsd_provinceid", "bsd_name", "createdon"], ["bsd_name"], false, null,
                                   ["bsd_country", "bsd_region", "bsd_area", "bsd_name", "bsd_provinceid"], ["eq", "eq", "eq", "eq", "ne"]
                                   , [0, data[0], 1, data[2], 2, data[4], 3, data[6], 4, id]);
            CrmFetchKit.Fetch(xmlprovince.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    var message = "Record is existed!";
                    var type = "INFO"; //INFO, WARNING, ERROR
                    var id = "Info1"; //Notification Id
                    var time = 3000; //Display time in milliseconds
                    //Display the notification
                    Xrm.Page.ui.setFormNotification(message, type, id);

                    //Wait the designated time and then remove
                    setTimeout(
                        function () {
                            Xrm.Page.ui.clearFormNotification(id);
                        },
                        time
                    );
                    setNull(["bsd_country", "bsd_region", "bsd_area", "bsd_name"]);
                    clear_area();
                    clear_region();
                }
            },
                        function (er) {
                            console.log(er.message)
                        });
        }
        else {
            fetch(xmlprovince, "bsd_province", ["bsd_provinceid", "bsd_name", "createdon"], ["bsd_name"], false, null,
                                   ["bsd_country", "bsd_region", "bsd_area", "bsd_name"], ["eq", "eq", "eq", "eq"]
                                   , [0, data[0], 1, data[2], 2, data[4], 3, data[6]]);
            CrmFetchKit.Fetch(xmlprovince.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    var message = "Record is existed!";
                    var type = "INFO"; //INFO, WARNING, ERROR
                    var id = "Info1"; //Notification Id
                    var time = 3000; //Display time in milliseconds
                    //Display the notification
                    Xrm.Page.ui.setFormNotification(message, type, id);

                    //Wait the designated time and then remove
                    setTimeout(
                        function () {
                            Xrm.Page.ui.clearFormNotification(id);
                        },
                        time
                    );
                    setNull(["bsd_country", "bsd_region", "bsd_area", "bsd_name"]);
                    clear_area();
                    clear_region();
                }
            },
                        function (er) {
                            console.log(er.message)
                        });
        }
    }

}