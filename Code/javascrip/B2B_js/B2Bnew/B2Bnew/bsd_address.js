//Desciption:load form
function init()
{
    load_province();
    load_district();
    load_ward();
    load_region();
    load_area();
    load_country();
    set_requireornot();
}
//author:Mr.Phong
//description:set province theo area
function filter_province() {
    var area = Xrm.Page.getAttribute("bsd_area").getValue();
    var provincedefault = Xrm.Page.getAttribute("bsd_province");
    var districdefault = Xrm.Page.getAttribute("bsd_district");
    var warddefault = Xrm.Page.getAttribute("bsd_ward"); 
    Xrm.Page.getControl("bsd_province").removePreSearch(presearch_province);
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
    Xrm.Page.getControl("bsd_ward").removePreSearch(presearch_ward);
        if (area != null) {
            var viewId = "{831690EC-0614-46D2-8BF5-423CE68B6BF8}";
            var entityName = "bsd_province";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_province'>");
            xml.push("<attribute name='bsd_provinceid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_area' uitype='bsd_areab2b' operator='eq' value='" + area[0].id + "' />");
            xml.push("<condition attribute='statecode' operator='eq' value='0' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_provinceid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_provinceid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_province").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            provincedefault.setValue(null);
            districdefault.setValue(null);
            warddefault.setValue(null);          
            clear_district();
            clear_ward();            
        }
        else {
            Xrm.Page.getAttribute("bsd_province").setValue(null);
            Xrm.Page.getAttribute("bsd_district").setValue(null);
            Xrm.Page.getAttribute("bsd_ward").setValue(null);
            clear_province();
            clear_district();
            clear_ward();
        }
}
//Author:Mr.Phong
//Description:filter area
function filter_area() {
    var region = Xrm.Page.getAttribute("bsd_region").getValue();
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    Xrm.Page.getControl("bsd_province").removePreSearch(presearch_province);
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
    Xrm.Page.getControl("bsd_ward").removePreSearch(presearch_ward);
    var xml = [];
    if (region != null) {
        var viewId = "{4BA4ADE8-3328-4823-BEC0-536B1006269D}";
        var entityName = "bsd_areab2b";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_areab2b'>");
        xml.push("<attribute name='bsd_areab2bid'/>");
        xml.push("<attribute name='bsd_name' />")
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_region' operator='eq' value='" + region[0].id + "' />");
        xml.push("<condition attribute='statecode' operator='eq' value='0' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_areab2bid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_areab2bid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_area").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        Xrm.Page.getAttribute("bsd_area").setValue(null);
        Xrm.Page.getAttribute("bsd_province").setValue(null);
        Xrm.Page.getAttribute("bsd_district").setValue(null);
        Xrm.Page.getAttribute("bsd_ward").setValue(null);
        clear_province();
        clear_district();
        clear_ward();
    }
    else {
        Xrm.Page.getAttribute("bsd_area").setValue(null);
        Xrm.Page.getAttribute("bsd_province").setValue(null);
        Xrm.Page.getAttribute("bsd_district").setValue(null);
        Xrm.Page.getAttribute("bsd_ward").setValue(null);
        clear_area();
        clear_province();
        clear_district();
        clear_ward();
    }
}
//author:Mr.Phong
//description:set district theo province
function filter_district() {
    var city = Xrm.Page.getAttribute("bsd_province").getValue();
    var districdefault = Xrm.Page.getAttribute("bsd_district");
    var warddefault = Xrm.Page.getAttribute("bsd_ward");
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
    Xrm.Page.getControl("bsd_ward").removePreSearch(presearch_ward);
    if (city != null) {
        var viewId = "{5F60D1E4-F7DF-476A-88B9-AC0EDE10E321}";
        var entityName = "bsd_district";
        var viewDisplayName = "test";
        var xml = [];
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_district'>");
        xml.push("<attribute name='bsd_districtid'/>");
        xml.push("<attribute name='bsd_name' />")
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_province' operator='eq' value='" + city[0].id + "' />");
        xml.push("<condition attribute='statecode' operator='eq' value='0' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_districtid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_districtid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_district").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        districdefault.setValue(null);
        warddefault.setValue(null);
        clear_ward();
    }
    else {
        Xrm.Page.getAttribute("bsd_district").setValue(null);
        Xrm.Page.getAttribute("bsd_ward").setValue(null);
        clear_district();
        clear_ward();
    }
}
//author:Mr.Phong
//description:get ward from district and province
function filter_ward() {
    var city = Xrm.Page.getAttribute("bsd_province").getValue();
    var district = Xrm.Page.getAttribute("bsd_district").getValue();
    var warddefault = Xrm.Page.getAttribute("bsd_ward");
    Xrm.Page.getControl("bsd_ward").removePreSearch(presearch_ward);
    if (city != null && district != null) {
        var viewId = "{AFD95BCE-6F3C-4879-8CBB-888A04FE2F73}";
        var entityName = "bsd_ward";
        var viewDisplayName = "test";
        var xml = [];
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_ward'>");
        xml.push("<attribute name='bsd_wardid'/>");
        xml.push("<attribute name='bsd_name' />")
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='new_province' operator='eq' value='" + city[0].id + "' />");
        xml.push("<condition attribute='bsd_district' operator='eq' value='" + district[0].id + "' />");
        xml.push("<condition attribute='statecode' operator='eq' value='0' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_wardid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_wardid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_ward").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        warddefault.setValue(null);
    }
    if (district == null) {
        Xrm.Page.getAttribute("bsd_ward").setValue(null);
        clear_ward();
    }
}
//Author:Mr.Phong
//Description:filter region
function filter_region() {
    var country = Xrm.Page.getAttribute("bsd_country").getValue();
    Xrm.Page.getControl("bsd_region").removePreSearch(presearch_region);
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    Xrm.Page.getControl("bsd_province").removePreSearch(presearch_province);
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
    Xrm.Page.getControl("bsd_ward").removePreSearch(presearch_ward);
    var xml = [];
    if (country != null) {
        var viewId = "{FEA802AA-99D2-48BC-AD41-23EBB42E24B4}";
        var entityName = "bsd_countryregion";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_countryregion'>");
        xml.push("<attribute name='bsd_countryregionid'/>");
        xml.push("<attribute name='bsd_name' />")
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_country' operator='eq' value='" + country[0].id + "' />");
        xml.push("<condition attribute='statecode' operator='eq' value='0' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_countryregionid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_countryregionid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_region").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        Xrm.Page.getAttribute("bsd_region").setValue(null);
        Xrm.Page.getAttribute("bsd_area").setValue(null);
        Xrm.Page.getAttribute("bsd_province").setValue(null);
        Xrm.Page.getAttribute("bsd_district").setValue(null);
        Xrm.Page.getAttribute("bsd_ward").setValue(null);
        clear_area();
        clear_province();
        clear_district();
        clear_ward();
    }
    if (country == null) {
        Xrm.Page.getAttribute("bsd_region").setValue(null);
        Xrm.Page.getAttribute("bsd_area").setValue(null);
        Xrm.Page.getAttribute("bsd_province").setValue(null);
        Xrm.Page.getAttribute("bsd_district").setValue(null);
        Xrm.Page.getAttribute("bsd_ward").setValue(null);
        clear_region();
        clear_area();
        clear_province();
        clear_district();
        clear_ward();
    }
}

//Đăng/ 17/03/2017: Chỉ hiển thị Street,Ward, District, Province, Country
function set_address() {
    Xrm.Page.getAttribute("bsd_name").setSubmitMode("always");
    var country = getValue("bsd_country");
    var city = getValue("bsd_province");
    var district = getValue("bsd_district");
    var ward = getValue("bsd_ward");
    var street = getValue("bsd_street");
    if (street == null) {
        setValue("bsd_name", ward[0].name + ", " + district[0].name + ", " + city[0].name + ", " + country[0].name);
    }
    else if (street != null) {
        setValue("bsd_name", street + ", " + ward[0].name + ", " + district[0].name + ", " + city[0].name + ", " + country[0].name);
    }
    /*
    if (city != null && district != null && ward != null && street != null) {
        Xrm.Page.getAttribute("bsd_name").setValue(street + " " + "," + ward[0].name + " " + "," + district[0].name + " " + "," + city[0].name);
    }
    if (city != null && district == null && ward != null && street != null) {
        Xrm.Page.getAttribute("bsd_name").setValue(street + " " + "," + ward[0].name + " " + "," + city[0].name);
    }
    if (city != null && district != null && ward == null && street != null) {
        Xrm.Page.getAttribute("bsd_name").setValue(street + " " + "," + district[0].name + " " + "," + city[0].name);
    }
    if (city != null && district != null && ward != null && street == null) {
        Xrm.Page.getAttribute("bsd_name").setValue(ward[0].name + " " + "," + district[0].name + " " + "," + city[0].name);
    }
    if (city != null && district == null && ward == null && street != null) {
        Xrm.Page.getAttribute("bsd_name").setValue(street + " " + "," + city[0].name);
    }
    if (city != null && district != null && ward == null && street == null) {
        Xrm.Page.getAttribute("bsd_name").setValue(district[0].name + " " + "," +city[0].name);
    }
    if (city != null && district == null && ward == null && street == null) {
        Xrm.Page.getAttribute("bsd_name").setValue(city[0].name);
    }
    */
}
//author:Mr.Phong
//description:load province
function load_province() {
    var area = Xrm.Page.getAttribute("bsd_area").getValue();
    Xrm.Page.getControl("bsd_province").removePreSearch(presearch_province);
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
    Xrm.Page.getControl("bsd_ward").removePreSearch(presearch_ward);
    if (area != null) {
            var viewId = "{831690EC-0614-46D2-8BF5-423CE68B6BF8}";
            var entityName = "bsd_province";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_province'>");
            xml.push("<attribute name='bsd_provinceid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_area' uitype='bsd_areab2b' operator='eq' value='" + area[0].id + "' />");
            xml.push("<condition attribute='statecode' operator='eq' value='0' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_provinceid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_provinceid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_province").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
    if (area == null) {
        Xrm.Page.getAttribute("bsd_province").setValue(null);
        Xrm.Page.getAttribute("bsd_district").setValue(null);
        Xrm.Page.getAttribute("bsd_ward").setValue(null);
        clear_province();
        clear_district();
        clear_ward();
    }
}
//author:Mr.Phong
//description:load district
function load_district() {
    var city = Xrm.Page.getAttribute("bsd_province").getValue();
    var districdefault = Xrm.Page.getAttribute("bsd_district");
    var warddefault = Xrm.Page.getAttribute("bsd_ward");
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
    Xrm.Page.getControl("bsd_ward").removePreSearch(presearch_ward); 
        if (city != null) {
            var viewId = "{5F60D1E4-F7DF-476A-88B9-AC0EDE10E321}";
            var entityName = "bsd_district";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_district'>");
            xml.push("<attribute name='bsd_districtid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_province' operator='eq' value='" + city[0].id + "' />");
            xml.push("<condition attribute='statecode' operator='eq' value='0' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_districtid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_districtid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_district").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        else {
            Xrm.Page.getAttribute("bsd_district").setValue(null);
            Xrm.Page.getAttribute("bsd_ward").setValue(null);
            Xrm.Page.getAttribute("bsd_street").setValue(null);
            clear_district();
            clear_ward();
        }   
}
//author:Mr.Phong
//description:load ward
function load_ward() {
    var city = Xrm.Page.getAttribute("bsd_province").getValue();
    var district = Xrm.Page.getAttribute("bsd_district").getValue();
    var warddefault = Xrm.Page.getAttribute("bsd_ward");
    Xrm.Page.getControl("bsd_ward").removePreSearch(presearch_ward);
        if (city != null && district != null) {
            var viewId = "{AFD95BCE-6F3C-4879-8CBB-888A04FE2F73}";
            var entityName = "bsd_ward";
            var viewDisplayName = "test";
            var xml = [];
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_ward'>");
            xml.push("<attribute name='bsd_wardid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='new_province' operator='eq' value='" + city[0].id + "' />");
            xml.push("<condition attribute='bsd_district' operator='eq' value='" + district[0].id + "' />");
            xml.push("<condition attribute='statecode' operator='eq' value='0' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_wardid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_wardid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_ward").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        if (district == null) {
            Xrm.Page.getAttribute("bsd_ward").setValue(null);
            clear_ward();
        }   
}
//author:Mr.Phong
//description:load region
function load_region() {
    var country = Xrm.Page.getAttribute("bsd_country").getValue();
    Xrm.Page.getControl("bsd_region").removePreSearch(presearch_region);
    var xml = [];   
        if (country != null) {
            var viewId = "{FEA802AA-99D2-48BC-AD41-23EBB42E24B4}";
            var entityName = "bsd_countryregion";
            var viewDisplayName = "test";
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_countryregion'>");
            xml.push("<attribute name='bsd_countryregionid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_country' operator='eq' value='" + country[0].id + "' />");
            xml.push("<condition attribute='statecode' operator='eq' value='0' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_countryregionid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_countryregionid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_region").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        if (country == null) {
            Xrm.Page.getAttribute("bsd_region").setValue(null);
            clear_region();
        }    
}
//author:Mr.Phong
//description:load area
function load_area() {
    var region = Xrm.Page.getAttribute("bsd_region").getValue();
    Xrm.Page.getControl("bsd_area").removePreSearch(presearch_area);
    var xml = [];   
        if (region != null) {
            var viewId = "{4BA4ADE8-3328-4823-BEC0-536B1006269D}";
            var entityName = "bsd_areab2b";
            var viewDisplayName = "test";
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_areab2b'>");
            xml.push("<attribute name='bsd_areab2bid'/>");
            xml.push("<attribute name='bsd_name' />")
            xml.push("<attribute name='createdon' />");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_region' operator='eq' value='" + region[0].id + "' />");
            xml.push("<condition attribute='statecode' operator='eq' value='0' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_areab2bid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_areab2bid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";
            Xrm.Page.getControl("bsd_area").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        }
        if (region == null) {
            Xrm.Page.getAttribute("bsd_area").setValue(null);
            clear_area();
        }   
}
//Author:Mr.Phong
//Description:set require or not 
function set_requireornot() {
    var account = getValue("bsd_account");
    var contact = getValue("bsd_contact");
    var lead = getValue("bsd_lead");
    if (account != null) {
        setRequired(["bsd_lead", "bsd_contact"], "none");
        setVisible(["bsd_lead", "bsd_contact"], false);
    }
    if (contact != null) {
        setRequired(["bsd_lead", "bsd_account"], "none");
        setVisible(["bsd_lead", "bsd_account"], false);
    }
    if (lead != null) {
        setRequired(["bsd_account", "bsd_contact"], "none");
        setVisible(["bsd_account", "bsd_contact"], false);
    }
    if (account == null && contact == null && lead == null) {
        setRequired(["bsd_lead", "bsd_account", "bsd_contact"], "none");
        setVisible(["bsd_lead", "bsd_account", "bsd_contact"], false);
    }
}
//author:Mr.Phong
//description:load country
function load_country() {
    var countryFilter = "<filter type='and'><condition attribute='statecode' operator='eq' value='0'/></filter>";
    Xrm.Page.getControl("bsd_country").addCustomFilter(countryFilter, "bsd_country");
}

function clear_ward() {
    Xrm.Page.getControl("bsd_ward").addPreSearch(presearch_ward);
}
function presearch_ward() {
    Xrm.Page.getControl("bsd_ward").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_district() {
    Xrm.Page.getControl("bsd_district").addPreSearch(presearch_district);
}
function presearch_district() {
    Xrm.Page.getControl("bsd_district").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_province() {
    Xrm.Page.getControl("bsd_province").addPreSearch(presearch_province);
}
function presearch_province() {
    Xrm.Page.getControl("bsd_province").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_area() {
    Xrm.Page.getControl("bsd_area").addPreSearch(presearch_area);
}
function presearch_area() {
    Xrm.Page.getControl("bsd_area").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_region() {
    Xrm.Page.getControl("bsd_region").addPreSearch(presearch_region);
}
function presearch_region() {
    Xrm.Page.getControl("bsd_region").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

