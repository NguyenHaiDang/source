﻿//Author:Mr.Đăng
//Description:Check Method of Payment
function Check_methodofpayment() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var methodofpayment = getValue("bsd_methodofpayment");
    var id = getId();
    if (methodofpayment != null) {
        if (mikExp.test(methodofpayment)) {
            setNotification("bsd_methodofpayment", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_methodofpayment">',
                            '<attribute name="bsd_methodofpaymentid" />',
                            '<attribute name="bsd_name" />',
                            '<attribute name="statecode" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_methodofpayment" operator="eq" value="' + methodofpayment + '" />',
                            '<condition attribute="bsd_methodofpaymentid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join('');
            var rs = CrmFetchKit.FetchSync(fetchxml);
            console.log(rs);
            if (rs.length > 0) {
                if (rs[0].attributes.statecode.value == 1)
                {
                    setNotification("bsd_methodofpayment", "Mã số tài khoản đã tồn tại và Deactivated, vui lòng kiểm tra lại.");
                }
                else
                {
                    setNotification("bsd_methodofpayment", "Mã số tài khoản đã tồn tại, vui lòng kiểm tra lại.");
                }
            }
            else {
                clearNotification("bsd_methodofpayment");
                setValue("bsd_methodofpayment", methodofpayment.toUpperCase());
                setValue("bsd_name", methodofpayment.toUpperCase());
            }
        }
    }
}