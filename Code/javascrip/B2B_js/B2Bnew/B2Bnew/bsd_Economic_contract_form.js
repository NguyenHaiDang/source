//author:Mr.Phong
//description:filter address
function set_address() {
    var customer = Xrm.Page.getAttribute("bsd_account").getValue();
    var fielddefault = Xrm.Page.getAttribute("bsd_address");
    var xml = [];
    var xml2 = [];
    if (fielddefault.getValue() == null) {
        if (customer != null) {
            var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
            var entityName = "bsd_address";
            var viewDisplayName = "test";
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_address'>");
            xml.push("<attribute name='bsd_addressid' />");
            xml.push("<attribute name='bsd_name'/>");
            xml.push("<attribute name='createdon'/>");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='" + customer[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_addressid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
            Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    var warehouse = rs[0].attributes.bsd_name;
                    if (warehouse != null) {
                        fielddefault.setValue([{
                            id: rs[0].Id,
                            name: rs[0].attributes.bsd_name.value,
                            entityType: rs[0].logicalName
                        }]);
                    }
                }
                if (rs.length == 0) {
                    fielddefault.setValue(null);
                }
            },
                  function (er) {
                      console.log(er.message)
                  });
            xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml2.push("<entity name='contact'>");
            xml2.push("<attribute name='fullname' />");
            xml2.push("<attribute name='telephone1' />");
            xml2.push("<attribute name='contactid' />");
            xml2.push("<attribute name='jobtitle' />");
            xml2.push("<order attribute='fullname' descending='true' />");
            xml2.push("<filter type='and' >");
            xml2.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
            xml2.push("</filter>");
            xml2.push("</entity>");
            xml2.push("</fetch>");
            CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                if (rs2.length > 0) {
                    if (rs2[0].attributes.fullname != null) {
                        Xrm.Page.getAttribute("bsd_present").setValue(rs2[0].attributes.fullname.value);
                    }
                    if (rs2[0].attributes.jobtitle != null) {
                        Xrm.Page.getAttribute("bsd_jobtitle").setValue(rs2[0].attributes.jobtitle.value);
                    }
                }
            },
                function (er) {
                    console.log(er.message)
                });
        }
    }
}

//author:Mr.Phong
//description:set info customer
function set_info() {
    var customer = Xrm.Page.getAttribute("bsd_account").getValue();
    var xml = [];
    var xml1 = [];
    if (customer != null) {

        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='account'>");
        xml.push("<attribute name='name' />");
        xml.push("<attribute name='primarycontactid'/>");
        xml.push("<attribute name='telephone1'/>");
        xml.push("<attribute name='accountid'/>");
        xml.push("<attribute name='bsd_taxregistration'/>");
        xml.push("<attribute name='fax'/>");
        xml.push("<order attribute='name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='accountid' operator='eq' value='" + customer[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.telephone1 != null) {
                    var telephone = rs[0].attributes.telephone1.value;
                    Xrm.Page.getAttribute("bsd_phone").setValue(telephone);
                }
                if (rs[0].attributes.fax != null) {
                    var fax = rs[0].attributes.fax.value;
                    Xrm.Page.getAttribute("bsd_fax").setValue(fax);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    var taxres = rs[0].attributes.bsd_taxregistration.value;
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(taxres);
                }
                if (rs[0].attributes.primarycontactid != null) {
                    var contact = rs[0].attributes.primarycontactid.name;
                    Xrm.Page.getAttribute("bsd_present").setValue(contact);
                    var id = rs[0].attributes.primarycontactid.guid;
                    xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                    xml1.push("<entity name='contact'>");
                    xml1.push("<attribute name='fullname' />");
                    xml1.push("<attribute name='telephone1'/>");
                    xml1.push("<attribute name='contactid'/>");
                    xml1.push("<attribute name='jobtitle'/>");
                    xml1.push("<order attribute='fullname' descending='false' />");
                    xml1.push("<filter type='and' >");
                    xml1.push("<condition attribute='contactid' operator='eq' value='" + id + "' />");
                    xml1.push("</filter>");
                    xml1.push("</entity>");
                    xml1.push("</fetch>");
                    CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
                        if (rs1.length > 0) {
                            if (rs1[0].attributes.jobtitle != null) {
                                var job = rs1[0].attributes.jobtitle.value;
                                Xrm.Page.getAttribute("bsd_jobtitle").setValue(job);
                            }
                        }
                        if (rs1.length == 0)
                        {
                            Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
                        }
                    },
           function (er) {
               console.log(er.message)
           });
                }
            }
            if (rs.length == 0)
            {
                Xrm.Page.getAttribute("bsd_phone").setValue(null);
                Xrm.Page.getAttribute("bsd_fax").setValue(null);
                Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                Xrm.Page.getAttribute("bsd_present").setValue(null);
            }
        },
               function (er) {
                   console.log(er.message)
               });
    }
 if(customer==null)
    {
        Xrm.Page.getAttribute("bsd_phone").setValue(null);
        Xrm.Page.getAttribute("bsd_fax").setValue(null);
        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
        Xrm.Page.getAttribute("bsd_present").setValue(null);
        Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
        Xrm.Page.getAttribute("bsd_address").setValue(null);
    }
}
//author:Mr.Phong
function get_name() {
    if (Xrm.Page.ui.getFormType() == 2) {
        var contactdefault = Xrm.Page.getAttribute("bsd_nguoidaidiena");
        Xrm.Page.getAttribute("bsd_renter").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_addressrenter").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_phonerenter").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_taxregistrationrenter").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_faxrenter").setSubmitMode("always");
        var fielddefault = Xrm.Page.getAttribute("bsd_renter");
        var addressdefault = Xrm.Page.getAttribute("bsd_addressrenter");
        var paymentdefault = Xrm.Page.getAttribute("bsd_paymenttermsa");
        var xml = [];
        var xml1 = [];
        var xml2 = [];
        var xml4 = [];
        var xml5 = [];
        if (fielddefault.getValue() == null || addressdefault.getValue() == null || contactdefault.getValue() == null || paymentdefault.getValue() == null) {
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='account'>");
            xml.push("<attribute name='name' />");
            xml.push("<attribute name='primarycontactid' />");
            xml.push("<attribute name='telephone1' />");
            xml.push("<attribute name='accountid' />");
            xml.push("<attribute name='bsd_taxregistration' />");
            xml.push("<attribute name='fax' />");
            xml.push("<attribute name='bsd_paymentterm' />");
            xml.push("<order attribute='name' descending='true' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='accountid' operator='eq' uitype='account' value='{6BE4B61C-13B9-E611-93F1-000C29D47EAB}' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    if (fielddefault.getValue()==null) {
                        if (rs[0].attributes.name != null) {
                            fielddefault.setValue([{
                                id: rs[0].Id,
                                name: rs[0].attributes.name.value,
                                entityType: rs[0].logicalName
                            }]);
                        }
                    }
                    if (paymentdefault.getValue()==null) {
                        if (rs[0].attributes.bsd_paymentterm != null) {
                            paymentdefault.setValue([{
                                id: rs[0].attributes.bsd_paymentterm.guid,
                                name: rs[0].attributes.bsd_paymentterm.name,
                                entityType: rs[0].attributes.bsd_paymentterm.logicalName,
                            }]);
                        }
                    }
                    if (Xrm.Page.getAttribute("bsd_phonerenter").getValue()==null) {
                        if (rs[0].attributes.telephone1 != null) {
                            Xrm.Page.getAttribute("bsd_phonerenter").setValue(rs[0].attributes.telephone1.value);
                        }
                    }
                    if (Xrm.Page.getAttribute("bsd_taxregistrationrenter").getValue()==null) {
                        if (rs[0].attributes.bsd_taxregistration != null) {
                            Xrm.Page.getAttribute("bsd_taxregistrationrenter").setValue(rs[0].attributes.bsd_taxregistration.value);
                        }
                    }
                    if (Xrm.Page.getAttribute("bsd_faxrenter").getValue()==null) {
                        if (rs[0].attributes.fax != null) {
                            Xrm.Page.getAttribute("bsd_faxrenter").setValue(rs[0].attributes.fax.value);
                        }
                    }                 
                }
            },
                function (er) {
                    console.log(er.message)
                });
            xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml1.push("<entity name='bsd_address'>");
            xml1.push("<attribute name='bsd_addressid' />");
            xml1.push("<attribute name='bsd_name' />");
            xml1.push("<attribute name='createdon' />");
            xml1.push("<order attribute='bsd_name' descending='true' />");
            xml1.push("<filter type='and' >");
            xml1.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='{6BE4B61C-13B9-E611-93F1-000C29D47EAB}' />");
            xml1.push("<condition attribute='bsd_purpose' operator='eq'  value='861450000' />");
            xml1.push("<condition attribute='statecode' operator='eq'  value='0' />");
            xml1.push("</filter>");
            xml1.push("</entity>");
            xml1.push("</fetch>");
            CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                if (rs1.length > 0) {
                    if (addressdefault.getValue()==null) {
                        if (rs1[0].attributes.bsd_name != null) {
                            addressdefault.setValue([{
                                id: rs1[0].Id,
                                name: rs1[0].attributes.bsd_name.value,
                                entityType: rs1[0].logicalName
                            }]);
                        }
                    }             
                }
            },
                function (er) {
                    console.log(er.message)
                });
            xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
            xml2.push("<entity name='account'>");
            xml2.push("<attribute name='name' />");
            xml2.push("<attribute name='primarycontactid'/>");
            xml2.push("<attribute name='telephone1'/>");
            xml2.push("<attribute name='accountid'/>");
            xml2.push("<order attribute='name' descending='false'/> ");
            xml2.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
            xml2.push("<attribute name='jobtitle'/>");
            xml2.push("<filter type='and'>");
            xml2.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='{6BE4B61C-13B9-E611-93F1-000C29D47EAB}'/>");
            xml2.push("</filter>");
            xml2.push("</link-entity>");
            xml2.push("</entity>");
            xml2.push("</fetch>");
            CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                if (rs2.length > 0) {
                    if (contactdefault.getValue()==null) {
                        if (rs2[0].attributes.primarycontactid != null) {
                            contactdefault.setValue([{
                                id: rs2[0].attributes.primarycontactid.guid,
                                name: rs2[0].attributes.primarycontactid.name,
                                entityType: 'contact'
                            }]);
                        }
                        xml5.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                        xml5.push("<entity name='contact'>");
                        xml5.push("<attribute name='fullname'/>");
                        xml5.push("<attribute name='telephone1'/>")
                        xml5.push("<attribute name='contactid'/>");
                        xml5.push("<attribute name='jobtitle'/>");
                        xml5.push("<order attribute='fullname' descending='true' />");
                        xml5.push("<filter type='and'>");
                        xml5.push("<condition attribute='fullname' operator='eq'  value='" + rs2[0].attributes.primarycontactid.name + "' />");
                        xml5.push("</filter>");
                        xml5.push("</entity>");
                        xml5.push("</fetch>");
                        CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                            if (rs5.length > 0) {
                                if (Xrm.Page.getAttribute("bsd_jobtitlea").getValue()==null) {
                                    if (rs5[0].attributes.jobtitle != null) {
                                        Xrm.Page.getAttribute("bsd_jobtitlea").setValue(rs5[0].attributes.jobtitle.value);
                                    }
                                }                         
                            }
                        },
                function (er) {
                    console.log(er.message)
                });
                    }
                    var viewId1 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                    var entityName1 = "contact";
                    var viewDisplayName1 = "test";
                    xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                    xml4.push("<entity name='contact'>");
                    xml4.push("<attribute name='fullname'/>");
                    xml4.push("<attribute name='telephone1'/>")
                    xml4.push("<attribute name='contactid'/>");
                    xml4.push("<order attribute='fullname' descending='true' />");
                    xml4.push("<filter type='and'>");
                    xml4.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='{6BE4B61C-13B9-E611-93F1-000C29D47EAB}' />");
                    xml4.push("</filter>");
                    xml4.push("</entity>");
                    xml4.push("</fetch>");
                    var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                     "<row name='result'  " + "id='contactid'>  " +
                                     "<cell name='fullname'   " + "width='200' />  " +
                                     "<cell name='telephone1'    " + "width='100' />  " +
                                     "</row>   " +
                                  "</grid>   ";
                    Xrm.Page.getControl("bsd_nguoidaidiena").addCustomView(viewId1, entityName1, viewDisplayName1, xml4.join(""), layoutXml1, true);
                }
            },
                function (er) {
                    console.log(er.message)
                });
        }
    }
}
//author:Mr.Phong
//description:filter type subgrid by renter
function filter_subgriddkbylookupkh() {
    //var objSubGrid = document.getElementById("bankaccountrenter");
    var objSubGrid = window.parent.document.getElementById("bankaccountrenter");
    if (objSubGrid == null) {
        setTimeout(filter_subgriddkbylookupkh, 2000);
        return;
    } else {
            var FetchXml = "<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0'>" +
                                "<entity name='bsd_bankaccount'>" +
                                    "<attribute name='bsd_bankaccountid'/>" +
                                    "<attribute name='bsd_name'/>" +
                                    "<attribute name='createdon'/>" +
                                    "<order attribute='bsd_name' descending='false' />" +
                                    "<filter type='and'>" +
                                        "<condition attribute='bsd_account' operator='eq' value='{6BE4B61C-13B9-E611-93F1-000C29D47EAB}' />" +
                                     "</filter>" +
                                "</entity>" +
                            "</fetch>";
            if (objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", FetchXml);
                objSubGrid.control.Refresh();
            } else {
                setTimeout(filter_subgriddkbylookupkh, 500);
            }       
    }
}
//author:Mr.Phong
//description:filter type subgrid by customer
function filter_subgriddkbylookupcus() {
    //var objSubGrid = document.getElementById("bankaccountb");
    var objSubGrid = window.parent.document.getElementById("bankaccountb");
    if (objSubGrid == null) {
        setTimeout(filter_subgriddkbylookupcus, 2000);
        return;
    } else {
        var lookup = Xrm.Page.getAttribute("bsd_account").getValue();
        if (lookup != null) {
            var FetchXml = "<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0'>" +
                                "<entity name='bsd_bankaccount'>" +
                                    "<attribute name='bsd_bankaccountid'/>" +
                                    "<attribute name='bsd_name'/>" +
                                    "<attribute name='createdon'/>" +
                                    "<order attribute='bsd_name' descending='false' />" +
                                    "<filter type='and'>" +
                                        "<condition attribute='bsd_account' operator='eq' value='" + lookup[0].id + "' />" +
                                     "</filter>" +
                                "</entity>" +
                            "</fetch>";
            if (objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", FetchXml);
                objSubGrid.control.Refresh();
            } else {
                setTimeout(filter_subgriddkbylookupcus, 500);
            }
        }
    }
}
//auhthor:Mr.Phong
//desciption:hide button add subgrid
function set_hidebutton()
{
 if (Xrm.Page.ui.getFormType() ==2) 
{
     try {
         window.parent.frames[0].document.getElementById('bankaccountrenter_contextualButtonsContainer').style.visibility = "hidden";
         window.parent.frames[0].document.getElementById('bankaccountrenter_openAssociatedGridViewImageButton').style.visibility = "hidden";
         window.parent.frames[0].document.getElementById('bankaccountb_contextualButtonsContainer').style.visibility = "hidden";
         window.parent.frames[0].document.getElementById('bankaccountb_openAssociatedGridViewImageButton').style.visibility = "hidden";
     } catch (err) {

     }
 }
    if (Xrm.Page.ui.getFormType() == 1) {
        try {
            window.parent.frames[0].document.getElementById('bankaccountrenter_contextualButtonsContainer').style.visibility = "hidden";
            window.parent.frames[0].document.getElementById('bankaccountrenter_openAssociatedGridViewImageButton').style.visibility = "hidden";
            window.parent.frames[0].document.getElementById('bankaccountb_contextualButtonsContainer').style.visibility = "hidden";
            window.parent.frames[0].document.getElementById('bankaccountb_openAssociatedGridViewImageButton').style.visibility = "hidden";
        } catch (err) {

        }
   
 }
}
//author:Mr.Phong
function hide_button() {
    window.parent.document.getElementById("stageBackActionContainer").style.display = "none";
    window.parent.document.getElementById("stageBackActionContainer").style.visibility = "hidden";
    window.parent.document.getElementById("stageAdvanceActionContainer").style.display = "none";
    window.parent.document.getElementById("stageAdvanceActionContainer").style.visibility = "hidden";
    window.parent.document.getElementById("stageSetActiveActionContainer").style.display = "none";
    window.parent.document.getElementById("stageSetActiveActionContainer").style.visibility = "hidden";
}
//author:Mr.Phong
function set_currenciesdefault() {
    var fielddefault = Xrm.Page.getAttribute("transactioncurrencyid");
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='transactioncurrency'>");
    xml.push("<attribute name='transactioncurrencyid' />");
    xml.push("<attribute name='currencyname' />");
    xml.push("<attribute name='isocurrencycode' />");
    xml.push("<attribute name='currencysymbol' />");
    xml.push("<attribute name='exchangerate' />");
    xml.push("<attribute name='currencyprecision' />");
    xml.push("<order attribute='exchangerate' descending='true' />");
    xml.push("</entity>");
    xml.push("</fetch>");
    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
        if (rs.length > 0 && rs[0].attributes.currencyname != null) {
            fielddefault.setValue([{
                id: rs[0].Id,
                name: rs[0].attributes.currencyname.value,
                entityType: rs[0].logicalName
            }]);
        }
    },
       function (er) {
           console.log(er.message)
       });
}
//Author:Mr.Phong
//description:get information customer from orderID
function get_thongtinkhachhang()
{
    if (Xrm.Page.ui.getFormType() == 2) {
        Xrm.Page.getAttribute("bsd_account").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_phone").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_taxregistration").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_fax").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_website").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_mail").setSubmitMode("always");
        var ordid = Xrm.Page.getAttribute("bsd_quote").getValue();
        var fiedpaymentterm = Xrm.Page.getAttribute("bsd_paymenttermsa");
        var fiedpaymentmethod = Xrm.Page.getAttribute("bsd_paymentmethod");
        var contactdefault = Xrm.Page.getAttribute("bsd_nguoidaidienb");
        var xml = [];
        var xml1 = [];
        var xml2 = [];
        var xml3 = [];
        var xml4 = [];
        var xml5 = [];
        var xml6 = [];
        var xml7 = [];
        var xml8 = [];
        if ( fiedpaymentterm.getValue() == null || fiedpaymentmethod.getValue() == null  || contactdefault.getValue() == null) {
            if (ordid != null) {
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
                xml.push("<entity name='account'>");
                xml.push("<attribute name='name' />");
                xml.push("<attribute name='primarycontactid'/>");
                xml.push("<attribute name='telephone1'/>");
                xml.push("<attribute name='websiteurl'/>");
                xml.push("<attribute name='bsd_taxregistration'/>");
                xml.push("<attribute name='fax'/>");
                xml.push("<attribute name='emailaddress1'/>");
                xml.push("<attribute name='accountid'/>");
                xml.push("<order attribute='name' descending='false'/> ");
                xml.push("<link-entity name='quote' from='customerid' to='accountid' alias='ad'>");
                xml.push("<filter type='and'>");
                xml.push("<condition attribute='quoteid' operator='eq' uitype='quote' value='" + ordid[0].id + "'/>");
                xml.push("</filter>");
                xml.push("</link-entity>");
                xml.push("</entity>");
                xml.push("</fetch>");
                CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                    if (rs.length > 0) {                      
                        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
                        xml2.push("<entity name='account'>");
                        xml2.push("<attribute name='name' />");
                        xml2.push("<attribute name='primarycontactid'/>");
                        xml2.push("<attribute name='telephone1'/>");
                        xml2.push("<attribute name='accountid'/>");
                        xml2.push("<order attribute='name' descending='false'/> ");
                        xml2.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
                        xml2.push("<attribute name='jobtitle'/>");
                        xml2.push("<filter type='and'>");
                        xml2.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + rs[0].Id + "'/>");
                        xml2.push("</filter>");
                        xml2.push("</link-entity>");
                        xml2.push("</entity>");
                        xml2.push("</fetch>");
                        CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                            if (rs2.length > 0) {
                                if (contactdefault.getValue()==null) {
                                    if (rs2[0].attributes.primarycontactid != null) {
                                        contactdefault.setValue([{
                                            id: rs2[0].attributes.primarycontactid.guid,
                                            name: rs2[0].attributes.primarycontactid.name,
                                            entityType: 'contact'
                                        }]);
                                }                    
                                    xml6.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                                    xml6.push("<entity name='contact'>");
                                    xml6.push("<attribute name='fullname'/>");
                                    xml6.push("<attribute name='telephone1'/>")
                                    xml6.push("<attribute name='contactid'/>");
                                    xml6.push("<attribute name='jobtitle'/>");
                                    xml6.push("<order attribute='fullname' descending='true' />");
                                    xml6.push("<filter type='and'>");
                                    xml6.push("<condition attribute='fullname' operator='eq'  value='" + rs2[0].attributes.primarycontactid.name + "' />");
                                    xml6.push("</filter>");
                                    xml6.push("</entity>");
                                    xml6.push("</fetch>");
                                    CrmFetchKit.Fetch(xml6.join(""), true).then(function (rs6) {
                                        if (rs6.length > 0) {
                                            if (Xrm.Page.getAttribute("bsd_jobtitle").getValue()==null) {
                                                if (rs6[0].attributes.jobtitle != null) {
                                                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(rs6[0].attributes.jobtitle.value);
                                                }
                                            }                         
                                        }
                                    },
                            function (er) {
                                console.log(er.message)
                            });
                                }
                                var viewId1 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                                var entityName1 = "contact";
                                var viewDisplayName1 = "test";
                                xml7.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                                xml7.push("<entity name='contact'>");
                                xml7.push("<attribute name='fullname'/>");
                                xml7.push("<attribute name='telephone1'/>")
                                xml7.push("<attribute name='contactid'/>");
                                xml7.push("<order attribute='fullname' descending='true' />");
                                xml7.push("<filter type='and'>");
                                xml7.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + rs[0].Id + "' />");
                                xml7.push("</filter>");
                                xml7.push("</entity>");
                                xml7.push("</fetch>");
                                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                                 "<row name='result'  " + "id='contactid'>  " +
                                                 "<cell name='fullname'   " + "width='200' />  " +
                                                 "<cell name='telephone1'    " + "width='100' />  " +
                                                 "</row>   " +
                                              "</grid>   ";
                                Xrm.Page.getControl("bsd_nguoidaidienb").addCustomView(viewId1, entityName1, viewDisplayName1, xml7.join(""), layoutXml1, true);
                            }
                        },
                            function (er) {
                                console.log(er.message)
                            });
                        xml5.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                        xml5.push("<entity name='quote'>");
                        xml5.push("<attribute name='name' />");
                        xml5.push("<attribute name='customerid' />");
                        xml5.push("<attribute name='statuscode' />");
                        xml5.push("<attribute name='totalamount' />");
                        xml5.push("<attribute name='quoteid' />");
                        xml5.push("<attribute name='createdon'/>");
                        xml5.push("<attribute name='bsd_paymentterm' />");
                        xml5.push("<attribute name='bsd_paymentmethod' />");
                        xml5.push("<order attribute='name' descending='true' />");
                        xml5.push("<filter type='and' >");
                        xml5.push("<condition attribute='quoteid' operator='eq' uitype='quote' value='" + ordid[0].id + "' />");
                        xml5.push("</filter>");
                        xml5.push("</entity>");
                        xml5.push("</fetch>");
                        CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                            if (rs5.length > 0) {
                                if (fiedpaymentterm.getValue()==null) {
                                    if (rs5[0].attributes.bsd_paymentterm != null) {
                                        fiedpaymentterm.setValue([{
                                            id: rs5[0].attributes.bsd_paymentterm.guid,
                                            name: rs5[0].attributes.bsd_paymentterm.name,
                                            entityType: 'bsd_paymentterm'
                                        }]);
                                    }
                                }
                                if (fiedpaymentmethod.getValue()==null) {
                                    if (rs5[0].attributes.bsd_paymentmethod != null) {
                                        fiedpaymentmethod.setValue([{
                                            id: rs5[0].attributes.bsd_paymentmethod.guid,
                                            name: rs5[0].attributes.bsd_paymentmethod.name,
                                            entityType: 'bsd_methodofpayment'
                                        }]);
                                    }
                                }                         
                            }
                        }, function (er) {
                            console.log(er.message)
                        });
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }
    }
}
//author:Mr.Phong
//description:filter type subgrid by order
function filter_subgriddkbylookuporder() {
    //var objSubGrid = document.getElementById("bsd_bugtidproduct");
    var objSubGrid = window.parent.document.getElementById("bsd_bugtidproduct");
    if (objSubGrid == null) {
        setTimeout(filter_subgriddkbylookuporder, 2000);
        return;
    } else {
        var lookup = Xrm.Page.getAttribute("bsd_quote").getValue();
        if (lookup != null) {
            var FetchXml = "<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0'>" +
                                "<entity name='quotedetail'>"+
                                "<attribute name='productid'/>" +
                                  "<attribute name='uomid'/>" +
                                "<attribute name='priceperunit'/>"+
                                "<attribute name='quantity'/>"+
                                "<attribute name='quotedetailid'/>"+
                                "<attribute name='bsd_vatprice'/>" +
                                 "<attribute name='bsd_amount'/>" +
                                  "<attribute name='baseamount'/>" +
                                 "<attribute name='tax'/>"+
                                    "<order attribute='productid' descending='false' />"+
                                    "<link-entity name='quote' from='quoteid' to='quoteid' alias='ak'>"+
                                    "<filter type='and'>" +
                                        "<condition attribute='quoteid' operator='eq' uitype='quote' value='"+ lookup[0].id +"' />" +
                                     "</filter>" +
                                     "</link-entity>"+
                                "</entity>" +
                            "</fetch>";
            if (objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", FetchXml);
                objSubGrid.control.Refresh();
            } else {
                setTimeout(filter_subgriddkbylookuporder, 500);
            }
        }
    }
}
//Author:Mr.Phong
//Description:lay thong tin tu nguoi dai dien ben b
function get_thongtinchucvu() {
    var present = Xrm.Page.getAttribute("bsd_nguoidaidienb").getValue();
    var xml = [];
    if (present != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='contact'>");
        xml.push("<attribute name='fullname'/>");
        xml.push("<attribute name='telephone1'/>")
        xml.push("<attribute name='contactid'/>");
        xml.push("<attribute name='jobtitle'/>");
        xml.push("<order attribute='fullname' descending='true' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='contactid' operator='eq' uitype='contact'  value='" + present[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.jobtitle != null) {
                    Xrm.Page.getAttribute("bsd_jobtitle").setValue(rs[0].attributes.jobtitle.value);
                }
            }
        },
         function (er) {
             console.log(er.message)
         });
    }
    if (present == null) {
        Xrm.Page.getAttribute("bsd_jobtitle").setValue(null);
    }
}
//Author:Mr.Phong
//Description:lay thong tin tu nguoi dai dien ben a
function get_thongtinchucvua() {
    var present = Xrm.Page.getAttribute("bsd_nguoidaidiena").getValue();
    var xml = [];
    if (present != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='contact'>");
        xml.push("<attribute name='fullname'/>");
        xml.push("<attribute name='telephone1'/>")
        xml.push("<attribute name='contactid'/>");
        xml.push("<attribute name='jobtitle'/>");
        xml.push("<order attribute='fullname' descending='true' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='contactid' operator='eq' uitype='contact'  value='" + present[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.jobtitle != null) {
                    Xrm.Page.getAttribute("bsd_jobtitlea").setValue(rs[0].attributes.jobtitle.value);
                }
            }
        },
         function (er) {
             console.log(er.message)
         });
    }
    if (present == null) {
        Xrm.Page.getAttribute("bsd_jobtitlea").setValue(null);
    }
}
//author:Mr.Phong
function get_warehouseadd() {
    var warehouse = Xrm.Page.getAttribute("bsd_warehouse").getValue();
    var fielddefault = Xrm.Page.getAttribute("bsd_warehouseaddress");
    var xml = [];
    if (fielddefault.getValue() == null)
    {
        if (warehouse != null) {
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_address'>");
            xml.push("<attribute name='bsd_addressid'/>");
            xml.push("<attribute name='bsd_name'/>")
            xml.push("<attribute name='createdon'/>");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<link-entity name='bsd_warehouseentity' from='bsd_address' to='bsd_addressid' alias='ag'>");
            xml.push("<filter type='and'>");
            xml.push("<condition attribute='bsd_warehouseentityid' operator='eq' uitype='bsd_warehouseentity' value='" + warehouse[0].id + "' />");
            xml.push("</filter>");
            xml.push("</link-entity>");
            xml.push("</entity>");
            xml.push("</fetch>");
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    fielddefault.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
            },
           function (er) {
               console.log(er.message)
           });
        }
        if (warehouse == null) {
            fielddefault.setValue(null);
        }
    }
}
//Author:Mr.Phong
//Description:get principal contract to economic contract
function get_principalcontractname() {
    Xrm.Page.getAttribute("bsd_principalcontractno").setSubmitMode("always");
    var customer = Xrm.Page.getAttribute("bsd_account").getValue();
    var fieldprincipalcontract = Xrm.Page.getAttribute("bsd_principalcontractname");
    var fieldprincipalcontractno = Xrm.Page.getAttribute("bsd_principalcontractno");
    var xml = [];
    if (fieldprincipalcontract.getValue() == null && fieldprincipalcontractno.getValue() == null) {
        if (customer != null) {
            var viewId = "{3F64F691-ADFF-4D66-87B5-CD996739E2C6}";
            var entityName = "bsd_principalcontract";
            var viewDisplayName = "test";
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_principalcontract'>");
            xml.push("<attribute name='bsd_principalcontractid'/>");
            xml.push("<attribute name='bsd_name'/>")
            xml.push("<attribute name='createdon'/>");
            xml.push("<attribute name='bsd_code'/>");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and'>");
            xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='" + customer[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_principalcontractid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_principalcontractid'>  " +
                             "<cell name='bsd_name'    " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
            Xrm.Page.getControl("bsd_principalcontractname").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    fieldprincipalcontract.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                    fieldprincipalcontractno.setValue(rs[0].attributes.bsd_code.value)
                }
                else {
                    fieldprincipalcontract.setValue(null);
                    fieldprincipalcontractno.setValue(null);
                }
            },
        function (er) {
            console.log(er.message)
        });
        }
    }
}
//Author:Mr.Phong
//Description:get principal contract no to economic contract
function get_principalcontractno() {
    var principalcontract = Xrm.Page.getAttribute("bsd_principalcontractname").getValue();
    var fieldprincipalcontract = Xrm.Page.getAttribute("bsd_principalcontractno");
    var xml = [];
    if (principalcontract != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_principalcontract'>");
        xml.push("<attribute name='bsd_principalcontractid'/>");
        xml.push("<attribute name='bsd_name'/>")
        xml.push("<attribute name='createdon'/>");
        xml.push("<attribute name='bsd_code'/>");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='bsd_principalcontractid' operator='eq' uitype='bsd_principalcontract' value='" + principalcontract[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                fieldprincipalcontract.setValue(rs[0].attributes.bsd_code.value)
            }
        },
     function (er) {
         console.log(er.message)
     });
    }
    else {
        fieldprincipalcontract.setValue(null);
    }
}
//auhthor:Mr Phong
function set_defaultaddressonload() {
    if (Xrm.Page.ui.getFormType() == 2) {
        var customer = Xrm.Page.getAttribute("bsd_account").getValue();
        var fiedpaymentterm = Xrm.Page.getAttribute("bsd_paymenttermsa");
        var fiedpaymentmethod = Xrm.Page.getAttribute("bsd_paymentmethod");
        var contactdefault = Xrm.Page.getAttribute("bsd_nguoidaidienb");
        var xml = [];
        var xml2 = [];
        var xml3 = [];
        var xml4 = [];
        var xml5 = [];
        //if (fiedpaymentterm.getValue() != null || fiedpaymentmethod.getValue() != null || contactdefault.getValue() != null) {
            var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
            var entityName = "bsd_address";
            var viewDisplayName = "test";
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_address'>");
            xml.push("<attribute name='bsd_addressid' />");
            xml.push("<attribute name='bsd_name'/>");
            xml.push("<attribute name='createdon'/>");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='" + customer[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_addressid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
            Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            var viewId1 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
            var entityName1 = "contact";
            var viewDisplayName1 = "test";
            xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml4.push("<entity name='contact'>");
            xml4.push("<attribute name='fullname'/>");
            xml4.push("<attribute name='telephone1'/>")
            xml4.push("<attribute name='contactid'/>");
            xml4.push("<order attribute='fullname' descending='true' />");
            xml4.push("<filter type='and'>");
            xml4.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
            xml4.push("</filter>");
            xml4.push("</entity>");
            xml4.push("</fetch>");
            var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                "<row name='result'  " + "id='contactid'>  " +
                "<cell name='fullname'   " + "width='200' />  " +
                "<cell name='telephone1'    " + "width='100' />  " +
                "</row>   " +
                "</grid>   ";
            Xrm.Page.getControl("bsd_nguoidaidienb").addCustomView(viewId1, entityName1, viewDisplayName1, xml4.join(""), layoutXml1, true);
        //}
    }
}
//author:Mr.Phong
function get_nameonload() {
    if (Xrm.Page.ui.getFormType() == 2) {
        var contactdefault = Xrm.Page.getAttribute("bsd_nguoidaidiena");
        Xrm.Page.getAttribute("bsd_renter").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_addressrenter").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_phonerenter").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_taxregistrationrenter").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_faxrenter").setSubmitMode("always");
        Xrm.Page.getAttribute("bsd_warehouseaddress").setSubmitMode("always");
        var fielddefault = Xrm.Page.getAttribute("bsd_renter");
        var addressdefault = Xrm.Page.getAttribute("bsd_addressrenter");
        var xml = [];
        var xml1 = [];
        var xml2 = [];
        var xml4 = [];
        var xml5 = [];
        if (fielddefault.getValue() != null || addressdefault.getValue() != null || contactdefault.getValue() == null) {
            var viewId1 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
            var entityName1 = "contact";
            var viewDisplayName1 = "test";
            xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml4.push("<entity name='contact'>");
            xml4.push("<attribute name='fullname'/>");
            xml4.push("<attribute name='telephone1'/>")
            xml4.push("<attribute name='contactid'/>");
            xml4.push("<order attribute='fullname' descending='true' />");
            xml4.push("<filter type='and'>");
            xml4.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='{A30A205A-EF94-E611-80CC-000C294C7A2D}' />");
            xml4.push("</filter>");
            xml4.push("</entity>");
            xml4.push("</fetch>");
            var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                "<row name='result'  " + "id='contactid'>  " +
                "<cell name='fullname'   " + "width='200' />  " +
                "<cell name='telephone1'    " + "width='100' />  " +
                "</row>   " +
                "</grid>   ";
            Xrm.Page.getControl("bsd_nguoidaidiena").addCustomView(viewId1, entityName1, viewDisplayName1, xml4.join(""), layoutXml1, true);
        }
    }
}
//Author:Mr.Phong
//Description:filter bank account theo ben A
function filter_bankaccounta() {
    var bankaccount = Xrm.Page.getAttribute("bsd_bankaccounta");
    var bankgroup = Xrm.Page.getAttribute("bsd_bankgroupa");
    var bankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbera");
    var xml = [];
    var viewId = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
    var entityName = "bsd_bankaccount";
    var viewDisplayName = "test";
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='bsd_bankaccount'>");
    xml.push("<attribute name='bsd_name'/>");
    xml.push("<attribute name='createdon'/>");
    xml.push("<attribute name='bsd_brand'/>");
    xml.push("<attribute name='bsd_bankaccount'/>");
    xml.push("<attribute name='bsd_account'/>");
    xml.push("<attribute name='bsd_bankaccountid'/>");
    xml.push("<attribute name='bsd_bankgroup'/>");
    xml.push("<order attribute='bsd_name' descending='true' />");
    xml.push("<filter type='and'>");
    xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='{6BE4B61C-13B9-E611-93F1-000C29D47EAB}' />");
    xml.push("<condition attribute='statecode' operator='eq'  value='0' />");
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
        "<row name='result'  " + "id='bsd_bankaccountid'>  " +
        "<cell name='bsd_name'   " + "width='200' />  " +
        "<cell name='createdon'    " + "width='100' />  " +
        "<cell name='bsd_brand'    " + "width='100' />  " +
        "<cell name='bsd_bankaccount'    " + "width='100' />  " +
        "<cell name='bsd_account'    " + "width='100' />  " +
        "<cell name='bsd_bankgroup'    " + "width='100' />  " +
        "</row>   " +
        "</grid>   ";
    Xrm.Page.getControl("bsd_bankaccounta").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    if (bankaccount.getValue() == null) {
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                bankaccount.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.bsd_name.value,
                    entityType: rs[0].logicalName
                }]);
                bankgroup.setValue([{
                    id: rs[0].attributes.bsd_bankgroup.guid,
                    name: rs[0].attributes.bsd_bankgroup.name,
                    entityType: rs[0].attributes.bsd_bankgroup.logicalName
                }]);
                bankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);

            }
        },
                   function (er) {
                       console.log(er.message)
                   });
    }
}
//Author:Mr.Phong
//Description:filter bank account theo ben b
function get_allbankaccountfromsitebonload() {
    if (Xrm.Page.ui.getFormType() == 2) {
        var bankaccount = Xrm.Page.getAttribute("bsd_bankaccountb");
        var account = Xrm.Page.getAttribute("bsd_account").getValue();
        var bankgroup = Xrm.Page.getAttribute("bsd_bankgroupb");
        var bankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumberb");
        if (account != null) {
            var xml = [];
            var viewId = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
            var entityName = "bsd_bankaccount";
            var viewDisplayName = "test";
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_bankaccount'>");
            xml.push("<attribute name='bsd_name'/>");
            xml.push("<attribute name='createdon'/>");
            xml.push("<attribute name='bsd_brand'/>");
            xml.push("<attribute name='bsd_bankaccount'/>");
            xml.push("<attribute name='bsd_account'/>");
            xml.push("<attribute name='bsd_bankaccountid'/>");
            xml.push("<attribute name='bsd_bankgroup'/>");
            xml.push("<order attribute='bsd_name' descending='true' />");
            xml.push("<filter type='and'>");
            xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='" + account[0].id + "' />");
            xml.push("<condition attribute='statecode' operator='eq'  value='0' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
           "<row name='result'  " + "id='bsd_bankaccountid'>  " +
           "<cell name='bsd_name'   " + "width='200' />  " +
           "<cell name='createdon'    " + "width='100' />  " +
           "<cell name='bsd_brand'    " + "width='100' />  " +
           "<cell name='bsd_bankaccount'    " + "width='100' />  " +
           "<cell name='bsd_account'    " + "width='100' />  " +
           "</row>   " +
           "</grid>   ";
            Xrm.Page.getControl("bsd_bankaccountb").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            if (bankaccount.getValue() == null) {
                CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        bankaccount.setValue([{
                            id: rs[0].Id,
                            name: rs[0].attributes.bsd_name.value,
                            entityType: rs[0].logicalName
                        }]);
                        bankgroup.setValue([{
                            id: rs[0].attributes.bsd_bankgroup.guid,
                            name: rs[0].attributes.bsd_bankgroup.name,
                            entityType: rs[0].attributes.bsd_bankgroup.logicalName
                        }]);
                        bankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                    }
                },
          function (er) {
              console.log(er.message)
          });
            }
        }
    }
}
//Author:Mr.Phong
//Description:load bank account number and bank group from bank acccount site a
function get_bankaccountnumberandbankgroupa() {
    var renter = Xrm.Page.getAttribute("bsd_renter").getValue();
    var bankaccount = Xrm.Page.getAttribute("bsd_bankaccounta").getValue();
    var bankgroup = Xrm.Page.getAttribute("bsd_bankgroupa");
    var bankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbera");
    var xml = [];
    if (bankaccount != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_bankaccount'>");
        xml.push("<attribute name='bsd_name'/>");
        xml.push("<attribute name='createdon'/>");
        xml.push("<attribute name='bsd_brand'/>");
        xml.push("<attribute name='bsd_bankaccount'/>");
        xml.push("<attribute name='bsd_account'/>");
        xml.push("<attribute name='bsd_bankaccountid'/>");
        xml.push("<attribute name='bsd_bankgroup'/>");
        xml.push("<order attribute='bsd_name' descending='true' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='bsd_bankaccountid' operator='eq' uitype='bsd_bankaccount' value='" + bankaccount[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                bankgroup.setValue([{
                    id: rs[0].attributes.bsd_bankgroup.guid,
                    name: rs[0].attributes.bsd_bankgroup.name,
                    entityType: rs[0].attributes.bsd_bankgroup.logicalName
                }]);
                bankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
            }
        },
      function (er) {
          console.log(er.message)
      });
    }
    else {
        bankgroup.setValue(null);
        bankaccountnumber.setValue(null);

    }
    if (renter == null) {
        Xrm.Page.getAttribute("bsd_bankaccounta").setValue(null);
    }
}
//Author:Mr.Phong
//Description:load bank account number and bank group from bank acccount site b
function get_bankaccountnumberandbankgroupb() {
    var account = Xrm.Page.getAttribute("bsd_account").getValue();
    var bankaccount = Xrm.Page.getAttribute("bsd_bankaccountb").getValue();
    var bankgroup = Xrm.Page.getAttribute("bsd_bankgroupb");
    var bankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumberb");
    var xml = [];
    if (bankaccount != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_bankaccount'>");
        xml.push("<attribute name='bsd_name'/>");
        xml.push("<attribute name='createdon'/>");
        xml.push("<attribute name='bsd_brand'/>");
        xml.push("<attribute name='bsd_bankaccount'/>");
        xml.push("<attribute name='bsd_account'/>");
        xml.push("<attribute name='bsd_bankaccountid'/>");
        xml.push("<attribute name='bsd_bankgroup'/>");
        xml.push("<order attribute='bsd_name' descending='true' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='bsd_bankaccountid' operator='eq' uitype='bsd_bankaccount' value='" + bankaccount[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                bankgroup.setValue([{
                    id: rs[0].attributes.bsd_bankgroup.guid,
                    name: rs[0].attributes.bsd_bankgroup.name,
                    entityType: rs[0].attributes.bsd_bankgroup.logicalName
                }]);
                bankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
            }
        },
      function (er) {
          console.log(er.message)
      });
    }
    else {
        bankgroup.setValue(null);
        bankaccountnumber.setValue(null);
    }
    if (account == null) {
        Xrm.Page.getAttribute("bsd_bankaccountb").setValue(null);
    }
}
//Author:Mr.Phong
//Description:set currency from quote
function setcurrencyfromquote()
{
    var quote = Xrm.Page.getAttribute("bsd_quote").getValue();
    var currencies = Xrm.Page.getAttribute("bsd_currency").getValue();
    var currency = Xrm.Page.getAttribute("bsd_currency");
    if (quote!= null && currencies ==null)
    {
        var xml = [];
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='quote'>");
        xml.push("<attribute name='name'/>");
        xml.push("<attribute name='customerid'/>");
        xml.push("<attribute name='statecode'/>");
        xml.push("<attribute name='totalamount'/>");
        xml.push("<attribute name='quoteid'/>");
        xml.push("<attribute name='createdon'/>");
        xml.push("<attribute name='transactioncurrencyid'/>");
        xml.push("<order attribute='name' descending='false' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='quoteid' operator='eq'  value='" + quote[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                currency.setValue([{
                    id: rs[0].attributes.transactioncurrencyid.guid,
                    name: rs[0].attributes.transactioncurrencyid.name,
                    entityType: rs[0].attributes.transactioncurrencyid.logicalName
                }]);
            }
        },
      function (er) {
          console.log(er.message)
      });
    }
}