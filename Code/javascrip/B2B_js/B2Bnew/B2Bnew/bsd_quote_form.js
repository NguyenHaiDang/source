var formtype = 1;
//Description:Load form
function init() {
    var statecode = Xrm.Page.getAttribute("statecode").getValue();
    //1 la active
    if (statecode != 1 && statecode != 3 && statecode != 2) {
        setaccounttypecustomer();
        set_currentdate();
        set_addressonload();
        setshiptoaddressonload();
        //showBusinessProcessFlowonload();
        switchProcess();
        showhide_fiedshiptofreightterms();
        changestatuswhenstagefinalonload();
        set_DisableDeliveryPort();
        setdisableallfield();
    }
    filter_gridQuotebyID();
    hide_button();
    //Trung.
    if (formType() == 1) {
        setDisabled(["bsd_address", "bsd_addressinvoiceaccount", "bsd_shiptoaddress", "bsd_contact"], true);
        setRequired("quotenumber", "none");
        load_unitdefault(true);
        //load_currencydefault(true);
        setTimeout(function () { Xrm.Page.getControl("bsd_quotationname").setFocus(); }, 1000);

        check_enable_shipping();
    } else {
        load_unitdefault(false);
        //load_currencydefault(false);
        check_enable_shipping(false);
        porteroption_change(false);
    }
 Xrm.Page.getControl("header_process_bsd_totalamount").setDisabled(true);
    // End Trung
}
//Trung
function AutoLoading() {
    var statecode = Xrm.Page.getAttribute("statecode").getValue();
    if (statecode != 1 && statecode != 3) { // 1 laf active
        if (formType() == 2) {
            LoadAddressPort(false);
        } else {
            Load_FromDate_Auto();
            clear_LoadAddressPort();
        }
    }
}
function check_enable_shipping(reset) {
    var statecode = Xrm.Page.getAttribute("statecode").getValue();
    // call when customername or warehouse or address change.
    var customername = getValue("customerid");
    var warehousefrom = getValue("bsd_warehouseto");
    var warehouseaddress = getValue("bsd_warehouseaddress");
    var shiptoaddress = getValue("bsd_shiptoaddress");
    var effectivefrom = getValue("effectivefrom");
    var effectiveto = getValue("effectiveto");
    if (customername != null && warehousefrom != null && shiptoaddress != null && warehouseaddress != null && effectivefrom != null && effectiveto != null) {
        setDisabled("bsd_transportation", false);
    } else {
        setValue("bsd_transportation", false);
        setDisabled("bsd_transportation", true);
    }
    shipping_change(reset);
}
function shipping_change(reset) {
    clearNotification("bsd_shippingpricelistname");
    if (reset != false) {
        setNull(["bsd_truckload", "bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
    }

    var bsd_transportation = getValue("bsd_transportation");
    if (bsd_transportation) { // có ship

        setRequired(["bsd_partnerscompany", "bsd_shiptoaddress"], "required");

        setVisible(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], true);
        setRequired(["bsd_shippingdeliverymethod", "bsd_shippingpricelistname", "bsd_priceoftransportationn"], "required");
        shipping_deliverymethod_change(reset);
    } else {
        setVisible(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], false);
        setRequired(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], "none");

        setRequired(["bsd_partnerscompany", "bsd_shiptoaddress"], "none");

        if (reset != false) {
            setValue("bsd_shippingporter", false);
            porteroption_change(reset);
        }
    }
}
function requestporter_change(reset) {
    shipping_change(reset);
}
function shipping_deliverymethod_change(reset) {

    if (reset != false) setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"]);
    var method;
    if (getValue("bsd_shippingdeliverymethod") == null) {
        setValue("bsd_shippingdeliverymethod", 861450000);
        method = 861450000;
    } else {
        method = getValue("bsd_shippingdeliverymethod");
    }

    if (method == 861450000) { // ton
        setVisible("bsd_unitshipping", true);
        setRequired("bsd_unitshipping", "required");
        if (getValue("bsd_unitshipping") == null) {
            setValue("bsd_unitshipping", [{
                id: "{886009E0-0095-E611-80CC-000C294C7A2D}",
                name: "Tấn",
                entityType: "uom"
            }]);
        }


        setVisible("bsd_truckload", false);
        setNull("bsd_truckload");
        setRequired("bsd_truckload", "none");

        load_shippingpricelist_ton(reset);

    } else if (861450001) { // trip

        setVisible("bsd_unitshipping", false);
        setNull("bsd_unitshipping");
        setRequired("bsd_unitshipping", "none");

        setVisible("bsd_truckload", true);
        setRequired("bsd_truckload", "required");

        if (reset != false) {
            setNull("bsd_truckload");
        }
        load_truckload(reset);
        truckload_change(reset);
    }
}
function shipping_pricelist_change(reset) {
    // code
    if (reset != false) setNull("bsd_priceoftransportationn");
    var pricelist = getValue("bsd_shippingpricelistname");
    if (pricelist != null) {

    } else if (reset != false) {

    }
    shipping_price_change();
}
function load_shippingpricelist_ton(reset) {
    if (reset != false) {
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var effective_from = getValue("effectivefrom");
        var effective_to = getValue("effectiveto");

        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();
        var district_to = null;
        var ward_to = null;
        // lấy district + ward từ Ship To Address
        var fetch_get_shiptoaddress = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_address">',
            '<attribute name="bsd_addressid" />',
            '<attribute name="bsd_ward" />',
            '<attribute name="bsd_district" />',
            '<filter type="and">',
              '<condition attribute="bsd_addressid" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetch_get_shiptoaddress, false).then(function (rs) {
            if (rs.length > 0 && rs[0].getValue("bsd_district") != null && rs[0].getValue("bsd_ward") != null) {
                district_to = rs[0].getValue("bsd_district");
                ward_to = rs[0].getValue("bsd_ward");
            }
        });

        var data = null;

        if (request_porter == true) {
            // Có yêu cầu bốc xếp
            //  // nếu có yêu cầu porter, và có theo xã.
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceunitporter" />',
                        '<order attribute="bsd_priceunitporter" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                          '<condition attribute="bsd_priceunitporter" operator="not-null" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");

            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_priceunitporter.value,
                        porter: true
                    };
                } else {
                    // nếu không có theo xã thì lấy theo quận  huyện
                    xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceunitporter" />',
                        '<order attribute="bsd_priceunitporter" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                          '<condition attribute="bsd_priceunitporter" operator="not-null" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                          '<condition attribute="bsd_wardto" operator="null" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
                    CrmFetchKit.Fetch(xml, false).then(function (rs) {
                        if (rs.length > 0) {
                            data = {
                                pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                                pricelist_name: rs[0].attributes.bsd_name.value,
                                price: rs[0].attributes.bsd_priceunitporter.value,
                                porter: true
                            };
                        } else {
                            data = null;
                        }
                    });

                }
            });
        }

        if (request_porter == false || (request_porter == true && data == null)) {
            // Không yêu cầu giao hàng, hoặc là yêu cầu mà không có !
            // Lấy theo xã. + Không lấy giá porter
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceofton" />',
                        '<order attribute="bsd_priceofton" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                          '<condition attribute="bsd_priceofton" operator="not-null" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_priceofton.value,
                        porter: false
                    };
                } else {
                    // Không có theo xã thì lấy theo huyện, không yêu cầu hoặc yêu cầu mà không có !
                    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceofton" />',
                        '<order attribute="bsd_priceofton" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                          '<condition attribute="bsd_priceofton" operator="not-null" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                          '<condition attribute="bsd_wardto" operator="null" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
                    CrmFetchKit.Fetch(xml, false).then(function (rs) {
                        if (rs.length > 0) {
                            data = {
                                pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                                pricelist_name: rs[0].attributes.bsd_name.value,
                                price: rs[0].attributes.bsd_priceofton.value,
                                porter: false
                            };
                        } else {
                            data = null;
                        }
                    });
                }
            });
        }

        if (data != null) {
            set_shipping_pricelist(data);
            clearNotification("bsd_shippingpricelistname");
        } else {
            set_shipping_pricelist(null);
        }
    } else {
        porteroption_change(reset);
    }
}
function set_shipping_pricelist(data) {
    if (data != null) {
        setValue("bsd_priceoftransportationn", data.price);
        setValue("bsd_shippingpricelistname", [{
            id: data.pricelist_id,
            name: data.pricelist_name,
            entityType: "bsd_shippingpricelist"
        }]);
        if (data.porter == true) {
            setValue("bsd_shippingporter", true);
            setValue("bsd_porter", true);
        } else {
            setValue("bsd_shippingporter", false);
        }
        if (getValue("bsd_shippingpricelistname") == null) {
            setTimeout(function () {
                set_shipping_pricelist(data);
            }, 50);
        } else {
            porteroption_change(true);
        }
    } else {
        setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
        porteroption_change(true);
    }
}
function shipping_price_change() {
    // validate price
    var price = getValue("bsd_priceoftransportationn");
    if (price == null) {
        //setNotification("bsd_priceoftransportationn", "You must provide a value for Price");
    } else if (price <= 0) {
        setNotification("bsd_priceoftransportationn", "Enter a value from 0");
    } else {
        clearNotification("bsd_priceoftransportationn");
    }
}
function load_truckload(reset) {
    var shiptoaddress = getValue("bsd_shiptoaddress");
    var warehouse_address = getValue("bsd_warehouseaddress");

    var xml_shiptoaddress = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
     '<entity name="bsd_address">',
        '<attribute name="bsd_addressid" />',
        '<attribute name="bsd_ward" />',
        '<attribute name="bsd_province" />',
        '<attribute name="bsd_district" />',
        '<order attribute="bsd_ward" descending="false" />',
        '<filter type="and">',
          '<condition attribute="bsd_addressid" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    CrmFetchKit.Fetch(xml_shiptoaddress, false).then(function (rs) {
        if (rs.length > 0) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
             '<entity name="bsd_truckload">',
               '<attribute name="bsd_truckloadid" />',
               '<attribute name="bsd_name" />',
               '<order attribute="bsd_name" descending="false" />',
               '<link-entity name="bsd_shippingpricelist" from="bsd_truckload" to="bsd_truckloadid" alias="ab">',
                 '<filter>',
                   '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                   '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouse_address[0].id + '" />',
                   '<filter type="or">',
                       '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + rs[0].getValue("bsd_district") + '" />',
                       '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + rs[0].getValue("bsd_ward") + '" />',
                   '</filter>',
                 '</filter>',
               '</link-entity>',
             '</entity>',
           '</fetch>'].join("");
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_truckloadid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                                    "<row name='result'  " + "id='bsd_truckloadid'>  " +
                                                    "<cell name='bsd_name'   " + "width='200' />  " +
                                                    "</row>   " +
                                                 "</grid>   ";

            getControl("bsd_truckload").addCustomView(getDefaultView("bsd_truckload"), "bsd_truckload", "bsd_truckload", xml, layoutXml, true);
        }
    });

}
function truckload_change(reset) {
    // load shipping theo trip
    if (reset != false) {
        setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
    }

    var truckload = getValue("bsd_truckload");
    if (truckload != null && reset != false) {
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var effective_from = getValue("effectivefrom");
        var effective_to = getValue("effectiveto");
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var effective_from = getValue("effectivefrom");
        var effective_to = getValue("effectiveto");

        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();
        var district_to = null;
        var ward_to = null;
        // lấy district + ward từ Ship To Address
        var fetch_get_shiptoaddress = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_address">',
            '<attribute name="bsd_addressid" />',
            '<attribute name="bsd_ward" />',
            '<attribute name="bsd_district" />',
            '<filter type="and">',
              '<condition attribute="bsd_addressid" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetch_get_shiptoaddress, false).then(function (rs) {
            if (rs.length > 0 && rs[0].getValue("bsd_district") != null && rs[0].getValue("bsd_ward") != null) {
                district_to = rs[0].getValue("bsd_district");
                ward_to = rs[0].getValue("bsd_ward");
            }
        });


        if (request_porter == true) {
            // Có yêu cầu bốc xếp
            //  // nếu có yêu cầu porter, và có theo xã.
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_pricetripporter" />',
                        '<order attribute="bsd_pricetripporter" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                          '<condition attribute="bsd_pricetripporter" operator="not-null" />',
                          '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");

            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_pricetripporter.value,
                        porter: true
                    };
                } else {
                    // nếu không có theo xã thì lấy theo quận  huyện
                    xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_pricetripporter" />',
                        '<order attribute="bsd_pricetripporter" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                          '<condition attribute="bsd_pricetripporter" operator="not-null" />',
                          '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                          '<condition attribute="bsd_wardto" operator="null" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
                    CrmFetchKit.Fetch(xml, false).then(function (rs) {
                        if (rs.length > 0) {
                            data = {
                                pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                                pricelist_name: rs[0].attributes.bsd_name.value,
                                price: rs[0].attributes.bsd_pricetripporter.value,
                                porter: true
                            };
                        } else {
                            data = null;
                        }
                    });

                }
            });
        }

        if (request_porter == false || (request_porter == true && data == null)) {
            // Không yêu cầu giao hàng, hoặc là yêu cầu mà không có !
            // Lấy theo xã. + Không lấy giá porter
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceoftrip" />',
                        '<order attribute="bsd_priceoftrip" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                          '<condition attribute="bsd_priceoftrip" operator="not-null" />',
                          '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_wardto" operator="eq" uitype="bsd_ward" value="' + ward_to + '" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    data = {
                        pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                        pricelist_name: rs[0].attributes.bsd_name.value,
                        price: rs[0].attributes.bsd_priceoftrip.value,
                        porter: false
                    };
                } else {
                    // Không có theo xã thì lấy theo huyện, không yêu cầu hoặc yêu cầu mà không có !
                    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="bsd_shippingpricelist">',
                        '<attribute name="bsd_shippingpricelistid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_priceoftrip" />',
                        '<order attribute="bsd_priceoftrip" descending="true" />',
                        '<filter type="and">',
                          '<condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                          '<condition attribute="bsd_priceoftrip" operator="not-null" />',
                          '<condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                          '<condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                          '<condition attribute="bsd_districtto" operator="eq" uitype="bsd_district" value="' + district_to + '" />',
                          '<condition attribute="bsd_wardto" operator="null" />',
                          '<condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                          ' <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                        '</filter>',
                      '</entity>',
                    '</fetch>'].join("");
                    CrmFetchKit.Fetch(xml, false).then(function (rs) {
                        if (rs.length > 0) {
                            data = {
                                pricelist_id: rs[0].getValue("bsd_shippingpricelistid"),
                                pricelist_name: rs[0].attributes.bsd_name.value,
                                price: rs[0].attributes.bsd_priceoftrip.value,
                                porter: false
                            };
                        } else {
                            data = null;
                        }
                    });
                }
            });
        }

        if (data != null) {
            set_shipping_pricelist(data);
            clearNotification("bsd_shippingpricelistname");
        } else {
            set_shipping_pricelist(null);
        }
    } else {
        porteroption_change(reset);
    }
}
function load_warehouse_address(reset) {

    if (reset != false) setNull("bsd_warehouseaddress");

    var warehouse = getValue("bsd_warehouseto");
    if (warehouse != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="bsd_warehouseentity">',
        '<attribute name="bsd_warehouseentityid" />',
        '<attribute name="bsd_address" />',
        '<filter type="and">',
          '<condition attribute="bsd_warehouseentityid" operator="eq" uitype="bsd_warehouseentity" value="' + warehouse[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && reset != false) {
                var first = rs[0];
                console.log(first);
                setValue("bsd_warehouseaddress", [{
                    id: first.attributes.bsd_address.guid,
                    name: first.attributes.bsd_address.name,
                    entityType: first.attributes.bsd_address.logicalName
                }]);
            }
        });
    }
    check_enable_shipping(reset);
}
function porteroption_change(reset) {
    //if (reset != false) setNull(["bsd_priceofporter", "bsd_pricepotter"]);
    var request_porter = getValue("bsd_requestporter");
    var porteroption = getValue("bsd_porteroption");
    var shipping_porter = getValue("bsd_shippingporter");
    if (request_porter == false) {
        // Không yêu cầu !
        setDisabled(["bsd_porteroption"], true);

        setVisible(["bsd_priceofporter", "bsd_pricepotter"], false);
        setRequired(["bsd_priceofporter", "bsd_pricepotter"], "none");
        if (reset != false) {
            setValue("bsd_porteroption", false);
            setValue("bsd_porter", 861450001);
            setNull(["bsd_priceofporter", "bsd_pricepotter"]);
        }

    } else if (request_porter == true) { // Có yêu cầu
        if (shipping_porter == true) { // Giá đã gồm Porter
            setDisabled(["bsd_porteroption"], true);
            setVisible(["bsd_priceofporter", "bsd_pricepotter"], false);
            setRequired(["bsd_priceofporter", "bsd_pricepotter"], "none");
            if (reset != false) {
                setValue("bsd_porteroption", true);
                setValue("bsd_porter", 861450000);
                setNull(["bsd_priceofporter", "bsd_pricepotter"]);
            }
        } else { // Không có giá  porter
            setDisabled(["bsd_porteroption"], true);
            setVisible(["bsd_priceofporter", "bsd_pricepotter"], true);
            setRequired(["bsd_priceofporter", "bsd_pricepotter"], "required");
            if (reset != false) {
                setValue("bsd_porteroption", true);
                setValue("bsd_porter", 861450000);
            }
            load_porter();
            porterprice_change(reset);
        }
    }
}
function porterprice_change(reset) {
    if (reset != false) {
        var bsd_priceofporter = getValue("bsd_priceofporter");
        if (bsd_priceofporter != null) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_porter">',
                '<attribute name="bsd_porterid" />',
                '<attribute name="bsd_pricee" />',
                '<order attribute="bsd_pricee" descending="false" />',
                '<filter type="and">',
                  '<condition attribute="bsd_porterid" operator="eq" uitype="bsd_porter" value="' + bsd_priceofporter[0].id + '" />',
                  '<condition attribute="bsd_pricee" operator="not-null" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    setValue("bsd_pricepotter", first.getValue("bsd_pricee"));
                }
            });
        } else {
            setNull("bsd_pricepotter");
        }
    }
}
function check_date() {
    // effective date
    clearFormNotification("1");
    clearNotification("bsd_date");

    var effective_from = getValue("effectivefrom");
    var currentDateTime = getCurrentDateTime();

    if (effective_from != null) {

        var date = getValue("bsd_date");
        if (effective_from < currentDateTime) {
            setFormNotification("The Effective From Date cannot occur after current date", "INFO", "1");
            setValue("effectivefrom", currentDateTime);
        }
        if (date > effective_from) {
            setNotification("bsd_date", "The Effective From Date cannot occur after Date");
        }
    }
    check_enable_shipping(true);
    check_fromdate_todate();
}
function getCurrentDateTime() {
    var d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
}
function check_fromdate_todate() {
    clearNotification("bsd_fromdate");
    clearNotification("bsd_todate");
    var from = getValue("bsd_fromdate");
    var to = getValue("bsd_todate");
    var effective_from = getValue("effectivefrom")
    var currentDateTime = getCurrentDateTime();
    if (from != null) {
        if (from <= effective_from) {
            setNotification("bsd_fromdate", "The From Date is must after the Effective From");
        }
    }
    if (to != null) {
        if (to <= effective_from) {
            setNotification("bsd_todate", "The To Date is must after the Effective From");
        }
    }
    if (from != null && to != null) {
        if (from > to) {
            setNotification("bsd_fromdate", "The From Date cannot occur before the To Date");
        }
    }
}
function onsave(econtext) {
    var bsd_transportation = getValue("bsd_transportation");
    if (bsd_transportation == true) {
        var pricelist = getValue("bsd_shippingpricelistname");
        if (pricelist == null) {
            setNotification("bsd_shippingpricelistname", "You must provide a value for Price List !");
        }
    }
}
function getDate(d) {
    return new Date('' + d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear() + ' 12:00:00 AM');
}
function modified_on_change() {
    shipping_change(false);
    if (formtype == 1) {
        Xrm.Page.data.save();
    }
}
function btnSubOrder() {
    var xml;
    var quote_type = getValue("bsd_quotationtype"); //TH bán Mật rỉ: không kiểm tra số lượng trên SUb Order phải nhỏ hơn số lượng còn lại trên Order
    if (quote_type == 861450003) {
        xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="quotedetail">',
                '<attribute name="quotedetailid" />',
                '<filter type="and">',
                  '<condition attribute="quoteid" operator="eq" uitype="quote" value="' + getId() + '" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join("");
    }
    else {
        xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="quotedetail">',
                '<attribute name="quotedetailid" />',
                '<filter type="and">',
                  '<condition attribute="quoteid" operator="eq" uitype="quote" value="' + getId() + '" />',
                  '<condition attribute="bsd_remainingquantity" operator="gt" value="0" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join("");
    }
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            if (confirm("Are you sure to create new sub order ?")) {
                ExecuteAction(getId(), "quote", "bsd_Action_QuoteToSubOrder", null, function (result) {
                    if (result != null && result.status != null) {
                        if (result.status == "success") {
                            reload_page();
                        } else if (result.status == "error") {
                            alert(result.data);
                        } else {
                            alert(result.data);
                        }
                    }
                });
            }
        } else {
            alert("Không sản phẩm nào còn dư số lượng để tạo suborder");
        }
    });
}
function BtnSubOrderEnableRule() {
    var statecode = Xrm.Page.getAttribute("statecode").getValue();
    if (statecode == 1 || statecode == 2) {
        return true;
    } else {
        return false;
    }
}
function load_sale_tax_group() {
    var customer = getValue("customerid");
    if (customer != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="account">',
           ' <attribute name="name" />',
           ' <attribute name="bsd_saletaxgroup" />',
           ' <filter type="and">',
            '  <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
           ' </filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && rs[0].getValue("bsd_saletaxgroup") != null) {
                console.log(rs[0]);
                setValue("bsd_saletaxgroup", [{
                    id: rs[0].getValue("bsd_saletaxgroup"),
                    name: rs[0].attributes.bsd_saletaxgroup.name,
                    entityType: rs[0].attributes.bsd_saletaxgroup.logicalName
                }]);
            }
        });
    } else {
        setNull("bsd_saletaxgroup");
    }
}
// End Trung
//huy: add function 11h50 27/2/2017
function load_porter() {
    var effective_to = getValue("effectiveto");
    var effective_from = getValue("effectivefrom");
    getControl("bsd_priceofporter").removePreSearch(presearch_priceofporter);
    if (effective_from != null && effective_to != null) {
        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_porter">',
                   '     <attribute name="bsd_porterid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_effectiveto" />',
                   '     <attribute name="bsd_effectivefrom" />',
                   '     <attribute name="bsd_unit" />',
                   '     <attribute name="bsd_pricee" />',
                   '     <order attribute="bsd_effectiveto" descending="true" />',
                   '     <order attribute="createdon" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");

        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_porterid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                           "<row name='result'  " + "id='bsd_porterid'>  " +
                                           "<cell name='bsd_name'   " + "width='200' />  " +
                                           "</row>" +
                                        "</grid>";
        getControl("bsd_priceofporter").addCustomView(getDefaultView("bsd_priceofporter"), "bsd_porter", "bsd_porter", xml, layoutXml, true);
        var priceofporter = getValue("bsd_priceofporter");
        if (priceofporter != null) {
            var xml2 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_porter">',
                   '     <attribute name="bsd_porterid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_effectiveto" />',
                   '     <attribute name="bsd_effectivefrom" />',
                   '     <attribute name="bsd_unit" />',
                   '     <attribute name="bsd_pricee" />',
                   '     <order attribute="bsd_effectiveto" descending="true" />',
                   '     <order attribute="createdon" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="bsd_porterid" operator="eq" uitype="bsd_porter" value="' + priceofporter[0].id + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join('');
            CrmFetchKit.Fetch(xml2, false).then(function (rs) {
                if (rs.length > 0) {

                } else {

                    setNull(["bsd_priceofporter", "bsd_pricepotter"]);
                }
            });
        }
    } else {
        clear_priceofporter();
        setNull(["bsd_priceofporter", "bsd_pricepotter"]);
    }
}
function clear_priceofporter() {
    getControl("bsd_priceofporter").addPreSearch(presearch_priceofporter);
}
function presearch_priceofporter() {
    getControl("bsd_priceofporter").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//End

//huy -8h20h00  14/3/2017
function load_unitdefault(reset) {

    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="uom">',
        '<attribute name="name" />',
        '<filter type="and">',
          '<condition attribute="uomscheduleid" operator="eq" uitype="uomschedule" value="{A8C3859A-0095-E611-80CC-000C294C7A2D}" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' object='1' jump='uomid' select='1' icon='0' preview='0'>  " +
                     "<row name='result'  " + "id='uomid'><cell name='name'   " + "width='200' /> </row></grid>";
    getControl("bsd_unitdefault").addCustomView(getDefaultView("bsd_unitdefault"), "uom", "uom", xml, layoutXml, true);

    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs[0].getValue("bsd_unitdefault") != null) {
                setValue("bsd_unitdefault", [{
                    id: rs[0].attributes.bsd_unitdefault.guid,
                    name: rs[0].attributes.bsd_unitdefault.name,
                    entityType: rs[0].attributes.bsd_unitdefault.logicalName
                }]);
            }
        });

    }
}

//huy
function load_currencydefault(reset) {
    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs[0].getValue("bsd_currencydefault") != null) {
                setValue("bsd_currencydefault", [{
                    id: rs[0].attributes.bsd_currencydefault.guid,
                    name: rs[0].attributes.bsd_currencydefault.name,
                    entityType: rs[0].attributes.bsd_currencydefault.logicalName
                }]);
            }
        });

    }
}
//Author:Mr.Đăng
//Description:set Date
function Load_FromDate_Auto() {
    var date = new Date();
    setValue("bsd_date", date);
}
//Author:Mr.Đăng
//Description: Autoload
function AutoLoad() {
    setdisable_deliveryattheport();
}
//Author:Mr.Đăng
//Description: Set Disable when Quote type
function setdisable_deliveryattheport() {
    var quotationtype = getValue("bsd_quotationtype");
    if (quotationtype == 861450001) // Nc ngoài
    {
        setDisabled("bsd_deliveryattheport", false);
    }
    else {
        setDisabled("bsd_deliveryattheport", true);
        setValue("bsd_deliveryattheport", 0);
        set_DisableDeliveryPort();
    }
}
//Author:Mr.Đăng
//Description: Disable when Delivery At The Port is No
function set_DisableDeliveryPort() {
    var deliveryport = getValue("bsd_deliveryattheport");
    if (deliveryport == "0") {
        setVisible(["bsd_port", "bsd_addressport"], false);
        set_value(["bsd_port", "bsd_addressport"], null);
        clear_LoadAddressPort();
    }
    else if (deliveryport == "1") {
        setVisible(["bsd_port", "bsd_addressport"], true);
        ChooseAccountPort();
    }
}
//Author:Mr.Đăng
//Description: Choose Account in Port
function ChooseAccountPort() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="accountid" />',
                    '<order attribute="name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="861450004" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");

    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
							   "<row name='result'  " + "id='accountid'>  " +
							   "<cell name='name'   " + "width='200' />  " +
							   "<cell name='createdon'    " + "width='100' />  " +
							   "</row>   " +
							"</grid>";
    getControl("bsd_port").addCustomView(getControl("bsd_port").getDefaultView(), "account", "Port", fetchxml, layoutXml, true);
    LoadAddressPort(false);
}
//Author:Mr.Đăng
//Description: Load Address Port
function LoadAddressPort(result) {
    if (result != false) setNull("bsd_addressport");
    var accountport = getValue("bsd_port");
    if (accountport != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + accountport[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            console.log(rs);
            if (rs.length > 0 && result != false) {
                var first = rs[0];
                if (getValue("bsd_addressport") == null) {
                    setValue("bsd_addressport", [{
                        id: first.Id,
                        name: first.attributes.bsd_name.value,
                        entityType: first.logicalName
                    }]
                   );
                }
            }

        }, function (er) { });
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_addressid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>";
        getControl("bsd_addressport").removePreSearch(presearch_LoadAddressPort);
        getControl("bsd_addressport").addCustomView(getControl("bsd_addressport").getDefaultView(), "bsd_address", "Address", fetchxml, layoutXml, true);
    }
    else if (result != false) {
        clear_LoadAddressPort();
    }
}
//End

/*
   Mr: Diệm
   Note: Load Price group theo fromdate.
*/
function LK_PotentialCustomer_Change(reset) {
    var customer = getValue("customerid");
    var fullday = getFullDay("effectivefrom");
    var currency = getValue("transactioncurrencyid");
    if (reset != false) setNull("pricelevelid");
    var first;
    if (customer != null && currency != null && fullday != null) {
        CrmFetchKit.Fetch(['<fetch version="1.0" output-format="xml-platform" count="1"  mapping="logical" distinct="true">',
                           '    <entity name="pricelevel">',
                           '        <all-attributes />',
                           '        <order attribute="createdon" descending="false" />',
                           '    <filter type="and">',
                           '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                           '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                           '        <condition attribute="bsd_account" operator="eq" uitype="account" value="' + customer[0].id + '"/>',
                           '        <condition attribute="statecode" operator="eq" value="0" />',
                           '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                           '    </filter>',
                           '    </entity>',
                           '</fetch>'].join(''), false).then(function (rsaccount) {
                               if (rsaccount.length > 0) /*Nếu dữ liệu Account*/ {
                                   first = rsaccount[0];
                                   if (getValue("pricelevelid") == null) {
                                       setValue("pricelevelid",
                                            [{
                                                id: first.Id,
                                                name: first.attributes.name.value,
                                                entityType: first.logicalName
                                            }]);
                                   }
                               }
                               else/*Price group*/ {
                                   CrmFetchKit.Fetch(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                                '   <entity name="pricelevel">',
                                '        <all-attributes />',
                                '     <order attribute="createdon" descending="false" />',
                                '     <filter type="and">',
                                '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                                '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                                '        <condition attribute="statecode" operator="eq" value="0" />',
                                '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                                '     </filter>',
                                '     <link-entity name="bsd_pricegroups" from="bsd_pricegroupsid" to="bsd_pricegroup" alias="ae">',
                                '      <link-entity name="account" from="bsd_pricegroup" to="bsd_pricegroupsid" alias="af">',
                                '        <filter type="and">',
                                '          <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                                '        </filter>',
                                '     </link-entity>',
                                '     </link-entity>',
                                '    </entity>',
                                '</fetch>'].join(''), false).then(function (rspricegroup) {
                                    if (rspricegroup.length > 0) {
                                        first = rspricegroup[0];
                                        if (getValue("pricelevelid") == null) {
                                            setValue("pricelevelid",
                                                 [{
                                                     id: first.Id,
                                                     name: first.attributes.name.value,
                                                     entityType: first.logicalName
                                                 }]);
                                        }
                                    }
                                    else /*All*/ {
                                        fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                                  '    <entity name="pricelevel">',
                                                  '        <all-attributes />',
                                                  '      <order attribute="createdon" descending="false" />',
                                                  '      <filter type="and">',
                                                  '        <condition attribute="statecode" operator="eq" value="0" />',
                                                  '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                                                  '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                                                  '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                                                  '        <condition attribute="bsd_pricelisttype" operator="eq" value="100000000" />',
                                                  '     </filter>',
                                                  '   </entity>',
                                                  '</fetch>'].join('');
                                        CrmFetchKit.Fetch(fetchxml, false).then(function (rsAll) {
                                            if (rsAll.length > 0) {
                                                first = rsAll[0];
                                                if (getValue("pricelevelid") == null) {
                                                    setValue("pricelevelid",
                                                         [{
                                                             id: first.Id,
                                                             name: first.attributes.name.value,
                                                             entityType: first.logicalName
                                                         }]);
                                                }
                                            } else {
                                                m_alert("Account không có price list hoặc price list đã hết hạn");
                                            }

                                        }, function (er) { });
                                    }
                                }, function (er) { });
                               }
                           }, function (er) { });
    }
    else if (reset != false) {
        clear_priceleve();
    }
    Load_exchangerate(currency);
}
/*Diêm: Load exchangerate tu` currency*/
function Load_exchangerate(currency) {
    if (currency != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="transactioncurrency">',
                        '    <attribute name="transactioncurrencyid" />',
                        '    <attribute name="currencyname" />',
                        '    <attribute name="isocurrencycode" />',
                        '    <attribute name="currencysymbol" />',
                        '    <attribute name="exchangerate" />',
                        '    <attribute name="currencyprecision" />',
                        '    <order attribute="currencyname" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            if (rs.length > 0) {
                if (getValue("exchangerate") == null) {
                    setValue("exchangerate", rs[0].attributes.exchangerate.value);
                }
            }
        }, function (er) { });
    }
    else {
        setNull(["exchangerate"])
    }
}
/*
    Mr: Diệm
    Note: Set Function null and clear.
*/
function clear_priceleve() {
    getControl("pricelevelid").addPreSearch(presearch_priceleve);
}
function presearch_priceleve() {
    getControl("pricelevelid").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//End Diệm



//Author:Mr.Phong
function setaccounttypecustomer() {
    var xml = [];
    var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    var entityName = "account";
    var viewDisplayName = "Account View";
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='account'>");
    xml.push("<attribute name='accountid' />");
    xml.push("<attribute name='name'/>");
    xml.push("<order attribute='name' descending='false' />");
    xml.push("<filter type='and' >");
    xml.push("<condition attribute='bsd_accounttype' operator='in'>");
    xml.push("<value>");
    xml.push("861450005");
    xml.push("</value>");
    xml.push("<value>");
    xml.push("861450000");
    xml.push("</value>");
    xml.push("</condition>");
    xml.push("<condition attribute='statecode' operator='eq' value='0'/>");
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='accountid'>  " +
                      "<cell name='name'    " + "width='200' />  " +
                      "</row>   " +
                   "</grid>   ";
    Xrm.Page.getControl("customerid").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    Xrm.Page.getControl("customerid").addPreSearch(addFilter);
    Xrm.Page.getControl("bsd_partnerscompany").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    Xrm.Page.getControl("bsd_invoicenameaccount").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
}
function addFilter() {
    var customerAccountFilter = "<filter type='and'><condition attribute='contactid' operator='null' /></filter>";
    Xrm.Page.getControl("customerid").addCustomFilter(customerAccountFilter, "contact");
}
//Description:change status when stage final onload
function changestatuswhenstagefinalonload() {
    debugger;
    stagename = Xrm.Page.data.process.getActiveStage().getId();
    var formState = Xrm.Page.ui.getFormType();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    for (var i = 0; i < rnames.length; i++) {
        //stage la:finnish
        if (stagename == "b3a9e571-3b15-6b60-b992-d1e5393d2a0b")
        {
            Xrm.Page.getAttribute("statuscode").setValue(100000002);
            set_disable(["statuscode"], 0);
        }
        //nhan vien va stage la:nhân viên lập
        if (rnames[i] == "Nhân Viên" && stagename == "9fa553c0-0fe2-4c79-82e4-09a262171027")
        {
            Xrm.Page.getAttribute("statuscode").setValue(100000000);
            set_disable(["statuscode"], 1);
        }
        //truong phong va stage la:nhan vien lap
        if (rnames[i] == "Trưởng Phòng" && stagename == "9fa553c0-0fe2-4c79-82e4-09a262171027") {
            Xrm.Page.getAttribute("statuscode").setValue(100000001);
            set_disable(["statuscode"], 1);
        }
        //nhan vien va stage la:truong phong duyet
        if (rnames[i] == "Nhân Viên" && stagename == "7d1e41dd-8e6c-459c-3019-5ae332a80466")
        {
            Xrm.Page.getAttribute("statuscode").setValue(100000005);
            set_disable(["statuscode"], 1);
        }
        //truong phong va stage la:truong phong duyet
        if (rnames[i] == "Trưởng Phòng" && stagename == "7d1e41dd-8e6c-459c-3019-5ae332a80466")
        {
            Xrm.Page.getAttribute("statuscode").setValue(100000001);
            set_disable(["statuscode"], 1);
        }
        //truong phong va stage la:cap tham quyen duyet
        if (rnames[i] == "Trưởng Phòng" && stagename == "d1040793-ce33-4c5c-c144-24ecd6480504") {
            Xrm.Page.getAttribute("statuscode").setValue(100000006);
            set_disable(["statuscode"], 1);
        }
        //cap tham quyen va stage la:cap tham quyen duyet
        if (rnames[i] == "Cấp Thẩm Quyền" && stagename == "d1040793-ce33-4c5c-c144-24ecd6480504") {
            Xrm.Page.getAttribute("statuscode").setValue(100000002);
            set_disable(["statuscode"], 1);
        }
    }
    //["376bea81-de94-e611-80cc-000c294c7a2d", "b28e95c7-7fd4-e611-93f1-000c29d47eab", "e9b4552d-8ed4-e611-93f1-000c29d47eab", "5ca8e749-8ed4-e611-93f1-000c29d47eab"]
    //["System Administrator", "Nhân Viên", "Trưởng Phòng", "Cấp Thẩm Quyền"]
    //["Nhân Viên", "Trưởng Phòng", "System Administrator", "Cấp Thẩm Quyền"]//ttc
    //["4013c487-6af1-42c1-a31a-0199b044bdf4", "4560e2f3-6ea4-4c72-9d3d-73a229bde188", "ec59dfcc-d2f2-4009-9380-9f876f26ab44", "26d2ec02-88ac-4079-93a0-bbd4becadad7"]//ttc
    if (formState == 1) {
        for (var i = 0; i < rnames.length; i++) {
            if (true) {
                //truong phong
                if (rnames[i] == "4560e2f3-6ea4-4c72-9d3d-73a229bde188") {
                    Xrm.Page.getAttribute("statuscode").setValue(100000001);
                    set_disable(["statuscode"], 1);

                }
            }
        }
    }
}
//Description:change status when stage final onchange
function changestatuswhenstagefinalonchange() {
    var statuscode = Xrm.Page.getAttribute("statuscode").getValue();
    stagename = Xrm.Page.data.process.getActiveStage().getName();
    if (stagename == "Finnish") {
        set_disable(["statuscode"], 0);
        if (statuscode == 1 && stagename == "Finnish") {
            var message = "Pls choose khách hàng duyệt";
            var type = "INFO"; //INFO, WARNING, ERROR
            var id = "Info1"; //Notification Id
            var time = 3000; //Display time in milliseconds
            //Display the notification
            Xrm.Page.ui.setFormNotification(message, type, id);

            //Wait the designated time and then remove
            setTimeout(
                function () {
                    Xrm.Page.ui.clearFormNotification(id);
                },
                time
            );
            Xrm.Page.getAttribute("statuscode").setValue(100000002);
        }
        else if (statuscode == 100000001 && stagename == "Finnish") {
            var message = "Pls choose khách hàng duyệt";
            var type = "INFO"; //INFO, WARNING, ERROR
            var id = "Info1"; //Notification Id
            var time = 3000; //Display time in milliseconds
            //Display the notification
            Xrm.Page.ui.setFormNotification(message, type, id);

            //Wait the designated time and then remove
            setTimeout(
                function () {
                    Xrm.Page.ui.clearFormNotification(id);
                },
                time
            );
            Xrm.Page.getAttribute("statuscode").setValue(100000002);
        }
        else if (statuscode == 100000000 && stagename == "Finnish") {
            var message = "Pls choose khách hàng duyệt";
            var type = "INFO"; //INFO, WARNING, ERROR
            var id = "Info1"; //Notification Id
            var time = 3000; //Display time in milliseconds
            //Display the notification
            Xrm.Page.ui.setFormNotification(message, type, id);

            //Wait the designated time and then remove
            setTimeout(
                function () {
                    Xrm.Page.ui.clearFormNotification(id);
                },
                time
            );
            Xrm.Page.getAttribute("statuscode").setValue(100000002);
        }
        else if (statuscode == 100000003 && stagename == "Finnish") {
            Xrm.Page.getAttribute("bsd_statusreasonname").setValue("100000003");
        }
    }
    else {
        set_disable(["statuscode"], 1);
    }
}
//Description:an hien fied Ship To Freight Terms Onload
function showhide_fiedshiptofreightterms() {
    var quotationtype = Xrm.Page.getAttribute("bsd_quotationtype").getValue();
    if (quotationtype == 861450001)//neu chon bao gia xuat khau
    {
        Xrm.Page.ui.controls.get("shipto_freighttermscode").setVisible(true);
    }
    else {
        Xrm.Page.ui.controls.get("shipto_freighttermscode").setVisible(false);
    }
}
//Description:an hien fied Ship To Freight Terms onchange
function showhide_fiedshiptofreighttermsonchange() {
    var quotationtype = Xrm.Page.getAttribute("bsd_quotationtype").getValue();
    if (quotationtype == 861450001)//neu chon bao gia xuat khau
    {
        Xrm.Page.ui.controls.get("shipto_freighttermscode").setVisible(true);
    }
    else {
        Xrm.Page.ui.controls.get("shipto_freighttermscode").setVisible(false);
    }
}
//Description:Filter_subgrid
function filter_gridQuotebyID() {
    debugger;
    //RelatedCars : is name of subgrid given on Form.
    //var objSubGrid = document.getElementById("subgridQuote");
    //var objSubGrid = Xrm.Page.getControl("subgridQuote").getGrid();
    var objSubGrid = window.parent.document.getElementById("subgridQuote");
    //CRM loads subgrid after form is loaded.. so when we are adding script on form load.. need to wait unitil subgrid is loaded. Thats why adding delay..
    if (objSubGrid == null) {
        setTimeout(filter_gridQuotebyID, 2000);
        return;
    } else {
        //when subgrid is loaded, get category value
        var name = Xrm.Page.getAttribute("quotenumber").getValue();
        if (name != null) {
            //Create FetchXML for sub grid to filter records based on category
            var FetchXml = "<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0'>" +
                              "<entity name='quote'>" +
                                    "<attribute name='name' />" +
                                    "<attribute name='customerid' />" +
                                    "<attribute name='statecode' />" +
                                    "<attribute name='totalamount' />" +
                                    "<attribute name='quoteid' />" +
                                    "<attribute name='createdon' />" +
                                    "<order attribute='name' descending='false' />" +
                                    "<filter type='and'>" +
                                        "<condition attribute='quotenumber' operator='eq' value='" + name + "' />" +
                                    "</filter>" +
                                "</entity>" +
                              "</fetch>";
            //apply layout and filtered fetchXML
            if (objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", FetchXml);
                objSubGrid.control.Refresh();
                //document.getElementById("subgridQuote").control.SetParameter("fetchXml", FetchXml)
                ////objSubGrid.control.SetParameter("fetchXml", FetchXml);
                ////Refresh grid to show filtered records only. 
                //document.getElementById("subgridQuote").control.Refresh();
            } else {
                setTimeout(filter_gridQuotebyID, 500);
            }
        }
    }
}
//Description:lay ngay hien tai gan vao field effective from
function set_currentdate() {
    if (Xrm.Page.ui.getFormType() == 1) {
        var currentDateTime = new Date();
        var d = new Date();
        var currentDateTime = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
        Xrm.Page.getAttribute("effectivefrom").setValue(currentDateTime);
        Xrm.Page.getAttribute("bsd_date").setValue(currentDateTime);
    }
}
//Description:get customer information
function get_customerinformation() {
    debugger;
    Xrm.Page.getAttribute("bsd_paymentterm").setSubmitMode("always");
    Xrm.Page.getAttribute("bsd_paymentmethod").setSubmitMode("always");
    var fieldshiptoaddressfake = Xrm.Page.getAttribute("new_shiptoaddress");
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    var fieldaddress = Xrm.Page.getAttribute("bsd_address");
    var fieldpricelist = Xrm.Page.getAttribute("pricelevelid");
    var fieldpaymentterm = Xrm.Page.getAttribute("bsd_paymentterm");
    var fieldpaymentmethod = Xrm.Page.getAttribute("bsd_paymentmethod");
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount");
    var addressinvoicenameaccount = Xrm.Page.getAttribute("bsd_addressinvoiceaccount");
    var partnerscompany = Xrm.Page.getAttribute("bsd_partnerscompany");
    var truongphong = Xrm.Page.getAttribute("bsd_tennguoiduyettruongphong");
    var currency = Xrm.Page.getAttribute("transactioncurrencyid").getValue();
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var xmlaccount = [];
    var xmlcustomeraddresskey = [];
    var xmlcustomeraddress = [];
    var xmlaccount = [];
    var xmlcustomeraddresskey = [];
    var xmlcustomeraddress = [];
    var xmlpricelistaccount = [];
    var xmlaccountpricegroup = [];
    var xmlpricegroup = [];
    var xmlinvoiceaddressinvoiceaccount = [];
    var xmlcustomeraddressinvoiceaddress = [];
    var xmlshiptoaddress = [];
    var xmlcustomeraddressshiptoaddress = [];
    var effectivefrom = Xrm.Page.getAttribute("effectivefrom").getValue();
    var numberprocess = Xrm.Page.getAttribute("bsd_numberprocess").getValue();
    var user = Xrm.Page.getAttribute("bsd_user");
    stagename = Xrm.Page.data.process.getActiveStage().getId();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    if (effectivefrom != null) {
        var year = effectivefrom.getFullYear() + "";
        if (effectivefrom.getMonth() + 1 <= 9) {
            var month = "0" + (effectivefrom.getMonth() + 1) + "";
        }
        else {
            var month = (effectivefrom.getMonth() + 1) + "";
        }
        var day = effectivefrom.getDate() + "";
        var dateFormat = year + "-" + month + "-" + day;

        var yearpricelist = effectivefrom.getFullYear();
        var monthpricelist = (effectivefrom.getMonth() + 1);
        var daypricelist = effectivefrom.getDate();
    }
    for (var i = 0; i < rnames.length; i++) {
        if (true) {
            if (rnames[i] == "Trưởng Phòng" && stagename == "9fa553c0-0fe2-4c79-82e4-09a262171027" && numberprocess == false && user.getValue() == null) {
                Xrm.Page.getAttribute("bsd_numberprocess").setValue(true);
                truongphong.setValue([{
                    name: Xrm.Page.context.getUserName(),
                    id: Xrm.Page.context.getUserId(),
                    type: "8"
                }]);
            }
        }
    }

    if (customer != null) {
        setDisabled(["bsd_address", "bsd_addressinvoiceaccount", "bsd_shiptoaddress", "bsd_contact"], false);
        //Account Information
        set_value(["bsd_shiptoaddress", "bsd_partnerscompany"], null);
        fetch(xmlaccount, "account", ["name", "primarycontactid", "telephone1", "accountid"
                                , "bsd_taxregistration", "fax", "accountnumber", "emailaddress1"
                                , "bsd_paymentterm", "bsd_paymentmethod", "transactioncurrencyid"], ["createdon"], false, null,
                                ["accountid"], ["eq"], [0, customer[0].id, 1]);
        CrmFetchKit.Fetch(xmlaccount.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                invoicenameaccount.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.name.value,
                    entityType: rs[0].logicalName
                }]);
                partnerscompany.setValue([{
                    id: customer[0].id,
                    name: customer[0].name,
                    entityType: 'account'
                }]);
                if (rs[0].attributes.accountnumber != null) {
                    Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(rs[0].attributes.accountnumber.value);
                }
                else if (rs[0].attributes.accountnumber == null) {
                    Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(null);
                }
                if (rs[0].attributes.telephone1 != null) {
                    Xrm.Page.getAttribute("bsd_telephone").setValue(rs[0].attributes.telephone1.value);
                }
                else if (rs[0].attributes.telephone1 == null) {
                    Xrm.Page.getAttribute("bsd_telephone").setValue(null);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(rs[0].attributes.bsd_taxregistration.value);
                }
                else if (rs[0].attributes.bsd_taxregistration == null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                }
                if (rs[0].attributes.fax != null) {
                    Xrm.Page.getAttribute("bsd_fax").setValue(rs[0].attributes.fax.value);
                }
                else if (rs[0].attributes.fax == null) {
                    Xrm.Page.getAttribute("bsd_fax").setValue(null);
                }
                if (rs[0].attributes.accountnumber != null) {
                    var no = rs[0].attributes.accountnumber.value;
                    Xrm.Page.getAttribute("bsd_customercode").setValue(no);
                }
                else if (rs[0].attributes.accountnumber == null) {
                    Xrm.Page.getAttribute("bsd_customercode").setValue(null);
                }
                if (rs[0].attributes.emailaddress1 != null) {
                    Xrm.Page.getAttribute("bsd_mail").setValue(rs[0].attributes.emailaddress1.value);
                }
                else if (rs[0].attributes.emailaddress1 == null) {
                    Xrm.Page.getAttribute("bsd_mail").setValue(null);
                }
                if (rs[0].attributes.bsd_paymentterm != null) {
                    fieldpaymentterm.setValue([{
                        id: rs[0].attributes.bsd_paymentterm.guid,
                        name: rs[0].attributes.bsd_paymentterm.name,
                        entityType: 'bsd_paymentterm'
                    }]);
                }
                else if (rs[0].attributes.bsd_paymentterm == null) {
                    fieldpaymentterm.setValue(null);
                }
                if (rs[0].attributes.bsd_paymentmethod != null) {
                    fieldpaymentmethod.setValue([{
                        id: rs[0].attributes.bsd_paymentmethod.guid,
                        name: rs[0].attributes.bsd_paymentmethod.name,
                        entityType: 'bsd_methodofpayment'
                    }]);
                }
                else if (rs[0].attributes.bsd_paymentmethod == null) {
                    fieldpaymentmethod.setValue(null);
                }
            }
        },
        function (er) {
            console.log(er.message)
        });
        //Address Account Information 
        var viewIdcustomeraddresskey = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNamecustomeraddresskey = "bsd_address";
        var viewDisplayNamecustomeraddresskey = "Customer Address have key";
        fetch(xmlcustomeraddresskey, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
        ["bsd_key", "statecode", "bsd_account"], ["eq", "eq", "eq"], [0, "1", 1, "0", 2, customer[0].id]);
        var layoutXmlcustomeraddresskey = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlcustomeraddresskey.join(""), true).then(function (rs) {
            //neu customer co address key la yes
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    fieldaddress.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
                Xrm.Page.getControl("bsd_address").addCustomView(viewIdcustomeraddresskey, entityNamecustomeraddresskey, viewDisplayNamecustomeraddresskey, xmlcustomeraddresskey.join(""), layoutXmlcustomeraddresskey, true);
            }
                //neu customer ko co address key la yes thi se load dia chi cua customer da co
            else if (rs.length == 0) {
                var viewIdcustomeraddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNamecustomeraddress = "bsd_address";
                var viewDisplayNamecustomeraddress = "Customer Address";
                fetch(xmlcustomeraddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXmlcustomeraddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_account'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                CrmFetchKit.Fetch(xmlcustomeraddress.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        if (rs[0].attributes.bsd_name != null) {
                            fieldaddress.setValue([{
                                id: rs[0].Id,
                                name: rs[0].attributes.bsd_name.value,
                                entityType: rs[0].logicalName
                            }]);
                            Xrm.Page.getControl("bsd_address").addCustomView(viewIdcustomeraddress, entityNamecustomeraddress, viewDisplayNamecustomeraddress, xmlcustomeraddress.join(""), layoutXmlcustomeraddress, true);
                        }
                    }
                    if (rs.length == 0) {
                        fieldaddress.setValue(null);
                        Xrm.Page.getControl("bsd_address").addCustomView(viewIdcustomeraddress, entityNamecustomeraddress, viewDisplayNamecustomeraddress, xmlcustomeraddress.join(""), layoutXmlcustomeraddress, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }
        },
         function (er) {
             console.log(er.message)
         });
        //Ship To Address
        var viewIdshiptoaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameshiptoaddress = "bsd_address";
        var viewDisplayNameshiptoaddress = "Ship to address";
        fetch(xmlshiptoaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
        ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customer[0].id]);
        var layoutXmlshiptoaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_account'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlshiptoaddress.join(""), true).then(function (rs) {
            //neu co dia chi la delivery
            if (rs.length > 0) {
                fieldshiptoaddress.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.bsd_name.value,
                    entityType: rs[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdshiptoaddress, entityNameshiptoaddress, viewDisplayNameshiptoaddress, xmlshiptoaddress.join(""), layoutXmlshiptoaddress, true);
            }
            //neu ko co dia chi delivery thi se load tat ca dia chi ma account do co
            if (rs.length == 0) {
                var viewIdcustomeraddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNamecustomeraddress = "bsd_address";
                var viewDisplayNamecustomeraddress = "Customer Address";
                fetch(xmlcustomeraddressshiptoaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXmlcustomeraddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_account'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdcustomeraddress, entityNamecustomeraddress, viewDisplayNamecustomeraddress, xmlcustomeraddressshiptoaddress.join(""), layoutXmlcustomeraddress, true);
            }

        }, function (er) {
            console.log(er.message)
        });
        //Invoice Address
        var viewIdinvoiceaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameinvoiceaddress = "bsd_address";
        var viewDisplayNameinvoiceaddress = "Invoice address invoice account";
        fetch(xmlinvoiceaddressinvoiceaccount, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
           ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Invoice" + "%", 1, "0", 2, customer[0].id]);
        var layoutXmlinvoiceaccount = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_account'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlinvoiceaddressinvoiceaccount.join(""), true).then(function (rs) {
            //neu account co dia chi invoice
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    addressinvoicenameaccount.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdinvoiceaddress, entityNameinvoiceaddress, viewDisplayNameinvoiceaddress, xmlinvoiceaddressinvoiceaccount.join(""), layoutXmlinvoiceaccount, true);
            }
            //neu account ko co dia chi invoice thi se load tat ca dia chi ma account do co
            if (rs.length == 0) {
                var viewIdaddresscustomer = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameaddresscustomer = "bsd_address";
                var viewDisplayNameaddresscustomer = "Address Customer";
                fetch(xmlcustomeraddressinvoiceaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                   ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXmladdresscustomer = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_account'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdaddresscustomer, entityNameaddresscustomer, viewDisplayNameaddresscustomer, xmlcustomeraddressinvoiceaddress.join(""), layoutXmladdresscustomer, true);
            }
        },
        function (er) {
            console.log(er.message)
        });
    }
    else {
        set_value(["pricelevelid", "bsd_telephone", "bsd_taxregistration", "bsd_fax", "bsd_customercode", "bsd_mail"
                    , "bsd_paymentterm", "bsd_paymentmethod", "bsd_contact", "bsd_invoicenameaccount", "bsd_invoiceaccount", "bsd_addressinvoiceaccount"
                    , "bsd_partnerscompany", "pricelevelid"], null);
        set_disable(["pricelevelid"], 1)
        setDisabled(["bsd_address", "bsd_addressinvoiceaccount", "bsd_shiptoaddress", "bsd_contact"], true);
        fieldaddress.setValue(null);
        fieldshiptoaddress.setValue(null);
    }
}
//Description:load contact theo account
function get_contact() {
    debugger;
    var contactdefault = Xrm.Page.getAttribute("bsd_contact");
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    Xrm.Page.getControl("bsd_contact").removePreSearch(presearch_Contact);
    var xmlprimarycontact = [];
    var xmlcontactaccount = [];
    if (customer != null) {
        xmlprimarycontact.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xmlprimarycontact.push("<entity name='account'>");
        xmlprimarycontact.push("<attribute name='name' />");
        xmlprimarycontact.push("<attribute name='primarycontactid'/>");
        xmlprimarycontact.push("<attribute name='telephone1'/>");
        xmlprimarycontact.push("<attribute name='accountid'/>");
        xmlprimarycontact.push("<order attribute='name' descending='false'/> ");
        xmlprimarycontact.push("<link-entity name='contact' from='parentcustomerid' to='accountid' alias='ab'>");
        xmlprimarycontact.push("<filter type='and'>");
        xmlprimarycontact.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "'/>");
        xmlprimarycontact.push("</filter>");
        xmlprimarycontact.push("</link-entity>");
        xmlprimarycontact.push("</entity>");
        xmlprimarycontact.push("</fetch>");
        CrmFetchKit.Fetch(xmlprimarycontact.join(""), true).then(function (rs) {
            //neu account co primary contact
            if (rs.length > 0 && rs[0].attributes.primarycontactid != null) {
                contactdefault.setValue([{
                    id: rs[0].attributes.primarycontactid.guid,
                    name: rs[0].attributes.primarycontactid.name,
                    entityType: 'contact'
                }]);
                var viewIdcontact = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityNamecontact = "contact";
                var viewDisplayNamecontact = "Contact Account";
                fetch(xmlcontactaccount, "contact", ["fullname", "telephone1", "contactid", "accountid"], ["fullname"], true, null,
                               ["parentcustomerid"], ["eq"], [0, customer[0].id, 1]);
                var layoutXmlcontact = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_contact").addCustomView(viewIdcontact, entityNamecontact, viewDisplayNamecontact, xmlcontactaccount.join(""), layoutXmlcontact, true);
            }
                //neu ko co primary contact thi se lay contat dau tien ma account do co
            else {
                var entityNamecontact = "contact";
                var viewDisplayNamecontact = "Contact Account";
                fetch(xmlcontactaccount, "contact", ["fullname", "telephone1", "contactid", "accountid"], ["fullname"], true, null,
                               ["parentcustomerid"], ["eq"], [0, customer[0].id, 1]);
                var layoutXmlcontact = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_contact").addCustomView(viewIdcontact, entityNamecontact, viewDisplayNamecontact, xmlcontactaccount.join(""), layoutXmlcontact, true);
                CrmFetchKit.Fetch(xmlcontactaccount.join(""), true).then(function (rs) {
                    if (rs.length > 0 && rs[0].attributes.fullname != null) {
                        contactdefault.setValue([{
                            id: rs[0].Id,
                            name: rs[0].attributes.fullname.value,
                            entityType: rs[0].logicalName
                        }]);
                    }
                    else {
                        clear_Contact();
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }

        }, function (er) {
            console.log(er.message)
        });
    }
}
//Description:gan gia tri cho filed invoice address
function set_invoiceaddress() {
    debugger;
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    var addressinvoicenameaccount = Xrm.Page.getAttribute("bsd_addressinvoiceaccount");
    Xrm.Page.getControl("bsd_addressinvoiceaccount").removePreSearch(presearch_invoiceaddress);
    var xmlinvoiceaddress = [];
    var xmlinvoiceaccountaddress = [];
    var xmlinvoiceaccountinformation = [];
    if (invoicenameaccount != null) {
        var viewIdinvoiceaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameinvoiceaddress = "bsd_address";
        var viewDisplayNameinvoiceaddress = "test";
        fetch(xmlinvoiceaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Invoice" + "%", 1, "0", 2, invoicenameaccount[0].id]);
        var layoutXmlinvoiceaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlinvoiceaddress.join(""), true).then(function (rs) {
            //neu invoice account co dia chi la invoice address
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    addressinvoicenameaccount.setValue([{
                        id: rs[0].Id,
                        name: rs[0].attributes.bsd_name.value,
                        entityType: rs[0].logicalName
                    }]);
                }
                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdinvoiceaddress, entityNameinvoiceaddress, viewDisplayNameinvoiceaddress, xmlinvoiceaddress.join(""), layoutXmlinvoiceaddress, true);
            }
            if (rs.length == 0) {
                var viewIdinvoiceaccountaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameinvoiceaccountaddress = "bsd_address";
                var viewDisplayNameinvoiceaccountaddress = "Invoice Account Address";
                fetch(xmlinvoiceaccountaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                       ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, invoicenameaccount[0].id]);
                var layoutXmlinvoiceaccountaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                CrmFetchKit.Fetch(xmlinvoiceaccountaddress.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        if (rs[0].attributes.bsd_name != null) {
                            if (addressinvoicenameaccount == null) {
                                addressinvoicenameaccount.setValue([{
                                    id: rs[0].Id,
                                    name: rs[0].attributes.bsd_name.value,
                                    entityType: rs[0].logicalName
                                }]);
                            }
                        }
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdinvoiceaccountaddress, entityNameinvoiceaccountaddress, viewDisplayNameinvoiceaccountaddress, xmlinvoiceaccountaddress.join(""), layoutXmlinvoiceaccountaddress, true);
                    }
                    if (rs.length == 0) {
                        addressinvoicenameaccount.setValue(null);
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdinvoiceaccountaddress, entityNameinvoiceaccountaddress, viewDisplayNameinvoiceaccountaddress, xmlinvoiceaccountaddress.join(""), layoutXmlinvoiceaccountaddress, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }

        },
        function (er) {
            console.log(er.message)
        });
        fetch(xmlinvoiceaccountinformation, "account", ["name", "primarycontactid", "telephone1", "accountid"
                              , "bsd_taxregistration", "fax", "accountnumber", "emailaddress1"
                              , "bsd_paymentterm", "bsd_paymentmethod", "transactioncurrencyid"], ["createdon"], false, null,
                              ["accountid"], ["eq"], [0, invoicenameaccount[0].id, 1]);
        CrmFetchKit.Fetch(xmlinvoiceaccountinformation.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.accountnumber != null) {
                    Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(rs[0].attributes.accountnumber.value);
                }
                else if (rs[0].attributes.accountnumber == null) {
                    Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(null);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(rs[0].attributes.bsd_taxregistration.value);
                }
                else if (rs[0].attributes.bsd_taxregistration == null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                }
            }
        },
    function (er) {
        console.log(er.message)
    });
    }
    else {
        addressinvoicenameaccount.setValue(null);
        clear_invoiceaddress();
        Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(null);
        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
    }
}
//Description:gan gia tri cho fied ship to address
function set_addressfromcustomerb() {
    var customerb = Xrm.Page.getAttribute("bsd_partnerscompany").getValue();
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    Xrm.Page.getControl("bsd_shiptoaddress").removePreSearch(presearch_LoadShiptoaddress);
    var xmlpartnerscompanyaddressdelivery = [];
    var xmlpartnerscompanyaddress = [];
    if (customerb != null) {
        var viewIdpartnerscompanyaddressdelivery = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNamepartnerscompanyaddressdelivery = "bsd_address";
        var viewDisplayNamepartnerscompanyaddressdelivery = "Partnerscompany Address Delivery";
        fetch(xmlpartnerscompanyaddressdelivery, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customerb[0].id]);
        var layoutXmlpartnerscompanyaddressdelivery = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xmlpartnerscompanyaddressdelivery.join(""), true).then(function (rs) {
            //noi partnerscompany co dia chi delivery
            if (rs.length > 0) {
                fieldshiptoaddress.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.bsd_name.value,
                    entityType: rs[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdpartnerscompanyaddressdelivery, entityNamepartnerscompanyaddressdelivery, viewDisplayNamepartnerscompanyaddressdelivery, xmlpartnerscompanyaddressdelivery.join(""), layoutXmlpartnerscompanyaddressdelivery, true);
            }
            //noi partnerscompany ko co dia chi delivery thi se load tat ca dia chi ma partnerscompany no co
            if (rs.length == 0) {
                var viewIdpartnerscompanyaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNamepartnerscompanyaddress = "bsd_address";
                var viewDisplayNamepartnerscompanyaddress = "Partnerscompany Address";
                fetch(xmlpartnerscompanyaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                       ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customerb[0].id]);
                var layoutXmlpartnerscompanyaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                CrmFetchKit.Fetch(xmlpartnerscompanyaddress.join(""), true).then(function (rs) {
                    if (rs.length > 0) {
                        Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdpartnerscompanyaddress, entityNamepartnerscompanyaddress, viewDisplayNamepartnerscompanyaddress, xmlpartnerscompanyaddress.join(""), layoutXmlpartnerscompanyaddress, true);
                    }
                    else {
                        Xrm.Page.getAttribute("bsd_shiptoaddress").setValue(null);
                        clear_LoadShiptoaddress();
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    if (customerb == null) {
        clear_LoadShiptoaddress();
        Xrm.Page.getAttribute("bsd_shiptoaddress").setValue(null);
    }
}
//Description:kiem tra ngay chon fromdate va todate
function checkdateonquote() {
    var fromday = Xrm.Page.getAttribute("bsd_fromdate").getValue();
    var today = Xrm.Page.getAttribute("bsd_todate").getValue();
    if (fromday != null) {
        Xrm.Page.getControl("bsd_fromdate").clearNotification();
        var year = fromday.getFullYear() + "";
        if (fromday.getMonth() + 1 <= 9) {
            var month = "0" + (fromday.getMonth() + 1);
        }
        else {
            var month = (fromday.getMonth() + 1);
        }
        var dayy = fromday.getDate();
    }
    if (fromday == null) {
        Xrm.Page.getAttribute("bsd_todate").setValue(null);
        Xrm.Page.getControl("bsd_fromdate").setNotification("From date is not null!!!");
    }
    if (today != null && fromday != null) {
        var yearr = today.getFullYear();
        if (fromday.getMonth() + 1 <= 9) {
            var monthh = "0" + (today.getMonth() + 1);
        }
        else {
            var monthh = (today.getMonth() + 1);
        }
        var day = today.getDate();
        if (yearr < year) {
            Xrm.Page.getControl("bsd_todate").setNotification("To date is not smaller than from date");
        }
        if (yearr == year) {
            if (monthh < month) {
                Xrm.Page.getControl("bsd_todate").setNotification("To date is not smaller than from date");
                Xrm.Page.getAttribute("bsd_todate").setValue(null);
            }
            if (monthh == month) {
                if (day => dayy) {
                    Xrm.Page.getControl("bsd_todate").clearNotification();
                }
                if (day < dayy) {
                    Xrm.Page.getControl("bsd_todate").setNotification("To date is not smaller than from date");
                    Xrm.Page.getAttribute("bsd_todate").setValue(null);

                }
            }
            if (monthh > month) {
                Xrm.Page.getControl("bsd_todate").clearNotification();
            }
        }
        if (yearr > year) {
            Xrm.Page.getControl("bsd_todate").clearNotification();
        }
    }
}
//Description:gan gia tri cho cac field address khi form la edit
function set_addressonload() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    var xmlcontactcustomer = [];
    var xmladdresscustomer = [];
    var xmlinvoiceaccountaddress = [];
    //Xrm.Page.getControl("bsd_address").removePreSearch(presearch_addresscustomer);
    Xrm.Page.getControl("bsd_contact").removePreSearch(presearch_Contact);
    Xrm.Page.getControl("bsd_addressinvoiceaccount").removePreSearch(presearch_invoiceaddress);
    if (customer != null) {
        var viewIdaddresscustomer = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameaddresscustomer = "bsd_address";
        var viewDisplayNameaddresscustomer = "Address Customer";
        fetch(xmladdresscustomer, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
        var layoutXmladdresscustomer = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='bsd_addressid'>  " +
              "<cell name='bsd_name'   " + "width='200' />  " +
              "<cell name='createdon'    " + "width='100' />  " +
                "<cell name='bsd_purpose'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
        //Xrm.Page.getControl("bsd_address").addCustomView(viewIdaddresscustomer, entityNameaddresscustomer, viewDisplayNameaddresscustomer, xmladdresscustomer.join(""), layoutXmladdresscustomer, true);
        var viewIdcontactcustomer = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
        var entityNamecontactcustomer = "contact";
        var viewDisplayNamecontactcustomer = "Contact Customer";
        fetch(xmlcontactcustomer, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], true, null,
                     ["statecode", "parentcustomerid"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
        var layoutXmlcontactcustomer = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='contactid'>  " +
                         "<cell name='fullname'   " + "width='200' />  " +
                         "<cell name='telephone1'    " + "width='100' />  " +
                         "</row>   " +
                      "</grid>   ";
        Xrm.Page.getControl("bsd_contact").addCustomView(viewIdcontactcustomer, entityNamecontactcustomer, viewDisplayNamecontactcustomer, xmlcontactcustomer.join(""), layoutXmlcontactcustomer, true);
    }
    else {
        setNull(["bsd_contact"]);
        //clear_Contact();
    }
    if (invoicenameaccount != null) {
        var viewIdinvoiceaccountaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameinvoiceaccountaddress = "bsd_address";
        var viewDisplayNameinvoiceaccountaddress = "Address Invoice Account";
        fetch(xmlinvoiceaccountaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, invoicenameaccount[0].id]);
        var layoutXmlinvoiceaccountaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='bsd_addressid'>  " +
              "<cell name='bsd_name'   " + "width='200' />  " +
              "<cell name='createdon'    " + "width='100' />  " +
                "<cell name='bsd_purpose'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewIdinvoiceaccountaddress, entityNameinvoiceaccountaddress, viewDisplayNameinvoiceaccountaddress, xmlinvoiceaccountaddress.join(""), layoutXmlinvoiceaccountaddress, true);
    }
    else {
        setNull("bsd_addressinvoiceaccount");
        //clear_invoiceaddress();
    }
}
//Description:gan gia tri cho field ship to address khi form la edit
function setshiptoaddressonload() {
    var customerb = Xrm.Page.getAttribute("bsd_partnerscompany").getValue();
    Xrm.Page.getControl("bsd_shiptoaddress").removePreSearch(presearch_LoadShiptoaddress);
    var xmlshiptoaddress = [];
    var xmlaccountaddress = [];
    if (customerb != null) {
        var viewIdshiptoaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityNameshiptoaddress = "bsd_address";
        var viewDisplayNamesiptoaddress = "Ship To Address";
        fetch(xmlshiptoaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customerb[0].id]);
        var layoutXmlshiptoaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";

        CrmFetchKit.Fetch(xmlshiptoaddress.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdshiptoaddress, entityNameshiptoaddress, viewDisplayNamesiptoaddress, xmlshiptoaddress.join(""), layoutXmlshiptoaddress, true);
            }
            if (rs.length == 0) {
                var viewIdaccountaddress = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityNameaccountaddress = "bsd_address";
                var viewDisplayNameaccountaddress = "Account Address";
                fetch(xmlaccountaddress, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customerb[0].id]);
                var layoutXmlaccountaddress = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewIdaccountaddress, entityNameaccountaddress, viewDisplayNameaccountaddress, xmlaccountaddress.join(""), layoutXmlaccountaddress, true);
            }

        }, function (er) {
            console.log(er.message)
        });
    }
    else {
        setNull("bsd_shiptoaddress");
        //clear_LoadShiptoaddress();
    }
}
function validation() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    if (customer == null) {
        Xrm.Page.getAttribute("bsd_address").setValue(null);
    }
    if (invoicenameaccount == null) {
        Xrm.Page.getAttribute("bsd_addressinvoiceaccount").setValue(null);
    }
}
function hide_button() {
    try {
        if (window.parent.document.getElementById("stageBackActionContainer") != null) {
            window.parent.document.getElementById("stageBackActionContainer").style.display = "none";
        }
    } catch (e) {

    }
    try {
        if (window.parent.document.getElementById("stageBackActionContainer") != null) {
            window.parent.document.getElementById("stageBackActionContainer").style.visibility = "hidden";
        }
    } catch (e) {

    }
    try {
        if (window.parent.document.getElementById("stageAdvanceActionContainer") != null) {
            window.parent.document.getElementById("stageAdvanceActionContainer").style.display = "none";
        }
    } catch (e) {

    }
    try {
        if (window.parent.document.getElementById("stageAdvanceActionContainer") != null) {
            window.parent.document.getElementById("stageAdvanceActionContainer").style.visibility = "hidden";
        }
    } catch (e) {

    }
    try {
        if (window.parent.document.getElementById("stageSetActiveActionContainer") != null) {
            window.parent.document.getElementById("stageSetActiveActionContainer").style.display = "none";
        }
    } catch (e) {

    }
    try {
        if (window.parent.document.getElementById("stageSetActiveActionContainer") != null) {

            window.parent.document.getElementById("stageSetActiveActionContainer").style.visibility = "hidden";
        }
    } catch (e) {

    }
}
function GetRoleName(roleIds) {
    var serverUrl = location.protocol + "//" + location.host + "/" + Xrm.Page.context.getOrgUniqueName();
    var odataSelect = serverUrl + "/XRMServices/2011/OrganizationData.svc" + "/" + "RoleSet?$select=Name";
    var cdn = "";
    if (roleIds != null && roleIds.length > 0) {
        for (var i = 0; i < roleIds.length; i++) {
            if (i == roleIds.length - 1)
                cdn += "RoleId eq guid'" + roleIds[i] + "'";
            else
                cdn += "RoleId eq guid'" + roleIds[i] + "' or ";
        }
        if (cdn.length > 0)
            cdn = "&$filter=" + cdn;
        odataSelect += cdn;
        var roleName = [];
        $.ajax(
            {
                type: "GET",
                async: false,
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                url: odataSelect,
                beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
                success: function (data, textStatus, XmlHttpRequest) {
                    if (data.d != null && data.d.results != null) {
                        var len = data.d.results.length;
                        for (var k = 0; k < len; k++)
                            roleName.push(data.d.results[k].Name);
                    }
                },
                error: function (XmlHttpRequest, textStatus, errorThrown) { alert('OData Select Failed: ' + textStatus + errorThrown + odataSelect); }
            }
        );
    }
    return roleName;
}
function switchProcess() {
    var formState = Xrm.Page.ui.getFormType();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var formState = Xrm.Page.ui.getFormType();
    for (var i = 0; i < rnames.length; i++) {
        if (rnames[i] = "Trưởng Phòng") {
            var formState = Xrm.Page.ui.getFormType();
            stageId = Xrm.Page.data.process.getActiveStage().getId();
            Xrm.Page.data.process.setActiveStage(stageId, switchProcessEnd);
        }
    }
}
function switchProcessEnd() {
    stagename = Xrm.Page.data.process.getActiveStage().getName();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var user = Xrm.Page.getAttribute("bsd_user").getValue();
    var numberprocess = Xrm.Page.getAttribute("bsd_numberprocess").getValue();
    for (var i = 0; i < rnames.length; i++) {
        if (stagename == "Nhân Viên Lập" && rnames[i] == "Trưởng Phòng" && numberprocess == true) {
            Xrm.Page.data.process.moveNext(function (e) {
                if (e == "success") {
                    Xrm.Page.ui.refreshRibbon();
                    //changestatuswhenstagefinalonload();
                }
            });
        }
    }

}
function showBusinessProcessFlowonload() {
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var formState = Xrm.Page.ui.getFormType();
    var shippingpricelist = Xrm.Page.getAttribute("bsd_shippingpricelistname").getValue();
    var numberprocess = Xrm.Page.getAttribute("bsd_numberprocess").getValue();
    var truongphong = Xrm.Page.getAttribute("bsd_tennguoiduyettruongphong");
    var user = Xrm.Page.getAttribute("bsd_user");
    for (var i = 0; i < rnames.length; i++) {
        if (rnames[i] == "Trưởng Phòng" && user.getValue() == null) // update
        {
            if (formState == 2 && numberprocess == false)
            {               
                Xrm.Page.getAttribute("bsd_numberprocess").setValue(true);
                truongphong.setValue([{
                    name: Xrm.Page.context.getUserName(),
                    id: Xrm.Page.context.getUserId(),
                    type: "8"
                }]);
                Xrm.Page.data.save();          
            }
        }
    }
}
function setdisableallfield() {
    var userid = Xrm.Page.context.getUserId();
    var owner = Xrm.Page.getAttribute("ownerid").getValue();
    var stagename = Xrm.Page.data.process.getActiveStage().getId();
    var statuscode = Xrm.Page.getAttribute("statuscode").getValue();
    Xrm.Page.data.entity.attributes.forEach(function (attribute, index) {
        var control = Xrm.Page.getControl(attribute.getName());
        //truong phong duyet
        if (control != null && stagename == "7d1e41dd-8e6c-459c-3019-5ae332a80466" && statuscode == 100000005) {
            control.setDisabled(true);
            Xrm.Page.getControl("bsd_transportation").setDisabled(true);
            control.getAttribute().setSubmitMode("never");
        }
            //nhan vien lap
        else if (control != null && stagename == "9fa553c0-0fe2-4c79-82e4-09a262171027" && userid != owner[0].id) {
            control.setDisabled(true);
            Xrm.Page.getControl("bsd_transportation").setDisabled(true);
            control.getAttribute().setSubmitMode("never");
        }
            //truong phong duyet
        else if (control != null && stagename == "7d1e41dd-8e6c-459c-3019-5ae332a80466" && statuscode == 100000001 && userid != owner[0].id) {
            control.setDisabled(true);
            Xrm.Page.getControl("bsd_transportation").setDisabled(true);
            control.getAttribute().setSubmitMode("never");
        }
            //cap tham quyen duyet
        else if (control != null && stagename == "d1040793-ce33-4c5c-c144-24ecd6480504" && statuscode == 100000002 && userid != owner[0].id) {
            control.setDisabled(true);
            Xrm.Page.getControl("bsd_transportation").setDisabled(true);
            control.getAttribute().setSubmitMode("never");
        }
            //cap tham quyen duyet
        else if (control != null && stagename == "d1040793-ce33-4c5c-c144-24ecd6480504" && statuscode == 100000006 && userid != owner[0].id) {
            control.setDisabled(true);
            Xrm.Page.getControl("bsd_transportation").setDisabled(true);
            control.getAttribute().setSubmitMode("never");
        }
            //finnish
        else if (control != null && stagename == "b3a9e571-3b15-6b60-b992-d1e5393d2a0b") {
            control.setDisabled(true);
            Xrm.Page.getControl("bsd_transportation").setDisabled(true);
            control.getAttribute().setSubmitMode("never");
        }
    });
}
//End

//Clear lookup
function clear_LoadAddressPort() {
    getControl("bsd_addressport").addPreSearch(presearch_LoadAddressPort);
}
function presearch_LoadAddressPort() {
    getControl("bsd_addressport").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_LoadShiptoaddress() {
    getControl("bsd_shiptoaddress").addPreSearch(presearch_LoadShiptoaddress);
}
function presearch_LoadShiptoaddress() {
    getControl("bsd_shiptoaddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_invoiceaddress() {
    getControl("bsd_addressinvoiceaccount").addPreSearch(presearch_invoiceaddress);
}
function presearch_invoiceaddress() {
    getControl("bsd_addressinvoiceaccount").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_addresscustomer() {
    getControl("bsd_address").addPreSearch(presearch_addresscustomer);
}
function presearch_addresscustomer() {
    getControl("bsd_address").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Contact() {
    getControl("bsd_contact").addPreSearch(presearch_Contact);
}
function presearch_Contact() {
    getControl("bsd_contact").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

//Button by Mr.Phong
function BtnActiveQuoteEnableRule() {
    debugger;
    var id = Xrm.Page.data.entity.getId();
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var totalamount = Xrm.Page.getAttribute("bsd_totalamount").getValue();
    var statecode = Xrm.Page.getAttribute("statuscode").getValue();
    var stagename = Xrm.Page.data.process.getActiveStage().getId();
    //stagename = Xrm.Page.data.process.getActiveStage().getId();
    var xmlaccount = [];
    if (id != " ") {
        if (totalamount == null) {
            return false;
        }
        else {
            if (totalamount != 0) {
                if (rnames.length > 1) {
                    for (var i = 0; i < rnames.length; i++) {
                        //Admin
                        //["Nhân Viên", "Trưởng Phòng", "System Administrator", "Cấp Thẩm Quyền"]//ttc
                        //["4013c487-6af1-42c1-a31a-0199b044bdf4", "4560e2f3-6ea4-4c72-9d3d-73a229bde188", "ec59dfcc-d2f2-4009-9380-9f876f26ab44", "26d2ec02-88ac-4079-93a0-bbd4becadad7"]//ttc
                        if (rnames[i] == "System Administrator") {
                            return true;
                        }
                    }
                }
                else {
                    if (stagename == "b3a9e571-3b15-6b60-b992-d1e5393d2a0b" || rnames == "System Administrator")
                        return true;
                    else
                        return false;
                }
            }
            else {
                return false;
            }
        }

    }
    if (id == " ") {
        return false;
    }
}
function BtnReviseQuoteEnableRule() {
    debugger;
    var statecode = Xrm.Page.getAttribute("statecode").getValue();
    if (statecode == 2) {
        return false;
    } else {
        return true;
    }
}
function BtnCreateEconomicContractEnableRule() {
    debugger;
    var statecode = Xrm.Page.getAttribute("statecode").getValue();
    var id = Xrm.Page.data.entity.getId();
    if (id != "") {
        if (statecode == 1 || statecode == 2) {
            return true;
        }
        else {
            return false;
        }
    } else {
        return false;
    }
}
function BtnEconomicContractClick() {
    debugger;
    try {
        var xml = [];
        var id = Xrm.Page.data.entity.getId();
        if (id != null) {
            fetch(xml, "salesorder", ["name", "customerid", "statuscode", "totalamount", "salesorderid"], ["name"], false, null,
              ["quoteid", "bsd_type"], ["eq", "eq"], [0, id, 1, "861450001"]);
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    if (confirm("Do you want to view Economic Contract ?")) {
                        Xrm.Utility.openEntityForm("salesorder", rs[0].Id);
                    }
                } else {
                    if (confirm("Do you want to create economic contract ?")) {
                        ExecuteAction(id, "quote", "bsd_Create_EconomicContractt", null, function (result) {
                            if (result != null && result.status != null) {
                                if (result.status == "success") {
                                    Xrm.Utility.openEntityForm("salesorder", result.data.ReturnId.value);
                                    Xrm.Utility.openEntityForm(Xrm.Page.data.entity.getEntityName(), Xrm.Page.data.entity.getId());
                                } else {
                                    alert(result.data);
                                }
                            }
                        });
                    }

                }
            }, function (er) {
                console.log(er.message);
            });

        }
        else {
            window.top.$ui.Dialog("Lỗi", "Thông báo lỗi!", null);
        }
    }
    catch (e) {
        window.top.$ui.Dialog("Lỗi", e.message, null);
    }
}




