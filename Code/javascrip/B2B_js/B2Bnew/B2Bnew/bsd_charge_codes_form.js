﻿function AutoLoad() {
    LK_Vender_Account();
    if (formType() == 2) {
    } else {
        Load_FromDate_Auto();
    }
}
//Author:Mr.Đăng
//Description:set Date
function Load_FromDate_Auto() {
    var date = new Date();
    setValue("bsd_date", date);
}
//Author:Mr.Đăng
//Description:Lookup Vender Account
function LK_Vender_Account() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="primarycontactid" />',
                    '<attribute name="telephone1" />',
                    '<attribute name="accountid" />',
                    '<order attribute="name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="in">',
                    '<value>861450004</value>',
                    '<value>861450001</value>',
                    '<value>861450003</value>',
                    '</condition>',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join('');
    LookUp_After_Load("bsd_vender", "account", "name", "Vender", fetchxml);
}
// 21.03.2017 Đăng: set name
//Author:Mr.Đăng
//Description: Set name Charge Code Types
function setName() {
    var chargescodetype = getValue("bsd_chargescodetype");
    if (chargescodetype != null) setValue("bsd_name", chargescodetype[0].name);
    else if (chargescodetype == null) setNull("bsd_name");
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}