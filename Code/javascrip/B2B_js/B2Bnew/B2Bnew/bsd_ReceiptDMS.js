// author: Mr.Binh
function filter_Account_Distributor() {
    debugger;
    var entityName = "account";
    var viewDisplayName = "Distributor Lookup View";
    var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
    xml.push("<entity name='account'> ");
    xml.push("<attribute name='name' /> ");
    xml.push("<attribute name='primarycontactid' /> ");
    xml.push("<attribute name='telephone1' /> ");
    xml.push("<attribute name='accountid' /> ");
    xml.push("<order attribute='name' descending='false' /> ");
    xml.push("<filter type='and'> ");
    xml.push("<condition attribute='bsd_accounttype' operator='eq' value='100000000' /> ");
    xml.push("</filter> ");
    xml.push("</entity> ");
    xml.push("</fetch> ");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                        "<row name='result'  " + "id='accountid'>  " +
                        "<cell name='name'   " + "width='200' />  " +
                        "<cell name='telephone1'    " + "width='100' />  " +
                        "</row>   " +
                     "</grid>   ";
    Xrm.Page.getControl("bsd_distributor").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
}

// author: Mr.Binh
function filter_Address_Distributor() {
    debugger;
    var entityName = "bsd_address";
    var account_dis = Xrm.Page.getAttribute("bsd_distributor").getValue();
    var viewDisplayName = "Employee From Lookup View";
    var viewId = "{FF82D3E5-2040-4798-AABE-9ECB298228E9}";
    if (account_dis != null) {
        var xml = [];
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xml.push("<entity name='bsd_address'> ");
        xml.push("<attribute name='bsd_addressid' /> ");
        xml.push("<attribute name='bsd_name' /> ");
        xml.push("<attribute name='createdon' /> ");
        xml.push("<order attribute='bsd_name' descending='false' /> ");
        xml.push("<filter type='and'> ");
        xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='" + account_dis[0].id + "' /> ");
        xml.push("</filter> ");
        xml.push("</entity> ");
        xml.push("</fetch> ");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                            "<row name='result'  " + "id='bsd_addressid'>  " +
                            "<cell name='bsd_name'   " + "width='200' />  " +
                            "<cell name='createdon'    " + "width='100' />  " +
                            "</row>   " +
                         "</grid>   ";
        Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    }
}
//author:Mr.Phong
//description:get address from distributor
function get_distributor()
{
    var xml = [];
        var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
        var entityName = "account";
        var viewDisplayName = "test";
        var xml = [];
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='account'>");
        xml.push("<attribute name='name'/>");
        xml.push("<attribute name='primarycontactid' />")
        xml.push("<attribute name='telephone1' />");
        xml.push("<attribute name='accountid' />");
        xml.push("<order attribute='name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_accounttype' operator='eq' value='100000000' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='accountid'>  " +
                           "<cell name='primarycontactid'   " + "width='200' />  " +
                           "<cell name='telephone1'    " + "width='100' />  " +
                             "<cell name='name'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_distributor").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
}
//author:Mr.Phong
//description:get address from distributor
function get_address()
{
    var distributor = Xrm.Page.getAttribute("bsd_distributor").getValue();
    var fielddefault = Xrm.Page.getAttribute("bsd_address");
    var xml = [];
    if (distributor != null)
    {
        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName = "bsd_address";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_address'>");
        xml.push("<attribute name='bsd_addressid'/>");
        xml.push("<attribute name='bsd_name' />")
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='" + distributor[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='name'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='name'>  " +
                           "<cell name='primarycontactid'   " + "width='200' />  " +
                           "<cell name='telephone1'    " + "width='100' />  " +
                             "<cell name='accountid'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_distributor").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                fielddefault.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.bsd_name.value,
                    entityType: rs[0].logicalName
                }]);
            }
        },
          function (er) {
              console.log(er.message)
          });
    }
    if (distributor == null)
    {
        fielddefault.setValue(null);
    }

}