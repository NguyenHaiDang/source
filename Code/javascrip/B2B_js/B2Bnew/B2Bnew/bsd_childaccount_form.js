﻿// 22.03.2017 Đăng: Autoload
function Autoload() {
    setDisabled_ParentAccount();
}
// JavaScript library
function set_value(a, b) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getAttribute(a[i]).setValue(b);
    }
}
function get_value(a) {
    var b = [];
    for (var i = 0; i < a.length; i++) {
        var j = Xrm.Page.getAttribute(a[i]).getValue();
        if (j != null) {
            var string = typeof j;
            if (string == "string") {
                b.push(j);
            }
            if (string == "boolean") {
                if (j == true) {
                    b.push("1");
                }
                if (j == false) {
                    b.push("0");
                }
            }
            if (string == "number") {
                b.push(j);
            }
            if (string == "object") {
                if (j[0].id != null) {
                    b.push(j[0].id);
                }
                if (j[0].name != null) {
                    b.push(j[0].name);
                }
            }
        }
        if (j == null) {
            b.push(null);
        }
    }
    return b;
}
function set_requirelevel(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.getAttribute(a[i]).setRequiredLevel("none");
        }
        if (b == 1) {
            Xrm.Page.getAttribute(a[i]).setRequiredLevel("required");
        }
    }
}
function set_disable(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.getControl(a[i]).setDisabled(false);
        }
        if (b == 1) {
            Xrm.Page.getControl(a[i]).setDisabled(true);
        }
    }
}
function set_visible(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.ui.controls.get(a[i]).setVisible(false);
        }
        if (b == 1) {
            Xrm.Page.ui.controls.get(a[i]).setVisible(true);
        }
    }
}
function set_notification(a, b) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getControl(a[i]).setNotification("" + b);
    }
}
function clear_nofitication(a) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getControl(a[i]).clearNotification();
    }
}
function fetch(xml, entity, attribute, orderattribute, descendingvalue, ascendingvalue, conditionattribute, operator, conditionvalue) {
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='" + entity + "'>");
    for (var i = 0; i < attribute.length; i++) {
        xml.push("<attribute name='" + attribute[i] + "' />");
    }
    if (descendingvalue != null) {
        xml.push("<order attribute='" + orderattribute + "' descending='" + descendingvalue + "' />");
    }
    if (ascendingvalue != null) {
        xml.push("<order attribute='" + orderattribute + "' descending='" + ascendingvalue + "' />");
    }
    xml.push("<filter type='and'>");
    for (var j = 0; j < conditionattribute.length; j++) {
        if (operator[j] == "inn") {
            xml.push("<condition attribute='" + conditionattribute[j] + "' operator='in'>");
            for (var k = 0; k < conditionvalue.length; k++) {
                if (conditionvalue[k] === parseInt(conditionvalue[k], 10)) {
                    if (j == conditionvalue[k]) {
                        for (var p = 1; p < conditionvalue.length; p++) {
                            if (conditionvalue[k + p] === parseInt(conditionvalue[k + p], 10) || conditionvalue[k + p] == null) {
                                xml.push("</condition>");
                                break;
                            }
                            else {
                                xml.push("<value>" + conditionvalue[k + p] + "");
                                xml.push("</value>");
                            }
                        }
                    }
                }
            }
        }
        else {
            for (var k = 0; k < conditionvalue.length; k++) {
                if (conditionvalue[k] === parseInt(conditionvalue[k], 10)) {
                    if (j == conditionvalue[k]) {
                        for (var p = 1; p < conditionvalue.length; p++) {
                            if (conditionvalue[k + p] === parseInt(conditionvalue[k + p], 10) || conditionvalue[k + p] == null) {
                                break;
                            }
                            else {
                                xml.push("<condition attribute='" + conditionattribute[j] + "' operator='" + operator[j] + "'  value='" + conditionvalue[k + p] + "' />");
                            }
                        }
                    }
                }
            }
        }
    }
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
}
//Author:Mr.Phong
//Description:set value null field child when create
function sevaluenullfieldchildwhencreate() {
    if (Xrm.Page.ui.getFormType() == 1) {
        var xml = [];
        var xml1 = [];
        set_value(["bsd_childaccount"], null);
        var a = [];
        a = get_value(["bsd_parentaccount"]);
        fetch(xml1, "bsd_childaccount", ["bsd_childaccountid", "bsd_name", "createdon", "bsd_childaccount"], ["bsd_childaccount"], false, null
            , ["bsd_parentaccount"], ["eq"], [0, a[0], 1])
        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
            if (rs1.length > 0) {
                var b = [];
                for (var i = 0; i < rs1.length; i++) {
                    b.push(rs1[i].attributes.bsd_childaccount.guid);
                }
                var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
                var entityName = "account";
                var viewDisplayName = "test";
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml.push("<entity name='account'>");
                xml.push("<attribute name='name' />");
                xml.push("<attribute name='primarycontactid' />");
                xml.push("<attribute name='telephone1' />");
                xml.push("<attribute name='accountid' />");
                xml.push("<order attribute='name' descending='false' />");
                xml.push("<filter type='and'>");
                xml.push("<condition attribute='accountid' operator='not-in'>");
                for (k = 0; k < b.length; k++) {
                    xml.push("<value uitype='account'>" + '{' + b[k] + '}' + "");
                    xml.push("</value>");
                }
                xml.push("</condition>");
                xml.push("</filter>");
                xml.push("</entity>");
                xml.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='accountid'>  " +
              "<cell name='name'   " + "width='200' />  " +
              "<cell name='primarycontactid'    " + "width='100' />  " +
                "<cell name='telephone1'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
                Xrm.Page.getControl("bsd_childaccount").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.Phong
//Description:set value filed child account not same parent account
function setvaluefiledchildaccountnotsameparentaccount() {
    var a = [];
    a = get_value(["bsd_parentaccount", "bsd_childaccount"]);
    if (a[0] == a[2]) {
        set_value(["bsd_childaccount"], null);
        Xrm.Page.getControl("bsd_childaccount").setNotification("child account not same parent account!!");
    }
    else {
        Xrm.Page.getControl("bsd_childaccount").clearNotification();
    }
}
//Author:Mr.Phong
//Description:set address from child account
function setaddressfromchildaccount() {
    var a = [];
    var xml1 = [];
    var xml = [];
    a = get_value(["bsd_childaccount"]);
    var fieldchildaccountaddress = Xrm.Page.getAttribute("bsd_childaccountaddress");
    if (a[0] != null) {
        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName = "bsd_address";
        var viewDisplayName = "test";
        fetch(xml1, "bsd_address", ["bsd_name", "bsd_purpose", "bsd_account", "bsd_contact", "bsd_addressid"]
            , ["bsd_name"], false, null, ["bsd_account", "bsd_purpose"], ["eq", "eq"], [0, a[0], 1, "861450000"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
           "<row name='result'  " + "id='bsd_addressid'>  " +
           "<cell name='bsd_name'   " + "width='200' />  " +
           "<cell name='bsd_purpose'    " + "width='100' />  " +
             "<cell name='bsd_account'    " + "width='100' />  " +
               "<cell name='bsd_contact'    " + "width='100' />  " +
           "</row>   " +
        "</grid>   ";
        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
            if (rs1.length > 0) {
                fieldchildaccountaddress.setValue([{
                    id: rs1[0].Id,
                    name: rs1[0].attributes.bsd_name.value,
                    entityType: rs1[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_childaccountaddress").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
            }
            if (rs1.length == 0) {
                var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName1 = "bsd_address";
                var viewDisplayName1 = "test";
                fetch(xml, "bsd_address", ["bsd_name", "bsd_purpose", "bsd_account", "bsd_contact", "bsd_addressid"]
                    , ["bsd_name"], false, null, ["bsd_account", "bsd_purpose"], ["eq", "eq"], [0, "{A20A206A-EF74-E621-73CC-000C582C5A3D}", 1, "861450000"]);
                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                   "<row name='result'  " + "id='bsd_addressid'>  " +
                   "<cell name='bsd_name'   " + "width='200' />  " +
                   "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "<cell name='bsd_account'    " + "width='100' />  " +
                       "<cell name='bsd_contact'    " + "width='100' />  " +
                   "</row>   " +
                "</grid>   ";
                Xrm.Page.getControl("bsd_childaccountaddress").addCustomView(viewId1, entityName1, viewDisplayName1, xml.join(""), layoutXml1, true);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    if (a[0] == null) {
        fieldchildaccountaddress.setValue(null);
        var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName1 = "bsd_address";
        var viewDisplayName1 = "test";
        fetch(xml, "bsd_address", ["bsd_name", "bsd_purpose", "bsd_account", "bsd_contact", "bsd_addressid"]
            , ["bsd_name"], false, null, ["bsd_account", "bsd_purpose"], ["eq", "eq"], [0, "{A20A206A-EF74-E621-73CC-000C582C5A3D}", 1, "861450000"]);
        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
           "<row name='result'  " + "id='bsd_addressid'>  " +
           "<cell name='bsd_name'   " + "width='200' />  " +
           "<cell name='bsd_purpose'    " + "width='100' />  " +
             "<cell name='bsd_account'    " + "width='100' />  " +
               "<cell name='bsd_contact'    " + "width='100' />  " +
           "</row>   " +
        "</grid>   ";
        Xrm.Page.getControl("bsd_childaccountaddress").addCustomView(viewId1, entityName1, viewDisplayName1, xml.join(""), layoutXml1, true);
    }
}
//Author:Mr.Phong
//Description:set address from child account
function setaddressfromchildaccountonload() {
    var a = [];
    var xml1 = [];
    var xml = [];
    a = get_value(["bsd_childaccount"]);
    var fieldchildaccountaddress = Xrm.Page.getAttribute("bsd_childaccountaddress");
    if (a[0] != null) {
        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName = "bsd_address";
        var viewDisplayName = "test";
        fetch(xml1, "bsd_address", ["bsd_name", "bsd_purpose", "bsd_account", "bsd_contact", "bsd_addressid"]
            , ["bsd_name"], false, null, ["bsd_account", "bsd_purpose"], ["eq", "eq"], [0, a[0], 1, "861450000"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
           "<row name='result'  " + "id='bsd_addressid'>  " +
           "<cell name='bsd_name'   " + "width='200' />  " +
           "<cell name='bsd_purpose'    " + "width='100' />  " +
             "<cell name='bsd_account'    " + "width='100' />  " +
               "<cell name='bsd_contact'    " + "width='100' />  " +
           "</row>   " +
        "</grid>   ";
        Xrm.Page.getControl("bsd_childaccountaddress").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
    }
    if (a[0] == null) {
        fieldchildaccountaddress.setValue(null);
        var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName1 = "bsd_address";
        var viewDisplayName1 = "test";
        fetch(xml, "bsd_address", ["bsd_name", "bsd_purpose", "bsd_account", "bsd_contact", "bsd_addressid"]
            , ["bsd_name"], false, null, ["bsd_account", "bsd_purpose"], ["eq", "eq"], [0, "{A20A206A-EF74-E621-73CC-000C582C5A3D}", 1, "861450000"]);
        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
           "<row name='result'  " + "id='bsd_addressid'>  " +
           "<cell name='bsd_name'   " + "width='200' />  " +
           "<cell name='bsd_purpose'    " + "width='100' />  " +
             "<cell name='bsd_account'    " + "width='100' />  " +
               "<cell name='bsd_contact'    " + "width='100' />  " +
           "</row>   " +
        "</grid>   ";
        Xrm.Page.getControl("bsd_childaccountaddress").addCustomView(viewId1, entityName1, viewDisplayName1, xml.join(""), layoutXml1, true);
    }
}
//Author:Mr.Phong
//Description:set value null field child when create
function sevaluenullfieldchildonchange() {
    var xml = [];
    var xml1 = [];
   
    var a = [];
    a = get_value(["bsd_parentaccount"]);
    if (a[0] != null)
    {
        set_value(["bsd_childaccount", "bsd_childaccountaddress"], null);
        fetch(xml1, "bsd_childaccount", ["bsd_childaccountid", "bsd_name", "createdon", "bsd_childaccount"], ["bsd_childaccount"], false, null
            , ["bsd_parentaccount"], ["eq"], [0, a[0], 1])
        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
            if (rs1.length > 0) {
                var b = [];
                for (var i = 0; i < rs1.length; i++) {
                    b.push(rs1[i].attributes.bsd_childaccount.guid);
                }
                var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
                var entityName = "account";
                var viewDisplayName = "test";
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml.push("<entity name='account'>");
                xml.push("<attribute name='name' />");
                xml.push("<attribute name='primarycontactid' />");
                xml.push("<attribute name='telephone1' />");
                xml.push("<attribute name='accountid' />");
                xml.push("<order attribute='name' descending='false' />");
                xml.push("<filter type='and'>");
                xml.push("<condition attribute='accountid' operator='not-in'>");
                for (k = 0; k < b.length; k++) {
                    xml.push("<value uitype='account'>" + '{' + b[k] + '}' + "");
                    xml.push("</value>");
                }
                xml.push("</condition>");
                xml.push("</filter>");
                xml.push("</entity>");
                xml.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='accountid'>  " +
              "<cell name='name'   " + "width='200' />  " +
              "<cell name='primarycontactid'    " + "width='100' />  " +
                "<cell name='telephone1'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
                Xrm.Page.getControl("bsd_childaccount").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    if (a[0] == null)
    {
        set_value(["bsd_childaccount", "bsd_childaccountaddress"], null);
    }
}
// 22.03.2017 Đăng: setDisabled Account
function setDisabled_ParentAccount() {
    var parentaccount = getValue("bsd_parentaccount");
    if (parentaccount != null) {
        setDisabled("bsd_parentaccount", true);
    }
}