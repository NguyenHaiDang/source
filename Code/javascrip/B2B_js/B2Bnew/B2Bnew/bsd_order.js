function get_quote()
{
    var quote_id = Xrm.Page.getAttribute("quoteid").getValue();
    if (quote_id != null) {
        var xml = [];
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
        xml.push("<entity name='quote'>");
        xml.push("<attribute name='name'/>");
        xml.push("<attribute name='customerid'/>");
        xml.push("<attribute name='statecode'/>");
        xml.push("<attribute name='totalamount'/>");
        xml.push("<attribute name='quoteid'/>");
        xml.push("<attribute name='createdon'/>");
        xml.push("<attribute name='bsd_taxvalue'/>");
        xml.push("<attribute name='bsd_value'/>");
        xml.push("<attribute name='pricelevelid'/>");
        xml.push("<attribute name='bsd_porter'/>");
        xml.push("<attribute name='bsd_priceoftransportation'/>");
        xml.push("<attribute name='bsd_carrierpartners'/>");
        xml.push("<order attribute='name' descending='false' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='name' operator='eq' value='" + quote_id[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_value != null) {
                    var gia = rs[0].attributes.bsd_value.value;
                    Xrm.Page.getAttribute("bsd_value").setValue(gia);
                }
            }
        },
           function (er) {
               console.log(er.message)
           });
        }
}