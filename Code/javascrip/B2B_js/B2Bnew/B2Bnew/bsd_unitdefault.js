function init() {
    if (formType() == 1) {

    } else {
        if (getValue("bsd_unit") != null) setDisabled("bsd_unit", true);
    }
}
function unit_change() {
    clearNotification("bsd_unit");
    var unit = getValue("bsd_unit");
    if (unit != null) {
        setValue("bsd_name", unit[0].name);
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_unitdefault">',
                  '      <attribute name="bsd_unitdefaultid" />',
                  '      <attribute name="bsd_name" />',
                  '      <attribute name="createdon" />',
                   '     <attribute name="bsd_default" />',
                  '      <attribute name="ownerid" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_unit" operator="eq" uitype="uom" value="' + unit[0].id + '" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                setNotification("bsd_unit", "Unit Default này đã tồn tại!");
                setNull("bsd_name");
            }
        });
    } else {
        setNull("bsd_name");
    }
}