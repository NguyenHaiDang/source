//Author:Mr.Đăng
//Description:Check Charges Code ID
function Check_ChargescodeID() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var chargescodeid = getValue("bsd_chargescodeid");
    var id = getId();
    if (chargescodeid != null) {
        if (mikExp.test(chargescodeid)) {
            setNotification("bsd_chargescodeid", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_chargescodetype">',
                            '<attribute name="bsd_chargescodetypeid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_chargescodeid" operator="eq" value="' + chargescodeid + '" />',
                            '<condition attribute="bsd_chargescodetypeid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_chargescodeid", "Mã đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_chargescodeid");
                setValue("bsd_chargescodeid", chargescodeid.toUpperCase());
                setValue("bsd_name", chargescodeid.toUpperCase());
            }
        }
    }
    else if (chargescodeid == null) {
        setNull("bsd_name");
    }
}