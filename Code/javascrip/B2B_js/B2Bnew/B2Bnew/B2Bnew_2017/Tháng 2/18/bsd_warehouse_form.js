﻿//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    LK_CompanyName();
    if (formType() == 2) {
    } else {
        clear_addresscompanyname();
    }
}
//Author:Mr.Đăng
//Description: Load Company name
function LK_CompanyName() {
    var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="primarycontactid" />',
                    '<attribute name="accountid" />',
                    '<order attribute="name" descending="false" />',
                    '<filter type="and">',
                    '<condition attribute="bsd_accounttype" operator="eq" value="861450005" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");
    LookUp_After_Load("bsd_companyname", "account", "name", "CompanyName", fetchxml);
    LK_AddressCompanyName(false);
}
//Author:Mr.Đăng
//Description: Address from CompanyName
function LK_AddressCompanyName(reset) {
    var companyname = getValue("bsd_companyname");
    getControl("bsd_address").removePreSearch(presearch_addresscompanyname);
    if (reset != false) setNull("bsd_address");
    if (companyname != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_purpose" />',
                        '<attribute name="bsd_account" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + companyname[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        LookUp_After_Load("bsd_address", "bsd_address", "bsd_name", "Address", fetchxml);
    }
    else if (reset != false) {
        clear_addresscompanyname();
    }
}
function clear_addresscompanyname() {
    getControl("bsd_address").addPreSearch(presearch_addresscompanyname);
}
function presearch_addresscompanyname() {
    getControl("bsd_address").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}