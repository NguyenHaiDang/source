﻿//Author:Mr.Đăng
//Description: Load name
function Load_name() {
    Load_name_deliveryproductbill();
}

function Load_name_deliveryproductbill() {
    var product = getValue("bsd_product");
    setNull(["bsd_name", "bsd_unit"]);
    if (product != null) {
        setValue("bsd_name", product[0].name);
        var fetchxml2 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                  '  <entity name="product">',
                  '    <attribute name="name" />',
                  '    <attribute name="defaultuomid" />',
                  '    <order attribute="productnumber" descending="false" />',
                  '    <filter type="and">',
                  '      <condition attribute="productid" operator="eq" uitype="product" value="' + product[0].id + '" />',
                  '    </filter>',
                  '  </entity>',
                  '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml2).then(function (rs2) {
            if (rs2.length > 0) {
                console.log("data: ", rs2);
                var first2 = rs2[0];
                if (getValue("bsd_unit") == null) {
                    setValue("bsd_unit", [{
                        id: first2.attributes.defaultuomid.guid,
                        name: first2.attributes.defaultuomid.name,
                        entityType: "uom"
                    }]);
                }
            }
        }, function (er) {
            alert("không lấy được bảng")
        });
    }
}
// load nhung unit co (*)
function load_unit() {
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="uom">',
        '<attribute name="name" />',
        '<filter type="and">',
          '<condition attribute="uomscheduleid" operator="eq" uitype="uomschedule" value="{A8C3859A-0095-E611-80CC-000C294C7A2D}" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='uomid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                       "<row name='result'  " + "id='uomid'>  " +
                                       "<cell name='name'   " + "width='200' />  " +
                                       "</row>" +
                                    "</grid>";
    getControl("bsd_tounit").addCustomView(getDefaultView("bsd_tounit"), "uom", "uom", xml, layoutXml, true);
}