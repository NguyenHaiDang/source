﻿//Author:Mr.Đăng
//Description: Load name Sales Tax Group
function Load_Name() {
    var salestaxcode = getValue("bsd_saletaxcode");
    if (salestaxcode != null) {
        setValue("bsd_name", salestaxcode[0].name);
    }
    else {
        setNull("bsd_name");
    }
}