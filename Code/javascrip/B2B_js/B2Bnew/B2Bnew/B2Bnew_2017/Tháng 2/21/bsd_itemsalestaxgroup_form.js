﻿//Author:Mr.Đăng
//Description:Check Code
function Check_Code() {
    var code = getValue("bsd_code");
    if (code != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_itemsalestaxgroup">',
                        '<attribute name="bsd_itemsalestaxgroupid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="createdon" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_code" operator="eq" value="' + code + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setNotification("bsd_code", "Mã đã tồn tại, vui lòng kiểm tra lại.");
        } else {
            clearNotification("bsd_code");
        }
        setValue("bsd_name", code);
    }
    else {
        setNull("bsd_name");
    }
}