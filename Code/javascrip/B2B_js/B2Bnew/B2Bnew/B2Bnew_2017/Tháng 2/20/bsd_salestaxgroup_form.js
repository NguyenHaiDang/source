﻿//Author:Mr.Đăng
//Description: Load name Sales Tax Group
function Load_Name() {
    var itemsalestaxgroup = getValue("bsd_itemsalestaxgroup");
    if (itemsalestaxgroup != null) {
        setValue("bsd_name", itemsalestaxgroup);
    }
    else
    {
        setNull("bsd_name");
    }
}