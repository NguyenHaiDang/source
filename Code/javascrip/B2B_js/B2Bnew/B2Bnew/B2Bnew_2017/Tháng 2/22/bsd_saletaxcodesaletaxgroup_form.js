﻿//Author:Mr.Đăng
//Description: Load name Sales Tax Group
function Load_Name() {
    var salestaxcode = getValue("bsd_saletaxcode");
    if (salestaxcode != null) {
        setValue("bsd_name", salestaxcode[0].name);
    }
    else {
        setNull("bsd_name");
    }
}
//Author:Mr.Đăng
//Description:load description
function load_description() {
    var saletaxcode = getValue("bsd_saletaxcode");
    if (saletaxcode != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_saletaxcode">',
                        '<attribute name="bsd_saletaxcodeid" />',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_percentageamount" />',
                        '<attribute name="bsd_description" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_saletaxcodeid" operator="eq" uitype="bsd_saletaxcode" value="' + saletaxcode[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        console.log(saletaxcode);
        if (rs.length > 0) {
            setValue("bsd_percentageamount", rs[0].attributes.bsd_percentageamount.value);
            setValue("bsd_description", rs[0].attributes.bsd_description.value);
        }
    }
    else {
        setValue("bsd_percentageamount", null);
        setValue("bsd_description", null);
    }
}