// JavaScript library
function set_value(a, b) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getAttribute(a[i]).setValue(b);
    }
}
function get_value(a) {
    var b = [];
    for (var i = 0; i < a.length; i++) {
        var j = Xrm.Page.getAttribute(a[i]).getValue();
        if (j != null) {
            var string = typeof j;
            if (string == "string") {
                b.push(j);
            }
            if (string == "boolean") {
                if (j == true) {
                    b.push("1");
                }
                if (j == false) {
                    b.push("0");
                }
            }
            if (string == "number") {
                b.push(j);
            }
            if (string == "object") {
                if (j[0].id != null) {
                    b.push(j[0].id);
                }
                if (j[0].name != null) {
                    b.push(j[0].name);
                }
            }
        }
        if (j == null) {
            b.push(null);
        }
    }
    return b;
}
function set_requirelevel(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.getAttribute(a[i]).setRequiredLevel("none");
        }
        if (b == 1) {
            Xrm.Page.getAttribute(a[i]).setRequiredLevel("required");
        }
    }
}
function set_disable(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.getControl(a[i]).setDisabled(false);
        }
        if (b == 1) {
            Xrm.Page.getControl(a[i]).setDisabled(true);
        }
    }
}
function set_visible(a, b) {
    for (var i = 0; i < a.length; i++) {
        if (b == 0) {
            Xrm.Page.ui.controls.get(a[i]).setVisible(false);
        }
        if (b == 1) {
            Xrm.Page.ui.controls.get(a[i]).setVisible(true);
        }
    }
}
function set_notification(a, b) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getControl(a[i]).setNotification("" + b);
    }
}
function clear_nofitication(a) {
    for (var i = 0; i < a.length; i++) {
        Xrm.Page.getControl(a[i]).clearNotification();
    }
}
function fetch(xml, entity, attribute, orderattribute, descendingvalue, ascendingvalue, conditionattribute, operator, conditionvalue) {
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='" + entity + "'>");
    for (var i = 0; i < attribute.length; i++) {
        xml.push("<attribute name='" + attribute[i] + "' />");
    }
    if (descendingvalue != null) {
        xml.push("<order attribute='" + orderattribute + "' descending='" + descendingvalue + "' />");
    }
    if (ascendingvalue != null) {
        xml.push("<order attribute='" + orderattribute + "' descending='" + ascendingvalue + "' />");
    }
    xml.push("<filter type='and'>");
    for (var j = 0; j < conditionattribute.length; j++) {
        if (operator[j] == "inn") {
            xml.push("<condition attribute='" + conditionattribute[j] + "' operator='in'>");
            for (var k = 0; k < conditionvalue.length; k++) {
                if (conditionvalue[k] === parseInt(conditionvalue[k], 10)) {
                    if (j == conditionvalue[k]) {
                        for (var p = 1; p < conditionvalue.length; p++) {
                            if (conditionvalue[k + p] === parseInt(conditionvalue[k + p], 10) || conditionvalue[k + p] == null) {
                                xml.push("</condition>");
                                break;
                            }
                            else {
                                xml.push("<value>" + conditionvalue[k + p] + "");
                                xml.push("</value>");
                            }
                        }
                    }
                }
            }
        }
        else {
            for (var k = 0; k < conditionvalue.length; k++) {
                if (conditionvalue[k] === parseInt(conditionvalue[k], 10)) {
                    if (j == conditionvalue[k]) {
                        for (var p = 1; p < conditionvalue.length; p++) {
                            if (conditionvalue[k + p] === parseInt(conditionvalue[k + p], 10) || conditionvalue[k + p] == null) {
                                break;
                            }
                            else {
                                xml.push("<condition attribute='" + conditionattribute[j] + "' operator='" + operator[j] + "'  value='" + conditionvalue[k + p] + "' />");
                            }
                        }
                    }
                }
            }
        }
    }
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
}
// Global
var diachi = null;
var phuong = null;
var quanhuyen = null;
var tinhthanh = null;
var quocgia = null;

function OnLoad() {
    debugger;

    //Load dia chi
    if (Xrm.Page.getAttribute("bsd_diachi").getValue() != null)
        MakeAddress();
}

// Quoc Gia OnChange
function Onchange_QuocGia() {

    quanhuyen = "bsd_quan_thuongtru";
    tinhthanh = "bsd_thanhphotinhtieubang_thuongtru";
    Xrm.Page.getAttribute(tinhthanh).setValue(null);
    Xrm.Page.getAttribute(quanhuyen).setValue(null);
    MakeAddress();
}

// Tinh Thanh OnChange
function Onchange_TinhThanh() {
    Xrm.Page.getAttribute("bsd_quan_thuongtru").setValue(null);
    MakeAddress();
}

// Copy tinh thanh vao Address Composite
function MakeAddress() {
    debugger;
    //UnBlock 
    Xrm.Page.ui.controls.get("bsd_diachi").setDisabled(false);

    var tmp = null;
    var fulltmp = null;
    var sonha = null;
    var fulladdress = null;

    address = "address1_line1";
    province = "address1_stateorprovince";
    country = "address1_country";

    // field
    fulladdress = "bsd_diachi";
    sonha = "bsd_duongsonha_thuongtru";
    phuong = "bsd_phuong_thuongtru";
    quanhuyen = "bsd_quan_thuongtru";
    tinhthanh = "bsd_thanhphotinhtieubang_thuongtru";
    quocgia = "bsd_quocgialanhtho_thuongtru";

    //Duong & so nha
    if (Xrm.Page.getAttribute(sonha).getValue()) {
        tmp = Xrm.Page.getAttribute(sonha).getValue();
    }

    //phuong  
    if (Xrm.Page.getAttribute(phuong).getValue()) {
        if (tmp != "")
            tmp += ", " + Xrm.Page.getAttribute(phuong).getValue();
    }

    //quanhuyen  
    if (Xrm.Page.getAttribute(quanhuyen).getValue()) {
        if (tmp != null)
            tmp += ", " + Xrm.Page.getAttribute(quanhuyen).getValue()[0].name;
    }

    fulltmp = tmp;
    Xrm.Page.getAttribute(address).setValue(tmp);

    //tinh thanh
    if (Xrm.Page.getAttribute(tinhthanh).getValue()) {
        tmp = Xrm.Page.getAttribute(tinhthanh).getValue()[0].name;
        Xrm.Page.getAttribute(province).setValue(tmp);
        fulltmp += ", " + tmp;

    }

    //quoc gia
    if (Xrm.Page.getAttribute(quocgia).getValue()) {
        tmp = Xrm.Page.getAttribute(quocgia).getValue()[0].name;
        Xrm.Page.getAttribute(country).setValue(tmp);
        fulltmp += ", " + tmp;
    }
    Xrm.Page.getAttribute(fulladdress).setValue(fulltmp);
    Xrm.Page.ui.controls.get("bsd_diachi").setDisabled(true);
}

function hide_Industrial() {
    debugger;
    var loai = Xrm.Page.getAttribute("bsd_industry_vl").getValue();

    if (loai != null) {
        Xrm.Page.getControl("bsd_beverage").setVisible(loai.indexOf(1) != -1);
        Xrm.Page.getControl("bsd_food").setVisible(loai.indexOf(2) != -1);
        Xrm.Page.getControl("bsd_milkicecream").setVisible(loai.indexOf(3) != -1);
        Xrm.Page.getControl("bsd_confectionary").setVisible(loai.indexOf(4) != -1);

    }
}
function filter_subgriddkbylookupkh() {
    if (Xrm.Page.ui.getFormType() == 2) {
        //var objSubGrid = document.getElementById("gridaddress");
        //var objSubGrid = window.parent.document.getElementById("gridaddress");
        //var objSubGrid = Xrm.Page.getControl("gridaddress").getGrid();
        var objSubGrid = document.getElementById("gridaddress");
        if (objSubGrid == null) {
            setTimeout(filter_subgriddkbylookupkh, 2000);
            return;
        } else {
            var lookup = Xrm.Page.getAttribute("customerid").getValue();
            //if (lookup != null) {
            var FetchXml = "<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0'>" +
                                "<entity name='bsd_address'>" +
                                    "<attribute name='bsd_addressid'/>" +
                                    "<attribute name='bsd_name'/>" +
                                    "<attribute name='bsd_name'/>" +
                                    "<attribute name='createdon'/>" +
                                    "<attribute name='bsd_account'/>" +
                                    "<order attribute='bsd_name' descending='false' />" +
                                    "<filter type='and'>" +
                                        "<condition attribute='bsd_account' operator='null' />" +
                                     "</filter>" +
                                "</entity>" +
                            "</fetch>";
            if (objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", FetchXml);
                objSubGrid.control.Refresh();
            } else {
                setTimeout(filter_subgriddkbylookupkh, 500);
            }
            //}
        }
    }
}
//author:Mr.Phong
//description:filter type subgrid by account number
function filter_subgriddkbylookupaccno() {
    var objSubGrid = document.getElementById("bsd_Factory");
    //var objSubGrid = window.parent.document.getElementById("Transportation");
    if (objSubGrid == null) {
        setTimeout(filter_subgriddkbylookupaccno, 2000);
        return;
    } else {
        var lookup = Xrm.Page.getAttribute("accountnumber").getValue();
        if (lookup != null) {
            var FetchXml = "<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0'>" +
                                "<entity name='bsd_factory'>" +
                                    "<attribute name='bsd_factoryid'/>" +
                                    "<attribute name='bsd_name'/>" +
                                    "<attribute name='createdon'/>" +
                                    "<order attribute='bsd_name' descending='false' />" +
                                    "<link-entity name='account' from='accountid' to='bsd_account' alias='ae'>" +
                                    "<filter type='and'>" +
                                    "<condition attribute='accountnumber' operator='eq' value='" + lookup + "' />" +
                                    "</filter>" +
                                    "</link-entity>" +
                                    "</entity>" +
                                    "</fetch>";
            if (objSubGrid.control != null) {
                objSubGrid.control.SetParameter("fetchXML", FetchXml);
                objSubGrid.control.Refresh();
            } else {
                setTimeout(filter_subgriddkbylookupaccno, 500);
            }
        }
    }
}
//Author:Mr.Phong
//Description:check same tax registration for each account
function check_sametaxregistrationonchange() {
    var taxregistration = Xrm.Page.getAttribute("bsd_taxregistration").getValue();
    var name = Xrm.Page.getAttribute("name").getValue();
    var noidentify = Xrm.Page.getAttribute("bsd_noidentify").getValue();
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='account'>");
    xml.push("<attribute name='name' />");
    xml.push("<attribute name='primarycontactid' />");
    xml.push("<attribute name='telephone1' />");
    xml.push("<attribute name='accountid' />");
    xml.push("<order attribute='name' descending='false' />");
    xml.push("<filter type='and'>");
    xml.push("<condition attribute='bsd_taxregistration' operator='eq'   value='" + taxregistration + "' />");
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
    CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
        if (rs.length > 0) {
            alert("Tax Registration is exist!!!");
            if (Xrm.Page.ui.getFormType() == 1) {
                Xrm.Page.getAttribute("name").setValue(null);
            }
            Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
        }
    }, function (er) {
        console.log(er.message)
    });
}
//Author:Mr.phong
//Description:set no identify
function set_noindentify() {
    if (Xrm.Page.getAttribute("bsd_noidentify").getValue() == null) {
        if (Xrm.Page.ui.getFormType() == 1) {
            Xrm.Page.getAttribute("bsd_noidentify").setValue("1");
        }
    }
}
//Author:Mr.Phong
//Description:create principal contract
function createprincipalcontract() {
    var noidentify = Xrm.Page.getAttribute("bsd_noidentify").getValue();
    if (noidentify == "0") {
        Notify.add("Bạn có muốn tạo hợp đồng nguyên tắc không?", "LOADING", "sale",
     [{
         type: "button",
         text: "Create Principal Contract",
         callback: function createprincipal(parameters) {
             var parameters = {};
             parameters["regarding_Id"] = Xrm.Page.data.entity.getId();
             Xrm.Utility.openEntityForm("bsd_principalcontract", null, parameters);
         }
     },
     {
         type: "link",
         text: "Tạo sau!",
         callback: function () {
             Notify.remove("sale");
         }
     }]);
    }


}
// trung. kiem tra da co tax chua.
function check_exist_tax_registration() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?|a-z|A-Z]/;
    var number = getValue("bsd_taxregistration");
    if (number != null) {
        if (mikExp.test(number)) {
            setNotification("bsd_taxregistration", "Không thể nhập ký tự ngoài số, vui lòng kiểm tra lại.");
        }
        else
        {
            var xml = [];
            var bsd_taxregistration = Xrm.Page.getAttribute("bsd_taxregistration").getValue();
            xml.push('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">');
            xml.push('<entity name="account">');
            xml.push('<attribute name="name" />');
            xml.push('<attribute name="telephone1" />');
            xml.push('<attribute name="accountid" />');
            xml.push('<filter type="and">');
            xml.push('<condition attribute="bsd_taxregistration" operator="eq" value="' + bsd_taxregistration + '" />');
            var id = Xrm.Page.data.entity.getId();
            if (id) {
                xml.push('<condition attribute="accountid" operator="ne" uitype="account" value="' + id + '" />');
            }
            xml.push('</filter>');
            xml.push('</entity>');
            xml.push('</fetch>');
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    Xrm.Page.getControl("bsd_taxregistration").setNotification("Tax Registration is exist!!!");
                } else {
                    Xrm.Page.getControl("bsd_taxregistration").clearNotification();
                }
            }, function () { });
        }
    }
}
//Author:Mr.Phong
//Description:check same tax registration for each account
function check_sametaxregistrationonsave() {
    var taxregistration = Xrm.Page.getAttribute("bsd_taxregistration").getValue();
    var name = Xrm.Page.getAttribute("name").getValue();
    var noidentify = Xrm.Page.getAttribute("bsd_noidentify").getValue();
    var xml = [];
    if (noidentify == "1") {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='account'>");
        xml.push("<attribute name='name' />");
        xml.push("<attribute name='primarycontactid' />");
        xml.push("<attribute name='telephone1' />");
        xml.push("<attribute name='accountid' />");
        xml.push("<order attribute='name' descending='false' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='bsd_taxregistration' operator='eq'   value='" + taxregistration + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length == 0) {
                Xrm.Page.getAttribute("bsd_noidentify").setValue("0");
                window.top.$ui.Confirm("Xác nhận", "Bạn có muốn tạo HDNT không ?", function (e) {
                    var windowOptions = {
                        openInNewWindow: true
                    };
                    var parameters = {};
                    parameters["regarding_Id"] = Xrm.Page.data.entity.getId();
                    Xrm.Utility.openEntityForm("bsd_principalcontract", null, parameters, windowOptions);
                }, null);
            }
        }, function (er) {
            console.log(er.message)
        });

    }
}
//author:Mr.Phong
function hide_button() {
    window.parent.document.getElementById("Sgrid_Principalcontract_contextualButtonsContainer").style.display = "none";
    window.parent.document.getElementById("Sgrid_Principalcontract_contextualButtonsContainer").style.display = "none";
}
//Author:Mr.Phong
//Description:set account type = customer when formtype =1
function set_accounttypecustomer() {
    if (Xrm.Page.ui.getFormType() == 1) {
        Xrm.Page.getAttribute("bsd_accounttype").setValue("861450000");
    }
}
//Author:Mr.Phong
//Description:filter field price list when create
function filterfieldpricelistwhencreate() {
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var a = [];
    a = get_value(["bsd_pricelist"]);
    var filedpricelist = Xrm.Page.getAttribute("bsd_pricelist");
    if (Xrm.Page.ui.getFormType() == 1) {
        var entityName = "pricelevel";
        var viewDisplayName = "Test";
        var viewId = "{EE499892-35E1-4D07-A1B7-BB69FE9BFD42}";
        fetch(xml, "pricelevel", ["name", "transactioncurrencyid", "enddate", "begindate", "statecode", "pricelevelid"], "name", false, null,
            ["bsd_pricelisttype", "bsd_account"], ["eq", "eq"], [0, "861450000", 1, "{A30A306A-EF94-E625-86CC-000C594C7A3D}"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='pricelevelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                "<row name='result'  " + "id='pricelevelid'>  " +
                                "<cell name='name'   " + "width='200' />  " +
                                 "<cell name='transactioncurrencyid'   " + "width='100' />  " +
                                  "<cell name='enddate'   " + "width='100' />  " +
                                    "<cell name='begindate'   " + "width='100' />  " +
                                  "<cell name='statecode'   " + "width='100' />  " +
                                "</row>   " +
                             "</grid>   ";
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length == 0) {
                Xrm.Page.getControl("bsd_pricelist").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    if (Xrm.Page.ui.getFormType() == 2) {
        var id = Xrm.Page.data.entity.getId();
        var entityName1 = "pricelevel";
        var viewDisplayName1 = "Test";
        var viewId1 = "{EE499892-35E1-4D07-A1B7-BB69FE9BFD42}";
        fetch(xml1, "pricelevel", ["name", "transactioncurrencyid", "enddate", "begindate", "statecode", "pricelevelid"], "createdon", true, null,
            ["bsd_pricelisttype", "bsd_account"], ["eq", "eq"], [0, "861450000", 1, id]);
        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='pricelevelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                "<row name='result'  " + "id='pricelevelid'>  " +
                                "<cell name='name'   " + "width='200' />  " +
                                 "<cell name='transactioncurrencyid'   " + "width='100' />  " +
                                  "<cell name='enddate'   " + "width='100' />  " +
                                    "<cell name='begindate'   " + "width='100' />  " +
                                  "<cell name='statecode'   " + "width='100' />  " +
                                "</row>   " +
                             "</grid>   ";
        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
            if (rs1.length > 0) {
                if (a[0] == null) {
                    filedpricelist.setValue([{
                        id: rs1[0].Id,
                        name: rs1[0].attributes.name.value,
                        entityType: rs1[0].logicalName
                    }]);
                    Xrm.Page.getControl("bsd_pricelist").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                }
                else {
                    Xrm.Page.getControl("bsd_pricelist").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                }
            }
            if (rs1.length == 0) {
                var entityName2 = "pricelevel";
                var viewDisplayName2 = "Test";
                var viewId2 = "{EE499892-35E1-4D07-A1B7-BB69FE9BFD42}";
                fetch(xml2, "pricelevel", ["name", "transactioncurrencyid", "enddate", "begindate", "statecode", "pricelevelid"], "name", false, null,
                    ["bsd_pricelisttype", "bsd_account"], ["eq", "eq"], [0, "861450000", 1, "{A30A306A-EF94-E625-86CC-000C594C7A3D}"]);
                var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='pricelevelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                        "<row name='result'  " + "id='pricelevelid'>  " +
                                        "<cell name='name'   " + "width='200' />  " +
                                         "<cell name='transactioncurrencyid'   " + "width='100' />  " +
                                          "<cell name='enddate'   " + "width='100' />  " +
                                            "<cell name='begindate'   " + "width='100' />  " +
                                          "<cell name='statecode'   " + "width='100' />  " +
                                        "</row>   " +
                                     "</grid>   ";
                Xrm.Page.getControl("bsd_pricelist").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.Phong
//Description:set value filed price group
function setvaluefilepricegroup() {
    var id = Xrm.Page.data.entity.getId();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var a = [];
    var filedpricegroup = Xrm.Page.getAttribute("bsd_pricegroup");
    Xrm.Page.getControl("bsd_pricegroup").removePreSearch(presearch_pricegroup);
    if (id != "") {       
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
        xml.push("<entity name='bsd_pricegroupaccount'>");
        xml.push("<attribute name='bsd_pricegroupaccountid'/>");
        xml.push("<attribute name='bsd_name' />");
        xml.push("<attribute name='createdon' />");
        xml.push("<attribute name='bsd_pricegroup' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='bsd_account' operator='eq' value='" + id + "' />");
        xml.push("</filter>");    
        xml.push("</entity>");
        xml.push("</fetch>");        
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {            
                for (var i = 0; i < rs.length; i++) {
                    a.push(rs[i].attributes.bsd_pricegroup.guid);
                }
                if (a.length==1) {
                    var viewId = "{A838D686-50C6-4723-96B6-6824108A946A}";
                    var entityName = "bsd_pricegroups";
                    var viewDisplayName = "test";
                    xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                    xml3.push("<entity name='bsd_pricegroups'>");
                    xml3.push("<attribute name='bsd_pricegroupsid'/>");
                    xml3.push("<attribute name='bsd_name'/>");
                    xml3.push("<attribute name='createdon'/>");
                    xml3.push("<order attribute='bsd_name' descending='false' />");
                    xml3.push("<filter type='and'>");
                    xml3.push("<condition attribute='bsd_pricegroupsid' operator='eq' value='" + '{' + rs[0].attributes.bsd_pricegroup.guid + '}' + "'/>");
                    xml3.push("</filter>");
                    xml3.push("</entity>");
                    xml3.push("</fetch>");
                    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_pricegroupsid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_pricegroupsid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                             "<cell name='bsd_pricegroup'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                    Xrm.Page.getControl("bsd_pricegroup").addCustomView(viewId, entityName, viewDisplayName, xml3.join(""), layoutXml, true);
                }
                if (a.length > 1)
                {
                    var viewId = "{A838D686-50C6-4723-96B6-6824108A946A}";
                    var entityName = "bsd_pricegroups";
                    var viewDisplayName = "test";
                    xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>");
                    xml3.push("<entity name='bsd_pricegroups'>");
                    xml3.push("<attribute name='bsd_pricegroupsid'/>");
                    xml3.push("<attribute name='bsd_name'/>");
                    xml3.push("<attribute name='createdon'/>");                
                    xml3.push("<order attribute='bsd_name' descending='false' />");
                    xml3.push("<filter type='and'>");                   
                    xml3.push("<condition attribute='bsd_pricegroupsid' operator='in'>");
                    for (k = 0; k < a.length; k++) {
                        xml3.push("<value uitype='bsd_pricegroups'>" + '{' + a[k] + '}' + "");
                        xml3.push("</value>");
                    }
                    xml3.push("</condition>");
                    xml3.push("</filter>");
                    xml3.push("</entity>");
                    xml3.push("</fetch>");
                    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_pricegroupsid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                          "<row name='result'  " + "id='bsd_pricegroupsid'>  " +
                          "<cell name='bsd_name'   " + "width='200' />  " +
                          "<cell name='createdon'    " + "width='100' />  " +
                          "<cell name='bsd_pricegroup'    " + "width='100' />  " +
                          "</row>   " +
                       "</grid>   ";
                    Xrm.Page.getControl("bsd_pricegroup").addCustomView(viewId, entityName, viewDisplayName, xml3.join(""), layoutXml, true);
                }
               
            }
            else {            
                clear_pricegroup();
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
function clear_pricegroup() {
    Xrm.Page.getControl("bsd_pricegroup").addPreSearch(presearch_pricegroup);
}
function presearch_pricegroup() {
    Xrm.Page.getControl("bsd_pricegroup").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//Author:Mr.Đăng
//Description: Check text
