//huy
function init() {
    showorhidesubgrid();
}
//Author:Mr.Đăng
//Description:Check Tax Group
function Check_salestaxgroup() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var salestaxgroup = getValue("bsd_salestaxgroup");
    var id = getId();
    if (salestaxgroup != null) {
        if (mikExp.test(salestaxgroup)) {
            setNotification("bsd_salestaxgroup", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_saletaxgroup">',
                            '<attribute name="bsd_saletaxgroupid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_name" operator="eq" value="' + salestaxgroup + '" />',
                            '<condition attribute="bsd_saletaxgroupid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_salestaxgroup", "Mã đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_salestaxgroup");
                setValue("bsd_salestaxgroup", salestaxgroup.toUpperCase());
                setValue("bsd_name", salestaxgroup.toUpperCase());
            }
        }
    }
    else {
        setNull("bsd_name");
    }
}
//Author:Mr.Phong
//Description:show or hide subgrid
function showorhidesubgrid()
{
    var type = getValue("bsd_type");
    if (type == 861450000)
    {
        Xrm.Page.ui.tabs.get("{3d4d7c4d-fedb-4b0e-a45c-01ade2f989f7}").sections.get("{3d4d7c4d-fedb-4b0e-a45c-01ade2f989f7}_section_2").setVisible(false);
    }
    else {
        Xrm.Page.ui.tabs.get("{3d4d7c4d-fedb-4b0e-a45c-01ade2f989f7}").sections.get("{3d4d7c4d-fedb-4b0e-a45c-01ade2f989f7}_section_2").setVisible(true);
    }
}