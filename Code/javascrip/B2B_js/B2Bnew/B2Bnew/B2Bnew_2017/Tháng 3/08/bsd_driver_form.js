﻿function onload()
{
    var xml = [];
    var viewId = "{BE3C94C9-0424-4389-9651-13ED88B9C15B}";
    var entityName = "bsd_deliverytruck";
    var viewDisplayName = "test";
    fetch(xml, "bsd_deliverytruck", ["bsd_deliverytruckid", "bsd_name", "createdon"], "bsd_name", false, null
           , ["bsd_companytype"], ["eq"], [0, "861450000", 1]);
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_deliverytruckid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                          "<row name='result'  " + "id='bsd_deliverytruckid'>  " +
                          "<cell name='bsd_name'   " + "width='200' />  " +
                          "<cell name='createdon'    " + "width='100' />  " +
                          "</row>   " +
                       "</grid>   ";
    Xrm.Page.getControl("bsd_deliverytruck").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
}
//Author:Mr.Đăng
//Description:Check Driver ID & UpperCase
function Check_DriverID() {
    var codedriver = getValue("bsd_codedriver");
    var id = getId();
    if (codedriver != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_driver">',
                        '<attribute name="bsd_driverid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_codedriver" operator="eq" value="' + codedriver + '" />',
                        '<condition attribute="bsd_driverid" operator="ne" value="' + id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setNotification("bsd_codedriver", "Mã đã tồn tại, vui lòng kiểm tra lại.");
        } else {
            clearNotification("bsd_codedriver");
            setValue("bsd_codedriver", codedriver.toUpperCase());
        }
    }
}