//author:Mr.Phong
//description:set district theo province
function set_district() {
    var city = Xrm.Page.getAttribute("new_province").getValue();
    var districdefault = Xrm.Page.getAttribute("bsd_district");
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
    if (city != null) {
        var viewId = "{5F60D1E4-F7DF-476A-88B9-AC0EDE10E321}";
        var entityName = "bsd_district";
        var viewDisplayName = "test";
        var xml = [];
        fetch(xml, "bsd_district", ["bsd_districtid", "bsd_name", "createdon"], ["bsd_name"], false, null
        , ["bsd_province", "statecode"], ["eq", "eq"], [0, city[0].id, 1, "0"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_districtid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_districtid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_district").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        districdefault.setValue(null);
    }
    else {
        Xrm.Page.getAttribute("bsd_district").setValue(null);
        clear_district();
    }
}
function clear_district() {
    Xrm.Page.getControl("bsd_district").addPreSearch(presearch_district);
}
function presearch_district() {
    Xrm.Page.getControl("bsd_district").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//author:Mr.Phong
function validation_district() {
    var project = Xrm.Page.getAttribute("new_province").getValue();
    if (project == null) {
        Xrm.Page.getAttribute("bsd_district").setValue(null);
    }
}
//author:Mr.Phong
//description:set district theo province
function set_districtonload() {
    var city = Xrm.Page.getAttribute("new_province").getValue();
    Xrm.Page.getControl("bsd_district").removePreSearch(presearch_district);
    var districdefault = Xrm.Page.getAttribute("bsd_district");
    if (city != null) {
        var viewId = "{5F60D1E4-F7DF-476A-88B9-AC0EDE10E321}";
        var entityName = "bsd_district";
        var viewDisplayName = "test";
        var xml = [];
        fetch(xml, "bsd_district", ["bsd_districtid", "bsd_name", "createdon"], ["bsd_name"], false, null
     , ["bsd_province", "statecode"], ["eq", "eq"], [0, city[0].id, 1, "0"]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_districtid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_districtid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_district").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);

    }
    else {
        Xrm.Page.getAttribute("bsd_district").setValue(null);
        clear_district();
    }
}
//author:Mr.Phong
//description:load province
function loadprovince() {
    var provinceFilter = "<filter type='and'><condition attribute='statecode' operator='eq' value='0'/></filter>";
    Xrm.Page.getControl("new_province").addCustomFilter(provinceFilter, "bsd_province");
}
//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    set_disableWard();
}
//Author:Mr.Đăng
//Description: Disable Ward
function set_disableWard() {
    if (formType() == 2) {
        setDisabled("bsd_id", true);
    }
    else {
        setDisabled("bsd_id", false);
    }
}
//Author:Mr.Đăng
//Description:Check Wardid & UpperCase
function Check_WardID() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var id = getValue("bsd_id");
    if (id != null) {
        if (mikExp.test(id)) {
            setNotification("bsd_id", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_ward">',
                        '<attribute name="bsd_wardid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_id" operator="eq" value="' + id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_id", "Mã phường/xã đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_id");
                setValue("bsd_id", id.toUpperCase());
            }
        }
    }
}
//author:Mr.Phong
//description: Kiểm tra không cho phép nhập kí tự đặc biệt
function Check_id() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var name = getValue("bsd_name");
    if (name != null) {
        if (mikExp.test(name)) {
            setNotification("bsd_name", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_name");
        }
    }
}