//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    set_disableDistrict();
}
//Author:Mr.Đăng
//Description: Disable District
function set_disableDistrict() {
    if (formType() == 2) {
        setDisabled("bsd_id", true);
    }
    else {
        setDisabled("bsd_id", false);
    }
}
//Author:Mr.Đăng
//Description:Check DistrictID
function Check_DistrictID() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var id = getValue("bsd_id");
    if (id != null) {
        if (mikExp.test(id)) {
            setNotification("bsd_id", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="bsd_district">',
                        '    <attribute name="bsd_districtid" />',
                        '    <attribute name="bsd_name" />',
                        '    <order attribute="bsd_name" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="bsd_id" operator="eq" value="' + id + '" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_id", "Mã quận/huyện đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_id");
                setValue("bsd_id", id.toUpperCase());
            }
        }
    }
}
//author:Mr.Phong
//description: Kiểm tra không cho phép nhập kí tự đặc biệt
function Check_id() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var name = getValue("bsd_name");
    if (name != null) {
        if (mikExp.test(name)) {
            setNotification("bsd_name", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_name");
        }
    }
}