﻿// 23.03.2017 Đăng: Autoload
function Autoload() {
    setDisabled_Account();
}
//Author:Mr.Phong
//Description:get all account type customer
function set_customer() {
    var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    var entityName = "account";
    var viewDisplayName = "test";
    var xml = [];
    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    xml.push("<entity name='account'>");
    xml.push("<attribute name='accountid' />");
    xml.push("<attribute name='name'/>");
    xml.push("<order attribute='name' descending='false' />");
    xml.push("<filter type='and' >");
    xml.push("<condition attribute='bsd_accounttype' operator='eq' value='861450000' />");
    xml.push("</filter>");
    xml.push("</entity>");
    xml.push("</fetch>");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='accountid'>  " +
                       "<cell name='name'    " + "width='200' />  " +
                       "</row>   " +
                    "</grid>   ";
    Xrm.Page.getControl("bsd_account").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
}
//Author:Mr.Phong
//Description:filter product not in account
function filter_productonaccount()
{
    var account = Xrm.Page.getAttribute("bsd_account").getValue();
    var product = Xrm.Page.getAttribute("bsd_product").getValue();
    var xml = [];
    var xml1 = [];
    var a = [];
    if (account != null && product==null)
    {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_customerrequired'>");
        xml.push("<attribute name='bsd_customerrequiredid' />");
        xml.push("<attribute name='bsd_name'/>");
        xml.push("<attribute name='createdon'/>");
        xml.push("<attribute name='bsd_product'/>");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_account' operator='eq' uitype='account' value='"+account[0].id+"' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0 && rs[0].attributes.bsd_product != null)
            {
                for (var i = 0; i < rs.length; i++)
                {
                    a.push(rs[i].attributes.bsd_product.guid);
                }
                var viewId = "{8BA625B2-6A2A-4735-BAB2-0C74AE8442A4}";
                var entityName = "product";
                var viewDisplayName = "test";
                xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml1.push("<entity name='product'>");
                xml1.push("<attribute name='name' />");
                xml1.push("<attribute name='productnumber'/>");
                xml1.push("<attribute name='description'/>");
                xml1.push("<attribute name='statecode'/>");
                xml1.push("<attribute name='productstructure'/>");
                xml1.push("<order attribute='productnumber' descending='false' />");
                xml1.push("<filter type='and' >");
                xml1.push("<condition attribute='productid' operator='not-in'>");
                for (var k = 0; k < a.length; k++) {
                    xml1.push("<value uitype='product'>" + '{' + a[k] + '}' + "");
                    xml1.push("</value>");
                }
                xml1.push("</condition>");
                xml1.push("</filter>");
                xml1.push("</entity>");
                xml1.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='name'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='name'>  " +
                     "<cell name='productstructure'    " + "width='200' />  " +
                      "<cell name='description'    " + "width='100' />  " +
                       "<cell name='statecode'    " + "width='100' />  " +
                        "<cell name='productnumber'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
                Xrm.Page.getControl("bsd_product").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);

            }
        }, function (er) {
            console.log(er.message)
        });
    }
    else {
        Xrm.Page.getAttribute("bsd_product").setValue(null);
    }
}
//Author:Mr.Phong
//Description:set value field name
function set_filedname()
{
    var account = Xrm.Page.getAttribute("bsd_account").getValue();
    var product = Xrm.Page.getAttribute("bsd_product").getValue();
    if (account!=null && product!=null) {
        Xrm.Page.getAttribute("bsd_name").setValue(product[0].name);
    }
}
// 23.03.2017 Đăng: setDisabled Account
function setDisabled_Account() {
    var account = getValue("bsd_account");
    if (account != null) {
        setDisabled("bsd_account", true);
    }
}