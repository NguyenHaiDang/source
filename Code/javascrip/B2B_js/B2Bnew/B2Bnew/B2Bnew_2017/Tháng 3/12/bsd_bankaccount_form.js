////author:Mr.Phong
////description:set name
//function set_name()
//{
//    var account = Xrm.Page.getAttribute("bsd_account").getValue();
//    var bankgroup = Xrm.Page.getAttribute("bsd_bankgroup").getValue();
//    var brand = Xrm.Page.getAttribute("bsd_brand").getValue();
//    var bankaccount = Xrm.Page.getAttribute("bsd_bankaccount").getValue();
//    if (account != null && bankgroup != null && brand != null && bankaccount != null)
//    {
//        Xrm.Page.getAttribute("bsd_name").setValue(account[0].name + "-" + bankgroup[0].name + "-" + brand + "-" + bankaccount);
//    }
//}

////author:Mr.Đăng
////description: load name
//function load_name() {
//    var bankid = getValue("bsd_bankid");
//    if (bankid != null) {
//        setValue("bsd_name", bankid);
//    }
//    else {
//        setNull("bsd_name");
//    }
//}
////author:Mr.Đăng
////description: check name
//function Check_name() {
//    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?|]/;
//    var name = getValue("bsd_name");
//    if (name != null) {
//        if (mikExp.test(name)) {
//            setNotification("bsd_name", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
//        }
//        else {
//            clearNotification("bsd_name");
//        }
//    }
//}
//author:Mr.Đăng
//description: check bank account ID
function Check_BankID() {
    debugger;
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var bankid = getValue("bsd_bankid");
    var account = getValue("bsd_account");
    var bankgroup = getValue("bsd_bankgroup");
    var id = getId();
    if (bankid != null) {
        if (mikExp.test(bankid)) {
            setNotification("bsd_bankid", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            if (account != null && bankgroup != null) {
                var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                '<entity name="bsd_bankaccount">',
                                '<attribute name="bsd_name" />',
                                '<attribute name="bsd_bankaccountid" />',
                                '<order attribute="bsd_name" descending="false" />',
                                '<filter type="and">',
                                '<condition attribute="bsd_bankid" operator="eq" value="'+ bankid +'" />',
                                '<condition attribute="bsd_account" operator="eq" uitype="account" value="'+ account[0].id +'" />',
                                '<condition attribute="bsd_bankgroup" operator="eq" uitype="bsd_bankgroup" value="' + bankgroup[0].id + '" />',
                                '<condition attribute="bsd_bankaccountid" operator="ne" value="' + id + '" />',
                                '</filter>',
                                '</entity>',
                                '</fetch>'].join("");
                var rs = CrmFetchKit.FetchSync(fetchxml);
                if (rs.length > 0) {
                    setNotification("bsd_bankid", "Mã số tài khoản đã tồn tại, vui lòng kiểm tra lại.");
                }
                else {
                    clearNotification("bsd_bankid");
                    setValue("bsd_bankid", bankid.toUpperCase());
                    setValue("bsd_name", bankid.toUpperCase());
                }
            }
        }
    }
    else if (bankid == null)
    {
        setNull("bsd_name");
    }
}
//author:Mr.Đăng
//description: check brand
function Check_brand() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?|]/;
    var number = getValue("bsd_brand");
    if (number != null) {
        if (mikExp.test(number)) {
            setNotification("bsd_brand", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_brand");
        }
    }
}
//author:Mr.Đăng
//description: check bank account vs duplicate Accountnumber
function Check_bankaccount() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var account = getValue("bsd_account");
    var bankgroup = getValue("bsd_bankgroup");
    var bankaccount = getValue("bsd_bankaccount");
    var id = getId();
    if (bankaccount != null) {
        if (mikExp.test(bankaccount)) {
            setNotification("bsd_bankaccount", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_bankaccount");
            if (account != null && bankgroup != null) {
                var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                '<entity name="bsd_bankaccount">',
                                '<attribute name="bsd_name" />',
                                '<attribute name="bsd_bankaccountid" />',
                                '<order attribute="bsd_name" descending="false" />',
                                '<filter type="and">',
                                '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + account[0].id + '" />',
                                '<condition attribute="bsd_bankgroup" operator="eq" uitype="bsd_bankgroup" value="' + bankgroup[0].id + '" />',
                                '<condition attribute="bsd_bankaccount" operator="eq" value="' + bankaccount + '" />',
                                '<condition attribute="bsd_bankaccountid" operator="ne" value="' + id + '" />',
                                '</filter>',
                                '</entity>',
                                '</fetch>'].join("");
                var rs = CrmFetchKit.FetchSync(fetchxml);
                if (rs.length > 0) {
                    setNotification("bsd_bankaccount", "Mã số tài khoản đã tồn tại, vui lòng kiểm tra lại.");
                }
                else {
                    clearNotification("bsd_bankaccount");
                }
            }
        }
    }
}