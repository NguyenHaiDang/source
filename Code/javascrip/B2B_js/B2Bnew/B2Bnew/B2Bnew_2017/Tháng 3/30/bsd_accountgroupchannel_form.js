﻿// 30.03.2017 Mr.Đăng : Không cho chọn trùng Channel
function change_fieldchannel() {
    var id = getId();
    var chanel = getValue("bsd_channel");
    var accountgroup = getValue("bsd_accountgroup");
    if (chanel != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_accountgroupchannel">',
                        '<attribute name="bsd_accountgroupchannelid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_channel" operator="eq" uitype="bsd_industriallevel1" value="' + chanel[0].id + '" />',
                        '<condition attribute="bsd_accountgroup" operator="eq" uitype="bsd_accountgroup" value="' + accountgroup[0].id + '" />',
                        '<condition attribute="bsd_accountgroupchannelid" operator="ne" value="' + id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs1) {
            if (rs1.length > 0) {
                alert("This record is exist!!!");
                setNull("bsd_channel", "bsd_name");
            }
            else {
                setValue("bsd_name", chanel[0].name);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    else {
        setValue("bsd_name", null);
    }
}