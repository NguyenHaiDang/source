function init() {
    var numberprocess = getValue("bsd_numberprocess");
    var numberprocessctq = getValue("bsd_numberprocessctq");
    if (numberprocess == null && numberprocessctq == null) {
        setValue("bsd_numberprocessctq", false);
        setValue("bsd_numberprocess", false);
    }
    hide_button();
    setTimeout("LoadSubGrids();", 2000);
    set_currentdate();
    setaccounttypecustomer();
    setshiptoaddressonload();
    set_addressonload();
    set_DisableDeliveryPort();
    setvaluefieldexchangerateonload();
    appearordisappearsectionBHSonload();
    filter_customerprincipalcontract();
    changestatuswhenstagefinalonload();
    switchProcess();
    switchProcessctq();
    SetRequire_EconomicContract();
    setdisableallfield();
    if (getValue("bsd_type") == 861450000 || getValue("bsd_type") == 861450003 || getValue("bsd_type") == 861450001 || getValue("bsd_type") == 861450002) {
        if (formType() == 1) {
            clear_contact_invoice();
            setValue("bsd_createfrom", 861450003);
            load_unitdefault(true);
            //load_currencydefault(true);
            check_enable_shipping();
        } else {
           
            load_economiccontract();
            invoice_name_account_change(false);
            check_enable_shipping(false);
            porteroption_change(false);
            load_unitdefault(false);
            //load_currencydefault(false);
            check_from_quote();
            check_createdsuborder();
        }
    }
}

function LoadSubGrids() {
    var grids = Xrm.Page.ui.controls.get(function (ctrl, index) {
        return (ctrl.getControlType() == "subgrid");
    });

    if (grids.length > 0) {
        for (var i in grids) {
            grids[i].refresh();
        }
    }
}


function check_from_quote() {
    if (getValue("quoteid") != null) {
        setDisabled(["customerid", "bsd_address", "bsd_invoicenameaccount", "bsd_addressinvoiceaccount", "bsd_shiptoaccount", "bsd_fromdate", "bsd_todate"], true);
        setDisabled(["bsd_warehouseto", "bsd_warehouseaddress", "bsd_shiptoaddress", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_unitshipping"], true);
        setDisabled(["bsd_partialshipping", "bsd_requestporter", "bsd_transportation", "bsd_shippingdeliverymethod", "bsd_truckload", "bsd_porter", "bsd_priceofporter", "bsd_shippingporter"], true);
        setDisabled(["bsd_paymentterm", "bsd_paymentmethod", "bsd_porteroption", "bsd_pricepotter","bsd_unitdefault"], true);
        setDisabled(["bsd_deliveryattheport", "shipto_freighttermscode"], true);
    }
}
function load_economiccontract() {
    var bsd_economiccontract = getValue("bsd_economiccontract");
    if (bsd_economiccontract != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_economiccontract">',
            '<attribute name="bsd_economiccontractid" />',
            '<attribute name="bsd_code" />',
           '<order attribute="bsd_code" descending="false" />',
            '<filter type="and">',
              '<condition attribute="bsd_economiccontractid" operator="eq" uitype="bsd_economiccontract" value="' + bsd_economiccontract[0].id + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                setValue("bsd_economiccontract", [{
                    id: bsd_economiccontract[0].id,
                    name: rs[0].getValue("bsd_code"),
                    entityType: "bsd_economiccontract",
                }]);
            }
        });
    }
}

function check_createdsuborder() {
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="bsd_suborder">',
        '<attribute name="bsd_suborderid" />',
        '<attribute name="bsd_name" />',
        '<filter type="and">',
          '<condition attribute="bsd_order" operator="eq" uiname="2017 huy test 4" uitype="salesorder" value="' + getId() + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            DisabledForm();
            Xrm.Page.ui.setFormNotification("Đơn hàng đã tạo Suborder, không thể chỉnh sửa. ", "INFO", "1");
        }
    });
}

//huy
function check_enable_shipping(reset) {
    // call when customername or warehouse or address change.
    var customername = getValue("customerid");
    var warehousefrom = getValue("bsd_warehouseto");
    var warehouseaddress = getValue("bsd_warehouseaddress");
    var shiptoaddress = getValue("bsd_shiptoaddress");
    var effectivefrom = getValue("bsd_fromdate");
    var effectiveto = getValue("bsd_todate");
    if (customername != null && warehousefrom != null && shiptoaddress != null && warehouseaddress != null && effectivefrom != null && effectiveto != null) {
        setDisabled("bsd_transportation", false);
    } else {
        setValue("bsd_transportation", false);
        setDisabled("bsd_transportation", true);
    }
    shipping_change(reset);
}

function shipping_change(reset) {
    clearNotification("bsd_shippingpricelistname");
    if (reset != false) {
        setNull(["bsd_truckload", "bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
    }

    var bsd_transportation = getValue("bsd_transportation");
    if (bsd_transportation) { // có ship
        setRequired(["bsd_shiptoaccount", "bsd_shiptoaddress"], "required");
        setVisible(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], true);
        setRequired(["bsd_shippingdeliverymethod", "bsd_shippingpricelistname", "bsd_priceoftransportationn"], "required");
        shipping_deliverymethod_change(reset);
    } else {
        setRequired(["bsd_shiptoaccount", "bsd_shiptoaddress"], "none");
        setVisible(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], false);
        setRequired(["bsd_shippingdeliverymethod", "bsd_truckload", "bsd_unitshipping", "bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"], "none");

        if (reset != false) {
            setValue("bsd_shippingporter", false);
            porteroption_change(reset);
        }
    }
}

function requestporter_change(reset) {
    shipping_change(reset);
}

function shipping_deliverymethod_change(reset) {

    if (reset != false) setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn", "bsd_shippingporter"]);
    var method;
    if (getValue("bsd_shippingdeliverymethod") == null) {
        setValue("bsd_shippingdeliverymethod", 861450000);
        method = 861450000;
    } else {
        method = getValue("bsd_shippingdeliverymethod");
    }

    if (method == 861450000) { // ton
        setVisible("bsd_unitshipping", true);
        setRequired("bsd_unitshipping", "required");
        if (getValue("bsd_unitshipping") == null) {
            setValue("bsd_unitshipping", [{
                id: "{886009E0-0095-E611-80CC-000C294C7A2D}",
                name: "Tấn",
                entityType: "uom"
            }]);
        }


        setVisible("bsd_truckload", false);
        setNull("bsd_truckload");
        setRequired("bsd_truckload", "none");

        load_shippingpricelist_ton(reset);

    } else if (861450001) { // trip

        setVisible("bsd_unitshipping", false);
        setNull("bsd_unitshipping");
        setRequired("bsd_unitshipping", "none");

        setVisible("bsd_truckload", true);
        setRequired("bsd_truckload", "required");

        if (reset != false) {
            setNull("bsd_truckload");
        }
        load_truckload(reset);
        truckload_change(reset);
    }
}

function shipping_pricelist_change(reset) {
    // code
    if (reset != false) setNull("bsd_priceoftransportationn");
    var pricelist = getValue("bsd_shippingpricelistname");
    if (pricelist != null) {

    } else if (reset != false) {

    }
    shipping_price_change();
}

function set_shipping_pricelist(data) {
    if (data != null) {
        setValue("bsd_shippingpricelistname", [{
            id: data.pricelist_id,
            name: data.pricelist_name,
            entityType: "bsd_shippingpricelist"
        }]);
        setValue("bsd_priceoftransportationn", data.price);
        if (data.porter == true) {
            setValue("bsd_shippingporter", true);
            setValue("bsd_porteroption", true);
        } else {
            setValue("bsd_shippingporter", false);
        }
        if (getValue("bsd_shippingpricelistname") == null) {
            setTimeout(function () {
                set_shipping_pricelist(data);
            }, 50);
        } else {
            porteroption_change(true);
        }
    } else {
        setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
        porteroption_change(true);
    }
}

function shipping_price_change() {
    // validate price
    var price = getValue("bsd_priceoftransportationn");
    if (price == null) {
        //setNotification("bsd_priceoftransportationn", "You must provide a value for Price");
    } else if (price <= 0) {
        setNotification("bsd_priceoftransportationn", "Enter a value from 0");
    } else {
        clearNotification("bsd_priceoftransportationn");
    }
}

function load_warehouse_address(reset) {

    if (reset != false) setNull("bsd_warehouseaddress");

    var warehouse = getValue("bsd_warehouseto");
    if (warehouse != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="bsd_warehouseentity">',
        '<attribute name="bsd_warehouseentityid" />',
        '<attribute name="bsd_address" />',
        '<filter type="and">',
          '<condition attribute="bsd_warehouseentityid" operator="eq" uitype="bsd_warehouseentity" value="' + warehouse[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && reset != false) {
                var first = rs[0];
                console.log(first);
                setValue("bsd_warehouseaddress", [{
                    id: first.attributes.bsd_address.guid,
                    name: first.attributes.bsd_address.name,
                    entityType: first.attributes.bsd_address.logicalName
                }]);
            }
        });
    }
    check_enable_shipping(reset);
}

function porteroption_change(reset) {
    //if (reset != false) setNull(["bsd_priceofporter", "bsd_pricepotter"]);
    var request_porter = getValue("bsd_requestporter");
    var porteroption = getValue("bsd_porteroption");
    var shipping_porter = getValue("bsd_shippingporter");
    if (request_porter == false) {
        // Không yêu cầu !
        setDisabled(["bsd_porteroption"], true);

        setVisible(["bsd_priceofporter", "bsd_pricepotter"], false);
        setRequired(["bsd_priceofporter", "bsd_pricepotter"], "none");
        if (reset != false) {
            setValue("bsd_porteroption", false);
            setValue("bsd_porter", 861450001);
            setNull(["bsd_priceofporter", "bsd_pricepotter"]);
        }

    } else if (request_porter == true) { // Có yêu cầu
        if (shipping_porter == true) { // Giá đã gồm Porter
            setDisabled(["bsd_porteroption"], true);
            setVisible(["bsd_priceofporter", "bsd_pricepotter"], false);
            setRequired(["bsd_priceofporter", "bsd_pricepotter"], "none");
            if (reset != false) {
                setValue("bsd_porteroption", true);
                setValue("bsd_porter", 861450000);
                setNull(["bsd_priceofporter", "bsd_pricepotter"]);
            }
        } else { // Không có giá  porter
            setDisabled(["bsd_porteroption"], true);
            setVisible(["bsd_priceofporter", "bsd_pricepotter"], true);
            setRequired(["bsd_priceofporter", "bsd_pricepotter"], "required");
            if (reset != false) {
                setValue("bsd_porteroption", true);
                setValue("bsd_porter", 861450000);
            }
            load_porter();
            porterprice_change(reset);
        }
    }
}

function porterprice_change(reset) {
    if (reset != false) {
        var bsd_priceofporter = getValue("bsd_priceofporter");
        if (bsd_priceofporter != null) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_porter">',
                '<attribute name="bsd_porterid" />',
                '<attribute name="bsd_pricee" />',
                '<order attribute="bsd_pricee" descending="false" />',
                '<filter type="and">',
                  '<condition attribute="bsd_porterid" operator="eq" uitype="bsd_porter" value="' + bsd_priceofporter[0].id + '" />',
                  '<condition attribute="bsd_pricee" operator="not-null" />',
                '</filter>',
              '</entity>',
            '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    setValue("bsd_pricepotter", first.getValue("bsd_pricee"));
                }
            });
        } else {
            setNull("bsd_pricepotter");
        }
    }
}

function check_date() {
    // effective date
    check_enable_shipping(true);
    check_fromdate_todate();
}

function check_fromdate_todate() {
    clearNotification("bsd_fromdate");
    var from = getValue("bsd_fromdate");
    var to = getValue("bsd_todate");
    if (from != null && to != null) {
        if (from > to) {
            setNotification("bsd_fromdate", "The From Date cannot occur before the To Date");
        }
    }
}

function onsave(econtext) {
    var bsd_transportation = getValue("bsd_transportation");
    if (bsd_transportation == true) {
        var pricelist = getValue("bsd_shippingpricelistname");
        if (pricelist == null) {
            setNotification("bsd_shippingpricelistname", "You must provide a value for Price List !");
        }
    }
}

function getDate(d) {
    return new Date('' + d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear() + ' 12:00:00 AM');
}
// End Trung

//huy
function load_shippingpricelist_ton(reset) {
    if (reset != false) {
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var effective_from = getValue("bsd_fromdate");
        var effective_to = getValue("bsd_todate");

        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();
        var data = null;

        if (request_porter == true) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                       '   <entity name="bsd_shippingpricelist">',
                       '     <attribute name="bsd_shippingpricelistid" />',
                       '     <attribute name="bsd_name" />',
                       '     <attribute name="createdon" />',
                       '     <attribute name="bsd_priceunitporter" />',
                       '     <attribute name="bsd_priceofton" />',
                       '     <attribute name="bsd_deliverymethod" />',
                       '     <order attribute="bsd_priceunitporter" descending="true" />',
                       '     <order attribute="bsd_priceofton" descending="true" />',
                       '     <filter type="and">',
                       '       <condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                       '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                       '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                       '       <condition attribute="statecode" operator="eq" value="0" />',
                       '       <condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                       '     </filter>',
                       '     <link-entity name="bsd_zone" from="bsd_zoneid" to="bsd_zone" alias="ao">',
                       '       <link-entity name="bsd_zoneaddress" from="bsd_zone" to="bsd_zoneid" alias="ap">',
                       '         <filter type="and">',
                       '           <condition attribute="bsd_address" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
                       '         </filter>',
                       '       </link-entity>',
                       '     </link-entity>',
                       '   </entity>',
                       ' </fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    if (first.getValue("bsd_priceunitporter") != null) {
                        console.log(first);
                        //shipping price list có price unit porter
                        data = {
                            pricelist_id: first.getValue("bsd_shippingpricelistid"),
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_priceunitporter.value,
                            porter: true
                        };
                    } else {
                        data = {
                            pricelist_id: first.getValue("bsd_shippingpricelistid"),
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_priceofton.value,
                            porter: false
                        };
                    }
                }
            });
        } else if (request_porter == false) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                       '   <entity name="bsd_shippingpricelist">',
                       '     <attribute name="bsd_shippingpricelistid" />',
                       '     <attribute name="bsd_name" />',
                       '     <attribute name="createdon" />',
                       '     <attribute name="bsd_priceunitporter" />',
                       '     <attribute name="bsd_priceofton" />',
                       '     <attribute name="bsd_deliverymethod" />',
                       '     <order attribute="bsd_priceofton" descending="true" />',
                       '     <filter type="and">',
                       '       <condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                       '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                       '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                       '       <condition attribute="statecode" operator="eq" value="0" />',
                       '       <condition attribute="bsd_deliverymethod" operator="eq" value="861450000" />',
                       '     </filter>',
                       '     <link-entity name="bsd_zone" from="bsd_zoneid" to="bsd_zone" alias="ao">',
                       '       <link-entity name="bsd_zoneaddress" from="bsd_zone" to="bsd_zoneid" alias="ap">',
                       '         <filter type="and">',
                       '           <condition attribute="bsd_address" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
                       '         </filter>',
                       '       </link-entity>',
                       '     </link-entity>',
                       '   </entity>',
                       ' </fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    if (first.getValue("bsd_priceofton") != null) {
                        data = {
                            pricelist_id: first.Id,
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_priceofton.value,
                            porter: false
                        };
                    }
                }
            });

        }

        if (data != null) {
            set_shipping_pricelist(data);
            clearNotification("bsd_shippingpricelistname");
        } else {
            set_shipping_pricelist(null);
        }

    } else {
        porteroption_change(reset);
    }
}

//huy
function truckload_change(reset) {
    // load shipping theo trip
    if (reset != false) {
        setNull(["bsd_shippingpricelistname", "bsd_priceoftransportationn"]);
        setValue("bsd_shippingporter", false);
    }

    var truckload = getValue("bsd_truckload");
    if (truckload != null && reset != false) {
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var warehouseaddress = getValue("bsd_warehouseaddress");
        var customer = getValue("customerid");
        var shiptoaddress = getValue("bsd_shiptoaddress");
        var request_porter = getValue("bsd_requestporter");
        var effective_from = getValue("bsd_fromdate");
        var effective_to = getValue("bsd_todate");

        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();
        var data = null;

        if (request_porter == true) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                   '   <entity name="bsd_shippingpricelist">',
                   '     <attribute name="bsd_shippingpricelistid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_pricetripporter" />',
                   '     <attribute name="bsd_priceoftrip" />',
                   '     <attribute name="bsd_deliverymethod" />',
                   '     <order attribute="bsd_pricetripporter" descending="true" />',
                   '     <order attribute="bsd_priceoftrip" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                    '      <condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                    '    </filter>',
                    '    <link-entity name="bsd_zone" from="bsd_zoneid" to="bsd_zone" alias="ao">',
                    '      <link-entity name="bsd_zoneaddress" from="bsd_zone" to="bsd_zoneid" alias="ap">',
                    '        <filter type="and">',
                    '          <condition attribute="bsd_address" operator="eq" value="' + shiptoaddress[0].id + '" />',
                    '        </filter>',
                    '      </link-entity>',
                    '    </link-entity>',
                    '  </entity>',
                    '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    if (first.getValue("bsd_pricetripporter") != null) {
                        console.log(first);
                        //shipping price list có price unit porter
                        data = {
                            pricelist_id: first.getValue("bsd_shippingpricelistid"),
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_pricetripporter.value,
                            porter: true
                        };
                    } else {
                        data = {
                            pricelist_id: first.getValue("bsd_shippingpricelistid"),
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_priceoftrip.value,
                            porter: false
                        };
                    }
                }
            });
        } else if (request_porter == false) {
            var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                   '   <entity name="bsd_shippingpricelist">',
                   '     <attribute name="bsd_shippingpricelistid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_pricetripporter" />',
                   '     <attribute name="bsd_priceoftrip" />',
                   '     <attribute name="bsd_deliverymethod" />',
                   '     <order attribute="bsd_priceoftrip" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_addressfrom" operator="eq" uitype="bsd_address" value="' + warehouseaddress[0].id + '" />',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="bsd_truckload" operator="eq" uitype="bsd_truckload" value="' + truckload[0].id + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                    '      <condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                    '    </filter>',
                    '    <link-entity name="bsd_zone" from="bsd_zoneid" to="bsd_zone" alias="ao">',
                    '      <link-entity name="bsd_zoneaddress" from="bsd_zone" to="bsd_zoneid" alias="ap">',
                    '        <filter type="and">',
                    '          <condition attribute="bsd_address" operator="eq" value="' + shiptoaddress[0].id + '" />',
                    '        </filter>',
                    '      </link-entity>',
                    '    </link-entity>',
                    '  </entity>',
                    '</fetch>'].join("");
            CrmFetchKit.Fetch(xml, false).then(function (rs) {
                if (rs.length > 0) {
                    var first = rs[0];
                    if (first.getValue("bsd_priceoftrip") != null) {
                        data = {
                            pricelist_id: first.Id,
                            pricelist_name: first.attributes.bsd_name.value,
                            price: first.attributes.bsd_priceoftrip.value,
                            porter: false
                        };
                    }
                }
            });
        }
        if (data != null) {
            set_shipping_pricelist(data);
            clearNotification("bsd_shippingpricelistname");
        } else {
            set_shipping_pricelist(null);
        }
    } else {
        porteroption_change(reset);
    }
}

//huy
function load_truckload(reset) {
    var shiptoaddress = getValue("bsd_shiptoaddress");
    var warehouse_address = getValue("bsd_warehouseaddress");

    if (shiptoaddress != null && warehouse_address != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                   '   <entity name="bsd_truckload">',
                   '     <attribute name="bsd_truckloadid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '     <link-entity name="bsd_shippingpricelist" from="bsd_truckload" to="bsd_truckloadid" alias="bv">',
                   '       <filter type="and">',
                   '         <condition attribute="bsd_deliverymethod" operator="eq" value="861450001" />',
                   '         <condition attribute="bsd_addressfrom" operator="eq"  uitype="bsd_address" value="' + warehouse_address[0].id + '" />',
                   '       </filter>',
                   '       <link-entity name="bsd_zone" from="bsd_zoneid" to="bsd_zone" alias="bw">',
                   '         <link-entity name="bsd_zoneaddress" from="bsd_zone" to="bsd_zoneid" alias="bx">',
                   '           <filter type="and">',
                   '             <condition attribute="bsd_address" operator="eq" uitype="bsd_address" value="' + shiptoaddress[0].id + '" />',
                   '           </filter>',
                   '         </link-entity>',
                   '       </link-entity>',
                   '     </link-entity>',
                   '   </entity>',
                   ' </fetch>'].join("");
        var layoutXml = "<grid name='resultset' object='1' jump='bsd_truckloadid' select='1' icon='0' preview='0'>  " +
                        "<row name='result' id='bsd_truckloadid'> <cell name='bsd_name'   " + "width='200' /></row></grid>";
        getControl("bsd_truckload").addCustomView(getDefaultView("bsd_truckload"), "bsd_truckload", "bsd_truckload", xml, layoutXml, true);
    }
}


//huy -8h20h00  14/3/2017
//huy update 10h30 17/3/2017
function load_unitdefault(reset) {
    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs[0].getValue("bsd_unitdefault") != null) {
                setValue("bsd_unitdefault", [{
                    id: rs[0].attributes.bsd_unitdefault.guid,
                    name: rs[0].attributes.bsd_unitdefault.name,
                    entityType: rs[0].attributes.bsd_unitdefault.logicalName
                }]);
            }
        });

    }
}

//huy
function load_currencydefault(reset) {
    if (reset != false) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_configdefault">',
                   '     <attribute name="bsd_configdefaultid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_unitdefault" />',
                   '     <attribute name="bsd_currencydefault" />',
                   '     <attribute name="bsd_bankdefault" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs[0].getValue("bsd_currencydefault") != null) {
                setValue("bsd_currencydefault", [{
                    id: rs[0].attributes.bsd_currencydefault.guid,
                    name: rs[0].attributes.bsd_currencydefault.name,
                    entityType: rs[0].attributes.bsd_currencydefault.logicalName
                }]);
            }
        });

    }
}

//huy: add function 11h50 27/2/2017
function load_porter() {
    var effective_to = getValue("bsd_todate");
    var effective_from = getValue("bsd_fromdate");
    getControl("bsd_priceofporter").removePreSearch(presearch_priceofporter);
    if (effective_from != null && effective_to != null) {
        var date_form = effective_from.getFullYear() + "-" + (effective_from.getMonth() + 1) + "-" + effective_from.getDate();
        var date_to = effective_to.getFullYear() + "-" + (effective_to.getMonth() + 1) + "-" + effective_to.getDate();

        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_porter">',
                   '     <attribute name="bsd_porterid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_effectiveto" />',
                   '     <attribute name="bsd_effectivefrom" />',
                   '     <attribute name="bsd_unit" />',
                   '     <attribute name="bsd_pricee" />',
                   '     <order attribute="bsd_effectiveto" descending="true" />',
                   '     <order attribute="createdon" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");

        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_porterid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                           "<row name='result'  " + "id='bsd_porterid'>  " +
                                           "<cell name='bsd_name'   " + "width='200' />  " +
                                           "</row>" +
                                        "</grid>";
        getControl("bsd_priceofporter").addCustomView(getDefaultView("bsd_priceofporter"), "bsd_porter", "bsd_porter", xml, layoutXml, true);
        var priceofporter = getValue("bsd_priceofporter");
        if (priceofporter != null) {
            var xml2 = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_porter">',
                   '     <attribute name="bsd_porterid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_effectiveto" />',
                   '     <attribute name="bsd_effectivefrom" />',
                   '     <attribute name="bsd_unit" />',
                   '     <attribute name="bsd_pricee" />',
                   '     <order attribute="bsd_effectiveto" descending="true" />',
                   '     <order attribute="createdon" descending="true" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_effectivefrom" operator="on-or-before" value="' + date_form + '" />',
                   '       <condition attribute="bsd_effectiveto" operator="on-or-after" value="' + date_to + '" />',
                   '       <condition attribute="bsd_porterid" operator="eq" uitype="bsd_porter" value="' + priceofporter[0].id + '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join('');
            CrmFetchKit.Fetch(xml2, false).then(function (rs) {
                if (rs.length > 0) {

                } else {

                    setNull(["bsd_priceofporter", "bsd_pricepotter"]);
                }
            });
        }
    } else {
        clear_priceofporter();
        setNull(["bsd_priceofporter", "bsd_pricepotter"]);
    }
}
function clear_priceofporter() {
    getControl("bsd_priceofporter").addPreSearch(presearch_priceofporter);
}
function presearch_priceofporter() {
    getControl("bsd_priceofporter").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}

function BtnCreateSubOrder() {
    var exec = true;
    var type = getValue("bsd_type");
    if (type == 861450001 || type == 861450002)
    {
        clearNotification("bsd_contracttype");
        clearNotification("bsd_daystartcontract");
        clearNotification("bsd_dayendcontract");
        var str = "";
        if (getValue("bsd_contracttype") == null)
        {
            setNotification("bsd_contracttype", "Contract Type is require");
            str += "Contract Type is require; "
            exec = false;
        }
        if (getValue("bsd_daystartcontract") == null)
        {
            setNotification("bsd_daystartcontract", "The day start contract is require");
            str += "The day start contract is require; "
            exec = false;
        }
        if (getValue("bsd_dayendcontract") == null)
        {
            setNotification("bsd_dayendcontract", "The day end contract is require");
            str += "The day end contract is require; "
            exec = false;
        }
        if (exec==false)
        {
            alert(str);
        }
    }
    if (exec == true)
    {
        var xml;
        //TH bán Mật rỉ: không kiểm tra số lượng trên SUb Order phải nhỏ hơn số lượng còn lại trên Order
        if (type == 861450003 || type == 861450002) {

            xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                  '<entity name="salesorderdetail">',
                    '<attribute name="salesorderdetailid" />',
                    '<filter type="and">',
                      '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + getId() + '" />',
                    '</filter>',
                  '</entity>',
                '</fetch>'].join("");
        }
        else {
            xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                  '<entity name="salesorderdetail">',
                    '<attribute name="salesorderdetailid" />',
                    '<filter type="and">',
                      '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + getId() + '" />',
                      '<condition attribute="bsd_remainingquantity" operator="gt" value="0" />',
                    '</filter>',
                  '</entity>',
                '</fetch>'].join("");
        }
        console.log(xml);
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                if (confirm("Are you sure to create new sub order ?")) {
                    ExecuteAction(Xrm.Page.data.entity.getId(), "salesorder", "bsd_Action_CreateSubOrder", null, function (result) {
                        if (result != null && result.status != null) {
                            if (result.status == "success") {
                                reload_page();
                            } else if (result.status == "error") {
                                alert(result.data);
                            } else {
                                alert(result.data);
                            }
                        }
                    });
                }

                if (type == 861450001) {

                } else if (type == 861450000) {
                    //ExecuteAction(Xrm.Page.data.entity.getId(), "salesorder", "bsd_Action_HDKT_SubOrder", null, function (result) {
                    //    if (result != null && result.status != null) {
                    //        if (result.status == "success") {
                    //            reload_page();
                    //        } else if (result.status == "error") {
                    //            alert(result.data);
                    //        } else {
                    //            alert(result.data);
                    //        }
                    //    }
                    //});
                }
            } else {
                alert("Không sản phẩm nào còn dư số lượng để tạo suborder");
            }
        });
    }
    
}

function BtnCreateSubOrderEnableRule() {
    var id = Xrm.Page.data.entity.getId();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var statecode = Xrm.Page.getAttribute("statuscode").getValue();
    var stagename = Xrm.Page.data.process.getActiveStage().getId();
    //stagename = Xrm.Page.data.process.getActiveStage().getId();
    var xmlaccount = [];
    if (id != " ") {
        if (rnames.length > 1) {
            for (var i = 0; i < rnames.length; i++) {
                if (rnames[i] == "System Administrator") {
                    return true;
                }
            }
        }
        else {
            //stage:finnish
            if (stagename == "cee35035-2ba2-a068-3669-dd52ee471c37" || rnames == "System Administrator")
                return true;
            else
                return false;
        }
    }
    if (id == " ") {
        return false;
    }
    return true;
}

// end huy

function load_sale_tax_group() {
    var customer = getValue("customerid");
    if (customer != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="account">',
           ' <attribute name="name" />',
           ' <attribute name="bsd_saletaxgroup" />',
           ' <filter type="and">',
            '  <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
           ' </filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0 && rs[0].getValue("bsd_saletaxgroup") != null) {
                console.log(rs[0]);
                setValue("bsd_saletaxgroup", [{
                    id: rs[0].getValue("bsd_saletaxgroup"),
                    name: rs[0].attributes.bsd_saletaxgroup.name,
                    entityType: rs[0].attributes.bsd_saletaxgroup.logicalName
                }]);
            }
        });
    } else {
        setNull("bsd_saletaxgroup");
    }
}
function invoice_name_account_change(rs) {
    if (rs != false) setNull("bsd_contactinvoiceaccount");
    getControl("bsd_contactinvoiceaccount").removePreSearch(presearch_contact_invoice);
    var invoice_account = getValue("bsd_invoicenameaccount");
    if (invoice_account != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
      '<entity name="contact">',
        '<attribute name="fullname" />',
        '<attribute name="telephone1" />',
        '<attribute name="contactid" />',
        '<order attribute="fullname" descending="false" />',
        '<filter type="and">',
          '<condition attribute="parentcustomerid" operator="eq" uitype="account" value="' + invoice_account[0].id + '" />',
        '</filter>',
      '</entity>',
    '</fetch>'].join("");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                            "<row name='result'  " + "id='contactid'>  " +
                                            "<cell name='fullname'   " + "width='200' />  " +
                                            "</row>" +
                                         "</grid>";
        getControl("bsd_contactinvoiceaccount").addCustomView(getDefaultView("bsd_contactinvoiceaccount"), "contact", "contact", xml, layoutXml, true);
    } else if (rs != false) {
        clear_contact_invoice();
    }
}
function clear_contact_invoice() {
    getControl("bsd_contactinvoiceaccount").addPreSearch(presearch_contact_invoice);
}
function presearch_contact_invoice() {
    getControl("bsd_contactinvoiceaccount").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function filter_grid_deliveryplan() {
    var objSubGrid = window.parent.document.getElementById("bsd_scheduledelivery");
    if (objSubGrid != null && objSubGrid.control != null) {
        var xml = [
            '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="bsd_scheduledelivery">',
                '<attribute name="bsd_scheduledeliveryid" />',
                '<attribute name="bsd_name" />',
                '<attribute name="createdon" />',
                '<order attribute="bsd_name" descending="false" />',
                '<filter type="and">',
                  '<condition attribute="bsd_orderid" operator="eq" uitype="salesorder" value="' + Xrm.Page.data.entity.getId() + '" />',
                '</filter>',
              '</entity>',
            '</fetch>'
        ];
        objSubGrid.control.SetParameter("fetchXML", xml);
        objSubGrid.control.Refresh();
    } else {
        setTimeout('filter_grid_deliveryplan()', 1000);
    }
}
function filter_grid_orderhistory() {
    var objSubGrid = window.parent.document.getElementById("orderhistorySubgrid");
    var rootOrder = getValue("bsd_rootorder");
    if (objSubGrid != null && objSubGrid.control != null) {
        var rootOrderId = (rootOrder != null) ? rootOrder[0].id : Xrm.Page.data.entity.getId();
        var xml = [
           '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
             '<entity name="salesorder">',
               '<attribute name="name" />',
               '<attribute name="salesorderid" />',
               '<order attribute="name" descending="false" />',
               '<filter type="or">',
                   '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + rootOrderId + '" />',
                   '<condition attribute="bsd_rootorder" operator="eq" uitype="salesorder" value="' + rootOrderId + '" />',
               '</filter>',
             '</entity>',
           '</fetch>'
        ];
        objSubGrid.control.SetParameter("fetchXML", xml);
        objSubGrid.control.Refresh();
    } else {
        setTimeout('filter_grid_orderhistory()', 1000);
    }
}

function CheckQuantityRemaining() {
    debugger;
    var result = false;
    var gridOrderProduct = window.parent.document.getElementById("salesorderdetailsGrid");
    if (gridOrderProduct != null && gridOrderProduct.control != null && gridOrderProduct.control.get_selectedRecords().length > 0) {
        var selectedRow = gridOrderProduct.control.get_selectedRecords()[0];
        CrmFetchKit.Fetch([
            '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
              '<entity name="salesorderdetail">',
                '<attribute name="productid" />',
                '<attribute name="salesorderid" />',
                        '<attribute name="quantity" />',
                '<order attribute="productid" descending="false" />',
                '<filter type="and">',
                  '<condition attribute="salesorderdetailid" operator="eq" value="' + selectedRow.Id + '" />',
                '</filter>',
              '</entity>',
            '</fetch>'
        ].join(""), false).then(function (rs) {
            if (rs.length > 0) {
                // số lượng đặt hàng của sản phẩm này
                var ProductQuantity = rs[0].attributes.quantity.value;
                CrmFetchKit.Fetch([
                        '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                              '<entity name="bsd_scheduledelivery">',
                                    '<attribute name="bsd_scheduledeliveryid" />',
                                    '<attribute name="bsd_shipquantity" />',
                                            '<attribute name="createdon" />',
                                            '<order attribute="bsd_name" descending="false" />',
                                            '<link-entity name="salesorder" from="salesorderid" to="bsd_orderid" alias="ae">',
                                                  '<filter type="and">',
                                                    '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + rs[0].attributes.salesorderid.guid + '" />',
                                                  '</filter>',
                                            '</link-entity>',
                                            '<link-entity name="product" from="productid" to="bsd_itemid" alias="af">',
                                                  '<filter type="and">',
                                                    '<condition attribute="productid" operator="eq" uitype="product" value="' + rs[0].attributes.productid.guid + '" />',
                                                  '</filter>',
                                            '</link-entity>',
                                      '</entity>',
                                '</fetch>',
                ].join(""), false).then(function (rs) {
                    var TotalShipped = 0;
                    if (rs.length > 0) {
                        for (var i = 0; i < rs.length; i++) {
                            TotalShipped += rs[i].attributes.bsd_shipquantity.value;
                        }
                    }
                    if (ProductQuantity > TotalShipped) {
                        Xrm.Page.ui.clearFormNotification('1');
                        result = true;
                    } else {
                        Xrm.Page.ui.setFormNotification("Sản phẩm này đã lên lịch đủ số lượng. ", "WARNING", "1");
                        result = false;
                    }
                });
            }
        });
    } else {
        CrmFetchKit.Fetch([
        '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
         '<entity name="salesorderdetail">',
           '<attribute name="productid" />',
            '<attribute name="salesorderid" />',
            '<attribute name="quantity" />',
            '<order attribute="productid" descending="false" />',
            '<filter type="and">',
            '<condition attribute="salesorderid" operator="eq" value="' + Xrm.Page.data.entity.getId() + '" />',
            '</filter>',
            '</entity>',
            '</fetch>'
        ].join(""), false).then(function (rs) {
            Xrm.Page.ui.clearFormNotification('1');
            if (rs.length > 0) {
                var OrderQuantity = 0;
                for (var i = 0; i < rs.length; i++) {
                    OrderQuantity += rs[i].attributes.quantity.value;
                }
                CrmFetchKit.Fetch([
                    '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                              '<entity name="bsd_scheduledelivery">',
                                    '<attribute name="bsd_scheduledeliveryid" />',
                                    '<attribute name="bsd_shipquantity" />',
                                    '<attribute name="createdon" />',
                                    '<order attribute="bsd_name" descending="false" />',
                                    '<link-entity name="salesorder" from="salesorderid" to="bsd_orderid" alias="ae">',
                                          '<filter type="and">',
                                            '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + Xrm.Page.data.entity.getId() + '" />',
                                          '</filter>',
                                    '</link-entity>',
                              '</entity>',
                        '</fetch>',
                ].join(""), false).then(function (rs) {
                    var TotalShipped = 0;
                    if (rs.length > 0) {
                        for (var i = 0; i < rs.length; i++) {
                            TotalShipped += rs[i].attributes.bsd_shipquantity.value;
                        }
                    }
                    if (OrderQuantity > TotalShipped) {
                        result = true;
                    } else {
                        Xrm.Page.ui.setFormNotification("Đơn hàng này đã lên lịch giao hàng đủ số lượng. ", "WARNING", "1");
                        result = false;
                    }
                });
            } else {
                Xrm.Page.ui.setFormNotification("Chưa có sản phẩm. ", "WARNING", "1");
            }
        });
    }
    return result;
}
function set_currenciesdefault() {
    var xml = [
           '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
           '<entity name="transactioncurrency">',
       '<attribute name="transactioncurrencyid" />',
       '<attribute name="currencyname" />',
       '<attribute name="isocurrencycode" />',
       '<attribute name="currencysymbol" />',
       '<attribute name="exchangerate" />',
       '<attribute name="currencyprecision" />',
       '<order attribute="currencyname" descending="false" />',
       '<filter type="and">',
         '<condition attribute="isocurrencycode" operator="eq" value="VND" />',
       '</filter>',
       '</entity>',
       '</fetch>'
    ].join("");
    CrmFetchKit.Fetch(xml, false).then(function (rs) {
        if (rs.length > 0) {
            setValue("transactioncurrencyid", [{
                id: rs[0].Id,
                name: rs[0].attributes.currencyname.value,
                entityType: "transactioncurrency"
            }]);
        }
    }, function (er) {
        console.log(er.message);
    });
}


function BtnReviseOrder() {
    if (confirm("Bạn có chắc chắn không? ")) {
        ExecuteAction(Xrm.Page.data.entity.getId(), "salesorder", "bsd_Action_ReviseOrder", null, function (result) {
            console.log(result);
            if (result != null && result.status != null) {
                if (result.status == "success") {
                    Xrm.Utility.openEntityForm("salesorder", result.data.ReturnId.value);
                } else if (result.status == "error") {
                    alert(result.data);
                } else {
                    alert(result.data);
                }
            }
        });
    }
}
function BtnReviseOrderEnableRule() {
    //if (formType() == 2) {
    //    return true;
    //} else {
    //    return false;
    //}
    //debugger;
    //var result = false;
    //var id = Xrm.Page.data.entity.getId();
    //stagename = Xrm.Page.data.process.getActiveStage().getName();
    //if (id != null) {
    //    var xml = [];
    //    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
    //    xml.push("<entity name='salesorder'>");
    //    xml.push("<attribute name='name' />");
    //    xml.push("<attribute name='customerid' />");
    //    xml.push("<attribute name='statuscode' />");
    //    xml.push("<attribute name='totalamount' />");
    //    xml.push("<attribute name='salesorderid' />");
    //    xml.push("<order attribute='name' descending='false' />");
    //    xml.push("<filter type='and'>");
    //    xml.push("<condition attribute='salesorderid' operator='eq' value='" + id + "' />");
    //    xml.push("</filter>");
    //    xml.push("</entity>");
    //    xml.push("</fetch>");
    //    CrmFetchKit.Fetch(xml.join(''), false).then(function (rs) {
    //        if (rs.length > 0) {
    //            if (stagename == "Cấp Thẩm Quyền Duyệt")
    //                result = true;
    //            else
    //                result = false;
    //        }
    //        else
    //            result = false;
    //    },
    //    function (er) {
    //        console.log(er.message);
    //    });
    //    //result = true;
    //} else {
    //    result = false;
    //}
    //return result;
    return false;
}

function BtnEconomicContractEnableRule() {
    var quote = getValue("quoteid");
    if (quote != null) {
        return false;
    } else {
        return true;
    }
}

function ExecuteAction(entityId, entityName, requestName, inputArg, callback, isGlobal) {
    debugger;
    // Creating the request XML for calling the Action
    function GetClientUrl() {
        if (typeof Xrm.Page.context == "object") {
            clientUrl = Xrm.Page.context.getClientUrl();
        }
        var ServicePath = "/XRMServices/2011/Organization.svc/web";
        return clientUrl + ServicePath;
    }
    var requestXML = [];
    requestXML.push("<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">");
    requestXML.push("<s:Body>");
    requestXML.push("<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">");
    requestXML.push("<request xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">");
    requestXML.push("<a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">");
    if (inputArg != null && inputArg.length > 0) {
        for (var i = 0; i < inputArg.length; i++) {
            var tmp = inputArg[i];
            //var tmp = { name: '',type:'',value:'' };
            requestXML.push("<a:KeyValuePairOfstringanyType>");
            requestXML.push("<b:key>" + tmp.name + "</b:key>");
            requestXML.push("<b:value i:type=\"c:" + tmp.type + "\" xmlns:c=\"http://www.w3.org/2001/XMLSchema\">" + tmp.value + "</b:value>");
            requestXML.push("</a:KeyValuePairOfstringanyType>");
        }
    }
    if (isGlobal != true) {
        requestXML.push("<a:KeyValuePairOfstringanyType>");
        requestXML.push("<b:key>Target</b:key>");
        requestXML.push("<b:value i:type=\"a:EntityReference\">");
        requestXML.push("<a:Id>" + entityId + "</a:Id>");
        requestXML.push("<a:LogicalName>" + entityName + "</a:LogicalName>");
        requestXML.push("<a:Name i:nil=\"true\" />");
        requestXML.push("</b:value>");
        requestXML.push("</a:KeyValuePairOfstringanyType>");
    }
    requestXML.push("</a:Parameters>");
    requestXML.push("<a:RequestId i:nil=\"true\" />");
    requestXML.push("<a:RequestName>" + requestName + "</a:RequestName>");
    requestXML.push("</request>");
    requestXML.push("</Execute>");
    requestXML.push("</s:Body>");
    requestXML.push("</s:Envelope>");
    var req = new XMLHttpRequest();
    req.open('POST', GetClientUrl(), true);
    req.setRequestHeader('Accept', 'application/xml, text/xml, */*');
    req.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
    req.setRequestHeader('SOAPAction', 'http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute');
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            if (req.status == 200) {
                debugger;
                var result = {
                    status: 'success',
                    data: null
                }
                if (req.response != null && req.response.length > 0) {
                    try {
                        var rs = $.parseXML(req.response).getElementsByTagName("KeyValuePairOfstringanyType");
                        if (rs.length == 0)
                            rs = $.parseXML(req.response).getElementsByTagName("a:KeyValuePairOfstringanyType");
                        var len = rs.length;
                        if (len > 0) {
                            result.data = {};
                            for (var i = 0; i < len; i++) {
                                var key = rs[i].firstElementChild;
                                var sib = key.nextElementSibling;
                                result.data[key.textContent] = {
                                    type: sib.hasAttribute("i:type") ? sib.attributes["i:type"].value : '',
                                    value: key.nextElementSibling.textContent
                                };
                            }
                        }
                    }
                    catch (ex) {
                        result.status = "error";
                        result.data = ex.message;
                        console.log(ex.message)
                    }
                }
                if (callback != null)
                    callback(result);
            }
            else if (req.status == 500) {
                if (req.responseXML != "") {
                    var mss = req.responseXML.getElementsByTagName("Message");
                    if (mss.length > 0) {
                        if (callback != null)
                            callback({ status: "error", data: mss[0].firstChild.nodeValue });
                        console.log(mss[0].firstChild.nodeValue);
                    }
                }
                else if (callback != null)
                    callback(null);
            }
        }
    };
    req.send(requestXML.join(''));
}

function BtnAddSalesorderdetail() {
    //var check = false;
    //var rs = CrmFetchKit.FetchSync(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
    //      '<entity name="salesorder">',
    //        '<attribute name="name" />',
    //        '<attribute name="bsd_status" />',
    //        '<attribute name="bsd_createfrom" />',
    //        '<attribute name="salesorderid" />',
    //        '<order attribute="name" descending="false" />',
    //        '<filter type="and">',
    //          '<condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + Xrm.Page.data.entity.getId() + '" />',
    //        '</filter>',
    //      '</entity>',
    //    '</fetch>'].join(''));
    //if (rs.length > 0) {
    //    var salesorder = rs[0];
    //    var bsd_createfrom = salesorder.attributes.bsd_createfrom.value;
    //    var bsd_status = salesorder.attributes.bsd_status.value;
    //    if (bsd_createfrom == 861450003 && bsd_status == 1) {
    //        check = true;
    //    }
    //}
    //return check;
    return true;
}

// bấm xóa sản phẩm trên subgrid product
function BtnDeleteProduct() {
    var result = false;
    var quote = getValue("quoteid");
    if (quote != null) {
        alert("Cannot delete this product.");
    } else {

        // nếu từ new. check suborder
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_suborder">',
            '<attribute name="bsd_suborderid" />',
            '<attribute name="bsd_name" />',
            '<filter type="and">',
              '<condition attribute="bsd_order" operator="eq" uitype="salesorder" value="' + getId() + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length == 0) {
                result = true;
            } else {
                alert("Đã tạo suborder");
            }
        });
    }
    setTimeout(function () {
        //Xrm.Page.ui.refreshRibbon();
    }, 3000);
    return result;
}

// bấm nút thêm mới trên subgrid, associated view.
function BtnAddNewProduct() {
    var result = false;
    var quote = Xrm.Page.getAttribute("quoteid").getValue();
    if (quote != null) {
        result = false;
    } else {
        result = true;
        //// nếu từ new. check suborder
        //var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
        //  '<entity name="bsd_suborder">',
        //    '<attribute name="bsd_suborderid" />',
        //    '<attribute name="bsd_name" />',
        //    '<filter type="and">',
        //      '<condition attribute="bsd_order" operator="eq" uitype="salesorder" value="' + getId() + '" />',
        //    '</filter>',
        //  '</entity>',
        //'</fetch>'].join("");
        //CrmFetchKit.Fetch(xml, false).then(function (rs) {
        //    if (rs.length == 0) {
        //        result = true;
        //    }
        //});
    }
    return result;
}

function setaccounttypecustomer() {
    var xml = [];
    var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    var entityName = "account";
    var viewDisplayName = "test";
    fetch(xml, "account", ["accountid", "name"], ["name"], false, null, ["bsd_accounttype","statecode"], ["eq","eq"], [0, "861450000",1,"0"]);
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='accountid'>  " +
                      "<cell name='name'    " + "width='200' />  " +
                      "</row>   " +
                   "</grid>   ";
    Xrm.Page.getControl("customerid").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    Xrm.Page.getControl("bsd_shiptoaccount").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    Xrm.Page.getControl("bsd_invoicenameaccount").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    //Xrm.Page.getControl("bsd_customername").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
}

function get_customerinformation() {
    Xrm.Page.getAttribute("bsd_paymentterm").setSubmitMode("always");
    Xrm.Page.getAttribute("bsd_paymentmethod").setSubmitMode("always");
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    var fieldaddress = Xrm.Page.getAttribute("bsd_address");
    var fieldpricelist = Xrm.Page.getAttribute("pricelevelid");
    var fieldpaymentterm = Xrm.Page.getAttribute("bsd_paymentterm");
    var fieldpaymentmethod = Xrm.Page.getAttribute("bsd_paymentmethod");
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount");
    var addressinvoicenameaccount = Xrm.Page.getAttribute("bsd_addressinvoiceaccount");
    var partnerscompany = Xrm.Page.getAttribute("bsd_shiptoaccount");
    var currency = Xrm.Page.getAttribute("transactioncurrencyid").getValue();
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    var xml7 = [];
    var xml8 = [];
    var xml9 = [];
    var xml10 = [];
    var xml11 = [];
    var xml12 = [];
    var xml13 = [];
    var xml14 = [];
    var xml15 = [];
    var xml16 = [];
    var xml17 = [];
    var xml18 = [];
    var xmlpricelistaccount = [];
    var xmlaccountpricegroup = [];
    var xmlpricegroup = [];
    var xmlpricelistall = [];
    var day = Xrm.Page.getAttribute("bsd_fromdate").getValue();
    if (day != null) {
        var year = day.getFullYear() + "";
        if (day.getMonth() + 1 <= 9) {
            var month = "0" + (day.getMonth() + 1) + "";
        }
        else {
            var month = (day.getMonth() + 1) + "";
        }
        var dayy = day.getDate() + "";
        var dateFormat = year + "-" + month + "-" + dayy;
        var yearpricelist = day.getFullYear();
        var monthpricelist = (day.getMonth() + 1);
        var daypricelist = day.getDate();
    }
    if (customer != null) {
        set_value(["bsd_shiptoaddress", "bsd_shiptoaccount"], null);
        fetch(xml, "account", ["name", "primarycontactid", "telephone1", "accountid"
                                , "bsd_taxregistration", "fax", "accountnumber", "emailaddress1"
                                , "bsd_paymentterm", "bsd_paymentmethod", "transactioncurrencyid"], ["createdon"], false, null,
                                ["accountid"], ["eq"], [0, customer[0].id, 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                invoicenameaccount.setValue([{
                    id: rs[0].Id,
                    name: rs[0].attributes.name.value,
                    entityType: rs[0].logicalName
                }]);
                partnerscompany.setValue([{
                    id: customer[0].id,
                    name: customer[0].name,
                    entityType: 'account'
                }]);
                Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(rs[0].attributes.accountnumber.value);
                if (rs[0].attributes.telephone1 != null) {
                    Xrm.Page.getAttribute("bsd_telephone").setValue(rs[0].attributes.telephone1.value);
                }
                if (rs[0].attributes.telephone1 == null) {
                    Xrm.Page.getAttribute("bsd_telephone").setValue(null);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(rs[0].attributes.bsd_taxregistration.value);
                }
                if (rs[0].attributes.bsd_taxregistration == null) {
                    Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                }
                if (rs[0].attributes.fax != null) {
                    Xrm.Page.getAttribute("bsd_fax").setValue(rs[0].attributes.fax.value);
                }
                if (rs[0].attributes.fax == null) {
                    Xrm.Page.getAttribute("bsd_fax").setValue(null);
                }
                if (rs[0].attributes.accountnumber != null) {
                    var no = rs[0].attributes.accountnumber.value;
                    Xrm.Page.getAttribute("bsd_customercode").setValue(no);
                }
                if (rs[0].attributes.accountnumber == null) {
                    Xrm.Page.getAttribute("bsd_customercode").setValue(null);
                }
                if (rs[0].attributes.emailaddress1 != null) {
                    Xrm.Page.getAttribute("bsd_mail").setValue(rs[0].attributes.emailaddress1.value);
                }
                if (rs[0].attributes.emailaddress1 == null) {
                    Xrm.Page.getAttribute("bsd_mail").setValue(null);
                }
                if (rs[0].attributes.bsd_paymentterm != null) {
                    fieldpaymentterm.setValue([{
                        id: rs[0].attributes.bsd_paymentterm.guid,
                        name: rs[0].attributes.bsd_paymentterm.name,
                        entityType: 'bsd_paymentterm'
                    }]);
                }
                if (rs[0].attributes.bsd_paymentterm == null) {
                    fieldpaymentterm.setValue(null);
                }
                if (rs[0].attributes.bsd_paymentmethod != null) {
                    fieldpaymentmethod.setValue([{
                        id: rs[0].attributes.bsd_paymentmethod.guid,
                        name: rs[0].attributes.bsd_paymentmethod.name,
                        entityType: 'bsd_methodofpayment'
                    }]);
                }
                if (rs[0].attributes.bsd_paymentmethod == null) {
                    fieldpaymentmethod.setValue(null);
                }
            }
        },
        function (er) {
            console.log(er.message)
        });
        var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName1 = "bsd_address";
        var viewDisplayName1 = "test";
        fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
           ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, customer[0].id]);
        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
            if (rs1.length > 0) {
                if (rs1[0].attributes.bsd_name != null) {
                    fieldaddress.setValue([{
                        id: rs1[0].Id,
                        name: rs1[0].attributes.bsd_name.value,
                        entityType: rs1[0].logicalName
                    }]);
                }
                Xrm.Page.getControl("bsd_address").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
            }
            if (rs1.length == 0) {
                var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName = "bsd_address";
                var viewDisplayName = "test";
                fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                    if (rs2.length > 0) {
                        if (rs2[0].attributes.bsd_name != null) {
                            fieldaddress.setValue([{
                                id: rs2[0].Id,
                                name: rs2[0].attributes.bsd_name.value,
                                entityType: rs2[0].logicalName
                            }]);
                            Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
                        }
                    }
                    if (rs2.length == 0) {
                        fieldaddress.setValue(null);
                        Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }
        },
    function (er) {
        console.log(er.message)
    });
        var viewId12 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName12 = "bsd_address";
        var viewDisplayName12 = "test";
        fetch(xml12, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Invoice" + "%", 1, "0", 2, customer[0].id]);
        var layoutXml12 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xml12.join(""), true).then(function (rs12) {
            if (rs12.length > 0) {
                if (rs12[0].attributes.bsd_name != null) {
                    addressinvoicenameaccount.setValue([{
                        id: rs12[0].Id,
                        name: rs12[0].attributes.bsd_name.value,
                        entityType: rs12[0].logicalName
                    }]);
                }
                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId12, entityName12, viewDisplayName12, xml12.join(""), layoutXml12, true);
            }
            if (rs12.length == 0) {
                var viewId13 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName13 = "bsd_address";
                var viewDisplayName13 = "test";
                fetch(xml13, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                       ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXml13 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                CrmFetchKit.Fetch(xml13.join(""), true).then(function (rs13) {
                    if (rs13.length > 0) {
                        if (rs13[0].attributes.bsd_name != null) {
                            if (rs13[0].attributes.bsd_name != null) {
                                addressinvoicenameaccount.setValue([{
                                    id: rs13[0].Id,
                                    name: rs13[0].attributes.bsd_name.value,
                                    entityType: rs13[0].logicalName
                                }]);
                            }
                            Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId13, entityName13, viewDisplayName13, xml13.join(""), layoutXml13, true);
                        }
                    }
                    if (rs13.length == 0) {
                        addressinvoicenameaccount.setValue(null);
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId13, entityName13, viewDisplayName13, xml13.join(""), layoutXml13, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }
        },
    function (er) {
        console.log(er.message)
    });
        var viewId14 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName14 = "bsd_address";
        var viewDisplayName14 = "test";
        fetch(xml14, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
            ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customer[0].id]);
        var layoutXml14 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xml14.join(""), true).then(function (rs14) {
            if (rs14.length > 0) {
                fieldshiptoaddress.setValue([{
                    id: rs14[0].Id,
                    name: rs14[0].attributes.bsd_name.value,
                    entityType: rs14[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId14, entityName14, viewDisplayName14, xml14.join(""), layoutXml14, true);
            }
            if (rs14.length == 0) {
                var viewId15 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName15 = "bsd_address";
                var viewDisplayName15 = "test";
                fetch(xml15, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                  ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXml15 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId15, entityName15, viewDisplayName15, xml15.join(""), layoutXml15, true);
            }

        }, function (er) {
            console.log(er.message)
        });
    }
    else {
        Xrm.Page.getAttribute("pricelevelid").setValue(null);
        Xrm.Page.getAttribute("bsd_telephone").setValue(null);
        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
        Xrm.Page.getAttribute("bsd_fax").setValue(null);
        Xrm.Page.getAttribute("bsd_customercode").setValue(null);
        Xrm.Page.getAttribute("bsd_mail").setValue(null);
        Xrm.Page.getAttribute("bsd_paymentterm").setValue(null);
        Xrm.Page.getAttribute("bsd_paymentmethod").setValue(null);
        Xrm.Page.getAttribute("bsd_contact").setValue(null);
        Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(null);
        Xrm.Page.getAttribute("bsd_invoicenameaccount").setValue(null);
        Xrm.Page.getAttribute("bsd_addressinvoiceaccount").setValue(null);
        Xrm.Page.getAttribute("bsd_shiptoaccount").setValue(null);
        Xrm.Page.getControl("pricelevelid").setDisabled(true);
        fieldaddress.setValue(null);
        fieldshiptoaddress.setValue(null);
    }
}


/*
   Mr: Diệm
   Note: Load Price group theo fromdate.
*/
function LK_PotentialCustomer_Change(reset) {
    var customer = getValue("customerid");
    var fullday = getFullDay("bsd_fromdate");
    var currency = getValue("transactioncurrencyid");
    if (reset != false) setNull("pricelevelid");
    var first;
    if (customer != null && currency != null && fullday != null) {
        CrmFetchKit.Fetch(['<fetch version="1.0" output-format="xml-platform" count="1"  mapping="logical" distinct="true">',
                           '    <entity name="pricelevel">',
                           '        <all-attributes />',
                           '        <order attribute="createdon" descending="false" />',
                           '    <filter type="and">',
                           '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                           '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                           '        <condition attribute="bsd_account" operator="eq" uitype="account" value="' + customer[0].id + '"/>',
                           '        <condition attribute="statecode" operator="eq" value="0" />',
                           '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                           '    </filter>',
                           '    </entity>',
                           '</fetch>'].join(''), false).then(function (rsaccount) {
                               if (rsaccount.length > 0) /*Nếu dữ liệu Account*/ {
                                   first = rsaccount[0];
                                   if (getValue("pricelevelid") == null) {
                                       setValue("pricelevelid",
                                            [{
                                                id: first.Id,
                                                name: first.attributes.name.value,
                                                entityType: first.logicalName
                                            }]);
                                   }
                               }
                               else/*Price group*/ {
                                   CrmFetchKit.Fetch(['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                                '   <entity name="pricelevel">',
                                '        <all-attributes />',
                                '     <order attribute="createdon" descending="false" />',
                                '     <filter type="and">',
                                '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                                '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                                '        <condition attribute="statecode" operator="eq" value="0" />',
                                '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                                '     </filter>',
                                '     <link-entity name="bsd_pricegroups" from="bsd_pricegroupsid" to="bsd_pricegroup" alias="ae">',
                                '      <link-entity name="account" from="bsd_pricegroup" to="bsd_pricegroupsid" alias="af">',
                                '        <filter type="and">',
                                '          <condition attribute="accountid" operator="eq" uitype="account" value="' + customer[0].id + '" />',
                                '        </filter>',
                                '     </link-entity>',
                                '     </link-entity>',
                                '    </entity>',
                                '</fetch>'].join(''), false).then(function (rspricegroup) {
                                    if (rspricegroup.length > 0) {
                                        first = rspricegroup[0];
                                        if (getValue("pricelevelid") == null) {
                                            setValue("pricelevelid",
                                                 [{
                                                     id: first.Id,
                                                     name: first.attributes.name.value,
                                                     entityType: first.logicalName
                                                 }]);
                                        }
                                    }
                                    else /*All*/ {
                                        fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                                                  '    <entity name="pricelevel">',
                                                  '        <all-attributes />',
                                                  '      <order attribute="createdon" descending="false" />',
                                                  '      <filter type="and">',
                                                  '        <condition attribute="statecode" operator="eq" value="0" />',
                                                  '        <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                                                  '        <condition attribute="begindate" operator="on-or-before" value="' + fullday + '" />',
                                                  '        <condition attribute="enddate" operator="on-or-after" value="' + fullday + '" />',
                                                  '        <condition attribute="bsd_pricelisttype" operator="eq" value="100000000" />',
                                                  '     </filter>',
                                                  '   </entity>',
                                                  '</fetch>'].join('');
                                        CrmFetchKit.Fetch(fetchxml, false).then(function (rsAll) {
                                            if (rsAll.length > 0) {
                                                first = rsAll[0];
                                                if (getValue("pricelevelid") == null) {
                                                    setValue("pricelevelid",
                                                         [{
                                                             id: first.Id,
                                                             name: first.attributes.name.value,
                                                             entityType: first.logicalName
                                                         }]);
                                                }
                                            } else {
                                                m_alert("Price List for this Account does not exist. Please define Price List first.");
                                            }

                                        }, function (er) { });
                                    }
                                }, function (er) { });
                               }
                           }, function (er) { });
    }
    else if (reset != false) {
        clear_priceleve();
    }
    Load_exchangerate(currency);
}

/*Diê?m: Load exchangerate tu` currency*/
function Load_exchangerate(currency) {
    if (currency != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '  <entity name="transactioncurrency">',
                        '    <attribute name="transactioncurrencyid" />',
                        '    <attribute name="currencyname" />',
                        '    <attribute name="isocurrencycode" />',
                        '    <attribute name="currencysymbol" />',
                        '    <attribute name="exchangerate" />',
                        '    <attribute name="currencyprecision" />',
                        '    <order attribute="currencyname" descending="false" />',
                        '    <filter type="and">',
                        '      <condition attribute="transactioncurrencyid" operator="eq" uitype="transactioncurrency" value="' + currency[0].id + '" />',
                        '      <condition attribute="statecode" operator="eq" value="0" />',
                        '    </filter>',
                        '  </entity>',
                        '</fetch>'].join('');
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            if (rs.length > 0) {
                if (getValue("exchangerate") == null) {
                    setValue("exchangerate", rs[0].attributes.exchangerate.value);
                }
            }
        }, function (er) { });
    }
    else {
        setNull(["exchangerate"])
    }
}
/*
    Mr: Diệm
    Note: Set Function null and clear.
*/
function clear_priceleve() {
    getControl("pricelevelid").addPreSearch(presearch_priceleve);
}
function presearch_priceleve() {
    getControl("pricelevelid").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
//End Diệm



function get_contact() {
    var contactdefault = Xrm.Page.getAttribute("bsd_contact");
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var xml = [];
    var xml1 = [];
    if (customer != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xml.push("<entity name='account'>");
        xml.push("<attribute name='name' />");
        xml.push("<attribute name='primarycontactid'/>");
        xml.push("<attribute name='telephone1'/>");
        xml.push("<attribute name='accountid'/>");
        xml.push("<order attribute='name' descending='false'/> ");
        xml.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "'/>");
        xml.push("</filter>");
        xml.push("</link-entity>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                contactdefault.setValue([{
                    id: rs[0].attributes.primarycontactid.guid,
                    name: rs[0].attributes.primarycontactid.name,
                    entityType: 'contact'
                }]);
                var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityName = "contact";
                var viewDisplayName = "test";
                xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml1.push("<entity name='contact'>");
                xml1.push("<attribute name='fullname'/>");
                xml1.push("<attribute name='telephone1'/>")
                xml1.push("<attribute name='contactid'/>");
                xml1.push("<order attribute='fullname' descending='true' />");
                xml1.push("<filter type='and'>");
                xml1.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
                xml1.push("</filter>");
                xml1.push("</entity>");
                xml1.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_contact").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
            }
            if (rs.length == 0) {
                var viewId1 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityName1 = "contact";
                var viewDisplayName1 = "test";
                xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml1.push("<entity name='contact'>");
                xml1.push("<attribute name='fullname'/>");
                xml1.push("<attribute name='telephone1'/>")
                xml1.push("<attribute name='contactid'/>");
                xml1.push("<order attribute='fullname' descending='true' />");
                xml1.push("<filter type='and'>");
                xml1.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
                xml1.push("</filter>");
                xml1.push("</entity>");
                xml1.push("</fetch>");
                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                Xrm.Page.getControl("bsd_contact").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                    if (rs1.length > 0 && rs1[0].attributes.fullname != null) {
                        contactdefault.setValue([{
                            id: rs1[0].Id,
                            name: rs1[0].attributes.fullname.value,
                            entityType: rs1[0].logicalName
                        }]);
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}

function set_currentdate() {
    if (Xrm.Page.ui.getFormType() == 1) {
        var currentDateTime = new Date();
        Xrm.Page.getAttribute("bsd_fromdate").setValue(currentDateTime);
    }
}

function set_invoiceaddress() {
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    var addressinvoicenameaccount = Xrm.Page.getAttribute("bsd_addressinvoiceaccount");
    var partnerscompany = Xrm.Page.getAttribute("bsd_shiptoaccount");
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    if (invoicenameaccount != null) {
        Xrm.Page.getControl("bsd_addressinvoiceaccount").setDisabled(false);
        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName = "bsd_address";
        var viewDisplayName = "test";
        fetch(xml, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                 ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Invoice" + "%", 1, "0", 2, invoicenameaccount[0].id]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_name != null) {
                    if (addressinvoicenameaccount == null) {
                        addressinvoicenameaccount.setValue([{
                            id: rs[0].Id,
                            name: rs[0].attributes.bsd_name.value,
                            entityType: rs[0].logicalName
                        }]);
                    }
                }
                Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            }
            if (rs.length == 0) {
                var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName1 = "bsd_address";
                var viewDisplayName1 = "test";
                xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml1.push("<entity name='bsd_address'>");
                xml1.push("<attribute name='bsd_addressid' />");
                xml1.push("<attribute name='bsd_name' />");
                xml1.push("<attribute name='createdon' />");
                xml1.push("<attribute name='bsd_purpose' />");
                xml1.push("<order attribute='bsd_name' descending='false' />");
                xml1.push("<filter type='and'>");
                xml1.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='" + invoicenameaccount[0].id + "' />");
                xml1.push("<condition attribute='statecode' operator='eq' value='0' />");
                xml1.push("</filter>");
                xml1.push("</entity>");
                xml1.push("</fetch>");
                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='bsd_addressid'>  " +
                      "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                        "<cell name='bsd_purpose'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
                CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                    if (rs1.length > 0) {
                        if (rs1[0].attributes.bsd_name != null) {
                            if (addressinvoicenameaccount == null) {
                                addressinvoicenameaccount.setValue([{
                                    id: rs1[0].Id,
                                    name: rs1[0].attributes.bsd_name.value,
                                    entityType: rs1[0].logicalName
                                }]);
                            }
                        }
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                    }
                    if (rs1.length == 0) {
                        addressinvoicenameaccount.setValue(null);
                        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                    }
                },
    function (er) {
        console.log(er.message)
    });
            }
        },
    function (er) {
        console.log(er.message)
    });
        fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                         ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, invoicenameaccount[0].id]);
        CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
            if (rs2.length > 0) {
                if (Xrm.Page.getAttribute("bsd_invoiceaccount").getValue == null) {
                    if (rs2[0].attributes.accountnumber != null) {
                        Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(rs2[0].attributes.accountnumber.value);
                    }
                }
                if (Xrm.Page.getAttribute("bsd_taxregistration").getValue == null) {
                    if (rs2[0].attributes.bsd_taxregistration != null) {
                        Xrm.Page.getAttribute("bsd_taxregistration").setValue(rs2[0].attributes.bsd_taxregistration.value);
                    }
                }
                if (Xrm.Page.getAttribute("bsd_taxregistration").getValue == null) {
                    if (rs2[0].attributes.bsd_taxregistration == null) {
                        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
                    }
                }
            }
        },
    function (er) {
        console.log(er.message)
    });
    }
    else {
        addressinvoicenameaccount.setValue(null);
        Xrm.Page.getAttribute("bsd_invoiceaccount").setValue(null);
        Xrm.Page.getAttribute("bsd_taxregistration").setValue(null);
        Xrm.Page.getControl("bsd_addressinvoiceaccount").setDisabled(true);
    }
}
function set_addressfromcustomerb() {
    Xrm.Page.getControl("bsd_shiptoaddress").setDisabled(false);
    var customerb = Xrm.Page.getAttribute("bsd_shiptoaccount").getValue();
    var fieldshiptoaddress = Xrm.Page.getAttribute("bsd_shiptoaddress");
    Xrm.Page.getControl("bsd_shiptoaddress").removePreSearch(presearch_LoadShiptoaddress);
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    if (customerb != null) {
        var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName2 = "bsd_address";
        var viewDisplayName2 = "test";
        fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
              ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customerb[0].id]);
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
        CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
            if (rs2.length > 0) {
                fieldshiptoaddress.setValue([{
                    id: rs2[0].Id,
                    name: rs2[0].attributes.bsd_name.value,
                    entityType: rs2[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
            }
            if (rs2.length == 0) {
                var viewId3 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName3 = "bsd_address";
                var viewDisplayName3 = "test";
                fetch(xml3, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customerb[0].id]);
                var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                    if (rs3.length > 0) {
                        Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
                    }
                    else {
                        Xrm.Page.getAttribute("bsd_shiptoaddress").setValue(null);
                        clear_LoadShiptoaddress();
                    }
                }, function (er) {
                    console.log(er.message)
                });
            }

        }, function (er) {
            console.log(er.message)
        });
    }
    if (customerb == null) {
        Xrm.Page.getAttribute("bsd_shiptoaddress").setValue(null);
        clear_LoadShiptoaddress();
    }
}
function checkdateonorder() {
    var fromday = Xrm.Page.getAttribute("bsd_fromdate").getValue();
    var today = Xrm.Page.getAttribute("bsd_todate").getValue();
    if (fromday != null) {
        Xrm.Page.getControl("bsd_fromdate").clearNotification();
        var year = fromday.getFullYear() + "";
        if (fromday.getMonth() + 1 <= 9) {
            var month = "0" + (fromday.getMonth() + 1);
        }
        else {
            var month = (fromday.getMonth() + 1);
        }
        var dayy = fromday.getDate();
    }
    if (fromday == null) {
        Xrm.Page.getAttribute("bsd_todate").setValue(null);
        Xrm.Page.getControl("bsd_fromdate").setNotification("From date is not null!!!");
    }
    if (today != null && fromday != null) {
        var yearr = today.getFullYear();
        if (fromday.getMonth() + 1 <= 9) {
            var monthh = "0" + (today.getMonth() + 1);
        }
        else {
            var monthh = (today.getMonth() + 1);
        }
        var day = today.getDate();
        if (yearr < year) {
            Xrm.Page.getControl("bsd_todate").setNotification("To date is not smaller than from date");
        }
        if (yearr == year) {
            if (monthh < month) {
                Xrm.Page.getControl("bsd_todate").setNotification("To date is not smaller than from date");
                Xrm.Page.getAttribute("bsd_todate").setValue(null);
            }
            if (monthh == month) {
                if (day => dayy) {
                    Xrm.Page.getControl("bsd_todate").clearNotification();
                }
                if (day < dayy) {
                    Xrm.Page.getControl("bsd_todate").setNotification("To date is not smaller than from date");
                    Xrm.Page.getAttribute("bsd_todate").setValue(null);

                }
            }
            if (monthh > month) {
                Xrm.Page.getControl("bsd_todate").clearNotification();
            }
        }
        if (yearr > year) {
            Xrm.Page.getControl("bsd_todate").clearNotification();
        }
    }
}
function checkdateonhdkt() {
    var fromday = Xrm.Page.getAttribute("bsd_fromdatehdkt").getValue();
    var today = Xrm.Page.getAttribute("bsd_todatehdkt").getValue();
    if (fromday != null) {
        Xrm.Page.getControl("bsd_fromdatehdkt").clearNotification();
        var year = fromday.getFullYear() + "";
        if (fromday.getMonth() + 1 <= 9) {
            var month = "0" + (fromday.getMonth() + 1);
        }
        else {
            var month = (fromday.getMonth() + 1);
        }
        var dayy = fromday.getDate();
    }
    if (fromday == null) {
        Xrm.Page.getAttribute("bsd_todatehdkt").setValue(null);
        Xrm.Page.getControl("bsd_fromdatehdkt").setNotification("From date is not null!!!");
    }
    if (today != null && fromday != null) {
        var yearr = today.getFullYear();
        if (fromday.getMonth() + 1 <= 9) {
            var monthh = "0" + (today.getMonth() + 1);
        }
        else {
            var monthh = (today.getMonth() + 1);
        }
        var day = today.getDate();
        if (yearr < year) {
            Xrm.Page.getControl("bsd_todatehdkt").setNotification("To date is not smaller than from date");
        }
        if (yearr == year) {
            if (monthh < month) {
                Xrm.Page.getControl("bsd_todatehdkt").setNotification("To date is not smaller than from date");
                Xrm.Page.getAttribute("bsd_todatehdkt").setValue(null);
            }
            if (monthh == month) {
                if (day => dayy) {
                    Xrm.Page.getControl("bsd_todatehdkt").clearNotification();
                }
                if (day < dayy) {
                    Xrm.Page.getControl("bsd_todatehdkt").setNotification("To date is not smaller than from date");
                    Xrm.Page.getAttribute("bsd_todatehdkt").setValue(null);

                }
            }
            if (monthh > month) {
                Xrm.Page.getControl("bsd_todatehdkt").clearNotification();
            }
        }
        if (yearr > year) {
            Xrm.Page.getControl("bsd_todatehdkt").clearNotification();
        }
    }
}
function set_addressonload() {   
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    var day = Xrm.Page.getAttribute("bsd_fromdate").getValue();
    if (day != null) {
        var year = day.getFullYear() + "";
        if (day.getMonth() + 1 <= 9) {
            var month = "0" + (day.getMonth() + 1) + "";
        }
        else {
            var month = (day.getMonth() + 1) + "";
        }
        var dayy = day.getDate() + "";
        var dateFormat = year + "-" + month + "-" + dayy;
    }
    var xml1 = [];
    var xml2 = [];
    var xml4 = [];
    var xml7 = [];
    var xml8 = [];
    var xml9 = [];
    var xml10 = [];
    var xml11 = [];
    var xml12 = [];
    var xml13 = [];
    var xml14 = [];
    var xml16 = [];
    var xml17 = [];
    if (customer != null) {
        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName = "bsd_address";
        var viewDisplayName = "test";
        fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                    ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='bsd_addressid'>  " +
              "<cell name='bsd_name'   " + "width='200' />  " +
              "<cell name='createdon'    " + "width='100' />  " +
                "<cell name='bsd_purpose'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
        Xrm.Page.getControl("bsd_address").addCustomView(viewId, entityName, viewDisplayName, xml2.join(""), layoutXml, true);
        var viewId7 = "{EE499892-35E1-4D07-A1B7-BB69FE9BFD42}";
        var entityName7 = "pricelevel";
        var viewDisplayName7 = "test";
        xml7.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml7.push("<entity name='pricelevel'>");
        xml7.push("<attribute name='name' />");
        xml7.push("<attribute name='transactioncurrencyid' />");
        xml7.push("<attribute name='enddate' />");
        xml7.push("<attribute name='begindate' />");
        xml7.push("<attribute name='statecode' />");
        xml7.push("<attribute name='pricelevelid' />");
        xml7.push("<attribute name='createdon'/>");
        xml7.push("<order attribute='createdon' descending='false' />");
        xml7.push("<filter type='and'>");
        xml7.push("<condition attribute='bsd_account' operator='eq' uitype='account ' value='" + customer[0].id + "' />");
        xml7.push("<condition attribute='begindate' operator='on-or-before' value='" + dateFormat + "' />");
        xml7.push("</filter>");
        xml7.push("</entity>");
        xml7.push("</fetch>");
        var layoutXml7 = "<grid name='resultset' " + "object='1' " + "jump='pricelevelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                   "<row name='result'  " + "id='pricelevelid'>  " +
                   "<cell name='name'   " + "width='200' />  " +
                   "<cell name='createdon'    " + "width='100' />  " +
                     "<cell name='transactioncurrencyid'    " + "width='100' />  " +
                   "</row>   " +
                "</grid>   ";
        CrmFetchKit.Fetch(xml7.join(""), true).then(function (rs7) {
            if (rs7.length > 0) {
                Xrm.Page.getControl("pricelevelid").addCustomView(viewId7, entityName7, viewDisplayName7, xml7.join(""), layoutXml7, true);
            }
            if (rs7.length == 0) {
                xml8.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml8.push("<entity name='account'>");
                xml8.push("<attribute name='name' />");
                xml8.push("<attribute name='primarycontactid' />");
                xml8.push("<attribute name='telephone1' />");
                xml8.push("<attribute name='accountid' />");
                xml8.push("<attribute name='bsd_pricegroup' />");
                xml8.push("<order attribute='name' descending='false' />");
                xml8.push("<filter type='and'>");
                xml8.push("<condition attribute='accountid' operator='eq' uitype='account ' value='" + customer[0].id + "' />");
                xml8.push("</filter>");
                xml8.push("</entity>");
                xml8.push("</fetch>");
                CrmFetchKit.Fetch(xml8.join(""), true).then(function (rs8) {
                    if (rs8.length > 0) {
                        if (rs8[0].attributes.bsd_pricegroup != null) {
                            var viewId9 = "{EE499892-35E1-4D07-A1B7-BB69FE9BFD42}";
                            var entityName9 = "pricelevel";
                            var viewDisplayName9 = "test";
                            xml9.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                            xml9.push("<entity name='pricelevel'>");
                            xml9.push("<attribute name='name' />");
                            xml9.push("<attribute name='transactioncurrencyid' />");
                            xml9.push("<attribute name='enddate' />");
                            xml9.push("<attribute name='begindate' />");
                            xml9.push("<attribute name='statecode' />");
                            xml9.push("<attribute name='pricelevelid' />");
                            xml9.push("<attribute name='createdon'/>");
                            xml9.push("<order attribute='createdon' descending='false' />");
                            xml9.push("<filter type='and'>");
                            xml9.push("<condition attribute='bsd_pricegroup' operator='eq' uitype='bsd_pricegroups ' value='" + rs8[0].attributes.bsd_pricegroup.guid + "' />");
                            xml9.push("<condition attribute='begindate' operator='on-or-before' value='" + dateFormat + "' />");
                            xml9.push("</filter>");
                            xml9.push("</entity>");
                            xml9.push("</fetch>");
                            var layoutXml9 = "<grid name='resultset' " + "object='1' " + "jump='pricelevelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                "<row name='result'  " + "id='pricelevelid'>  " +
                                "<cell name='name'   " + "width='200' />  " +
                                "<cell name='createdon'    " + "width='100' />  " +
                                "<cell name='transactioncurrencyid'    " + "width='100' />  " +
                                "</row>   " +
                                "</grid>   ";
                            Xrm.Page.getControl("pricelevelid").addCustomView(viewId9, entityName9, viewDisplayName9, xml9.join(""), layoutXml9, true);
                        }
                    }
                },
                function (er) {
                    console.log(er.message)
                });
            }
        },
    function (er) {
        console.log(er.message)
    });
        var viewId = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
        var entityName = "contact";
        var viewDisplayName = "test";
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='contact'>");
        xml1.push("<attribute name='fullname'/>");
        xml1.push("<attribute name='telephone1'/>")
        xml1.push("<attribute name='contactid'/>");
        xml1.push("<order attribute='fullname' descending='true' />");
        xml1.push("<filter type='and'>");
        xml1.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + customer[0].id + "' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='contactid'>  " +
                         "<cell name='fullname'   " + "width='200' />  " +
                         "<cell name='telephone1'    " + "width='100' />  " +
                         "</row>   " +
                      "</grid>   ";
        Xrm.Page.getControl("bsd_contact").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
    }
    if (invoicenameaccount != null) {
        var viewId10 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName10 = "bsd_address";
        var viewDisplayName10 = "test";
        fetch(xml10, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                      ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, invoicenameaccount[0].id]);
        var layoutXml10 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
              "<row name='result'  " + "id='bsd_addressid'>  " +
              "<cell name='bsd_name'   " + "width='200' />  " +
              "<cell name='createdon'    " + "width='100' />  " +
                "<cell name='bsd_purpose'    " + "width='100' />  " +
              "</row>   " +
           "</grid>   ";
        Xrm.Page.getControl("bsd_addressinvoiceaccount").addCustomView(viewId10, entityName10, viewDisplayName10, xml10.join(""), layoutXml10, true);
    }
    var viewId11 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
    var entityName11 = "contact";
    var viewDisplayName11 = "test";
    fetch(xml11, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
        ["parentcustomerid", "statecode"], ["eq", "eq"], [0, "{6BE4B61C-13B9-E611-93F1-000C29D47EAB}", 1, "0"]);
    var layoutXml11 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='contactid'>  " +
                     "<cell name='fullname'   " + "width='200' />  " +
                     "<cell name='telephone1'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
    Xrm.Page.getControl("bsd_representative").addCustomView(viewId11, entityName11, viewDisplayName11, xml11.join(""), layoutXml11, true);
    var viewId16 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
    var entityName16 = "bsd_bankaccount";
    var viewDisplayName16 = "test";
    fetch(xml16, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
        ["bsd_account", "statecode"], ["eq", "eq"], [0, "{6BE4B61C-13B9-E611-93F1-000C29D47EAB}", 1, "0"]);
    var layoutXml16 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
    "<row name='result'  " + "id='bsd_bankaccountid'>  " +
    "<cell name='bsd_name'   " + "width='200' />  " +
    "<cell name='createdon'    " + "width='100' />  " +
    "<cell name='bsd_brand'    " + "width='100' />  " +
    "<cell name='bsd_bankaccount'    " + "width='100' />  " +
    "<cell name='bsd_account'    " + "width='100' />  " +
    "<cell name='bsd_bankgroup'    " + "width='100' />  " +
    "</row>   " +
    "</grid>   ";
    Xrm.Page.getControl("bsd_bankaccounttrader").addCustomView(viewId16, entityName16, viewDisplayName16, xml16.join(""), layoutXml16, true);

}
function setshiptoaddressonload() {

    var customerb = Xrm.Page.getAttribute("bsd_shiptoaccount").getValue();
    var xml2 = [];
    var xml3 = [];
    if (customerb != null) {
        var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName2 = "bsd_address";
        var viewDisplayName2 = "test";
        fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Delivery" + "%", 1, "0", 2, customerb[0].id]);
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='bsd_addressid'>  " +
                     "<cell name='bsd_name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_purpose'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";

        CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
            if (rs2.length > 0) {
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
            }
            if (rs2.length == 0) {
                var viewId3 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName3 = "bsd_address";
                var viewDisplayName3 = "test";
                fetch(xml3, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                       ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customerb[0].id]);
                var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                             "<row name='result'  " + "id='bsd_addressid'>  " +
                             "<cell name='bsd_name'   " + "width='200' />  " +
                             "<cell name='createdon'    " + "width='100' />  " +
                               "<cell name='bsd_purpose'    " + "width='100' />  " +
                             "</row>   " +
                          "</grid>   ";
                Xrm.Page.getControl("bsd_shiptoaddress").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
            }

        }, function (er) {
            console.log(er.message)
        });
    }
}
function hidebuttonprocess() {
    window.parent.document.getElementById("stageBackActionContainer").style.display = "none";
    window.parent.document.getElementById("stageBackActionContainer").style.visibility = "hidden";
    window.parent.document.getElementById("stageAdvanceActionContainer").style.display = "none";
    window.parent.document.getElementById("stageAdvanceActionContainer").style.visibility = "hidden";
    window.parent.document.getElementById("stageSetActiveActionContainer").style.display = "none";
    window.parent.document.getElementById("stageSetActiveActionContainer").style.visibility = "hidden";
}
//Author:Mr.Đăng
//Description: Disable when Delivery At The Port is No
function set_DisableDeliveryPort() {
    var deliveryport = getValue("bsd_deliveryattheport");
    if (deliveryport == "0") {
        setVisible(["bsd_port", "bsd_addressport"], false);
    }
    if (deliveryport == "1") {
        setVisible(["bsd_port", "bsd_addressport"], true);
        ChooseAccountPort();
    }
}
//Author:Mr.Đăng
//Description: Choose Account in Port
function ChooseAccountPort() {
    var fetchxml = ['<fetch mapping="logical" version="1.0">',
                    '<entity name="account">',
                    '<attribute name="name" />',
                    '<attribute name="accountid" />',
                    '<filter>',
                    '<condition attribute="bsd_accounttype" operator="eq" value="861450003" />',
                    '</filter>',
                    '</entity>',
                    '</fetch>'].join("");

    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
							   "<row name='result'  " + "id='accountid'>  " +
							   "<cell name='name'   " + "width='200' />  " +
							   "<cell name='createdon'    " + "width='100' />  " +
							   "</row>   " +
							"</grid>";
    getControl("bsd_port").addCustomView(getControl("bsd_port").getDefaultView(), "account", "Port", fetchxml, layoutXml, true);
    LoadAddressPort(false);
}
function AutoLoading() {
    if (formType() == 2) {
        LoadAddressPort(false);
    }
    else {
        clear_LoadAddressPort();
    }
}
//Author:Mr.Đăng
//Description: Load Address Port
function LoadAddressPort(result) {
    if (result != false) setNull("bsd_addressport");
    var accountport = getValue("bsd_port");
    if (accountport != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_address">',
                        '<attribute name="bsd_name" />',
                        '<attribute name="bsd_addressid" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + accountport[0].id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        CrmFetchKit.Fetch(fetchxml, false).then(function (rs) {
            console.log(rs);
            if (rs.length > 0) {
                var first = rs[0];
                if (getValue("bsd_addressport") == null) {
                    setValue("bsd_addressport", [{
                        id: first.Id,
                        name: first.attributes.bsd_name.value,
                        entityType: first.logicalName
                    }]
                   );
                }
            }

        }, function (er) { });
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_addressid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>";
        getControl("bsd_addressport").removePreSearch(presearch_LoadAddressPort);
        getControl("bsd_addressport").addCustomView(getControl("bsd_addressport").getDefaultView(), "bsd_address", "Address", fetchxml, layoutXml, true);
    }
    else if (result != false) {
        clear_LoadAddressPort();
    }
}
function clear_LoadAddressPort() {
    getControl("bsd_addressport").addPreSearch(presearch_LoadAddressPort);
}
function presearch_LoadAddressPort() {
    getControl("bsd_addressport").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function validation() {
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var invoicenameaccount = Xrm.Page.getAttribute("bsd_invoicenameaccount").getValue();
    if (customer == null) {
        Xrm.Page.getAttribute("bsd_address").setValue(null);
    }
    if (invoicenameaccount == null) {
        Xrm.Page.getAttribute("bsd_addressinvoiceaccount").setValue(null);
    }
}

//Author:Mr.phong
//Description:set value field exchange rate
function setvaluefieldexchangerateonload() {
    var a = [];
    a = get_value(["transactioncurrencyid", "exchangerate"]);
    var xml = [];
    if (a[0] != null && a[2] == null) {
        fetch(xml, "transactioncurrency", ["transactioncurrencyid", "currencyname", "isocurrencycode", "currencysymbol", "exchangerate", "currencyprecision"], "currencyname", false, null
           , ["transactioncurrencyid"], ["eq"], [0, a[0], 1]);
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(rs[0].attributes.exchangerate.value);
            }
            if (rs.length == 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(null);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
}
//Author:Mr.phong
//Description:set value field exchange rate
function setvaluefieldexchangerateonchange() {
    var a = [];
    a = get_value(["transactioncurrencyid"]);
    var xml = [];
    if (a[0] != null) {
        fetch(xml, "transactioncurrency", ["transactioncurrencyid", "currencyname", "isocurrencycode", "currencysymbol", "exchangerate", "currencyprecision"], "currencyname", false, null
           , ["transactioncurrencyid"], ["eq"], [0, a[0], 1]);
        CrmFetchKit.Fetch(xml.join(""), false).then(function (rs) {
            if (rs.length > 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(rs[0].attributes.exchangerate.value);
            }
            if (rs.length == 0) {
                Xrm.Page.getAttribute("exchangerate").setValue(null);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    if (a[0] == null) {
        Xrm.Page.getAttribute("exchangerate").setValue(null);
    }
}
function checkpricelist() {
    if (Xrm.Page.getAttribute("pricelevelid").getValue() != null) {
        Xrm.Page.getControl("pricelevelid").clearNotification();
    }
    if (Xrm.Page.getAttribute("pricelevelid").getValue() == null) {
        Xrm.Page.getControl("pricelevelid").setNotification("Price list is missing");
    }
}
//Author:Mr.Phong
//Description:appear or disappear section BHS
function appearordisappearsectionBHS() {
    debugger;
    var type = Xrm.Page.getAttribute("bsd_type").getValue();
    if (type == 861450001 || type == 861450002)
    {      
        setValue("bsd_type", 861450000);      
    }
    else {
        Xrm.Page.ui.tabs.get("information_tab").sections.get("section_bhs").setVisible(false);
        Xrm.Page.ui.tabs.get("information_tab").sections.get("section_packingandproductquality").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_summary").sections.get("section_contactinformation").setVisible(false);
        setVisible([ "bsd_positioncustomer", "bsd_dateauthorizedcustomer", "bsd_numberauthorizedcustomer"
            , "bsd_bankaccountcustomer", "bsd_brandcustomer", "bsd_bankgroupcustomer", "bsd_bankaccountnumbercustomer"], false);
    }
}
//Author:Mr.Phong
//Description:lay du lieu bank account,bank group,brand tu trader
function get_bankaccountnumberandbankgrouptrader() {
    var bankaccounttrader = Xrm.Page.getAttribute("bsd_bankaccounttrader").getValue();
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgrouptrader");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbertrader");
    var fieldbrand = Xrm.Page.getAttribute("bsd_brandtrader");
    var xml = [];
    if (bankaccounttrader != null) {
        //load bank account cua BHS            
        fetch(xml, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_bankaccountid", "statecode"], ["eq", "eq"], [0, bankaccounttrader[0].id, 1, "0"]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.bsd_bankgroup != null) {
                    fieldbankgroup.setValue([{
                        id: rs[0].attributes.bsd_bankgroup.guid,
                        name: rs[0].attributes.bsd_bankgroup.name,
                        entityType: rs[0].attributes.bsd_bankgroup.logicalName
                    }]);
                }
                if (rs[0].attributes.bsd_bankaccount != null) {
                    fieldbankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                }
                if (rs[0].attributes.bsd_brand != null) {
                    fieldbrand.setValue(rs[0].attributes.bsd_brand.value);
                }
            }
            else {
                fieldbankgroup.setValue(null);
                fieldbrand.setValue(null);
                fieldbankaccountnumber.setValue(null);
            }
        },
            function (er) {
                console.log(er.message)
            });
    }
    else {
        fieldbankgroup.setValue(null);
        fieldbrand.setValue(null);
        fieldbankaccountnumber.setValue(null);
    }
}

//Author:Mr.Phong
//Description:lay du lieu bank account,bank group,brand tu customer
function get_bankaccountnumberandbankgroupcustomer() {
    var fieldbankaccount = Xrm.Page.getAttribute("bsd_bankaccountcustomer").getValue();
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgroupcustomer");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbercustomer");
    var fieldbrand = Xrm.Page.getAttribute("bsd_brandcustomer");
    var xml = [];
    if (fieldbankaccount != null) {
        fetch(xml, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_bankaccountid"], ["eq"], [0, fieldbankaccount[0].id, 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                fieldbankgroup.setValue([{
                    id: rs[0].attributes.bsd_bankgroup.guid,
                    name: rs[0].attributes.bsd_bankgroup.name,
                    entityType: rs[0].attributes.bsd_bankgroup.logicalName
                }]);
                fieldbankaccountnumber.setValue(rs[0].attributes.bsd_bankaccount.value);
                fieldbrand.setValue(rs[0].attributes.bsd_brand.value);
            }
        },
      function (er) {
          console.log(er.message)
      });
    }
    else {
        fieldbankgroup.setValue(null);
        fieldbankaccountnumber.setValue(null);
        fieldbrand.setValue(null);

    }
}
//Author:Mr.Phong
//Description:lay thong tin chuc vu nguoi dai dien tu BHS
function get_thongtinchucvu() {
    var present = Xrm.Page.getAttribute("bsd_representative").getValue();
    var xml = [];
    if (present != null) {
        fetch(xml, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
              ["contactid"], ["eq"], [0, present[0].id, 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.jobtitle != null) {
                    Xrm.Page.getAttribute("bsd_position").setValue(rs[0].attributes.jobtitle.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_position").setValue(null);
                }
            }
        },
         function (er) {
             console.log(er.message)
         });
    }
    if (present == null) {
        Xrm.Page.getAttribute("bsd_position").setValue(null);
    }
}
//Author:Mr.Phong
//Description:lay thong tin chuc vu nguoi dai dien tu customer
function get_thongtinchucvucustomer() {
    var type = Xrm.Page.getAttribute("bsd_type").getValue();
    if (type == 861450001) {
        var present = Xrm.Page.getAttribute("bsd_contact").getValue();
        var xml = [];
        if (present != null) {
            fetch(xml, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                  ["contactid"], ["eq"], [0, present[0].id, 1]);
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    if (rs[0].attributes.jobtitle != null) {
                        Xrm.Page.getAttribute("bsd_positioncustomer").setValue(rs[0].attributes.jobtitle.value);
                    }
                }
            },
             function (er) {
                 console.log(er.message)
             });
        }
        if (present == null) {
            Xrm.Page.getAttribute("bsd_positioncustomer").setValue(null);
        }
    }
}
//Author:Mr.Phong
//Description:get thong tin khach hang HDKT
function get_customerinformationhdkt() {
    var fieldaddress = Xrm.Page.getAttribute("bsd_addresscustomer");
    var fieldpaymentterm = Xrm.Page.getAttribute("bsd_paymenttermscustomer");
    var fieldpaymentmethod = Xrm.Page.getAttribute("bsd_paymentmethodcustomer");
    var customer = Xrm.Page.getAttribute("bsd_customername").getValue();
    var fieldbankaccount = Xrm.Page.getAttribute("bsd_bankaccountcustomer");
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgroupcustomer");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbercustomer");
    var fieldbrand = Xrm.Page.getAttribute("bsd_brandcustomer");
    var fieldcontact = Xrm.Page.getAttribute("bsd_representativecustomer");
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    if (customer != null) {
        fetch(xml, "account", ["accountid", "name", "primarycontactid", "telephone1", "bsd_taxregistration", "fax", "bsd_paymentterm", "bsd_paymentmethod"], ["name"], false, null, ["accountid"], ["eq"], [0, customer[0].id]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.telephone1 != null) {
                    Xrm.Page.getAttribute("bsd_phonecustomer").setValue(rs[0].attributes.telephone1.value);
                }
                if (rs[0].attributes.telephone1 == null) {
                    Xrm.Page.getAttribute("bsd_phonecustomer").setValue(null);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxcodecustomer").setValue(rs[0].attributes.bsd_taxregistration.value);
                }
                if (rs[0].attributes.bsd_taxregistration == null) {
                    Xrm.Page.getAttribute("bsd_taxcodecustomer").setValue(null);
                }
                if (rs[0].attributes.fax != null) {
                    Xrm.Page.getAttribute("bsd_faxcustomer").setValue(rs[0].attributes.fax.value);
                }
                if (rs[0].attributes.fax == null) {
                    Xrm.Page.getAttribute("bsd_faxcustomer").setValue(null);
                }
                if (rs[0].attributes.bsd_paymentterm != null) {
                    fieldpaymentterm.setValue([{
                        id: rs[0].attributes.bsd_paymentterm.guid,
                        name: rs[0].attributes.bsd_paymentterm.name,
                        entityType: 'bsd_paymentterm'
                    }]);
                }
                if (rs[0].attributes.bsd_paymentterm == null) {
                    fieldpaymentterm.setValue(null);
                }
                if (rs[0].attributes.bsd_paymentmethod != null) {
                    fieldpaymentmethod.setValue([{
                        id: rs[0].attributes.bsd_paymentmethod.guid,
                        name: rs[0].attributes.bsd_paymentmethod.name,
                        entityType: 'bsd_methodofpayment'
                    }]);
                }
                if (rs[0].attributes.bsd_paymentmethod == null) {
                    fieldpaymentmethod.setValue(null);
                }
            }
        },
        function (er) {
            console.log(er.message)
        });
        var viewId = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName = "bsd_address";
        var viewDisplayName = "test";
        fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
            ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, customer[0].id]);
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                    "<row name='result'  " + "id='bsd_addressid'>  " +
                    "<cell name='bsd_name'   " + "width='200' />  " +
                    "<cell name='createdon'    " + "width='100' />  " +
                      "<cell name='bsd_account'    " + "width='100' />  " +
                    "</row>   " +
                 "</grid>   ";
        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
            //neu account co dia chi Business
            if (rs1.length > 0) {
                fieldaddress.setValue([{
                    id: rs1[0].Id,
                    name: rs1[0].attributes.bsd_name.value,
                    entityType: rs1[0].logicalName
                }]);
                Xrm.Page.getControl("bsd_addresscustomer").addCustomView(viewId, entityName, viewDisplayName, xml1.join(""), layoutXml, true);
            }
                //neu account ko co dia chi Business
            else {
                var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName1 = "bsd_address";
                var viewDisplayName1 = "test";
                fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                    ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, customer[0].id]);
                var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                            "<row name='result'  " + "id='bsd_addressid'>  " +
                            "<cell name='bsd_name'   " + "width='200' />  " +
                            "<cell name='createdon'    " + "width='100' />  " +
                              "<cell name='bsd_account'    " + "width='100' />  " +
                            "</row>   " +
                         "</grid>   ";
                Xrm.Page.getControl("bsd_addresscustomer").addCustomView(viewId1, entityName1, viewDisplayName1, xml2.join(""), layoutXml1, true);
            }
        },
        function (er) {
            console.log(er.message)
        });
        //load bank account cua customer      
        var viewId2 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
        var entityName2 = "bsd_bankaccount";
        var viewDisplayName2 = "test";
        fetch(xml3, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_account", "statecode"], ["eq", "eq"], [0, customer[0].id, 1, "0"]);
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
        "<row name='result'  " + "id='bsd_bankaccountid'>  " +
        "<cell name='bsd_name'   " + "width='200' />  " +
        "<cell name='createdon'    " + "width='100' />  " +
        "<cell name='bsd_brand'    " + "width='100' />  " +
        "<cell name='bsd_bankaccount'    " + "width='100' />  " +
        "<cell name='bsd_account'    " + "width='100' />  " +
        "<cell name='bsd_bankgroup'    " + "width='100' />  " +
        "</row>   " +
        "</grid>   ";
        CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
            //neu customer co bankaccount
            if (rs3.length > 0) {
                fieldbankaccount.setValue([{
                    id: rs3[0].Id,
                    name: rs3[0].attributes.bsd_name.value,
                    entityType: rs3[0].logicalName
                }]);
                fieldbankgroup.setValue([{
                    id: rs3[0].attributes.bsd_bankgroup.guid,
                    name: rs3[0].attributes.bsd_bankgroup.name,
                    entityType: rs3[0].attributes.bsd_bankgroup.logicalName
                }]);
                fieldbankaccountnumber.setValue(rs3[0].attributes.bsd_bankaccount.value);
                fieldbrand.setValue(rs3[0].attributes.bsd_brand.value);
                Xrm.Page.getControl("bsd_bankaccountcustomer").addCustomView(viewId2, entityName2, viewDisplayName2, xml3.join(""), layoutXml2, true);
            }
                //neu customer ko co bankaccount
            else {
                clear_LoadBankAccountCustomer();
            }
        },
            function (er) {
                console.log(er.message)
            });
        //load nguoi dai dien cua customer
        var viewId3 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
        var entityName3 = "contact";
        var viewDisplayName3 = "test";
        fetch(xml4, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
            ["parentcustomerid", "statecode"], ["eq", "eq"], [0, customer[0].id, 1, "0"]);
        var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='contactid'>  " +
                         "<cell name='fullname'   " + "width='200' />  " +
                         "<cell name='telephone1'    " + "width='100' />  " +
                         "</row>   " +
                      "</grid>   ";
        CrmFetchKit.Fetch(xml4.join(""), true).then(function (rs4) {
            //neu co customer co contact
            if (rs4.length > 0) {
                fieldcontact.setValue([{
                    id: rs4[0].Id,
                    name: rs4[0].attributes.fullname.value,
                    entityType: 'contact'
                }]);
                Xrm.Page.getControl("bsd_representativecustomer").addCustomView(viewId3, entityName3, viewDisplayName3, xml4.join(""), layoutXml3, true);
                fetch(xml5, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                ["fullname"], ["eq"], [0, rs4[0].attributes.fullname.value, 1]);
                CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                    if (rs5.length > 0) {
                        Xrm.Page.getAttribute("bsd_positioncustomer").setValue(rs5[0].attributes.jobtitle.value);
                    }
                    else {
                        Xrm.Page.getAttribute("bsd_position").setValue(null);
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
                //neu co customer ko co contact
            else {
                clear_Loadrepresentativecustomer();
            }
        },
            function (er) {
                console.log(er.message)
            });
    }
}
//Author:Mr.Phong
//Description:appear or disappear section BHS onload
function appearordisappearsectionBHSonload() {
    var type = Xrm.Page.getAttribute("bsd_type").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    var xml7 = [];
    var xml8 = [];
    var xml9 = [];
    //BHS  
    var fieldtrader = Xrm.Page.getAttribute("bsd_trader").getValue();
    Xrm.Page.getControl("bsd_trader").removePreSearch(presearch_Loadtrader);
    Xrm.Page.getControl("bsd_addresstrader").removePreSearch(presearch_LoadAddressTrader);
    Xrm.Page.getControl("bsd_bankaccounttrader").removePreSearch(presearch_LoadBankAccountTrader);
    Xrm.Page.getControl("bsd_representative").removePreSearch(presearch_Loadrepresentative);
    //customer   
    var fieldcustomer = Xrm.Page.getAttribute("customerid").getValue();
    var fieldcontact = Xrm.Page.getAttribute("bsd_contact");
    Xrm.Page.getControl("bsd_bankaccountcustomer").removePreSearch(presearch_LoadBankAccountCustomer);
    Xrm.Page.getControl("bsd_contact").removePreSearch(presearch_Loadrepresentativecustomer);
    if (type == 861450000 || type == 861450003)//type đơn hàng
    {
        Xrm.Page.ui.tabs.get("information_tab").sections.get("section_bhs").setVisible(false);
        Xrm.Page.ui.tabs.get("information_tab").sections.get("section_packingandproductquality").setVisible(false);
        Xrm.Page.ui.tabs.get("tab_summary").sections.get("section_contactinformation").setVisible(false);
        
        setVisible(["bsd_contact", "bsd_positioncustomer", "bsd_dateauthorizedcustomer", "bsd_numberauthorizedcustomer"
            , "bsd_bankaccountcustomer", "bsd_brandcustomer", "bsd_bankgroupcustomer", "bsd_bankaccountnumbercustomer"], false);
    }
    if (type == 861450001 || type == 861450002)//type HĐKT 
    {
        Xrm.Page.ui.tabs.get("information_tab").sections.get("section_bhs").setVisible(true);
        Xrm.Page.ui.tabs.get("tab_summary").sections.get("section_contactinformation").setVisible(true);
        Xrm.Page.data.entity.attributes.forEach(function (attribute, index) {
           var control = Xrm.Page.getControl(attribute.getName());        
            if (control != null ) {
                control.setDisabled(true);
                setDisabled(["bsd_bankaccountcustomer", "bsd_contact", "bsd_dateauthorizedcustomer", "bsd_numberauthorizedcustomer"
                    , "bsd_contracttype", "bsd_daystartcontract", "bsd_dayendcontract", "bsd_trader", "bsd_bankaccounttrader", "bsd_addresstrader"
                    , "bsd_representative", "bsd_dateauthorized", "bsd_numberauthorized", "bsd_packingspecification", "bsd_principleoftally","bsd_pono"], false);
                setVisible(["bsd_ordername", "ordernumber", "bsd_economiccontract"], false);
            }              
        });
        if (fieldtrader != null) {
            //load thong tin cua BHS
            var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
            var entityName = "account";
            var viewDisplayName = "Account View";
            fetch(xml, "account", ["accountid", "name", "primarycontactid", "telephone1", "bsd_taxregistration", "fax"], ["name"], false, null,
                ["bsd_accounttype", "statecode"], ["eq", "eq"], [0, "861450005", 1, "0"]);
            var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='accountid'>  " +
                      "<cell name='name'   " + "width='200' />  " +
                     "<cell name='createdon'    " + "width='100' />  " +
                      "<cell name='primarycontactid'    " + "width='100' />  " +
                      "</row>   " +
                   "</grid>   ";
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    Xrm.Page.getControl("bsd_trader").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
                }
                else {
                    clear_Loadtrader();
                }
            },
                function (er) {
                    console.log(er.message)
                });
            Xrm.Page.getControl("bsd_trader").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            //load thong tin dia chi cua BHS
            var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
            var entityName1 = "bsd_address";
            var viewDisplayName1 = "Address View";
            fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null, ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, fieldtrader[0].id]);
            var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                       "<row name='result'  " + "id='bsd_addressid'>  " +
                       "<cell name='bsd_name'   " + "width='200' />  " +
                      "<cell name='createdon'    " + "width='100' />  " +
                       "<cell name='bsd_account'    " + "width='100' />  " +
                       "</row>   " +
                    "</grid>   ";
            CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
                if (rs1.length > 0) {
                    Xrm.Page.getControl("bsd_addresstrader").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
                }
                else {
                    var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                    var entityName2 = "bsd_address";
                    var viewDisplayName2 = "Address View";
                    fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
                   ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, fieldtrader[0].id]);
                    var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_addressid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";

                    CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                        if (rs2.length > 0) {
                            Xrm.Page.getControl("bsd_addresstrader").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
                        }
                        else {
                            clear_LoadAddressTrader();
                        }
                    },
                function (er) {
                    console.log(er.message)
                });
                }
            },
                function (er) {
                    console.log(er.message)
                });
            //load bank account cua BHS      
            var viewId3 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
            var entityName3 = "bsd_bankaccount";
            var viewDisplayName3 = "Bank Account View";
            fetch(xml3, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
                ["bsd_account", "statecode"], ["eq", "eq"], [0, fieldtrader[0].id, 1, "0"]);
            var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
            "<row name='result'  " + "id='bsd_bankaccountid'>  " +
            "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
            "<cell name='bsd_brand'    " + "width='100' />  " +
            "<cell name='bsd_bankaccount'    " + "width='100' />  " +
            "<cell name='bsd_account'    " + "width='100' />  " +
            "<cell name='bsd_bankgroup'    " + "width='100' />  " +
            "</row>   " +
            "</grid>   ";
            CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
                if (rs3.length > 0) {
                    Xrm.Page.getControl("bsd_bankaccounttrader").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
                }
                else {
                    clear_LoadBankAccountTrader();
                }
            },
                function (er) {
                    console.log(er.message)
                });
            //load nguoi dai dien      
            var viewId5 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
            var entityName5 = "contact";
            var viewDisplayName5 = "Contact View";
            xml5.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml5.push("<entity name='contact'>");
            xml5.push("<attribute name='fullname'/>");
            xml5.push("<attribute name='telephone1'/>")
            xml5.push("<attribute name='contactid'/>");
            xml5.push("<order attribute='fullname' descending='true' />");
            xml5.push("<filter type='and'>");
            xml5.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + fieldtrader[0].id + "' />");
            xml5.push("</filter>");
            xml5.push("</entity>");
            xml5.push("</fetch>");
            var layoutXml5 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='contactid'>  " +
                     "<cell name='fullname'   " + "width='200' />  " +
                     "<cell name='telephone1'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
            CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                if (rs5.length > 0) {
                    Xrm.Page.getControl("bsd_representative").addCustomView(viewId5, entityName5, viewDisplayName5, xml5.join(""), layoutXml5, true);
                }
                else {
                    clear_Loadrepresentative();
                }
            },
        function (er) {
            console.log(er.message)
        });
            //load bank account cua customer      
            var viewId6 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
            var entityName6 = "bsd_bankaccount";
            var viewDisplayName6 = "Bank Account View";
            fetch(xml6, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
                ["bsd_account", "statecode"], ["eq", "eq"], [0, fieldcustomer[0].id, 1, "0"]);
            var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
            "<row name='result'  " + "id='bsd_bankaccountid'>  " +
            "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
            "<cell name='bsd_brand'    " + "width='100' />  " +
            "<cell name='bsd_bankaccount'    " + "width='100' />  " +
            "<cell name='bsd_account'    " + "width='100' />  " +
            "<cell name='bsd_bankgroup'    " + "width='100' />  " +
            "</row>   " +
            "</grid>   ";
            CrmFetchKit.Fetch(xml6.join(""), true).then(function (rs6) {
                if (rs6.length > 0) {
                    Xrm.Page.getControl("bsd_bankaccountcustomer").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
                }
                else {
                    clear_LoadBankAccountCustomer();
                }
            },
                function (er) {
                    console.log(er.message)
                });
            //load nguoi dai dien customer         
            var viewId8 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
            var entityName8 = "contact";
            var viewDisplayName8 = "Contact View";
            xml8.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml8.push("<entity name='contact'>");
            xml8.push("<attribute name='fullname'/>");
            xml8.push("<attribute name='telephone1'/>")
            xml8.push("<attribute name='contactid'/>");
            xml8.push("<order attribute='fullname' descending='true' />");
            xml8.push("<filter type='and'>");
            xml8.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + fieldcustomer[0].id + "' />");
            xml8.push("</filter>");
            xml8.push("</entity>");
            xml8.push("</fetch>");
            var layoutXml8 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                     "<row name='result'  " + "id='contactid'>  " +
                     "<cell name='fullname'   " + "width='200' />  " +
                     "<cell name='telephone1'    " + "width='100' />  " +
                     "</row>   " +
                  "</grid>   ";
            CrmFetchKit.Fetch(xml8.join(""), true).then(function (rs8) {
                if (rs8.length > 0) {
                    if (fieldcontact.getValue() == null) {
                        fieldcontact.setValue([{
                            id: '{' + rs8[0].Id.toUpperCase() + '}',
                            name: rs8[0].attributes.fullname.value,
                            entityType: rs8[0].logicalName
                        }]);
                    }
                    if (Xrm.Page.getAttribute("bsd_positioncustomer").getValue() == null) {
                        fetch(xml9, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                        ["contactid"], ["eq"], [0, rs8[0].Id, 1]);
                        CrmFetchKit.Fetch(xml9.join(""), true).then(function (rs9) {
                            if (rs9.length > 0) {
                                Xrm.Page.getAttribute("bsd_positioncustomer").setValue(rs9[0].attributes.jobtitle.value);
                            }
                            else {
                                Xrm.Page.getAttribute("bsd_positioncustomer").setValue(null);
                            }
                        },
                    function (er) {
                        console.log(er.message)
                    });
                    }
                    Xrm.Page.getControl("bsd_contact").addCustomView(viewId8, entityName8, viewDisplayName8, xml8.join(""), layoutXml8, true);
                }
                else {
                    clear_Loadrepresentativecustomer();
                }
            },
        function (er) {
            console.log(er.message)
        });
        }
    }
}
//Author:Mr.Phong
//Description:action change filed trader
function action_changefieldtrader() {
    debugger;
    var trader = Xrm.Page.getAttribute("bsd_trader").getValue();
    var fieldaddress = Xrm.Page.getAttribute("bsd_addresstrader");
    var fieldbankaccount = Xrm.Page.getAttribute("bsd_bankaccounttrader");
    var fieldbankgroup = Xrm.Page.getAttribute("bsd_bankgrouptrader");
    var fieldbankaccountnumber = Xrm.Page.getAttribute("bsd_bankaccountnumbertrader");
    var fieldbrand = Xrm.Page.getAttribute("bsd_brandtrader");
    var fieldcontact = Xrm.Page.getAttribute("bsd_representative");
    Xrm.Page.getControl("bsd_addresstrader").removePreSearch(presearch_LoadAddressTrader);
    Xrm.Page.getControl("bsd_bankaccounttrader").removePreSearch(presearch_LoadBankAccountTrader);
    Xrm.Page.getControl("bsd_representative").removePreSearch(presearch_Loadrepresentative);
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    var xml5 = [];
    var xml6 = [];
    var xml7 = [];
    var xml8 = [];
    if (trader != null) {
        //load thong tin cua BHS  
        fetch(xml, "account", ["accountid", "name", "primarycontactid", "telephone1", "bsd_taxregistration", "fax"]
            , ["name"], false, null, ["accountid"], ["eq"]
            , [0, trader[0].id, 1]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.telephone1 != null) {
                    Xrm.Page.getAttribute("bsd_telephonetrader").setValue(rs[0].attributes.telephone1.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_telephonetrader").setValue(null);
                }
                if (rs[0].attributes.bsd_taxregistration != null) {
                    Xrm.Page.getAttribute("bsd_taxregistrationtrader").setValue(rs[0].attributes.bsd_taxregistration.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_taxregistrationtrader").setValue(null);
                }
                if (rs[0].attributes.fax != null) {
                    Xrm.Page.getAttribute("bsd_faxtrader").setValue(rs[0].attributes.fax.value);
                }
                else {
                    Xrm.Page.getAttribute("bsd_faxtrader").setValue(null);
                }
            }
        },
                 function (er) {
                     console.log(er.message)
                 });
        //load thong tin dia chi cua BHS       
        var viewId1 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
        var entityName1 = "bsd_address";
        var viewDisplayName1 = "Address View";
        fetch(xml1, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
            ["bsd_purpose_tmpvalue", "statecode", "bsd_account"], ["like", "eq", "eq"], [0, "%" + "Business" + "%", 1, "0", 2, trader[0].id]);
        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
             "<row name='result'  " + "id='bsd_addressid'>  " +
             "<cell name='bsd_name'   " + "width='200' />  " +
            "<cell name='createdon'    " + "width='100' />  " +
             "<cell name='bsd_account'    " + "width='100' />  " +
             "</row>   " +
          "</grid>   ";
        CrmFetchKit.Fetch(xml1.join(""), true).then(function (rs1) {
            if (rs1.length > 0) {
                if (rs1[0].attributes.bsd_name != null) {
                    fieldaddress.setValue([{
                        id: rs1[0].Id,
                        name: rs1[0].attributes.bsd_name.value,
                        entityType: rs1[0].logicalName
                    }]);
                }
                else {
                    fieldaddress.setValue(null);
                }
                Xrm.Page.getControl("bsd_addresstrader").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
            }
            else {
                var viewId2 = "{0662F9A1-43DC-45E2-9C30-CA1426C33347}";
                var entityName2 = "bsd_address";
                var viewDisplayName2 = "Address View";
                fetch(xml2, "bsd_address", ["bsd_addressid", "bsd_name", "createdon", "bsd_account"], ["bsd_name"], false, null,
               ["statecode", "bsd_account"], ["eq", "eq"], [0, "0", 1, fieldtrader[0].id]);
                var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                               "<row name='result'  " + "id='bsd_addressid'>  " +
                               "<cell name='bsd_name'   " + "width='200' />  " +
                               "<cell name='createdon'    " + "width='100' />  " +
                               "</row>   " +
                            "</grid>   ";

                CrmFetchKit.Fetch(xml2.join(""), true).then(function (rs2) {
                    if (rs2.length > 0) {
                        Xrm.Page.getControl("bsd_addresstrader").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
                    }
                    else {
                        clear_LoadAddressTrader();
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
        },
         function (er) {
             console.log(er.message)
         });
        //load bank account cua BHS      
        var viewId3 = "{9FA8A5B8-F599-4E64-A891-EA926E6BC66B}";
        var entityName3 = "bsd_bankaccount";
        var viewDisplayName3 = "Bank Account Customer View";
        fetch(xml3, "bsd_bankaccount", ["bsd_bankaccountid", "bsd_name", "createdon", "bsd_brand", "bsd_bankaccount", "bsd_account", "bsd_bankgroup"], ["bsd_name"], false, null,
            ["bsd_account", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
        var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_bankaccountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
        "<row name='result'  " + "id='bsd_bankaccountid'>  " +
        "<cell name='bsd_name'   " + "width='200' />  " +
        "<cell name='createdon'    " + "width='100' />  " +
        "<cell name='bsd_brand'    " + "width='100' />  " +
        "<cell name='bsd_bankaccount'    " + "width='100' />  " +
        "<cell name='bsd_account'    " + "width='100' />  " +
        "<cell name='bsd_bankgroup'    " + "width='100' />  " +
        "</row>   " +
        "</grid>   ";
        CrmFetchKit.Fetch(xml3.join(""), true).then(function (rs3) {
            if (rs3.length > 0) {
                if (rs3[0].attributes.bsd_name != null) {
                    fieldbankaccount.setValue([{
                        id: rs3[0].Id,
                        name: rs3[0].attributes.bsd_name.value,
                        entityType: rs3[0].logicalName
                    }]);
                    fieldbankgroup.setValue([{
                        id: rs3[0].attributes.bsd_bankgroup.guid,
                        name: rs3[0].attributes.bsd_bankgroup.name,
                        entityType: rs3[0].attributes.bsd_bankgroup.logicalName
                    }]);
                    fieldbankaccountnumber.setValue(rs3[0].attributes.bsd_bankaccount.value);
                    fieldbrand.setValue(rs3[0].attributes.bsd_brand.value);
                }
                else {
                    fieldbankaccount.setValue(null);
                    fieldbankgroup.setValue(null);
                    fieldbankaccountnumber.setValue(null);
                    fieldbrand.setValue(null);
                }
                Xrm.Page.getControl("bsd_bankaccounttrader").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
            }
            else {
                fieldbankaccount.setValue(null);
                fieldbrand.setValue(null);
                fieldbankaccountnumber.setValue(null);
                clear_LoadBankAccountTrader();
            }
        },
            function (er) {
                console.log(er.message)
            });
        //load nguoi dai dien

        xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> ");
        xml4.push("<entity name='account'>");
        xml4.push("<attribute name='name' />");
        xml4.push("<attribute name='primarycontactid'/>");
        xml4.push("<attribute name='telephone1'/>");
        xml4.push("<attribute name='accountid'/>");
        xml4.push("<order attribute='name' descending='false'/> ");
        xml4.push("<link-entity name='contact' from='contactid' to='primarycontactid' alias='ad'>");
        xml4.push("<attribute name='jobtitle'/>");
        xml4.push("<filter type='and'>");
        xml4.push("<condition attribute='parentcustomerid' operator='eq' uitype='account' value='" + trader[0].id + "'/>");
        xml4.push("</filter>");
        xml4.push("</link-entity>");
        xml4.push("</entity>");
        xml4.push("</fetch>");
        CrmFetchKit.Fetch(xml4.join(""), true).then(function (rs4) {
            if (rs4.length > 0) {
                fieldcontact.setValue([{
                    id: rs4[0].attributes.primarycontactid.guid,
                    name: rs4[0].attributes.primarycontactid.name,
                    entityType: 'contact'
                }]);
                var viewId4 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityName4 = "contact";
                var viewDisplayName4 = "Contact View";
                fetch(xml8, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                   ["parentcustomerid", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
                var layoutXml4 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                         "<row name='result'  " + "id='contactid'>  " +
                         "<cell name='fullname'   " + "width='200' />  " +
                         "<cell name='telephone1'    " + "width='100' />  " +
                         "</row>   " +
                      "</grid>   ";             
                        Xrm.Page.getControl("bsd_representative").addCustomView(viewId4, entityName4, viewDisplayName4, xml8.join(""), layoutXml4, true);                              
                fetch(xml5, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
           ["contactid"], ["eq"], [0, rs4[0].attributes.primarycontactid.guid, 1]);
                CrmFetchKit.Fetch(xml5.join(""), true).then(function (rs5) {
                    if (rs5.length > 0) {
                        Xrm.Page.getAttribute("bsd_position").setValue(rs5[0].attributes.jobtitle.value);
                    }
                    else {
                        Xrm.Page.getAttribute("bsd_position").setValue(null);
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
            else {
                var viewId6 = "{A2D479C5-53E3-4C69-ADDD-802327E67A0D}";
                var entityName6 = "contact";
                var viewDisplayName6 = "test";
                fetch(xml6, "contact", ["fullname", "telephone1", "contactid"], ["fullname"], false, null,
                    ["parentcustomerid", "statecode"], ["eq", "eq"], [0, trader[0].id, 1, "0"]);
                var layoutXml6 = "<grid name='resultset' " + "object='1' " + "jump='contactid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                 "<row name='result'  " + "id='contactid'>  " +
                                 "<cell name='fullname'   " + "width='200' />  " +
                                 "<cell name='telephone1'    " + "width='100' />  " +
                                 "</row>   " +
                              "</grid>   ";
                CrmFetchKit.Fetch(xml6.join(""), true).then(function (rs6) {
                    if (rs6.length > 0) {
                        fieldcontact.setValue([{
                            id: rs6[0].Id,
                            name: rs6[0].attributes.fullname.value,
                            entityType: 'contact'
                        }]);
                        Xrm.Page.getControl("bsd_representative").addCustomView(viewId6, entityName6, viewDisplayName6, xml6.join(""), layoutXml6, true);
                        fetch(xml7, "contact", ["fullname", "telephone1", "contactid", "jobtitle"], ["fullname"], false, null,
                        ["fullname"], ["eq"], [0, rs6[0].attributes.fullname.value, 1]);
                        CrmFetchKit.Fetch(xml7.join(""), true).then(function (rs7) {
                            if (rs7.length > 0) {
                                Xrm.Page.getAttribute("bsd_position").setValue(rs7[0].attributes.jobtitle.value);
                            }
                            else {
                                Xrm.Page.getAttribute("bsd_position").setValue(null);
                            }
                        },
                    function (er) {
                        console.log(er.message)
                    });
                    }
                    else {
                        Xrm.Page.getAttribute("bsd_position").setValue(null);
                        clear_Loadrepresentative();
                    }
                },
            function (er) {
                console.log(er.message)
            });
            }
        },
            function (er) {
                console.log(er.message)
            });
    }
    else {
        fieldaddress.setValue(null);
        fieldbankaccount.setValue(null);
        fieldbankgroup.setValue(null);
        fieldbankaccountnumber.setValue(null);
        fieldbrand.setValue(null);
        fieldcontact.setValue(null);
        set_value(["bsd_telephonetrader", "bsd_taxregistrationtrader", "bsd_faxtrader", "bsd_position"], null);
        clear_LoadAddressTrader();
        clear_LoadBankAccountTrader();
        clear_Loadrepresentative();
    }
}
function filter_customerprincipalcontract() {
    var fromdate = Xrm.Page.getAttribute("bsd_fromdate").getValue();
    var fieldprincipalcontract = Xrm.Page.getAttribute("bsd_principalcontract");
    if (fromdate != null) {
        var year = fromdate.getFullYear() + "";
        if (fromdate.getMonth() + 1 <= 9) {
            var month = "0" + (fromdate.getMonth() + 1) + "";
        }
        else {
            var month = (fromdate.getMonth() + 1) + "";
        }
        var dayy = fromdate.getDate();
        var dateFormat = year + "-" + month + "-" + dayy;
    }
    var customer = Xrm.Page.getAttribute("customerid").getValue();
    var xml = [];
    if (fromdate != null && customer != null) {
        fetch(xml, "bsd_principalcontract", ["bsd_principalcontractid", "bsd_name", "createdon", "bsd_code", "bsd_fromdate", "bsd_todate"], ["bsd_name"], false, null,
             ["bsd_account", "bsd_fromdate"], ["eq", "on-or-before"], [0, customer[0].id, 1, dateFormat]);
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                var i
                for (i = 0; i < rs.length; i++) {
                    var enddate = rs[i].attributes.bsd_todate.value;
                    var yearr = enddate.getFullYear() + "";
                    if (enddate.getMonth() + 1 <= 9) {
                        var monthh = "0" + (enddate.getMonth() + 1) + "";
                    }
                    else {
                        var monthh = (enddate.getMonth() + 1) + "";
                    }
                    var dayyy = enddate.getDate() + "";
                    if (year <= yearr) {
                        if (month < monthh) {
                            if (fieldprincipalcontract.getValue() == null) {
                                fieldprincipalcontract.setValue([{
                                    id: '{' + rs[i].Id.toUpperCase() + '}',
                                    name: rs[i].attributes.bsd_name.value,
                                    entityType: rs[i].logicalName
                                }]);
                                Xrm.Page.getAttribute("bsd_principalcontractno").setValue(rs[i].attributes.bsd_code.value);
                            }
                            Xrm.Page.getAttribute("bsd_hidehdkt").setValue("khong");
                            break;
                        }
                        if (month == monthh) {
                            if (dayy < dayyy) {
                                if (fieldprincipalcontract.getValue() == null) {
                                    fieldprincipalcontract.setValue([{
                                        id: '{' + rs[i].Id.toUpperCase() + '}',
                                        name: rs[i].attributes.bsd_name.value,
                                        entityType: rs[i].logicalName
                                    }]);
                                    Xrm.Page.getAttribute("bsd_principalcontractno").setValue(rs[i].attributes.bsd_code.value);
                                }
                                Xrm.Page.getAttribute("bsd_hidehdkt").setValue("khong");
                                break;
                            }
                        }
                    }
                    Xrm.Page.getAttribute("bsd_hidehdkt").setValue("co");
                }
                //if (Xrm.Page.getAttribute("bsd_hidehdkt").getValue() == "co") {
                //    fieldprincipalcontract.setValue(null);
                //    Xrm.Page.getAttribute("bsd_principalcontractno").setValue(null);
                //}

            }
        },
            function (er) {
                console.log(er.message)
            });
    }
}

//Author:Mr.Phong
//Description:set value field pricelist when change field from date
function clear_LoadAddressTrader() {
    getControl("bsd_addresstrader").addPreSearch(presearch_LoadAddressTrader);
}
function presearch_LoadAddressTrader() {
    getControl("bsd_addresstrader").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_LoadBankAccountTrader() {
    getControl("bsd_bankaccounttrader").addPreSearch(presearch_LoadBankAccountTrader);
}
function presearch_LoadBankAccountTrader() {
    getControl("bsd_bankaccounttrader").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Loadrepresentative() {
    getControl("bsd_representative").addPreSearch(presearch_Loadrepresentative);
}
function presearch_Loadrepresentative() {
    getControl("bsd_representative").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_shippingpricelistname() {
    getControl("bsd_shippingpricelistname").addPreSearch(presearch_shippingpricelistname);
}
function presearch_shippingpricelistname() {
    getControl("bsd_shippingpricelistname").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_LoadBankAccountCustomer() {
    getControl("bsd_bankaccountcustomer").addPreSearch(presearch_LoadBankAccountCustomer);
}
function presearch_LoadBankAccountCustomer() {
    getControl("bsd_bankaccountcustomer").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Loadrepresentativecustomer() {
    getControl("bsd_contact").addPreSearch(presearch_Loadrepresentativecustomer);
}
function presearch_Loadrepresentativecustomer() {
    getControl("bsd_contact").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function clear_Loadtrader() {
    getControl("bsd_tradersup").addPreSearch(presearch_Loadtrader);
}
function presearch_Loadtrader() {
    getControl("bsd_tradersup").addPreSearch(presearch_Loadtrader);
}
function clear_LoadShiptoaddress() {
    getControl("bsd_shiptoaddress").addPreSearch(presearch_LoadShiptoaddress);
}
function presearch_LoadShiptoaddress() {
    getControl("bsd_shiptoaddress").addCustomFilter("<filter type='and'><condition attribute='createdon' operator='null'/></filter>");
}
function modified_on_change() {
    shipping_change(false);
    Xrm.Page.data.save();
}

function change_daystartcontract() {
    //create by nudo on 2017-03-15
    clearNotification("bsd_daystartcontract");
    clearNotification("bsd_dayendcontract");
    var from = getValue("bsd_daystartcontract");
    var to = getValue("bsd_dayendcontract");
    if (from != null && to != null) {
        if (from > to) {
            setNotification("bsd_daystartcontract", "The Day Start Contract cannot occur before the Day End Contract");
        }
    }
    if (to != null) {
        //var orderid = getValue("salesorderid");
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                      '<entity name="salesorder">',
                      '  <attribute name="name" />',
                      '  <attribute name="salesorderid" />',
                      '  <order attribute="name" descending="false" />',
                      '  <filter type="and">',
                      '    <condition attribute="salesorderid" operator="eq" uitype="salesorder" value="' + getId() + '" />',
                      '  </filter>',
                      '  <link-entity name="quote" from="quoteid" to="quoteid" alias="ab">',
                      '    <attribute name="effectiveto" />',
                      '  </link-entity>',
                      '</entity>',
                    '</fetch>'].join('');
        console.log(xml);
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {

                var effectiveto = rs[0].getValue("ab.effectiveto");
                console.log(effectiveto);
                if (effectiveto != null && effectiveto > to) {

                    setNotification("bsd_dayendcontract", "The Day End Start Contract to cannot occur after the Day Effective");
                }

            }

        });
    }
}

function SetRequire_EconomicContract() {
    //create by nudo on 2017-03-15
    var type = getValue("bsd_type");
    if (type == 861450000 || type == 861450003)//type đơn hàng
    {
        setRequired("bsd_contracttype", "none");
        setRequired("bsd_daystartcontract", "none");
        setRequired("bsd_dayendcontract", "none");
    }
    else {
        setRequired("bsd_contracttype", "required");
        setRequired("bsd_daystartcontract", "required");
        setRequired("bsd_dayendcontract", "required");
    }


}


//Description:change status when stage final onload
function changestatuswhenstagefinalonload() {
    debugger;
    stagename = Xrm.Page.data.process.getActiveStage().getId();
    var formState = Xrm.Page.ui.getFormType();
    var nhanvien = getValue("bsd_tennhanvienduyet");
    var truongphong = getValue("bsd_tentruongphongduyet");
    var numberprocess = getValue("bsd_numberprocess");
    var numberprocessctq = getValue("bsd_numberprocessctq");
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    for (var i = 0; i < rnames.length; i++) {
        //stage la:finnish
        if (stagename == "cee35035-2ba2-a068-3669-dd52ee471c37") {
            Xrm.Page.getAttribute("statuscode").setValue(861450002);//status:Cấp Thẩm Quyền Duyệt
        }
        //nhan vien va stage la:staff
        if (rnames[i] == "Nhân Viên" && stagename == "ccd2ca63-f0be-4acd-bbf5-3d0a59c03304") {
            Xrm.Page.getAttribute("statuscode").setValue(861450000);//status:nhan vien lap
        }
        //nhan vien va stage la:staff va fied ten nhan vien lap !null
        if (rnames[i] == "Nhân Viên" && stagename == "ccd2ca63-f0be-4acd-bbf5-3d0a59c03304" && nhanvien != null) {
            Xrm.Page.getAttribute("statuscode").setValue(861450004);//status:Trưởng phòng không duyệt
        }
        //nhan vien va stage la:manager va fied ten nhan vien lap !null
        if (rnames[i] == "Nhân Viên" && stagename == "b80a51c4-4782-2899-d060-5488b453c388") {
            Xrm.Page.getAttribute("statuscode").setValue(861450003);//status:Chờ Trưởng Phòng Duyệt
        }
        //truong phong va stage la:manager
        if (rnames[i] == "Trưởng Phòng" && stagename == "b80a51c4-4782-2899-d060-5488b453c388") {
            Xrm.Page.getAttribute("statuscode").setValue(861450001);//status:Trưởng Phòng Duyệt
        }
        //truong phong va stage la:staff va fied ten nhan vien lap !null
        if (rnames[i] == "Trưởng Phòng" && stagename == "ccd2ca63-f0be-4acd-bbf5-3d0a59c03304" && nhanvien != null) {
            Xrm.Page.getAttribute("statuscode").setValue(861450004);//status:Trưởng phòng không duyệt
        }
        //truong phong va stage la:staff va fied ten nhan vien lap =null
        if (rnames[i] == "Trưởng Phòng" && stagename == "ccd2ca63-f0be-4acd-bbf5-3d0a59c03304" && nhanvien == null && numberprocess == false) {
            Xrm.Page.getAttribute("statuscode").setValue(861450001);//status:Trưởng Phòng Duyệt
            Xrm.Page.getAttribute("bsd_numberprocess").setValue(true);
            setValue("bsd_tentruongphongduyet", [{
                name: Xrm.Page.context.getUserName(),
                id: Xrm.Page.context.getUserId(),
                type: "8"
            }]);
        }
        //Cấp Thẩm Quyền va stage la:staff va fied ten nhan vien lap =null va fied ten truong phong =null
        if (rnames[i] == "Cấp Thẩm Quyền" && stagename == "ccd2ca63-f0be-4acd-bbf5-3d0a59c03304" && nhanvien == null && truongphong == null && numberprocessctq == false) {
            Xrm.Page.getAttribute("statuscode").setValue(861450002);//status:Cấp Thẩm Quyền Duyệt
            Xrm.Page.getAttribute("bsd_numberprocessctq").setValue(true);
            setValue("bsd_tencapthamquyenduyet", [{
                name: Xrm.Page.context.getUserName(),
                id: Xrm.Page.context.getUserId(),
                type: "8"
            }]);
        }
        if (rnames[i] == "Trưởng Phòng" && stagename == "ccd2ca63-f0be-4acd-bbf5-3d0a59c03304" && nhanvien == null && numberprocess == false) {
            Xrm.Page.getAttribute("statuscode").setValue(861450001);//status:Trưởng Phòng Duyệt
            Xrm.Page.getAttribute("bsd_numberprocess").setValue(true);
            setValue("bsd_tentruongphongduyet", [{
                name: Xrm.Page.context.getUserName(),
                id: Xrm.Page.context.getUserId(),
                type: "8"
            }]);
        }
        //truong phong va stage la:Chief Officer 
        if (rnames[i] == "Trưởng Phòng" && stagename == "6a4bad44-d7c0-56ae-12d5-b634400b2258") {
            Xrm.Page.getAttribute("statuscode").setValue(861450005);//status:Chờ Cấp Thẩm Quyền Duyệt
        }
        //Trưởng Phòng va stage la:manager va fied ten truong phong lap !null
        if (rnames[i] == "Trưởng Phòng" && stagename == "b80a51c4-4782-2899-d060-5488b453c388" && truongphong != null) {
            Xrm.Page.getAttribute("statuscode").setValue(861450006);//status:Cấp Thẩm Quyền Không Duyệt
        }
        //Cấp Thẩm Quyền va stage la:Chief Officer 
        if (rnames[i] == "Cấp Thẩm Quyền" && stagename == "6a4bad44-d7c0-56ae-12d5-b634400b2258") {
            Xrm.Page.getAttribute("statuscode").setValue(861450002);//status:Cấp Thẩm Quyền Duyệt
        }
    }
    Xrm.Page.data.save();
}
function switchProcess() {
    var formState = Xrm.Page.ui.getFormType();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var formState = Xrm.Page.ui.getFormType();
    for (var i = 0; i < rnames.length; i++) {
        if (rnames[i] = "Trưởng Phòng") {
            var formState = Xrm.Page.ui.getFormType();
            stageId = Xrm.Page.data.process.getActiveStage().getId();
            Xrm.Page.data.process.setActiveStage(stageId, switchProcessEnd);
        }
    }
}
function switchProcessEnd() {
    stagename = Xrm.Page.data.process.getActiveStage().getId();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var numberprocess = Xrm.Page.getAttribute("bsd_numberprocess").getValue();
    for (var i = 0; i < rnames.length; i++) {
        //stage:staff
        if (stagename == "ccd2ca63-f0be-4acd-bbf5-3d0a59c03304" && rnames[i] == "Trưởng Phòng" && numberprocess == true) {
            Xrm.Page.data.process.moveNext(function (e) {
                if (e == "success") {
                    Xrm.Page.ui.refreshRibbon();
                }
            });
        }
    }

}
function switchProcessctq() {
    var formState = Xrm.Page.ui.getFormType();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var formState = Xrm.Page.ui.getFormType();
    for (var i = 0; i < rnames.length; i++) {
        if (rnames[i] = "Cấp Thẩm Quyền") {
            var formState = Xrm.Page.ui.getFormType();
            stageId = Xrm.Page.data.process.getActiveStage().getId();
            Xrm.Page.data.process.setActiveStage(stageId, switchProcessEndctq);
        }
    }
}
function switchProcessEndctq() {
    stagename = Xrm.Page.data.process.getActiveStage().getId();
    var rnames = GetRoleName(Xrm.Page.context.getUserRoles());
    var numberprocessctq = Xrm.Page.getAttribute("bsd_numberprocessctq").getValue();
    for (var i = 0; i < rnames.length; i++) {
        //stage:staff
        if (stagename == "ccd2ca63-f0be-4acd-bbf5-3d0a59c03304" && rnames[i] == "Cấp Thẩm Quyền" && numberprocessctq == true) {
            Xrm.Page.data.process.moveNext(function (e) {
                if (e == "success") {
                    Xrm.Page.ui.refreshRibbon();
                    Xrm.Page.data.process.moveNext(function (e) {
                        if (e == "success") {
                            Xrm.Page.ui.refreshRibbon();
                        }
                    });

                }
            });

        }
    }

}
function GetRoleName(roleIds) {
    var serverUrl = location.protocol + "//" + location.host + "/" + Xrm.Page.context.getOrgUniqueName();
    var odataSelect = serverUrl + "/XRMServices/2011/OrganizationData.svc" + "/" + "RoleSet?$select=Name";
    var cdn = "";
    if (roleIds != null && roleIds.length > 0) {
        for (var i = 0; i < roleIds.length; i++) {
            if (i == roleIds.length - 1)
                cdn += "RoleId eq guid'" + roleIds[i] + "'";
            else
                cdn += "RoleId eq guid'" + roleIds[i] + "' or ";
        }
        if (cdn.length > 0)
            cdn = "&$filter=" + cdn;
        odataSelect += cdn;
        var roleName = [];
        $.ajax(
            {
                type: "GET",
                async: false,
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                url: odataSelect,
                beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
                success: function (data, textStatus, XmlHttpRequest) {
                    if (data.d != null && data.d.results != null) {
                        var len = data.d.results.length;
                        for (var k = 0; k < len; k++)
                            roleName.push(data.d.results[k].Name);
                    }
                },
                error: function (XmlHttpRequest, textStatus, errorThrown) { alert('OData Select Failed: ' + textStatus + errorThrown + odataSelect); }
            }
        );
    }
    return roleName;
}
function setdisableallfield() {
    debugger;
    var userid = Xrm.Page.context.getUserId();
    var owner = Xrm.Page.getAttribute("ownerid").getValue();
    var stagename = Xrm.Page.data.process.getActiveStage().getId();
    var statuscode = Xrm.Page.getAttribute("statuscode").getValue();
    //stage:manager va status:Chờ Trưởng phòng duyệt
    if (stagename == "b80a51c4-4782-2899-d060-5488b453c388" && statuscode == 861450003) {
        setTimeout(DisabledForm, 1);
    }
    //stage:manager va status:Chờ Cấp Thẩm Quyền Duyệt
    if (stagename == "b80a51c4-4782-2899-d060-5488b453c388" && statuscode == 861450005) {
        setTimeout(DisabledForm, 1);
    }
        //stage:staff va status:Nhân Viên Lập va nguoi tao ra khac vs nguoi dang dang nhap
    else if (stagename == "ccd2ca63-f0be-4acd-bbf5-3d0a59c03304" && statuscode == 861450000 && userid != owner[0].id) {
        setTimeout(DisabledForm, 1);
    }
        //stage:manager va status:Trưởng Phòng Duyệt va nguoi tao ra khac vs nguoi dang dang nhap
    else if (stagename == "b80a51c4-4782-2899-d060-5488b453c388" && statuscode == 861450001 && userid != owner[0].id) {
        setTimeout(DisabledForm, 1);
    }
        //stage:Cấp Thẩm Quyền Duyệt va status:Cấp Thẩm Quyền Duyệt va nguoi tao ra khac vs nguoi dang dang nhap
    else if (stagename == "6a4bad44-d7c0-56ae-12d5-b634400b2258" && statuscode == 861450002 && userid != owner[0].id) {
        setTimeout(DisabledForm, 1);
    }
        //stage:Cấp Thẩm Quyền Duyệt va status:Chờ Cấp Thẩm Quyền Duyệt va nguoi tao ra khac vs nguoi dang dang nhap
    else if (stagename == "6a4bad44-d7c0-56ae-12d5-b634400b2258" && statuscode == 861450005 && userid != owner[0].id) {
        setTimeout(DisabledForm, 1);
    }
        //stage:finnish
    else if (stagename == "cee35035-2ba2-a068-3669-dd52ee471c37") {
        setTimeout(DisabledForm, 1);
    }
    Xrm.Page.data.save();
}
//author:Mr.Phong
function hide_button() {
    try {
        window.parent.document.getElementById("stageBackActionContainer").style.display = "none";
    } catch (e) {

    }
    try {
        window.parent.document.getElementById("stageBackActionContainer").style.visibility = "hidden";
    } catch (e) {

    }
    try {
        window.parent.document.getElementById("stageAdvanceActionContainer").style.display = "none";
    } catch (e) {

    }
    try {
        window.parent.document.getElementById("stageAdvanceActionContainer").style.visibility = "hidden";
    } catch (e) {

    }
    try {
        window.parent.document.getElementById("stageSetActiveActionContainer").style.display = "none";
    } catch (e) {

    }
    try {
        window.parent.document.getElementById("stageSetActiveActionContainer").style.visibility = "hidden";
    } catch (e) {

    }

}