// 22.03.2017 Đăng: Autoload
function Autoload() {
    setDisabled_ParentCustomer();
}
function set_fullname() {
    debugger;

    if (Xrm.Page.getAttribute("bsd_fullname").getValue() != null && Xrm.Page.getAttribute("bsd_fullname").getValue() != "undefined") {
        {
            //Xrm.Page.getAttribute("firstname").setValue("1");
            Xrm.Page.getAttribute("lastname").setValue(Xrm.Page.getAttribute("bsd_fullname").getValue());
            Xrm.Page.getAttribute("lastname").setRequiredLevel("none");
            Xrm.Page.getAttribute("fullname").setRequiredLevel("none");
        }
    }
}

// JavaScript source code

// Global
var diachi = null;
var phuong = null;
var quanhuyen = null;
var tinhthanh = null;
var quocgia = null;

function OnLoad() {
    debugger;

    //Load dia chi
    if (Xrm.Page.getAttribute("bsd_diachi").getValue() != null)
        MakeAddress();
}

// Quoc Gia OnChange
function Onchange_QuocGia() {

    quanhuyen = "bsd_quan_thuongtru";
    tinhthanh = "bsd_thanhphotinhtieubang_thuongtru";
    Xrm.Page.getAttribute(tinhthanh).setValue(null);
    Xrm.Page.getAttribute(quanhuyen).setValue(null);
    MakeAddress();
}

// Tinh Thanh OnChange
function Onchange_TinhThanh() {
    Xrm.Page.getAttribute("bsd_quan_thuongtru").setValue(null);
    MakeAddress();
}

// Copy tinh thanh vao Address Composite
function MakeAddress() {
    debugger;
    //UnBlock 
    Xrm.Page.ui.controls.get("bsd_diachi").setDisabled(false);

    var tmp = null;
    var fulltmp = null;
    var sonha = null;
    var fulladdress = null;

    address = "address1_line1";
    province = "address1_stateorprovince";
    country = "address1_country";

    // field
    fulladdress = "bsd_diachi";
    sonha = "bsd_duongsonha_thuongtru";
    phuong = "bsd_phuong_thuongtru";
    quanhuyen = "bsd_quan_thuongtru";
    tinhthanh = "bsd_thanhphotinhtieubang_thuongtru";
    quocgia = "bsd_quocgialanhtho_thuongtru";

    //Duong & so nha
    if (Xrm.Page.getAttribute(sonha).getValue()) {
        tmp = Xrm.Page.getAttribute(sonha).getValue();
    }

    //phuong  
    if (Xrm.Page.getAttribute(phuong).getValue()) {
        if (tmp != "")
            tmp += ", " + Xrm.Page.getAttribute(phuong).getValue();
    }

    //quanhuyen  
    if (Xrm.Page.getAttribute(quanhuyen).getValue()) {
        if (tmp != null)
            tmp += ", " + Xrm.Page.getAttribute(quanhuyen).getValue()[0].name;
    }

    fulltmp = tmp;
    Xrm.Page.getAttribute(address).setValue(tmp);

    //tinh thanh
    if (Xrm.Page.getAttribute(tinhthanh).getValue()) {
        tmp = Xrm.Page.getAttribute(tinhthanh).getValue()[0].name;
        Xrm.Page.getAttribute(province).setValue(tmp);
        fulltmp += ", " + tmp;

    }

    //quoc gia
    if (Xrm.Page.getAttribute(quocgia).getValue()) {
        tmp = Xrm.Page.getAttribute(quocgia).getValue()[0].name;
        Xrm.Page.getAttribute(country).setValue(tmp);
        fulltmp += ", " + tmp;
    }
    Xrm.Page.getAttribute(fulladdress).setValue(fulltmp);
    Xrm.Page.ui.controls.get("bsd_diachi").setDisabled(true);
}

function hide_Industrial() {
    debugger;
    var loai = Xrm.Page.getAttribute("bsd_industry_vl").getValue();

    if (loai != null) {        
        Xrm.Page.getControl("bsd_beverage").setVisible(loai.indexOf(1) != -1);
        Xrm.Page.getControl("bsd_food").setVisible(loai.indexOf(2) != -1);
        Xrm.Page.getControl("bsd_milkicecream").setVisible(loai.indexOf(3) != -1);
        Xrm.Page.getControl("bsd_confectionary").setVisible(loai.indexOf(4) != -1);       
       
    }
}
//Author:Mr.Phong
//Description:get Business phone from account
function get_businessphonefromaccount() {
    var account = Xrm.Page.getAttribute("parentcustomerid").getValue();
    var xml = [];
    if (account != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='account'>");
        xml.push("<attribute name='name'/>");
        xml.push("<attribute name='primarycontactid' />")
        xml.push("<attribute name='telephone1' />");
        xml.push("<attribute name='accountid' />");
        xml.push("<order attribute='name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='accountid' uitype='account' operator='eq' value='" + account[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0 && rs[0].attributes.telephone1 != null) {
                Xrm.Page.getAttribute("telephone1").setValue(rs[0].attributes.telephone1.value);
            }
            else {
                Xrm.Page.getAttribute("telephone1").setValue(null);
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    else
    {
        Xrm.Page.getAttribute("telephone1").setValue(null);
    }
}
//Author:Mr.Phong
//Description:set require or not 
function set_requireornot()
{
    var conpanyname = Xrm.Page.getAttribute("parentcustomerid").getValue();
    var contact = Xrm.Page.getAttribute("bsd_factorycontactid").getValue();
    var xml = [];
    if (conpanyname != null && contact==null)
    {
        Xrm.Page.getAttribute("parentcustomerid").setRequiredLevel("required");
        Xrm.Page.getAttribute("bsd_factorycontactid").setRequiredLevel("none");
        Xrm.Page.ui.controls.get("bsd_factorycontactid").setVisible(false);
    }
    if (contact != null && conpanyname==null) {
        Xrm.Page.getAttribute("parentcustomerid").setRequiredLevel("none");
        Xrm.Page.getAttribute("bsd_factorycontactid").setRequiredLevel("required");
        Xrm.Page.ui.controls.get("parentcustomerid").setVisible(true);
        if (conpanyname==null) {
            xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml.push("<entity name='bsd_factory'>");
            xml.push("<attribute name='bsd_factoryid' />");
            xml.push("<attribute name='bsd_name'/>");
            xml.push("<attribute name='createdon'/>");
            xml.push("<attribute name='bsd_account'/>");
            xml.push("<order attribute='bsd_name' descending='false' />");
            xml.push("<filter type='and' >");
            xml.push("<condition attribute='bsd_factoryid' operator='eq' uitype='bsd_factory' value='" + contact[0].id + "' />");
            xml.push("</filter>");
            xml.push("</entity>");
            xml.push("</fetch>");
            CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
                if (rs.length > 0) {
                    Xrm.Page.getAttribute("parentcustomerid").setValue([{
                        id: rs[0].attributes.bsd_account.guid,
                        name: rs[0].attributes.bsd_account.name,
                        entityType: rs[0].attributes.bsd_account.logicalName
                    }]);
                }
                if (rs.length == 0) {
                    Xrm.Page.getAttribute("parentcustomerid").setValue(null);
                }
            },
                    function (er) {
                        console.log(er.message)
                    });
        }    
    }
    if (conpanyname == null && contact == null)
    {
        Xrm.Page.getAttribute("parentcustomerid").setRequiredLevel("none");
        Xrm.Page.getAttribute("bsd_factorycontactid").setRequiredLevel("none");
        Xrm.Page.ui.controls.get("parentcustomerid").setVisible(true);
        Xrm.Page.ui.controls.get("bsd_factorycontactid").setVisible(true);
    }
}
// 22.03.2017 Đăng: setDisabled Account
function setDisabled_ParentCustomer() {
    var customer = getValue("parentcustomerid");
    if (customer != null) {
        setDisabled("parentcustomerid", true);
    }
}