﻿// 22.03.2017 Đăng: Autoload
function Autoload() {
    setDisabled_Customer();
}

//Author:Mr.phong
//Desciption:filter segment follow channel
function filtersegmentfollowchannel() {
    var channel = Xrm.Page.getAttribute("bsd_accountgroupp").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    if (channel != null) {
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='bsd_accountgroupchannel'>");
        xml1.push("<attribute name='bsd_accountgroupchannelid'/>");
        xml1.push("<attribute name='bsd_name' />")
        xml1.push("<attribute name='createdon' />");
        xml1.push("<attribute name='statecode' />");
        xml1.push("<attribute name='bsd_accountgroup' />");
        xml1.push("<attribute name='bsd_channel' />");
        xml1.push("<order attribute='bsd_name' descending='false' />");
        xml1.push("<filter type='and' >");
        xml1.push("<condition attribute='bsd_accountgroupchannelid' operator='eq' uitype='bsd_accountgroupchannel' value='" + channel[0].id + "' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
            if (rs1.length > 0) {
                var viewId = "{80ED8A04-4BA7-4845-B5AB-67B53A1822BD}";
                var entityName = "bsd_channelsegment";
                var viewDisplayName = "test";
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml.push("<entity name='bsd_channelsegment'>");
                xml.push("<attribute name='bsd_channelsegmentid'/>");
                xml.push("<attribute name='bsd_name' />")
                xml.push("<attribute name='createdon' />");
                xml.push("<attribute name='statecode' />");
                xml.push("<attribute name='bsd_segment' />");
                xml.push("<attribute name='bsd_channel' />");
                xml.push("<order attribute='bsd_name' descending='false' />");
                xml.push("<filter type='and' >");
                xml.push("<condition attribute='bsd_channel' operator='eq' uitype='bsd_industriallevel1' value='" + rs1[0].attributes.bsd_channel.guid + "' />");
                xml.push("<condition attribute='statecode' operator='eq' value='0' />");
                xml.push("</filter>");
                xml.push("</entity>");
                xml.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_channelsegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_channelsegmentid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                Xrm.Page.getControl("bsd_segmentt").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
                Xrm.Page.getAttribute("bsd_segmentt").setValue(null);
            }
        }, function (er) {
            console.log(er.message)
        });
        var viewId2 = "{1A73B586-16F8-4478-9F60-D82FA897AF46}";
        var entityName2 = "bsd_typesegment";
        var viewDisplayName2 = "test";
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml2.push("<entity name='bsd_typesegment'>");
        xml2.push("<attribute name='bsd_typesegmentid'/>");
        xml2.push("<attribute name='bsd_name' />")
        xml2.push("<attribute name='createdon' />");
        xml2.push("<attribute name='bsd_type' />");
        xml2.push("<attribute name='statecode' />");
        xml2.push("<attribute name='bsd_segment' />");
        xml2.push("<order attribute='bsd_name' descending='false' />");
        xml2.push("<filter type='and' >");
        xml2.push("<condition attribute='createdon' operator='null' />");
        xml2.push("</filter>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_typesegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_typesegmentid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_typee").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
        Xrm.Page.getAttribute("bsd_typee").setValue(null);
    }
    if (channel == null) {
        var viewId = "{80ED8A04-4BA7-4845-B5AB-67B53A1822BD}";
        var entityName = "bsd_channelsegment";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_channelsegment'>");
        xml.push("<attribute name='bsd_channelsegmentid'/>");
        xml.push("<attribute name='bsd_name' />")
        xml.push("<attribute name='createdon' />");
        xml.push("<attribute name='statecode' />");
        xml.push("<attribute name='bsd_segment' />");
        xml.push("<attribute name='bsd_channel' />");
        xml.push("<order attribute='bsd_name' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='createdon' operator='null' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_channelsegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_channelsegmentid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_segmentt").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        Xrm.Page.getAttribute("bsd_segmentt").setValue(null);
        var viewId2 = "{1A73B586-16F8-4478-9F60-D82FA897AF46}";
        var entityName2 = "bsd_typesegment";
        var viewDisplayName2 = "test";
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml2.push("<entity name='bsd_typesegment'>");
        xml2.push("<attribute name='bsd_typesegmentid'/>");
        xml2.push("<attribute name='bsd_name' />")
        xml2.push("<attribute name='createdon' />");
        xml2.push("<attribute name='bsd_type' />");
        xml2.push("<attribute name='statecode' />");
        xml2.push("<attribute name='bsd_segment' />");
        xml2.push("<order attribute='bsd_name' descending='false' />");
        xml2.push("<filter type='and' >");
        xml2.push("<condition attribute='createdon' operator='null' />");
        xml2.push("</filter>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_typesegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_typesegmentid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_typee").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
        Xrm.Page.getAttribute("bsd_typee").setValue(null);
    }
}
//Author:Mr.phong
//Desciption:filter type follow segment
function filtertypefollowchannel() {
    var segment = Xrm.Page.getAttribute("bsd_segmentt").getValue();
    var xml = [];
    var xml1 = [];
    if (segment != null) {
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='bsd_channelsegment'>");
        xml1.push("<attribute name='bsd_name' />")
        xml1.push("<attribute name='createdon' />");
        xml1.push("<attribute name='statecode' />");
        xml1.push("<attribute name='bsd_segment' />");
        xml1.push("<attribute name='bsd_channel' />");
        xml1.push("<attribute name='bsd_channelsegmentid' />");
        xml1.push("<order attribute='bsd_name' descending='false' />");
        xml1.push("<filter type='and' >");
        xml1.push("<condition attribute='bsd_channelsegmentid' operator='eq' uitype='bsd_channelsegment' value='" + segment[0].id + "' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
            if (rs1.length > 0) {
                var viewId = "{1A73B586-16F8-4478-9F60-D82FA897AF46}";
                var entityName = "bsd_typesegment";
                var viewDisplayName = "test";
                xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml.push("<entity name='bsd_typesegment'>");
                xml.push("<attribute name='bsd_typesegmentid'/>");
                xml.push("<attribute name='bsd_name' />")
                xml.push("<attribute name='createdon' />");
                xml.push("<attribute name='bsd_type' />");
                xml.push("<attribute name='statecode' />");
                xml.push("<attribute name='bsd_segment' />");
                xml.push("<order attribute='bsd_name' descending='false' />");
                xml.push("<filter type='and' >");
                xml.push("<condition attribute='bsd_segment' operator='eq' uitype='bsd_industriallevel2' value='" + rs1[0].attributes.bsd_segment.guid + "' />");
                xml.push("<condition attribute='statecode' operator='eq' value='0' />");
                xml.push("</filter>");
                xml.push("</entity>");
                xml.push("</fetch>");
                var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_typesegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_typesegmentid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                Xrm.Page.getControl("bsd_typee").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
            }
        }, function (er) {
            console.log(er.message)
        });
        Xrm.Page.getAttribute("bsd_typee").setValue(null);
    }
    if (segment == null) {
        var entityName2 = "bsd_typesegment";
        var viewDisplayName2 = "test";
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml2.push("<entity name='bsd_typesegment'>");
        xml2.push("<attribute name='bsd_typesegmentid'/>");
        xml2.push("<attribute name='bsd_name' />")
        xml2.push("<attribute name='createdon' />");
        xml2.push("<attribute name='bsd_type' />");
        xml2.push("<attribute name='statecode' />");
        xml2.push("<attribute name='bsd_segment' />");
        xml2.push("<order attribute='bsd_name' descending='false' />");
        xml2.push("<filter type='and' >");
        xml2.push("<condition attribute='createdon' operator='null' />");
        xml2.push("</filter>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_typesegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_typesegmentid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_typee").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
        Xrm.Page.getAttribute("bsd_typee").setValue(null);
    }
}
//Author:Mr.Phong
//Description:validation field type
function set_validation() {
    var channel = Xrm.Page.getAttribute("bsd_industriallevel1").getValue();
    var segment = Xrm.Page.getAttribute("bsd_industriallevel2").getValue();
    if (channel == null && segment == null) {
        Xrm.Page.getAttribute("bsd_type").setValue(null);
    }
    if (channel != null && segment == null) {
        Xrm.Page.getAttribute("bsd_type").setValue(null);
    }
}
//Author:Mr.Phong
//Description:filter channel follow account group
function filterchannelfollowaccountgroup() {
    var accountgroup = Xrm.Page.getAttribute("bsd_accountgroup").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    if (accountgroup != null) {
        var viewId = "{D9EA3132-950F-44F8-811E-156E8191B945}";
        var entityName = "bsd_accountgroupchannel";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_accountgroupchannel'>");
        xml.push("<attribute name='statecode'/>");
        xml.push("<attribute name='bsd_channel' />");
        xml.push("<attribute name='bsd_accountgroup' />");
        xml.push("<attribute name='bsd_name' />");
        xml.push("<attribute name='bsd_accountgroupchannelid' />");
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_accountgroup' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='bsd_accountgroup' operator='eq' uitype='bsd_accountgroup' value='" + accountgroup[0].id + "' />");
        xml.push("<condition attribute='statecode' operator='eq' value='0' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_accountgroupchannelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_accountgroupchannelid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_accountgroupp").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        Xrm.Page.getAttribute("bsd_accountgroupp").setValue(null);
        var viewId1 = "{80ED8A04-4BA7-4845-B5AB-67B53A1822BD}";
        var entityName1 = "bsd_channelsegment";
        var viewDisplayName1 = "test";
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='bsd_channelsegment'>");
        xml1.push("<attribute name='bsd_channelsegmentid'/>");
        xml1.push("<attribute name='bsd_name' />")
        xml1.push("<attribute name='createdon' />");
        xml1.push("<attribute name='statecode' />");
        xml1.push("<attribute name='bsd_segment' />");
        xml1.push("<attribute name='bsd_channel' />");
        xml1.push("<order attribute='bsd_name' descending='false' />");
        xml1.push("<filter type='and' >");
        xml1.push("<condition attribute='createdon' operator='null' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_channelsegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_channelsegmentid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_segmentt").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
        Xrm.Page.getAttribute("bsd_segmentt").setValue(null);
        var viewId2 = "{1A73B586-16F8-4478-9F60-D82FA897AF46}";
        var entityName2 = "bsd_typesegment";
        var viewDisplayName2 = "test";
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml2.push("<entity name='bsd_typesegment'>");
        xml2.push("<attribute name='bsd_typesegmentid'/>");
        xml2.push("<attribute name='bsd_name' />")
        xml2.push("<attribute name='createdon' />");
        xml2.push("<attribute name='bsd_type' />");
        xml2.push("<attribute name='statecode' />");
        xml2.push("<attribute name='bsd_segment' />");
        xml2.push("<order attribute='bsd_name' descending='false' />");
        xml2.push("<filter type='and' >");
        xml2.push("<condition attribute='createdon' operator='null' />");
        xml2.push("</filter>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_typesegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_typesegmentid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_typee").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
        Xrm.Page.getAttribute("bsd_typee").setValue(null);
    }
    if (accountgroup == null) {
        var viewId = "{D9EA3132-950F-44F8-811E-156E8191B945}";
        var entityName = "bsd_accountgroupchannel";
        var viewDisplayName = "test";
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='bsd_accountgroupchannel'>");
        xml.push("<attribute name='statecode'/>");
        xml.push("<attribute name='bsd_channel' />");
        xml.push("<attribute name='bsd_accountgroup' />");
        xml.push("<attribute name='bsd_name' />");
        xml.push("<attribute name='bsd_accountgroupchannelid' />");
        xml.push("<attribute name='createdon' />");
        xml.push("<order attribute='bsd_accountgroup' descending='false' />");
        xml.push("<filter type='and' >");
        xml.push("<condition attribute='createdon' operator='null' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_accountgroupchannelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_accountgroupchannelid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_accountgroupp").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
        Xrm.Page.getAttribute("bsd_accountgroupp").setValue(null);
        var viewId1 = "{80ED8A04-4BA7-4845-B5AB-67B53A1822BD}";
        var entityName1 = "bsd_channelsegment";
        var viewDisplayName1 = "test";
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='bsd_channelsegment'>");
        xml1.push("<attribute name='bsd_channelsegmentid'/>");
        xml1.push("<attribute name='bsd_name' />")
        xml1.push("<attribute name='createdon' />");
        xml1.push("<attribute name='statecode' />");
        xml1.push("<attribute name='bsd_segment' />");
        xml1.push("<attribute name='bsd_channel' />");
        xml1.push("<order attribute='bsd_name' descending='false' />");
        xml1.push("<filter type='and' >");
        xml1.push("<condition attribute='createdon' operator='null' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_channelsegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_channelsegmentid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_segmentt").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
        Xrm.Page.getAttribute("bsd_segmentt").setValue(null);
        var viewId2 = "{1A73B586-16F8-4478-9F60-D82FA897AF46}";
        var entityName2 = "bsd_typesegment";
        var viewDisplayName2 = "test";
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml2.push("<entity name='bsd_typesegment'>");
        xml2.push("<attribute name='bsd_typesegmentid'/>");
        xml2.push("<attribute name='bsd_name' />")
        xml2.push("<attribute name='createdon' />");
        xml2.push("<attribute name='bsd_type' />");
        xml2.push("<attribute name='statecode' />");
        xml2.push("<attribute name='bsd_segment' />");
        xml2.push("<order attribute='bsd_name' descending='false' />");
        xml2.push("<filter type='and' >");
        xml2.push("<condition attribute='createdon' operator='null' />");
        xml2.push("</filter>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_typesegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_typesegmentid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_typee").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
        Xrm.Page.getAttribute("bsd_typee").setValue(null);
    }
}
function domesticonload() {
    var channel = Xrm.Page.getAttribute("bsd_accountgroupp").getValue();
    var segment = Xrm.Page.getAttribute("bsd_segmentt").getValue();
    var accountgroup = Xrm.Page.getAttribute("bsd_accountgroup").getValue();
    var xml = [];
    var xml1 = [];
    var xml2 = [];
    var xml3 = [];
    var xml4 = [];
    if (channel != null)
    {      
            xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
            xml1.push("<entity name='bsd_accountgroupchannel'>");
            xml1.push("<attribute name='bsd_accountgroupchannelid'/>");
            xml1.push("<attribute name='bsd_name' />")
            xml1.push("<attribute name='createdon' />");
            xml1.push("<attribute name='statecode' />");
            xml1.push("<attribute name='bsd_accountgroup' />");
            xml1.push("<attribute name='bsd_channel' />");
            xml1.push("<order attribute='bsd_name' descending='false' />");
            xml1.push("<filter type='and' >");
            xml1.push("<condition attribute='bsd_accountgroupchannelid' operator='eq' uitype='bsd_accountgroupchannel' value='" + channel[0].id + "' />");
            xml1.push("</filter>");
            xml1.push("</entity>");
            xml1.push("</fetch>");
            CrmFetchKit.Fetch(xml1.join(""), false).then(function (rs1) {
                if (rs1.length > 0) {
                    var viewId = "{80ED8A04-4BA7-4845-B5AB-67B53A1822BD}";
                    var entityName = "bsd_channelsegment";
                    var viewDisplayName = "test";
                    xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                    xml.push("<entity name='bsd_channelsegment'>");
                    xml.push("<attribute name='bsd_channelsegmentid'/>");
                    xml.push("<attribute name='bsd_name' />")
                    xml.push("<attribute name='createdon' />");
                    xml.push("<attribute name='statecode' />");
                    xml.push("<attribute name='bsd_segment' />");
                    xml.push("<attribute name='bsd_channel' />");
                    xml.push("<order attribute='bsd_name' descending='false' />");
                    xml.push("<filter type='and' >");
                    xml.push("<condition attribute='bsd_channel' operator='eq' uitype='bsd_industriallevel1' value='" + rs1[0].attributes.bsd_channel.guid + "' />");
                    xml.push("<condition attribute='statecode' operator='eq' value='0' />");
                    xml.push("</filter>");
                    xml.push("</entity>");
                    xml.push("</fetch>");
                    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_channelsegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                       "<row name='result'  " + "id='bsd_channelsegmentid'>  " +
                                       "<cell name='bsd_name'   " + "width='200' />  " +
                                       "<cell name='createdon'    " + "width='100' />  " +
                                       "</row>   " +
                                    "</grid>   ";
                    Xrm.Page.getControl("bsd_segmentt").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);                  
                }
            }, function (er) {
                console.log(er.message)
            });
    }
    if (channel == null)
    {
        var viewId1 = "{80ED8A04-4BA7-4845-B5AB-67B53A1822BD}";
        var entityName1 = "bsd_channelsegment";
        var viewDisplayName1 = "test";
        xml1.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml1.push("<entity name='bsd_channelsegment'>");
        xml1.push("<attribute name='bsd_channelsegmentid'/>");
        xml1.push("<attribute name='bsd_name' />")
        xml1.push("<attribute name='createdon' />");
        xml1.push("<attribute name='statecode' />");
        xml1.push("<attribute name='bsd_segment' />");
        xml1.push("<attribute name='bsd_channel' />");
        xml1.push("<order attribute='bsd_name' descending='false' />");
        xml1.push("<filter type='and' >");
        xml1.push("<condition attribute='createdon' operator='null' />");
        xml1.push("</filter>");
        xml1.push("</entity>");
        xml1.push("</fetch>");
        var layoutXml1 = "<grid name='resultset' " + "object='1' " + "jump='bsd_channelsegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_channelsegmentid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_segmentt").addCustomView(viewId1, entityName1, viewDisplayName1, xml1.join(""), layoutXml1, true);
    }
    if (accountgroup != null) {
        var viewId2 = "{D9EA3132-950F-44F8-811E-156E8191B945}";
        var entityName2 = "bsd_accountgroupchannel";
        var viewDisplayName2 = "test";
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml2.push("<entity name='bsd_accountgroupchannel'>");
        xml2.push("<attribute name='statecode'/>");
        xml2.push("<attribute name='bsd_channel' />");
        xml2.push("<attribute name='bsd_accountgroup' />");
        xml2.push("<attribute name='bsd_name' />");
        xml2.push("<attribute name='bsd_accountgroupchannelid' />");
        xml2.push("<attribute name='createdon' />");
        xml2.push("<order attribute='bsd_accountgroup' descending='false' />");
        xml2.push("<filter type='and' >");
        xml2.push("<condition attribute='bsd_accountgroup' operator='eq' uitype='bsd_accountgroup' value='" + accountgroup[0].id + "' />");
        xml2.push("<condition attribute='statecode' operator='eq' value='0' />");
        xml2.push("</filter>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_accountgroupchannelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_accountgroupchannelid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_accountgroupp").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
    }
    if (accountgroup == null) {
        var viewId2 = "{D9EA3132-950F-44F8-811E-156E8191B945}";
        var entityName2 = "bsd_accountgroupchannel";
        var viewDisplayName2 = "test";
        xml2.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml2.push("<entity name='bsd_accountgroupchannel'>");
        xml2.push("<attribute name='statecode'/>");
        xml2.push("<attribute name='bsd_channel' />");
        xml2.push("<attribute name='bsd_accountgroup' />");
        xml2.push("<attribute name='bsd_name' />");
        xml2.push("<attribute name='bsd_accountgroupchannelid' />");
        xml2.push("<attribute name='createdon' />");
        xml2.push("<order attribute='bsd_accountgroup' descending='false' />");
        xml2.push("<filter type='and' >");
        xml2.push("<condition attribute='createdon' operator='null' />");
        xml2.push("</filter>");
        xml2.push("</entity>");
        xml2.push("</fetch>");
        var layoutXml2 = "<grid name='resultset' " + "object='1' " + "jump='bsd_accountgroupchannelid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_accountgroupchannelid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_accountgroupp").addCustomView(viewId2, entityName2, viewDisplayName2, xml2.join(""), layoutXml2, true);
    }
    if (segment != null)
    {
        xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml3.push("<entity name='bsd_channelsegment'>");
        xml3.push("<attribute name='bsd_name' />")
        xml3.push("<attribute name='createdon' />");
        xml3.push("<attribute name='statecode' />");
        xml3.push("<attribute name='bsd_segment' />");
        xml3.push("<attribute name='bsd_channel' />");
        xml3.push("<attribute name='bsd_channelsegmentid' />");
        xml3.push("<order attribute='bsd_name' descending='false' />");
        xml3.push("<filter type='and' >");
        xml3.push("<condition attribute='bsd_channelsegmentid' operator='eq' uitype='bsd_channelsegment' value='" + segment[0].id + "' />");
        xml3.push("</filter>");
        xml3.push("</entity>");
        xml3.push("</fetch>");
        CrmFetchKit.Fetch(xml3.join(""), false).then(function (rs3) {
            if (rs3.length > 0) {
                var viewId4 = "{1A73B586-16F8-4478-9F60-D82FA897AF46}";
                var entityName4 = "bsd_typesegment";
                var viewDisplayName4 = "test";
                xml4.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
                xml4.push("<entity name='bsd_typesegment'>");
                xml4.push("<attribute name='bsd_typesegmentid'/>");
                xml4.push("<attribute name='bsd_name' />")
                xml4.push("<attribute name='createdon' />");
                xml4.push("<attribute name='bsd_type' />");
                xml4.push("<attribute name='statecode' />");
                xml4.push("<attribute name='bsd_segment' />");
                xml4.push("<order attribute='bsd_name' descending='false' />");
                xml4.push("<filter type='and' >");
                xml4.push("<condition attribute='bsd_segment' operator='eq' uitype='bsd_industriallevel2' value='" + rs3[0].attributes.bsd_segment.guid + "' />");
                xml4.push("<condition attribute='statecode' operator='eq' value='0' />");
                xml4.push("</filter>");
                xml4.push("</entity>");
                xml4.push("</fetch>");
                var layoutXml4 = "<grid name='resultset' " + "object='1' " + "jump='bsd_typesegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                                   "<row name='result'  " + "id='bsd_typesegmentid'>  " +
                                   "<cell name='bsd_name'   " + "width='200' />  " +
                                   "<cell name='createdon'    " + "width='100' />  " +
                                   "</row>   " +
                                "</grid>   ";
                Xrm.Page.getControl("bsd_typee").addCustomView(viewId4, entityName4, viewDisplayName4, xml4.join(""), layoutXml4, true);
            }
        }, function (er) {
            console.log(er.message)
        });    
    }
    if (segment == null)
    {
        var viewId3 = "{1A73B586-16F8-4478-9F60-D82FA897AF46}";
        var entityName3 = "bsd_typesegment";
        var viewDisplayName3 = "test";
        xml3.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml3.push("<entity name='bsd_typesegment'>");
        xml3.push("<attribute name='bsd_typesegmentid'/>");
        xml3.push("<attribute name='bsd_name' />")
        xml3.push("<attribute name='createdon' />");
        xml3.push("<attribute name='bsd_type' />");
        xml3.push("<attribute name='statecode' />");
        xml3.push("<attribute name='bsd_segment' />");
        xml3.push("<order attribute='bsd_name' descending='false' />");
        xml3.push("<filter type='and' >");
        xml3.push("<condition attribute='createdon' operator='null' />");
        xml3.push("</filter>");
        xml3.push("</entity>");
        xml3.push("</fetch>");
        var layoutXml3 = "<grid name='resultset' " + "object='1' " + "jump='bsd_typesegmentid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                           "<row name='result'  " + "id='bsd_typesegmentid'>  " +
                           "<cell name='bsd_name'   " + "width='200' />  " +
                           "<cell name='createdon'    " + "width='100' />  " +
                           "</row>   " +
                        "</grid>   ";
        Xrm.Page.getControl("bsd_typee").addCustomView(viewId3, entityName3, viewDisplayName3, xml3.join(""), layoutXml3, true);
    }
}
// 22.03.2017 Đăng: setDisabled Customer
function setDisabled_Customer() {
    var customer = getValue("bsd_customer");
    if (customer != null) {
        setDisabled("bsd_customer", true);
    }
}