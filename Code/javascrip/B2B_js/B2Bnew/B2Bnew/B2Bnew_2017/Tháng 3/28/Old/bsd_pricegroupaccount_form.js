//Author:Mr.Phong
//Description:filter account type customer and BHS
function setaccounttypecustomer() {
    var xml = [];
    var viewId = "{A9AF0AB8-861D-4CFA-92A5-C6281FED7FAB}";
    var entityName = "account";
    var viewDisplayName = "test";
    fetch(xml, "account", ["accountid", "name"], ["name"], false, null, ["bsd_accounttype"], ["eq"], [0, "861450000"]);
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='accountid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                      "<row name='result'  " + "id='accountid'>  " +
                      "<cell name='name'    " + "width='200' />  " +
                      "</row>   " +
                   "</grid>   ";
    Xrm.Page.getControl("bsd_account").addCustomView(viewId, entityName, viewDisplayName, xml.join(""), layoutXml, true);
    Xrm.Page.getControl("bsd_account").addPreSearch(addFilter);
}
function addFilter() {
    var customerAccountFilter = "<filter type='and'><condition attribute='contactid' operator='null' /></filter>";
    Xrm.Page.getControl("bsd_account").addCustomFilter(customerAccountFilter, "contact");
}
//Author:Mr.Phong
//Description:set filed name
function set_valuefilename() {
    var id = getId();
    var account = getValue("bsd_account");
    var pricegroup = getValue("bsd_pricegroup");
    if (account != null && pricegroup != null) {
        var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                        '<entity name="bsd_pricegroupaccount">',
                        '<attribute name="bsd_pricegroupaccountid" />',
                        '<attribute name="bsd_name" />',
                        '<order attribute="bsd_name" descending="false" />',
                        '<filter type="and">',
                        '<condition attribute="bsd_pricegroup" operator="eq" uitype="bsd_pricegroups" value="' + pricegroup[0].id + '" />',
                        '<condition attribute="bsd_account" operator="eq" uitype="account" value="' + account[0].id + '" />',
                        '<condition attribute="bsd_pricegroupaccountid" operator="ne" value="' + id + '" />',
                        '</filter>',
                        '</entity>',
                        '</fetch>'].join("");
        var rs = CrmFetchKit.FetchSync(fetchxml);
        if (rs.length > 0) {
            setNotification("bsd_account", "Đã tồn tại, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_account");
            setValue("bsd_name", account[0].name);
        }
        
    }
    else {
        setNull("bsd_name");
    }
}