//Huy
function init() {
    load_addressfrom();
    if (formType() == 1) {
        setValue("bsd_unit", [{
            id: '{886009E0-0095-E611-80CC-000C294C7A2D}',
            name: 'Tấn',
            entityType: 'uom'
        }]);
        setValue("bsd_deliverymethod", 861450000);
        deliverymethod_change(true);
        addressfrom_change(true);

    } else {
        deliverymethod_change(false);
        addressfrom_change(false);
    }

}
function addressfrom_change(reset) {
    load_zone(reset);
}

function load_zone(reset) {
    if (reset != false) setNull(["bsd_zone", "bsd_leadtime", "bsd_distance"]);
    var address_from = getValue("bsd_addressfrom");
    if (address_from != null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
                   '   <entity name="bsd_zone">',
                   '     <attribute name="bsd_zoneid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '     <link-entity name="bsd_distance" from="bsd_zone" to="bsd_zoneid" alias="ab">',
                   '       <filter type="and">',
                   '         <condition attribute="bsd_warehouseaddress" operator="eq" uitype="bsd_address" value="'+address_from[0].id+'" />',
                   '       </filter>',
                   '     </link-entity>',
                   '   </entity>',
                   ' </fetch>'].join("");
        LookUp_After_Load("bsd_zone", "bsd_zone", "bsd_name", "Zone", xml);
    } else if (reset != false) {
        setNull(["bsd_zone", "bsd_leadtime", "bsd_distance"]);
        clear_lookup("bsd_zone", "bsd_zone");
    }

}

function zone_change(reset) {
    if (reset != false) setNull(["bsd_leadtime", "bsd_distance"]);
    var zone = getValue("bsd_zone");
    var address_from = getValue("bsd_addressfrom");
    if (zone != null && address_from!=null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_distance">',
                   '     <attribute name="bsd_distanceid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <attribute name="bsd_zone" />',
                   '     <attribute name="bsd_leadtime" />',
                   '     <attribute name="bsd_distance" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_zone" operator="eq" uiname="HCM" uitype="bsd_zone" value="'+zone[0].id+'" />',
                   '       <condition attribute="bsd_warehouseaddress" operator="eq"  uitype="bsd_address" value="' + address_from[0].id+ '" />',
                   '       <condition attribute="statecode" operator="eq" value="0" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");

        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                setValue("bsd_distance",rs[0].getValue("bsd_distance"));
                setValue("bsd_leadtime", rs[0].getValue("bsd_leadtime"));
            }
        });

    }
}

function effective_change() {
    clearNotification("bsd_effectiveto");

    var effective_from = getValue("bsd_effectivefrom");
    var effective_to = getValue("bsd_effectiveto");
    if (effective_to != null && effective_from != null) {
        if (effective_from > effective_to) {
            setNotification("bsd_effectiveto", "Effective To is not smaller than Effective To!");
        }
    }
}

function deliverymethod_change(reset) {
    clear_all_notification();
    if (reset != false) setNull(["bsd_priceofton", "bsd_priceunitporter", "bsd_priceoftrip", "bsd_truckload", "bsd_pricetripporter"]);
    var deliverymethod = getValue("bsd_deliverymethod");
    if (deliverymethod == 861450000) {
        setVisible(["bsd_priceofton", "bsd_priceunitporter", "bsd_unit"], true);
        setVisible(["bsd_priceoftrip", "bsd_truckload", "bsd_pricetripporter"], false);
    }
    else if (deliverymethod == 861450001) {
        setVisible(["bsd_priceofton", "bsd_priceunitporter", "bsd_unit"], false);
        setVisible(["bsd_priceoftrip", "bsd_truckload", "bsd_pricetripporter"], true);
    }
}
function clear_all_notification() {
    clearNotification("bsd_priceofton");
    clearNotification("bsd_priceunitporter");
    clearNotification("bsd_truckload");
    clearNotification("bsd_priceoftrip");
    clearNotification("bsd_pricetripporter");
}

function priceunit_change() {
    clear_all_notification()
    var priceunit = getValue("bsd_priceofton");
    if (priceunit < 0) {
        setNotification("bsd_priceofton", "Price Unit is not smaller than 0");
    }
}

function priceunitandporter_change() {
    clear_all_notification();
    var priceunitandporter = getValue("bsd_priceunitporter");
    if (priceunitandporter < 0) {
        setNotification("bsd_priceunitporter", "Price Unit + Porter is not smaller than 0");
    }
}

function pricetrip_change() {
    clear_all_notification()
    var pricetrip = getValue("bsd_priceoftrip");
    if (pricetrip < 0) {
        setNotification("bsd_priceoftrip", "Price Trip is not smaller than 0");
    }
}
function pricetripandporter_change() {
    clear_all_notification();
    var priceunitandporter = getValue("bsd_pricetripporter");
    if (priceunitandporter < 0) {
        setNotification("bsd_pricetripporter", "Price Trip + Porter is not smaller than 0");
    }
}

function truckload_change() {
    clear_all_notification();
}

function check_ton() {
    var priceunit = getValue("bsd_priceofton");
    var priceunitandporter = getValue("bsd_priceunitporter");

    if (priceunit == null && priceunitandporter == null) {
        setNotification("bsd_priceofton", "You must provide a value for Price Unit");
    }
}
function check_trip() {
    var pricetrip = getValue("bsd_priceoftrip");
    var pricetripandproter = getValue("bsd_pricetripporter");
    if (getValue("bsd_truckload") == null) {
        setNotification("bsd_truckload", "You must prodive a value for Truck Load");
    }
    else if (pricetrip == null && pricetripandproter == null) {
        setNotification("bsd_priceoftrip", "You must prodive a value for Price Trip");
    }
}

function load_priceofdo() {
    var effectiveto = getValue("bsd_effectiveto");
    var effectivefrom = getValue("bsd_effectivefrom");

    if (effectiveto != null & effectivefrom != null & effectivefrom <= effectiveto) {

        var year = effectiveto.getFullYear();
        var month = effectiveto.getMonth() + 1;
        var date = effectiveto.getDate();
        

        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
          '<entity name="bsd_priceofdo">',
            '<attribute name="bsd_priceofdoid" />',
            '<attribute name="bsd_name" />',
            '<attribute name="bsd_date" />',
            '<attribute name="bsd_unitprice" />',
            '<order attribute="bsd_date" descending="true" />',
            '<filter type="and">',
              '<condition attribute="bsd_date" operator="on-or-before" value="' + year + '-' + month + '-' + date + '" />',
            '</filter>',
          '</entity>',
        '</fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            console.log(rs);
            if (rs.length > 0) {
                var data = rs[0];
                setValue("bsd_namepriceofdo", [{
                    id: data.Id,
                    name: data.attributes.bsd_name.value,
                    entityType: data.logicalName
                }]);
                setValue("bsd_price", data.attributes.bsd_unitprice.value);
            } else {
                setNull(["bsd_namepriceofdo", "bsd_price"]);
            }

        });
    }
    else {
        setNull(["bsd_namepriceofdo", "bsd_price"]);
    }
}

function load_addressfrom() {
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="true">',
               '   <entity name="bsd_address">',
               '     <attribute name="bsd_name" />',
               '     <attribute name="bsd_purpose" />',
               '     <attribute name="bsd_addressid" />',
               '     <order attribute="bsd_name" descending="false" />',
               '     <link-entity name="bsd_warehouseentity" from="bsd_address" to="bsd_addressid" alias="ab" />',
               '   </entity>',
               ' </fetch>'].join("");
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='bsd_addressid'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='bsd_addressid'>  " +
                  "<cell name='bsd_name'   " + "width='200' />  " +
                  "</row>   " +
               "</grid>";
    getControl("bsd_addressfrom").addCustomView(getControl("bsd_addressfrom").getDefaultView(), "bsd_address", "Address", xml, layoutXml, true);
}

function onsave() {
    //var eventArgs = econtext.getEventArgs();
    //eventArgs.preventDefault();
    var deliverymethod = getValue("bsd_deliverymethod");
    if (deliverymethod == 861450000) {
        check_ton();
    } else if (deliverymethod == 861450001) {
        check_trip();
    }

}
