//Author:Mr.Đăng
//Description:Check ProductID & UpperCase
function Check_ProductID() {
    var productid = getValue("productnumber");
    if (productid != null) {
        setValue("productnumber", productid.toUpperCase());
    }
}
// 18.03.2017 Đăng: Khóa Product ID khi đã tạo
//Author:Mr.Đăng
//Description: Auto Load
function AutoLoad() {
    set_disableProductID();
}
//Author:Mr.Đăng
//Description: Disable Province
function set_disableProductID() {
    if (formType() == 2) {
        setDisabled("productnumber", true);
    }
    else {
        setDisabled("productnumber", false);
    }
}