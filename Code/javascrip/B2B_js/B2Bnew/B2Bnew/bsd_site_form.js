﻿//Author:Mr.Đăng
//Description:Check Name
function Check_name() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var name = getValue("bsd_name");
    if (name != null) {
        if (mikExp.test(name)) {
            setNotification("bsd_name", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            clearNotification("bsd_name");
        }
    }
}
//Author:Mr.Đăng
//Description:Check Code
function Check_code() {
    var mikExp = /[~`!@#$%\^&*+=\-\[\]\\';,./()_{}|\\":<>\?]/;
    var code = getValue("bsd_code");
    var id = getId();
    if (code != null) {
        if (mikExp.test(code)) {
            setNotification("bsd_code", "Không thể nhập ký tự đặc biệt, vui lòng kiểm tra lại.");
        }
        else {
            var fetchxml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                            '<entity name="bsd_site">',
                            '<attribute name="bsd_siteid" />',
                            '<attribute name="bsd_name" />',
                            '<order attribute="bsd_name" descending="false" />',
                            '<filter type="and">',
                            '<condition attribute="bsd_code" operator="eq" value="' + code + '" />',
                            '<condition attribute="bsd_siteid" operator="ne" value="' + id + '" />',
                            '</filter>',
                            '</entity>',
                            '</fetch>'].join("");
            var rs = CrmFetchKit.FetchSync(fetchxml);
            if (rs.length > 0) {
                setNotification("bsd_code", "Mã đã tồn tại, vui lòng kiểm tra lại.");
            } else {
                clearNotification("bsd_code");
                setValue("bsd_code", code.toUpperCase());
            }
        }
    }
}