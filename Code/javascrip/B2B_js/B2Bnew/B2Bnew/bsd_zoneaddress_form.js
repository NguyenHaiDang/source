
function init() {

    if (getValue("bsd_zone") == null) setDisabled("bsd_zone", false);
    if (formType() == 1) {

    } else {

    }

}

//huy 3h40 17/3/2017
function address_change() {
    clearNotification("bsd_address");

    var address = getValue("bsd_address");
    var zone = getValue("bsd_zone");
    if (address != null && zone!= null) {
        var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="bsd_zoneaddress">',
                   '     <attribute name="bsd_zoneaddressid" />',
                   '     <attribute name="bsd_name" />',
                   '     <attribute name="createdon" />',
                   '     <order attribute="bsd_name" descending="false" />',
                   '     <filter type="and">',
                   '       <condition attribute="bsd_address" operator="eq"  uitype="bsd_address" value="'+address[0].id+'" />',
                   '       <condition attribute="bsd_zone" operator="eq" uitype="bsd_zone" value="'+zone[0].id+'" />',
                   '     </filter>',
                   '   </entity>',
                   ' </fetch>'].join("");
        CrmFetchKit.Fetch(xml, false).then(function (rs) {
            if (rs.length > 0) {
                setNotification("bsd_address","Address đã tồn tại trong Zone.")
            } else {
                setValue("bsd_name", address[0].name);
            }
        });
    }
}

//Author:Mr.Đăng
//Description:Check Address
function setName() {
    var address = getValue("bsd_address");
    if (address != null) {
        setValue("bsd_name", address[0].name);
    }
    else {
        setNull("bsd_name");
    }
}