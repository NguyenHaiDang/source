function getId() {
    return Xrm.Page.data.entity.getId();
}
function getEntityName() {
    return Xrm.Page.data.entity.getEntityName();
}
function setRequired(attributeName, level) {
    if (typeof attributeName == 'object') {
        for (var i = 0; i < attributeName.length; i++) {
            Xrm.Page.getAttribute(attributeName[i]).setRequiredLevel(level);
        }
    } else if (typeof attributeName == 'string') {
        Xrm.Page.getAttribute(attributeName).setRequiredLevel(level);
    }
}
function setVisible(attributeName, visibiled) {
    if (typeof attributeName == 'object') {
        for (var i = 0; i < attributeName.length; i++) {
            Xrm.Page.ui.controls.get(attributeName[i]).setVisible(visibiled);
        }
    } else if (typeof attributeName == 'string') {
        Xrm.Page.ui.controls.get(attributeName).setVisible(visibiled);
    }
}
function setDisabled(attributeName, disabled) {
    if (typeof attributeName == 'object') {
        for (var i = 0; i < attributeName.length; i++) {
            Xrm.Page.getControl(attributeName[i]).setDisabled(disabled);
        }
    } else if (typeof attributeName == 'string') {
        Xrm.Page.getControl(attributeName).setDisabled(disabled);
    }
}
function setValue(attributeName, value) {
    if (Xrm.Page.getAttribute(attributeName) == null) {
        alert("Kh�ng c� field " + attributeName);
    } else {
        Xrm.Page.getAttribute(attributeName).setValue(value);
    }

}
function setNull(attributeName) {
    if (typeof attributeName == 'object') {
        for (var i = 0; i < attributeName.length; i++) {
            Xrm.Page.getAttribute(attributeName[i]).setValue(null);
        }
    } else if (typeof attributeName == 'string') {
        Xrm.Page.getAttribute(attributeName).setValue(null);
    }
}
function getValue(attributeName) {
    return Xrm.Page.getAttribute(attributeName).getValue();
}
function getControl(attributeName) {
    return Xrm.Page.getControl(attributeName);
}
function setFormNotification(message, type, id) {
    Xrm.Page.ui.setFormNotification(message, type, id);
}
function clearFormNotification(id) {
    Xrm.Page.ui.clearFormNotification(id);
}
function setNotification(attributeName, message) {
    Xrm.Page.getControl(attributeName).setNotification(message);
} // set form
function clearNotification(attributeName) {
    Xrm.Page.getControl(attributeName).clearNotification();
} // clear form
function formType() {
    return Xrm.Page.ui.getFormType();
}
function getDefaultView(lookupfield) {
    return Xrm.Page.getControl(lookupfield).getDefaultView();
}
function check_all_field_required_valid() {
    var populated = true;
    Xrm.Page.getAttribute(function (attribute, index) {
        if (attribute.getRequiredLevel() == "required") {
            if (attribute.getValue() === null) {
                populated = false;
            }
        }
    });
    return populated;
}
function DisabledForm() {
    Xrm.Page.data.entity.attributes.forEach(function (attribute, index) {
        var control = Xrm.Page.getControl(attribute.getName());
        if (control) {
            control.setDisabled(true)
        }
    });
}
function errorHandler(err) {

}
function m_confirm(content, callback) {
    window.top.$ui.Confirm("Confirm", content, function (e) {
        callback();
    });
}
function m_alert(message) {
    window.top.$ui.Dialog("Infomation", message);
}
function setSubmitMode(attributeName, type) {
    if (typeof attributeName == 'object') {
        for (var i = 0; i < attributeName.length; i++) {
            Xrm.Page.getAttribute(attributeName[i]).setSubmitMode(type);
        }
    } else if (typeof attributeName == 'string') {
        Xrm.Page.getAttribute(attributeName).setSubmitMode(type);
    }
}
function reload_page() {
    Xrm.Utility.openEntityForm(getEntityName(), getId());
}
// Xrm.Page.data.save().then(function () {});
//Xrm.Page.getAttribute("bsd_quantity").setSubmitMode("always");
Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}
function clear_lookup(field, entityname) {
    var xml = ['<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">',
                   '   <entity name="' + entityname + '">',
                   '     <filter type="and"><condition attribute="createdon" operator="null"/></filter>',
                   '   </entity>',
                   ' </fetch>'].join("");
    var layoutXml = "<grid name='resultset' object='1' jump='" + entityname + "id' select='1' icon='0' preview='0'><row name='result' id='" + entityname + "id'><cell name='createdon' width='200' /></row></grid>";
    getControl(field).addCustomView(getDefaultView(field), entityname, entityname, xml, layoutXml, true);
}

//Phong
function getvalue(a) {
    var b = [];
    for (var i = 0; i < a.length; i++) {
        var j = Xrm.Page.getAttribute(a[i]).getValue();
        if (j != null) {
            var string = typeof j;
            if (string == "string") {
                b.push(j);
            }
            if (string == "boolean") {
                if (j == true) {
                    b.push("1");
                }
                if (j == false) {
                    b.push("0");
                }
            }
            if (string == "number") {
                b.push(j);
            }
            if (string == "object") {
                if (j[0].id != null) {
                    b.push(j[0].id);
                }
                if (j[0].name != null) {
                    b.push(j[0].name);
                }
            }
        }
        if (j == null) {
            b.push(null);
        }
    }
    return b;
}
function getAttribute(field) {
    return Xrm.Page.getAttribute(field);
}
//end


/*Diệm*/
function getFullDay(day) {
    var fullday = null;
    var createdatefrom = getValue(day);
    if (createdatefrom != null) {
        var year = createdatefrom.getFullYear();
        var date = createdatefrom.getDate();

        var laymonth = (createdatefrom.getMonth() + 1);
        var month;
        if (laymonth > 0 && laymonth <= 9) {
            month = "0" + laymonth;
        } else {
            month = laymonth;
        }
        fullday = year + "-" + month + "-" + date;
    }
    return fullday;
}
function getFullDaySystem(day) {
    var fullday = null;
    var createdatefrom = day;
    if (createdatefrom != null) {
        var year = createdatefrom.getFullYear();
        var date = createdatefrom.getDate();

        var laymonth = (createdatefrom.getMonth() + 1);
        var month;
        if (laymonth > 0 && laymonth <= 9) {
            month = "0" + laymonth;
        } else {
            month = laymonth;
        }
        fullday = year + "-" + month + "-" + date;
    }
    return fullday;
}
function LookUp_After_Load(attributors, logicalname, name, displayname, fetchxml) {
    var id = logicalname + "id";
    var layoutXml = "<grid name='resultset' " + "object='1' " + "jump='" + id + "'  " + "select='1'  " + "icon='0'  " + "preview='0'>  " +
                  "<row name='result'  " + "id='" + id + "'>  " +
                  "<cell name='" + name + "'   " + "width='200' />  " +
                  "<cell name='createdon'    " + "width='100' />  " +
                  "</row>   " +
               "</grid>";
    getControl(attributors).addCustomView(getControl(attributors).getDefaultView(), logicalname, displayname, fetchxml, layoutXml, true);
}
// set visible section
function setVisibleSection(general, section, condition) {
    Xrm.Page.ui.tabs.get(general).sections.get(section).setVisible(condition);//condition(true/ false)
}
/*
   check starting date <= finish date.
*/
function CheckDateFromTo(from_date, to_date) {
    var todate = getValue(to_date);
    var fromdate = getValue(from_date);
    if (fromdate != null && todate != null) {
        if (todate.getFullYear() > fromdate.getFullYear()) {
            clearNotification(from_date);
        }
        else if (todate.getFullYear() == fromdate.getFullYear()) {
            if ((todate.getMonth() + 1) > (fromdate.getMonth() + 1)) {
                clearNotification(from_date);
            }
            else if ((todate.getMonth() + 1) == (fromdate.getMonth() + 1)) {
                if (todate.getDate() >= fromdate.getDate()) {
                    clearNotification(from_date);
                } else {
                    setNotification(from_date, "Please!!! Choose start date smaller than end date");
                }
            }
            else {
                setNotification(from_date, "Please!!! Choose start date smaller than end date");
            }
        } else {
            setNotification(from_date, "Please!!! Choose start date smaller than end date");
        }
    }
}
/*End*/