//Author:Mr.Phong
//Description:get account number,account name,credit limit
function getaccountnumberaccountnamecreditlimit() {
    debugger;
    var customer = Xrm.Page.getAttribute("bsd_account").getValue();
    var xml = [];
    if (customer != null) {
        xml.push("<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>");
        xml.push("<entity name='account'>");
        xml.push("<attribute name='name' />");
        xml.push("<attribute name='primarycontactid' />");
        xml.push("<attribute name='telephone1' />");
        xml.push("<attribute name='accountid' />");
        xml.push("<attribute name='creditlimit' />");
        xml.push("<attribute name='bsd_accountname' />");
        xml.push("<attribute name='accountnumber' />");
        xml.push("<order attribute='name' descending='false' />");
        xml.push("<filter type='and'>");
        xml.push("<condition attribute='accountid' operator='eq' uitype='account'   value='" + customer[0].id + "' />");
        xml.push("</filter>");
        xml.push("</entity>");
        xml.push("</fetch>");
        CrmFetchKit.Fetch(xml.join(""), true).then(function (rs) {
            if (rs.length > 0) {
                if (rs[0].attributes.accountnumber != null) {
                    Xrm.Page.getAttribute("bsd_customeraccount").setValue(rs[0].attributes.accountnumber.value);
                }
                if (rs[0].attributes.name != null) {
                    Xrm.Page.getAttribute("bsd_customername").setValue(rs[0].attributes.name.value);
                }
                if (rs[0].attributes.creditlimit != null) {
                    Xrm.Page.getAttribute("bsd_cretditlimit").setValue(rs[0].attributes.creditlimit.value);
                }
            }
        }, function (er) {
            console.log(er.message)
        });
    }
    else {
        Xrm.Page.getAttribute("bsd_customeraccount").setValue(null);
        Xrm.Page.getAttribute("bsd_customername").setValue(null);
        Xrm.Page.getAttribute("bsd_cretditlimit").setValue(null);
    }
}
//Author:Mr.Đăng
//Description:Load Name
function LoadName() {
    var account = getValue("bsd_account");
    if (account != null) {
        console.log(account);
        setValue("bsd_name", account[0].name);
    }
    else {
        setNull("bsd_name");
    }
}
